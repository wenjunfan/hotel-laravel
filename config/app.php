<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    'name' => env('APP_NAME', 'Usitrip Hotels'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'https://hotel.usitrip.com'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'America/Los_Angeles',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'cn',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'cn',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'daily'),
    'log_max_files' => 30,

    /*
    |--------------------------------------------------------------------------
    | Application host names
    |--------------------------------------------------------------------------
    |
    */

    'b_host_keyword' => env('B_HOST_KEYWORD', '117book'),
    'c_host_keyword' => env('C_HOST_KEYWORD', 'usitrip'),
    'c_en_host_keyword' => env('C_EN_HOST_KEYWORD', 'usitour'),

    'b_host' => env('B_HOST', '117book.com'),
    'c_host' => env('C_HOST', 'hotel.usitrip.com'),
    'c_en_host' => env('C_EN_HOST', 'hotel.usitour.com'), // could be any *.usitour.com except tours and tour subdomain

    /*
    |--------------------------------------------------------------------------
    | Usitrip Api & Auth
    | TODO: cleanup, remove default production value, put them to env files
    |--------------------------------------------------------------------------
    |
    */
    'usi_endpoint' => env('USITRIP_ENDPOINT', 'https://www.usitrip.com'),
    'usi_user_name' => env('USITRIP_USERNAME', 'api@117book.com'),
    'usi_user_pwd' => env('USITRIP_USERPW', '2355906871Chris'),
    'usi_search_api_address' => env('SEARCHAPIADDR', 'http://api3.usitrip.com:8080/webservice-search/search/searchNameResults'),
    'usi_search_mark_api_address' => env('SEARCHMARKAPIADDR', 'http://api3.usitrip.com:8080/webservice-search/search/markKeyword'),
    'usi_filter_mark_api_address' => env('FILTERMARKAPIADDR', 'http://api3.usitrip.com:8080/webservice-search/search/markFilterword'),
    'usi_hotels_api_test' => env('HOTELS_API_TEST', 'http://35.162.145.32:8080/backend-test/hotels'),
    'usi_hotels_api' => env('HOTELS_API', 'http://35.162.145.32:8080/backend/hotels'),
    'usi_email_server_address' => env('MAILSERVERADDR', 'http://api3.usitrip.com:8080/mailServer/mail'),
    'partner_id' => env('PARTNER_ID', 11087),
    'aa_partner_id' => env('AA_PARTNER_ID', 11676),
    '2b_test_partner_id' => env('2B_TEST_PARTNER_ID',10663),
    '2b_ad_partner_id' => env('2B_AD_PARTNER_ID',10409),
    'paypal_button' => env('PAYPAL_BUTTON', 'AWby-yIGcrdj7Lz-PY7_DOV5YJPlehggQc84szXsxGgL8XO2tP9m_44gbxi3iN67GEjYfJjTxtLrESWM'),
    'sms_delay' => env('SMS_DELAY', 60 * 20), //20分钟以后
    'email_delay' => env('EMAIL_DELAY', 60 * 60 * 24 * 3), //三天以后
    'email_queue' => env('EMAIL_QUEUE', 'hotel'),
    'default_mail_to' => env('DEFAULT_MAIL_TO', '550@usitrip.com'),

    /*
    |--------------------------------------------------------------------------
    | CITCON auth
    |--------------------------------------------------------------------------
    |
    */

    'citcon_endpoint' => env('CENDPOINT', 'https://citconpay.com/payment'),
    'citcon_token' => env('CTOKEN', '4E74C91F813848A88BE56200E3BAF735'),

    /*
    |--------------------------------------------------------------------------
    | 3rd Party Service Env Configuration 2018.02.15
    |--------------------------------------------------------------------------
    | reCaptcha: 机器人验证码服务
    | TWILIO: 手机验证码服务
    | PAYPAL: 美国支付服务
    |
    */

    'recaptcha_site_key' => env('RECAPTCHA_SITE_KEY', '6LcEiaIUAAAAAEijAes44hxl-RYJTE6Lk64zdpsI'),
    'twilio_account_sid' => env('TWILIO_ACCOUNT_SID', 'ACd1c86e9322ed9442415fd8e4ed9b61ce'),
    'twilio_auth_token' => env('TWILIO_AUTH_TOKEN', 'f34d84939fd38c96cb1075583e5db66d'),
    'twilio_number' => env('TWILIO_NUMBER', '14243250569'),
    'google_maps_api_key' => env('GOOGLE_MAPS_API_KEY', 'AIzaSyCIMUUWVX4IHta5KDwBZUWiOJ6Gvuos6wk'),

    /*
    |--------------------------------------------------------------------------
    | Other Env Configuration 2019.01.23
    |--------------------------------------------------------------------------
    | enable_h5_we: open wechat pay in h5
    | enable_cc: 是否开放信用卡支付，仅对c端有效
    | expedia_bookable: 是否开放Expedia酒店预订
    | hide_expedia_rooms: 是否隐藏 expedia 房型
	| enable_nonrefundable_cc: 是否可以用信用卡预订不可取消房型
    | send_exception_alert: 是否打开exception alert邮件
    | show_site_notes: 是否打开顶部网站环境注意事项
    | enable_tracking：是否代开ga的tracking功能
    | enable_email_campaign：是否打开email campaign功能
    |
    */

    'enable_h5_we' => env('ENABLEH5WE', true),
    'enable_cc' => env('ENABLECC', true),
    'expedia_bookable' => env('EXPEDIA_BOOKABLE', false),
    'hide_expedia_rooms' => env('HIDEEXPEDIAROOMS', false),
    'enable_nonrefundable_cc' => env('ENABLENONREFUNDABLECC', true),
    'cache_lifetime' => env('CACHE_LIFETIME', 30),//30 mins default
    'send_exception_alert' => env('SENDEXCEPTIONALERT', false),
    'show_site_notes' => env('SHOWSITENOTES', true),
    'enable_tracking' => env('ENABLETRACKING', false),
    'enable_email_campaign' => env('ENABLEEMAILCAMPAGIN', false),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Jenssegers\Agent\AgentServiceProvider::class,
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        Mpociot\ApiDoc\ApiDocGeneratorServiceProvider::class,
        Barryvdh\Debugbar\ServiceProvider::class,
        Infusionsoft\FrameworkSupport\Laravel\InfusionsoftServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
        'Agent' => Jenssegers\Agent\Facades\Agent::class,

    ],

];
