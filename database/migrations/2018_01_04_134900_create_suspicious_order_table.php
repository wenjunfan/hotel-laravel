<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuspiciousOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suspicious_order', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('Reorderid', 32);
            $table->string('payment_id', 32);
            $table->text('search_info');
            $table->string('cc_info', 255);
            $table->string('booker', 255);
            $table->text('rooms');
            $table->string('file_url')->nullable();
            $table->string('reorder_url');
            $table->float('amount');
            $table->string('currency');
            $table->integer('sales_no')->nullable();
            $table->integer('isLocked')->nullable()->default(0);
            $table->integer('isDone')->nullable()->default(0);
            $table->integer('isDeclined')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suspicious_order');
    }
}
