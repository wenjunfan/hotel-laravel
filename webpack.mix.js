/*
 * 1. b端和c端的客户端页面基本使用相同blade，除了个别pc页面(home和search)，他们的assets应编译到public/assets/(js|css)/pc下；
 * 2. c端客户端pc版的模板为master.blade.php，global assets为public/assets/(js|css)/pc/(master.css|master.js)；
 * 3. b端客户端pc版的模板为master-b.blade.php，global assets为public/assets/(js|css)/pc/(master-b.css|master-b.js)
 * 4. b端和c端的后台使用相同的模板和assets文件，其下属页面的assets也都编译到public/assets/(js|css)/admin下；
 * note: vuejs should be compiled to js injected globally in layout; moment.js should be complied in single page
 */

let mix = require('laravel-mix'),
    fs = require('fs'),
    sass_folder_excludes = ['variables', 'components'],
    js_folder_excludes = ['components'];

//non production site use uncompressed file for easier debugging
var vuejs = 'node_modules/vue/dist/vue.js';
if (mix.inProduction()) {
    vuejs = 'node_modules/vue/dist/vue.min.js';
}

// get all 'files' in a directory
let getFiles = function (dir) {
    return fs.readdirSync(dir).filter(file => {
        return fs.statSync(`${dir}/${file}`).isFile();
    });
};

// get all 'directories' in a directory
let getDir = function(dir) {
    return fs.readdirSync(dir).filter(folder => {
        return fs.statSync(`${dir}/${folder}`).isDirectory();
    });
};

// compile all sass files except variables and components folder
getDir('resources/assets/sass').forEach(function (dir) {
    if (!sass_folder_excludes.includes(dir)) {
        getFiles('resources/assets/sass/' + dir).forEach(function (filepath) {
            mix.sass('resources/assets/sass/' + dir + '/' + filepath, 'public/assets/css/' + dir + '/');
        });
    }
});

// compile all js files except components folder
getDir('resources/assets/js').forEach(function (dir) {
    if (!js_folder_excludes.includes(dir)) {
        getFiles('resources/assets/js/' + dir).forEach(function (filepath) {
            mix.js('resources/assets/js/' + dir + '/' + filepath, 'public/assets/js/' + dir + '/');
        });
    }
});

// -----------------------------------------------------------------------mobile 客户端-----------------------------------------------------------------------
// mobile plugins css
mix.combine([
    'public/css/lato.css',
    'node_modules/muse-ui/dist/muse-ui.css',
    'node_modules/muse-ui/dist/theme-light.min.css',
    'resources/assets/plugins/toastr/toastr.min.css',
    'resources/assets/plugins/sweetalert2/sweetalert2.css',
    'resources/assets/plugins/ladda/ladda-themeless.min.css',
    'resources/assets/plugins/slick/slick.css',
    'resources/assets/plugins/fontawesome.min.css',
    'resources/assets/plugins/slick/slick-theme.css',
], 'public/assets/css/mobile/plugins.mobile.css');

// mobile plugins js
mix.combine([
    'resources/assets/plugins/jquery/jquery.js',
    'resources/assets/custom/autocomplete/jquery.autocomplete.js',
    'resources/assets/plugins/moment.js',
    'resources/assets/plugins/toastr/toastr.min.js',
    'resources/assets/plugins/sweetalert2/sweetalert2.js',
    'resources/assets/plugins/validate/jquery.validate.js',
    'resources/assets/plugins/ladda/spin.js',
    'resources/assets/plugins/ladda/ladda.js',
    'resources/assets/plugins/ladda/ladda.jquery.js',
    'resources/assets/plugins/slick/slick.min.js',
    'resources/assets/js/includes/actions.js',
    'public/assets/js/mobile/app.js',
], 'public/assets/js/mobile/plugins.mobile.js');

// mobile paypal css, this page is extending master blade
mix.combine([
    'resources/assets/plugins/iCheck/custom.css', //check icon the customized green and orange one
    'public/assets/css/mobile/paypal.css',
], 'public/assets/css/mobile/plugins.paypal.css');

// mobile paypal js
mix.combine([
    'resources/assets/plugins/iCheck/icheck.js',   //check icon the customized green and orange one
    'resources/assets/plugins/jquery.maskedinput.js',
    'public/assets/js/mobile/paypal.js',
], 'public/assets/js/mobile/plugins.paypal.js');

// mobile voucher css
mix.combine([
    'public/css/roboto.css',
    'resources/assets/plugins/fontawesome.min.css',
    'resources/assets/plugins/fontawesome.brands.min.css',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.min.css',
    'resources/assets/plugins/sweetalert/sweetalert.css',
    'public/assets/css/mobile/voucher.css',
], 'public/assets/css/mobile/voucher.css');

// mobile file upload css
mix.combine([
    'resources/assets/plugins/sweetalert/sweetalert.css',
    'public/assets/css/mobile/file-upload.css',
], 'public/assets/css/mobile/plugins.file-upload.css');

// -----------------------------------------------------------------------pc c端客户端-----------------------------------------------------------------------
// pc master plugins css
mix.combine([
    'public/css/roboto.css',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.min.css',
    'resources/assets/plugins/fontawesome.min.css',
    'resources/assets/plugins/toastr/toastr.min.css',
    'resources/assets/plugins/side-customer-services/side-customer-services.css',
    'resources/assets/plugins/animate.css',
    'resources/assets/plugins/sweetalert/sweetalert.css',
    'resources/assets/plugins/ladda/ladda-themeless.min.css',
    'resources/assets/plugins/pace/pace.css',// page loading
    'resources/assets/plugins/sk-spinner.css',
    'public/assets/css/pc/common.css',
    'public/assets/css/pc/header.css',
    'public/assets/css/pc/master.css',
], 'public/assets/css/pc/plugins.master.css');

// pc master plugins js
mix.combine([
    'resources/assets/plugins/jquery/jquery.js',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.js',
    'resources/assets/plugins/sweetalert/sweetalert.min.js', // sweet alert
    'resources/assets/plugins/side-customer-services/side-customer-services.js', // side customer services plug
    'resources/assets/plugins/pace/pace.1.0.2.min.js',// page loading
    'resources/assets/plugins/slimscroll.min.js',// div scroll
    'resources/assets/custom/distributor.js',// distributor for marketing partner
    'resources/assets/plugins/toastr/toastr.min.js', // alert mini
    'resources/assets/plugins/ladda/spin.js', //ladda loading
    'resources/assets/plugins/ladda/ladda.js',
    'resources/assets/plugins/ladda/ladda.jquery.js',
    vuejs,
    'resources/assets/js/includes/actions.js',
    'public/assets/js/pc/master.js',
], 'public/assets/js/pc/plugins.master.js');

// pc home plugins css
mix.combine([
    'resources/assets/plugins/datapicker/datepicker3.css',
    'resources/assets/plugins/slick/slick.css',
    'resources/assets/plugins/slick/slick-theme.css',
    'resources/assets/plugins/awesome-bootstrap-checkbox.css',
    'resources/assets/plugins/iCheck/custom.css',
    'resources/assets/custom/hotel-hot-city-sub.css', //hot city and destination
    'public/assets/css/pc/home.css',
], 'public/assets/css/pc/plugins.home.css');

// pc home plugins js
mix.combine([
    'resources/assets/plugins/datapicker/bootstrap-datepicker.js',
    'resources/assets/plugins/iCheck/icheck.min.js',
    'resources/assets/plugins/slick/slick.min.js',
    'resources/assets/plugins/poster/js/posterGrid.js',  //home promotion slider
    'resources/assets/custom/autocomplete/jquery.autocomplete.js',
    'resources/assets/custom/autocomplete/autocomplete.js', //customized autocomplete
    'resources/assets/custom/hotel-hot-city-sub.js', //hot city and destination
    'resources/assets/custom/distributor.js',// distributor for marketing partner
    'resources/assets/plugins/jquery.maskedinput.js',
    'public/assets/js/pc/home.js',
], 'public/assets/js/pc/plugins.home.js');

// pc list plugins  css
mix.combine([
    'resources/assets/plugins/datapicker/datepicker3.css', //calendar
    'resources/assets/plugins/iCheck/custom.css',
    'resources/assets/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css', //customized checkbox for filter
    'resources/assets/custom/autocomplete/autocomplete.css', //customized autocomplete
    'resources/assets/custom/hotel-hot-city-sub.css', //hot city and destination
], 'public/assets/css/pc/plugins.list.css');

// pc list plugins js
mix.combine([
    'resources/assets/plugins/iCheck/icheck.js',
    'resources/assets/plugins/datapicker/bootstrap-datepicker.js', //calendar
    'resources/assets/custom/autocomplete/autocomplete.js', //customized autocomplete
    'resources/assets/custom/autocomplete/jquery.autocomplete.js', //original autocomplete
    'resources/assets/custom/hotel-hot-city-sub.js', //hot city and destination
    'public/assets/js/pc/list.js',
], 'public/assets/js/pc/plugins.list.js');

// pc hotel plugins  css
mix.combine([
    'resources/assets/plugins/datapicker/datepicker3.css', //calendar
    'resources/assets/plugins/blueimp-gallery.min.css',//gallery
    'resources/assets/plugins/iCheck/custom.css', //check icon the customized green and orange one
    'resources/assets/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css', //customized checkbox for filter
], 'public/assets/css/pc/plugins.hotel.css');

// pc hotel plugins js
mix.combine([
    'resources/assets/plugins/datapicker/bootstrap-datepicker.js', //calendar
    'resources/assets/plugins/iCheck/icheck.js', //check icon the customized green and orange one
    'resources/assets/plugins/check.js', //hotel change date logic
    'public/assets/js/pc/hotel.js',
], 'public/assets/js/pc/plugins.hotel.js');

// pc book plugins  css
mix.combine([
  'public/assets/css/pc/book.css',
], 'public/assets/css/pc/plugins.book.css');


// pc book plugins js
mix.combine([
    'resources/assets/plugins/validate/jquery.validate.js',  //rules validate plugin
    'resources/assets/custom/distributor.js',  //tracking distributors for orders
    'public/assets/js/pc/book.js',
], 'public/assets/js/pc/plugins.book.js');


// pc book-check plugins js
mix.combine([
  'resources/assets/plugins/validate/jquery.validate.js',  //rules validate plugin
  'resources/assets/custom/distributor.js',  //tracking distributors for orders
  'resources/assets/plugins/sweetalert/sweetalert.min.js', // sweet alert
], 'public/assets/js/pc/plugins.book-check.js');

// pc paypal plugins  css
mix.combine([
    'resources/assets/plugins/iCheck/custom.css', //check icon the customized green and orange one
    'public/assets/css/pc/paypal.css',
], 'public/assets/css/pc/plugins.paypal.css');

// pc paypal plugins js
mix.combine([
    'resources/assets/plugins/iCheck/icheck.js',   //check icon the customized green and orange one
    'resources/assets/plugins/validate/jquery.validate.js',   //rules validate plugin
    'resources/assets/plugins/jquery.maskedinput.js',
    'public/assets/js/pc/paypal.js',
], 'public/assets/js/pc/plugins.paypal.js');

// pc deal css
mix.combine([
    'resources/assets/plugins/datapicker/datepicker3.css',
    'public/assets/css/pc/deal.css',
], 'public/assets/css/pc/deal.css');

// pc deal js
mix.combine([
    'resources/assets/custom/autocomplete/jquery.autocomplete.js',
    'resources/assets/plugins/datapicker/bootstrap-datepicker.js', //calendar
    'resources/assets/plugins/moment.js',
    'public/assets/js/pc/deal.js',
], 'public/assets/js/pc/all.deal.js');

// pc file upload css
mix.combine([
    'resources/assets/plugins/sweetalert/sweetalert.css',
    'public/assets/css/pc/file-upload.css',
], 'public/assets/css/pc/plugins.file-upload.css');

// -----------------------------------------------------------------------pc b端客户端-----------------------------------------------------------------------
// pc master-b plugins css
mix.combine([
    'public/css/roboto.css',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.min.css',
    'resources/assets/plugins/fontawesome.min.css',
    'resources/assets/plugins/sweetalert/sweetalert.css',
    'resources/assets/plugins/fullscreen/fullscreen-spinner.css',
    'resources/assets/plugins/ladda/ladda-themeless.min.css',
    'resources/assets/plugins/inspinia/inspinia.css',
    'resources/assets/plugins/pace/pace.css',// page loading
    'resources/assets/plugins/toastr/toastr.min.css',
    'resources/assets/plugins/sk-spinner.css',
    'public/css/hotel/global.css', // 包含各种plugin的衍生css
    'public/assets/css/pc/common.css',
    'public/assets/css/pc/master-b.css',
], 'public/assets/css/pc/plugins.master-b.css');

// pc master-b plugins js
mix.combine([
    'resources/assets/plugins/jquery/jquery.js',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.js',
    'resources/assets/plugins/metismenu.min.js',// menu
    'resources/assets/plugins/slimscroll.min.js',// div scroll
    'resources/assets/plugins/pace/pace.1.0.2.min.js',// page loading
    'resources/assets/plugins/sweetalert/sweetalert.min.js',
    'resources/assets/plugins/fullscreen/fullscreen-spinner.js',
    'resources/assets/plugins/ladda/spin.js', //ladda loading
    'resources/assets/plugins/ladda/ladda.js',
    'resources/assets/plugins/ladda/ladda.jquery.js',
    'resources/assets/plugins/inspinia/inspinia.js',
    'resources/assets/plugins/toastr/toastr.min.js',
    'resources/assets/js/includes/actions.js',
    vuejs,
], 'public/assets/js/pc/plugins.master-b.js');

// pc business home css
mix.combine([
    'public/css/roboto.css',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.min.css',
    'resources/assets/plugins/iCheck/custom.css',
    'resources/assets/plugins/datapicker/datepicker3.css',
    'resources/assets/plugins/inspinia/inspinia.css',
    'resources/assets/plugins/fullscreen/fullscreen-spinner.css',
    'resources/assets/plugins/pace/pace.css',// page loading
    'resources/assets/plugins/animate.css',
    'resources/assets/plugins/fontawesome.min.css',
    'public/css/hotel/global.css',
    'public/assets/css/business/home.css',
], 'public/assets/css/business/plugins.home.css');

// pc business home js
mix.combine([
    'resources/assets/plugins/jquery/jquery.js',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.js',
    'resources/assets/plugins/slimscroll.min.js',// div scroll
    'resources/assets/plugins/inspinia/inspinia.js',
    'resources/assets/plugins/pace/pace.1.0.2.min.js',// page loading
    'resources/assets/plugins/wow/wow.min.js', // Animations When You Scroll.
    'resources/assets/plugins/iCheck/icheck.min.js',   //check icon the customized green and orange one
    'resources/assets/plugins/datapicker/bootstrap-datepicker.js',
    'resources/assets/plugins/fullscreen/fullscreen-spinner.js',
    'resources/assets/plugins/jquery.maskedinput.js',
    'resources/assets/plugins/metismenu.min.js',// menu
], 'public/assets/js/business/plugins.home.js');

// pc business search
mix.combine([
    'resources/assets/plugins/datapicker/datepicker3.css', //calendar
    'resources/assets/plugins/iCheck/custom.css',
    'resources/assets/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css', //customized checkbox for filter
    'resources/assets/custom/autocomplete/autocomplete.css', //customized autocomplete
    'resources/assets/custom/hotel-hot-city-sub.css', //hot city and destination
    'public/assets/css/pc/search.css',
], 'public/assets/css/business/plugins.search.css');

// pc business search
mix.combine([
    'resources/assets/plugins/iCheck/icheck.js',
    'resources/assets/plugins/jquery.maskedinput.js',
    'resources/assets/plugins/datapicker/bootstrap-datepicker.js', //calendar
    'resources/assets/custom/autocomplete/autocomplete.js', //customized autocomplete
    'resources/assets/custom/autocomplete/jquery.autocomplete.js', //original autocomplete
    'resources/assets/custom/hotel-hot-city-sub.js', //hot city and destination
], 'public/assets/js/business/plugins.search.js');

mix.combine([
    'resources/assets/plugins/iCheck/custom.css',
    'resources/assets/plugins/sweetalert/sweetalert.css',
], 'public/assets/css/business/plugins.files.css');

mix.combine([
    'resources/assets/plugins/iCheck/icheck.js',
    'resources/assets/plugins/sweetalert/sweetalert.min.js',
    'resources/assets/plugins/validate/jquery.validate.js',
], 'public/assets/js/business/plugins.files.js');

// -----------------------------------------------------------------------bc端客户端-----------------------------------------------------------------------
// pc hotel info update page
mix.combine([
    'public/css/roboto.css',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.min.css',
    'resources/assets/plugins/sweetalert/sweetalert.css',
    'resources/assets/plugins/sk-spinner.css',
    'public/assets/css/pc/hotel-update.css',
], 'public/assets/css/pc/all.hotel-update.css');
mix.combine([
    'resources/assets/plugins/jquery/jquery.js',
    'resources/assets/plugins/validate/jquery.validate.js',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.js',
    'resources/assets/plugins/sweetalert/sweetalert.min.js',
    vuejs,
    'public/assets/js/pc/hotel-update.js'
], 'public/assets/js/pc/all.hotel-update.js');

// -----------------------------------------------------------------------bc端后台-----------------------------------------------------------------------
mix.combine([
    'public/css/roboto.css',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.min.css',
    'resources/assets/plugins/dataTables/datatables.min.css',
    'resources/assets/plugins/fontawesome.min.css',
    'resources/assets/plugins/sweetalert/sweetalert.css',
    'resources/assets/plugins/fullscreen/fullscreen-spinner.css',
    'resources/assets/plugins/ladda/ladda-themeless.min.css',
    'resources/assets/plugins/toastr/toastr.min.css',
    'resources/assets/plugins/pace/pace.css',// page loading
    'resources/assets/plugins/inspinia/inspinia.css',
    'public/css/hotel/global.css',
    'public/assets/css/admin/admin.css',
], 'public/assets/css/admin/plugins.admin.css');

mix.combine([
    'resources/assets/plugins/jquery/jquery.js',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.js',
    'resources/assets/plugins/dataTables/datatables.min.js',
    'resources/assets/plugins/metismenu.min.js',// menu
    'resources/assets/plugins/slimscroll.min.js',// div scroll
    'resources/assets/plugins/pace/pace.1.0.2.min.js',// page loading
    'resources/assets/plugins/sweetalert/sweetalert.min.js',
    'resources/assets/plugins/fullscreen/fullscreen-spinner.js',
    'resources/assets/plugins/ladda/spin.js', //ladda loading
    'resources/assets/plugins/ladda/ladda.js',
    'resources/assets/plugins/ladda/ladda.jquery.js',
    'resources/assets/plugins/inspinia/inspinia.js',
    'resources/assets/plugins/toastr/toastr.min.js',
    'resources/assets/js/includes/actions.js',
    vuejs,
], 'public/assets/js/admin/plugins.admin.js');

// -----------------------------------------------------------------------c端后台-----------------------------------------------------------------------
// c端 affiliate login css
mix.combine([
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.min.css',
    'resources/assets/plugins/fontawesome.min.css',
    'resources/assets/plugins/iCheck/custom.css',    //check icon the customized green and orange one
    'resources/assets/plugins/animate.css',
], 'public/assets/css/admin/plugins.affiliate.login.css');

// c端 affiliate login js
mix.combine([
    'resources/assets/plugins/jquery/jquery.js',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.js',
    'resources/assets/plugins/iCheck/icheck.js',   //check icon the customized green and orange one
    'resources/assets/plugins/sweetalert/sweetalert.min.js',
], 'public/assets/js/admin/plugins.affiliate.login.js');

mix.combine([
    'resources/assets/plugins/datapicker/datepicker3.css',
    'public/assets/css/admin/deal-list.css',
], 'public/assets/css/admin/plugins.deal-list.css');

mix.combine([
    'resources/assets/plugins/validate/jquery.validate.js',
    'resources/assets/plugins/datapicker/bootstrap-datepicker.js',
    'resources/assets/plugins/moment.js',
    'public/assets/js/admin/deal-list.js',
], 'public/assets/js/admin/plugins.deal-list.js');

// -----------------------------------------------------------------------b端后台-----------------------------------------------------------------------
// b端 admin（含b端用户）销售记录 orders styles
mix.combine([
    'resources/assets/plugins/dataTables/datatables.min.css',
    'resources/assets/plugins/chosen/bootstrap-chosen.css',
    'resources/assets/plugins/datapicker/datepicker3.css', //calendar
], 'public/assets/css/admin/plugins.orders.css');

// b端 admin（含b端用户）销售记录 orders js
mix.combine([
    'resources/assets/plugins/dataTables/datatables.min.js',
    'resources/assets/plugins/chosen/chosen.jquery.js',
    'resources/assets/plugins/datapicker/bootstrap-datepicker.js',
], 'public/assets/js/admin/plugins.orders.js');

// b端 财务到账记录（及b端用户） checkAllOrders styles
mix.combine([
    'resources/assets/plugins/dataTables/datatables.min.css',
    'resources/assets/plugins/chosen/chosen.jquery.js',
    'resources/assets/plugins/datapicker/datepicker3.css', //calendar
    'resources/assets/plugins/bootstrap-switch.min.css',
], 'public/assets/css/admin/plugins.checkorders.css');

// b端 财务到账记录（及b端用户） checkAllOrders js
mix.combine([
    'resources/assets/plugins/dataTables/datatables.min.js',
    'resources/assets/plugins/datapicker/bootstrap-datepicker.js',
    'resources/assets/plugins/bootstrap-switch.js',
], 'public/assets/js/admin/plugins.checkorders.js');

// b端 checkAllOrders 到账记录 css
mix.combine([
    'resources/assets/plugins/chosen/bootstrap-chosen.css',
    'resources/assets/plugins/datapicker/datepicker3.css',
    'resources/assets/plugins/daterangepicker/daterangepicker-bs3.css',
], 'public/assets/css/admin/plugins.check-all-orders.css');

// b端 checkAllOrders 到账记录 js 使用vue
mix.combine([
    'resources/assets/plugins/chosen/chosen.jquery.js',
    'resources/assets/plugins/moment.js',
    'resources/assets/plugins/datapicker/bootstrap-datepicker.js',
    'resources/assets/plugins/daterangepicker/daterangepicker.js',
], 'public/assets/js/admin/plugins.check-all-orders.js');

// b端 accounts 账户管理 js 使用vue
mix.combine([
    'resources/assets/plugins/daterangepicker/daterangepicker-bs3.css',
], 'public/assets/css/admin/plugins.accounts.css');


mix.combine([
    'resources/assets/plugins/moment.js',
    'resources/assets/plugins/daterangepicker/daterangepicker.js',
    'public/assets/js/admin/accounts.js',
], 'public/assets/js/admin/plugins.accounts.js');


mix.combine([
  'resources/assets/plugins/moment.js',
  'resources/assets/plugins/daterangepicker/daterangepicker.js',
  'public/assets/js/affiliate/accounts.js',
], 'public/assets/js/affiliate/plugins.accounts.js');
// -----------------------------------------------------------------------其他（laravel自带页面）-----------------------------------------------------------------------
// laravel generic pages style
mix.combine([
    'public/css/roboto.css',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.min.css',
    'resources/assets/plugins/fontawesome.min.css',
    'resources/assets/plugins/iCheck/custom.css',
    'resources/assets/plugins/animate.css',
    'resources/assets/plugins/sweetalert/sweetalert.css',
    'resources/assets/plugins/inspinia/inspinia.css',
    'public/css/hotel/global.css', // 包含各种plugin的衍生css
], 'public/assets/css/plugins.generic.css');

// laravel generic pages scripts
mix.combine([
    'resources/assets/plugins/jquery/jquery.js',
    'resources/assets/plugins/bootstrap/bootstrap.3.3.6.js',
    'resources/assets/plugins/iCheck/icheck.min.js',
    'resources/assets/plugins/sweetalert/sweetalert.min.js',
], 'public/assets/js/plugins.generic.js');

// voucher page
mix.combine([
  'resources/assets/plugins/jquery/jquery.js',
  'resources/assets/plugins/sweetalert/sweetalert.min.js',
  'resources/assets/js/includes/actions.js',
], 'public/assets/js/plugins.voucher.js');

// c端后台 price-ratio page
mix.combine([
  'resources/assets/plugins/iCheck/icheck.min.js',
  'resources/assets/plugins/chosen/chosen.jquery.js',
  'resources/assets/plugins/typehead/bootstrap3-typeahead.min.js',//多选Input
  'resources/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',// 多选 tags
  'resources/assets/plugins/sweetalert/sweetalert.min.js',
], 'public/assets/js/plugins.price-ratio.js');

mix.combine([
  'resources/assets/plugins/iCheck/custom.css', //check icon the customized green and orange one
  'resources/assets/plugins/chosen/bootstrap-chosen.css',
  'resources/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',// 多选 tags
  'resources/assets/plugins/sweetalert/sweetalert.css',
], 'public/assets/css/plugins.price-ratio.css');

// 404 page
mix.combine([
    'resources/assets/plugins/fontawesome.min.css',
    'public/css/lato.css',
], 'public/assets/css/404.css');
