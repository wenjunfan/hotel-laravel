# [WIKI LINK (all documents)](https://bitbucket.org/usitrip/hotel_c/wiki/Home)

## A.酒店B/C端，前端源文件 assets文件夹结构：   
- B/C端后台文件夹名称: admin, 使用AdminLTE, RWD自适应   
- C端前台，手机端和PC端分开，非自适应    
    
````  
├── resoures
	├── assets/
        ├── js/                         // js源文件
             ├── admin/                 // B端后台 js源文件(117book，使用AdminLTE, RWD自适应)
                ├── accounts.js         // B端后台 账户管理页面 js源文件
                ├── check-all-orders.js // B端后台 财务订单管理页面 js源文件
                ├── deal-list.js       // B端后台 coupon 页面 js源文件
             ├── affiliate/             // C端后台 js源文件(分销/AD 后台)
                ├── accounts.js         // C端后台 账户管理页面 js源文件
             ├── componets/             // C/B端前台 通用vue组件
             ├── mobile/                // C端前台 手机端 js源文件
                ├── components/         // C端前台 手机端 vue组件
                ├── app.js              // C端前台 手机端 全局js入口文件
                ├── book.js             // C端前台 手机端 book页js源文件
                ├── cancelSubResult.js  // C端前台 手机端 
                ├── deal.js            // C端前台 手机端
                ├── home.js             // C端前台 手机端 
                ├── hotel.js            // C端前台 手机端 
                ├── list.js             // C端前台 手机端 
                ├── voucher.js          // C端前台 手机端 
             ├── pc/                    // C端前台 PC端 js源文件
                ├── components/         // C端前台 PC端 vue组件
                ├── book.js             // C端前台 PC端 book页js源文件
                ├── deal.js            // C端前台 PC端
                ├── home.js             // C端前台 PC端 
                ├── hotel.js            // C端前台 PC端 
                ├── list.js             // C端前台 PC端 
                ├── master.js           // C端前台 PC端 
                ├── paypal.js           // C端前台 PC端 
                ├── header.js     // C端前台 PC端
                ├── header-en.js  // C端前台 PC端
             ├── feedback.js        // 
        ├── plugins/                 // 第三方插件含js和css(新插件可以直接复制文件夹到这里)
             ├── animate/           // ......
        ├── sass/                   // 全局定义及每个页面的sass源文件
             ├── admin/             // B/C端后台样式定义（使用AdminLTE, RWD自适应）
                ├── import/         // 后台全局样式变量定义（AdminLTE） 尽量不要动！
                     ├── _badges_labels.scss
                        .....
                     ├── _variables.scss
             ├── mobile/                // 手机端样式定义(使用muse-ui)
             ├── pc/                    // B/C端前台样式定义（部分使用AdminLTE）
             ├── colors.scss            // 
             ├── colors-business.scss   // 
             ├── file-upload.scss       // 
             ├── processing-modal.scss  // 
````  

## B.后端文件夹结构说明  

文件夹说明  
  
````
├── app/
    ├── Classes/                    // Api类
    ├── Event/                      // 事件类
    ├── Exception/                  // 异常类
    ├── Http/
        ├── Controllers/ 
            ├── Admin/                      // B端 117book.com 后台admin
            ├── Affiliate/                  // C端后台 
            ├── Business/                   // B端 117book.com 
                ├── Auth                    //  B端登录注册
            ├── Client/                     // C端
                ├── Auth                    //  C端登录注册
            ├── Hotel/                      // C端 每个页面业务逻辑Controller
        ├── Middleware/                     // 中间件
        ├── Routes/                         // 路由定义
    ├── Jobs/                   // 任务
    ├── Listeners/  // 事件监听
    ├── Model/                  // 数据表对应模型（Model -> Eloquent ORM)
    ............
    ├── Traits/                 // 重复使用的方法代码块traits, 可以随时在function中加载
    ├── Utils/                  // TODO 全局调用的方法类，在composer中加载

```` 
  
文件说明：  

````
├── app/
    ├── Classes/                    // Api类
        ├── Paypal/                 // 
            ├── CreditCardInfo.php  // 
            ├── Paypal.php          // 
            ├── PaypalResult.php    // 
            ├── PayResult.php       // 
            ├── RefundResult.php    // 
        ├── PaypalNew/              // 
            ├── CreditCardInfoNew.php  // 
            ├── PaypalNew.php          // 
            ├── PaypalResultNew.php    // 
            ├── PayResultNew.php       // 
            ├── RefundResultNew.php    // 
        ├── CitconPay.php           //  
        ├── HotelApi.php            // 数据端返回 hotel Api 
        ├── HotelDB.php             // 
        ├── InfusionsoftAPI.php     // 
        ├── SinaUrl.php             // 
    ├── Event/                  // 异常类
        ├── EbRoomBookedEvent.php   // 
    ├── Exception/                  // 异常类
    ├── Http/
        ├── Controllers/ 
            ├── Admin/                      // B端 117book.com 后台admin
                ├── AccountController.php           //
                ├── CheckAllOrderController.php     //
                ├── DashboardController.php         //
                ├── FeedbackController.php          //
                ├── GroupRequestController.php      //
                ├── MissedListController.php        //
                ├── OrderController.php             //
                ├── PaymentController.php           //
                ├── PotentialListController.php     //
                ├── ProfileController.php           //
                ├── RefundController.php            //
                ├── StatementController.php         //
            ├── Affiliate/                  // C端后台 
                ├── AccountController.php           //
                ├── DashboardController.php         //
                ├── DealListController.php          //
                ├── FeedbackController.php          //
                ├── FraudListController.php         //
                ├── MissedListController.php        //
                ├── OrderController.php             //
                ├── PotentiaListController.php      //
                ├── ProfileController.php           //
            ├── Business/                   // B端 117book.com 
                ├── Auth                    //
                    ├── AuthController.php          //
                    ├── PasswordController.php      //
                ├── DashboardController.php //
                ├── ExtraPayController.php  //
                ├── HomeController.php      //
                ├── OrderController.php     //
                ├── ProfileController.php   //
                ├── RefundController.php    //
            ├── Client/                     // C端
                ├── Auth                    //
                    ├── AuthController.php          //
                    ├── PasswordController.php      //
                    ├── SendMessageController.php   //
                    ├── VerifyPhoneController.php   //
                    ├── VerifyuserController.php    //
                ├── CreditcardController.php//
                ├── DashboardController.php //
                ├── DealController.php     //
                ├── FeedbackController.php  //
                ├── HomeController.php      //
                ├── PaymentController.php   //
                ├── StatementController.php //
                ├── SubscribeController.php //
            ├── Hotel/                      // C端 每个页面业务逻辑Controller
                ├── BookController.php      //
                ├── CitconController.php    //
                ├── HotelController.php     //
                ├── LanguageController.php  //
                ├── ListController.php      //
                ├── PaypalController.php    //
                ├── VocherController.php    //
            ├── Util.php                    // helper functions
        ├── Middleware/                     // 以下只显示自定义中间件
            ├── AccountMiddleware.php       // 
            ├── AdminMiddleware.php         // 
            ├── DestributorMiddleware.php   // 
            ├── LangMiddleware.php          // 检测并配置网站语言：session & Locale
            ├── PlateformMiddleware.php     // 检测访问者设备的平台并记录 
        ├── Routes/ 
            ├── affiliate.php               // C端后台（affiliate）路径           
            ├── api.php                     // B端和C端共有的api路径
            ├── business.php                // B端前台和后台路径
            ├── client.php                  // C端前台路径
            ├── routes.php                  // B端和C端共有的前台路径
    ├── Jobs/                   // 
        ├── SendAlertEmail.php                  //
        ├── SendReminderCancelActionEmail.php   //     
        ├── SendReminderSms.php                 //
        ├── TriggerFeedback.php                 //
        ├── UpdateInfusionHotelsData.php        //
        ├── UpdateInfusionsoftCityData.php      //
    ├── Listeners/  // 数据表对应模型（Model -> Eloquent ORM)
        ├── EbRoomBookedListener.php            //
    ├── Model/                  // 数据表对应模型（Model -> Eloquent ORM)
    ............
    ├── Traits/                 // 重复使用的方法代码块traits, 可以随时在function中加载
        ├── ApiPostTrait.php        // 调用数据端api的POST方法代码块
        ├── GetIpAddress.php        // 
        ├── HotelDataRetriver.php   // 
        ├── HttpGet.php             // 
        ├── IsLandLineTrait.php     // 
        ├── IsMobileDevice.php      // 
        ├── PaymentTrait.php        // 
        ├── SendMessageTrait.php    // 
        ├── UploadFile.php          // 
    ├── Utils/                  // TODO 全局调用的方法类，在composer中加载
        ├── Helper.php 

```` 


# Requirement #
PHP 7+ , node npm and Composer are required.

# Installation #
Before dev, run 'composer install' and then 'npm install'.
Then config .env, .local.env and .production.env

# Task Workflow #
1. On your local ```development``` branch, do ```git pull```.
2. Then start task with a new branch: ```git branch -b taskName (or your name)``` ( begin with feature/ or debug/ depend on your task type).
3. After you finish the task, commit all your changes ( a better practice is to commit as much as can, so that you can track your working steps).
4. Push your branch to bitbucket: ```git push origin branchName```.
5. Create a pull request from this branch to development on bitbucket.
6. Jie review the pr (pull request) and merge it.
7. Ann review new changes and update on production site.

# Important Tips #
* Your .env file should not be committed to your application's source control.
* You should have .env, .production.env and .local.env, where .env should only have content as "local" or "production" to decide which env to use. Ask the administrator for the .local.env and .production.env.
* Please clean your logs (either js's console or php's Log) before you create pr.
* Frontend: Local storage stores lang/ subEmail/ userEmail / source/
    checkin / checkout/ destination/ .
* Frontend: Cookies stores a.

# Implemented Techs #
* [Laravel](https://laravel.com/docs/5.2)
* [Alipay](https://github.com/latrell/Alipay)
* [jQuery Autocomplete](https://github.com/devbridge/jQuery-Autocomplete)
* [jQuery Datepicker](https://jqueryui.com/datepicker/)
* [SweetAlert](https://sweetalert.js.org/)
* [WechatPay](http://doc.citconpay.com/citconAPI/#intro)

