<?php

namespace App\Traits;

use Illuminate\Support\Facades\Log;
use App\Model\Sms;
use App\Model\Order;
use Exception;
use Illuminate\Support\Facades\Mail;

/**
 * Class SendMessageTrait
 * @package App\Http\Controllers\Client\Auth
 */
trait SendMessageTrait
{
    /**
     * @param $input_country_code
     * @param $input_mobile
     * @param $code
     * @return \Illuminate\Http\JsonResponse
     */
    private function sendSms($input_country_code ,$input_mobile, $code)
    {
        // 去除空格
        $country_code = str_replace(' ', '', $input_country_code);
        $mobile = str_replace(' ', '', $input_mobile);
        $full_mobile = $country_code . $mobile;
        // 检查sms表中是否有该手机号，验证码是否在10分钟内
        $sms = Sms::where('mobile', $full_mobile)
                  ->orderBy('created_at', 'desc')
                  ->first();

        if (strrpos($code, "usitour") > -1) {
            $name = 'Usitour';
        }else{
            $name = 'Usitrip';
        }

        if($sms == null) {
            // 手机没有验证码发送记录，很可能发送不成功，记录下来，发邮件给Ann
            Log::info('Non-cc payment send sms-' .$full_mobile);
        }
        // 如果在sms表中没有该手机号或者已经过了10分钟有效期，生成新的sms手机验证码记录
        $sms = new Sms(
            [
                'type'    => 0,
                'mobile'  => $full_mobile,
                'user_id' => '0',
            ]
        );

        // call twilio 服务, 无论成功与否，只能返回success true不阻碍流程
        try{
            $sms->sendOrderSuccessSms($name, $full_mobile, 0, $code);
        }catch(Exception $e){
            // 手机没有验证码发送不成功，记录下来，发邮件给Ann
            Log::info('send new order sms failed: ' . $e->getMessage());
            try {
                Log::info('payment method is not paid by credit card, send sms might occurs problem: phone number is ' . $full_mobile. 'voucher code is '. $code);
                $message = 'payment method is not paid by credit card, send sms might occurs problem: phone number is ' . $full_mobile. 'voucher code is '. $code;
                Mail::raw($message, function ($message) use ($full_mobile){
                    $message->subject('New SMS failed!'. $full_mobile )->to(config('staff-emails.ann'));
                });
            } catch (Exception $e) {
                Log::error($full_mobile .' send SMS Non-cc payment user email failed: ' . $e->getMessage());
            }
        }
        return response()->json([
            "success" => true
        ]);
    }

    /**
     * @param $input_country_code
     * @param $input_mobile
     * @param $link //继续下单短连接
     * @param $language //用户语言 cn en
     * @param $host //用户域名 Usitour Usitrip
     * @return \Illuminate\Http\JsonResponse
     */
    public function followUpSms($input_country_code ,$input_mobile, $link, $language, $host)
    {
        // 去除空格
        $country_code = str_replace(' ', '', $input_country_code);
        $mobile = str_replace(' ', '', $input_mobile);
        $full_mobile = $country_code . $mobile;
        $sms = Sms::where('short_link', $link)
            ->orderBy('created_at', 'desc')
            ->first();
        Log::info('remainder sms send or not? ' . json_encode($sms));
        if($sms == null) {
            // 手机没有相同短连接发送记录
            $sms = new Sms(
                [
                    'type'    => 0,
                    'mobile'  => $full_mobile,
                    'user_id' => '0',
                    'short_link' => $link,
                ]
            );
            if($language === 'cn'){
                $msg = '尊敬的客人您好！您还在查看这间酒店吗? '. $link .' 酒店折扣马上就要结束啦，快快行动哦-走四方酒店网';
            }else{
                $msg = 'Hi there! Are you still looking for this hotel? '. $link .' Act now, offer ends soon-'. $host.' Hotels';
            }

            // call twilio 服务, 无论成功与否，只能返回success true不阻碍流程
            try{
                $sms->send($msg, $full_mobile, 0,   0);
            }catch(Exception $e){
                // remainder email send failed, Email Ann
                Log::info('send remainder sms failed: ' . $e->getMessage());
                try {
                    Log::info(' payment method is not paid by credit card, send sms might occurs problem: phone number is ' . $full_mobile. 'follow up link is '.$link);
                    $message = ' payment method is not paid by credit card, send sms might occurs problem: phone number is ' . $full_mobile. 'follow up link is '. $link;
                    Mail::raw($message, function ($message) use ($full_mobile){
                        $message->subject('New remainder follow up SMS failed!'. $full_mobile )->to(config('staff-emails.ann'));
                    });
                } catch (Exception $e) {
                    Log::error($full_mobile .  ' send remainder follow up SMS failed Email Failed: ' . $e->getMessage());
                }
            }
        }else{
            Log::info($full_mobile . ':already sent remainder email' .$link);
        }
        return response()->json([
            "success" => true
        ]);
    }
}
