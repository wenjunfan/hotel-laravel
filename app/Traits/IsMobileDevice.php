<?php

namespace App\Traits;

/**
 * Trait IsMobileDevice
 * @package App\Traits
 */
trait IsMobileDevice
{
    /**
     * @return mixed
     */
    private function isMobileDevice ()
    {
        return \Agent::isMobile();
    }
}