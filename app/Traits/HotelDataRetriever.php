<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Model\SearchHistory;
use App\Model\Payment;
use Illuminate\Http\Request;

/**
 * Trait HotelDataRetriever
 * @package App\Traits
 */
trait HotelDataRetriever
{

    /**
     * @param $roomCount
     * @param $adultCount
     * @param $childCount
     * @return array
     */
    private function getOccupancies($roomCount, $adultCount, $childCount, $childAge)
    {
        $occupancies = [];
        $paxes = [];

        for ($i = 0; $i < $adultCount; $i++) {
            $paxes[] = [
                'type' => 'AD'
            ];
        }

        for ($i = 0; $i < $childCount; $i++) {
            $paxes[] = [
                'type' => 'CH',
                'age' => isset($childAge[$i]) ? $childAge[$i] : 12
            ];
        }

        $occupancies[] = [
            'rooms' => $roomCount,
            'adults' => $adultCount,
            'children' => $childCount,
            'paxes' => $paxes
        ];

        return $occupancies;
    }

    /**
     * @param $checkin
     * @param $checkout
     * @param $destinationName
     * @param null $destinationCity
     * @return array
     */
    private function generateStaticParams($checkin, $checkout, $destinationName, $destinationCity = null)
    {
        $occupancies = [];
        $paxes = [];
        $paxes[0] = [
            'type' => 'AD'
        ];
        $paxes[1] = [
            'type' => 'AD'
        ];
        $occupancies[0] = [
            'rooms' => 1,
            'adults' => 2,
            'children' => 0,
            'paxes' => $paxes,
        ];
        $searchInfo = [
            'checkin' => $checkin,
            'checkout' => $checkout,
            'destinationName' => $destinationName,
            'destinationCity' => $destinationCity || '',
            'showAllDiscount' => false,
            'occupancies' => $occupancies
        ];

        return $searchInfo;
    }

    /**
     * @param $checkin
     * @param $checkout
     * @return float
     */
    private function calculateDays($checkin, $checkout)
    {
        $checkin = strtotime($checkin);
        $checkout = strtotime($checkout);
        $length = ($checkout - $checkin) / 60 / 60 / 24;

        return round($length);
    }

    /**
     * @param $hotels
     * @param $hotelId
     * @return null
     */
    private function findHotel($hotels, $hotelId)
    {
        foreach ($hotels as $hotel) {
            if ($hotel['id'] == $hotelId) {
                return $hotel;
            }
        }

        return null;
    }

    /**
     * merge bulk and single static and dynamic hotel data
     * @param $hotelDetails
     * @param $hotelCaches
     * @param $roomCount
     * @param $dayCount
     * @return array
     */
    private function getFilteredPageHotels($hotelDetails, $hotelCaches, $roomCount, $dayCount)
    {
        $hotelDetails = json_decode(json_encode($hotelDetails), true);

        $finalHotels = [];

        foreach ($hotelDetails as $key => $hotel) {
            $hotelCache = $this->findHotel($hotelCaches, $hotel['hotelId']);
            if ($hotelCache == null) {
                continue;
            }

            $destination = $hotel['destination'];

            // setup hotel address
            $address = trim(str_replace(",", " ", $hotel['address']));
            $desName = is_null($destination) ? '' : substr($destination['desName'], 0, strrpos($destination['desName'], ","));
            $hotel['address'] = ucwords(strtolower($address)) . ', ' . $desName . ' ' . $hotel['zipcode'];
            $hotel['name'] = ucwords($hotel['name']);

            $beforeTax = $hotelCache['beforeTax'];
            $tax = $hotelCache['minRate'] - $beforeTax;

            $originalPrice = $hotelCache['minRate'];
            if ($hotelCache['discount'] > 0) {
                $originalPrice = floor($beforeTax / (1 - $hotelCache['discount']) / $roomCount / $dayCount) * $roomCount * $dayCount;
            }

            $hotel['thumbnail'] = config('constants.image_host') . (stripos($hotel['thumbnail'], '_Image') === false ? 'Hotel_Image/' : '') . $hotel['thumbnail'];
            $hotel['destinationName'] = isset($hotel['desName']) ? $hotel['desName'] : '';
            $hotel['destinationName_zh'] = isset($destination['desName_zh']) ? $destination['desName_zh'] : '';
            $hotel['countryName'] = isset($destination['countryName']) ? $destination['countryName'] : '';
            $hotel['countryName_zh'] = isset($destination['countryName_zh']) ? $destination['countryName_zh'] : '';
            $hotel['tax'] = $tax;
            $hotel['currency'] = $hotelCache['currency'];
            $hotel['discount'] = $hotelCache['discount'];
            $hotel['minPrice'] = $beforeTax / $roomCount / $dayCount;
            $hotel['originalPrice'] = $originalPrice / $roomCount / $dayCount;
            $hotel['lowestFlg'] = $hotelCache['lowestFlg'];
            $hotel['distance'] = $hotelCache['distance'];
            $hotel['guestRating'] = $hotelCache['guestRating'];
            $hotel['guestReviewCount'] = $hotelCache['guestReviewCount'];

            $finalHotels[] = $hotel;
        }

        return $finalHotels;
    }

    /**
     * @param $transaction
     * @param $result
     */
    private function updatePaymentHistory($transaction, $result)
    {
        Payment::where('transactionId', $transaction->transactionId)
            ->update([
                'orderReference' => $result['orderReference']
            ]);
    }

    /**
     * EBooking new order email
     * @param $order
     */
    public function sendEbEmail($order)
    {
        $hotel_user = DB::table('eb_hotel_user')->where('hotelId', $order->hotelId)->first();
        if (!empty($hotel_user)) {
            $hotel_user_id = $hotel_user->user_id;
            $eb_user = DB::table('eb_user')->where('user_id', $hotel_user_id)->first();
            $eb_user_email = !empty($eb_user) ? $eb_user->email : '';
        } else {
            $eb_user_email = '';
        }

        if (config('app.env') == 'local') {
            $eb_user_email = config('app.default_mail_to');
        }

        Log::info('ready for E-Booking provider email ' . $eb_user_email);
        $orderTem =$order->orderTem;
        $roomInfo = json_decode($orderTem['rooms'])[0];
        $roomDetails = $roomInfo->roomDetails;
        $boardName = $roomDetails->boardName;
        $roomType = $roomDetails->name;
        $cancellationInfo = $roomDetails->cancelDate;
        $cancellation = str_replace("T", " ", substr($cancellationInfo, 0, 19));
        if (!$cancellation) {
            $cancellation = "Non-Refundable";
        } else{
            $cancellation = "Free Cancellation before " . $cancellation;
        }
        $guestInfo = $roomInfo->paxes[0];
        $firstName1 = $guestInfo->name;
        $lastName1 = $guestInfo->surname;
        $guestName = $firstName1 . ' ' . $lastName1;
        $paxesSummary = $roomInfo->roomDetails->paxes;
        $ad_total = substr($paxesSummary, 0, 1);
        $ch_total = substr($paxesSummary, 3, 1);

        $data = [
            'toMail' => $eb_user_email,
            'hotelName' => $order->hotelName,
            'orderId' => $order->id,
            'checkin' => $order->checkinDate,
            'checkout' => $order->checkoutDate,
            'guestName' => $guestName,
            'roomCount' =>$order->room_number,
            'paxes' => $ad_total. ' adults, '. $ch_total. ' children' ,
            'boardType' =>  $boardName,
            'roomType' =>  $roomType,
            'cancellationPolicy' => $cancellation,
            'contractPrice' =>  $order->totalOriginalNetPrice,
        ];

        $this->post('/sendNewOrderEmail', $data);
    }
}
