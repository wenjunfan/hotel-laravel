<?php

namespace App\Traits;

use App\Model\Sms;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Log;

// 使用 twilio.com 查phone type, 一共有三种phone type返回： mobile / landline / voip
/*
    "mobile_country_code" => "310"
      "mobile_network_code" => "120"
      "name" => "Sprint Spectrum, L.P."
      "type" => "mobile"
      "error_code" => null
*/

/**
 * Trait IsLandLineTrait
 * @package App\Traits
 */
trait IsLandLineTrait
{
    /**
     * @param $phone_number
     * @return \Illuminate\Http\JsonResponse|string
     */
    private function getPhoneType($phone_number)
    {
        // $phone_number 去除 空格 和 -, 还有的客人电话前面加+，也去掉
        $final_phone_number = '+' . trim(str_replace('-', '', str_replace('+', '', $phone_number)));
        Log::info('Test voip or mobile: ' . $final_phone_number);
        // check exist mobile in sms table first, 避免重复请求(省钱)
        // phone_number 是从 sms短信验证来的，所以肯定在sms表中有记录。
        $checkPhoneNumber = Sms::where('mobile', $phone_number)->first();
        // check exist phone type (sometime not able to get the type on time, need double check)
        if ($checkPhoneNumber->phone_type && $checkPhoneNumber->phone_type != '0' && $checkPhoneNumber->phone_type != '') {
            // Log::info('phone number is in SMS table: ' . $phone_number);
            $checkPhoneNumber = json_decode($checkPhoneNumber, true);
            Log::info('phone number is in SMS table: ' . $checkPhoneNumber['phone_type'] . $checkPhoneNumber['phone_num_country']);
            return response()->json([
                'success' => true,
                'phoneType' => $checkPhoneNumber['phone_type'],
                'phoneCountryCode' => $checkPhoneNumber['phone_num_country'],
            ]);
        }
        try {
            // Your Account Sid and Auth Token from twilio.com/console
            $twilio = new Client(config('app.twilio_account_sid'), config('app.twilio_auth_token'));
            $phone_number = $twilio->lookups->v1->phoneNumbers($phone_number)->fetch(array("type" => "carrier"));
            // save phone type to sms table
            if ($phone_number->carrier['type'] == '0' || $phone_number->carrier['type'] == '') {
                //再試一次如果type的返回是0或者空也没办法了。。。省钱就只再跑一次
                $phone_number = $twilio->lookups->v1->phoneNumbers($phone_number)->fetch(array("type" => "carrier"));
            }
            // save phone type to sms table
            Log::info('twilio return carrier ' . print_r($phone_number->carrier, true) . ' country code ' . $phone_number->countryCode);
            $checkPhoneNumber->phone_type = $phone_number->carrier['type'];
            $checkPhoneNumber->phone_num_country = $phone_number->countryCode;
            $checkPhoneNumber->save();
        } catch (\Exception $e) {
            Log::error('twilio fetch phone type error: ' . $e->getMessage());
            return '';
        }
        return response()->json([
            'success' => true,
            'phoneType' => $phone_number->carrier['type'],
            'phoneCountryCode' => $phone_number->countryCode,
        ]);
    }
}