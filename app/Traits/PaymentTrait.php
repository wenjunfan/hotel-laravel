<?php

namespace App\Traits;

use App\Classes\Paypal\Paypal;
use App\Classes\Paypal\RefundResult;
use App\Model\Refund;
use App\Model\Payment;
use App\Classes\PaypalNew\PaypalNew;
use App\Classes\PaypalNew\RefundResultNew;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Log;

/**
 * Trait PaymentTrait
 * @package App\Traits
 */
trait PaymentTrait
{

    /**
     *  b and c user use different paypal to refund, if refund for c customer, or host not 117book use c paypal, else use b
     * @param $transaction
     * @return string
     */
    private function refund($transaction)
    {
        $is_c = false;
        $is_ppBth = false;

        if (isset($transaction->partnerId) && $transaction->partnerId == 11087) {
            $is_c = true;
        }
        if(!empty($transaction->transaction_type) && $transaction->transaction_type === 'Paypal Button'){
            $is_ppBth = true;
        }

        if ($is_c || strpos($this->host, '117book') === false || $is_ppBth) {
            $refundRes = PaypalNew::refund($transaction->transactionId, 'Full', $transaction->amount);
            if (!$refundRes['success']) {
                if (session('language', 0) == 0) {
                    return '退款失败，错误代码：' . $refundRes['errorNo'] . '，错误描述：' . $refundRes['errorMessage'];
                } else {
                    return 'Refund error, errorId is' . $refundRes['errorNo'] . ', and error message is: ' . $refundRes['errorMessage'];
                }
            }
            $refundResult = new RefundResultNew($refundRes['request'], $refundRes['response']);

            Log::info($this->ip . 'usitrip or usitour cc refund result: ' . json_encode($refundResult));

            unset($refundRes['request']);
            $transaction->operationResult = json_encode($refundRes);
            $this->partnerId = config('app.partner_id');
            $this->saveRefund($transaction, $refundResult, $this->partnerId);

            if ($refundResult->result != 0) {
                if (session('language', 0) == 0) {
                    return '退款失败，' . $refundResult->respMsg;
                } else {
                    return 'Refund failed. ' . $refundResult->respMsg;
                }
            }
        } else {
            $refundRes = Paypal::refund($transaction->transactionId, 'Full', $transaction->amount, '117book full refund');
            if (!$refundRes['success']) {
                return '退款失败，错误代码：' . $refundRes['errorNo'] . '，错误描述：' . $refundRes['errorMessage'];
            }
            $refundResult = new RefundResult($refundRes['request'], $refundRes['response']);

            Log::info($this->ip . ' 117book cc refund result: ' . json_encode($refundResult));
            unset($refundRes['request']);
            $transaction->operationResult = json_encode($refundRes);
            $this->partnerId = Auth::check() ? Auth::user()->id : config('app.partner_id');
            $this->saveRefund($transaction, $refundResult, $this->partnerId);

            if ($refundResult->ack != 'Success') {
                if (session('language', 0) == 0) {
                    return '退款失败.请联系客服';
                } else {
                    return 'Refund failed, please contact customer representative for details.';
                }
            }
        }

        // 目前只退全款
        if (session('language', 0) == 0) {
            return '已退全款';
        } else {
            return 'Fully Refunded';
        }
    }

    /**
     * @param $transaction
     * @param $refundResult
     * @param $partnerId
     */
    private function saveRefund($transaction, $refundResult, $partnerId)
    {
        $is_c = false;
        if (isset($transaction->partnerId) && $transaction->partnerId == 11087) {
            $is_c = true;
        }

        if ($is_c || strpos($this->host, '117book') === false) {
            $p = new Refund();
            if (isset($transaction->orderReference)) {
                $p->orderReference = $transaction->orderReference;
            }
            $p->partnerId = $partnerId;
            $p->transactionId = $transaction->transactionId;
            $p->refundType = $transaction->type;
            $p->amount = $transaction->amount;
            $p->currency = $transaction->currency;
            $p->note = $transaction->note;
            $p->result = $refundResult->result;
            $p->correlationId = $refundResult->correlationId;
            $p->request = $refundResult->request;
            $p->response = $transaction->operationResult;
            $p->name = $transaction->first_name . $transaction->last_name;
        } else {
            $p = new Refund;
            if (isset($transaction->orderReference)) {
                $p->orderReference = $transaction->orderReference;
            }
            $p->partnerId = $partnerId;
            $p->transactionId = $transaction->transactionId;
            $p->refundType = 'full';
            $p->amount = $transaction->amount;
            $p->currency = $transaction->currency;
            $p->note = '117book full refund';
            $p->ack = $refundResult->ack;
            $p->version = $refundResult->version;
            $p->build = $refundResult->build;
            $p->correlationId = $refundResult->correlationId;
            $p->request = $refundResult->request;
            $p->response = $transaction->operationResult;
            $p->errors = $refundResult->errors;
            $p->refundTransactionId = $refundResult->refundtransactionId;
            $p->feeRefundAmount = $refundResult->feeRefundAmount;
            $p->grossRefundAmount = $refundResult->grossRefundAmount;
            $p->netRefundAmount = $refundResult->netRefundAmount;
            $p->totalRefundedAmount = $refundResult->totalRefundedAmount;
            $p->name = $transaction->name;

            $p->save();
        }

        //尝试存储数据库，显示信用卡的返款数据
        try {
            $p->save();
        } catch (\Exception $e) {
            Log::error($this->ip . ' save paypal refund error: ' . $e->getMessage());
        }
    }

    /**
     * @param $result
     * @param $ccInfo
     * @return bool|mixed
     */
    private function savePaymentHistory($result, $ccInfo)
    {
        $p = new Payment();

        $p->partnerId = $this->partnerId;
        $p->transactionId = $result->transactionId;
        $p->correlationId = $result->correlationId;
        $p->request = $result->request;
        $p->cvv2Match = $result->cvv2Match;
        $p->amount = $result->amount;
        $p->currency = $result->currency;
        $p->operation_type = $result->operationType;
        $p->operation_result = $result->operationResult;

        if (isset($result->booker_email)) { //c端，存8位数因为有fraud check的可能性
            $p->booker_email = $result->booker_email;
            $p->result = $result->result;
            $p->authCode = $result->authCode;
            $p->ppref = $result->ppref;
            $p->first_name = $ccInfo->firstName;
            $p->last_name = $ccInfo->lastName;
            $p->cc_number = substr($ccInfo->number, -8);
            $p->exp_date = $ccInfo->month . '-' . $ccInfo->year;
        } else { //B端
            $p->ack = $result->ack;
            $p->version = $result->version;
            $p->build = $result->build;
            $p->errors = $result->errors;
            $p->avsCode = $result->avsCode;
            $p->first_name = $result->firstName;
            $p->last_name = $result->lastName;
            $p->cc_number = substr($result->number, -4);
        }
        try {
            $p->save();
            return $p->id;
        } catch (\Exception $e) {
            Log::error($this->ip . ' save paypal payment history error: ' . $e->getMessage());
            return false;
        }
    }
}