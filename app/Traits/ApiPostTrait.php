<?php
/**
 * Created by PhpStorm.
 * User: Haidong.Wang
 * Date: 15/08/2018
 * Time: 09:47
 */

namespace App\Traits;

use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Support\Facades\Mail;

/**
 * Trait ApiPostTrait
 * @package App\Traits
 */
trait ApiPostTrait
{
    /**
     * @param $method
     * @param $data
     * @return mixed
     */
    private static function post($method, $data)
    {
        $input = array();
        $input['data'] = $data;
        $data_string = json_encode($input);

        // create curl resource
        $ch = curl_init();

        // set URL and other appropriate options
        $curlSetupArray = array(
            CURLOPT_URL => config('app.usi_email_server_address') . $method, CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => 1,
        );

        // set url
        curl_setopt_array($ch, $curlSetupArray);

        // this are my post vars
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        //return the transfer as json
        $headers = array('Content-Type: application/json', 'Content-Length: ' . strlen($data_string),);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // $result contains the output json
        $result = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        Log::info($method . ' request: ' . $data_string);
        Log::info($method . ' response: ' . $result);

        $jsonResult = json_decode($result, true);

        if (isset($jsonResult['status']) && $jsonResult['status'] === 'success') {
            if (isset($data['toMail'])) {
                Log::info('EB MESSAGE SEND SUCCESSFULLY');
            } else {
                Log::info('MESSAGE SEND SUCCESSFULLY');
            }
        } else {
            if (config('app.env') === 'production') {
                try {
                    $msg = 'Java: ' . $method . ' failed for reason: ' . $jsonResult;
                    Mail::raw($msg, function ($message) {
                        $message->to(config('app.default_mail_to'))
                            ->subject('New Failed Java Email!!!!');
                    });
                } catch (Exception $e) {
                    Log::error('send java email failed!!!! error: ' . $e->getMessage());
                }
            }
        }

        return $jsonResult;
    }
}
