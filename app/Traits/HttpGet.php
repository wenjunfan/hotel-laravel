<?php

namespace App\Traits;

/**
 * Trait HttpGet
 * @package App\Traits
 */
trait HttpGet
{
    /**
     * @param $url
     * @return bool|string
     */
    private function httpGet ($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);

        $output = curl_exec($ch);

        if (curl_errno($ch)) {
            return false;
        }

        curl_close($ch);

        return $output;
    }
}