<?php

namespace App\Traits;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

/**
 * Trait UploadFile
 * @package App\Traits
 */
trait UploadFile
{
    /**
     * @param $file
     * @param $fileName
     * @return string
     */
	public function uploadFile($file, $fileName)
	{
		$storePath = '';

		try {
			Storage::disk('s3')->put($fileName, file_get_contents($file), 'public');
			$storePath = Storage::disk('s3')->url($fileName);
		} catch (\Exception $e) {
			Log::error('save file to aws error: ' . $e->getMessage());
		}

        if (strpos($fileName, 'b2b/') !== false) {
            $lastPartUrl = substr($storePath, strrpos($storePath, '/') + 1);
            $originalName = str_replace('b2b/','' ,$fileName);
            $fileUrl = str_replace($lastPartUrl,$originalName,$storePath);
        }else{
            $fileUrl = $storePath;
        }

		return $fileUrl;
	}
}