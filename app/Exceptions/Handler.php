<?php

namespace App\Exceptions;

use App\Traits\GetIpAddress;
use Exception;

use Illuminate\Http\Exception\PostTooLargeException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    use GetIpAddress;

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        TokenMismatchException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Exception  $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $e)
    {
        $ip = $this->getIpAddr();
        if (!empty($ip)) {
            $isNotInsider = strpos(implode(",", config('constants.innerIps')), $ip) === false;
        } else {
            $isNotInsider = true;
        }

        // check whether should email the exception
        if (($this->shouldReport($e) && !(strpos($e, 'Swift_TransportException') !== false || \Agent::isRobot())) && $isNotInsider) {
            if (config('app.send_exception_alert')) {
                $host = $request->getHost();
                try {
                    $receivers = [config('staff-emails.jie'), config('staff-emails.ann')];
                    if (config('app.env') === 'local') {
                        $receivers = [config('staff-emails.jie')];
                    }

                    $subject = $host . ' ' . $ip . ' ' . config('app.url') . ' PHP Exception';

                    Mail::queue('emails.exception', ['msg' => $e . '<br><br> Request details: ' . $request], function ($msg) use ($receivers, $subject) {
                        $msg->to($receivers)
                            ->subject($subject);
                    });
                } catch (Exception $e) {
                    Log::error('Send alert email failed, ' . $e->getMessage());
                }
            }
        }
        if ($e instanceof PostTooLargeException ) {
            return response()->json([
                'success' => false,
                'errorMessage' => session('language', 1) == 0 ? '文件太大' : 'File size too big'
            ]);
        }
        if ($e instanceof TokenMismatchException ) {
            return redirect('/'); //Redirect to home page if token mismatch
        }
        if ($e instanceof MethodNotAllowedHttpException ) {
            abort(404); //Redirect if method not allowed
        }
        return parent::render($request, $e);
    }
}
