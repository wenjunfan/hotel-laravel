<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\OrderTem;
use App\Traits\SendMessageTrait;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SendReminderSms extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, SendMessageTrait;

    public $countryCode;
    public $phone;
    public $link;
    public $language;
    public $host;
    public $Reorderid;

    /**
     * SendReminderSms constructor.
     * @param $countryCode //短信国家区号
     * @param $phone
     * @param $link //follow up short url
     * @param $language
     * @param $host
     * @return void
     */
    public function __construct($countryCode , $phone, $link, $language, $host, $Reorderid)
    {
        $this->countryCode = $countryCode;
        $this->phone = $phone;
        $this->link = $link;
        $this->language = $language;
        $this->host = $host;
        $this->Reorderid = $Reorderid;
    }

    /**
     * Execute the job.
     * 20 minutes later from config or env sms-delay
     * @return void
     */
    public function handle()
    {
        Log::info('------------start sent follow sms to potential order--------------------');
        $order = OrderTem::where('Reorderid', $this->Reorderid)->get()->first();
        $code = $order->code;
        if($order != null && $code != ''){
            Log::info('No Remainder SMS Job needed, already have voucher '.$code);
        }else{
            try {
                $this->followUpSms($this->countryCode ,  $this->phone, $this->link, $this->language, $this->host);
            }catch(\Exception $exception){
                Log::error('send remainder sms failed or no need to send: ' . $exception->getMessage());
            }
        }
    }
}
