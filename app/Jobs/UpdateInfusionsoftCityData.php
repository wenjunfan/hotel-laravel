<?php

namespace App\Jobs;

use App\Classes\InfusionsoftAPI;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class UpdateInfusionsoftCityData extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $contact;

    public $fields;

    public $destination;

    public $hotels;

    public $goals;

    public $tags;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($contact, $fields, $destination, $hotels, $goals, $tags)
    {
        $this->contact = $contact;
        $this->fields = $fields;
        $this->destination = $destination;
        $this->hotels = $hotels;
        $this->goals = $goals;
        $this->tags = $tags;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //update infusionsoft contact info
        $fields = $this->fields;
        $destination = $this->destination;

        try {
            //创建或更新infusionsoft上contact信息
            $infusionsoft = new InfusionsoftAPI();
            $lang = $fields['language'];
            $is_cn = $lang === 'cn';

            $custom_fields = [];

            if (isset($destination)) {
                $city = ($is_cn && !empty($destination->city_zh)) ? ($destination->city . '(' . $destination->city_zh . ')') : $destination->city;
                $state = ($is_cn && !empty($destination->state_zh)) ? ($destination->state . '(' . $destination->state_zh . ')') : $destination->state;
                array_push($custom_fields, ['id' => 10, 'content' => $city], ['id' => 32, 'content' => $state]);
            }

            if (isset($fields['search_info'])) {
                $search_info = $fields['search_info'];
                $params = 'checkin=' . $search_info['checkin'] . '&checkout=' . $search_info['checkout'] . '&roomCount=1&adultCount1=2&childCount1=0';
                array_push($custom_fields,
                    ['id' => 18, 'content' => $lang],
                    ['id' => 79, 'content' => strpos($search_info['destinationId'], 'D') === false && strpos($search_info['destinationId'], 'H') === false ? 'D' . $search_info['destinationId'] : $search_info['destinationId']],
                    ['id' => 67, 'content' => $search_info['checkin']],
                    ['id' => 69, 'content' => $search_info['checkout']],
                    ['id' => 71, 'content' => $params],
                    ['id' => 89, 'content' => $fields['domain']]
                );
            }

            $hotels = $this->groupHotels($this->hotels);
            if (count($hotels) === 3) { // has rec hotels in cache
                $hotel1 = $hotels[0];
                $hotel2 = $hotels[1];
                $hotel3 = $hotels[2];

                array_push($custom_fields, [
                    'id' => 101,
                    'content' => $hotel1['currency'] . ' ' . $hotel1['minPrice']
                ], [
                    'id' => 103,
                    'content' => $hotel2['currency'] . ' ' . $hotel2['minPrice']
                ], [
                    'id' => 105,
                    'content' => $hotel3['currency'] . ' ' . $hotel3['minPrice']
                ]);

                unset($hotel1, $hotel2, $hotel3);
            }
            $custom_fields[] = [
                'id' => 99,
                'content' => count($hotels) === 3 ? 1 : 0
            ];

            $custom_fields[] = [
                'id' => 95,
                'content' => count($hotels) === 3 ? 1 : 0
            ];

            $custom_fields[] = [ 'id' => 107, 'content' => '0'];

            $contact = $infusionsoft->updateContact($this->contact, [], $custom_fields);

            if (isset($contact->id)) {
                if (count($this->goals) > 0) {
                    $infusionsoft->achieveGoals($contact->id, $this->goals);
                }
                if (count($this->tags) > 0) {
                    $infusionsoft->applyTags($contact->id, $this->tags);
                }
            }
        } catch (\Exception $exception) {
            Log::error('Update infusionsoft contact job failed, ' . $exception->getMessage());
        }
    }

    private function groupHotels($hotels)
    {
        $recHotels = [];
        if (count($hotels) >= 3) {
            $hotelsGroup = collect($hotels)->groupBy('rating')->toArray();

            if (count($hotelsGroup) >= 3) {
                ksort($hotelsGroup);
                $hotelsGroup = array_slice($hotelsGroup, -3, 3);
                foreach ($hotelsGroup as &$group) {
                    $group = collect($group)->sortBy('minPrice')->toArray();
                }
                $recHotels[] = $hotelsGroup[0][0];
                $recHotels[] = $hotelsGroup[1][0];
                $recHotels[] = $hotelsGroup[2][0];
            }
        }

        return $recHotels;
    }
}
