<?php

namespace App\Jobs;

use App\Classes\InfusionsoftAPI;
use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class UpdateInfusionHotelsData extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $contact;

    public $fields;

    public $hotel;

    public $hotels;

    public $goals;

    public $tags;

    /**
     * Create a new job instance.
     *
     * @param $contact
     * @param $fields
     * @param $hotel: main hotel
     * @param $hotels: recommend hotels
     * @param $goals
     * @param $tags
     */
    public function __construct($contact, $fields, $hotel, $hotels, $goals, $tags)
    {
        $this->contact = $contact;
        $this->fields = $fields;
        $this->hotel = $hotel;
        $this->hotels = $hotels;
        $this->goals = $goals;
        $this->tags = $tags;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //update infusionsoft contact info
        $fields = $this->fields;
        $hotel = $this->hotel;

        try {
            //创建或更新infusionsoft上contact信息
            $infusionsoft = new InfusionsoftAPI();
            $lang = $fields['language'];
            $is_cn = $lang === 'cn';

            $default_fields = [];
            $custom_fields = [];
            if (isset($fields['firstname'])) {
                $default_fields['family_name'] = $fields['lastname'];
                $default_fields['given_name'] = $fields['firstname'];
                $custom_fields[] = ['id' => 30, 'content' => $fields['phone']];
                $custom_fields[] = ['id' => 91, 'content' => $fields['display_hotelId']];
                $custom_fields[] = ['id' => 89, 'content' => $fields['domain']];
            }

            if (isset($hotel)) {
                array_push($custom_fields,
                    ['id' => 38, 'content' => ($is_cn && !empty($hotel->name_zh)) ? ($hotel->name . '(' . $hotel->name_zh . ')') : $hotel->name],
                    ['id' => 87, 'content' => $hotel['rating']],
                    ['id' => 93, 'content' => config('constants.image_host') . (stripos($hotel['thumbnail'], '_Image') === false ? 'Hotel_Image/' : '') . $hotel['thumbnail']]
                );
            }

            $hotels = $this->hotels;
            if (count($hotels) >= 3) { // has rec hotels in cache
                $hotel1 = $hotels[0];
                $hotel2 = $hotels[1];
                $hotel3 = $hotels[2];

                array_push($custom_fields, [
                    'id' => 81,
                    'content' => $hotel1['hotelId']
                ], [
                    'id' => 49,
                    'content' => $is_cn && !empty($hotel1['name_zh']) ? $hotel1['name'] . '(' . $hotel1['name_zh'] . ')' : $hotel1['name']
                ], [
                    'id' => 51,
                    'content' => $hotel1['currency'] . ' ' . $hotel1['minPrice']
                ], [
                    'id' => 53,
                    'content' => $hotel1['rating']
                ], [
                    'id' => 73,
                    'content' => $hotel1['thumbnail']
                ], [
                    'id' => 83,
                    'content' => $hotel2['hotelId']
                ], [
                    'id' => 55,
                    'content' => $is_cn && !empty($hotel2['name_zh']) ? $hotel2['name'] . '(' . $hotel2['name_zh'] . ')' : $hotel2['name']
                ], [
                    'id' => 57,
                    'content' => $hotel2['currency'] . ' ' . $hotel2['minPrice']
                ], [
                    'id' => 59,
                    'content' => $hotel2['rating']
                ], [
                    'id' => 75,
                    'content' => $hotel2['thumbnail']
                ], [
                    'id' => 85,
                    'content' => $hotel3['hotelId']
                ], [
                    'id' => 61,
                    'content' => $is_cn && !empty($hotel3['name_zh']) ? $hotel3['name'] . '(' . $hotel3['name_zh'] . ')' : $hotel3['name']
                ], [
                    'id' => 63,
                    'content' => $hotel3['currency'] . ' ' . $hotel3['minPrice']
                ], [
                    'id' => 65,
                    'content' => $hotel3['rating']
                ], [
                        'id' => 77,
                        'content' => $hotel3['thumbnail']
                    ]
                );

                unset($hotel1, $hotel2, $hotel3);
            }

            $custom_fields[] = [
                'id' => 95,
                'content' => count($hotels) >= 3 ? 1 : 0
            ];

            $custom_fields[] = [ 'id' => 107, 'content' => '0'];

            $contact = $infusionsoft->updateContact($this->contact, $default_fields, $custom_fields);

            if (isset($contact->id)) {
                if (count($this->goals) > 0) {
                    $infusionsoft->achieveGoals($contact->id, $this->goals);
                }
                if (count($this->tags) > 0) {
                    $infusionsoft->applyTags($contact->id, $this->tags);
                }
            }
        } catch (\Exception $exception) {
            Log::error('Update infusionsoft contact job failed, ' . $exception->getMessage());
        }
    }

}
