<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\Order;
use App\Traits\ApiPostTrait;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SendReminderCancelActionEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, ApiPostTrait;

    public $data;

    /**
     * SendReminderCancelActionEmail constructor.
     * @param $data
     */

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job for ebooking new cancel.
     * 3 days later from config or env email-delay
     * @return void
     */
    public function handle()
    {
        Log::info('Remainder Email Job Array--------------------' . json_encode($this->data));
        $json_data = $this->data;
        $id = $json_data['orderId'];
	    Log::info('Remainder Email Job orderID--------------------' . $id);
        $order = Order::find($id);
        $cancelConfirmed = $order->isConfirm_eb; //0 is not confirm yet, 1 is confirmed, 2 is confirm cancelled
        if($cancelConfirmed !== 1){
            Log::info('No Remainder Email Job needed, already confirmed by hotel '. $id);
        }else{
            try {
                $this->post('/sendCancelledEmail', $json_data);
            }catch(\Exception $exception){
                Log::error('send remainder email failed' . $exception->getMessage());
            }
        }
    }
}
