<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendAlertEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $receiver =''; // array of emails or just single email
    public $subject = '';
    public $template = null; // the template you use, or null if you use raw email
    public $content = null; // raw email content, or null if you use template
    public $data = []; // optional, the data you used in your template

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($receiver, $subject, $template, $content, $data = [])
    {
        $this->receiver = $receiver;
        $this->subject = $subject;
        $this->template = $template;
        $this->content = $content;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            if (!empty($this->template)) {
                Mail::send($this->template, $this->data, function ($m) {
                    $m->to($this->receiver)
                        ->subject($this->subject);
                });
            } elseif (!empty($this->content)) {
                Mail::raw($this->content, function ($msg) {
                    $msg->to($this->receiver)
                        ->subject($this->subject);
                });
            }
        } catch (\Exception $exception) {
            Log::error('send alert email failed, ' . $exception->getMessage());
        }
    }
}
