<?php

namespace App\Jobs;

use App\Classes\InfusionsoftAPI;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class TriggerFeedback extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $email;
    public $language;
    public $domain;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $language, $domain)
    {
        $this->email = $email;
        $this->language = $language;
        $this->domain = $domain;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (config('app.enable_email_campaign')) {
            try {
                    $infusionsoft = new InfusionsoftAPI();
                    $custom_files = [['id' => 18, 'content' => $this->language], ['id' => 107, 'content' => '1'],
                        ['id' => 89, 'content' => $this->domain]];

                    $contact = $infusionsoft->createContact($this->email, $custom_files, 'new order');

                    if (!empty($contact)) {
                        $infusionsoft->achieveGoals($contact->id, ['feedback']);
                    }
                    Log::info('Sent feedback email to ' . $this->email);
            } catch (\Exception $e) {
                Log::error('Update infusionsoft booked and send feedback request failed.');
            }
        }
    }
}
