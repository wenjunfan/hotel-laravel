<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EbRoom extends Model
{
    protected $table = 'eb_hotel_room_type';

    public $timestamps =  false;

    public function hotel()
    {
        return $this->belongsTo('App\Model\Hotel', 'hotelId', 'hotelId');
    }
}
