<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticate;

class User extends Authenticate
{
    protected $table = 'partner';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'paymentType'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function creditcards()
    {
        return $this->hasMany('App\Model\Creditcard', 'partnerId');
    }

    public function statements()
    {
        return $this->hasMany('App\Model\Statement', 'partnerId');
    }

    public function distributor()
    {
        $this->hasOne('App\model\Distributor', 'partner_id');
    }
    public function orders()
    {
        $this->hasMany('App\model\Orders', 'partnerId','partner_id');
    }
}
