<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UsiComingUser extends Model
{
    protected $table = 'usi_coming_users';
}
