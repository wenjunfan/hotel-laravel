<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $table = 'hotel';

    protected $primaryKey = 'hotelId';

    protected $fillable = [
        'hotelId',
        'name',
        'name_zh',
        'rating',
        'category',
        'type',
        'chain',
        'desId',
        'thumbnail',
        'city',
        'address',
        'destinationName',
        'destinationName_zh',
        'countryName',
        'countryName_zh',
        'zipcode',
        'latitude',
        'provider', // 上游名称 HotelBeds, EBooking,
        'longitude',
        'minPrice',
        'tax',
        'currency',
        'discount',
        'originalPrice',
        'lowestFlg',
        'distance',
        'guestRating',
        'guestReviewCount'
    ];

    public function destination()
    {
        return $this->belongsTo('App\Model\Destination', 'desId', 'desId');
    }
}
