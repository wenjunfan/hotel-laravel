<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    protected $table = 'destination';

    protected $primaryKey = 'desId';

    public function country()
    {
        return $this->belongsTo('App\Model\Country', 'countryId', 'countryId');
    }
}
