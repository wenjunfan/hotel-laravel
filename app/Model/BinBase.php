<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BinBase extends Model
{
    protected $table = 'binbase';
}
