<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 4/11/2016
 * Time: 11:36 AM
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PartnerTransaction extends Model
{
    // Table:
    protected $table = 'partner_transaction';

    // primary key:
    protected $primaryKey = "id";

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['orderId', 'orderReference', 'amount', 'balance', 'description', 'partnerId', 'netPrice', 'currencyCode'];

}