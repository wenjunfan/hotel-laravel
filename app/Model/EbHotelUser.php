<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EbHotelUser extends Model
{
    protected $table = 'eb_hotel_user';
}
