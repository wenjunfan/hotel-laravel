<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Log;

class Sms extends Model
{
    protected $table = 'sms';
    protected $fillable = [
        'phone_type', // mobile or landline
        'mobile', // 手机号
        'content', //  发送内容
        'short_link', //follow short link
        'sid',  // 短信服务商返回的id
        'sent_count', // 发送次数
        'created_at',
        'updated_at',
        'type', // 类型 0:验证码 1:其他 2：eb拒单通知给alex
        'user_id', //接收短信的用户id 可为空
        'phone_num_country', //twiliow 检查返回的手机国家
    ];

    const TYPE_VERIFY = 0;
    const TYPE_OTHER = 1;

    /**
     * 发验证码
     */
    public function sendVerifyCode($name, $full_mobile, $user_id)
    {
        // 去除空格
        $full_mobile = str_replace(' ', '', $full_mobile);
        $code = rand('10000', '99999');
        $language = session('language', 0);
        if($language == 0){
            $message = '尊敬的用户，您请求的验证码是'.$code.'请勿泄露，有效期10分钟。[走四方旅游网]';
        }else{
            $message = $name.': ' .$code .' is your authorization code which expires in 10 mins';
        }
        $this->send(
            $message,
            $full_mobile,
            $user_id,
            self::TYPE_VERIFY
        );
            $this->content = $code;
            $this->save();
    }

    public function sendOldVerifyCode($name, $full_mobile, $user_id, $code)
    {
            $language = session('language', 0);
            if($language == 0){
                $message = '尊敬的用户，您请求的验证码是'.$code.'请勿泄露，有效期10分钟。[走四方旅游网]';
            }else{
                $message = $name.': ' .$code .' is your authorization code which expires in 10 mins';
            }
            $this->send(
                $message,
                $full_mobile,
                $user_id,
                self::TYPE_VERIFY
            );
                $this->content = $code;
                $this->save();
    }

    /**
     * 发预订成功短信
     */
    public function sendOrderSuccessSms($name, $full_mobile, $user_id, $code)
    {
        // 去除空格
        $full_mobile = str_replace(' ', '', $full_mobile);

        $order = Order::where('code',substr($code, strrpos($code,'/')+1))->first();
        $language = isset($order['user_language']) ? $order['user_language'] : session('language', 0);

        if($language == 0){
            $message = '尊敬的用户，您已预订成功，酒店凭证：'.$code.'。[走四方旅游网]';
        }else{
            $message = $name.': ' .$code .' is your hotel confirmation.';
        }
        $this->send(
            $message,
            $full_mobile,
            $user_id,
            self::TYPE_VERIFY
        );
        $this->content = $code;
        $this->save();
    }

    /**
     * 发短信
     */
    public function send($msg, $full_mobile, $user_id, $type)
    {
        // 去除空格
        $full_mobile = str_replace(' ', '', $full_mobile);
        try{
            $client = new Client(config('app.twilio_account_sid'), config('app.twilio_auth_token'));
            $message = $client->messages->create(
                $full_mobile, // Text this number
                [
                    'from' => config('app.twilio_number'), // From a valid Twilio number
                    'body' => $msg
                ]
            );
            $this->mobile = $full_mobile;
            $this->user_id = $user_id;
            $this->type = $type;
            $this->sid = $message->sid;
            $this->save();
            Log::info("Twilio Return SMS sid: " . $message->sid . " - " .$full_mobile . " - "  . $msg);
            return response()->json([
                'success' => true,
            ]);
        }catch(\Exception $e){
            log::error('Twilio send message error: ' . $e->getMessage());
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }
}
