<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderRoom extends Model
{
    protected $table = 'porder_room';

    public function paxes()
    {
        return $this->hasMany('App\Model\OrderPax', 'roomIndex');
    }

    public function cancelPolicies()
    {
        $policies = $this->cancelPolicies;
        if (empty($policies)) {
            return [];
        }

        $policies = json_decode($policies, true);
        if ($policies == null) {
            return [];
        }

        foreach ($policies as &$policy) {
            $policy['from'] = $this->localTime($policy['from']);
        }

        return $policies;
    }

    private function localTime($t)
    {
        $d = new DateTime($t);
        $d->setTimezone(new DateTimeZone('UTC'));

        return $d->format('Y-m-d H:i:s');
    }
}
