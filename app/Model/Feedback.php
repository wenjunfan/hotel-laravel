<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
	protected $table = 'feedback';
	//public $timestamps = false;

	protected $fillable = [
		'question', 'solution', 'isDone'
	];

	public function order()
	{
		return $this->hasOne('App\Model\Order', 'id', 'porder_id');
	}

}
