<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderPax extends Model
{
    protected $table = 'porder_pax';

    protected $fillable = [
        'firstName',
        'lastName',
        'update_at',
    ];
}
