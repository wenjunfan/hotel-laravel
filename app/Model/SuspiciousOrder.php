<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SuspiciousOrder extends Model
{
	protected $table = 'suspicious_order';
	protected $fillable = [
	    'orderReference',
        'file_url',
        'isLocked',
        'isDone',
        'isDeclined',
        'danger_sign',
        'commend',
        'isFraud',
        'sales_no'
    ];

	public function payment()
	{
		return $this->hasOne('App\Model\Payment', 'id', 'payment_id');
	}

	public function orderTem()
	{
		return $this->hasOne('App\Model\OrderTem', 'Reorderid', 'Reorderid');
	}
}