<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupon';

    protected $fillable = ['code', 'amount', 'title', 'title_zh', 'name', 'name_zh', 'hotelIds', 'start_at',
                           'expires_at', 'checkin_start_date', 'checkin_end_date', 'enabled', 'hide_image'];

    public function couponHistory()
    {
        return $this->hasMany('App\Model\CouponHistory');
    }
}
