<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RequestLog extends Model
{
    // Table:
    protected $table = 'request_response_log';

    // primary key:
    protected $primaryKey = "id";
    protected $fillable = [
        'uuid',
        'operation',
        'created_at',
    ];

    public function order()
    {
        return $this->belongsTo('App\Model\Order', 'orderReference', 'uuid');
    }

}
