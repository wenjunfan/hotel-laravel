<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class PhoneCountry extends Model
{
    protected $table = 'phone_code';
    protected $fillable = [
        'id',  // id
        'country', // 国家全称
        'country_code', //  国家
        'phone_code', // 手机国家编号
    ];
}
