<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CustomerInformation extends Model
{
    protected $table = 'customer_information';
    protected $guarded = ['id', 'creation_time', 'updated_at'];
}
