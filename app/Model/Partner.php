<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    // Table:
    protected $table = 'partner';

    // primary key:
    protected $primaryKey = "id";

    protected $hidden = ['photo', 'photoName'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                'email',
                'name',
                'password',
                'priceRatio',
                'providers',
                'updated_at',
                'balance',
                'paymentType',
                'remember_token',
                'active',
                'admin',
                'mobile_phone',
                'contact_person',
                'company_phone',
                'company_address',
                'company_name',
                'avatar',
                'api_password',
                'created_at',
                'Country_Code',
                'channel',
                'wechat',
                'tax',
                'language',
                'see_provider',
                'remark',
                'cardholder',
                'postpay_unpaid',
                'postpay_paid',
                'affiliateRate',
                'host_name',
    ];

    public function orders()
    {
        return $this->hasMany('App\Model\Order', 'partnerId', 'id');
    }
    // 分销商
    public function distributor()
    {
        return $this->hasOne('App\Model\Distributor', 'partner_id', 'id');
    }
}