<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GroupRequest extends Model
{
	protected $table = 'group_hotel_request';

	protected $fillable = [
		'destination_one', 'destination_two', 'checkin_one', 'checkout_one','checkin_two', 'checkout_two', 'groupnumber','roomnumber', 'rate', 'budget','contactname','companyname','email','phone','remark','solution','amount','rep','isDone','isFollow'
	];

	public function user()
	{
		return $this->hasOne('App\Model\User', 'id', 'rep');
	}
}
