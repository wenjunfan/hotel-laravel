<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderTem extends Model
{
	protected $table = 'porder_tem';
    //定义可以批量赋值的黑名单,定义白名单是protected $fillable
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public $timestamps = false;

    public function order()
    {
        return $this->hasOne('App\Model\Order', 'code', 'code');
    }

    public function suspicious()
    {
        return $this->hasOne('App\Model\SuspiciousOrder', 'Reorderid', 'Reorderid');
    }

    public function partner()
    {
        return $this->hasOne('App\Model\Partner', 'id', 'partnerId');
    }

    public function linkedorderid()
    {
        return $this->hasOne('App\Model\Order', 'id', 'linked_order_id');
    }
}
