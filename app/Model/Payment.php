<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Classes\PaypalNew\PaypalResultNew;

class Payment extends Model
{
    protected $table = 'payment_history';
    const UPDATED_AT = null;

    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'partnerId');
    }

    public function request()
    {
        $o = new PaypalResultNew();

        return $o->deformat($this->request);
    }
}
