<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'porder';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'partnerId', 'provider', 'orderReference', // 是否不能修改
        'ip_address', 'orderPReference', 'cancellationReference', 'hotelId', 'display_hotelId', 'hotelName',
        'hotelName_zh', 'productId', 'bookerFirstName', 'bookerLastName', 'bookerEmail', 'bookerPhone', 'checkinDate',
        'checkoutDate', 'totalPrice', 'totalNetPrice', 'totalOriginalNetPrice', 'status', 'currencyCode',
        'cancellable', 'code', 'vat', 'supplier', 'created_at', 'updated_at', 'payment_type',
        'payment_status', 'infact_cost', 'infact_cost_cn', 'remark', 'source', 'isChecked', 'isPayed', 'isBanked',
        'day_number', 'room_number', 'orderCReference', 'special_request', 'finance_remark',
        'channel_remark', 'user_email', 'source_pos', 'isReturned', 'isAffiliated', 'sales_no',
    ];

    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'partnerId');
    }

    // 分销商
    public function distributor()
    {
        return $this->hasOne('App\Model\Distributor', 'distributor_code', 'source');
    }

    public function rooms()
    {
        return $this->hasMany('App\Model\OrderRoom', 'orderReference', 'orderReference');
    }

    public function payment()
    {
        return $this->hasMany('App\Model\Payment', 'orderReference', 'orderReference');
    }

    public function hotel()
    { // should try to use displayId for hotel data to make sure user will see same hotel info as search and book
        return empty($this->display_hotelId) ? $this->hasOne('App\Model\Hotel', 'hotelId', 'hotelId') :
            $this->hasOne('App\Model\Hotel', 'hotelId', 'display_hotelId');
    }

    public function realHotel()
    { //For hotel phone number, since the display hotel id has no phone number
        return $this->hasOne('App\Model\Hotel', 'hotelId', 'hotelId');
    }

    public function voucher()
    {
        $host = ($this->http_host =='' || $this->http_host ==null) ? $_SERVER['HTTP_HOST'] : $this->http_host;
        $ssl =  in_array($host, ['usitrip.com', 'usitour.com']) ? 'https://' : 'http://';
        return ($this->code == null || $this->code == '') ? '' :$ssl.$host.'/voucher/' . $this->code;
    }

    public function orderTem()
    {
        return $this->hasOne('App\Model\OrderTem', 'orderReference', 'orderReference');
    }

    public function porder_pax()
    {
        return $this->hasOne('App\Model\OrderPax', 'orderReference', 'orderReference');
    }

    public function porder_room()
    {
        return $this->hasOne('App\Model\OrderRoom', 'orderReference', 'orderReference');
    }

    public function partner()
    {
        return $this->belongsTo('App\Model\Partner', 'partnerId');
    }

    //    获取取消日期。。。
    public function requestlog()
    {
        return $this->hasMany('App\Model\RequestLog', 'uuid', 'orderReference');
    }

    public function partner_transaction()
    {
        return $this->hasOne('App\Model\PartnerTransaction', 'orderReference', 'orderReference');
    }

    public function feedback()
    {
        return $this->hasOne('App\Model\Feedback', 'porder_id');
    }

    public function linked_id()
    {
        return $this->hasOne('App\Model\OrderTem', 'linked_order_id', 'id');
    }

    public function paxes()
    {
        return $this->hasMany('App\Model\OrderPax', 'orderReference', 'orderReference');
    }

    public function ctrip()
    {
        return $this->hasOne('App\Model\OrderCtrip', 'orderReference', 'orderReference');
    }
}
