<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SearchHistory extends Model
{
    protected $table = 'search_history';
    const UPDATED_AT = null;

    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'partnerId');
    }
}
