<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Refund extends Model
{
    protected $table = 'refunds';
    const UPDATED_AT = null;

    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'partnerId');
    }

    public function payment()
    {
        return $this->belongsTo('App\Model\Payment', 'transactionId', 'transactionId');
    }
}
