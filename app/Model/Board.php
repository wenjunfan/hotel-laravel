<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 2016/3/14
 * Time: 17:12
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    // Table:
    protected $table = 'board';

    // primary key:
    protected $primaryKey = "id";

    public $timestamps = true;

    public static function getBoardByPid($pid, $provider)
    {
        return self::where('boardPid', $pid)
                   ->where('provider', $provider)
                   ->first();
    }

    public static function getBoardByName($name, $provider)
    {
        return self::where('boardName', $name)
                   ->where('provider', $provider)
                   ->first();
    }
}