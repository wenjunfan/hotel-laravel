<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Distributor extends Model
{
    protected $table = 'distributor';
    // primary key:
    protected $primaryKey = "id";

    public function partner()
    {
        return $this->hasOne('\App\Model\Partner', 'id', 'partner_id');
    }

    public function order()
    {
        return $this->hasOne('\App\Model\Order', 'source', 'distributor_code');
    }
}
