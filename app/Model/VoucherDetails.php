<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VoucherDetails extends Model
{
    protected $table = 'voucher_details';
}
