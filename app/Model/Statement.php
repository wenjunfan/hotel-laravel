<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Statement extends Model
{
    protected $table = 'partner_transaction';

    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'partnerId');
    }
}
