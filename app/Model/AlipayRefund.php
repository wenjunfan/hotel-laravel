<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AlipayRefund extends Model
{
    protected $table = 'alipay_refund';
    const UPDATED_AT = null;

    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'partnerId');
    }

    public function order()
    {
        return $this->belongsTo('App\Model\Order', 'order_reference', 'orderReference');
    }

}
