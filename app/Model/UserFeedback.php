<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserFeedback
 * @package App\Model
 */
class UserFeedback extends Model
{
    /**
     * @var string
     */
    protected $table = 'user_feedback';
    /**
     * @var array
     */
    protected $fillable = ['user_email', 'details'];
}
