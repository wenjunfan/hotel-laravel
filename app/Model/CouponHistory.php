<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CouponHistory extends Model
{
    protected $table = 'coupon_history';

    public function coupon()
    {
        return $this->belongsTo('App\Model\Coupon');
    }

    public function hotel()
    {
        return $this->belongsTo('App\Model\Hotel', 'hotel_id', 'hotelId');
    }
}
