<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Creditcard extends Model
{
    protected $table = 'credit_card';
    const UPDATED_AT = null;

    protected $fillable = [
        'partnerId', 'number', 'cardType', 'month', 'year', 'CVC', 'firstName',
        'lastName', 'street', 'street2', 'city', 'state', 'zip', 'countryCode', 'defaultCard'
    ];
}
