<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EbUser extends Model
{
    protected $table = 'eb_user';
}
