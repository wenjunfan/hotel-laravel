<?php

/* B端和C端共有的api接口路径 */

/************** general user interaction routes **************/
Route::group(['namespace' => 'Hotel'], function () {
    Route::post('/usitrip/logout', 'HotelController@logoutUsitrip'); // todo: move to auth controller
});

/************** hotel search **************/
Route::group(['namespace' => 'Hotel'], function () {
    Route::get('/list/static', 'ListController@tmpHotels');
    Route::post('/list/filter/{sessionKey}', 'ListController@filterHotels');
    Route::post('/list/map/filter/{sessionKey}', 'ListController@filterMapHotels');
    Route::get('/hotel/suggestions/{destinationId}/{keyword}/{count?}', 'HotelController@getHotelOptions')
         ->where([
             'destinationId' => '[0-9]+[0-9]*',
             'count'         => '[1-9]+[0-9]*'
         ]);
    Route::get('/list/recommend', 'ListController@searchHotelsRecommend');
    Route::get('/list/hotel', 'ListController@searchHotel');
    Route::get('/list/scenic', 'ListController@scenicArea');
    Route::get('/hotel/new', 'HotelDetailController@searchHotel');
    Route::get('/hotel/rooms', 'HotelDetailController@searchRooms');
    Route::get('/hotel/recommend', 'HotelDetailController@recommendHotels');
    Route::group(['middleware'=>'auth'], function (){
        Route::post('/hotel/rooms/list', 'HotelDetailController@getBookingRooms');
        Route::post('/hotel/rooms/pair/cancel', 'HotelDetailController@cancelPairedRoom');
        Route::post('/hotel/rooms/pair/submit', 'HotelDetailController@submitPairedRoom');
        Route::post('/hotel/wiki/submit', 'HotelDetailController@submitHotelWiki');
    });
});

Route::post('/paypal/button', 'Hotel\BookController@pbPaymentData');

Route::post('/paypal/submit', 'Hotel\HotelController@submitPayment');

/************** book and cancel flow **************/
Route::group(['namespace' => 'Hotel'], function () {
    Route::post('/payment/issue/email', 'BookController@PaymentIssueEmail')
         ->name('payment.issue.email');
    Route::post('/reserve/{sessionKey}', 'BookController@reserve');
    Route::post('/missed-order/reserve/business/{sessionKey}/{Reorderid}', 'BookController@missedSalesReserve2B');//solve issue of url citcon not open, fast booking for  orders paid not booking
    Route::post('/order/cancel', 'HotelController@cancel');
    //cancel order api for usitrip, do not change route
    Route::get('/cancelUsiOrder', 'HotelController@cancelUsi');
    Route::post('/order/check', 'HotelController@checkReservation');
    Route::post('/rooms/check', 'BookController@checkRooms');
    Route::post('/duplicate/proceed', 'HotelController@duplicateProceed');
    Route::post('/coupon/apply', 'BookController@applyCoupon');
    Route::get('/coupon/remove', 'BookController@removeCoupon');
});

/************** citcon pay flow **************/
Route::group(['namespace' => 'Hotel'], function () {
    Route::any('/ccpay/ipn/{type}', 'CitconController@citconpayIpn');
    Route::any('/ccpay/confirm', 'CitconController@citconpayConfirm');
    // to check transaction status in order to go to place order
    Route::get('/ccpay/inquire', 'CitconController@citconpayInquire');
    Route::post('/ccpay/order', 'CitconController@placeOrder');
    Route::get('/ccpay/order/check', 'CitconController@checkOrder');
    Route::get('/continue/{reference}/{vendor}', 'CitconController@continueOrder')->middleware('lang'); // continue order view
    Route::get('/citconQr/{reference}/{vendor}/{amount}', 'CitconController@citconQr');
});