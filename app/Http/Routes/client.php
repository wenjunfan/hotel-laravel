<?php

/* C端前台路径 */

Route::pattern('c_domain', '(' . config('app.c_host') . '|' . config('app.c_en_host') . ')');

Route::group(['domain' => '{c_domain}'], function () {
    Route::group(['namespace' => 'Client'], function () {
        /************** subscribe **************/
        if (config('app.env') == 'local') {
            Route::post('/subscribe', 'SubscribeController@subscribe');
        } else {
            Route::post('/subscribe', 'SubscribeController@subscribe')
                ->middleware('throttle:3');
        }
        /************** cancel subscribe **************/
        Route::get('/subscribe/cancel/{email}', 'SubscribeController@cancel');

        /************** user fraud payment flow **************/
        Route::group(['namespace' => 'Auth'], function () {
            Route::post('/user-file/upload', 'VerifyUserController@saveFraudUserFile');
            Route::get('/card/check/{number}/{Reorderid}', 'VerifyUserController@checkCardCountry'); // send validation code and check
            Route::get('/card/validate/{number}', 'VerifyUserController@checkCardList'); // send validation code and check
            Route::post('/sms-code/send', 'VerifyPhoneController@sendVerificationCode'); // send validation code and check
            Route::get('/sms-code/verify', 'VerifyPhoneController@verifyCode');
        });
    });

    /************** not sure **************/
    Route::group(['namespace' => 'Client'], function () {
        Route::get('/dashboard', 'DashboardController@index');
        Route::resource('creditcards', 'CreditcardController', [
            'only' => [
                'index',
                'show',
                'store',
                'destroy'
            ]
        ]);
    });

    //infusionsoft token request
    Route::get('/infusionsoft/auth', 'Client\HomeController@authInfusionsoft');

    Route::get('/feedback', 'Client\FeedbackController@index')->middleware('lang');
    if (config('app.env') === 'local') {
        Route::get('/feedback/submit', 'Client\FeedbackController@submitForm');
    } else {
        Route::get('/feedback/submit', 'Client\FeedbackController@submitForm')->middleware('throttle:1,1440');
    }

    // close site notes
    Route::get('/site-notes/close', 'Client\HomeController@closeSiteNotes');
});