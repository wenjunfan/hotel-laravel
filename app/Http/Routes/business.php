<?php

/* B端前台和后台路径 */

/************** no auth routes **************/
Route::get('/group-room-request/create', 'Business\HomeController@createGroupRequest'); // 团房请求

Route::group(['namespace' => 'Admin'], function () {
    //061918 missed citcon charged not booked START
    Route::post('/business/missed-order/update', 'MissedListController@fix117bookMissList');
    Route::post('/business/missed-order/decline/{Reorderid}', 'MissedListController@declineMissedOrder2B'); //used for accountant to decline order for paid not booked only
    Route::post('/business/missed-order/amount/update', 'MissedListController@updateNewCitconAmount2B'); //update new amount for porder_tem extra paid in order to process new order for citcon paid not booked orders
    //061918 missed citcon charged not booked END

    //061918 potential order for 117book START
    Route::post('/business/potential-order/comment', 'PotentialListController@fixOrderPotentialList2B');  //COMMENT tem order
    Route::post('/business/potential-order/complete', 'PotentialListController@isDoneOrder2B');  //done tem order
    Route::post('/business/potential-order/followup', 'PotentialListController@isFollowedOrder2B'); //follow tem order
    Route::post('/business/potential-order/link', 'PotentialListController@addLinkedOrderId2B'); //add Linked Order Id
    //061918 potential order for 117book End

    Route::post('/business/order/update', 'OrderController@fixOrder');
    Route::post('/business/user/update', 'AccountController@fixUser');
    Route::post('/distributor/assign', 'AccountController@addAffiliate'); //添加分销商
    Route::post('/update-all-user-finance-records', 'AccountController@updateAllUserFinanceRecords'); //一键更新全部用户的消费总额和欠额
    Route::post('/order-channel/update', 'OrderController@fixOrderChannel');
    Route::get('/partner-info/show/{id}', 'OrderController@getPartnerInfo');//获取分銷商信息
    Route::get('/guest-name/show/{id}', 'OrderController@getGuestName');//获取入住人名字
    Route::get('/guest-name/update', 'OrderController@saveGuestName');//修改保存入住人名字
});

Route::group(['domain' => config('app.b_host'), 'namespace' => 'Business', 'middleware' => 'lang.business'], function () {
    Route::auth(); // include login, logout, reg...
    Route::post('/logoutAndRegister', 'HomeController@logoutAndRegister');
    Route::get('/logoutToRegister', 'HomeController@logoutToRegister');
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');
});

/************** non-admin routes **************/
Route::group(['domain' => config('app.b_host'), 'namespace' => 'Business', 'middleware' => 'lang'], function () {
    Route::get('/search', 'HomeController@searchView');
    Route::get('/jumpAds/{type}/{id}', 'HomeController@jumpAds'); //自动登入测试b端账号给未注册b端用户浏览网页用
    // 客户的订单管理列表
    Route::get('/orders', 'OrderController@index'); // 20180525 update
    Route::get('/orders/tours', 'OrderController@tourOrders'); // 20180525 update
    // dashoboard
    Route::get('/admin', 'DashboardController@index');
    Route::get('/dashboard', 'DashboardController@index');
    // guide
    Route::get('/guide', 'ProfileController@guideView');
    // profile
    Route::post('/resetPassword', 'ProfileController@resetPassword');
    Route::post('/avatar', 'ProfileController@updateAvatar');
    Route::get('/tours', 'DashboardController@tours');
    Route::get('/profile', 'ProfileController@index');
    Route::post('/profile/update', 'ProfileController@updateProfile');

    //partner files management page
    Route::get('/profile/files/{id?}', 'ProfileController@showAllPics');
    Route::post('/profile/files/upload', 'ProfileController@uploadCert');
    Route::post('/profile/files/remove', 'ProfileController@removeFiles');
    //migrate profile photos to s3
    Route::get('/profile/migrate', 'ProfileController@migrateFile');

    // alipay refund later change to citcon
    Route::get('/alipay/refunds', 'RefundController@alipayRefunds');

    // credit card refund todo: 这些真的是credit card refund的路径的吗？为什么请求的function名字会带有alipay
    Route::group(['prefix' => 'refunds'], function () {
        Route::get('/', 'RefundController@index');
        Route::get('/afrom/{from}/ato/{to}', 'RefundController@alipayBetween');
        Route::get('/afrom/{from}', 'RefundController@alipayFrom');
        Route::get('/ato/{to}', 'RefundController@alipayTo');
        Route::get('/from/{from}/to/{to}', 'RefundController@between');
        Route::get('/from/{from}', 'RefundController@from');
        Route::get('/to/{to}', 'RefundController@to');
    });
});

Route::group(['namespace' => 'Business', 'middleware' => 'auth'], function () {
    Route::post('/business/extra-pay/credit-card', 'ExtraPayController@CreditcardsPay');
    Route::get('/business/extra-pay/payment-type', 'ExtraPayController@paymtType');
    Route::post('/business/extra-pay/pay', 'ExtraPayController@paymtDo');
    Route::get('/business/extra-pay/{Reorderid}', 'ExtraPayController@extraPayView');

    // 發票，衹給117book用戶
    Route::get('/order/{reference}', 'OrderController@order');
});

/************** auth for admin routes **************/
Route::group(['domain' => config('app.b_host'), 'prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {
    Route::get('/dashboard', 'DashboardController@index');
    // url: /admin/orders/* admin 订单管理
    Route::get('orders', 'OrderController@index');

    Route::resource('/accounts', 'AccountController', ['only' => ['index', 'update']]);

    Route::get('/missed-lists', 'MissedListController@index');

    /************** 到账管理New 20180627 May **************/
    Route::get('/order/list', 'CheckAllOrderController@index')->name("admin-check-order");
    Route::post('/order/comment/accounting', 'CheckAllOrderController@addAccountingComment');
    Route::post('/order/update/banked', 'CheckAllOrderController@updateIsBanked');
    Route::post('/order/update/paid', 'CheckAllOrderController@updateIsPayed');
    Route::post('/order/update/checked', 'CheckAllOrderController@updateIsChecked');
    Route::post('/order/update/returned', 'CheckAllOrderController@updateIsReturned');
    Route::post('/order/update/affiliated', 'CheckAllOrderController@updateIsAffiliated');

    /************** groupRequest **************/
    Route::group(['prefix' => 'groupRequests'], function () {
        Route::get('/', 'GroupRequestController@index');
        Route::get('/isFollow/{isFollow}/from/{from}/to/{to}/processStatus/{processStatus}', 'GroupRequestController@between')->where(['isFollow' => 'all|0|1', 'processStatus' => 'all|0|1']);
        Route::get('/isFollow/{isFollow}/from/{from}/processStatus/{processStatus}', 'GroupRequestController@from')->where(['isFollow' => 'all|0|1', 'processStatus' => 'all|0|1']);
        Route::get('/isFollow/{isFollow}/to/{to}/processStatus/{processStatus}', 'GroupRequestController@to')->where(['isFollow' => 'all|0|1', 'processStatus' => 'all|0|1']);
        Route::get('/isFollow/{isFollow}/processStatus/{processStatus}', 'GroupRequestController@indexn')->where(['isFollow' => 'all|0|1', 'processStatus' => 'all|0|1']);
    });

    /************** statements **************/
    Route::group(['prefix' => 'statements'], function () {
        Route::get('/{partnerId}', 'StatementController@index')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::get('/{partnerId}/from/{from}/to/{to}', 'StatementController@between')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::get('/{partnerId}/from/{from}', 'StatementController@from')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::get('/{partnerId}/to/{to}', 'StatementController@to')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
    });

    /************** payments **************/
    Route::group(['prefix' => 'payments'], function () {
        Route::get('/{partnerId}', 'PaymentController@index')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::get('/{partnerId}/from/{from}/to/{to}', 'PaymentController@between')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::get('/{partnerId}/from/{from}', 'PaymentController@from')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::get('/{partnerId}/to/{to}', 'PaymentController@to')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
    });

    /************** refunds **************/
    Route::group(['prefix' => 'refunds'], function () {
        Route::get('/{partnerId}', 'RefundController@index')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::get('/{partnerId}/from/{from}/to/{to}', 'RefundController@between')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::get('/{partnerId}/from/{from}', 'RefundController@from')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::get('/{partnerId}/to/{to}', 'RefundController@to')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::get('/{partnerId}/afrom/{from}/ato/{to}', 'RefundController@alipay_between')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::get('/{partnerId}/afrom/{from}', 'RefundController@alipay_from')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::get('/{partnerId}/ato/{to}', 'RefundController@alipay_to')->where(['partnerId' => 'all|[1-9]+[0-9]*']);
        Route::post('/refundConfirm', 'RefundController@refundConfirm');
    });

    /************** feedbacks **************/
    Route::group(['prefix' => 'feedbacks'], function () {
        Route::get('/', 'FeedbackController@index');
        Route::get('/{provider}/processStatus/{processStatus}', 'FeedbackController@indexnn')->where(['provider' => 'all|Innstant|HotelBeds|Tourico|Expedia|Bonotel|Priceline|EBooking', 'processStatus' => 'all|[0-9]']);
        Route::get('/{provider}/from/{from}/to/{to}/processStatus/{processStatus}', 'FeedbackController@betweennn')->where(['provider' => 'all|Innstant|HotelBeds|Tourico|Expedia|Bonotel|Priceline|EBooking', 'processStatus' => 'all|[0-9]']);
        Route::get('/{provider}/from/{from}/processStatus/{processStatus}', 'FeedbackController@fromnn')->where(['provider' => 'all|Innstant|HotelBeds|Tourico|Expedia|Bonotel|Priceline|EBooking', 'processStatus' => 'all|[0-9]']);
        Route::get('/{provider}/to/{to}/processStatus/{processStatus}', 'FeedbackController@tonn')->where(['provider' => 'all|Innstant|HotelBeds|Tourico|Expedia|Bonotel|Priceline|EBooking', 'processStatus' => 'all|[0-9]']);
        Route::get('/keywords/{orderId}', 'FeedbackController@ordernn')->where(['orderId' => '[1-9]+[0-9]*']);
    });
});

// TODO 名称不规范，等待优化合并到 admin group
Route::group(['domain' => config('app.b_host'), 'middleware' => 'admin'], function () {
    Route::post('/group-room-request/update', 'Admin\GroupRequestController@fixGroupRequest');
    Route::post('/group-room-request/complete', 'Admin\GroupRequestController@isDoneOrder');
    Route::post('/group-room-request/followup', 'Admin\GroupRequestController@isFollowedOrder');
    // TODO 名称不规范，等待优化合并到refund group
    Route::get('/admin/alipay/refunds/{partnerId}', 'Admin\RefundController@alipay_index')->where(['partnerId' => 'all|[1-9]+[0-9]*']);

    //06142017 FEEDBACK start  |Bonotel|Priceline
    Route::post('/admin/feedback/update', 'Admin\FeedbackController@fixFeedback');
    Route::get('/order/show/{orderId}', 'Admin\FeedbackController@getOrder')
        ->where(['orderId' => 'all|[1-9]+[0-9]*']);
    //06142017 FEEDBACK end

});
