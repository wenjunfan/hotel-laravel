<?php

/* C端后台（Affiliate） */

/************** affiliate login **************/
Route::group(['prefix' => 'affiliate', 'namespace' => 'Client\Auth', 'middleware' => 'lang'], function () {
    // Authentication Routes...
    Route::get('login', 'AuthController@showLoginForm');
    Route::post('login', 'AuthController@login');
    Route::get('logout', 'AuthController@logout');

    // Registration Routes...
    Route::get('register', 'AuthController@showRegistrationForm');
    Route::post('register', 'AuthController@register');

    //Register Policies
    Route::get('terms-of-use', 'AuthController@termsOfUse');
    Route::get('private-policy', 'AuthController@privatePolicy');

    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'PasswordController@showResetForm'); // this token var will be the second var in function
    Route::post('password/email', 'PasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'PasswordController@reset');
});

Route::get('/login', 'Client\Auth\AuthController@showLoginForm')->middleware('lang');
Route::get('/register', 'Client\Auth\AuthController@showRegistrationForm')->middleware('lang');

Route::post('/affiliate/resetPassword', 'Affiliate\ProfileController@resetPassword');

/************** affiliate dashboard for non-admin **************/
Route::group(['namespace' => 'Affiliate'], function () {
    //protected hotel link, used for sales to redo order for client only
    Route::get('/user-file/show/{Reorderid}', 'FraudListController@showUserFileView');
    Route::post('/fraud-order/comment', 'FraudListController@fixOrderList');
    // make ajax call order id, speed time for user open listOrders
    Route::get('/fraud-order/order-id/{id}', 'FraudListController@getOrderid');
    // make ajax call more info, speed time for user open listOrders
    Route::get('/fraud-order/info/{id}', 'FraudListController@moreInfo');
    // make commend for paid not booked orders at porder_tem
    Route::post('/missed-order/comment', 'MissedListController@fixOrderMissList');
    // make commend for potential order follow up at porder_tem
    Route::post('/potential-order/comment', 'PotentialListController@fixOrderPotentialList');
    //done tem order
    Route::post('/potential-order/complete', 'PotentialListController@isDoneOrder');
    //follow tem order
    Route::post('/potential-order/followup', 'PotentialListController@isFollowedOrder');
    Route::post('/potential-order/link', 'PotentialListController@addLinkedOrderId'); //add Linked Order Id
});

/************** affiliate dashboard for admin **************/
Route::group(['prefix' => 'affiliate', 'middleware' => 'admin'], function () {
    Route::get('/', 'Affiliate\DashboardController@index');
    Route::get('dashboard', 'Affiliate\DashboardController@index');
    Route::group(['middleware' => 'account'], function () {
        Route::post('/config/update', 'Affiliate\FraudListController@changeConfig');  // 修改fraud 参数
        //    fraud 黑白名单列表
        Route::get('fraud-list', 'Affiliate\FraudListController@index');

        Route::post('/fraud-order/email/resend/{id}', 'Affiliate\FraudListController@sendSuspiciousOrderEmailAgain');
        Route::post('/sms-code/resend/{id}',
            'Affiliate\FraudListController@sendSmsAgain'); // sms sender for new order， separate function, like newOrderEmailAgain,
        Route::post('/fraud-order/blacklist/remove/{id}',
            'Affiliate\FraudListController@confirmNotFraudOrder'); //只有没退款的有机会移除黑名单列表
        Route::get('missed-lists', 'Affiliate\MissedListController@index');

    });
    Route::group(['prefix' => 'orders', 'namespace' => 'Affiliate'], function () {
        Route::get('/{source}', 'OrderController@index')->middleware('lang'); // 20180525 update
        Route::post('/cashing/{amount}', 'OrderController@cashing');
    });
    Route::group(['prefix' => 'accounts', 'namespace' => 'Affiliate'], function () {
        Route::get('/', 'AccountController@index')->middleware('account');
        Route::post('/update/{id}', 'AccountController@update'); //todo: deprecated, replaced by new one in routes.php
    });
    Route::group(['middleware' => 'lang', 'namespace' => 'Affiliate'], function () {
        Route::get('profile', 'ProfileController@profileView');
        Route::post('profile/update', 'ProfileController@updateProfile');
        Route::get('welcome', 'ProfileController@welcome');
    });
    Route::get('/deal-list', 'Affiliate\DealListController@index')->middleware('account');
    Route::get('/feedback-list', 'Affiliate\FeedbackListController@index')->middleware('account');
    Route::get('/price-ratio', 'Affiliate\PriceRatioController@index')->middleware('account');// 市场部调价

    // 调价功能 API
    Route::get('/get-price-ratio-hotels', 'Affiliate\PriceRatioController@getPriceRatioHotels'); // 获取调价酒店列表
    Route::post('/update-hotels-price-ratio', 'Affiliate\PriceRatioController@updateHotelsPriceRatio');// 更新调价酒店列表酒店的ratio

    /************** coupon resource **************/
    Route::post('/create-coupon', 'Affiliate\DealListController@store');
    Route::post('/update-coupon/{id}', 'Affiliate\DealListController@update');
    Route::post('/coupon/upload-images/{id}', 'Affiliate\DealListController@uploadImages');
});

/************** sales send order email again **************/
Route::post('/order/email/resend/{id}', 'Hotel\HotelController@againOrderEmail');

/************** tmp order processing **************/
Route::group(['namespace' => 'Hotel'], function () {
    Route::post('/potential-order/decline/{Reorderid}', 'HotelController@declineTmpOrder'); //used for user and sales to decline the fraud orders
    Route::post('/missed-order/decline/{Reorderid}', 'CitconController@declineMissedOrder'); //used for accountant to decline order for paid not booked only
    Route::post('/paypal/extra-pay/submit/{Reorderid}', 'HotelController@extraSubmitPayment'); //fraud extra pay, need update payment history, stay with other payment function at hotelController
    Route::post('/ccpay/amount/update', 'HotelController@updateNewCitconAmount');
});
Route::group(['namespace' => 'Hotel', 'middleware' => 'admin'], function () {
    Route::post('/reserve/sales/{sessionKey}/{Reorderid}', 'BookController@salesReserve');
    Route::post('/reserve/sales/missed-order/{sessionKey}/{Reorderid}', 'BookController@missedSalesReserve');//solve issue of url citcon not open, fast booking for orders paid not booking
    Route::post('/fraud-order/blacklist/add/{id}', 'HotelController@confirmFraudOrder'); //设黑名单并退款
});