<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
| 只有B端和C端共有的前台路径需要放在这里，api接口路径需放在api.php文件里
|
*/

require(app_path() . '/Http/Routes/business.php'); // b site client routes
require(app_path() . '/Http/Routes/client.php'); // c site client routes
require(app_path() . '/Http/Routes/affiliate.php'); // admin and affiliate dashboards
require(app_path() . '/Http/Routes/api.php'); // for ajax requests

// change Language
Route::match(['get', 'post'], '/lang/update', 'Hotel\LanguageController@updateLanguage');

/************** B and C shared web routes **************/
Route::group(['middleware' => 'lang'], function () {
    Route::group(['middleware' => 'distributor'], function () {
        /************** hotel search **************/
        Route::get('/{desId}/{checkin?}/{checkout?}', 'Hotel\ListController@index')
            ->where([
                'desId' => '[0-9]+'
            ]);
        Route::get('/list/{desId}', 'Hotel\ListController@index');
        Route::get('/hotel/{hotelId}', 'Hotel\HotelDetailController@index');
        Route::get('/scenic/{scenicId}/{checkin?}/{checkout?}', 'Hotel\HotelController@scenic');//for ci ci use branding scenic area - 20180313
    });

    /************** hotel search auto complete**************/
    Route::get('/destination/suggestions', 'Client\HomeController@getDHOptions'); //auto complete // 更改这个接口要通知国内，主站顶部搜索功能要搜酒店数据
    Route::get('/getDHOptions', 'Client\HomeController@getDHOptions'); //临时路由 // 功能同上，老的路由，国内修改接口地址后可删除
    Route::get('/keyword/mark/{keyword}/{filter?}', 'Client\HomeController@markKeyword');

    /************** payment flow **************/
    Route::get('/paypal/{Reorderid}', 'Hotel\PaypalController@index');

    /************** book and cancel flow **************/
    Route::get('/book/{hotelId}/{roomRef}/{passedpartnerId?}', 'Hotel\BookController@index');
    Route::get('/voucher/{code}', 'Hotel\VoucherController@index')
        ->name('voucher');
    Route::post('/saveEmailInfo', 'Hotel\BookController@saveEmailInfo');

    /************** fraud processing **************/
    Route::get('/file-upload/{Reorderid}', 'Hotel\HotelController@fileUploadView');
    Route::get('/paypal/extra-pay/{Reorderid}', 'Hotel\HotelController@extraPayView'); // suspicious fraud extra pay view
    Route::group(['namespace' => 'Hotel', 'middleware' => 'admin'], function () {
        Route::get('/hotel/fraud/reorder/{hotelId}/{id}', 'HotelDetailController@reorderHotelView'); //protected hotel link, used for sales to redo order for client only
        Route::get('/hotel/missed/reorder/{hotelId}/{Reorderid}', 'HotelDetailController@reorderMissedHotelView'); //used for accountant to redo order for paid not booked only
    });
});

/************** deal page **************/
Route::get('/deal/{code}', 'Client\DealController@index')->middleware('lang');
Route::post('/deal/price/{couponId}', 'Client\DealController@getDealPrice');

Route::group(['namespace' => 'Client', 'middleware' => ['lang', 'distributor']], function () {
    Route::get('/{desId?}', 'HomeController@index')
        ->where([
            'desId' => '[A-Z]+[0-9]+'
        ]);
    Route::get('/home', 'HomeController@index');
    Route::get('/index', 'HomeController@index');
    Route::get('/search', 'HomeController@searchView');
    Route::get('/m', 'HomeController@index');

});

/************** 301 redirects **************/
Route::get('/hotels/{desId}/{sessionKey?}', 'Hotel\ListController@redirectToList');
Route::group(['prefix' => 'm', 'namespace' => 'Hotel'], function () {
    Route::get('/list/{desId}', 'ListController@redirectToList');
    Route::get('/hotel/{hotelId}', 'HotelDetailController@redirectToHotel');
    Route::get('/book/{hotelId}/{roomRef}', 'BookController@redirectToBook');
    Route::get('/voucher/{code}', 'VoucherController@redirectToVoucher');
});

// 潜在订单管理列表
Route::group(['middleware' => 'admin'], function () {
    // c端
    Route::get('/affiliate/potential-lists', 'Affiliate\PotentialListController@index')->middleware('account'); // 20182226 Ann
    // b端
    Route::get('/admin/potential-lists', 'Admin\PotentialListController@index'); // 20182226 Ann
    Route::get('/ccpay/status/{Reorderid}', 'Affiliate\MissedListController@statusCitconPmt'); //是否citcon付钱
});

// 酒店信息更新功能
Route::group(['middleware' => 'admin', 'namespace' => 'Hotel'], function () {
    Route::get('/hotel/update/{id}', 'HotelUpdateController@index')->where('id', '[0-9]+(\.html)?');
    Route::post('/hotel/update/info', 'HotelUpdateController@updateHotelInfo');
    Route::post('/hotel/update/reviews', 'HotelUpdateController@updateHotelReviews');
    //todo: add the endpoint
    Route::post('/hotel/update/images', 'HotelUpdateController@updateHotelInfo');
    Route::post('/hotel/update/cards', 'HotelUpdateController@updateHotelInfo');
    Route::post('/hotel/update/image', 'HotelUpdateController@updateHotelImage');
});

// 测试路径
if (file_exists(app_path() . '/Http/Routes/_test.php')) {
    require(app_path() . '/Http/Routes/_test.php');
}
