<?php

namespace App\Http\Controllers\Business;

use App\Http\Controllers\Controller;
use App\Model\Order;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Business
 */
class DashboardController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();
        $today = Carbon::now();
        $monthStartDate = $today->subDays(30)->toDateTimeString();
        $yearStateDate = $today->subMonth(12)->toDateTimeString();

        /**
         * calculate last 30 days order count
         * and order's total sell price
         */
        $past30Days = Order::select(DB::raw('count(id) as count, sum(totalPrice) as price, date_format(created_at, \'%m-%d\') as day'))
            ->where('partnerId', $user->id)
            ->where('created_at', '>=', $monthStartDate)
            ->groupBy(DB::raw('date_format(created_at, \'%m-%d\')'))
            ->orderBy(DB::raw('date_format(created_at, \'%m-%d\')'), 'desc')
            ->get();

        /**
         * calculate last 12 months order count
         * and order's total sell price
         */
        $past12Months = Order::select(DB::raw('count(id) as count, sum(totalPrice) as price, date_format(created_at, \'%Y-%m\') as month'))
            ->where('partnerId', $user->id)
            ->where('created_at', '>=', $yearStateDate)
            ->groupBy(DB::raw('date_format(created_at, \'%Y-%m\')'))
            ->orderBy(DB::raw('date_format(created_at, \'%Y-%m\')'), 'desc')
            ->get();

        return view('business.dashboard', [
            'optionName' => 'dashboard',
            'past30Days' => $past30Days,
            'past12Months' => $past12Months,
        ]);
    }

    public function tours()
    {
        if (session('language', 0) != 1) {
            $token = base64_encode(Auth::user()->email);
            session(['visitedTour' => 1]);

            return view('business.tours', [
                'optionName' => 'tours',
                'src' => 'http://tour.117book.com/?b=' . $token
            ]);
        }

        return redirect('search');
    }
}
