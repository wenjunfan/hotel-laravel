<?php

namespace App\Http\Controllers\Business;

use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\User;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Model\VoucherDetails;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

/**
 * Class OrderController
 * @package App\Http\Controllers\Business
 */
class OrderController extends Controller
{
    protected $creationStart; // 订单创建 开始时间
    protected $creationEnd; // 订单创建 结束时间
    protected $checkinStart; // 入住 开始日期
    protected $checkinEnd; // 入住 结束日期

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

        $this->creationStart = (new Carbon('last week',
            'America/Los_Angeles'))->toDateTimeString();// 上周至今 的订单 "2018-05-21 10:56:53"
        $this->creationEnd = (new Carbon('America/Los_Angeles'))->endOfDay()->toDateTimeString();// 上个月至今 的订单 "2018-05-21 10:56:53"
    }

    /**
     * 说明：business订单管理列表
     * url: /orders/*
     * blade: /business
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $createdFrom = $request->input('from') ;
        $createdTo = $request->input('to');
        $checkinFrom = $request->input('cfrom');
        $checkinTo = $request->input('cto');
        $orderStatus = $request->input('orderStatus');
        $keywords = $request->input('keywords');
        // init createdFrom and createTo
        if(!$request->has('from')){
            $createdFrom = $this->creationStart;
        } else {
            $createdFrom = $createdFrom . ' 00:00:00';
        }
        if(!$request->has('to')){
            $createdTo = $this->creationEnd;
        } else {
            $createdTo = $createdTo  . ' 23:59:59';
        }
        if(!$request->has('orderStatus')){
            $orderStatus = 6;
        }

        if($request->has('cfrom')){
            $checkinFrom = $checkinFrom . ' 00:00:00';
        }
        if($request->has('cto')){
            $checkinTo = $checkinTo . ' 23:59:59';
        }

        $data = $this->getData($createdFrom, $createdTo, $checkinFrom, $checkinTo, $keywords, $orderStatus);
        $data['optionName'] = 'hotel orders';

        return view('business.orders', $data);
    }

    /**
     * @param $createdFrom
     * @param $createdTo
     * @param string $checkinFrom
     * @param string $checkinTo
     * @param string $keywords
     * @param $orderStatus
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getData($createdFrom, $createdTo, $checkinFrom, $checkinTo, $keywords, $orderStatus)
    {
        $user = Auth::user()->id; //只看分销商自己的
        $orders = Order::with('payment.user','rooms')
            ->where('partnerId', $user);
        if($keywords == ""){
            $orders = $orders->where('created_at', '>=', $createdFrom)
                ->where('created_at', '<=', $createdTo);

            if ($checkinFrom != '') {
                if (strpos($checkinFrom, '00:00:00') === false) {
                    $checkinFrom = $checkinFrom . ' 00:00:00';
                }
                $orders = $orders->where('checkinDate', '>=', $checkinFrom);
            }

            if ($checkinTo != '') {
                if (strpos($checkinTo, '23:59:59') === false) {
                    $checkinTo = $checkinTo . ' 23:59:59';
                }
                $orders = $orders->where('checkinDate', '<=', $checkinTo);
            }

            if($orderStatus != 6 && $orderStatus!== ""){
                switch ($orderStatus)
                {
                    case 1:
                        $orders = $orders->where('status', 'ON REQUEST');
                        break;
                    case 2:
                        $orders = $orders->where('status', 'CONFIRMED');
                        break;
                    case 3:
                        $orders = $orders->where('payment_status', 'PENDING')->where('status', 'CANCELLED');
                        break;
                    case 4:
                        $orders = $orders->where('payment_status', '<>', 'PENDING')->where('status', 'CANCELLED');
                        break;
                    case 5:
                        $orders = $orders->where('status', 'FAILED');
                        break;
                    default:
                }
            }
        }
        else {
            // order id 不为空 忽略其他所有条件
            $orders = $orders->where('id', $keywords);
            $createdFrom = "";
            $createdTo = "";
            $checkinFrom = "";
            $checkinTo = "";
        }

        try {
            //start save info for post pay user paid sum and unpaid sum, also pass to db
            $sum_original_paid = Order::where('partnerId', $user)->where('isChecked', 1)->sum('totalPrice');
            $sum_returned = Order::where('partnerId', $user)->Where('isReturned', 1)->sum('totalPrice');
            $sum_paid = $sum_original_paid - $sum_returned;
            $sum_allConfirmed = Order::where('partnerId', $user)->where('status', 'CONFIRMED')->sum('totalPrice');
            $sum_unpaid=$sum_allConfirmed-$sum_paid;
            session([
                'balance_paid' => $sum_paid,
                'balance_unpaid' => $sum_unpaid
            ]);
            $partnerTable = USER::where('id',$user)->get()->first();
            $partnerTable->balance = $sum_allConfirmed;
            $partnerTable->postpay_paid = $sum_paid;
            $partnerTable->postpay_unpaid = $sum_unpaid;
            $partnerTable->update();
            log::info("Auth user ".$user." update paid balance" .$sum_paid.  ", unpaid balance" .$sum_unpaid);
        }
        catch(\Exception $e) {
            log::error('failed to update paid and unpaid for postpay user: '. $user .'reason: ' . $e->getMessage());
        }

        $orders = $orders->orderBy('id', 'desc')
            ->offset(0)
            ->limit(1000)
            ->get();

        $count = $orders->count(); // 返回数量，max: orderLimit
        $from = substr($createdFrom, 0, 10);
        $to = substr($createdTo, 0, 10);
        $cfrom = substr($checkinFrom, 0, 10);
        $cto = substr($checkinTo, 0, 10);

        return compact([
            'orders', 'from', 'to', 'cfrom', 'cto', 'orderStatus', 'keywords', 'count',
        ]);
    }

    /**
     * @param $reference
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function order($reference)  //invoice
    {
        $order = Order::with('payment.user','rooms.paxes','hotel.destination')->where('orderReference', $reference)->first();
        if ($order == null) {
            abort(404);
        }
        $numbers = VoucherDetails::where('orderReference', $reference)->first()->rooms == null? 1: VoucherDetails::where('orderReference', $reference)->first()->rooms ;

        return view('order', [
            'optionName' => 'order',
            'order' => $order,
            'numbers' => $numbers
        ]);
    }

    /**
     * show tour orders management dashboard
     */
    public function tourOrders()
    {
        if (session('language', 0) != 1) {
            if(empty(session('visitedTour'))) {
                return redirect('/tours');
            }
            $token = base64_encode(Auth::user()->email);

            return view('business.tours', [
                'optionName' => 'tour orders',
                'src' => 'http://tour.117book.com/index.php?mod=account_history&language_code=gb2312&b=' . $token
            ]);
        }

        return redirect('orders');
    }
}
