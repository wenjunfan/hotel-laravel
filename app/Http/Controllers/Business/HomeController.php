<?php

namespace App\Http\Controllers\Business;

use App\Model\User;

use App\Model\GroupRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; //for group Request
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail; //Mail group Request, full fill later

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


use Exception;

/**
 * Class HomeController
 * @package App\Http\Controllers\Business
 */
class HomeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (preg_match('/MSIE\s(?P<v>\d+)/i', @$_SERVER['HTTP_USER_AGENT'], $IE) && $IE['v'] < 10) {
            // Browsers IE 10 and below
            Log::info('Browsers IE 10 and below show alert---------------------' .$_SERVER['HTTP_USER_AGENT']);
            return view('errors.ieLow');
        }

        return view('business.home');
    }


    /**
     * @return mixed
     */
    private function isMobileDevice()
    {
        // use 3rd party package to detect device May 20180220
        return \Agent::isMobile();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function searchView()
    {
        if (Auth::check()) {
            if (Auth::user()->active) {
                if ($this->isMobileDevice()) {
                    return view('mobile.home', [
                        'optionName' => 'search'
                    ]);
                }else{
                    return view('business.search', [
                        'optionName' => 'search'
                    ]);
                }
            } else {
                return view('business.profile', [
                    'optionName' => 'profile'
                ]);
            }
        } else {
            return redirect('/login');
        }
    }

    /**
     * 自动登入广告测试用户，并导向酒店列表页
     * @param $type
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function jumpAds($type, $id){
        $user = User::find(config('app.2b_ad_partner_id'));
        if (Auth::check()) {
            return redirect('/'.$type.'/' . $id.'.html');
        }else{
            try{
                Auth::login($user);
                if (Auth::check()) {
                    // The user is active, not suspended, and exists.
                    return redirect('/'.$type.'/' . $id.'.html');
                }
            }catch(Exception $exception){
                Log::info(\Request::ip() . ':Failed to open ad page');
            }
        }
        return redirect('/'.$type.'/' . $id.'.html');
    }

    /**
     *  mobile对所有已登入的要注册的先登出
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logoutToRegister(){
        Auth::logout();
        return redirect('/register');
    }

    public function logoutAndRegister(Request $request){
        $password =  $request->input('password');
        $c = $c1 = $c2 =  $c3 = $c4 =0;

        for ($i = 0; $i < strlen($password); $i++) {
            if ((ord(substr($password, $i, 1)) >= 48 && ord(substr($password, $i,
                        1)) <= 57) && (0 == $c1)) {
                $c = $c + 1;
                $c1 = 1;
            }
            if ((ord(substr($password, $i, 1)) >= 65 && ord(substr($password, $i,
                        1)) <= 90) && (0 == $c2)) {
                $c = $c + 1;
                $c2 = 1;
            }
            if ((ord(substr($password, $i, 1)) >= 97 && ord(substr($password, $i,
                        1)) <= 122) && (0 == $c3)) {
                $c = $c + 1;
                $c3 = 1;
            }
            if ((ord(substr($password, $i, 1)) >= 33 && ord(substr($password, $i,
                        1)) <= 47) && (0 == $c4)) {
                $c = $c + 1;
                $c4 = 1;
            }
            if ((ord(substr($password, $i, 1)) >= 58 && ord(substr($password, $i,
                        1)) <= 64) && (0 == $c4)) {
                $c = $c + 1;
                $c4 = 1;
            }
        }
        $lang = session('language', 0); //0 cn 1 en
        $error ='Something went wrong, please check your input and submit the form again.'; //前面检查过应该用不到
        if ($c < 2) {
            if($lang == 1){
                $error = 'Password Rule: Contains 8+ characters with 2+ of the following: a-z, A-Z, 0-9, symbols.';
            }else{
                $error = '密码规则：至少8位，区分大小写，字母、数字、特殊符号中的两种或两种以上. ';
            }
        }

        $validator = Validator::make( $request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|between:3,64|email|unique:partner,email',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|same:password|min:8'
        ]);

        if ($validator->fails() || \Agent::isRobot()) {
            return response()->json([
                'success' => false,
                'message' => $error
            ]);
        }else{
            $langId = 0;
            if ($lang == 0) {
                $CR = '117book客服';
                $subject = '欢迎加入117book要趣订';
                $enAgg = '用户协议英文正本.pdf';
            } else {
                $langId = 1;
                $CR = 'Customer Support';
                $subject = 'Welcome to 117book';
                $enAgg = 'User Agreement.pdf';
            }

            try {
                $receiverEmail = Input::get('email');
                Mail::queue('emails.117book_newUser', ['NewUserName' => $request['name'], 'langId' => $langId],
                    function ($message) use ($CR, $subject, $lang, $enAgg, $receiverEmail) {
                        $En = storage_path('agreements/UserAgreementEn.pdf');
                        $Cn = storage_path('agreements/UserAgreementCn.pdf');
                        $message->to($receiverEmail)
                            ->cc('bd@117book.com', $CR)
                            ->bcc('bd@usitrip.com', 'Jimmy BD')
                            ->bcc('181@usitrip.com', 'Allen BD')
                            ->subject($subject);
                        $message->attach($En, ['as' => $enAgg, 'mime' => 'application/pdf']);
                        if (preg_match("/zh/i", $lang == 0)) {
                            $message->attach($Cn, ['as' => '用户协议中文译本.pdf', 'mime' => 'application/pdf']);
                        }
                    });
                Log::info("send New User Email successfully");
            } catch (\Exception $e) {
                Log::info("not send New User Email" . $e);
            }

            $user_data = [
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'active' => 1,
                'Country_Code' => 'CN',
                'host_name' => 'www.117book.com',
                'remark' => '从首页广告点进来看价格后注册直接激活的用户，需要跟进',
            ];
            $oldUser = User::where('email', $request['email'])->first();
            if($request['password'] === $request['password_confirmation'] && $oldUser === NULL){
                User::insertGetId($user_data);
                $userId = User::where('email', $request['email'])->first();
                Auth::login($userId, true);
                return response()->json([
                    'success' => true,
                    'message' => $request['link']
                ]);
            }else{
                if($request['password'] !== $request['password_confirmation']){
                    $error = 'Please enter same password to register'; //前面检查过应该用不到
                }elseif($oldUser !== NULL){
                    if($lang == 1){
                        $error = '(' .$request['email']. ') already has an account with us';
                    }else{
                        $error = '(' .$request['email']. ') 已经是我们的用户了';
                    }
                }
                return response()->json([
                    'success' => false,
                    'message' => $error
                ]);
            }
        }
    }

    /**
     * 团房请求
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createGroupRequest(Request $request)
    {
        $clientIP = \Request::ip();
        $groupRequest = new GroupRequest;
        //write group request
        $groupRequest->fill($request->all());
        $validator = Validator::make($request->all(), [
            'destination_one' => 'required|min:2',  //目的地城市一
            'checkin_one'     => 'required|date',//目的地城市一入住时间
            'checkout_one'    => 'required|date',//目的地城市一退房住时间
            'destination_two' => 'max:50', //目的地城市二
            'checkin_two'     => 'date',//目的地城市二入住时间
            'checkout_two'    => 'date',//目的地城市二退房住时间
            'phone'           => 'required|min:10',//联系人电话
            'email'           => 'required|email', //联系人邮箱
            'contactname'     => 'required|min:2', //联系人姓名
            'roomnumber'      => 'required|min:2', //每晚的客房数量
            'budget'          => 'required|min:2', //理想的预算
            'rate'            => 'required|min:2', //理想的星级
        ]);

        Log::info('IP:' . $clientIP . Input::get('phone'). ':has a new group request');
//        Log::info('IP:' . $clientIP . 'Details:' . $request);
        if ($validator->fails() || \Agent::isRobot()) {
            Mail::raw($groupRequest, function ($message) {
                $message->subject('Robot Group Request, maybe ads');
                $message->to(config('staff-emails.ann'));
            });
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong, please check your input and submit the form again.'
            ]);
        }else{
            try {
                $groupRequest->save();
            } catch (Exception $e) {
                Log::error('save group request error: ' . $e->getMessage());
            }
            if (trim($request['destination_one']) != "") {
                //发给用户
                Mail::send('emails.117book_group_hotel_request', [
                    'destinationOne' => Input::get('destination_one'),
                    'checkinOne' => Input::get('checkin_one'),
                    'checkoutOne' => Input::get('checkout_one'),
                    'destinationTwo' => Input::get('destination_two'),
                    'checkinTwo' => Input::get('checkin_two'),
                    'checkoutTwo' => Input::get('checkout_two'),
                    'groupCode' => Input::get('groupnumber'),
                    'roomsPerNight' => Input::get('roomnumber'),
                    'starRating' => Input::get('rate'),
                    'nightBudget' => Input::get('budget'),
                    'fullName' => Input::get('contactname'),
                    'companyName' => Input::get('companyname'),
                    'email' => Input::get('email'),
                    'phone' => Input::get('phone'),
                    'comments' => Input::get('remark'),
                ], function ($message) {
                    $message->subject('New 117book Group Request!!');
                    $message->to(Input::get('email'),Input::get('contactname'));
                    $message->bcc(config('staff-emails.ann'));
                    if( config('app.env') === 'production'){
                        $message->cc('bd@117book.com')->bcc('hotels@117book.com');
                    }
                });
                return response()->json([
                    'success' => true,
                    'message' => 'Our customer representative will be in touch soon.'
                ]);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Destination can not be null, please write some destinations down and submit the form again.'
                ]);
            }
        }
    }
}
