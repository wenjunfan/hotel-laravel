<?php

namespace App\Http\Controllers\Business;

use App\Http\Controllers\Controller;
use App\Model\Partner;
use App\Traits\UploadFile;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Business
 */
class ProfileController extends Controller
{
    use UploadFile;

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('business.profile', ['optionName' => 'profile']);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAvatar(Request $request)
    {
        $user = Auth::user();

        if ($user && $request->has('avatar')) {
            $user->avatar = $request->input('avatar');
            $user->save();

            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'fail', 'message' => session('language') == 0 ? '您没有登录或者没有选择头像！' : session('language') == 1 ?'Auth Failed or Profile Img null' : '您未登入或者沒有選擇頭像']);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateProfile(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            return redirect('/profile')->with('message',session('language') == 0 ?  '更新失败，您没有登录！': session('language') == 1 ?'Please Login first to update' : '更新失敗,您未登入');
        }

        $rules = [
            'name'           => 'required',
            'contact_person' => 'required|min:2',
            'mobile_phone'   => 'required|min:3',
            'channel'        => 'required|min:3',
        ];

        if ($user->email != $request->input('email')) {
            $rules['email'] = 'email|unique:users';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/profile')
                ->withErrors($validator)
                ->withInput();
        }

        $user->name = $request->input('name');
        $user->company_name = $request->input('company_name');
        $user->company_address = $request->input('company_address');
        $user->company_phone = $request->input('company_phone');
        $user->contact_person = $request->input('contact_person');
        $user->mobile_phone = $request->input('mobile_phone');
        $user->channel = $request->input('channel');
        $user->tax = $request->input('tax');
        $user->wechat = $request->input('wechat');

        try {
            $user->save();
        } catch (\Exception $e) {
            Log::error("update business profile failed, " . $e->getMessage());
            return redirect('profile')->with([
                'success' => false,
                'message' =>  session('language') == 1 ?'Something went wrong' :'更新失败'
            ]);
        }

        return redirect('profile')->with([
            'success' => true,
            'message' =>  session('language') == 1 ?'Update Successfully' :'更新成功'
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function resetPassword(Request $request)
    {
        Auth::logout();

        return redirect('/password/reset');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function guideView()
    {
        return view('business.guide', ['optionName' => 'guide']);
    }

    /*
     * show all files for a partner
     * only allow view personal files or view others' if is admin 1 or 3
     */
    public function showAllPics($id = null)
    {
        if ($id) {
            $user = Partner::find($id);
        } else {
            $user = Auth::user();
        }

        if ($user->id === Auth::user()->id || in_array(Auth::user()->admin, [1,2, 3])) {
            $files = [];
            if ($user->files) {
                $files = explode(',', $user->files);
                $files = array_values($files);
                $files = array_chunk($files, 4);
            }

            return view('business.all-files', compact('files', 'id'));
        } else {
            return redirect('profile')->with([
                'type' => 'error',
                'message' => session('language') == 0 ? '没有权限' : session('language') == 1 ?'Auth Failed' : '沒有權限'
            ]);
        }
    }

    /*
     * upload new file for a partner
     * only allow manipulate personal files or others' if is admin 1 or 3
     */
    public function uploadCert(Request $request)
    {
        if ($request->has('id')) {
            $id = $request->input('id');
            $user = Partner::find($id);
        } else {
            $user = $request->user();
        }

        if ($user->id === Auth::user()->id || in_array(Auth::user()->admin, [1, 3])) {
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $file_type = $file->getMimeType();

                if (!($file->isValid() && $file->getSize() <= 2000000)) {
                    return response()->json([
                        'success' => false,
                        'errorMessage' => session('language') == 0 ? '文件太大超过1.9M' : session('language') == 1 ?'Size too big exceed 1.9M' : '文件太大超過1.9M'
                    ]);
                } elseif (!(strrpos($file_type, 'image') !== false || $file_type === 'application/pdf')) {
                    return response()->json([
                        'success' => false,
                        'errorMessage' => session('language') == 0 ? '请检查文件类型' : session('language') == 1 ?'Invalid File' : '請檢查文件類型'
                    ]);
                } else {
                    //upload business files to s3 storage
                    $fileOriginalName = $file->getClientOriginalName();
                    $count = substr_count($user->files, $fileOriginalName);
                    if ($count > 0) {
                        $fileNameArr = explode('.', $fileOriginalName);
                        $type = array_pop($fileNameArr);
                        $fileNameNew = implode('_', $fileNameArr) . ($count + 1);
                        $fileOriginalName = $fileNameNew . '.' . $type;
                    }
                    $url = $this->uploadFile($file, 'b2b/' .$user->id. '_'. $fileOriginalName);
                    if ($url) {
                        $user->files = $user->files ? $user->files . ',' . $url : $url;
                    }
                }

                try {
                    $user->save();

                    return response()->json([
                        'success' => true,
                        'message' =>session('language') == 0 ? '上传成功' : session('language') == 1 ?'Success' : '上傳文件成功'
                    ]);
                } catch (Exception $e) {
                    Log::error('upload new profile file failed, ' . $e->getMessage());

                    return response()->json([
                        'success' => false,
                        'errorMessage' => session('language') == 0 ? '上传失败' : session('language') == 1 ?'Upload File Failed' : '上傳文件失敗'
                    ]);
                }
            }

            return response()->json([
                'success' => false,
                'errorMessage' => session('language') == 0 ? '文件缺失' : session('language') == 1 ?'Missing File' : '文件缺失'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'errorMessage' =>  session('language') == 0 ? '没有权限' : session('language') == 1 ?'Auth Failed' : '沒有權限'
            ]);
        }
    }

    /*
     * remove all kinds of business files for a partner
     * only allow manipulate personal files or others' if is admin 1 or 3
    */
    public function removeFiles(Request $request)
    {
        $back_route = 'profile/files';
        if ($request->has('id')) {
            $id = $request->input('id');
            $back_route .= '/' . $id;
            $user = Partner::find($id);
        } else {
            $user = $request->user();
        }

        if ($user->id === Auth::user()->id || in_array(Auth::user()->admin, [1, 3])) {
            $selected = $request->input('file');

            if ($user->files && $selected) {
                $user_files = explode(',', $user->files);
                $new_files = array_diff($user_files, $selected);
                $user->files = implode(',', $new_files);

                $fileNames = [];
                foreach ($selected as $url) {
                    $arr = explode('/', $url);
                    $fileName = end($arr);
                    $fileNames[] = 'b2b/' . $fileName;
                }
                $this->deleteCloudFile($fileNames);

                try {
                    $user->save();

                    return redirect($back_route)->with([
                        'type' => 'success',
                        'message' => session('language') == 0 ? '删除成功' : session('language') == 1 ?'Delete Successfully' : '刪除成功'
                    ]);
                } catch (Exception $e) {
                    return redirect($back_route)->with([
                        'type' => 'error',
                        'message' => session('language') == 0 ? '删除失败' : session('language') == 1 ?'Delete Failed' : '刪除失敗'
                    ]);
                }
            }

            return redirect($back_route)->with([
                'type' => 'error',
                'message' => session('language') == 0 ? '删除失败' : session('language') == 1 ?'Delete Failed' : '刪除失敗'
            ]);
        } else {
            return back()->with([
                'type' => 'error',
                'message' => session('language') == 0 ? '没有权限' : session('language') == 1 ?'Auth Failed' : '沒有權限'
            ]);
        }
    }

    /*
     * remove file from s3 storage
     */
    private function deleteCloudFile($fileNames)
    {
        try {
            Storage::disk('s3')->delete($fileNames);
        } catch (Exception $e) {
            Log::error('Remove files from s3 failed, msg: ' . $e->getMessage() . ', files: ' . json_encode($fileNames));
        }
    }

    /*
     * migrate partner photo to aws, already done
     */
    public function migrateFile()
    {
        $failed = [];
        $users = Partner::whereNotNull('photo')->get();
        if (count($users) > 0) {
            foreach ($users as $user) {
                $fileName = 'b2b/' . $user->id . '_' . $user->photoName;
                if ($user->files && strpos(urldecode($user->files), $fileName) !== false) {
                    continue;
                }
                Storage::disk('s3')->put($fileName, $user->photo, 'public');
                $url = Storage::disk('s3')->url($fileName);
                $lastPartUrl = substr($url, strrpos($url, '/') + 1);

                $originalName = str_replace('b2b/','' ,$fileName);
                $fileUrl = str_replace($lastPartUrl,$originalName,$url);

                $user->files = $user->files ? $user->files . ',' . $fileUrl : $fileUrl;

                try {
                    $user->save();
                } catch (\Exception $e) {
                    $failed[] = $user->id;
                    Log::error('migrate user photos failed, ' . $e->getMessage());
                    continue;
                }
            }
        }

        if (count($failed) > 0) {
            Log::info('failed: ' . json_encode($failed));
        }
        return back()->with('message', 'Finished');
    }
}
