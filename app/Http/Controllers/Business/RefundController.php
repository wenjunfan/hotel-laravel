<?php

namespace App\Http\Controllers\Business;

use App\Http\Controllers\Controller;
use App\Model\AlipayRefund;
use App\Model\Refund;

use Illuminate\Support\Facades\Auth;

/**
 * Class RefundController
 * @package App\Http\Controllers\Business
 */
class RefundController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->between('', '');
    }

    /**
     * @param $from
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function between($from, $to)
    {
        $refunds = Refund::with('payment')->where('partnerId', Auth::user()->id);

        if ($from != '') {
            $refunds = $refunds->where('created_at', '>=', $from);
        }

        if ($to != '') {
            $refunds = $refunds->where('created_at', '<=', $to);
        }

        $refunds = $refunds->orderBy('created_at', 'desc')->get();

        return view('business.refunds', ['optionName' => 'refunds', 'refunds' => $refunds, 'from' => $from, 'to' => $to]);
    }

    /**
     * @param $from
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function from($from)
    {
        return $this->between($from, '');
    }

    /**
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function to($to)
    {
        return $this->between('', $to);
    }

    /**
     * @param $from
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function alipayFrom($from)
    {
        return $this->alipayBetween($from, '');
    }

    /**
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function alipayTo($to)
    {
        return $this->alipayBetween('', $to);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function alipayRefunds()
    {
        return $this->alipayBetween('', '');
    }

    /**
     * @param $from
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function alipayBetween($from, $to)
    {
		$refunds = AlipayRefund::with('order')->where('partner_id', Auth::user()->id);

        if ($from != '') {
            $refunds = $refunds->where('created_at', '>=', $from);
        }

        if ($to != '') {
            $refunds = $refunds->where('created_at', '<=', $to);
        }

        $refunds = $refunds->orderBy('created_at', 'desc')->get();

        return view('business.alipay-refunds', ['optionName' => 'alipayRefunds', 'refunds' => $refunds, 'from' => $from, 'to' => $to]);
    }
    
}
