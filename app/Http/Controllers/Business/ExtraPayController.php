<?php

namespace App\Http\Controllers\Business;

use App\Http\Controllers\Controller;

use Exception;
use App\Model\Payment;
use App\Model\User;
use Illuminate\Http\Request;
use App\Model\OrderTem;

use App\Classes\Paypal\CreditCardInfo;
use App\Classes\Paypal\Paypal;
use App\Classes\Paypal\PayResult;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/**
 * Class ExtraPayController
 * @package App\Http\Controllers\Business
 */
class ExtraPayController extends Controller
{

    const amount = 0.01;

   //needs to know who is doing the extra pay now, for group request user, no need, so command this auth for now
   // public function __construct()
   // {
   //     parent::__construct();
   //     $this->middleware('auth');
   // }

    /**
     * Only used for 117book user extra pay now. while
     * Most function related with alipay and cc pay is not been used anymore,
     * in case needs to remember card after we upgrade to https for 117book user,
     * leave here, do not delete
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function paymtType(Request $request)
    {
        if (Auth::user()->active == 2) {
            return '该账号尚未激活，请升级权限。';
        }

        return view('paymt-type', [
            'optionName' => '117book make payment',
            'amount'     => self::amount,
            '_token'     => $request->input('_token')
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paymtDo(Request $request)
    {
        try {
            if (!Auth::user()->active) {
                return response()->json([
                    'success'      => false,
                    'errorMessage' => '欢迎您使用117book要趣定酒店B2B预订平台，您现在还不能预订酒店，因为您的账户还没有激活，请到网站首页的加盟介绍查看如何加盟我们，谢谢！'
                ]);
            }

            $tempInfo = OrderTem::where('Reorderid','<>','')->get();

            $tempInfo->amount = self::amount;
            $tempInfo->currencyCode = 'USD';
            $tempInfo->Reorderid = '100100';
            $tempInfo->goods_name = '追加付款';
            $tempInfo->goods_description = '追加付款';

            $bookInfo = [
                'amount'   => $tempInfo->amount,
                'currency' => $tempInfo->currencyCode
            ];

            $user = User::with('creditcards')
                        ->find(Auth::user()->id);
            //pay:1为信用卡，2为支付宝
            if ($request->Input('tempType') == 'cc' && $user->paymentType == 'Prepay') {
                $payResult = $this->pay($bookInfo, $user);
                if (!$payResult['success'] && isset($payResult['voucherUrl'])) {
                    return response()->json([
                        'success'    => false,
                        'voucherUrl' => $payResult['voucherUrl'] . '/' . $tempInfo->Reorderid,
                        'message'    => $payResult['message'],
                    ]);
                }
            } elseif ($request->Input('tempType') == 'al') {
                $alipay = $this->alipay($tempInfo);
                return response()->json([
                    'success'    => true,
                    'voucherUrl' => $alipay,
                ]);
            }
        } catch (Exception $e) {
            Log::info($this->ip . ' ' . $e->getMessage());
        }
        return response()->json([
            'success'    => false,
            'message' => 'pay failed' ,
        ]);
    }

    /**
     * @param $result
     * @param $partnerId
     */
    private function savePaymentHistory($result, $partnerId)
    {
        $p = new Payment();

        $p->partnerId = $partnerId;
        $p->ack = $result->ack;
        $p->version = $result->version;
        $p->build = $result->build;
        $p->correlationId = $result->correlationId;
        $p->request = $result->request;
        $p->errors = $result->errors;
        $p->avsCode = $result->avsCode;
        $p->cvv2Match = $result->cvv2Match;
        $p->transactionId = $result->transactionId;
        $p->amount = $result->amount;
        $p->currency = $result->currency;

        $p->save();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function CreditcardsPay(Request $request)
    {
        $user = User::with('creditcards')
                    ->find(Auth::user()->id);
        $creditcardInfo = new CreditCardInfo($request->Input('firstName'), $request->Input('lastname'), $request->Input('cardType'), $request->Input('number'), $request->Input('month'),
            $request->Input('year'), $request->Input('CVC'), $request->Input('street'), $request->Input('street2'), $request->Input('city'), $request->Input('state'), $request->Input('zip'),
            $request->Input('countryCode'));
        //algorithm
        $number = $request->Input('number');

        settype($number, 'string');
        $sumTable = array(
            array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
            array(0, 2, 4, 6, 8, 1, 3, 5, 7, 9)
        );
        $sum = 0;
        $flip = 0;
        for ($i = strlen($number) - 1; $i >= 0; $i--) {
            $sum += $sumTable[$flip++ & 0x1][$number[$i]];
        }

        if (0 == ($sum % 10)) {
        } else {
            DB::table("error_bankcard")
              ->insert([
                  'partnerId' => Auth::user()->id,
                  'cardInfo'  => $request->Input('number')
              ]);

            return [
                'success' => false,
                'message' => 'Bank card error',
            ];

        }

        $amount = self::amount;

        $r = Paypal::auth($creditcardInfo, $amount);
        $payResultAuth = new PayResult($r['request'], $r['response']);

        Log::info($this->ip . ' extraPay payResultAuth result: ' . json_encode($payResultAuth));
        if ($payResultAuth->ack != 'Success') {
            DB::table("error_bankcard")
              ->insert([
                  'partnerId' => Auth::user()->id,
                  'cardInfo'  => $request->Input('number')
              ]);

            return [
                'success' => false,
                'message' => "auth error"
            ];
        }

        $r1['success'] = false;
        $r1 = Paypal::capture($payResultAuth->transactionId, $amount);
        Log::info($this->ip . ' extraPay paypal capture result: ' . json_encode($r1));
        if (!$r1['success']) {
            DB::table("error_bankcard")
              ->insert([
                  'partnerId' => Auth::user()->id,
                  'cardInfo'  => $request->Input('number')
              ]);

            //auth
            $rdoVoid = Paypal::doVoid($payResultAuth->authorizationId);
            $payResultdoVoid = new PayResult($rdoVoid['request'], $rdoVoid['response']);

            return [
                'success'      => false,
                'errorMessage' => '付款失败，错误代码：' . $r['errorNo'] . '，错误描述：' . $r['errorMessage'],
            ];
        }
        $payResultCapture = new PayResult($r1['request'], $r1['response']);

        $this->savePaymentHistory($payResultCapture, Auth::user()->id);
        if ($payResultCapture->ack != 'Success') {
            DB::table("error_bankcard")
              ->insert([
                  'partnerId' => Auth::user()->id,
                  'cardInfo'  => $request->Input('number')
              ]);

            return [
                'success'      => false,
                'errorMessage' => 'error:付款失败！'
            ];
        }

        $transaction = $payResultCapture;

        return response()->json([
            'success'      => true,
            'errorMessage' => '付款成功！'
        ]);
    }

    /**
     * @param $creditcards
     * @return null
     */
    private function findCreditcard($creditcards)
    {
        $r = null;
        if (empty($creditcards)) {
            return $r;
        }

        foreach ($creditcards as $c) {
            if ($r == null) {
                $r = $c;
                if ($r->defaultCard) {
                    break;
                }
            } else {
                if ($c->defaultCard) {
                    $r = $c;
                    break;
                }
            }
        }

        return $r;
    }

    /**
     * PC支付宝支付
     * @param $bookInfo
     * @return mixed
     */
    private function alipay($bookInfo)
    {
        try {
            $amount = $this->changeCurrency($bookInfo->amount, $bookInfo->currencyCode, 'CNY');
            $alipay = app('alipay.web');
            $alipay->setOutTradeNo($bookInfo->Reorderid);
            $alipay->setTotalFee($amount);
            $alipay->setSubject($bookInfo->goods_name);
            $alipay->setBody($bookInfo->goods_description);
            $alipay->setQrPayMode('5'); //该设置为可选，添加该参数设置，支持二维码支付。
        } catch (Exception $e) {
            Log::info($this->ip . ' ' . $e->getMessage());
        }

        // 跳转到支付页面。
        return $alipay->getPayLink();
    }

    /**
     * @param $amount
     * @param $currency
     * @param string $out_code
     * @return float|int
     */
    private function changeCurrency($amount, $currency, $out_code = 'USD')
    {
        try {
            $payment = self::httpGet("http://post.usitrip.com/index.php?mod=api_public&action=currencyConversion&money=$amount&source_code=$currency&out_code=$out_code");
            if ($payment === false) {
                return 0;
            }

            $payment = json_decode($payment, true);
            if ($payment['Value'] !== false) {
                return round($payment['Value'], 2);
            }
        } catch (\Exception $e) {
            Log::info($this->ip . ' ' . $e->getMessage());
        }

        return 0;
    }

    /**
     * @param $url
     * @return bool|mixed
     */
    public static function httpGet($url)
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);

            $output = curl_exec($ch);

            if (curl_errno($ch)) {
                return false;
            }

            curl_close($ch);

            return $output;
        } catch (Exception $e) {
            Log::info($e->getMessage());
        }
        return false;
    }

    /**
     * @param $Reorderid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function extraPayView($Reorderid)
    {
        //detect if order already solved
        return view('paypal-ex', [
            'optionName' => '',
            'Reorderid'  => $Reorderid,
        ]);
    }
}