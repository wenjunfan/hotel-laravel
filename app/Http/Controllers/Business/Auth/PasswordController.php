<?php

namespace App\Http\Controllers\Business\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $redirectTo = '/';
    protected $broker = 'business';

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        return view('auth.business.passwords.reset')->with(compact('token', 'email'));
    }

    public function showResetForm(Request $request, $token = null)
    {
        if (is_null($token)) {
            if (strpos($request->server('HTTP_ACCEPT_LANGUAGE'), 'en') !== false) {
                return view('auth.business.passwords.email_en');
            }
            return view('auth.business.passwords.email');
        }

        $email = $request->input('email');

        if (strpos($request->server('HTTP_ACCEPT_LANGUAGE'), 'en') !== false) {
            return view('auth.business.passwords.reset_en')->with(compact('token', 'email'));
        }
        return view('auth.business.passwords.reset')->with(compact('token', 'email'));
    }

}
