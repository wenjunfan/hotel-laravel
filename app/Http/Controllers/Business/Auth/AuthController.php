<?php

namespace App\Http\Controllers\Business\Auth;

use App\Model\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/search';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $c = 0;
        $c1 = 0;
        $c2 = 0;
        $c3 = 0;
        $c4 = 0;

        for ($i = 0; $i < strlen($data['password']); $i++) {
            if ((ord(substr($data['password'], $i, 1)) >= 48 && ord(substr($data['password'], $i,
                        1)) <= 57) && (0 == $c1)) {
                $c = $c + 1;
                $c1 = 1;
            }
            if ((ord(substr($data['password'], $i, 1)) >= 65 && ord(substr($data['password'], $i,
                        1)) <= 90) && (0 == $c2)) {
                $c = $c + 1;
                $c2 = 1;
            }
            if ((ord(substr($data['password'], $i, 1)) >= 97 && ord(substr($data['password'], $i,
                        1)) <= 122) && (0 == $c3)) {
                $c = $c + 1;
                $c3 = 1;
            }
            if ((ord(substr($data['password'], $i, 1)) >= 33 && ord(substr($data['password'], $i,
                        1)) <= 47) && (0 == $c4)) {
                $c = $c + 1;
                $c4 = 1;
            }
            if ((ord(substr($data['password'], $i, 1)) >= 58 && ord(substr($data['password'], $i,
                        1)) <= 64) && (0 == $c4)) {
                $c = $c + 1;
                $c4 = 1;
            }
        }

        if ($c < 2) {
            $data['password'] = 'error';
            $data['password_confirmation'] = 'error';
        }

        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|between:3,64|email|unique:partner,email',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|same:password|min:8'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $locale = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : '';
        $locale = substr($locale, 0, 5);

        $langId = 0;
        if (preg_match("/zh/i", $locale)) {
            $CR = '117book客服';
            $subject = '欢迎加入117book要趣订';
            $enAgg = '用户协议英文正本.pdf';
        } else {
            $langId = 1;
            $CR = 'Customer Support';
            $subject = 'Welcome to 117book';
            $enAgg = 'User Agreement.pdf';
        }

        try {
            $receiverEmail = Input::get('email');
            Mail::queue('emails.117book_newUser', ['NewUserName' => $data['name'], 'langId' => $langId],
                function ($message) use ($CR, $subject, $locale, $enAgg, $receiverEmail) {
                    $En = storage_path('agreements/UserAgreementEn.pdf');
                    $Cn = storage_path('agreements/UserAgreementCn.pdf');
                    $message->to($receiverEmail)
                        ->cc('bd@117book.com', $CR)
                        ->bcc('bd@usitrip.com', 'Jimmy BD')
                        ->bcc('181@usitrip.com', 'Allen BD')
                        ->subject($subject);
                    $message->attach($En, ['as' => $enAgg, 'mime' => 'application/pdf']);
                    if (preg_match("/zh/i", $locale)) {
                        $message->attach($Cn, ['as' => '用户协议中文译本.pdf', 'mime' => 'application/pdf']);
                    }
                });
            Log::info("send New User Email successfully");
        } catch (\Exception $e) {
            Log::info("not send New User Email" . $e);
        }

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function showLoginForm(Request $request)
    {
        if (strpos($request->server('HTTP_ACCEPT_LANGUAGE'), 'zh') === 0) {
            return view('auth.business.login');
        }
        return view('auth.business.login_en');
    }

    public function showRegistrationForm(Request $request)
    {
        if (strpos($request->server('HTTP_ACCEPT_LANGUAGE'), 'zh') === 0) {
            return view('auth.business.register');
        }
        return view('auth.business.register_en');
    }

}
