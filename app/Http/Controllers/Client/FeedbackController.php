<?php

namespace App\Http\Controllers\Client;

use App\Model\UserFeedback;
use App\Traits\IsMobileDevice;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class FeedbackController
 * @package App\Http\Controllers\Client
 */
class FeedbackController extends Controller
{
    use IsMobileDevice;

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->has('email')) {
            session(['user_email' => $request->input('email')]);

            $blade = 'feedback';

            if ($this->isMobileDevice()) {
                $blade = 'mobile.feedback';
            }

            return view($blade, ['feedback' => $request->input('feedback')]);
        } else {
            abort('404');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitForm(Request $request)
    {
        $data = $request->all();

        $email = '';
        if (session()->has('user_email')) {
            $email = session('user_email');
        }

        UserFeedback::create([
            'user_email' => $email,
            'details' => json_encode($data)
        ]);

        return response()->json([
            'success' => true
        ]);
    }
}
