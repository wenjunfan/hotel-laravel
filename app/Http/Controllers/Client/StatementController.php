<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Model\Statement;

use Illuminate\Support\Facades\Auth;

/**
 * Class StatementController
 * @package App\Http\Controllers\Client
 */
class StatementController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->between('', '');
    }

    /**
     * @param $from
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function between($from, $to)
    {
        $user = Auth::user();

        $statements = Statement::where('partnerId', $user->id);

        if ($from != '') {
            $statements = $statements->where('created_at', '>=', $from);
        }

        if ($to != '') {
            $statements = $statements->where('created_at', '<=', $to);
        }

        $statements = $statements->orderBy('created_at', 'desc')->get();


        return view('statements', ['optionName' => 'statements', 'statements' => $statements, 'balance' => $user->balance, 'from' => $from, 'to' => $to]);
    }

    /**
     * @param $from
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function from($from)
    {
        return $this->between($from, '');
    }

    /**
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function to($to)
    {
        return $this->between('', $to);
    }
}
