<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Model\Payment;

use Illuminate\Support\Facades\Auth;

/**
 * Class PaymentController
 * @package App\Http\Controllers\Client
 */
class PaymentController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->between('', '');
    }

    /**
     * @param $from
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function between($from, $to)
    {
        $payments = Payment::where('partnerId', Auth::user()->id);

        if ($from != '') {
            $payments = $payments->where('created_at', '>=', $from);
        }

        if ($to != '') {
            $payments = $payments->where('created_at', '<=', $to);
        }

        $payments = $payments->orderBy('created_at', 'desc')->get();

        return view('payments', ['optionName' => 'payments', 'payments' => $payments, 'from' => $from, 'to' => $to]);
    }

    /**
     * @param $from
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function from($from)
    {
        return $this->between($from, '');
    }

    /**
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function to($to)
    {
        return $this->between('', $to);
    }
}
