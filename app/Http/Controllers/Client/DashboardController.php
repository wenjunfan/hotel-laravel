<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Model\Order;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Client
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();
        /**
         * calculate last 30 days order count
         * and order's total sell price
         */
        $past30Days = Order::select(DB::raw('count(id) as count, sum(totalPrice) as price, date_format(created_at, \'%m-%d\') as day'))
            ->where('partnerId', $user->id)
            ->groupBy(DB::raw('date_format(created_at, \'%m-%d\')'))
            ->orderBy(DB::raw('date_format(created_at, \'%m-%d\')'), 'desc')
            ->take(30)
            ->get();

        /**
         * calculate last 12 months order count
         * and order's total sell price
         */
        $past12Months = Order::select(DB::raw('count(id) as count, sum(totalPrice) as price, date_format(created_at, \'%Y-%m\') as month'))
            ->where('partnerId', $user->id)
            ->groupBy(DB::raw('date_format(created_at, \'%Y-%m\')'))
            ->orderBy(DB::raw('date_format(created_at, \'%Y-%m\')'), 'desc')
            ->take(12)
            ->get();


        return view('dashboard', [
            'optionName' => 'dashboard',
            'past30Days' => $past30Days,
            'past12Months' => $past12Months,
        ]);
    }
}
