<?php

namespace App\Http\Controllers\Client;

use App\Classes\InfusionsoftAPI;
use App\Http\Controllers\Controller;
use App\Model\Destination;
use App\Model\UsiComingUser;
use App\Traits\IsMobileDevice;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

use Illuminate\Http\Request;

/**
 * Class HomeController
 * @package App\Http\Controllers\Client
 */
class HomeController extends Controller
{
    // traits
    use IsMobileDevice;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string $desId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $desId = '')
	{
        if (preg_match('/MSIE\s(?P<v>\d+)/i', @$_SERVER['HTTP_USER_AGENT'], $IE) && $IE['v'] < 10) {
            // Browsers IE 10 and below
            Log::info('Browsers IE 10 and below show alert---------------------' . $_SERVER['HTTP_USER_AGENT']);
            return view('errors.ieLow');
        }
        $source = $request->has('source') ? $request->get('source') : '';
        $cid = $request->has('cid') ? $request->get('cid') : '';
        $token = $request->has('token') ? $request->get('token') : '';
        if (!empty($token)) {
            $referer = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
            $actual_url = $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $this->host . $referer;
            Log::info($this->ip . ' usitrip incoming url: ' . $actual_url);

            $newToken = md5('www.usitrip.com:hotels.usitrip.com:' . $source . ':' . $cid . ':HowardChris');

            if ($token == $newToken) {
                $url = config('app.usi_endpoint') . '/index.php?mod=api_customers_info';
                $fields = [
                    'PHP_AUTH_USER' => config('app.usi_user_name'),
                    'PHP_AUTH_PW' => config('app.usi_user_pwd'),
                    'customers_id' => $cid
                ];

                $fields_string = http_build_query($fields);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, count($fields));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                $response = curl_exec($ch);
                curl_close($ch);

                Log::info($this->ip . ' request tours user info response: ' . $response);
                $response = json_decode($response, true);

                if ($response['result'] == 'success') {
                    $user = $response['data'];
                    session([
                        'user' => $user,
                        'user_email' => $user['customers_email_address']
                    ]);

                    // create infusionsoft cache
                    $subscriber = new SubscribeController();
                    $subscriber->registerInf(['email' => $user['customers_email_address'], 'pos' => 'Tours incoming users']);

                    $savedRecords = UsiComingUser::where('email', $user['customers_email_address'])->get();
                    if (count($savedRecords) == 0) {
                        $newUsiUser = new UsiComingUser();
                        $newUsiUser->email = $user['customers_email_address'];
                        $newUsiUser->save();
                    }
                }
            }
        }

        $desName = '';
        if (strpos($desId, 'D') !== false) {
            $destination = Destination::where('desId', str_replace('D', '', $desId))
                ->first();
            if (!empty($destination)) {
                $desName = $destination->desName;
            }
        } else {
            $desId = '';
        }

        $data = [
            'optionName' => 'home',
            'desId' => $desId,
            'desName' => $desName,
        ];

        $isMobile = $this->isMobileDevice();

        if ($isMobile) {
            return view('mobile.home', $data); // new version mobile blade
        }

        return view('home', $data);


    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getDHOptions(Request $request)
    {
        $keyword = urldecode($request->input('keyword'));
        $fields = http_build_query([
            'input' => $keyword,
        ]);
        $getUrl = config('app.usi_search_api_address') . '?' . $fields;
        $options = $this->curlGet($getUrl);

        if (isset($options['result'])) {
            return response()->json($options['result']);
        } else {
            return [];
        }
    }

    /**
     * @param $keyword
     * @param null $filter
     */
    public function markKeyword($keyword, $filter = null)
    {
        $ip = $this->ip;
        if ($ip === '69.75.227.206' || $ip === '76.79.234.181') { // escape company wide ip address
            return;
        }

        $fields = http_build_query([
            'input' => $keyword,
        ]);

        if (empty($filter)) {
            $url = config('app.usi_search_mark_api_address') . '?' . $fields;
        } else {
            $url = config('app.usi_filter_mark_api_address') . '?' . $fields;
        }

        $options = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1
        ];
        $ch = curl_init();

        curl_setopt_array($ch, $options);
        curl_exec($ch);
        curl_close($ch);

        unset($ch, $options, $fields);

        return;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function searchView(Request $request)
    {
        if ($request->has('q')) {
            $query = $request->input('q');
            $fields = http_build_query([
                'input' => $query,
            ]);
            $getUrl = config('app.usi_search_api_address') . '?' . $fields;
            $options = $this->curlGet($getUrl);

            if (isset($options['result'])) {
                $response = $options['result'];
                unset($options);
                $option = $response[0];

                if ($option['type'] === 'destination') {
                    return redirect('/list/D' . $option['id'] . '.html');
                } elseif ($option['type'] === 'hotel') {
                    return redirect('/list/H' . $option['id'] . '.html');
                } elseif (isset($option['destination_id'])) {
                    return redirect('/list/D' . $option['destination_id'] . '.html' . '?scenicId=' . $option['id']);
                }
            }
        }

        return redirect('/');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function authInfusionsoft(Request $request)
    {
        $infusionsoft = new InfusionsoftAPI($request);
        return $infusionsoft->authenticate($request);
    }

    public function closeSiteNotes()
    {
        session(['hidenotes' => 1]);
    }

    /**
     * @param $url
     * @return mixed
     */
    private function curlGet($url)
    {
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
        ];

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, true);
    }
}
