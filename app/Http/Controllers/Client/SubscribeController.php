<?php

namespace App\Http\Controllers\Client;

use App\Classes\InfusionsoftAPI;
use App\Http\Controllers\Controller;
use App\Model\Subscriber;
use App\Traits\HotelDataRetriever;
use App\Traits\HttpGet;
use App\Traits\IsMobileDevice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * Class SubscribeController
 * @package App\Http\Controllers\Client
 */
class SubscribeController extends Controller
{
    use HttpGet, IsMobileDevice, HotelDataRetriever;

    private $UsitripUserApi = [];

    /**
     * SubscribeController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        //$this->middleware('auth');
        $this->UsitripUserApi = [
            'endpoint' => config('app.usi_endpoint'),
            'username' => config('app.usi_user_name'),
            'userpw' => config('app.usi_user_pwd'),
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribe(Request $request)
    {
        $email = $request->has('email') ? $request->input('email') : '';

        if (!empty($email)) {
            $data = $request->all();

            return $this->registerInf($data);
        } else {
            return response()->json([
                'success' => false,
                'message' => ''
            ]);
        }
    }

    /**
     * Register infusionsoft user
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerInf($data)
    {
        $email = $data['email'];
        $pos = $data['pos'];

        $langCode = session('language', '0');
        $language = ($langCode == 0) ? 'cn' : ($langCode == 2) ? 'tw':'en';
        $subscriber = Subscriber::where('email', $email)->first();

        if (config('app.enable_email_campaign')) {
            try {
                // create infusionsoft contact
                $infusionsoft = new InfusionsoftAPI();
                $contact = $infusionsoft->createContact($email, [
                    [
                        'id' => 18,
                        'content' => $language
                    ],
                    [
                        'id' => 89,
                        'content' => isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'hotel.usitrip.com'
                    ]
                ], 'new subscriber');

                // apply newsletter tag
                if (!empty($contact)) {
                    $tagId = $language == 'cn' ? 135 : 133;
                    $infusionsoft->applyTags($contact->id, [$tagId]);
                }
            } catch (\Exception $exception) {
                Log::error($this->ip . ' create infusionsoft contact failed, ' . $exception->getMessage());
            }
        }

        if (filter_var($email, FILTER_VALIDATE_EMAIL) && trim($email) != "") {
            if ($subscriber != null && $subscriber->registered == 1 && $subscriber->subscribed == 1) {
                return response()->json([
                    'success' => true,
                    'message' => 'sub01',
                ]);
            } elseif ($subscriber != null && $subscriber->registered == 1) {
                // subscribe user
                $subscriber->subscribed = 1;
                try {
                    $subscriber->save();
                } catch (\Exception $e) {
                    Log::error('save subscriber error: ' . $e->getMessage());
                }

                return response()->json([
                    'success' => true,
                    'message' => 'sub02',
                ]);
            } else {
                if ($subscriber == null) {
                    $subscriber = new Subscriber([
                        'email' => $email,
                        'ip' => $this->ip,
                        'pos' => $pos,
                        'lang' => $language,
                        'registered' => 0,
                        'subscribed' => 0
                    ]);
                }

                $checkUserRes = session()->has('user') ? session('user') : $this->checkUser($email);
                if (isset($checkUserRes->result) && $checkUserRes->result !== 'success') {
                    // register tours user
                    $regUserRes = $this->registerUser($email, $language);

                    if (isset($regUserRes->result) && $regUserRes->result === 'success') {
                        $subscriber->registered = 1;
                    }
                } else {
                    $subscriber->registered = 1;
                }

                // subscribe user
                $subscriber->subscribed = 1;

                try {
                    $subscriber->save();
                } catch (\Exception $e) {
                    Log::error('save subscriber error: ' . $e->getMessage());
                }
                return response()->json([
                    'success' => true,
                    'message' => 'sub03'
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => "blank or not valid"
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null $email
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cancel(Request $request, $email = null)
    {
        if (!empty(trim($email)) && strpos($email, '@')) {
            $url = config('app.usi_email_server_address') . '/unsubscribeEmail/' . $email;
            if ($request->has('code')) {
                $url .= '?code=' . $request->input('code');
            }
            $response = json_decode($this->httpGet($url), true);

            if (isset($response['status']) && $response['status'] === 'success') {
                $data = [
                    'success' => true
                ];
            } else {
                $data = [
                    'success' => false
                ];
            }
            if ($this->isMobileDevice()) {
                return view('mobile.cancel-sub-result', $data);
            }
            return view('cancel-sub-result', $data);
        }

        $data = [
            'success' => false
        ];
        if ($this->isMobileDevice()) {
            return view('mobile.cancel-sub-result', $data);
        }
        return view('cancel-sub-result', $data);
    }

    /**
     * @param $email
     * @return mixed
     */
    private function checkUser($email)
    {
        $url = $this->UsitripUserApi['endpoint'] . '/index.php?mod=api_customers_info';
        $data = [
            'PHP_AUTH_USER' => $this->UsitripUserApi['username'],
            'PHP_AUTH_PW' => $this->UsitripUserApi['userpw'],
            'customers_id' => $email,
        ];
        $options = [
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPAUTH => CURLAUTH_ANY,
            CURLOPT_USERPWD => "test:test2016"
        ];

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);

        Log::info($this->ip . ' check tours user result: ' . $response);
        $checkResult = json_decode($response);

        return $checkResult;
    }

    /**
     * @param $email
     * @param $lang
     * @return mixed
     */
    private function registerUser($email, $lang)
    {
        $url = $this->UsitripUserApi['endpoint'] . '/index.php?mod=api_customers_create';
        $pwd = uniqid();
        $data = [
            'PHP_AUTH_USER' => $this->UsitripUserApi['username'],
            'PHP_AUTH_PW' => $this->UsitripUserApi['userpw'],
            'email' => $email,
            'lang' => $lang,
            'password' => $pwd,
            'confirmation' => $pwd,
            'customers_referer_type' => 1,
        ];
        $options = [
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPAUTH => CURLAUTH_ANY,
            CURLOPT_USERPWD => "test:test2016"
        ];

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);

        $regResult = json_decode($response);

        return $regResult;
    }

}
