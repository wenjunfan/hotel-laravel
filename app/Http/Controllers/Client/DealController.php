<?php

namespace App\Http\Controllers\Client;

use App\Classes\HotelAPI;
use App\Model\Coupon;
use App\Model\Destination;
use App\Model\Hotel;
use App\Model\User;
use App\Traits\HotelDataRetriever;
use App\Traits\IsMobileDevice;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class DealController
 * @package App\Http\Controllers\Client
 */
class DealController extends Controller
{
    use IsMobileDevice, HotelDataRetriever;

    /**
     * @param $domain
     * @param $code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($code)
    {
        $coupon = Coupon::where('code', $code)->first();
        $hotelGroups = [];

        if (!empty($coupon)) {
            $today = Carbon::now();

            if (isset($coupon->start_at) && Carbon::parse($coupon->start_at)->greaterThan($today)) {
                abort('404', 'Coupon not available yet.');
            }

            if (isset($coupon->expires_at) && Carbon::parse($coupon->expires_at)->lessThan($today)) {
                abort('404', 'Coupon already expired.');
            }

            session(['coupon' => json_encode($coupon)]);

            $hotelIds = explode(",", $coupon->hotelIds);
            $hotels = Hotel::whereIn('hotelId', $hotelIds)->get()->groupBy('desId')->toArray();

            if (count($hotels) > 0) {
                foreach ($hotels as $key => $hotelGroup) {
                    if (count($hotelGroup) > 0) {
                        $destination = Destination::where('desId', $key)->first();
                        $hotelGroups[] = [
                            'destination' => $destination,
                            'hotels' => $hotelGroup
                        ];
                    }
                }
            }

            $checkin = Carbon::now()->addDays(15)->toDateString();
            $checkout = (new Carbon($checkin))->addDay()->toDateString();

            $blade = $this->isMobileDevice() ? 'mobile.deal' : 'deal';

            return view($blade, compact('hotelGroups', 'coupon', 'checkin', 'checkout'));
        } else {
            abort('404');
        }
    }

    /**
     * @param $domain
     * @param $couponId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDealPrice($couponId, Request $request)
    {
        $coupon = Coupon::find($couponId);
        if (!empty($coupon)) {
            $hotelIds = explode(',', $coupon->hotelIds);
            $staticHotels = Hotel::whereIn('hotelId', $hotelIds)->get();
            $checkinDate = $request->input('checkin');
            $checkoutDate = $request->input('checkout');
            $searchInfo = $this->generateStaticParams($checkinDate, $checkoutDate, '');
            $hotelGroups = $staticHotels->groupBy('desId')->toArray();

            $hotels = [];

            $partner_id = config('app.partner_id');
            if (request()->getHost() === config('app.b_host')) {
                $partner_id = auth()->user()->id;
            }
            $hotelAPI = new HotelAPI(User::find($partner_id));
            foreach ($hotelGroups as $key => $hotelGroup) {
                $destination = Destination::where('desId', $key)->first();
                if (!empty($destination)) {
                    $hotelCachesNew = $hotelAPI->searchHotels($key, [], $checkinDate, $checkoutDate, $searchInfo['occupancies']);
                    if (isset($hotelCachesNew['data']['hotels'])) {
                        $cacheData = $hotelCachesNew['data'];
                        $hotelsData = collect($cacheData['hotels'])->whereInLoose('id', $hotelIds)->toArray();
                        $cacheData['hotels'] = $hotelsData;
                        $hotelCaches = $this->cacheDealHotels($cacheData, 1, 1); // we are using default params here
                        $hotels = array_merge($hotelCaches, $hotels);
                        unset($cacheData, $hotelsData, $hotelCaches);
                    }
                }
            }

            return response()->json([
                'success' => true,
                'hotels' => $hotels,
                'checkin' => $checkinDate,
                'checkout' => $checkoutDate
            ]);
        }

        return response()->json([
            'success' => false
        ]);
    }

    /**
     * @param $hotelCaches
     * @param $roomCount
     * @param $dayCount
     * @return array
     */
    private function cacheDealHotels($hotelCaches, $roomCount, $dayCount) // cache hotels for filter and open hotel detail page
    {
        $hotels = $hotelCaches['hotels'];
        $hotelsCache = [];
        $expediaProvider = DB::table('gate')->where('provider', '["Expedia"]')->first();
        $expediaEnabled = !is_null($expediaProvider) && $expediaProvider->display == 1 ? true : false; // show lowest flag on hotel or not

        foreach ($hotels as $key => $hotel) {
            $discount = isset($hotel['discount']) ? $hotel['discount'] : 0;
            if (isset($hotel['dynamicDiscount']) && $hotel['dynamicDiscount'] > $discount) {
                $discount = $hotel['dynamicDiscount'];
            }

            $taxRate = config('constants.tax_rate');

            $beforeTax = $hotel['minRate'] === -1 ? $hotel['minRate'] : floor(($hotel['minRate'] * (1 - $taxRate)) / $roomCount / $dayCount) * $roomCount * $dayCount;

            $hotelsCache[] = [ // cache hotels to be used for filtering on list page only
                'id' => $hotel['id'],
                'currency' => $hotel['currency'],
                'minRate' => $hotel['minRate'],
                'beforeTax' => $beforeTax,
                'discount' => $discount,
                'lowestFlg' => isset($hotel['lowestFlg']) && $expediaEnabled ? $hotel['lowestFlg'] : 0,
                'distance' => isset($hotel['distance']) && $expediaEnabled ? $hotel['distance'] : null,
                'guestReviewCount' => isset($hotel['guestReviewCount']) && $expediaEnabled ? $hotel['guestReviewCount'] : 0,
                'guestRating' => isset($hotel['guestRating']) && $expediaEnabled ? $hotel['guestRating'] : '',
                'sessionKey' => $hotelCaches['sessionKey'],
            ];
        }

        $redisHotelCachesKey = $hotelCaches['sessionKey'] . '.hotels'; // api sessionKey
        Cache::put($redisHotelCachesKey, json_encode($hotelsCache), config('app.cache_lifetime'));

        return $hotelsCache;
    }

}
