<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Model\Creditcard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 *  deprecated
 * Class CreditcardController
 * @package App\Http\Controllers\Client
 */
class CreditcardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->show(-1);
    }

    /**
     * Show the form for creating a new resource.
     *
     // * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('defaultCard') != 1) {
            $request->merge( array( 'defaultCard' => 0 ) );
        }
        if(1 == $request->input('defaultCard'))
        {
            Creditcard::where('partnerId', Auth::user()->id)
            ->update(['defaultCard' => 0]);
        }
        $d = [
            'partnerId' => Auth::user()->id,
            'number' => $request->input('number'),
            'cardType' => $request->input('cardType'),
            'month' => $request->input('month'),
            'year' => $request->input('year'),
            'CVC' => $request->input('CVC'),
            'firstName' => $request->input('firstName'),
            'lastName' => $request->input('lastName'),
            'street' => $request->input('street'),
            'street2' => $request->input('street2'),
            'city' => $request->input('city'),
            'state' => $request->input('state'),
            'zip' => $request->input('zip'),
            'countryCode' => $request->input('countryCode'),
            'defaultCard' => $request->input('defaultCard'),
        ];

        $o = Creditcard::where('partnerId', Auth::user()->id)
            ->where('number', $request->input('number'))
            ->first();

        if (!is_null($o)) {
            Creditcard::find($o->id)->update($d);
        }
        else {
            Creditcard::create($d);
        }

        return redirect('creditcards')->with('message', '操作成功！');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currentCreditCard = null;
        $creditCards = Creditcard::where('partnerId', Auth::user()->id)->get();

        foreach($creditCards as $c) {
            if ($c->id == $id) {
                $currentCreditCard = $c;
                break;
            }
        }

        return view('credit-card', [
            'optionName' => 'creditcards',
            'currentCreditCard' => $currentCreditCard,
            'creditCards' => $creditCards,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     // * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     // * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $o = Creditcard::find($id);
        if (is_null($o)) {
            return response()->json(['success' => false, 'message' => '信用卡不存在！']);
        }

        Creditcard::destroy($id);
        return response()->json(['success' => true]);
    }
}
