<?php

namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Controller;
use App\Traits\SendMessageTrait;


class SendMessageController extends Controller
{
    use SendMessageTrait;
    public function sendOrderSms($input_country_code ,$input_mobile, $code)
    {
        $this->sendSms($input_country_code, $input_mobile, $code);
    }
}
