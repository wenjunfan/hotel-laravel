<?php

namespace App\Http\Controllers\Client\Auth;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

use App\Http\Controllers\Controller;
use App\Model\Sms;
use Illuminate\Http\Request;
use Carbon\Carbon;

// 使用 Twilio
class VerifyPhoneController extends Controller
{
    // 注意，每条短信都要记录，以统计最后发送数量
    public function sendVerificationCode(Request $request)
    {
        $input_country_code = $request->input('country_code');
        $input_mobile = $request->input('mobile');
        // 去除空格
        $country_code = str_replace(' ', '', $input_country_code);
        $mobile = str_replace(' ', '', $input_mobile);
        $full_mobile = $country_code . $mobile;
        // 检查sms表中是否有该手机号，验证码是否在10分钟内
        $sms = Sms::where('mobile', $full_mobile)
                  ->orderBy('created_at', 'desc')
                  ->first();

 	    $host = $this->host;
        if(strrpos($host, "supervacation") > -1){
            $name = 'America Asia';
        }elseif(strrpos($host, "usitour") > -1) {
            $name = 'Usitour';
        }else{
            $name = 'Usitrip';
        }
		
        if ($sms != null && strpos($sms, 'http') === false) {
            // 手机已经存在，且没有成单，已经成单的即使是10分钟内也要新的code
            // 检查验证码是否是在10分钟内
            $now = Carbon::now();
            $minutes = $now->diffInMinutes($sms->created_at);
            // dd($minutes);
            if ($minutes < 10) {
                // 如果10分钟内已经发送过五次短信了
                if ($sms->sent_count > 5 && config('app.env') != 'local') {
                    return response()->json([
                        'success' => false,
                        'errorId' => 'IT0105',
                    ]);
                }

                // 没有call twilio 发送旧代码
                $sms->sendOldVerifyCode($name, $full_mobile, 0, $sms->content);
                $sms->sent_count++;// 发送计数

                $sms->save();
                return response()->json([
                    'success' => true
                ]);
            }
        }
        // 如果在sms表中没有该手机号或者已经过了10分钟有效期，生成新的sms手机验证码记录
        $sms = new Sms(
            [
                'type'    => 0,
                'mobile'  => $full_mobile,
                'user_id' => '0',
            ]
        );
        // call twilio 服务
        try{
            $sms->sendVerifyCode($name, $full_mobile, 0);
            return response()->json([
                "success" => true
            ]);
        }catch(\Exception $e){
            Log::info('send Verify Code failed: ' . $e->getMessage());
            return response()->json([
                "success" => false,
                "message" => 'send code failed'
            ]);
        }
    }

    public function verifyCode(Request $request)
    {
        $Reorderid = $request->input('Reorderid');
        $tempOrderTriedTimesKey = $Reorderid . '_verifyTimes';
        $tempOrderTriedTimes = 0;
        if (Cache::has($tempOrderTriedTimesKey)) {
            $tempOrderTriedTimes = Cache::get($tempOrderTriedTimesKey);
        }
        $tempOrderTriedTimes++;

        Log::info($this->ip . ' temp order tried times: ' . $tempOrderTriedTimes);

        if (config('app.env') == 'production' && $tempOrderTriedTimes > 3) {
            return response()->json([
                'success'      => false,
                'errorId'      => 'IT0106',
                'errorMessage' => 'Tried too many times'
            ]);
        }
        Cache::put($tempOrderTriedTimesKey, $tempOrderTriedTimes, config('app.cache_lifetime'));

        $phone = str_replace(' ', '', $request->input('phone'));
        $phoneNumberTriedTimesKey = $phone . '_verifyTimes';
        $phoneTriedTimes = 0;
        if (Cache::has($phoneNumberTriedTimesKey)) {
            $phoneTriedTimes = Cache::get($phoneNumberTriedTimesKey);
        }
        $phoneTriedTimes++;

        if (config('app.env') == 'production' && $phoneTriedTimes > 15) {
            return response()->json([
                'success'      => false,
                'errorId'      => 'IT0107',
                'errorMessage' => 'Tried too many times'
            ]);
        }

        Cache::put($phoneNumberTriedTimesKey, $phoneTriedTimes, config('app.cache_lifetime'));

        // 检查sms表中是否有该手机号, 最新
        $sms = Sms::where('mobile', $phone)
                  ->orderBy('created_at', 'desc')
                  ->first();

        if ($sms != null && $sms->content == $request->input('code')) {
            Log::info($this->ip . ' phone verification success');
            return response()->json([
                'success' => true
            ]);
        } else {
            Log::info($this->ip . ' phone verification failed');
            return response()->json([
                'success' => false
            ]);
        }
    }
}
