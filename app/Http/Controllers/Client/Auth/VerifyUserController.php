<?php

namespace App\Http\Controllers\Client\Auth;

use App\Model\BinBase;
use App\Model\OrderTem;
use App\Model\SuspiciousOrder;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\UploadFile;

class VerifyUserController extends Controller
{
    use UploadFile;

    public function saveFraudUserFile(Request $request)
    {
        $Reorderid = $request->input('Reorderid');
        $sus_order = SuspiciousOrder::where('Reorderid', $Reorderid)
            ->select('file_url')
            ->first();
        if (empty($sus_order)) {
            return response()->json([
                'success' => false,
                'errorMessage' => 'Order issue, please contact customer service'
            ]);
        }

        $file = $request->photo;
        if ($file) {
            $file_type = $file->getMimeType();
            if ($file->getSize() > 2000000) {
                return response()->json([
                    'success' => false,
                    'errorMessage' => session('language', 0) == 0 ? '图片大小不可超过2M' : 'File size can not be over 2M'
                ]);
            } elseif (strrpos($file_type, 'image') !== false || $file_type === 'application/pdf') {
                $originalName = $file->getClientOriginalName();
                $fileNameArr = explode('.', $originalName);
                $fileType = array_pop($fileNameArr);
                $fileName = $Reorderid . '_' . implode($fileNameArr) . '_' . strtotime("now") . '.' . $fileType;
                $fileName = 'hotelUserVerifyFiles/' . $fileName;

                $savedFilePath = $this->uploadFile($file, $fileName);

                $file_url = $sus_order->file_url;

                if ($file_url != '') {
                    SuspiciousOrder::where('Reorderid', $Reorderid)
                        ->update(['file_url' => $file_url . ',' . $savedFilePath]);
                } else {
                    SuspiciousOrder::where('Reorderid', $Reorderid)
                        ->update(['file_url' => $savedFilePath]);
                }

                return response()->json([
                    'success' => true,
                    'url' => $savedFilePath
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'errorMessage' => session('language', 0) == 0 ? '图片类型不支持' : 'Please upload allowed file type image'
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'errorMessage' => 'No files uploaded'
            ]);
        }
    }

    public function checkCardCountry($number, $Reorderid)
    {
        $binbase = BinBase::where('bin', $number)
            ->first();

        if (isset($binbase) && !empty($binbase->isocountry)) {
            //先记录国家，无论中国美国加拿大
            Log::info('Start check user cc card country ' . $number . ' is ' . $binbase->isocountry);//记录用户使用的信用卡国家
            try {
                OrderTem::where('Reorderid', $Reorderid)
                    ->update(['cardCountry' => $binbase->isocountry]);
            } catch (\Exception $e) {
                //记录用户使用的信用卡国家，如果没有成功save的话就发邮件
                Log::error($this->ip . ' save temp country card failed' . $e->getMessage());
                Mail::raw($e->getMessage(), function ($message) {
                    $message->subject('Error Message From B2C Site, save card country failed');
                    $message->to(config('staff-emails.ann'));
                });
            }

            if ($binbase->isocountry == 'CHINA') {
                return [
                    'success' => true,
                ];
            } elseif ($binbase->isocountry == 'UNITED STATES' || $binbase->isocountry == 'CANADA') {
                return [
                    'safe' => true,
                ];
            }
        }
        return [
            'success' => false,
        ];
    }

    public function checkCardList($domain, $uniquenumber)
    {
        log::info($this->ip . ' From domain' . $domain . ' passedNumber is ' . $uniquenumber);

        $cardCombination = SuspiciousOrder::where('uniqueCard', $uniquenumber)
            ->orderBy('created_at', 'desc')
            ->first();

        log::info($this->ip . ' card combination' . json_encode($cardCombination));

        if (isset($cardCombination) && $cardCombination->isFraud == 1) {
            return [
                'success' => false,
                'errorId' => "BL1001",
                'errorMessage' => '黑名单，曾经被拒单.'
            ];
        }
        if (isset($cardCombination) && $cardCombination->isDone == 1) {
            return [
                'success' => true
            ];
        }
        return [
            'success' => false
        ];
    }
}