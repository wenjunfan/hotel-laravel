<?php

namespace App\Http\Controllers\Client\Auth;

use App\Model\User;
use App\Model\Distributor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	/**
	 * Where to redirect users after login / registration.
	 *
	 // * @var string
	 */
	public function redirectPath()
	{
	    if (request()->getHost() === config('app.b_host')) {
	        return '/search';
        }

		if (Auth::user()->id == 11180) {
			return property_exists($this, 'redirectTo') ? $this->redirectTo : '/affiliate/fraud-list';
		} else {
			return property_exists($this, 'redirectTo') ? $this->redirectTo : '/affiliate/profile';
		}
	}
	// protected $redirectTo = '/affiliate/dashboard';

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        parent::__construct();
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);

	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		$c = 0;
		$c1 = 0;
		$c2 = 0;
		$c3 = 0;
		$c4 = 0;
		for ($i = 0; $i < strlen($data['password']); $i++) {
			if ((ord(substr($data['password'], $i, 1)) >= 48 && ord(substr($data['password'], $i, 1)) <= 57) && (0 == $c1)) {
				$c = $c + 1;
				$c1 = 1;
			}
			if ((ord(substr($data['password'], $i, 1)) >= 65 && ord(substr($data['password'], $i, 1)) <= 90) && (0 == $c2)) {
				$c = $c + 1;
				$c2 = 1;
			}
			if ((ord(substr($data['password'], $i, 1)) >= 97 && ord(substr($data['password'], $i, 1)) <= 122) && (0 == $c3)) {
				$c = $c + 1;
				$c3 = 1;
			}
			if ((ord(substr($data['password'], $i, 1)) >= 33 && ord(substr($data['password'], $i, 1)) <= 47) && (0 == $c4)) {
				$c = $c + 1;
				$c4 = 1;
			}
			if ((ord(substr($data['password'], $i, 1)) >= 58 && ord(substr($data['password'], $i, 1)) <= 64) && (0 == $c4)) {
				$c = $c + 1;
				$c4 = 1;
			}
		}
		if ($c < 2) {
			$data['password'] = 'error';
			$data['password_confirmation'] = 'error';
		}

		return Validator::make($data, [
			'name'                  => 'required|max:255',
			'email'                 => 'required|between:3,64|email|unique:partner,email',
			'password'              => 'required|min:8|confirmed',
			'password_confirmation' => 'required|same:password|min:8'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 * @return User
	 */
	protected function create(array $data)
	{
        $user = new User();
        $user->name =  $data['name'];
        $user->email = $data['email'];
        $user->admin = 5;
        $user->affiliateRate = config('constants.affiliate_rate');
        $user->password = bcrypt($data['password']);
        $user->host_name = $data['domain'];
        $user->language =$data['language'];
        $user->save();

        $id = User::where('email', $data['email'])->first()->id;
        // 如果曾经没有开通过分销账户
        $distributor_code = $this->generateRandomString();
        $distributorTable = new Distributor();
        $distributorTable->distributor_code = strtolower($distributor_code);
        $distributorTable->partner_id = $id;
        $distributorTable->distributor_name = $data['name'];
        $distributorTable->life_time = 30;
        try {
            $distributorTable->save();
        } catch (\Exception $e) {
            Log::error('add distributor failed for user ' . $data['email'] . ' reason: ' . $e->getMessage());
        }

        return Auth::loginUsingId($id, true);
	}
    private function generateRandomString($length = 32)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
    /*
     * C端销售联盟LOGIN PAGE
     */
    public function showLoginForm (Request $request)
    {
            return view('auth.client.login');
    }

    public function showRegistrationForm (Request $request)
    {
            return view('auth.client.register');
    }

    public function termsOfUse (Request $request)
    {
        return view('auth.client.terms-of-use');
    }

    public function privatePolicy (Request $request)
    {
        return view('auth.client.privacy-policy');
    }

}
