<?php

namespace App\Http\Controllers\Hotel;

use App\Classes\SinaUrl;
use App\Jobs\TriggerFeedback;
use App\Traits\IsMobileDevice;
use Exception;
use App\Model\Sms;
use App\Classes\HotelAPI;
use App\Classes\HotelDB;
use App\Classes\Paypal\CreditCardInfo;
use App\Classes\Paypal\Paypal;
use App\Classes\Paypal\PayResult;
use App\Jobs\SendReminderCancelActionEmail;
use App\Http\Controllers\Controller;

use App\Model\Destination;
use App\Model\Hotel;
use App\Model\Order;
use App\Model\OrderPax;
use App\Model\OrderRoom;
use App\Model\Partner;
use App\Model\Refund;
use App\Model\EbUser;
use App\Model\EbHotelUser;
use App\Model\ScenicArea;
use App\Model\User;
use App\Model\Feedback;
use App\Model\OrderTem;
use App\Model\Payment;
use App\Model\SuspiciousOrder;

use App\Traits\HotelDataRetriever;
use App\Traits\HttpGet;
use App\Traits\IsLandLineTrait;
use App\Traits\PaymentTrait;
use App\Classes\CitconPay;
use App\Classes\PaypalNew\CreditCardInfoNew;
use App\Classes\PaypalNew\PaypalNew;
use App\Classes\PaypalNew\PayResultNew;
use App\Traits\ApiPostTrait;
use App\Traits\SendMessageTrait;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\Client\Auth\VerifyUserController as VerifyUser;
use App\Http\Controllers\Client\Auth\SendMessageController as sendMessage;
use App\Http\Controllers\Hotel\BookController as bookController;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * @resource Hotel
 * Class HotelController
 * @package App\Http\Controllers\Hotel
 */
class HotelController extends Controller
{
    use HotelDataRetriever, HttpGet, IsLandLineTrait, PaymentTrait, ApiPostTrait, SendMessageTrait, IsMobileDevice;

    private $partnerId;
    private $cacheLifeTime;
    private $techEmails;
    private $otherEmails;
    private $testEmails;
    private $accEmails;
    private $salesEmails;
    private $marketingEmails;
    private $annbdcn;
    private $aa;

    public function __construct()
    {
        parent::__construct();

        if (strrpos($this->host, '117book') > -1) {
            $this->partnerId = Auth::check() ? Auth::user()->id : config('app.partner_id');
        }elseif(strpos($this->host, 'supervacation') >-1){
            $this->partnerId = config('app.aa_partner_id');
        }else {
            $this->partnerId = config('app.partner_id');
        }
        $this->cacheLifeTime = config('app.cache_lifetime');
        $this->techEmails = config('staff-emails.tech');
        $this->otherEmails = config('staff-emails.other');
        $this->testEmails = config('staff-emails.test');
        $this->accEmails = config('staff-emails.acc');
        $this->salesEmails = config('staff-emails.sales');
        $this->marketingEmails = config('staff-emails.marketing');
        $this->ann106 = config('staff-emails.ann106');
        $this->annbdcn = config('staff-emails.annbdcn');
        $this->aa = config('staff-emails.aa');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(Request $request)
    {
        $executionStartTime = microtime(true);
        if ($request->input('type') == 'special') {
            $id = $request->input('id');
        } else {
            $id = Order::where('orderReference', $request->input('id'))
                ->first()->id;
        }

        $order = Order::with('rooms', 'hotel.destination', 'partner')
            ->where('id', $id);
        $order = $order->leftJoin(DB::raw('(select orderReference,description FROM partner_transaction) p'),
            function ($join) {
                $join->on('p.orderReference', '=', 'porder.orderReference');
            });//Arthur 060817
        $order = $order->first();

        if ($order == null) {
            return response()->json([
                'success' => false,
                'message' => '没有这个订单，订单号' . $request->input('id')
            ]);
        }

        if ($request->input('type') == 'special') {
            //如果是特殊cancel的情况
            if ($order->status != 'CONFIRMED') {
                return response()->json([
                    'success' => false,
                    'message' => $id . '这个订单已经被处理,订单状态' . $order->status
                ]);
            }

            //link feedback for special cancel
            $feedback = Feedback::where('porder_id', $id)->first();
            $feedback = new Feedback;
            $feedback->porder_id = $id;
            $feedback->solution = Carbon::now() . ' 已发送特殊取消邮件 (' . Auth::user()->id . ') &#013;' .  $feedback->solution;
            $feedback->fill($request->all());
            $feedback->save();
            $order['status'] = 'CANCELLED';
            $order->save();

            //不是ali ctrip等才需要发邮件
            if(!in_array($order->partnerId, ['10020','10021','10022'])){
            //after update order status, mail to customer, bd tell accountant to refund.
            Log::info($this->ip . ' Special Cancel Email Start');
                try {
                    $cancelStatus = 'CANCELLED';
                    $emailto = $order->bookerEmail;
                    $partnerName = $order->partnerId === 11087 ? $order->bookerFirstName . ' ' . $order->bookerLastName : $order->partner->name;
                    $httpHost = $order->http_host ? $order->http_host : 'Unitedstars inc';
                    $paymentType = $order->payment_type == 'CC' ? 'Credit Card' : $order->payment_type;
                    $userLanguage = $order->user_language;
                    if($userLanguage == 0) {
                        $cancelTitle = '酒店取消';
                        $emailName = 'emails.special_cancel';
                    }elseif($userLanguage == 2){
                        $cancelTitle = '飯店取消';
                        $emailName = 'emails.special_cancel_tw';
                    }else {
                        $cancelTitle = ' Hotel Special Cancellation ';
                        $emailName = 'emails.special_cancel_en';
                    }
                    $img = '';
                    if($userLanguage === 2){
                        $img = 'https://hotel.usitour.com/img/general/logo/logo-usitour-tw.png';
                    }else{
                        if (strrpos($httpHost, 'usitrip') !== false) {
                            if ($userLanguage === 1) {
                                $img = 'https://hotel.usitrip.com/img/email/Email-Logo-usitrip-en.png';
                            }else {
                                $img = 'https://hotel.usitrip.com/img/email/Email-Logo.png';
                            }
                        }
                        if (strrpos($httpHost, 'usitour') !== false) {
                            $img = 'https://hotel.usitrip.com/img/email/Email-Logo-en.png';
                        }
                        if (strrpos($httpHost, '117book') !== false) {
                            $img = 'http://www.117book.com/img/landing/Logo-CN.png';
                        }
                    }


                    Log::info('special cancel for customer ' . $partnerName . ' order id:' . $id);

                    Mail::queue($emailName, [
                        'bookingId' => $id,
                        'partnerId' => $order->partnerId,
                        'partnerName' => $partnerName,
                        'status' => $cancelStatus,
                        'provider' => $order->provider,
                        'paymentType' => $paymentType,
                        'totalPrice' => $order->currencyCode . ' ' . $order->totalPrice,
                        'hotelId' => $order->hotelId,
                        'hotelName' => $order->hotelName,
                        'orderRef' => $order->orderReference,
                        'httpHost' => $httpHost,
                        'img' => $img,
                    ], function ($message) use ($id, $emailto, $httpHost, $cancelTitle) {
                        $message->subject($httpHost . $cancelTitle . ' (' . $id . ') !')
                            ->to($emailto)->bcc(config('staff-emails.ann'));
                    });
                } catch (Exception $e) {
                    Log::error($this->ip . ' send special cancel email error: ' . $e->getMessage());
                }
            }

            //for e-booking orders, if user cancel, send email to alex and hotel provider, for further assistance
            if ($order->provider === 'EBooking') {
                Log::info('special cancel call eb reminder email');
                $this->ebCancelRemainderEmail($order);
            }

            return response()->json([
                'success' => true,
            ]);
        }

        $user = User::find($order->partnerId);
        $api = new HotelAPI($user);

        $result = $api->cancelOrder($order->orderReference);
        Log::info($this->ip . ' api cancel result: ' . json_encode($result));

        $this->cancelEmail($result, $id);

        $executionEndTime = microtime(true);
        $seconds = $executionEndTime - $executionStartTime;
        Log::info($this->ip . ' TIME to email new cancel for ' . $id . ' is ' . $seconds . ' seconds');

        if (!isset($result['success']) || !$result['success']) {
            return response()->json([
                'success' => false,
                'message' => $result['errorMessage']
            ]);
        }

        $anaOptions = false;
        if (request()->getHost() === $order['http_host']) {
            $anaOptions = [
                'id' => $id,
            ];
        }

        Log::info($this->ip . ' cancel anaOptions: ' . json_encode($anaOptions));

        if (isset($order->orderReference)) {
            if ('ALIPAY' == $order->payment_type || 'WECHATPAY' == $order->payment_type) {
                Log::info($this->ip . ' Alipay and wechatpay cancel order reference ' . $order->orderReference);
                $porder_tem = OrderTem::where('orderReference', $order->orderReference)
                    ->first();

                $amount = $result['data']['refund'] * 100;
                if (!$amount) { //没付钱的情况下
                    $response = [
                        'success' => false,
                        'message' => 'Refund failed, please contact customer service'
                    ];

                    return $response;
                }

                $transactionRef = $porder_tem->Reorderid;
                Log::info($this->ip . ' transaction reference: ' . $transactionRef);

                $transaction = CitconPay::inquire($transactionRef);
                Log::info($this->ip . ' Citcon inquire response: ' . json_encode($transaction));

                if (empty($transaction)) {
                    Log::info($this->ip . ' Cancel order can not find transaction');
                    Order::where('orderReference', $order->orderReference)
                        ->update([
                            'payment_status' => 'PENDING',
                        ]);

                    return response()->json([
                        'success' => true,
                        'options' => $anaOptions,
                        'message' => "待退款"
                    ]);
                }

                $refundType = "Full";
                if ($amount < $transaction->amount) {
                    $refundType = "Partial";
                } else {
                    $amount = $transaction->amount;
                }

                $reason = 'cancel order refund';
                $refundResult = $this->citconpayRefund($transaction, $reason, $refundType, $amount, $order);
                if (isset($refundResult->status) && 'success' == $refundResult->status) {
                    $response = [
                        'success' => true,
                        'options' => $anaOptions,
                        'message' => 'Refund successfully'
                    ];
                } else {
                    $response = [
                        'success' => false,
                        'options' => $anaOptions,
                        'message' => isset($refundResult->status) ? $refundResult->status : 'Refund failed, please contact customer service'
                    ];
                }

                return response()->json($response);
            } else {
                $message = '';
                if ($order->description == 'Prepay' && $order->payment_type !== 'PB') {
                    Log::info($this->ip . ': ' . $order->payment_type . ' cancel order, order reference: ' . $order->orderReference);
                    $porder_tem = OrderTem::where('orderReference', $order->orderReference)
                        ->first();

                    $transactions = Payment::where('orderReference', $order->orderReference)
                        ->get();

                    foreach ($transactions as $transaction) {
                        if ($transaction != null) {
                            $type = "Partial";
                            $amount = $result['data']['refund'];
                            // $amount = 1; // test refund without actually cancel order

                            if ($amount >= $transaction->amount) {
                                $type = "Full";
                                $amount = $transaction->amount;
                            }

                            if (isset($porder_tem->amount)) {
                                if (1 > ($porder_tem->amount - $amount)) {
                                    $type = "Full";
                                    $amount = $porder_tem->amount;
                                }
                            }

                            if ($amount > 0) {
                                $transaction->type = $type;
                                $transaction->amount = $amount;
                                $transaction->note = "取消订单";
                                $transaction->partnerId = $order->partnerId;
                                $message = $this->refund($transaction);
                            }
                        } else {
                            Log::info($this->ip . ' did not find payment');
                        }
                    }
                }elseif($order->payment_type === 'PB'){
                    Log::info('PB paid order get cancelled, need refund: ' . $order->id);
                    try {
                        $msg = 'Order id : ' . $order->id;
                        Mail::raw($msg, function ($message) {
                            $message->to(config('staff-emails.ann'))
                                ->subject('Paypal Btn need refund');
                        });
                    } catch (Exception $e) {
                        Log::error($this->ip . ' pb need refund email send failed, error: ' . $e->getMessage());
                    }
                }

                //for e-booking orders, if user cancel, send email to alex and hotel provider, for further assistance
                if ($order->provider === 'EBooking') {
                    $this->ebCancelRemainderEmail($order);
                }

                return response()->json([
                    'success' => true,
                    'options' => $anaOptions,
                    'message' => $message
                ]);
            }
        }

        return response()->json([
            'success' => false,
            'options' => [],
            'message' => 'no such order'
        ]);
    }

    /**
     * for e-booking orders, if user cancel, send email to alex and hotel provider, for further assistance
     * @param $order
     */
    public function ebCancelRemainderEmail($order){
        $hotel_user = EbHotelUser::where('hotelId', $order->hotelId)->first();
        if (!empty($hotel_user)) {
            $hotel_user_id = $hotel_user->user_id;
            $eb_user = EbUser::where('user_id', $hotel_user_id)->first();
            $eb_user_email = !empty($eb_user) ? $eb_user->email : '';
        } else {
            $eb_user_email = '';
        }

        $orderReference = $order->orderReference;
        if (config('app.env') == 'local') {
            $eb_user_email = config('app.default_mail_to');
        }

        Log::info('ready for E-Booking provider email ' . $eb_user_email);

        $orderPax = OrderPax::where('orderReference',$orderReference)->get();
        $orderRoom = OrderRoom::where('orderReference', $orderReference)->first();
        $guestName = $orderPax[0]->firstName.' '. $orderPax[0]->lastName;
        $ad_total = count(OrderPax::where('orderReference', $orderReference)->where('type','AD')->get());
        $ch_total = count(OrderPax::where('orderReference',$orderReference)->where('type','<>','AD')->get());
        $boardName = $orderRoom->boardName;
        $roomType = $orderRoom->name;
        $cancellationInfo = json_decode($orderRoom->cancelPolicies);
        $cancellation = "Non-Refundable";
        if(isset($cancellationInfo)){
            $cancellationInfo = $cancellationInfo[0];
            if($cancellationInfo->amount== '0'){
                if(isset($cancellationInfo->end)){
                    $cancellationInfo = $cancellationInfo->end;
                }else{
                    $cancellationInfo = $cancellationInfo->from;
                }
                $cancellation = str_replace("T", " ", substr($cancellationInfo, 0, 19));
                if (!$cancellation) {
                    $cancellation = "Non-Refundable";
                } else{
                    $cancellation = "Free Cancellation before " . $cancellation;
                }
            }
        }

        $data = [
            'toMail' => $eb_user_email,
            'hotelName' => $order->hotelName,
            'orderId' => $order->id,
            'checkin' => $order->checkinDate,
            'checkout' => $order->checkoutDate,
            'guestName' => $guestName,
            'roomCount' =>$order->room_number,
            'paxes' => $ad_total. ' adults, '. $ch_total. ' children' ,
            'boardType' =>  $boardName,
            'roomType' =>  $roomType,
            'cancellationPolicy' => $cancellation,
            'contractPrice' =>  $order->totalOriginalNetPrice,
        ];

        try{
            $this->post('/sendCancelledEmail', $data);
            $job = (new SendReminderCancelActionEmail($data))->delay(config('app.email_delay'));
            $this->dispatch($job);
        }catch (Exception $e) {
            Log::error($this->ip . ' send e-booking cancel email error: ' . $e->getMessage());
        }
    }
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelUsi(Request $request)
    {
        Log::info($this->ip . ' cancel from usitrip');
        $source = $request->has('source') ? $request->get('source') : '';
        $user_id = $request->has('cid') ? $request->get('cid') : '';
        $token = $request->has('token') ? $request->get('token') : '';

        $new_token = md5('www.usitrip.com:hotels.usitrip.com:' . $source . ':' . $user_id . ':HowardChris');

        if ($token == $new_token) {
            $orderReference = $request->input('id');

            $executionStartTime = microtime(true);
            $id = Order::where('orderReference', $orderReference)
                ->first()->id;
            $order = Order::with('rooms')
                ->where('id', $id);
            $order = $order->leftJoin(DB::raw('(select orderReference,description FROM partner_transaction) p'),
                function ($join) {
                    $join->on('p.orderReference', '=', 'porder.orderReference');
                });//Arthur 060817
            $order = $order->first();

            if ($order == null) {
                return response()->json([
                    'success' => false,
                    'message' => '没有这个订单，订单号' . $request->input('id')
                ]);
            }

            $user = User::find($order->partnerId);
            $api = new HotelAPI($user);
            $result = $api->cancelOrder($order->orderReference);
            Log::info($this->ip . ' api cancel result: ' . json_encode($result));

            if (!isset($result['success']) || !$result['success']) {
                return response()->json([
                    'success' => false,
                    'message' => $result['errorMessage']
                ]);
            }

            $this->cancelEmail($result, $id);

            $executionEndTime = microtime(true);
            $seconds = $executionEndTime - $executionStartTime;
            Log::info($this->ip . ' TIME to email new cancel for ' . $id . ' is ' . $seconds . ' seconds');

            if (!isset($result['success']) || !$result['success']) {
                return response()->json([
                    'success' => false,
                    'message' => $result['errorMessage']
                ]);
            }

            $anaOptions = [
                'id' => $id,
            ];
            Log::info($this->ip . ' cancel anaOptions: ' . json_encode($anaOptions));

            if (isset($order->orderReference)) {
                if ('ALIPAY' == $order->payment_type || 'WECHATPAY' == $order->payment_type) {
                    Log::info($this->ip . ' Alipay and wechatpay cancel order reference ' . $order->orderReference);

                    $porder_tem = OrderTem::where('orderReference', $order->orderReference)
                        ->first();
                    $amount = $result['data']['refund'] * 100;
                    $transactionRef = $porder_tem->Reorderid;
                    Log::info($this->ip . ' transaction reference: ' . $transactionRef);

                    $transaction = CitconPay::inquire($transactionRef);
                    Log::info($this->ip . ' Citcon inquire response: ' . json_encode($transaction));

                    if (empty($transaction)) {
                        Log::info($this->ip . ' Cancel order cannot find transaction');

                        Order::where('orderReference', $order->orderReference)
                            ->update([
                                'payment_status' => 'PENDING',
                            ]);

                        return response()->json([
                            'success' => true,
                            // 'options' => $anaOptions,
                            'message' => "取消成功，citcon待退款"
                        ]);
                    }

                    $refundType = "Full";
                    if ($amount < $transaction->amount) {
                        $refundType = "Partial";
                    } else {
                        $amount = $transaction->amount;
                    }

                    $reason = 'cancel order refund';
                    $refundResult = $this->citconpayRefund($transaction, $reason, $refundType, $amount, $order);
                    if ('success' == $refundResult->status) {
                        $response = [
                            'success' => true,
                            // 'options' => $anaOptions,
                            'message' => 'Refund successfully'
                        ];
                    } else {
                        $response = [
                            'success' => false,
                            // 'options' => $anaOptions,
                            'message' => $refundResult->status
                        ];
                    }
                    return response()->json($response);
                } else {
                    $message = 'Payment release, will refund to your bank account soon';
                    if ($order->description == 'Prepay' && $order->payment_type !== 'PB') {
                        Log::info($this->ip . ' cancel cc order, order reference: ' . $order->orderReference);
                        $porder_tem = OrderTem::where('orderReference', $order->orderReference)
                            ->first();

                        $transactions = Payment::where('orderReference', $order->orderReference)
                            ->get();

                        foreach ($transactions as $transaction) {
                            if ($transaction != null) {
                                $type = "Partial";
                                $amount = $result['data']['refund'];
                                // $amount = 1; // test refund without actually cancel order

                                if ($amount >= $transaction->amount) {
                                    $type = "Full";
                                    $amount = $transaction->amount;
                                }

                                if (isset($porder_tem->amount)) {
                                    if (1 > ($porder_tem->amount - $amount)) {
                                        $type = "Full";
                                        $amount = $porder_tem->amount;
                                    }
                                }

                                if ($amount > 0) {
                                    $transaction->type = $type;
                                    $transaction->amount = $amount;
                                    $transaction->note = "取消订单";
                                    $transaction->partnerId = $order->partnerId;
                                    $message = $this->refund($transaction);
                                }
                            } else {
                                Log::info($this->ip . ' did not find payment');
                            }
                        }
                    }elseif($order->payment_type === 'PB'){
                        Log::info('PB paid order get cancelled, need refund: ' . $order->id);
                        try {
                            $msg = 'Order id : ' . $order->id;
                            Mail::raw($msg, function ($message) {
                                $message->to(config('staff-emails.ann'))
                                    ->subject('Paypal Btn need refund');
                            });
                        } catch (Exception $e) {
                            Log::error($this->ip . ' pb need refund email send failed, error: ' . $e->getMessage());
                        }
                    }

                    //for e-booking orders, if user cancel, send email to alex and hotel provider, for further assistance
                    if ($order->provider === 'EBooking') {
                        $this->ebCancelRemainderEmail($order);
                    }

                    return response()->json([
                        'success' => true,
                        // 'options' => $anaOptions,
                        'message' => $message
                    ]);
                }
            }
        }

        return response()->json([
            'success' => false,
            'message' => '未登录'
        ]);
    }

    /**
     * @param $result
     * @param $id
     */
    public function cancelEmail($result, $id)
    {
        if ($result['errorMessage'] != '') {
            $user = 'Client';
            $data = print_r($result, true);
            try {
                Mail::raw($data, function ($message) use ($user, $id) {
                    $message->subject('Cancel Error from ' . $user . ' (order#' . $id . ')');
                    $message->to(config('staff-emails.bd117'))->bcc(config('staff-emails.ann'));
                });
                Log::info($this->ip . ' sent Cancelled Error Email to us');
            } catch (Exception $e) {
                Log::error($this->ip . ' error with sending cancel error email to us ' . $e->getMessage());
            }
        } else {
            $data = $result['data'];
            $bookingId = $data['bookingId'];
            $totalPrice = $data['currencyCode'] . ' ' . $data['totalPrice'];
            $rooms = $data['rooms'][0];
            $roomType = $rooms['name'];
            $status = $data['status'];
            $displayId = empty($data['display_hotelId']) ? $data['hotelId'] : $data['display_hotelId'];
            $order = Order::find($id);

            $partnerId = $order->partnerId;
            $provider = $order->provider;
            $boardName = $order->rooms[0]['boardName'];
            $partnerName = 'Client';
            $bookerEmail = $order->bookerEmail;
            $bookerName = $order->bookerFirstName . ' ' . $order->bookerLastName;
            $hotelName = $order->hotelName;
            $orderRef = $order->orderPReference;
            $roomNumber = $order->room_number;
            $checkin = strtotime($order->checkinDate);
            $checkin = date('M d, Y', $checkin);
            $checkout = strtotime($order->checkoutDate);
            $checkout = date('M d, Y', $checkout);
            $address = str_replace(", ", "", $order->address) . ' ,' . $data['destination'];

            $paxesInfo = $rooms['paxes']['0'];
            $guestName = $paxesInfo['firstName'] . ' ' . $paxesInfo['lastName'];
            $paxesArray = $rooms['paxes'];
            $ad_total = count(array_filter($paxesArray, function ($pax) { return $pax['type'] == 'AD'; }));
            $ch_total = count(array_filter($paxesArray, function ($pax) { return $pax['type'] == 'CH'; }));
            $cancellationInfo = $rooms['cancellationPolicies']['0'];
            $cancellation = $cancellationInfo['end'];
            $cancellation = str_replace("T", " ", mb_substr($cancellation, 0, 19));
            $cancelTime = strtotime($cancellation);
            $creationTime = $order->created_at;
            $orderTime = strtotime($creationTime);

            if (!$cancellation) {
                $cancellation = "Non-Refundable";
            } elseif ($cancelTime < $orderTime) {
                $cancellation = "Non-Refundable";
            } else {
                $cancellation = "Free Cancellation before " . $cancellation;
            }

            $host = $order->http_host ? $order->http_host : '';
            $locale = $order->user_language ? $order->user_language : 0;

            Log::info($this->ip . ' locale language of user: ' . $locale);
            if( strrpos($host, 'usitour') > -1&& $locale !== 2) {
                $locale = 1;
            }
            if ($locale == 1) {
                $user_language = 'en';
            } elseif ($locale == 2) {
                $user_language = 'tw';
            } else {
                $user_language = 'cn';
            }

            if (strrpos($host, '117book') !== false) {
                $hostName = '117book';
            } elseif (strrpos($host, 'usitour') !== false) {
                $hostName = 'usitour';
            } elseif (strrpos($host, 'supervacation') !== false) {
                $hostName = 'supervacation';
            } else {
                $hostName = 'usitrip';
            }

            Log::info($this->ip . ' Cancel Email Start');

            try {
                $emailto = array_merge($this->techEmails, $this->otherEmails);
                if (config('app.env') == 'local') {
                    $emailto = config('app.default_mail_to');
                }

                Mail::queue('emails.new-cancel', [
                        'bookingId' => $bookingId, 'partnerId' => $partnerId, 'partnerName' => $partnerName,
                        'status' => $status, 'provider' => $provider, 'roomType' => $roomType,
                        'totalPrice' => $totalPrice, 'hotelId' => $displayId, 'hotelName' => $hotelName,
                        'orderRef' => $orderRef
                ], function ($message) use ($partnerName, $emailto) {
                    $message->subject('New Cancel Notice from ' . $partnerName . ' !')->to($emailto);
                });

                //for e-booking orders, if user cancel, send email to alex and hotel provider, for further assistance
                if ($provider === 'EBooking') {
                    $hotel_user = EbHotelUser::where('hotelId', $order->hotelId)->first();

                    if (!empty($hotel_user)) {
                        $hotel_user_id = $hotel_user->user_id;
                        $eb_user = EbUser::where('user_id', $hotel_user_id)->first();
                        $eb_user_email = !empty($eb_user) ? $eb_user->email : '';
                    } else {
                        $eb_user_email = '';
                    }

                    if (config('app.env') == 'local') {
                        $eb_user_email = config('app.default_mail_to');
                    }

                    Log::info('ready for E-Booking provider email' . $eb_user_email);

                    $data = [
                        'toMail' => $eb_user_email,
                        'hotelName' => $hotelName,
                        'orderId' => $bookingId,
                        'checkin' => $checkin,
                        'checkout' => $checkout,
                        'guestName' => $guestName,
                        'roomCount' =>$roomNumber,
                        'paxes' => $ad_total .'adults, ' .$ch_total. 'children',
                        'cancellationPolicy' => $cancellation,
                        'contractPrice' => $order->totalOriginalNetPrice,
                    ];

                    $job = (new SendReminderCancelActionEmail($data))->delay(config('app.email_delay'));
                    $this->dispatch($job);
                }

                Log::info($this->ip . ' sent Cancelled Email to us');

                $data = ['host' => $hostName, 'language' => $user_language, 'partner' => $partnerId,
                    'orderId' => $bookingId, 'checkin' => $checkin, 'checkout' => $checkout, 'hotelName' => $hotelName,
                    'reference' => $orderRef, 'guest' => $guestName, 'roomCount' => $roomNumber,
                    'totalPrice' => $totalPrice, 'adNumber' => $ad_total, 'chNumber' => $ch_total,
                    'roomType' => $roomType, 'boardType' => $boardName, 'cancellation' => $cancellation,
                    'hotelAddress' => $address, 'bookerEmail' => $bookerEmail, 'bookerName' => $bookerName];

                $this->post('/sendCancelledEmail ', $data);

                Log::info($this->ip . ' sent Cancelled Email to hotel');
            } catch (Exception $e) {
                Log::error($this->ip . ' send cancel email error: ' . $e->getMessage());
            }
        }
    }

    /**
     * @param $transaction
     * @param $reason
     * @param $refundType
     * @param $amount
     * @param null $order
     * @return bool|mixed
     */
    public function citconpayRefund($transaction, $reason, $refundType, $amount, $order = null)
    {
        $refundResult = CitconPay::refund($transaction->currency, $transaction->id, $transaction->amount, $reason);
        Log::info($this->ip . ' Citcon refund response: ' . json_encode($refundResult));

        $refund = new Refund();
        if (empty($refundResult)) {
            return false;
        } elseif ($refundResult->status == 'success') {
            $refund->orderReference = isset($order) ? $order->orderReference : '';
            $refund->partnerId = isset($order) ? $order->partnerId : $this->partnerId;
            $refund->refundType = $refundType;
            $refund->amount = $amount;
            $refund->transactionId = $transaction->id;
            $refund->currency = $transaction->currency;
            $refund->note = 'cancel order, citcon refund';
            $refund->response = json_encode($refundResult);
            $refund->refundTransactionId = $refundResult->id;
            $refund->totalRefundedAmount = $refundResult->refunded;
            $name = $order->bookerFirstName . " " . $order->bookerLastName;
            $refund->name = $name;

            try {
                $refund->save();
            } catch (Exception $e) {
                Log::error($this->ip . ' refund success, save citcon refund error: ' . $e->getMessage());
            }
        } else {
            Log::info($this->ip . ' refund failed, reason: ' . $refundResult->status);
            $refund->orderReference = $order->orderReference;
            $refund->partnerId = $order->partnerId;
            $refund->refundType = $refundType;
            $refund->amount = $amount;
            $refund->transactionId = $transaction->id;
            $refund->currency = $transaction->currency;
            $refund->note = 'cancel order, citcon refund';
            $refund->response = json_encode($refundResult);
            $refund->totalRefundedAmount = $refundResult->refunded;
            $name = $order->bookerFirstName . " " . $order->bookerLastName;
            $refund->name = $name;

            try {
                $refund->save();
            } catch (Exception $e) {
                Log::error($this->ip . ' refund failed, save citcon refund error: ' . $e->getMessage());
            }
        }

        return $refundResult;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkReservation(Request $request)
    {
        $order = Order::with('rooms')
            ->where('id', $request->input('id'))
            ->first();
        if ($order == null) {
            return response()->json([
                'success' => false,
                'message' => '没有这个订单，订单号' . $request->input('id')
            ]);
        }
        $user = User::find($order->partnerId);
        $api = new HotelAPI($user);
        $result = $api->checkReservation($order->orderReference);
        if (!$result['success']) {
            return response()->json([
                'success' => false,
                'message' => $result['errorMessage']
            ]);
        }
        $status = $result['data']['status'];

        $message = 'Request status success';
        if ($status == 'REJECTED') {
            if ($user->paymentType == 'Prepay') {
                $payment = Payment::where('orderReference', $order->orderReference)
                    ->first();
                if ($payment != null) {
                    $type = "Partial";
                    $amount = Order::where('orderReference', $order->orderReference)
                        ->first()->totalPrice;
                    if ($amount >= $payment->amount) {
                        $type = "Full";
                        $amount = $payment->amount;
                    }

                    if ($amount > 0) {
                        $payment->type = $type;
                        $payment->amount = $amount;
                        $payment->note = "check reservation rejected";
                        $message = $this->refund($payment);
                    }
                }
            }
        } elseif ($status == 'ON REQUEST') {
            return response()->json([
                'success' => false,
                'message' => 'Order still on request status'
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => $message
        ]);
    }

    /**
     * @param $amount
     * @param $currency
     * @param string $out_code
     * @return float|int
     */
    private function changeCurrency($amount, $currency, $out_code = 'USD')
    {
        $payment = $this->httpGet("http://post.usitrip.com/index.php?mod=api_public&action=currencyConversion&money=$amount&source_code=$currency&out_code=$out_code");
        if ($payment === false) {
            return 0;
        }

        $payment = json_decode($payment, true);
        if ($payment['Value'] !== false) {
            return round($payment['Value'], 2);
        }

        return 0;
    }

    /**
     * in use by business search
     * @param $destinationId
     * @param $keyword
     * @param int $count
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHotelOptions($destinationId, $keyword, $count = 10)
    {
        $db = new HotelDB();
        $user = User::find($this->partnerId);
        $options = $db->getHotelOptions($destinationId, $keyword, $count, $user);

        return response()->json($options->toArray());
    }

    /**
     * for ad slider to show list page with scenicId
     * @param Request $request
     * @param $desId
     * @param string $start
     * @param string $end
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function scenic(Request $request, $desId, $start = '', $end = '')
    {
        $scenicId = str_replace('.html', '', $desId);

        $ScenicArea = ScenicArea::where('id', $scenicId)
            ->first();
        $destinationId = $ScenicArea['destination_id'];

        if (session('language', 0) == 0) {
            $scenicName = $ScenicArea['name_full'];
        } else {
            $scenicName = $ScenicArea['name'];
        }

        $destination = Destination::where('desId', $destinationId)
            ->first();

        if (empty($destination)) {
            abort('404', 'Invalid Destination');
        }

        $destinationName = $destination->desName;
        $destinationCity = $destination->city;

        $checkin = empty($start) ? Carbon::parse('next sunday')
            ->addDays(7)
            ->toDateString() : $start;
        $checkout = empty($end) || (strtotime($end) < strtotime($checkin)) ? Carbon::parse($checkin)
            ->addDays(1)
            ->toDateString() : $end;

        $searchInfo = $this->generateStaticParams($checkin, $checkout, $destinationName);

        if ($request->has('no')) {
            session(['sales_no' => $request->input('no')]);
        }

        $dayCount = $this->calculateDays($checkin, $checkout);
        $destinationName = $searchInfo['destinationName'];
        $occupancies = $searchInfo['occupancies'][0];
        $roomCount = $occupancies['rooms'];
        $adultCount1 = $occupancies['adults'];
        $childCount1 = $occupancies['children'];
        $showAllDiscount = $searchInfo['showAllDiscount'];
        $childAges = [12];
        $isMobile = $this->isMobileDevice();

        $data = [
            'optionName' => 'list',
            'destinationName' => $destinationName,
            'destinationCity' => $destinationCity,
            'destinationId' => 'D' . $destinationId,
            'scenicId' => $scenicId,
            'scenicName' => $scenicName,
            'checkin' => $checkin,
            'checkout' => $checkout,
            'roomCount' => $roomCount,
            'dayCount' => $dayCount,
            'adultCount1' => $adultCount1,
            'childCount1' => $childCount1,
            'childAges' => $childAges,
            'showAllDiscount' => $showAllDiscount, // to mark filter to show all discounted hotels at first or not
            'orderBy' => $request->has('orderBy') ? $request->input('orderBy') : 'discount',
            'sort' => $request->has('sort') ? $request->input('sort') : 'desc',
            'key' => $request->has('key') ? $request->input('key') : '',
            'pagePos' => $request->has('pagePos') ? $request->input('pagePos') : '1',
            'rating' => $request->has('rating') ? $request->input('rating') : '',
            'price' => $request->has('price') ? $request->input('price') : '',
            'scenicArea' => $request->has('scenicArea') ? $request->input('scenicArea') : '', // passed when share url
            'chainNames' => $request->has('chainNames') && !empty($request->input('chainNames')) ? $request->input('chainNames') : '',
        ];

        if ($isMobile) {
            return view('mobile.list', $data); // new mobile list
        }

        return view('list', $data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logoutUsitrip()
    {
        session(['user_email' => '']);

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * TODO:该功能等java的failed重发做好了以后再使用替代newOrderEmail, 需要測試是否tw的有效
     * @param $tempInfo
     * @param $result
     * @return \Illuminate\Http\JsonResponse
     */
    public function newJavaOrderEmail($tempInfo, $result)
    {
        //Email New Order
        $executionStartTime = microtime(true);
        $orderTable = Order::where('code', $result['code'])
            ->leftjoin('hotel', 'porder.hotelId', '=', 'hotel.hotelId')
            ->leftJoin('destination', 'hotel.desId', '=', 'destination.desId')
            ->leftJoin('porder_pax', 'porder.orderReference', '=', 'porder_pax.orderReference')
            ->leftJoin('porder_room', 'porder.orderReference', '=', 'porder_room.orderReference')
            ->first();
        $partnerId = $orderTable->partnerId;
        $hotelName = $tempInfo['goods_name'];
        $emailRaw = $result;
        $orderRef = $orderTable->orderPReference;
        $creationTime = $orderTable->created_at;
        $bookingId = $emailRaw['bookingId'];
        $totalPrice = $emailRaw['currencyCode'] . ' ' . $emailRaw['totalPrice'];
        $bookerEmail = $emailRaw['bookerEmail'];
        $bookerName = $emailRaw['bookerFirstName'] . ' ' . $emailRaw['bookerLastName'];
        $roomInfo = $emailRaw['rooms']['0'];
        $boardName = $roomInfo['boardName'];
        $roomType = $roomInfo['name'];
        $cancellationInfo = $roomInfo['cancellationPolicies']['0'];
        $cancellation = $cancellationInfo['end'];
        $cancellation = str_replace("T", " ", mb_substr($cancellation, 0, 19));
        $cancelTime = strtotime($cancellation);
        $orderTime = strtotime($creationTime);
        if (!$cancellation) {
            $cancellation = "Non-Refundable";
        } elseif ($cancelTime < $orderTime) {
            $cancellation = "Non-Refundable";
        } else {
            $cancellation = "Free Cancellation before " . $cancellation;
        }
        $roomNumber = $orderTable->room_number;
        $dest1 = $orderTable->address;
        $dest2 = $result['destination'];
        $dest = str_replace(", ", "", $dest1) . ", " . $dest2;
        $checkin = strtotime($emailRaw['checkInDate']);
        $checkin = date('M d, Y', $checkin);
        $checkout = strtotime($emailRaw['checkOutDate']);
        $checkout = date('M d, Y', $checkout);
        $guestInfo = $roomInfo['paxes']['0'];
        $firstName1 = $guestInfo['firstName'];
        $lastName1 = $guestInfo['lastName'];
        $guest = $firstName1 . ' ' . $lastName1;
        $paxesArray = $roomInfo['paxes'];
        $ad_total = count(array_filter($paxesArray, function ($pax) { return $pax['type'] == 'AD'; }));
        $ch_total = count(array_filter($paxesArray, function ($pax) { return $pax['type'] == 'CH'; }));
        $locale = $orderTable->user_language ? $orderTable->user_language : session('language', 0);
        Log::info($this->ip . ' locale language of user: ' . $locale);

        $host = $orderTable->http_host;
        if( strrpos($host, 'usitour') > -1&& $locale !== 2) {
            $locale = 1;
        }
        if ($locale == 1) {
            $user_language = 'en';
            if ($result['status'] == 'CONFIRMED') {
                $status = " is CONFIRMED";
            } else {
                $status = " is IN PROCESS";
            }
        } elseif($locale == 2){
            $user_language = 'tw';
            if ($result['status'] == 'CONFIRMED') {
                $status = "已經被確認";
            }else {
                $status = "正在處理中";
            }
        } else {
            $user_language = 'cn';
            if ($result['status'] == 'CONFIRMED') {
                $status = "已经被确认";
            } else {
                $status = "正在处理中";
            }
        }

        if (strrpos($host, '117book') !== false) {
            $hostName = '117book';
        } elseif (strrpos($host, 'usitour') !== false) {
            $hostName = 'Usitour';
        } else {
            $hostName = 'Usitrip';
        }

        try {
            $booker = json_decode($tempInfo->booker, true);
            Log::info('for sms $booker info: ' . json_encode($booker));
            //For all new order, send sms
            $input_country_code = isset($booker['countryCodePhone']) ? $booker['countryCodePhone'] : $booker['phone_country'];
            $input_mobile = isset($booker['validPhoneNumber']) ? $booker['validPhoneNumber'] : $booker['phone'];
            $code = SinaUrl::getShort($orderTable->voucher());
            $sendSms = new sendMessage();
            $sendSms->sendOrderSms($input_country_code, $input_mobile, $code);
        } catch (Exception $e) {
            Log::error($this->ip . ' send new order message error: ' . $e->getMessage());
        }

        try {
            $data = [
                'host' => $hostName,
                'language' => $user_language,
                'partner' => $partnerId,
                'orderId' => $bookingId,
                'status' => $status,
                'voucher' => $orderTable->voucher(),
                'checkin' => $checkin,
                'checkout' => $checkout,
                'hotelName' => $hotelName,
                'reference' => $orderRef,
                'guest' => $guest,
                'roomCount' => $roomNumber,
                'totalPrice' => $totalPrice,
                'adNumber' => $ad_total,
                'chNumber' => $ch_total,
                'roomType' => $roomType,
                'boardType' => $boardName,
                'cancellation' => $cancellation,
                'hotelAddress' => $dest,
                'bookerEmail' => $bookerEmail,
                'bookerName' => $bookerName,
            ];
            $this->post('/sendNewOrderEmail', $data);
            Log::info($this->ip . ' 发送预定邮件');

            return response()->json([
                'success' => true,
            ]);
        } catch (Exception $e) {
            Log::error($this->ip . ' send new order email error: ' . $e->getMessage());
        }
        $executionEndTime = microtime(true);
        //The result will be in seconds and milliseconds.
        $seconds = $executionEndTime - $executionStartTime;
        Log::info($this->ip . ' TIME to email new Order for ' . $bookingId . ' is ' . $seconds . ' seconds');
        return response()->json([
            'success' => false,
        ]);
    }

    /**
     * @param $msg
     */
    private function payError($msg)
    {
        if (config('app.env') == 'production') {
            try {
                Log::info($this->ip . ' cc pay failed: ' . json_encode($msg));
                Mail::raw($msg, function ($message) {
                    $message->subject('Credit Card Error From ' . $_SERVER['HTTP_HOST'] . ' Site');
                    $message->to(config('staff-emails.henry'))
                        ->cc(config('staff-emails.ann'))
                        ->cc(config('staff-emails.jie'));
                    if (strrpos($_SERVER['HTTP_HOST'], "117book") > -1 || $_SERVER['HTTP_HOST'] == 'B2B') {
                        //发给销售628
                        $message->cc(config('staff-emails.bdcn'));
                    }
                });
            } catch (Exception $e) {
                Log::error($this->ip . ' cc pay error email failed: ' . $e->getMessage());
            }
        } else {
            //测试环境print log
            Log::info($this->ip . 'under test env,' . $_SERVER['HTTP_HOST'] . ' cc pay failed: ' . json_encode($msg));
        }
    }

    /**
     * @param $authTransaction
     * @param $ccInfo
     * @return \App\Classes\PaypalNew\PayResultNew|array
     */
    private function paypalDovoid($authTransaction, $ccInfo)
    {
        $doVoidRes = PaypalNew::doVoid($authTransaction->transactionId, $authTransaction->amount);
        if (!$doVoidRes['success']) {
            return [
                'success' => false,
                'message' => '网络错误' . $doVoidRes['errorNo'] . '，错误描述：' . $doVoidRes['errorMessage'],
            ];
        }

        $doVoidResult = new PayResultNew($doVoidRes['request'], $doVoidRes['response']);
        $doVoidResult->operationType = "Book failed and didn't find reservation when check, doVoid";
        $doVoidResult->operationResult = json_encode($doVoidRes['response']);
        $doVoidResult->amount = $authTransaction->amount;
        $doVoidResult->currency = $authTransaction->currency;
        $doVoidResult->transaction_type = 'V';
        $this->savePaymentHistory($doVoidResult, $ccInfo);

        return $doVoidResult;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $Reorderid
     * @return \Illuminate\Http\JsonResponse
     */
    public function extraSubmitPayment(Request $request, $Reorderid) // similar to submitPayment
    {
        Log::info($this->ip . ' reorderid is ' . $Reorderid);
        $tempInfo = OrderTem::where('Reorderid', $Reorderid)
            ->first();

        // cache placing order key to prevent book two times
        $placingOrderKey = $Reorderid . 'placingOrder';
        // API568 如果是重复订单用ER1414返回
        if (Cache::has($placingOrderKey) || !empty($tempInfo->orderReference)) {
            return response()->json([
                'success' => false,
                'errorId' => 'ER1414',
                'errorMessage' => Order::where('orderReference',$tempInfo->orderReference)->first()->id
            ]);
        }

        Cache::put($placingOrderKey, true, $this->cacheLifeTime);

        if (empty($tempInfo)) {
            Cache::forget($placingOrderKey); // remove placing order key
            if (session('language', 0) == 0) {
                $plzSameCard = '请使用之前预付款的同一张卡';
            } else {
                $plzSameCard = 'Please use the same card';
            }

            return response()->json([
                'success' => false,
                'errorId' => 'CC15006', // Todo: which id should we use to inform user to use the same card
                'errorMessage' => $plzSameCard,
            ]);
        }

        $specialRequest = $tempInfo->special_request;

        // credit card number basic check
        $number = $request->Input('number');
        $sus_order = SuspiciousOrder::where('Reorderid', $Reorderid)
            ->get()
            ->last();
        $oldTransaction = Payment::where('transactionId', $sus_order->payment_id)
            ->first();

        if (strpos($number, $oldTransaction->cc_number) == false) {
            Cache::forget($placingOrderKey); // remove placing order key
            if (session('language', 0) == 0) {
                $plzSameCard = '请使用之前预付款的同一张卡';
            } else {
                $plzSameCard = 'Please use the same card';
            }

            return response()->json([
                'success' => false,
                'errorId' => 'CC15006', // todo: which id should we use to inform user to use the same card
                'errorMessage' => $plzSameCard,
            ]);
        }

        $amount = $tempInfo->amount;
        $oldccInfo = json_decode($sus_order->cc_info, true);
        $creditCardInfo = new CreditCardInfoNew($oldccInfo['firstName'], $oldccInfo['lastName'],
            $request->input('cardType'),
            $request->input('number'), $request->input('countryCodePhone'), $request->input('phone'),
            $request->input('month'),
            $request->input('year'), $request->input('CVC'), $oldccInfo['street'], $oldccInfo['street2'],
            $oldccInfo['city'], $oldccInfo['state'],
            $oldccInfo['zip'], $oldccInfo['countryCode']);

        if (config('app.env') == 'local') {
            $amount = 1;
        }

        $authRes = PaypalNew::auth($creditCardInfo, $amount);
        if (!$authRes['success']) {
            Cache::forget($placingOrderKey); // remove placing order key

            return response()->json([
                'success' => false,
                'errorId' => 'IT1001',
                'errorMessage' => '网络错误' . $authRes['errorNo'] . '，错误描述：' . $authRes['errorMessage'],
            ]);
        }

        $authTransaction = new PayResultNew($authRes['request'], $authRes['response']);
        Log::info($this->ip . 'extra Payment paypal auth result: ' . json_encode($authTransaction));
        if ($authTransaction->result != 0) {
            $authTransaction->booker_email = $tempInfo->user_email;
            $authTransaction->operationType = "Credit Card Auth Failed";
            $authTransaction->operationResult = json_encode($authRes['response']);
            $authTransaction->amount = $amount;
            $authTransaction->currency = 'USD';
            $authTransaction->transaction_type = 'A';
            $this->payError('Credit Card Auth Failed. Amount: ' . $amount . ', from user: ' . $tempInfo->user_email . ', reason: ' . $authTransaction->respMsg);
            $this->savePaymentHistory($authTransaction, $creditCardInfo);

            $errDetail = $authTransaction->respMsg;

            Log::info($this->ip . ' $errDetail' . $errDetail);

            DB::table("error_bankcard")
                ->insert([
                    'partnerId' => $tempInfo->partnerId,
                    'cardInfo' => $request->Input('number')
                ]);

            Cache::forget($placingOrderKey); // remove placing order key

            return response()->json([
                'success' => false,
                'errorId' => "CC15005",
                'message' => $errDetail ? 'Auth failed, please check your input information' : $errDetail
            ]);
        }

        $captureRes = PaypalNew::capture($authTransaction->transactionId, $amount);
        if (!$captureRes['success']) {
            $this->payError('Network caused cc capture failed:' . json_encode($captureRes));
            $doVoidResult2 = $this->paypalDovoid($authTransaction, $creditCardInfo);
            Log::info($this->ip . ' Paypal capture failed, doVoid result: ' . json_encode($doVoidResult2));

            Cache::forget($placingOrderKey); // remove placing order key

            return response()->json([
                'success' => false,
                'errorId' => '',
                'errorMessage' => '网络错误' . $captureRes['errorNo'] . '，错误描述：' . $captureRes['errorMessage'],
            ]);
        }

        $captureTransaction = new PayResultNew($captureRes['request'], $captureRes['response']);
        Log::info($this->ip . ' paypal capture result: ' . json_encode($captureTransaction));
        if ($captureTransaction->result != 0) {
            $this->payError('credit card auth succeeded but capture failed, detail: ' . json_encode($captureRes));
            $doVoidResult3 = $this->paypalDovoid($authTransaction, $creditCardInfo);
            Log::info($this->ip . ' Paypal capture failed, doVoid result: ' . json_encode($doVoidResult3));

            Cache::forget($placingOrderKey); // remove placing order key

            return response()->json([
                'success' => false,
                'errorId' => "CC15005",
                'message' => 'error:请填写正确的信用卡信息'
            ]);
        }

        $captureTransaction->booker_email = $tempInfo->user_email;
        $captureTransaction->operationType = "Capture Success";
        $captureTransaction->operationResult = json_encode($captureRes['response']);
        $captureTransaction->amount = $amount;
        $captureTransaction->currency = 'USD';
        $captureTransaction->type = 'Full';
        $captureTransaction->transaction_type = 'D';
        $new_p_id = $this->savePaymentHistory($captureTransaction, $creditCardInfo);
        $p_ids[] = $new_p_id;

        $rooms = json_decode($tempInfo->rooms, true);
        $booker = json_decode($tempInfo->booker, true);
        $user = User::find($this->partnerId);
        $api = new HotelAPI($user);

        $bookRoomResult = $api->bookRooms($booker, $rooms, $specialRequest);

        if (!isset($bookRoomResult['success']) || !$bookRoomResult['success']) {
            if (isset($bookRoomResult['errorId']) && $bookRoomResult['errorId'] === 'IT1001') {
                Cache::forget($placingOrderKey); // place order failed, remove placing order key

                return response()->json([
                    'success' => false,
                    'errorId' => $bookRoomResult['errorId'],
                ]);
            }

            $room = $rooms[0];
            $orderCReference = $room['roomReference'];
            $order = Order::where('orderCReference', $orderCReference)
                ->first();
            if ($order != null) {
                $porderReference = $order->orderReference;
                $bookRoomResult = $api->checkReservation($porderReference);
            }

            // book failed
            if (!isset($bookRoomResult['success']) || !$bookRoomResult['success']) {
                Cache::forget($placingOrderKey); // place order failed, remove placing order key

                $old_p_id = Payment::where('transactionId', $sus_order->payment_id)
                    ->first()->id;
                $p_ids[] = $old_p_id;

                if (isset($bookRoomResult['errorId']) && $bookRoomResult['errorId'] === 'ER1415') { // if it's duplicate booking error from api
                    return response()->json([
                        'success' => false,
                        'errorId' => $bookRoomResult['errorId'],
                        'Reorderid' => $Reorderid,
                        'p_id' => $p_ids,
                    ]);
                }

                $captureTransaction->first_name = $creditCardInfo->firstName;
                $captureTransaction->last_name = $creditCardInfo->lastName;
                $captureTransaction->note = 'extra pay book failed';
                //退回新charge的款
                $this->refund($captureTransaction);

                // 如果extra pay没订上，isDeclined
                $sus_order->update([
                    'isLocked' => 0,
                    'isDeclined' => 1,
                    'commend' => Carbon::now() . ': 补差价下单失败，自动拒单退全款. (' . $sus_order->sales_no . ')&#013;' . $sus_order->commend,
                ]);

                $this->sendFraudEmail($sus_order, $Reorderid);

                return response()->json([
                    'success' => false,
                    'errorId' => isset($bookRoomResult['errorId']) ? $bookRoomResult['errorId'] : '',
                    'errorMessage' => isset($bookRoomResult['errorMessage']) ? $bookRoomResult['errorMessage'] : 'Booking failed'
                ]);
            }
        }
        $sus_order->update([
            'isLocked' => 0,
            'isDone' => 1,
        ]);
        $bookRoomResult = $bookRoomResult['data'];
        if($bookRoomResult === null){
            $sus_order->update([
                'commend' =>Carbon::today()->format('m/d'). ' 额外付款成功，预订成功，但是数据存储失败，技术会实时向上游请求数据 (' . $sus_order->sales_no .')' . '&#013;' .$sus_order->commend,
            ]);
            $pricelineMissData = new BookController();
            $pricelineMissData->pricelineBookSuccessDataFailed($Reorderid);
        }else{
            $this->updatePaymentHistory($captureTransaction, $bookRoomResult);
            $oldTransaction->orderReference = $bookRoomResult['orderReference'];
            $oldTransaction->save();

            $tempInfo->orderReference = $bookRoomResult['orderReference'];
            $tempInfo->code = $bookRoomResult['code'];
            $tempInfo->save();
            Order::where('orderReference', $bookRoomResult['orderReference'])
                ->update([
                    'infact_cost' => $tempInfo->amount,
                    'payment_type' => 'CC',
                    'payment_status' => 'PAID',
                    'remark' => $tempInfo->remark,
                    'source' => $tempInfo->source,
                    'source_pos' =>  isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '',
                    'special_request' => $specialRequest,
                    'sales_no' => $tempInfo->sales_no,
                    'user_email' => $tempInfo->user_email,
                    'display_hotelId' => $tempInfo->display_hotelId,
                    'http_host' => $this->host, //add HTTP host for order table. --052318 Ann
                    'user_language' => $sus_order->language, //add user_language for order table. --060518 Ann
                ]);

            $order = Order::where('orderReference', $bookRoomResult['orderReference'])
                ->first();

            if (!empty($order) && $order->provider === 'EBooking') {
                Log::info('2: extraSubmitPayment eb email send' . $order->id);
                $this->sendEbEmail($order);
            }

            $this->newOrderEmail($tempInfo, $bookRoomResult);

            //预定完成，更新infusionsoft contact，以发送feedback邮件
            $langString = $booker['language'] === 0 ? 'cn' : 'en';
            $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'hotel.usitrip.com';
            $this->dispatch(new TriggerFeedback($booker['email'], $langString, $domain));

            Cache::forget($placingOrderKey); // remove placing order key

            return response()->json([
                'success' => true,
                'voucherUrl' => url('voucher/' . $bookRoomResult['code'])
            ]);
        }
    }

    /**
     * @param $checkUserInfo
     * @param $booker
     * @param $rooms
     * @param $creditCardInfo
     * @param $searchInfo
     * @return array|mixed
     */
    private function checkSuspiciousOrder($checkUserInfo, $booker, $rooms, $creditCardInfo, $searchInfo)
    {
        $language = (session('language', 0) == 0) ? 'ch' : 'en';
        $hotelIds[] = $checkUserInfo['hotelId'];
        $number = $creditCardInfo->number;
        $ccInfo = [
            'firstName' => $creditCardInfo->firstName,
            'lastName' => $creditCardInfo->lastName,
            'country' => $creditCardInfo->countryCode,
            'last8' => substr($number, -8, 8),
        ];
        $roomDetails = $rooms[0]['roomDetails'];

        $checkUserData = [
            'language' => $language,
            'ipAddress' => $checkUserInfo['clientIp'],
            'checkIn' => isset($searchInfo['checkin']) ? $searchInfo['checkin'] : '',
            'checkOut' => isset($searchInfo['checkout']) ? $searchInfo['checkout'] : '',
            'hotelId' => $hotelIds,
            'occupancies' => $rooms[0]['paxes'],
            'rooms' => $rooms,
            'booker' => $booker,
            'cancelDate' => isset($roomDetails['cancelDate']) ? $roomDetails['cancelDate'] : '',
            'sessionKey' => $checkUserInfo['sessionKey'],
            'creditCard' => $ccInfo,
        ];
        $user = $user = User::find($this->partnerId);
        $api = new HotelAPI($user);
        $checkUserResult = $api->checkUser($checkUserData);

        return $checkUserResult;
    }

    /**
     * @param $Reorderid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function extraPayView($Reorderid) //suspicious fraud extra pay
    {
        $sus_order = SuspiciousOrder::where('Reorderid', $Reorderid)
            ->get()
            ->last();
        if (empty($sus_order)) {
            return redirect('/');
        }

        if ($sus_order->isDeclined && $sus_order->isDeclined == 1) {
            $message = "该订单已被拒绝";
            abort('404', $message);
        }

        if (empty($sus_order) || $sus_order->isDone) {
            abort('404', "Order is completed");
        }
        $tempInfo = OrderTem::where('Reorderid', $Reorderid)
            ->first();
        $rooms = json_decode($tempInfo->rooms, true);
        $payment = Payment::where('transactionId', $sus_order->payment_id)
            ->first();

        $blade = 'extra-pay';
        if ($this->isMobileDevice()) {
            $blade = 'mobile.extra-pay';
        }

        return view($blade, [
            'optionName' => 'extraPay',
            'Reorderid' => $Reorderid,
            'roomDetails' => $rooms[0]['roomDetails'],
            'ccNumber' => $payment['cc_number'],
        ]);
    }

    /**
     * @param $suspiciousOrder
     * @param $Reorderid
     * @return \Illuminate\Http\JsonResponse
     */
    private function sendFraudEmail($suspiciousOrder, $Reorderid)
    {
        $transaction = Payment::where('transactionId', $suspiciousOrder->payment_id)
            ->first();
        $porderTem = OrderTem::where('Reorderid', $Reorderid)->first();
        $hotelName = $porderTem->goods_name;
        $transaction->note = 'cancel fraud process order';
        $message = $this->refund($transaction);
        Log::info($this->ip . ' suspicious order has been refunded, ' . $message);

        $locale = $suspiciousOrder->language;
        $host = $suspiciousOrder->http_host;

        $booker = $suspiciousOrder['booker'];
        $bookerArray = json_decode($booker, true);
        $bookerEmail = $bookerArray['email'];

        $langId = 0;
        $hostName = 'Ustrip';
        if(strrpos($host, 'usitour') !== false) {
            $hostName = 'Usitour';
        }
        $fraudEmailSubject = $hostName . '走四方酒店-订单失敗： ' . $Reorderid;

        if ($locale == 2) {
            $langId = 2;
            if (strrpos($host, 'supervacation') !== false) {
                $fraudEmailSubject = '美國亞洲旅遊hotel.supervacation.net酒店-訂單失敗：' . $Reorderid;
            } else {
                $fraudEmailSubject = $hostName . '走四方酒店-訂單失敗：' . $Reorderid;
            }
        } elseif ($locale == 1) {
            $fraudEmailSubject = $hostName . '- Your Order is been Rejected: ' . $Reorderid;
        }
        try {
            Mail::send('emails.order-failed', [
                'Reorderid' => $Reorderid,
                'hotelName' => $hotelName,
                'is_aa' => strrpos($host, "supervacation"),
                'langId' => $langId,
                'hostName' => $hostName
            ], function ($message) use ($fraudEmailSubject, $bookerEmail) {
                $message->subject($fraudEmailSubject)
                    ->to($bookerEmail)
                    ->bcc(config('staff-emails.ann'));
            });
            return response()->json([
                'success' => true,
            ]);
        } catch (Exception $e) {
            Log::error($this->ip . ' send fraud email failed' . $e->getMessage());
            return response()->json([
                'success' => false,
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitPayment(Request $request)
    {
        $Reorderid = $request->get('Reorderid');
        $tempInfo = OrderTem::where('Reorderid', $Reorderid)
            ->first();
        $rooms = json_decode($tempInfo->rooms, true);
        $redisBookRoomKey = $tempInfo['display_hotelId'] . ':' . urlencode($rooms[0]['roomDetails']['roomReference']) . 1;
        //check if session Expired, if cache has no redisBookRoomKey, then do not process to auth or capture money
        if (Cache::has($redisBookRoomKey)) {
           // b c has different card and different payment process steps, america asia share with c
            if ($request->getHost() === config('app.b_host')) {
                return $this->NoCreditcardsPay($request);
            } else {
                return $this->processPayment($request);
            }
        } else {
            return response()->json([
                'success' => false,
                'errorId' => 'ER1004',
                'errorMessage' => 'Session timeout or not exist, the time limit is 30 minutes.'
            ]);
        }
    }

    /**
     * c site paypal payment
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function processPayment(Request $request)
    {
        $Reorderid = $request->input('Reorderid');
        $tempInfo = OrderTem::where('Reorderid', $Reorderid)
            ->first();

        // cache placing order key to prevent book two times
        $placingOrderKey = $Reorderid . 'placingOrder';
        // API568 如果是重复订单用ER1414返回
        if (Cache::has($placingOrderKey) || !empty($tempInfo->orderReference)) {
            return response()->json([
                'success' => false,
                'errorId' => 'ER1414',
                'errorMessage' => Order::where('orderReference',$tempInfo->orderReference)->first()->id
            ]);
        }

        Cache::put($placingOrderKey, true, $this->cacheLifeTime);

        $specialRequest = $tempInfo->special_request;
        $creditCardInfo = new CreditCardInfoNew($request->Input('firstName'), $request->Input('lastName'),
            $request->Input('cardType'),
            $request->Input('number'), $request->input('countryCodePhone'), $request->input('phone'),
            $request->Input('month'),
            $request->Input('year'), $request->Input('CVC'), $request->Input('street'), $request->Input('street2'),
            $request->Input('city'),
            $request->Input('state'), $request->Input('zip'), $request->Input('countryCode'));

        // credit card number basic check
        $number = $request->Input('number');
        settype($number, 'string');
        $sumTable = array(
            array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
            array(0, 2, 4, 6, 8, 1, 3, 5, 7, 9)
        );
        $sum = 0;
        $flip = 0;
        for ($i = strlen($number) - 1; $i >= 0; $i--) {
            $sum += $sumTable[$flip++ & 0x1][$number[$i]];
        }
        if (0 == ($sum % 10)) {
        } else {
            DB::table("error_bankcard")
                ->insert([
                    'partnerId' => $tempInfo->partnerId,
                    'cardInfo' => $request->Input('number')
                ]);

            Cache::forget($placingOrderKey); // remove placing order key

            return response()->json([
                'success' => false,
                'errorId' => 'CC15005',
                'message' => 'Invalid Bank Card',
            ]);
        }

        // currency exchange
        $amount = $tempInfo->amount;
        if ($tempInfo->currencyCode != 'USD') {
            $amount = $this->changeCurrency($amount, $tempInfo->currencyCode);
        }
        if ($amount == 0) {
            Cache::forget($placingOrderKey); // remove placing order key

            return response()->json([
                'success' => false,
                'errorId' => '',
                'errorMessage' => '汇率转换错误，原始价格：' . $tempInfo->amount . $tempInfo->currencyCode
            ]);
        }

        $tempInfo->currencyCode = 'USD';

        $tempInfo->save();

        if (config('app.env') == 'local') {
            $amount = 1;
        }

        // check whether card is in whitelist
        $uniquenumber = substr($number, -8, 8) . $request->input('month') . $request->input('year');
        $cardCombination = SuspiciousOrder::where('uniqueCard', $uniquenumber)
            ->orderBy('created_at', 'desc')
            ->first();
        $isInWhiteList = !empty($cardCombination) && $cardCombination->isDone === 1 ? true : false;

        // check card country
        $verifyUser = new VerifyUser();
        $checkCardRes = $verifyUser->checkCardCountry(substr($number, 0, 6), $request->Input('Reorderid'));
        $isChineseCard = isset($checkCardRes['success']) ? $checkCardRes['success'] : false;
        $isSafeCard = isset($checkCardRes['safe']) ? $checkCardRes['safe'] : false;; //US OR CANADA CARD
        $danger_sign = 0; //原始的danger_sign为零

        // if it's not china bank issued card check whether phone is verified
        if (!$isChineseCard && !$isInWhiteList) {
            // new fraud check step 1: check card issued country, if not china us or canada, fraud danger_sign as 1,  back office mark purple
            if (!$isSafeCard) {
                //写入黑名单 类型1， 其他国家信用卡
                $danger_sign = 1; // mark danger_sign as 1
                Log::info($this->ip . ' is not china US or Canada card, so mark the danger_sign as ' . $danger_sign);
            } else {
                // new fraud check step 3: check card issued country, if not tie with phone country, fraud danger_sign as 3,  back office mark blue
                $phoneCountryCode = $request->has('countryCodePhone') ? $request->input('countryCodePhone') : '';
                if ($phoneCountryCode != '+1') {
                    //写入黑名单 类型3， 国家信用卡和手机国家不为加拿大或美国
                    $danger_sign = 3; // mark danger_sign as 3
                    Log::info($this->ip . ' card issued country is us or canada, while phone country code is ' . $phoneCountryCode . ', mark the danger_sign as ' . $danger_sign);
                }
            }
            $phoneNumber = $request->has('countryCodePhone') ? $request->input('countryCodePhone') . $request->input('phone') : '';
            $validatePhoneCode = $request->input('validatePhoneCode');
            $sms = Sms::where('mobile', $phoneNumber)
                ->orderBy('created_at', 'desc')
                ->first();

            if ($sms != null && $sms->content == $validatePhoneCode) {
                $phoneVerified = true;
            } else {
                $phoneVerified = false;
            }

            if (!$phoneVerified) {
                Cache::forget($placingOrderKey); // remove placing order key

                return response()->json([
                    'success' => false,
                    'errorId' => 'PHONE1001',
                    'errorMessage' => 'Phone is not verified'
                ]);
            } else {
                // new fraud check step 2: check mobile not using mobile but land line or voip, fraud danger_sign as 2, back office mark dark grey
                $smsResult = $this->getPhoneType($phoneNumber) ? $this->getPhoneType($phoneNumber)->getData() : '';// param eg.: +14143502296
                if ($smsResult != '') {
                    $phoneType = $smsResult->phoneType;
                    //write country code for suspicious order if going there
                    $phoneCountrySMS = $smsResult->phoneCountryCode;
                    if ($smsResult == '' || $phoneType != 'mobile') {
                        //写入黑名单 类型2， 非mobile
                        $danger_sign = 2; // mark danger_sign as 2
                        Log::info($this->ip . ' use phone number: ' . $phoneNumber . ' to validate code, which is not using mobile, but ' . $phoneType . ', mark danger sign as ' . $danger_sign);
                    }
                } else {
                    $phoneType = '';
                    $phoneCountrySMS = '';
                    $danger_sign = 2; // mark danger_sign as 2
                    Log::info($this->ip . ' use phone number: ' . $phoneNumber . ' to validate code, which is not using mobile, but no result come from twilio,  mark danger sign as ' . $danger_sign);
                }
            }
        }

        $rooms = json_decode($tempInfo->rooms, true);
        $booker = json_decode($tempInfo->booker, true);

        //添加验证过的booker 电话
        $booker['countryCodePhone'] = $request->has('countryCodePhone') ? $request->input('countryCodePhone') : '';
        $booker['validPhoneNumber'] = $request->has('phone') ? $request->input('phone') : '';
        // if goes to suspicious order, still need to save booker info
        $tempInfo->booker = json_encode($booker);
        $tempInfo->save();

        $ccAuthTriedTimes = 0;

        $authRes = PaypalNew::auth($creditCardInfo, $amount);
        if (!$authRes['success']) {

            Cache::forget($placingOrderKey); // remove placing order key

            return response()->json([
                'success' => false,
                'errorId' => '',
                'errorMessage' => '网络错误' . $authRes['errorNo'] . '，错误描述：' . $authRes['errorMessage'],
            ]);
        }
        $authTransaction = new PayResultNew($authRes['request'], $authRes['response']);
        Log::info($this->ip . 'c site paypal auth result: ' . json_encode($authTransaction));

        //try to save auth failed times
        $ccAuthTriedTimesKey = $Reorderid . '_verifyTimes';
        if (Cache::has($ccAuthTriedTimesKey)) {
            $ccAuthTriedTimes = Cache::get($ccAuthTriedTimesKey);
        }

        // if auth failed
        if ($authTransaction->result != 0) {
            $authTransaction->booker_email = $tempInfo->user_email;
            $authTransaction->operationType = "Credit Card Auth Failed";
            $authTransaction->operationResult = json_encode($authRes['response']);
            $authTransaction->amount = $amount;
            $authTransaction->currency = 'USD';
            $authTransaction->transaction_type = 'A';
            $this->payError('Credit Card Auth Failed: Amount:' . $amount . ', from user: ' . $tempInfo->user_email . ', reason: ' . $authTransaction->respMsg);
            $this->savePaymentHistory($authTransaction, $creditCardInfo);
            $ccAuthTriedTimes++;
            log::info($tempInfo->user_email . ': cc auth tried times: ' . $ccAuthTriedTimes . 'Details: ' . json_encode($authTransaction));
            DB::table("error_bankcard")
                ->insert([
                    'partnerId' => $tempInfo->partnerId,
                    'cardInfo' => $request->Input('number')
                ]);
            $authErrorDetail = $authTransaction->respMsg;
            if ($authErrorDetail == 'Declined by Fraud Service') {
                if ($authTransaction->avsAddr != 'Y') {
                    $authErrorDetail = 'This transaction cannot be processed. Please enter a valid address.';
                } elseif ($authTransaction->cvv2Match != 'Y') {
                    $authErrorDetail = 'This transaction cannot be processed. Please enter a valid Credit Card Verification Number.';
                } elseif ($authTransaction->avsZip != 'Y') {
                    $authErrorDetail = 'This transaction cannot be processed. Please enter a valid zip code!';
                }
            }

            Cache::forget($placingOrderKey); // remove placing order key

            return response()->json([
                'success' => false,
                'errorId' => "CC15005",
                'message' => $authErrorDetail,
            ]);
        }

        // do capture
        $captureRes = PaypalNew::capture($authTransaction->transactionId, $amount);
        if (!$captureRes['success']) {
            $this->payError('Network caused cc capture failed:' . json_encode($captureRes));
            $doVoidResult2 = $this->paypalDovoid($authTransaction, $creditCardInfo);
            Log::info($this->ip . ' Paypal capture failed, doVoid result: ' . json_encode($doVoidResult2));

            Cache::forget($placingOrderKey); // remove placing order key

            return response()->json([
                'success' => false,
                'errorId' => '',
                'errorMessage' => '网络错误' . $captureRes['errorNo'] . '，错误描述：' . $captureRes['errorMessage'],
            ]);
        }

        $captureTransaction = new PayResultNew($captureRes['request'], $captureRes['response']);
        Log::info($this->ip . ' paypal capture result: ' . json_encode($captureTransaction));

        if ($captureTransaction->result != 0) {
            $this->payError('credit card auth succeeded but capture failed:' . json_encode($captureRes));
            $doVoidResult3 = $this->paypalDovoid($authTransaction, $creditCardInfo);
            Log::info($this->ip . ' Paypal capture failed, doVoid result: ' . json_encode($doVoidResult3));

            Cache::forget($placingOrderKey); // remove placing order key

            return response()->json([
                'success' => false,
                'errorId' => "CC15005",
                'message' => 'Auth failed',
            ]);
        }

        $captureTransaction->booker_email = $tempInfo->user_email;
        $captureTransaction->operationType = "Capture Success";
        $captureTransaction->operationResult = json_encode($captureRes['response']);
        $captureTransaction->amount = $amount;
        $captureTransaction->currency = 'USD';
        $captureTransaction->type = 'Full';
        $captureTransaction->transaction_type = 'D';
        $payment_history_id = $this->savePaymentHistory($captureTransaction, $creditCardInfo);

        $transactionId = isset($captureTransaction->transactionId) ? $captureTransaction->transactionId : '';
        // end of credit card auth and capture

        // new fraud check step 4: check weather card holder and guest name consistent with user language, fraud danger_sign as 4, back office mark orange
        $cardHolderLN = $request->Input('lastName');
        $bookerLN = $booker['lastname'];
        $userLang = session('language', 0);
        if ((strlen($cardHolderLN) > 6 || strlen($bookerLN) > 6) && $userLang != 1) {
            //写入黑名单 类型4， 中国人用外国人的卡
            $danger_sign = 4; // mark danger_sign as 4
            Log::info($this->ip . 'card holder lastname ' . $cardHolderLN . ', mark danger sign as ' . $danger_sign);
        }

        // check whether room is cancellable
        $searchInfo = json_decode($tempInfo->search_info, true);

        $roomDetails = $rooms[0]['roomDetails'];
        $cancelDate = isset($roomDetails['cancelDate']) ? $roomDetails['cancelDate'] : '';
        if (empty($cancelDate) && isset($roomDetails['cancellationPolicies'])) { // in case cancelDate is not saved to roomDetails in checkRooms
            $cancelDate = count($roomDetails['cancellationPolicies']) > 0 ? $roomDetails['cancellationPolicies'][0]['end'] : '';
            $roomDetails['cancelDate'] = $cancelDate;
        }

        // if it's not chinese card and it's not in whitelist
        // is passed all four new fraud policy: (us/canada card, mobile phone, card and phone country us or canada, card and guest last name less than 6 character), do next step fraud check
        if (!$isChineseCard && !$isInWhiteList) {
            $checkUserInfo = [
                'hotelId' => $tempInfo->hotelId,
                'clientIp' => $tempInfo->client_ip,
                'sessionKey' => $tempInfo->sessionKey
            ];
            $checkUserResult = $this->checkSuspiciousOrder($checkUserInfo, $booker, $rooms, $creditCardInfo,
                $searchInfo);
            unset($checkUserInfo);

            // convert java returned success type
            if (isset($checkUserResult['success'])) {
                if (is_string($checkUserResult['success'])) {
                    if ($checkUserResult['success'] == 'false') {
                        $checkUserResult['success'] = false;
                    } else {
                        $checkUserResult['success'] = true;
                    }
                }
            } else {
                $checkUserResult['success'] = false;
            }
            // if it's fraud
            if (!$checkUserResult['success'] || $danger_sign != 0) {
                if($danger_sign != 0) {
                    $errorMessage = [
                        'errId' => 'DANGER'.$danger_sign,
                        'errScore' => 1
                    ];
                    if ($danger_sign === 1) {
                        $errorMessage['errDesc'] =  "Your Credit card is not issued by China, US or Canada Bank.";
                    } elseif ($danger_sign === 2) {
                        $errorMessage['errDesc'] =  "Your phone number is not a mobile number or null, need double check";
                    } elseif ($danger_sign === 3) {
                        $errorMessage['errDesc'] =  "Credit card is not issued by China, and phone country code is not US or Canada";
                    } elseif ($danger_sign === 4) {
                        $errorMessage['errDesc'] =  "Credit cards may belong to non-Chinese speaking users.";
                    }
                    //add errorMessage to table suspicious table error_list, and show to customer.
                    if(isset($checkUserResult['data'])){
                        $danger_array = $checkUserResult['data']['errList'];
                        array_push($danger_array, $errorMessage);
                    }else{
                        $danger_array[] = $errorMessage;
                    }
                }else{
                    $danger_array = $checkUserResult['data']['errList'];
                }

                $finalErrorMessage = json_encode($danger_array);
                $hotel = Hotel::where('hotelId', $tempInfo->display_hotelId)
                    ->first();
                if (!empty($hotel)) {
                    $searchInfo['hotelCity'] = $hotel->city;
                }
                $suspiciousOrder = new SuspiciousOrder();

                $creditCardInfo2 = $creditCardInfo;
                $creditCardInfo2->number = substr($number, -8, 8);

                $suspiciousOrder->Reorderid = $tempInfo->Reorderid;
                $suspiciousOrder->payment_id = $transactionId;
                $suspiciousOrder->search_info = json_encode($searchInfo);
                $suspiciousOrder->cc_info = json_encode($creditCardInfo2);
                $suspiciousOrder->booker = json_encode($booker);
                $suspiciousOrder->rooms = json_encode($rooms);
                $suspiciousOrder->amount = $tempInfo->amount;
                $suspiciousOrder->currency = $tempInfo->currencyCode;
                $suspiciousOrder->uniqueCard = $creditCardInfo2->number . $creditCardInfo2->month . $creditCardInfo2->year;
                $suspiciousOrder->language = session('language', '0');
                $suspiciousOrder->http_host = $this->host;
                $suspiciousOrder->error_list = $finalErrorMessage;
                $suspiciousOrder->cc_error_count = $ccAuthTriedTimes;
                $suspiciousOrder->danger_sign = $danger_sign; //记录fraud danger 类型
                $suspiciousOrder->phone_num_type = $phoneType; //记录手机号的类型
                $suspiciousOrder->phone_num_country = $phoneCountrySMS; //记录手机号的国家

                try {
                    $suspiciousOrder->save();
                    $reorderUrl = 'http://' . $this->host . '/hotel/fraud/reorder/' . $tempInfo->display_hotelId . '/' . $suspiciousOrder->id;
                    $suspiciousOrder->reorder_url = $reorderUrl;
                    $suspiciousOrder->save();
                } catch (Exception $e) {
                    Log::error($this->ip . ' save suspicious order error: ' . $e->getMessage());
                }

                $emailData = [
                    'searchInfo' => $searchInfo,
                    'creditCardInfo' => $creditCardInfo2,
                    'booker' => $booker,
                    'rooms' => $rooms,
                    'fileUrl' => url('user-file/show/' . $tempInfo->Reorderid),
                    'reorderUrl' => $reorderUrl,
                    'amount' => $tempInfo->amount,
                    'currency' => $tempInfo->currencyCode,
                    'hotelName' => $tempInfo->goods_name,
                    'Reorderid' => $tempInfo->Reorderid,
                    'hotelId' => $tempInfo->display_hotelId,
                    'is_aa' => strrpos( $this->host,'supervacation'),
                ];
                $this->sendSuspiciousOrderEmail($emailData);

                // for ga on payment complete data
                $anaOptions = [
                    'hotelId' => $tempInfo->display_hotelId,
                    'hotelName' => $tempInfo->goods_name,
                ];

                Cache::forget($placingOrderKey); // remove placing order key

                return response()->json([
                    'success' => false,
                    'errorId' => 'ER3999',
                    'errorMessage' =>$finalErrorMessage,
                    'options' => $anaOptions
                ]);
            }
        }

        // if passed fraud checked
        $user = User::find($this->partnerId);
        $api = new HotelAPI($user);

        $bookRoomResult = $api->bookRooms($booker, $rooms, $specialRequest);

        if (!isset($bookRoomResult['success']) || !$bookRoomResult['success']) {
            if (isset($bookRoomResult['errorId']) && $bookRoomResult['errorId'] === 'ER1415') { // if it's duplicate booking error from api
                return response()->json([
                    'success' => false,
                    'errorId' => $bookRoomResult['errorId'],
                    'Reorderid' => $Reorderid,
                    'p_id' => $payment_history_id,
                ]);
            } elseif (isset($bookRoomResult['errorId']) && $bookRoomResult['errorId'] === 'IT1001') {
                Cache::forget($placingOrderKey); // place order failed, remove placing order key

                return response()->json([
                    'success' => false,
                    'errorId' => $bookRoomResult['errorId'],
                ]);
            }

            $room = $rooms[0];
            $orderCReference = $room['roomReference'];
            $order = Order::where('orderCReference', $orderCReference)
                ->first();
            if ($order != null) {
                $porderReference = $order->orderReference;
                $bookRoomResult = $api->checkReservation($porderReference);// in case it's network error, check again
            }

            if (!isset($bookRoomResult['success']) || !$bookRoomResult['success']) {
                Cache::forget($placingOrderKey); // place order failed, remove placing order key

                $captureTransaction->first_name = $creditCardInfo->firstName;
                $captureTransaction->last_name = $creditCardInfo->lastName;
                $captureTransaction->note = 'book failed';
                $refundResult = $this->refund($captureTransaction);

                Log::info($this->ip . ' Book failed, refund result: ' . json_encode($refundResult));

                if (isset($bookRoomResult['errorId'])) {
                    Log::info($this->ip . ' cc book failed, error message' . $bookRoomResult['errorMessage']);

                    return response()->json([
                        'success' => false,
                        'errorId' => $bookRoomResult['errorId'],
                        'errorMessage' => $bookRoomResult['errorMessage'],
                    ]);
                } else {
                    return response()->json([
                        'success' => false,
                        'errorId' => '',
                        'errorMessage' => 'Booking failed'
                    ]);
                }
            }
        }

        $bookRoomResult = $bookRoomResult['data'];
        if($bookRoomResult === null) {
            $pricelineMissData = new BookController();
            $pricelineMissData->pricelineBookSuccessDataFailed($Reorderid);
        }else{
            $this->updatePaymentHistory($captureTransaction, $bookRoomResult);
            $tempInfo->orderReference = $bookRoomResult['orderReference'];
            $tempInfo->code = $bookRoomResult['code'];
            $tempInfo->booker = json_encode($booker);
            $tempInfo->save();

            Order::where('orderReference', $bookRoomResult['orderReference'])
                ->update([
                    'infact_cost' => $tempInfo->amount,
                    'payment_type' => 'CC',
                    'payment_status' => 'PAID',
                    'remark' => $tempInfo->remark,
                    'source' => $tempInfo->source, //user browser stored distributor source code
                    'source_pos' =>  isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '',
                    'special_request' => $specialRequest,
                    'sales_no' => $tempInfo->sales_no, //stored cache when user opened sales shared link
                    'user_email' => $tempInfo->user_email, //info from tours incoming user and has to be logged in on tours site
                    'display_hotelId' => $tempInfo->display_hotelId,
                    'http_host' => $this->host, //add HTTP host for order table. --052318 Ann
                    'user_language' => session('language', 0), //add user_language for order table. --060518 Ann
                ]);

            $order = Order::where('orderReference', $bookRoomResult['orderReference'])
                ->first();

            if (!empty($order) && $order->provider === 'EBooking') {
                Log::info('3: processPayment eb email send' . $order->id);
                $this->sendEbEmail($order);
            }
            $this->newOrderEmail($tempInfo, $bookRoomResult);

            //预定完成，更新infusionsoft contact，以发送feedback邮件
            $langString = $booker['language'] === 0 ? 'cn' : 'en';
            $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'hotel.usitrip.com';
            $this->dispatch(new TriggerFeedback($booker['email'], $langString, $domain));

            Cache::forget($placingOrderKey); // place order succeed, remove placing order key

            return response()->json([
                'success' => true,
                'voucherUrl' => url('voucher/' . $bookRoomResult['code']),
            ]);
        }
    }

    /**
     * @param $data
     */
    private function sendSuspiciousOrderEmail($data)
    {
        $locale = session('language', 0);
        $bookerEmail = $data['booker']['email'];
        // 用戶信息：
        $roomDetails = $data['rooms'][0]['roomDetails'];
        $cancellation0 = isset($roomDetails['cancelDate']) ? $roomDetails['cancelDate'] : '';
        if (empty($cancellation0) && isset($roomDetails['cancellationPolicies'])) { // in case cancelDate is not saved to roomDetails in checkRooms
            $cancellation0 = count($roomDetails['cancellationPolicies']) > 0 ? $roomDetails['cancellationPolicies'][0]['end'] : '';
        }
        $cancellation = str_replace("T", " ", mb_substr($cancellation0, 0, 19));
        $cancelTime = strtotime($cancellation);
        $creationTime = date("Y-m-d", time());
        $orderTime = strtotime($creationTime);
        if (!$cancellation) {
            $cancellation = "Non-Refundable";
        } elseif ($cancelTime < $orderTime) {
            $cancellation = "Non-Refundable";
        } else {
            $cancellation = "Free Cancellation before " . $cancellation;
        }

        if (config('app.env') == 'local') {
            $emails = config('staff-emails.ann');
        } else {
            if (strrpos($this->host, 'supervacation') > -1) {
                $emails = array_merge($this->techEmails, $this->aa);
            }else{
                $emails = array_merge($this->techEmails, $this->salesEmails);
            }
        }
        $function = "queue";
        if($locale == 1){
            $subject = 'Your Hotel Booking is in Process!';
        }elseif($locale == 2) {
            if (strrpos($this->host, 'supervacation') !== false) {
                $emails = $this->aa;
                $function = "send";
            }
            $subject = '你的飯店預訂正在處理中!';
        }elseif($locale == 0){
            $subject = '您的酒店预订正在处理中!';
        }
        try {
            //fraud处理邮件发给用户
            Mail::$function('emails.suspicious-order-email', [
                'data' => $data,
                'cancellation' => $cancellation,
                'is_aa' =>strrpos($_SERVER['HTTP_HOST'], 'supervacation') ,
            ], function ($message) use ($emails, $bookerEmail,$subject) {
                $message->to($bookerEmail)
                    ->bcc($emails)
                    ->subject($subject);
            });
            //fraud处理邮件发给内部销售
            if (strrpos($_SERVER['HTTP_HOST'], 'supervacation') !== false) {
                $emails = $this->aa ;
            }
            Mail::send('emails.suspicious-alert', [
                'data' => $data,
            ], function ($message) use ($emails) {
                $message->to($emails)
                    ->subject('待查看可疑订单!');
            });
        } catch (Exception $e) {
            Log::error($this->ip . ' send suspicious order email failed, ' . $e->getMessage());
        }
    }

    /**
     * @param $Reorderid
     * @return \Illuminate\Http\JsonResponse
     */
    public function declineTmpOrder($Reorderid)
    {
        Log::info($this->ip . ' Decline reorderid: ' . $Reorderid);
        $sus = SuspiciousOrder::where('Reorderid', $Reorderid)
            ->get()
            ->last();
        $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';
        if ($sus->isDone == 0 && $sus->isFraud == 0) {
            $sus->update([
                'isLocked' => 0,
                'isDeclined' => 1,
                'sales_no' => $sales_no,
            ]);

            $this->sendFraudEmail($sus, $Reorderid);

            return response()->json([
                'success' => true,
            ]);

        } else {
            return response()->json([
                'success' => false,
                'message' => '该订单已经被完成，无法拒绝'
            ]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmFraudOrder($id)
    {
        Log::info($this->ip . ' confirm is fraud id: ' . $id);
        $orderId = $id;
        $sus = SuspiciousOrder::find($orderId);
        //        save phone
        $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';
        try {
            $this->sendFraudEmail($sus, $sus->Reorderid);
            //send fraud email and at the same time do refund
            $sus->update([
                'isLocked' => 0,
                'isFraud' => 1,
                'sales_no' => $sales_no,
            ]);

            return response()->json([
                'success' => true
            ]);
        } catch (Exception $e) {
            Log::error($this->ip . ' error with setting cc card as blacklist ' . $e->getMessage());
        }
        return response()->json([
            'success' => false,
        ]);
    }

    /**
     * @param $Reorderid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fileUploadView($Reorderid)
    {
        $sus_order = SuspiciousOrder::where('Reorderid', $Reorderid)
            ->select('file_url')
            ->first();
        if (empty($sus_order)) {
            abort('404', 'No such order');
        }

        $blade = 'file-upload';
        if($this->isMobileDevice()) {
            $blade = 'mobile.file-upload';
        }

        return view($blade, [
            'optionName' => 'fileUpload',
            'Reorderid' => $Reorderid,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function duplicateProceed(Request $request) // user confirm to continue or cancel order placing
    {
        $action = $request->input('action');

        if ($action === 'continue') { // if continue book rooms
            return $this->duplicateContinue($request);
        } else {
            return $this->duplicateCancel($request);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function duplicateContinue(Request $request) // confirm not duplicate, continue
    {
        $Reorderid = $request->input('Reorderid');
        $vendor = $request->input('vendor');

        $tempInfo = OrderTem::where('Reorderid', $Reorderid)
            ->first();

        // prevent book request two times
        $placingOrderKey = $Reorderid . 'placingOrder';

        if (Cache::has($placingOrderKey)) {
            return response()->json([
                'success' => false,
                'message' => 'wait'
            ]);
        }

        $paid = false;

        if ('cc' === $vendor) {
            $paymentType = 'CC';
            $paid = true;
        } else {
            if ('we' == $vendor || 'wechatpay' == $vendor) {
                $paymentType = 'WECHATPAY';
            } elseif ('al' == $vendor || 'alipay' == $vendor) {
                $paymentType = 'ALIPAY';
            }

            $transaction = CitconPay::inquire($Reorderid);

            if ($transaction->type == 'charge' && $transaction->status == 'success') {
                $paid = true;
            }
        }

        Log::info($this->ip . ' confirm not duplicate placeorder, paymentType: ' . $paymentType);

        if (empty($tempInfo->orderReference) && $paid) { // if it's not booked and paid
            if ($paymentType !== 'CC') {
                $tempInfo->paid_not_booked = 0;
                $tempInfo->save();
            }

            Cache::put($placingOrderKey, true, $this->cacheLifeTime);

            $specialRequest = $tempInfo->special_request;
            $user = User::find($this->partnerId);
            $api = new HotelAPI($user);
            $booker = json_decode($tempInfo->booker, true);
            $rooms = json_decode($tempInfo->rooms, true);

            $bookResult = $api->bookRoomsDu($booker, $rooms, $specialRequest);

            Log::info($this->ip . ' placeorderDu Book room result ' . json_encode($bookResult));

            if (isset($bookResult['success']) && $bookResult['success']) {
                $book_data = $bookResult['data'];
                if($book_data === null){
                    $pricelineMissData = new BookController();
                    $pricelineMissData->pricelineBookSuccessDataFailed($Reorderid);
                }else{
                    // update temp info table
                    $tempInfo->code = $bookResult['data']['code'];
                    $tempInfo->orderReference = $bookResult['data']['orderReference'];
                    $tempInfo->save();

                    // update order table
                    Order::where('orderReference', $bookResult['data']['orderReference'])
                        ->update([
                            'infact_cost' => $tempInfo->amount,
                            'payment_type' => $paymentType,
                            'payment_status' => 'PAID',
                            'remark' => $tempInfo->remark,
                            'source' => $tempInfo->source,
                            'source_pos' => isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '',
                            'special_request' => $specialRequest,
                            'sales_no' => $tempInfo->sales_no,
                            'user_email' => $tempInfo->user_email,
                            'display_hotelId' => $tempInfo->display_hotelId,
                            'http_host' => $this->host,
                            'user_language' => session('language', 0)
                        ]);

                    $this->newOrderEmail($tempInfo, $bookResult['data']);

                    //预定完成，更新infusionsoft contact，以发送feedback邮件
                    $langString = $booker['language'] === 0 ? 'cn' : 'en';
                    $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'hotel.usitrip.com';
                    $this->dispatch(new TriggerFeedback($booker['email'], $langString, $domain));

                    Cache::forget($placingOrderKey);

                    return response()->json([
                        'success' => true,
                        'voucherUrl' => url('voucher/' . $bookResult['data']['code'])
                    ]);
                }
            } elseif (isset($bookRoomResult['errorId']) && $bookResult['errorId'] === 'IT1001') {
                return response()->json([
                    'success' => false,
                    'errorId' => $bookResult['errorId'],
                ]);
            } else { // void pay if book failed
                Cache::forget($placingOrderKey);

                Log::info($this->ip . ' place order failed with placeorderDu, start refund');

                if ('cc' === $vendor) {
                    $p_ids = $request->input('p_id');
                    if (is_array($p_ids)) {
                        foreach ($p_ids as $p_id) {
                            $transaction = Payment::find($p_id);

                            if ($transaction->amount > 0) {
                                $transaction->type = 'Full';
                                $transaction->note = "priceline duplicate confirm book failed";
                                $this->refund($transaction);
                            }
                        }

                        return response()->json([
                            'success' => false,
                            'errorId' => isset($bookResult['errorId']) ? $bookResult['errorId'] : '',
                            'message' => 'fully refund'
                        ]);
                    } else {
                        $p_id = $p_ids;
                        $transaction = Payment::find($p_id);

                        if ($transaction->amount > 0) {
                            $transaction->type = 'Full';
                            $transaction->note = "priceline duplicate confirm book failed";
                            $message = $this->refund($transaction);

                            return response()->json([
                                'success' => false,
                                'errorId' => isset($bookResult['errorId']) ? $bookResult['errorId'] : '',
                                'message' => $message
                            ]);
                        }
                    }
                } else {
                    if ($transaction == null) {
                        Log::info($this->ip . ' citcon inquire response: ' . json_encode($transaction));

                        return response()->json([
                            'success' => false,
                            'errorId' => isset($bookResult['errorId']) ? $bookResult['errorId'] : '',
                            'message' => "Place order failed, please contact us to get refund"
                        ]);
                    }

                    $refundInfo = [
                        'reason' => 'citcon order failed, citcon ' . $vendor . ' refund',
                        'refundType' => 'Full',
                        'amount' => $transaction->amount,
                        'orderReference' => '',
                        'partnerId' => $this->partnerId,
                        'name' => $booker['firstname'] . ' ' . $booker['lastname'],
                        'Reorderid' => $Reorderid,
                    ];

                    $refundResult = $this->citconRefund($transaction, $refundInfo); // refund and save to db

                    $errorMsg = 'Place order failed, fully refunded';
                    if (!$refundResult->refunded) {
                        $errorMsg = 'Place order failed, refund failed';
                    }

                    return response()->json([
                        'success' => false,
                        'errorId' => isset($bookResult['errorId']) ? $bookResult['errorId'] : '',
                        'errorMessage' => $errorMsg,
                    ]);
                }
            }
        } elseif (!empty($tempInfo->code)) { // if it's booked, return voucher url
            return response()->json([
                'success' => true,
                'voucherUrl' => url('voucher/' . $tempInfo->code)
            ]);
        }
        // if it's none of both above
        return response()->json([
            'success' => false,
            'errorId' => 'IT1001'
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function duplicateCancel(Request $request) // confirm it's duplicate, refund
    {
        $Reorderid = $request->input('Reorderid');
        $vendor = $request->input('vendor');

        $tempInfo = OrderTem::where('Reorderid', $Reorderid)
            ->first();
        $booker = json_decode($tempInfo->booker, true);

        if ('cc' === $vendor) {
            $p_ids = $request->input('p_id');
            if (is_array($p_ids)) {
                // in case multiple payments
                foreach ($p_ids as $p_id) {
                    $transaction = Payment::find($p_id);
                    if ($transaction->amount > 0) {
                        $transaction->type = 'Full';
                        $transaction->note = "priceline duplicate confirm book failed";
                        $this->refund($transaction);
                    }
                }

                return response()->json([
                    'success' => true,
                    'message' => 'fully refund'
                ]);
            } else {
                $p_id = $p_ids;
                $transaction = Payment::find($p_id);

                if ($transaction->amount > 0) {
                    $transaction->type = 'Full';
                    $transaction->note = "Priceline duplicate confirm book failed";
                    $message = $this->refund($transaction);
                    $message = (session('language', 0) == 0 ? '预定失败, ' : 'Book failed, ') . $message;

                    return response()->json([
                        'success' => true,
                        'message' => $message
                    ]);
                }
            }
        } else {
            $transaction = CitconPay::inquire($Reorderid);

            if ($transaction == null) {
                Log::info($this->ip . ' citcon inquire response: ' . json_encode($transaction));

                return response()->json([
                    'success' => false,
                    'errorId' => 'IT1002',
                ]);
            }

            $refundInfo = [
                'reason' => 'user confirmed duplicate order, refund, citcon ' . $vendor . ' refund',
                'refundType' => 'Full',
                'amount' => $transaction->amount,
                'orderReference' => '',
                'partnerId' => $this->partnerId,
                'bookerName' => $booker['firstname'] . ' ' . $booker['lastname'],
                'Reorderid' => $Reorderid,
            ];

            $refundResult = $this->citconRefund($transaction, $refundInfo); // refund and save to db

            $msg = 'Cancelled placing order, fully refunded';
            if (!$refundResult->refunded) {
                $msg = 'Cancelled placing order, refund failed';
            }

            return response()->json([
                'success' => true,
                'message' => $msg,
            ]);
        }
        //if none of the above situations
        return response()->json([
            'success' => false,
        ]);
    }

    /**
     * @param $transaction
     * @param $refundInfo
     * @return mixed
     */
    private function citconRefund($transaction, $refundInfo)
    {
        $refundResult = CitconPay::refund($transaction->currency, $transaction->id, $transaction->amount,
            $refundInfo['reason']);
        Log::info($this->ip . ' Citcon refund response: ' . json_encode($refundResult));

        $refund = new Refund();
        if ($refundResult->status == 'success') {
            $refund->orderReference = $refundInfo['orderReference'];
            $refund->partnerId = $refundInfo['partnerId'];
            $refund->refundType = $refundInfo['refundType'];
            $refund->amount = $refundInfo['amount'];
            $refund->transactionId = $transaction->id;
            $refund->currency = $transaction->currency;
            $refund->note = $refundInfo['reason'];
            $refund->response = json_encode($refundResult);
            $refund->refundTransactionId = $refundResult->id;
            $refund->totalRefundedAmount = $refundResult->refunded;
            $refund->name = $refundInfo['name'];

            try {
                $refund->save();
            } catch (Exception $e) {
                Log::error($this->ip . ' refund success, save citcon refund error: ' . $e->getMessage());
            }
        } else {
            Log::info($this->ip . ' citcon refund failed, reason: ' . $refundResult->status);
            try {
                $msg = 'citcon refund failed. Reorderid: ' . $refundInfo['Reorderid'] . ', reason: ' . $refundResult->status;
                Mail::raw($msg, function ($message) {
                    $message->to(config('staff-emails.jie'))
                        ->subject('citcon refund failed');
                });
            } catch (Exception $e) {
                Log::error($this->ip . ' refund failed, save citcon refund error: ' . $e->getMessage());
            }
        }

        return $refundResult;
    }

    /**
     * b site paypal payment
     */
    public function NoCreditcardsPay(Request $request)
    {
        //check if session expired, if so redirect to login page
        if(!Auth::check()){
            return redirect()->guest('login');
        }
        $user = User::with('creditcards')->find(Auth::user()->id);

        $Reorderid = $request->Input('Reorderid');
        $tem_Info = OrderTem::where('Reorderid', $Reorderid)
            ->first();

        $specialRequest = $tem_Info->special_request;

        $creditcardInfo = new CreditCardInfo($request->Input('firstName'), $request->Input('lastName'),
            $request->Input('cardType'),
            $request->Input('number'), $request->Input('month'), $request->Input('year'), $request->Input('CVC'),
            $request->Input('street'), $request->Input('street2'), $request->Input('city'), $request->Input('state'),
            $request->Input('zip'),
            $request->Input('countryCode'));

        $cc_info = [
            'firstName' => $creditcardInfo->firstName,
            'lastName' => $creditcardInfo->lastName,
            'number' => $creditcardInfo->number
        ];

        //luen algorithm for card number check
        $number = $request->Input('number');

        settype($number, 'string');
        $sumTable = array(
            array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
            array(0, 2, 4, 6, 8, 1, 3, 5, 7, 9)
        );
        $sum = 0;
        $flip = 0;
        for ($i = strlen($number) - 1; $i >= 0; $i--) {
            $sum += $sumTable[$flip++ & 0x1][$number[$i]];
        }

        if (0 !== ($sum % 10)) {
            DB::table("error_bankcard")
                ->insert([
                    'partnerId' => $tem_Info->partnerId,
                    'cardInfo' => $request->Input('number')
                ]);

            return response()->json([
                'success' => false,
                'errorId' => 'CC15005',
                'message' => 'Invalid Bank Card',
            ]);

        }

        // change currency
        $amount = $tem_Info->amount;
        if ($tem_Info->currencyCode != 'USD') {
            $amount = $this->changeCurrency($amount, $tem_Info->currencyCode);
        }

        if ($amount == 0) {
            return response()->json([
                'success' => false,
                'message' => '汇率转换错误，原始价格：' . $tem_Info->amount . $tem_Info->currencyCode
            ]);
        }

        $auth_response = Paypal::auth($creditcardInfo, $amount);

        $auth_result = new PayResult($auth_response['request'], $auth_response['response']);

        Log::info($this->ip . 'b site paypal auth result: ' . json_encode($auth_result));

        // save payment history
        $auth_result->firstName = $creditcardInfo->firstName;
        $auth_result->lastName = $creditcardInfo->lastName;
        $auth_result->number = $creditcardInfo->number;
        $auth_result->operationType = "Credit Card Auth Failed";
        unset($auth_response['request']);
        $auth_result->operationResult = json_encode($auth_response);
        $auth_result->amount = $amount;

        $this->savePaymentHistory($auth_result, $user->id);

        // if auth failed
        if ($auth_result->ack != 'Success') {
            $this->payError($user->company_name . ' 刚刚尝试下单信用卡验证失败, 其后四位信用卡号码为：* ' . substr($creditcardInfo->number, -4) .
                ', 持卡人姓名是: ' . $creditcardInfo->firstName . ' ' . $creditcardInfo->lastName . ', 金额为: ' . $amount . ', 详细的验证信息为: ' . print_r($auth_response, true));

            // save error back card to db
            DB::table("error_bankcard")
                ->insert([
                    'partnerId' => $tem_Info->partnerId,
                    'cardInfo' => $request->Input('number')
                ]);

            return response()->json([
                'success' => false,
                'errorId' => 'CC15005',
                'message' => 'Auth Failed'
            ]);
        }

        // if auth succeed, do capture
        $user_id = $user->id;
        $capture_response['success'] = false;

        $capture_response = Paypal::capture($auth_result->transactionId, $amount);

        if (!$capture_response['success']) { // if capture failed (normally won't happen)
            DB::table("error_bankcard")
                ->insert([
                    'partnerId' => $tem_Info->partnerId,
                    'cardInfo' => $cc_info['number']
                ]);

            $this->doVoid($auth_result->transactionId, $cc_info, $amount, 'Credit Card Capture Failed');

            Log::error($this->ip . ' 117book paypal capture失败');

            return response()->json([
                'success' => false,
                'errorId' => 'IT1001',
                'errorMessage' => '付款失败，请检查信用卡信息'
            ]);
        }

        $payment_history_id = null;
        // if capture succeed, save capture result to payment history
        $capture_result = new PayResult($capture_response['request'], $capture_response['response']);
        // firstname and lastname is used by payment_history record
        $capture_result->firstName = $cc_info['firstName'];
        // name is used by refund record
        $capture_result->lastName = $cc_info['lastName'];
        $capture_result->name = $cc_info['firstName'] . ' ' . $cc_info['lastName'];
        $capture_result->number = $cc_info['number'];
        $capture_result->operationType = "Capture Success";
        unset($capture_response['request']);
        $capture_result->operationResult = json_encode($capture_response);
        $capture_result->amount = $amount;

        $payment_history_id = $this->savePaymentHistory($capture_result, $user_id);

        $booker = json_decode($tem_Info->booker, true);
        $rooms = json_decode($tem_Info->rooms, true);

        $user = User::find($this->partnerId);
        $api = new HotelAPI($user);

        // send book rooms request
        $book_result = $api->bookRooms($booker, $rooms, $specialRequest);
        // API568 如果是重复订单可以退款, 因为不会真的下单,所以这个收款需要退回
        if (isset($book_result['errorId']) && $book_result['errorId'] === 'ER1414') {
            $this->refund($capture_result);

            return response()->json([
                'success' => false,
                'errorId' => $book_result['errorId'],
                'errorMessage' => str_replace("same order.:", "", $book_result['errorMessage'])
            ]);
        } elseif (isset($book_result['errorId']) && $book_result['errorId'] === 'ER1415') { // if it's duplicate booking error from api
            return response()->json([
                'success' => false,
                'errorId' => $book_result['errorId'],
                'Reorderid' => $Reorderid,
                'p_id' => $payment_history_id,
            ]);
        } elseif (isset($book_result['errorId']) && $book_result['errorId'] === 'IT1001') {
            return response()->json([
                'success' => false,
                'errorId' => $book_result['errorId'],
            ]);
        } elseif (!isset($book_result['success']) || isset($book_result['success']) && !$book_result['success']) { // if book failed
            //if no response then query porder and getOrderStatus
            $room = $rooms[0];
            $orderCReference = $room['roomReference'];
            $order = Order::where('orderCReference', $orderCReference)
                ->first();

            if ($order != null) {
                $porderReference = $order->orderReference;
                $book_result = $api->checkReservation($porderReference);
            }

            Log::info($this->ip . ' paypal check reservation result: ' . json_encode($book_result));

            // if check reservation also failed, refund
            if (!isset($book_result['success']) || isset($book_result['success']) && !$book_result['success']) {
                Log::info($this->ip . " paypal book and check reservation failed. Start to void pay.");

                $this->refund($capture_result);

                return response()->json([
                    'success' => false,
                    'errorId' => isset($book_result['errorId']) ? $book_result['errorId'] : '',
                    'errorMessage' => 'Booking failed'
                ]);
            }
        }

        $book_data = $book_result['data'];
        if($book_data === null){
            $pricelineMissData = new BookController();
            return $pricelineMissData->pricelineBookSuccessDataFailed($Reorderid);
        }else{
            // update payment history with booking result
            Payment::where('transactionId', $capture_result->transactionId)
                ->update([
                    'created_at' => $book_result['dateStamp'],
                    'orderReference' => $book_result['data']['orderReference']
                ]);

            // update porder_tem with orderReference
            OrderTem::where('Reorderid', $Reorderid)
                ->update(['orderReference' => $book_data['orderReference']]);

            $remark = $tem_Info->remark;

            // update porder with tem_info and language
            Order::where('orderReference', $book_data['orderReference'])
                ->update([
                    'infact_cost' => $tem_Info->amount,
                    'remark' => $remark,
                    'payment_type' => 'CC',
                    'special_request' => $specialRequest,
                    'http_host' => $this->host, //add HTTP host for order table. --052318 Ann
                    'user_language' => session('language', 0), //add user_language for order table. --060518 Ann
                ]);

            $order = Order::where('orderReference', $book_data['orderReference'])
                ->first();

            if (!empty($order) && $order->provider === 'EBooking') {
                Log::info('7: NoCreditcardsPay 117book eb email sent' . $order->id);
                $this->sendEbEmail($order);
            }

            $this->newOrderEmail($tem_Info, $book_data);

            //预定完成，更新infusionsoft contact，以发送feedback邮件
            $langString = $booker['language'] === 0 ? 'cn' : 'en';
            $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'hotel.usitrip.com';
            $this->dispatch(new TriggerFeedback($booker['email'], $langString, $domain));

            return response()->json([
                'success' => true,
                'voucherUrl' => url('voucher/' . $book_data['code'])
            ]);
        }
    }

    /**
     * @param $transactionId
     * @param $cc_info
     * @param $amount
     * @param $msg
     */
    private function doVoid($transactionId, $cc_info, $amount, $msg)
    {
        $doVoid_response = Paypal::doVoid($transactionId);

        $doVoid_result = new PayResult($doVoid_response['request'], $doVoid_response['response']);

        log::info($this->ip . ' ' . $msg . ', do void result' . json_encode($doVoid_result));

        $doVoid_result->firstName = $cc_info['firstName'];
        $doVoid_result->lastName = $cc_info['lastName'];
        $doVoid_result->number = $cc_info['number'];
        $doVoid_result->operationType = $msg;
        unset($doVoid_response['request']);
        $doVoid_result->operationResult = json_encode($doVoid_response);
        $doVoid_result->amount = $amount;

        $this->savePaymentHistory($doVoid_result, Auth::user()->id);

        unset($doVoid_response, $doVoid_result);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateNewCitconAmount(Request $request)
    {
        $missed_order = OrderTem::where('id', $request->id)
            ->get()
            ->last();
        $missed_order->update([
            'amount' => $request->new_amount_confirmed,
            'commend' => $request->new_commend,
        ]);
        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * @param $tempInfo
     * @param $result
     */
    public function newOrderEmail($tempInfo, $result)
    {
        //Email New Order
        try {
            $executionStartTime = microtime(true);
            $orderTable = Order::where('code', $result['code'])
                ->leftjoin('hotel', 'porder.hotelId', '=', 'hotel.hotelId')
                ->leftJoin('destination', 'hotel.desId', '=', 'destination.desId')
                ->leftJoin('porder_pax', 'porder.orderReference', '=', 'porder_pax.orderReference')
                ->leftJoin('porder_room', 'porder.orderReference', '=', 'porder_room.orderReference')
                ->first();
            $partner = 'Customer';
            $hotelName = $tempInfo['goods_name'];
            $voucherUrl = $orderTable->voucher();
            $orderRef = $orderTable->orderPReference;
            $creationTime = $orderTable->created_at;
            $bookingId = $result['bookingId'];
            $totalPrice = $result['currencyCode'] . ' ' . $result['totalPrice'];
            $bookerEmail = $result['bookerEmail'];
            $bookerName = $result['bookerFirstName'] . ' ' . $result['bookerLastName'];
            $roomInfo = $result['rooms']['0'];
            $boardName = $roomInfo['boardName'];
            $roomType = $roomInfo['name'];
            $cancellationInfo = $roomInfo['cancellationPolicies']['0'];
            $cancellation = $cancellationInfo['end'];
            $cancellation = str_replace("T", " ", mb_substr($cancellation, 0, 19));
            $cancelTime = strtotime($cancellation);
            $orderTime = strtotime($creationTime);
            if (!$cancellation) {
                $cancellation = "Non-Refundable";
            } elseif ($cancelTime < $orderTime) {
                $cancellation = "Non-Refundable";
            } else {
                $cancellation = "Free Cancellation before " . $cancellation;
            }
            $roomNumber = $orderTable->room_number;
            $dest1 = $orderTable->address;
            $dest2 = $result['destination'];
            $dest = str_replace(", ", "", $dest1) . ", " . $dest2;
            $checkin = strtotime($result['checkInDate']);
            $checkin = date('M d, Y', $checkin);
            $checkout = strtotime($result['checkOutDate']);
            $checkout = date('M d, Y', $checkout);
            $guestInfo = $roomInfo['paxes']['0'];
            $firstName1 = $guestInfo['firstName'];
            $lastName1 = $guestInfo['lastName'];
            $guest = $firstName1 . ' ' . $lastName1;
            $paxesArray = $roomInfo['paxes'];
            $ad_total = count(array_filter($paxesArray, function ($pax) { return $pax['type'] == 'AD'; }));
            $ch_total = count(array_filter($paxesArray, function ($pax) { return $pax['type'] == 'CH'; }));
            $locale = $orderTable->user_language ? $orderTable->user_language : session('language', 0);
            Log::info($this->ip . ' locale language of user: ' . $locale);

            $host = $orderTable->http_host;
            if( strrpos($host, 'usitour') > -1&& $locale !== 2) {
                $locale = 1;
            }

            $hostName = 'Usitour';
            $emailContent = 'emails.new-order';
            if (strrpos($host, "usitrip") !== false) {
                $hostName = 'Usitrip';
                $hostNameCn = '走四方';
            } elseif (strrpos($host, "117book") !== false){
                $hostName = '117book';
                $hostNameCn = $locale == 2 ? '要趣訂' : '要趣订';
                $emailContent = 'emails.117book_newOrder';
            }

            $status = $result['status'] == 'CONFIRMED' ? '已经被确认': '正在处理中';
            if ($locale == 1) {
                $status = $result['status'] == 'CONFIRMED' ? ' is confirmed' : ' is in process';
            } elseif ($locale == 2) {
                $status = $result['status'] == 'CONFIRMED' ? '已經被確認' : '正在處理中';
            }

            if ($locale == 0) {
                $langId = 0;
                if ($result['status'] == 'CONFIRMED') {
                    $emailSubject = $hostName . $hostNameCn . '酒店预订确认 - （订单号';
                    Log::info($this->ip . ' 发送确认中文邮件');
                } else {
                    $emailSubject = $hostName . $hostNameCn . '酒店预订正在处理 - （订单号';
                    Log::info($this->ip . ' 发送正在处理中文邮件');
                }
            } elseif ($locale === 2) {
                $langId = 2;
                if ($result['status'] == 'CONFIRMED') {
                    $emailSubject = $hostName . $hostNameCn . '酒店預訂確認 - （訂單號';
                    Log::info($this->ip . ' 发送确认繁体邮件');
                } else {
                    $emailSubject = $hostName . $hostNameCn . '酒店預訂正在處理 - （訂單號';
                    Log::info($this->ip . ' 发送正在处理繁体邮件');
                }
            } else {
                $langId = 1;
                if ($result['status'] == 'CONFIRMED') {
                    $emailSubject = $hostName . ' Hotel Confirmation - (Booking #';
                    Log::info($this->ip . ' 发送确认英文邮件');
                } else {
                    $emailSubject = $hostName . ' Hotel is in process- (Booking #';
                    Log::info($this->ip . ' 发送正在处理英文邮件');
                }
            }

            $bcc = config('staff-emails.ann'); //bdcn看所有的117book新预订邮件, config does not comply with RFC 2822, 3.6.2. ？？ use email directly
            if (strrpos($emailSubject, "117book") > -1) {
                $bcc = $this->annbdcn;
            }
            //目前只給英文usitour用戶發短信
            if (strrpos($host, "usitour") > -1 && $locale !== 2) {
                try {
                    $booker = json_decode($tempInfo->booker, true);
                    Log::info('for sms $booker info: ' . json_encode($booker));
                    //For all new order, send sms
                    $input_country_code = isset($booker['countryCodePhone']) ? $booker['countryCodePhone'] : $booker['phone_country'];
                    $input_mobile = isset($booker['validPhoneNumber']) ? $booker['validPhoneNumber'] : $booker['phone'];
                    $code = SinaUrl::getShort($voucherUrl);
                    $sendSms = new sendMessage();
                    $sendSms->sendOrderSms($input_country_code, $input_mobile, $code);
                } catch (Exception $e) {
                    Log::error($this->ip . ' send new order message error: ' . $e->getMessage());
                }
            }
            if (strrpos($host, 'supervacation') !== false) {
                $function = 'send';
            }else{
                $function = 'queue';
            }
            try {
                if ($result['status'] == 'CONFIRMED' || $result['status'] == 'ON REQUEST') {
                    Mail::$function($emailContent, [
                        'partner' => $partner,
                        'orderId' => $bookingId,
                        'status' => $status,
                        'voucher' => $voucherUrl,
                        'checkin' => $checkin,
                        'checkout' => $checkout,
                        'hotelName' => $hotelName,
                        'reference' => $orderRef,
                        'guest' => $guest,
                        'roomCount' => $roomNumber,
                        'totalPrice' => $totalPrice,
                        'adNumber' => $ad_total,
                        'chNumber' => $ch_total,
                        'roomType' => $roomType,
                        'boardType' => $boardName,
                        'cancellation' => $cancellation,
                        'hotelAddress' => $dest,
                        'is_aa' => strrpos($host, 'supervacation'),
                        'langId' => $langId,
                        'hostName' => $hostName
                    ], function ($message) use (
                        $bookingId,
                        $bookerEmail,
                        $bookerName,
                        $emailSubject,
                        $result,
                        $bcc
                    ) {
                        $message->subject($emailSubject . $bookingId . ')')
                            ->to($bookerEmail, $bookerName)
                            ->bcc($bcc);
                    });
                    Log::info($this->ip . ' 发送预定邮件');
                }
            } catch (Exception $e) {
                Log::error($this->ip . ' send new order email error: ' . $e->getMessage());
            }
            $executionEndTime = microtime(true);
            //The result will be in seconds and milliseconds.
            $seconds = $executionEndTime - $executionStartTime;
            Log::info($this->ip . ' TIME to email new Order for ' . $bookingId . ' is ' . $seconds . ' seconds');
        } catch (\Exception $e) {
            Log::error($this->ip . ' Send new order email failed, ' . $e->getMessage());
        }
    }

    /**
     * sales or op send email to customer again, in case their email is not been received.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function againOrderEmail($id)
    {
        Log::info(Auth::user()->id . ' try to send new order email again, for order ' . $id);
        //Email New Order again
        $orderTable = Order::with('hotel', 'hotel.destination')->where('id', $id)
            ->first();

        $partnerId = $orderTable->partnerId;
        $hotelName = $orderTable->hotelName;
        $orderRef = $orderTable->orderPReference;
        $creationTime = $orderTable->created_at;
        $bookingId = $id;
        $totalPrice = $orderTable->currencyCode . ' ' . $orderTable->totalPrice;
        $bookerEmail = $orderTable->bookerEmail;
        $bookerName = $orderTable->bookerFirstName . ' ' . $orderTable->bookerLastName;
        $orderTem = OrderTem::where('code', $orderTable->code)->orWhere('orderReference',
            $orderTable->orderReference)->first();
        $roomInfo = json_decode($orderTem['rooms'])[0];
        $roomDetails = $roomInfo->roomDetails;
        $boardName = $roomDetails->boardName;
        $roomType = $roomDetails->name;
        $cancellationInfo = $roomDetails->cancelDate;
        $cancellation = str_replace("T", " ", substr($cancellationInfo, 0, 19));
        $cancelTime = strtotime($cancellation);
        $orderTime = strtotime($creationTime);
        if (!$cancellation) {
            $cancellation = "Non-Refundable";
        } elseif ($cancelTime < $orderTime) {
            $cancellation = "Non-Refundable";
        } else {
            $cancellation = "Free Cancellation before " . $cancellation;
        }
        $roomNumber = $orderTable->room_number;
        $dest1 = $orderTable->hotel->address;
        $dest2 = $orderTable->hotel->desName ? $orderTable->hotel->desName : $orderTable->hotel->city;
        $dest = str_replace(", ", "", $dest1) . ' ,' . $dest2 . ', ' . $orderTable->hotel->zipcode;
        $checkin = $orderTable->checkinDate;
        $checkout = $orderTable->checkoutDate;
        //改过入住人名字的话主入住人替换成从porder_pax里面拿那个改过的名字，in case改完名字以后，有人再次发送邮件， 防止客人confuse；
        $orderPaxes = OrderPax::where('orderReference', $orderTable->orderReference)->where('updated_at','<>', null )->first();
        if(empty($orderPaxes)){
            $guestInfo = $roomInfo->paxes[0];
            $firstName1 = $guestInfo->name;
            $lastName1 = $guestInfo->surname;
        }else{
            Log::info($id .' confirm email again with name changed');
            $firstName1 =$orderPaxes->firstName;
            $lastName1 =$orderPaxes->lastName;
        }
        $guest = $firstName1 . ' ' . $lastName1;

        $paxesSummary = $roomInfo->roomDetails->paxes;
        $ad_total = substr($paxesSummary, 0, 1);
        $ch_total = substr($paxesSummary, 3, 1);
        $locale = isset($orderTable->user_language) ?  $orderTable->user_language : session('language', 0);
        //目前3個站沒有繁體確認模板,先發英文的
        if($locale === 2){
            $locale = 1;
        }
        Log::info($this->ip . ' locale language of user: ' . $locale);
        $host = $orderTable->http_host;

        if(strrpos($host, "usitour") > -1 && $locale !== 2) {
            $locale = 1;
        }elseif(strrpos($host, "supervacation") > -1) {
            $locale = 2;
        }
        if ($locale == 1) {
            $user_language = 'en';
        }elseif($locale == 2){
            $user_language = 'tw';
       }else {
            $user_language = 'cn';
        }

        if ($orderTable->status == 'CONFIRMED') {
            if ($locale == 0) {
                $status = "已经被确认";
            } elseif ($locale == 2) {
                $status = "已經被確認";
            } else {
                $status = " is CONFIRMED";
            }
        } else {
            if ($locale == 0) {
                $status = "正在处理中";
            } elseif ($locale == 2) {
                $status = "正在處理中";
            } else {
                $status = " is IN PROCESS";
            }
        }

        if (strrpos($host, '117book') !== false) {
            $hostName = '117book';
        } elseif (strrpos($host, 'supervacation') !== false) {
            $hostName = 'supervacation';
        }  elseif (strrpos($host, 'usitour') !== false) {
            $hostName = 'usitour';
        } else {
            $hostName = 'usitrip';
        }
//if( $hostName === 'supervacation'){
//    try {
//        $emailContent = 'emails.new-order-tw';
//        $emailSubject = $hostName.'酒店預訂確認 - （訂單號';
//            Mail::send($emailContent, [
//                'partner' => 'Customer',
//                'orderId' => $bookingId,
//                'status' => $status,
//                'voucher' => $orderTable->voucher(),
//                'checkin' => $checkin,
//                'checkout' => $checkout,
//                'hotelName' => $hotelName,
//                'reference' => $orderRef,
//                'guest' => $guest,
//                'roomCount' => $roomNumber,
//                'totalPrice' => $totalPrice,
//                'adNumber' => $ad_total,
//                'chNumber' => $ch_total,
//                'roomType' => $roomType,
//                'boardType' => $boardName,
//                'cancellation' => $cancellation,
//                'hotelAddress' => $dest,
//                'is_aa' => strrpos($host, 'supervacation')
//            ], function ($message) use (
//                $bookingId,
//                $bookerEmail,
//                $bookerName,
//                $emailSubject,
//            ) {
//                $message->subject($emailSubject . $bookingId . ')')
//                    ->to($bookerEmail, $bookerName)->bcc('550@usitrip.com');
//            });
//    } catch (Exception $e) {
//        Log::error($this->ip . ' send new order email error: ' . $e->getMessage());
//    }
//}else{
    try {
        $data = [
            'host' => $hostName,
            'language' => $user_language,
            'partner' => $partnerId,
            'orderId' => $bookingId,
            'status' => $status,
            'voucher' => $orderTable->voucher(),
            'checkin' => $checkin,
            'checkout' => $checkout,
            'hotelName' => $hotelName,
            'reference' => $orderRef,
            'guest' => $guest,
            'roomCount' => $roomNumber,
            'totalPrice' => $totalPrice,
            'adNumber' => $ad_total,
            'chNumber' => $ch_total,
            'roomType' => $roomType,
            'boardType' => $boardName,
            'cancellation' => $cancellation,
            'hotelAddress' => $dest,
            'bookerEmail' => $bookerEmail,
            'bookerName' => $bookerName,
        ];
        $this->post('/sendNewOrderEmail', $data);

        return response()->json([
            'success' => true,
        ]);
    } catch (Exception $e) {
        Log::error($this->ip . ' send order email again error: ' . $e->getMessage());
    }
//}
        return response()->json([
            'success' => false,
        ]);
    }
}
