<?php

namespace App\Http\Controllers\Hotel;

use App\Model\Coupon;
use App\Model\Hotel;
use App\Model\OrderTem;
use App\Model\EbRoom;
use App\Traits\IsMobileDevice;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Log;

/**
 * Class PaypalController
 * @package App\Http\Controllers\Hotel
 */
class PaypalController extends Controller
{
    use IsMobileDevice;

    /**
     * @param $Reorderid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function index($Reorderid)
    {
        $tempInfo = OrderTem::where('Reorderid', $Reorderid)
            ->first();

        if (empty($tempInfo)) {
            return abort('404');
        }

        $price = $tempInfo->currencyCode . '' . $tempInfo->amount;
        $hotelName = $tempInfo->goods_name;

        $rooms = json_decode($tempInfo->rooms, true);
        $room = $rooms[0];
        $paxes = $room['paxes'];
        $paxe = $paxes[0];
        $guest = $paxe['name'] . ' ' . $paxe['surname'];

        $booker = json_decode($tempInfo->booker, true);
        $phoneCountryCode = str_replace( ' ', '', isset($booker['phone_country']) ? $booker['phone_country'] : ' ');
        $phone = $booker['phone'];

        $hotelId = (string)$tempInfo['display_hotelId'];
        $hotelInfo = Hotel::where('hotelId', $hotelId)
            ->first();

        $roomDetails = $rooms[0]['roomDetails'];

        if (!isset($roomDetails['cancelDate']) && isset($roomDetails['cancellationPolicies'])) { // in case cancelDate is not saved to roomDetails in checkRooms
            $roomDetails['cancelDate'] = count($roomDetails['cancellationPolicies']) > 0 ? $roomDetails['cancellationPolicies'][0]['end'] : '';
        }

        $totalPrice = $tempInfo['amount'];
        $cleanFee = isset($roomDetails['cleanFee'])? $roomDetails['cleanFee'] : 0;

        $coupon = null;
        if (!empty($tempInfo['coupon_code'])) {
            $coupon = Coupon::where('code', $tempInfo['coupon_code'])->first();
        }

        // check whether it's cancellable
        $canOrNot = false;
        if (isset($roomDetails['cancelDate']) && !empty($roomDetails['cancelDate']))
        { // in case either cancelDate or cancellationPolicies is saved
            $canOrNot = $roomDetails['cancelDate'];
        }elseif(isset($roomDetails['cancellationPolicies']) && count($roomDetails['cancellationPolicies']) > 0 && $roomDetails['cancellationPolicies'][0]['amount'] === 0){
            $canOrNot = $roomDetails['cancellationPolicies'][0]['end'];
        }

        $params = '';
        $searchInfo = json_decode($tempInfo->search_info, true);
        Log::info($this->ip . ' open paypal page searchInfo: ' . json_encode($searchInfo));
        if (!empty($searchInfo)) {
            $dayCount = $this->calculateDays($searchInfo['checkin'], $searchInfo['checkout']);
            $occ = $searchInfo['occupancies'][0];
            $params = 'checkin=' . $searchInfo['checkin'] . '&checkout=' . $searchInfo['checkout'] . '&dayCount=' . $dayCount . '&roomCount=' . $occ['rooms'] .
                '&adultCount1=' . $occ['adults'] . '&childCount1=' . $occ['children'];
        }

        $hotelPath = url('/hotel') . '/' . $hotelId . '.html?' . $params;
        //if it is E-booking room type show comments from hotel itself

        $isEbooking = strpos($roomDetails['roomReference'], 'eb:') !== false ?  true : false;
        if($isEbooking){
            $ebRoom = EbRoom::where('hotelId', $roomDetails['hotelId'])
                ->Where('room_type_id',$roomDetails['roomId'])
                ->first();
            if(!empty($ebRoom) && isset($ebRoom->remark)){
                $roomDetails['comments'][0]['comments']  = $ebRoom->remark;
            }
        }

        $data = [
            'optionName' => '',
            'Reorderid' => $Reorderid,
            'phone' => $phone,
            'phoneCountryCode' => $phoneCountryCode,
            'hotelInfo' => $hotelInfo,
            'canOrNot' => $canOrNot,
            'roomDetails' => $roomDetails,
            'totalPrice' => $totalPrice,
            'cleanFee' => $cleanFee,
            'coupon' => $coupon,
            'tempInfo' => $tempInfo,
            'guest' => $guest,
            'hotelName' => $hotelName,
            'price' => $price,
            'hotelPath' => $hotelPath,
        ];

        $isMobile = $this->isMobileDevice();

        if ($isMobile) {
            return view('mobile.paypal', $data);
        }

        return view('paypal', $data);
    }

    /**
     * @param $checkin
     * @param $checkout
     * @return float
     */

    private function calculateDays($checkin, $checkout)
    {
        $checkin = strtotime($checkin);
        $checkout = strtotime($checkout);
        $length = ($checkout - $checkin) / 60 / 60 / 24;

        return round($length);
    }

}
