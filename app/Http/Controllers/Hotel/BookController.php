<?php

namespace App\Http\Controllers\Hotel;

use App\Classes\CitconPay;
use App\Classes\HotelAPI;
use App\Classes\HotelDB;
use App\Classes\SinaUrl;
use App\Jobs\TriggerFeedback;
use App\Model\Coupon;
use App\Jobs\SendReminderSms;
use App\Classes\InfusionsoftAPI;
use App\Jobs\UpdateInfusionHotelsData;
use App\Model\CouponHistory;
use App\Model\Hotel;
use App\Model\Payment;
use App\Traits\ApiPostTrait;
use App\Traits\SendMessageTrait;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Util;
use App\Model\OrderTem;
use App\Model\User;
use App\Model\EbRoom;
use App\Model\Order;
use App\Model\CustomerInformation;
use App\Model\SuspiciousOrder;
use App\Traits\HttpGet;
use App\Traits\IsMobileDevice;
use App\Traits\HotelDataRetriever;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

use App\Http\Controllers\Hotel\HotelController as hotelControllerEmail;
use App\Http\Controllers\Client\Auth\SendMessageController as sendMessage;
use Jenssegers\Agent\Facades\Agent;

/**
 * Class BookController
 * @package App\Http\Controllers\Hotel
 */
class BookController extends Controller
{
    use IsMobileDevice, HttpGet, HotelDataRetriever, SendMessageTrait, ApiPostTrait;

    private $partnerId;
    private $cacheLifeTime;
    private $techEmails;
    private $otherEmails;
    private $testEmails;
    private $accEmails;
    private $ann106;
    private $annbdcn;
    private $missedOrderChinaEmails;
    private $missedOrderUsEmails;
    private $missedOrderAmericaAsia;

    public function __construct()
    {
        parent::__construct();
        if (strpos($this->host, '117book') > -1) {
            $this->partnerId = Auth::check() ? Auth::user()->id : config('app.partner_id');
        } elseif (strpos($this->host, 'supervacation') > -1) {
            $this->partnerId = config('app.aa_partner_id');
        } else {
            $this->partnerId = config('app.partner_id');
        }
        $this->cacheLifeTime = config('app.cache_lifetime');
        $this->techEmails = config('staff-emails.tech');
        $this->otherEmails = config('staff-emails.other');
        $this->testEmails = config('staff-emails.test');
        $this->accEmails = config('staff-emails.acc');
        $this->salesEmails = config('staff-emails.sales');
        $this->ann106 = config('staff-emails.ann106');
        $this->annbdcn = config('staff-emails.annbdcn');
        $this->missedOrderChinaEmails = config('staff-emails.china');
        $this->missedOrderUsEmails = config('staff-emails.us');
        $this->missedOrderAmericaAsia = config('staff-emails.aa');
    }

    /**
     * @param Request $request
     * @param $displayId , which is the displayed hotel id
     * @param $roomRef
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $displayId, $roomRef, $passedpartnerId = '')
    {
        Log::info($this->ip . ' book begin partnerId: ' . $passedpartnerId);
        $sessionKey = $request->has('s') ? $request->input('s') : ''; // api sessionKey

        $fastBooking = $request->has('Reorderid');
        $Reorderid = '';

        // check whether it's from fraud_processing or missed_order processing hotel page
        if ($fastBooking) {
            $Reorderid = $request->input('Reorderid');
        }

        $hotelPath = url('/hotel') . '/' . $displayId . '.html' . '?desId=H' . $displayId . '&' . http_build_query($request->all());

        $redisSearchRoomsKey = $request->input('rooms_key'); // api sessionKey

        if (Cache::has($redisSearchRoomsKey)) { // get room from search rooms cache
            $searchRoomsResult = json_decode(Cache::get($redisSearchRoomsKey), true);
            $searchInfo = $searchRoomsResult['searchInfo'];

            Log::info($this->ip . ' open book page, searchInfo: ' . json_encode($searchInfo));

            $rooms = $searchRoomsResult['rooms'][0];

            if (empty($rooms)) { // search rooms cache expired, jump to home page
                Log::error($this->ip . ' open book view failed because of empty rooms in search rooms cache result');
                return redirect($hotelPath)->with('message', 'Please search again, session expired!');
            }

            $roomInfo = [];

            foreach ($rooms as $room) { // find the room user choose from search rooms cache
                if ($room['roomReference'] == $roomRef) {
                    $roomInfo = $room;
                    $roomInfo['roomRef'] = $roomInfo['roomReference'];
                    $roomInfo['fees'] = $rooms[0]['mandatoryFees'] ? $rooms[0]['mandatoryFees'] : null;
                    // in case cancelDate is not saved to roomDetails in checkRoom
                    $roomInfo['cancelDate'] = count($roomInfo['cancellationPolicies']) > 0 ? $roomInfo['cancellationPolicies'][0]['end'] : '';
                }
            }

            if (empty($roomInfo)) { // search rooms cache expired, jump to home page
                Log::error($this->ip . ' open book view failed as did not find room info in search rooms cache');
                return redirect($hotelPath)->with('message', 'Please search again, session expired!');
            }

            $hotelId = $roomInfo['hotelId'];

            $db = new HotelDB();
            $hotel = $db->getHotel($displayId);
            if ($hotel == null) {
                Log::error($this->ip . ' did not find hotel in db');
                return redirect($hotelPath)->with('message', 'Please search again, session expired!');
            }

            if ($request->has('no')) { // save sales no in session when sales share book link
                session(['sales_no' => $request->input('no')]);
            }

            $occupancies = $searchInfo['occupancies'];

            $rooms = [];

            $days = $this->calculateDays($searchInfo['checkin'], $searchInfo['checkout']);

            // build room data to display in blade
            foreach ($occupancies as $occ) {
                $roomCount = $occ['rooms'];
                $dayCount = $days;

                $taxRate = config('constants.tax_rate');
                $beforeTax = floor(($roomInfo['netPrice'] * (1 - $taxRate)) / $roomCount / $dayCount) * $roomCount * $dayCount;
                $tax = $roomInfo['netPrice'] - $beforeTax;

                $roomInfo['beforeTax'] = $beforeTax;
                $roomInfo['tax'] = $tax;
                $roomInfo['roomCount'] = $roomCount;
                $roomInfo['dayCount'] = $dayCount;

                $room['beforeTax'] = $beforeTax;
                $room['tax'] = $tax;

                $rooms[] = [
                    'occupancy' => $occ,
                    'room' => $roomInfo
                ];
            }

            $isEbooking = strpos($roomInfo['roomRef'], 'eb:') !== false ? true : false;
            //如果含有clean fee
            $rooms[0]['room']['cleanFee'] = 0;
            if ($isEbooking) {
                $ebRoom = EbRoom::where('hotelId', $hotelId)
                    ->Where('room_type_id', $rooms[0]['room']['roomTypeId'])
                    ->first();
                if (!empty($ebRoom) && isset($ebRoom->clean_fee)) {
                    $rooms[0]['room']['cleanFee'] = $ebRoom->clean_fee;
                }
            }

            $redisBookRoomKey = $displayId . ':' . urlencode($roomRef) . 1;
            // save room details for reserve
            Cache::put($redisBookRoomKey, json_encode($roomInfo), $this->cacheLifeTime);

            //非快速下单的，即非postpay用户，非销售下单客人,booker信息来自usitrip的return 用户
            $booker = session('user', '');
            $guests = [];
            $paidAmount = '';
            $paidCurrency = 'USD';
            $coupon_code = '';
            $coupon_amount = 0;

            // get data for fraud processing book view
            if ($fastBooking) {
                $Reorderid = $request->input('Reorderid');
                $temp_order = OrderTem::where('Reorderid', $Reorderid)
                    ->get()
                    ->last(); // this is suspicious order

                $filledRooms = json_decode($temp_order['rooms'], true);

                foreach ($filledRooms as $filledRoom) {
                    $guests[] = $filledRoom['paxes'];
                }

                $salesSolved = false; //default没处理过
                if ($request->has('type') && $request->input('type') === 'missedOrder') {
                    $paidAmount = $temp_order->amount;
                    $paidCurrency = $temp_order->currency;
                    $booker = json_decode($temp_order['booker'], true);
                    if ($temp_order->paid_not_booked != 1) { //如果已处理过
                        $salesSolved = true;
                    }
                } else {
                    $sus_order = SuspiciousOrder::where('Reorderid', $Reorderid)
                        ->first();
                    $paidAmount = $sus_order->amount;
                    $paidCurrency = $sus_order->currency;
                    $booker = json_decode($sus_order['booker'], true);
                    $old_tem_order = OrderTem::where('Reorderid', $Reorderid)
                        ->first();
                    if (isset($old_tem_order->coupon_code)) {
                        $coupon = Coupon::where('code', $old_tem_order->coupon_code)
                            ->first();
                        if (!empty($coupon)) {
                            $coupon_code = $coupon->code;
                            $coupon_amount = $coupon->amount;
                            session(['coupon' => json_encode($coupon)]);
                        }
                    }
                    if ($sus_order->isDone == 1 || $sus_order->isDeclined == 1 || $sus_order->isFraud == 1) { //如果已处理过
                        $salesSolved = true;
                    }
                }
            }

            //ip stack check ip country
            // set IP address and API access key
            $ip = $this->ip;

            if (session()->has('ipLocation')) {
                $ipLocation = session('ipLocation');
            } elseif (config('app.env') === 'local') {
                $ipLocation = 'US';
            } else {
                // free plan limited to 10,000 request, separate fraud check with book process to avoid exceed limit
                $access_key1 = '6e765ee64feeaf3d86309081c930964f'; //2355958065@qq.com
                $access_key2 = 'd2c8e633ef2881a47529b535630146fa'; //iannievan@gmail.com
                $access_key3 = '26d3742b04b2e315c4fe763b3ac31d96';  //wenjun.fan.ann@gmail.com
                $api_result = $this->ipstack($ip, $access_key1);
                Log::info($this->ip . ' ip result 1: ' . json_encode($api_result));
                if (isset($api_result['error'])) {
                    $api_result = $this->ipstack($ip, $access_key2);
                    Log::info($this->ip . ' ip result 2: ' . json_encode($api_result));
                    if (isset($api_result['error'])) {
                        $api_result = $this->ipstack($ip, $access_key3);
                        Log::info($this->ip . ' ip result 3: ' . json_encode($api_result));
                        if (isset($api_result['error'])) {
                            //do not prevent book
                            $api_result['country_code'] = 'US';
                        }
                    }
                }
                // Output  ip location
                $ipLocation = $api_result['country_code'];
                session(['ipLocation' => $ipLocation]);
            }
            //如果是117的用户，自动填充booker信息电话邮箱
            if (strpos($this->host, '117book') > -1) {
                $booker = [
                    'customers_mobile_phone' => Auth::user()->mobile_phone !== '' ? Auth::user()->mobile_phone : Auth::user()->company_phone,
                    'customers_email_address' => Auth::user()->email,
                ];
            }
            $data = [
                'optionName' => 'book',
                'bookIndex' => urldecode($roomRef),
                'sessionKey' => $sessionKey,
                'rooms_key' => $redisSearchRoomsKey,
                'fastBooking' => $fastBooking,
                'Reorderid' => $Reorderid,
                'hotel' => $hotel,
                'displayId' => $displayId,
                'checkin' => $searchInfo['checkin'],
                'checkout' => $searchInfo['checkout'],
                'rooms' => $rooms,
                'room' => $rooms[0],
                'singleRoomNightPriceB4Tax' => ceil($rooms[0]['room']['beforeTax'] * 100 / $rooms[0]['room']['dayCount'] / $rooms[0]['room']['roomCount']) / 100.0,
                'singleRoomNightTax' => ceil($rooms[0]['room']['tax'] * 100 / $rooms[0]['room']['dayCount'] / $rooms[0]['room']['roomCount']) / 100.0,
                'guests' => $guests,
                'Country_Code' => 'CN',
                'roomId' => $request->input('room' . $hotelId . '_1'),
                'hotelId' => $hotelId,
                'booker' => $booker,
                'paidAmount' => $paidAmount,
                'paidCurrency' => $paidCurrency,
                'hotelPath' => $hotelPath,
                'passedpartnerId' => $passedpartnerId,
                'ipLocation' => $ipLocation,
                'coupon_code' => $coupon_code,
                'coupon_amount' => $coupon_amount,
            ];

            $isMobile = $this->isMobileDevice();
            if ($fastBooking) { // show fraud processing book view
                if ($salesSolved) { //如果已处理过
                    $message = "该订单已完成";
                    abort('404', $message);
                } else {
                    return view('fraud.book-check', $data);
                }
            }

            if ($isMobile) {
                return view('mobile.book', $data); // new version mobile blade
            }

            return view('book', $data);
        } else { // if cache expired
            Log::info($this->ip . ' open book page redisSearchRoomsKey expired, redirect to hotel page');

            return redirect($hotelPath);
        }
    }

    /**
     * @param $ip
     * @param $access_key
     * @return mixed
     */
    public function ipstack($ip, $access_key)
    {
        // Initialize CURL:
        $ch = curl_init('http://api.ipstack.com/' . $ip . '?access_key=' . $access_key . '');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);
        // Decode JSON response
        return json_decode($json, true);
    }

    /**
     * for mobile routes with /m redirection
     * @param \Illuminate\Http\Request $request
     * @param $hotelId
     * @param $roomRef
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectToBook(Request $request, $hotelId, $roomRef)
    {
        $params = $request->all();
        $paramStr = '';
        foreach ($params as $key => $value) {
            $paramStr .= '&' . $key . '=' . $value;
        }
        $paramStr = substr($paramStr, 1);

        $url = url('/book') . '/' . $hotelId . '/' . $roomRef;
        if (!empty($paramStr)) {
            $url .= '?' . $paramStr;
        }

        return redirect($url, 301);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkRooms(Request $request)
    {
        $references = $request->input('references');
        $references = json_decode($references, true);
        $passedpartnerId = $request->input('passedpartnerId');
        if ($passedpartnerId != '') {
            $this->partnerId = $passedpartnerId;
        }
        $user = User::find($this->partnerId);
        $api = new HotelAPI($user);
        $rooms = $api->checkRooms($references); // api checkrooms

        if (isset($rooms['errorId']) || !$rooms['success'] || empty($rooms['data'])) {
            return response()->json($rooms);
        }

        $checkResult = $rooms['data'][0];
        $roomCount = $request->input('roomCount');
        $dayCount = $request->input('dayCount');

        $taxRate = config('constants.tax_rate');
        $beforeTax = floor(($checkResult['netPrice'] * (1 - $taxRate)) / $roomCount / $dayCount) * $roomCount * $dayCount;
        $tax = $checkResult['netPrice'] - $beforeTax;

        $checkResult['beforeTax'] = $beforeTax;
        $checkResult['tax'] = $tax;

        $rooms['data'][0] = $checkResult;

        if ($request->has('displayId') && isset($references[0]['roomReference'])) {
            $redisBookRoomKey = $request->input('displayId') . ':' . urlencode($references[0]['roomReference']) . 1;
            if (Cache::has($redisBookRoomKey)) { // merge search room cached room and checked room
                $roomInfo = json_decode(Cache::get($redisBookRoomKey), true);
                $roomInfo['rateClass'] = $checkResult['rateClass']; // we need this to stop cc non-refundable order passing through in reserve
                $roomInfo['netPrice'] = $checkResult['netPrice'];
                $roomInfo['tax'] = $checkResult['tax'];
            } else { // most of the time it won't come here, except sometimes redisBookRoom cache didn't find
                $roomInfo = $checkResult;
            }
            $roomInfo['cancelDate'] = count($checkResult['cancellationPolicies']) > 0 ? $checkResult['cancellationPolicies'][0]['end'] : '';
            $roomInfo['roomCount'] = $roomCount;
            $roomInfo['dayCount'] = $dayCount;

            //save Tourico and HB mandatory fee to redis 03012018
            if (isset($checkResult['mandatoryFeesDetails'][0]['amount'])) {
                $roomInfo['mandatoryFees'] = $checkResult['mandatoryFeesDetails'][0]['amount'];
            }

            $roomInfo['comments'] = isset($checkResult['comments']) ? isset($checkResult['comments'][0]['comments']) ? $checkResult['comments'][0]['comments'] : $checkResult['comments'] : 'None';
            Cache::forget($redisBookRoomKey);
            Cache::put($redisBookRoomKey, json_encode($roomInfo),
                $this->cacheLifeTime); // update room details for reserve
        }

        $rooms['data'][0]['totalMandatoryFees'] = isset($roomInfo['fees']) ? $roomInfo['fees'] : null;//pass searchRoom's mandatory fee to here - Ann 051418

        //if it is E-booking room type show comments from hotel itself
        $rooms['data'][0]['cleanFee'] = 0;
        $isEbooking = strpos($rooms['data'][0]['roomReference'], 'eb:') !== false ? true : false;
        if ($isEbooking) {
            $ebRoom = EbRoom::where('hotelId', $roomInfo['hotelId'])
                ->Where('room_type_id', $roomInfo['roomTypeId'])
                ->first();
            if (!empty($ebRoom) && isset($ebRoom->remark)) {
                $rooms['data'][0]['comments'][0]['comments'] = $ebRoom->remark;
            }
            if (!empty($ebRoom) && isset($ebRoom->clean_fee)) {
                $rooms['data'][0]['cleanFee'] = $ebRoom->clean_fee;
                $rooms['data'][0]['netPrice'] = $rooms['data'][0]['netPrice'] + $ebRoom->clean_fee;
            }
        }

        return response()->json($rooms['data']);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function PaymentIssueEmail(Request $request) //TODO:需討論是否之后可以完全删除
    {
        $reorder_id = $request->input('Reorderid');
        $vendor = $request->input('vendor');
        $order_tem = OrderTem::where('Reorderid', $reorder_id)
            ->first();

        if (empty($order_tem)) {
            return response()->json([]);
        } else {
            //0 is default, 1 is paid not booked, 2 is solved paid not booked，3 is declined, 4待定,不確定是否下單成功!!!!
            $order_tem->paid_not_booked = 4;
            $order_tem->commend = Carbon::now()
                    ->toDateTimeString() . ' System: 打开付款链接的QR Code加载时间过久,需要销售下单前检查是否已有订单,如没有,需确认是否已付款成功';
            if ('al' == $vendor || 'alipay' == $vendor) {
                $order_tem->alipayid = $reorder_id;
            } else {
                $order_tem->wechatpayid = $reorder_id;
            }
            if (config('app.env') === 'production') {
                //先人工处理,通知550和553
                Mail::raw($order_tem, function ($message) {
                    $message->subject('System: 打开付款链接的QR Code加载时间过久,需要销售下单前检查是否已有订单,如没有,需确认是否已付款成功');
                    if (config('app.env') == 'production') {
                        $message->to('550@usitrip.com')
                            ->cc('553@usitrip.com');
                    } else {
                        $message->to(config('app.default_mail_to'));
                    }
                });
            }
            $order_tem->save();

            return response()->json([
                'success' => true
            ]);
        }
    }

    /**
     * PB Paid but book failed
     * @param $message
     */
    public function pbRefundAlertEmail($message)
    {
        try {
            Mail::raw($message, function ($message) {
                $message->to(config('staff-emails.ann'))
                    ->subject('Paypal Btn need refund');
            });
        } catch (\Exception $e) {
            Log::error($this->ip . ' pb need refund email send failed, error: ' . $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $sessionKey - api sessionKey
     * @return \Illuminate\Http\JsonResponse
     */
    public function reserve(Request $request, $sessionKey)
    {
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            Log::info($this->ip . ' user agent: ' . strtolower($_SERVER['HTTP_USER_AGENT']));
        }

        $redisBookRoomKey = $request->input('displayId') . ':' . urlencode($request->input('bookIndex')) . 1;

        $roomDetails = Cache::get($redisBookRoomKey);
        $roomDetails = json_decode($roomDetails, true);

        $tempInfo = $this->storeTempOrder($request, $sessionKey, $roomDetails);
        if (!$tempInfo) {
            // if save temp info failed, send emails to IT
            Mail::raw($request, function ($message) {
                $message->to(config('staff-emails.ann'))
                    ->subject('Store Temp Order failed');
            });

            if ($request->input('tempType') == 'pb') {
                $message = 'Store Temp Order failed need refund: ' . $sessionKey;
                $this->pbRefundAlertEmail($message);
            }
            return response()->json([
                'success' => false,
                'errorId' => 'IT1001',
                'voucherUrl' => '',
            ]);
        }

        //联系人信息
        $booker = json_decode($tempInfo->booker, true);
        $orderReference = $tempInfo->orderReference;
        $amount = $tempInfo->amount;
        $Reorderid = $tempInfo->Reorderid;
        $passedpartnerId = $request->input('passedpartnerId');
        if ($passedpartnerId != '') {
            $this->partnerId = $passedpartnerId;
        }
        $user = User::find($this->partnerId);
        $api = new HotelAPI($user);
        if (config('app.env') === 'production' && $this->partnerId == config('app.partner_id')) {
            //only register for new users
            $existUser = DB::table('usi_coming_users')
                ->where('email', $booker['email'])
                ->get();
            if (empty($existUser)) {
                $api->regUser($orderReference, $booker);
            }
        }
        $specialRequest = $tempInfo->special_request;
        $rooms = json_decode($tempInfo->rooms, true);
        $transaction = null;

        if (($request->input('tempType') == 'po' && $user->paymentType == 'Postpay') || $request->input('tempType') == 'pb') {
            if ($request->input('tempType') === 'pb') {
                //                $payment_history = Payment::where('orderId', $sessionKey.'-ppbtn')->first();
                //                $transactionId = $payment_history->ppref;
                //                if(isset($transactionId) && $transactionId !== null){
                //                    Log::info( 'Paypal btn received money for Reorderid: ' .$Reorderid. ', the transaction id is '.$transactionId );
                //                }else{
                return response()->json([
                    'success' => false,
                    'errorId' => 'IT1001',
                    'errorMessage' => 'Please try again, that you order payment is not went through via Paypal',
                ]);
                //                }
            }

            $result = $api->bookRooms($booker, $rooms, $specialRequest);
            Log::info($this->ip . ' ' . $request->input("tempType") . ' book room result : ' . json_encode($result));

            if (isset($book_result['errorId']) && $result['errorId'] === 'IT1001') {
                return response()->json([
                    'success' => false,
                    'errorId' => $result['errorId'],
                ]);
            }

            if (isset($result['success'])) { // if book response has 'success' key
                if (!$result['success']) { // if success is false, check reservation
                    //if no response then query porder and getOrderStatus
                    $orderCReference = $request->input('roomRef' . "1");
                    $order = Order::where('orderCReference', $orderCReference)
                        ->first();

                    if ($order != null) {
                        $porderReference = $order->orderReference;
                        $result = $api->checkReservation($porderReference);
                    } else {
                        if ($request->input('tempType') == 'pb') {
                            $msg = $this->ip . ' Fail order paid not refunded - Reorderid : ' . $Reorderid;
                            $this->pbRefundAlertEmail($msg);
                        }
                    }
                }
                if ($result['success']) { // if book succeed
                    $result = $result['data'];
                    if ($result === null) {
                        $this->pricelineBookSuccessDataFailed($Reorderid);
                    } else {
                        if ($user->paymentType == 'Prepay') {
                            if ($request->input('tempType') == 'pb') {
                                // update paypal button payment history
                                $payId = $request->input('payId') ? $request->input('payId') : '';
                                $this->updatepbPaymentHistory($payId, $result);
                            } else {
                                // update credit card payment history
                                $this->updatePaymentHistory($transaction, $result);
                            }
                        }

                        // update porder_tem table
                        $remark = $tempInfo->remark;
                        $tempInfo->orderReference = $result['orderReference'];
                        $tempInfo->save();

                        $langInt = session('language', 0);
                        // update porder table
                        Order::where('orderReference', $result['orderReference'])
                            ->update([
                                'remark' => $remark,
                                'payment_type' => strtoupper($request->input('tempType')),
                                'special_request' => $specialRequest,
                                'http_host' => $this->host,
                                //add HTTP host for order table. --052318 Ann
                                'user_language' => $langInt,
                                'infact_cost' => $tempInfo->amount,
                                'payment_status' => 'PAID',
                                'source' => $tempInfo->source,
                                //user browser stored distributor source code
                                'source_pos' => isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '',
                                'sales_no' => $tempInfo->sales_no,
                                //stored cache when user opened sales shared link
                                'user_email' => $tempInfo->user_email,
                                //info from tours incoming user and has to be logged in on tours site
                                'display_hotelId' => $tempInfo->display_hotelId,
                            ]);

                        $order = Order::where('orderReference', $result['orderReference'])
                            ->first();

                        if (!empty($order) && $order->provider === 'EBooking') {
                            $he = new hotelControllerEmail();
                            Log::info('8: postpay 117book eb email sent' . $order->id);
                            $he->sendEbEmail($order);
                        }

                        $he = new hotelControllerEmail();
                        Log::info('----------' . $request->input('tempType') . ': PO or PB Order id: ' . $order->id . ' ----------');
                        $he->newOrderEmail($tempInfo, $result);

                        //预定完成，更新infusionsoft contact，以发送feedback邮件
                        $langString = $langInt === 0 ? 'cn' : 'en';
                        $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'hotel.usitrip.com';
                        $this->dispatch(new TriggerFeedback($booker['email'], $langString, $domain));

                        Log::info('ready for voucher ' . url('voucher/' . $result['code']));
                        return response()->json([
                            'success' => true,
                            'voucherUrl' => url('voucher/' . $result['code'])
                        ]);
                    }
                } else {
                    if ($request->input('tempType') === 'pb') {
                        $message = $this->ip . ' Failed order paid not refunded - Reorderid : ' . $Reorderid;
                        $this->pbRefundAlertEmail($message);
                    }
                    // check if it's price line duplicate confirmation
                    if (isset($result['errorId']) && $result['errorId'] === 'ER1415') {
                        return response()->json([
                            'success' => false,
                            'errorId' => $result['errorId'],
                            'Reorderid' => $Reorderid
                        ]);
                    }

                    return response()->json([
                        "success" => false,
                        "errorId" => isset($result['errorId']) ? $result['errorId'] : "IT1001",
                        "errorMessage" => isset($result['errorMessage']) ? $result['errorMessage'] : 'Booking failed'
                    ]);
                }
            } else { // if book response does not have 'success' key ( api may throw exception with no such key )
                if ($request->input('tempType') == 'pb') {
                    $message = $this->ip . 'book response does not have success key: Failed order paid not refunded - Reorderid : ' . $Reorderid;
                    $this->pbRefundAlertEmail($message);
                }
                return response()->json([
                    'success' => false,
                    'errorId' => isset($result['errorId']) ? $result['errorId'] : '',
                    'errorMessage' => 'Booking failed'
                ]);
            }

        } elseif ($request->input('tempType') == 'cc') {
            if ($amount >= config('constants.max_price')) {
                $msg = "客户名：" . $booker['firstname'] . ' ' . $booker['lastname'] . ', 订购金额: ' . $tempInfo->currencyCode . ' ' . $amount . ',  订购酒店名: ' . $tempInfo->goods_name . ', 房型: ' . $roomDetails['name'] . ', 早餐类型: ' . $roomDetails['boardName'] . ', 房间数量: ' . $roomDetails['roomCount'] . ', 电话: ' . $booker['phone'] . ', 邮箱: ' . $booker['email'] . ', 临时订单号: ' . $Reorderid;
                try {
                    Mail::raw($msg, function ($message) {
                        $message->subject('信用卡付款额度超过' . config('constants.max_price') . '!');
                        $message->to(config('staff-emails.ann'));
                        $message->to(config('staff-emails.todd'));
                    });
                } catch (\Exception $e) {
                    Log::error($this->ip . ' send cc amount ceiling email failed, ' . $msg . ', error:' . $e->getMessage());
                }
            }
            if (isset($roomDetails['rateClass']) && $roomDetails['rateClass'] == '不可取消' && !config('app.enable_nonrefundable_cc')) {
                return response()->json([
                    'success' => false,
                    'errorId' => 'IT1001',
                    'voucherUrl' => '',
                ]);
            }

            return response()->json([
                'success' => true,
                'errorId' => '',
                'voucherUrl' => url('paypal') . '/' . $tempInfo->Reorderid,
            ]);
        } elseif ($request->input('tempType') == 'we' || $request->input('tempType') == 'al') // wechatpay or alipay
        {
            if ($request->input('tempType') == 'al') {
                $vendor = 'alipay';
            } else {
                $vendor = 'wechatpay';
            }

            $isMobile = $this->isMobileDevice();
            if ($isMobile) {
                $isWechatBrowser = $request->input('isWechatBrowser') == 'true' ? true : false;
                $url = CitconPay::h5pay($tempInfo->amount, $tempInfo->currencyCode, $tempInfo->Reorderid, $vendor,
                    $isWechatBrowser);
                Log::info($this->ip . ' citcon h5 pay url: ' . $url);
            } else {
                $url = CitconPay::qrpay($tempInfo->amount, $tempInfo->currencyCode, $tempInfo->Reorderid, $vendor);
                Log::info($this->ip . ' citcon qr pay url: ' . $url);
            }

            return response()->json([
                'success' => true,
                'errorId' => '',
                'voucherUrl' => $url,
                'reference' => $tempInfo->Reorderid
            ]);
        }

    }

    /**
     * @param $request
     * @param $sessionKey - api sessionKey
     * @param $roomDetails - search rooms cache
     * @return mixed
     */
    private function storeTempOrder(Request $request, $sessionKey, $roomDetails)
    {
        $specialRequest = $request->input('specialrequest');
        $specialRequest = empty($specialRequest) ? '' : implode("; ", $specialRequest);

        $bookInfo = [
            'currency' => $roomDetails['currency'],
            'amount' => $roomDetails['netPrice'],
        ];

        $displayId = $request->input('displayId');
        $redisSearchRoomsKey = $request->input('rooms_key'); // api sessionKey

        $rooms = json_decode(Cache::get($redisSearchRoomsKey, ''), true);

        if (empty($rooms) || !isset($rooms['searchInfo'])) {
            Log::error($this->ip . ' reserve searchInfo did not find');
            if (config('app.env') === 'production') {
                try {
                    Mail::raw($this->ip . '缓存过期报错', function ($message) {
                        $message->subject('缓存过期报错，数据库存储失败');
                        $message->to(config('staff-emails.ann'));
                    });
                } catch (\Exception $e) {
                    Log::error($this->ip . '缓存过期报错，数据库存储失败email失败, ' . $e->getMessage());
                }
            }
            return null;
        }

        $searchInfo = $rooms['searchInfo']; // get searchInfo from cached searchRooms data
        $occupancies = $searchInfo['occupancies'];
        $currencyCode = $bookInfo['currency'];
        $amount = $bookInfo['amount'];

        if ($request->has('totalPrice')) {
            $amount = $request->input('totalPrice');
        }

        // 生成rooms信息
        $rooms = $this->buildRooms($occupancies, $request);
        $rooms[0]['roomDetails'] = $roomDetails;
        $rooms[0]['roomDetails']['cleanFee'] = 0;
        $isEbooking = strpos($rooms[0]['roomDetails']['roomReference'], 'eb:') !== false ? true : false;
        if ($isEbooking) {
            $ebRoom = EbRoom::where('hotelId', $rooms[0]['roomDetails']['hotelId'])
                ->Where('room_type_id', $rooms[0]['roomDetails']['roomTypeId'])
                ->first();
            if (!empty($ebRoom) && isset($ebRoom->remark)) {
                if (isset($rooms[0]['roomDetails']['comments'][0]['comments'])) {
                    $rooms[0]['roomDetails']['comments'][0]['comments'] = $ebRoom->remark;
                } else {
                    $rooms[0]['roomDetails']['comments'] = $ebRoom->remark;
                }
            }
            if (!empty($ebRoom) && isset($ebRoom->clean_fee) && $ebRoom->clean_fee !== 'undefined') {
                $rooms[0]['roomDetails']['cleanFee'] = $ebRoom->clean_fee;
            }
        }
        $langId = session('language', 0);

        //联系人信息
        $booker = [
            'firstname' => $request->input('booker_fn'),
            'lastname' => $request->input('booker_ln'),
            'phone_country' => $request->input('phone_country'),
            'phone' => $request->input('booker_phone'),
            'email' => str_replace('\u2006', '', str_replace(' ', '', $request->input('booker_email'))),
            'http_host' => $this->host,
            'language' => $langId,
        ];

        //生成预定订单号
        $userId = $this->partnerId;
        $Reorderid = substr(md5($sessionKey . $userId . time()), 0, 16);

        // convert usd and cny amount
        $USD_cost = $amount;
        if ($bookInfo['currency'] != 'USD' && $bookInfo['currency'] != '') {
            $USD_cost = Util::httpGet("http://post.usitrip.com/index.php?mod=api_public&action=currencyConversion&money=" . $amount . "&source_code=" . $bookInfo['currency'] . "&out_code=USD");
            $USD_cost = json_decode($USD_cost, true);
            if ($USD_cost['Value'] !== false) {
                $USD_cost = round($USD_cost['Value'], 2);
            } else {
                $USD_cost = 0;
            }
        }
        //添加目前必须是美元的ebooking部分酒店clean fee-- 10112018 Ann
        $amount = $rooms[0]['roomDetails']['cleanFee'] + $amount;

        $cny = $this->changeCurrency($amount, $currencyCode !== '' ? $currencyCode : 'USD', 'CNY');

        $source = isset($_COOKIE['source']) ? $_COOKIE['source'] : ''; // source is used for paying distributor contribution

        $user = session('user', ''); // get logged in user (refer to HomeController@index)
        $userEmail = isset($user['customers_email_address']) ? $user['customers_email_address'] : '';
        if (empty($userEmail)) {
            $userEmail = $booker['email'];
        }

        if ($source == false || $source == '' || $source == 'null' || $source == 'undefined') {
            $source = '';
        }

        // get sales number
        if ($request->has('no')) {
            $salesNo = $request->input('no');
        } elseif (isset($_COOKIE['a'])) {
            $salesNo = $_COOKIE['a'];
        } elseif (session()->has('sales_no')) {
            $salesNo = session('sales_no');
        } else {
            $salesNo = 0;
        }

        $remark = $request->input('booker_remark');
        $remark = mb_substr($remark, 0, 50);
        $clientIp = $this->ip;

        $hotel = Hotel::where('hotelId', $displayId)
            ->first();

        //coupon减价额度必须是美元
        if ($request->input('coupon_code') !== '') {
            $coupon = json_decode(session('coupon'));
            if ($coupon) {
                $test = $USD_cost - $coupon->amount;
                $USD_cost = $test;
                $coupon_history = new CouponHistory();
                $coupon_history->coupon_id = $coupon->id;
                $coupon_history->hotel_id = $displayId;
                $coupon_history->save();
            }
        }

        $data = [
            'Reorderid' => $Reorderid,
            'booker' => json_encode($booker),
            'search_info' => json_encode($searchInfo),
            'bookIndex' => $request->input('bookIndex'),
            'partnerId' => $userId,
            'currencyCode' => 'USD',
            'amount' => $USD_cost,
            'created_at' => date('Y-m-d H:i:s'),
            'hotelId' => $request->input('hotelId'),
            // have to use hotelId from request as the $hotel is get from displayId
            'display_hotelId' => $displayId,
            'rooms' => json_encode($rooms),
            'sessionKey' => $sessionKey,
            'goods_name' => $hotel->name,
            'goods_description' => $hotel->description,
            'cny' => $cny,
            'remark' => $remark,
            'source' => $source,
            'user_email' => $userEmail,
            // it's booker email
            'special_request' => $specialRequest,
            'sales_no' => $salesNo,
            'client_ip' => $clientIp,
            'mandatory_fee' => $roomDetails['mandatoryFees'] ? $roomDetails['mandatoryFees'] : $request->input('total_mandatory_fees'),
            //Fees due at hotel 02212018
            'booker_local_time' => $request->input('booker_local_time'),
            //booker local server time 003012018
            'is_pc' => Agent::isDesktop() ? 1 : 2,
            //1是pc，2是手机
            'coupon_code' => isset($coupon) ? $coupon->code : ''
        ];

        //create temp order
        try {
            $id = OrderTem::insertGetId($data);
            Log::info($this->ip . ' successfully saved temp_info, sessionKey: ' . $sessionKey . ', Reorderid: ' . $Reorderid);

            //Send following up message contains hotel info url after 20 mins after save temp info except for 117book
            if (strpos($this->host, '117book') === false && config('app.env') !== 'local') {
                $url_long = $_SERVER['HTTP_REFERER'];
                $link = SinaUrl::getShort($url_long);
                $language = session('language', 0) == 0 ? 'cn' : 'en';
                $host = strpos($this->host, 'usitour') ? 'Usitour' : 'Usitrip';
                Log::info($this->ip.' send follow up email to phone '.$request->input('phone_country') . $request->input('booker_phone') .' short link is: '. $link .' in language '. $language .' at host: '. $host . ' with reorder id: '.$Reorderid);
                $job = (new SendReminderSms($request->input('phone_country'), $request->input('booker_phone'), $link,
                    $language, $host, $Reorderid))->delay(config('app.sms_delay'));
                $this->dispatch($job);
            }

        } catch (\Exception $exception) {
            try {
                Mail::raw($exception->getMessage(), function ($message) {
                    $message->subject('BookController的Reserve过程中临时数据库存储失败');
                    $message->to(config('staff-emails.tech'));
                });
            } catch (\Exception $e) {
                Log::error($this->ip . ' 发送临时订单存数据库失败email失败, ' . $e->getMessage());
            }

            return false;
        }

        $temp_order = OrderTem::find($id);

        if (config('app.enable_email_campaign')) {
            try {
                //create infusionsoft contact
                $infusionsoft = new InfusionsoftAPI();
                $language = $langId == '0' ? 'cn' : ($langId == 1 ? 'en' : 'tw');
                $contact = $infusionsoft->createContact($booker['email'], [
                    [
                        'id' => 18,
                        'content' => $language
                    ]
                ], 'new book');

                //update infusionsoft contact
                if (!empty($contact)) {
                    $newsTagId = $language == 'cn' ? 135 : 133;

                    $contact_fields = [
                        'language' => $language,
                        'firstname' => $booker['firstname'],
                        'lastname' => $booker['lastname'],
                        'phone' => $booker['phone'],
                        'display_hotelId' => $temp_order->display_hotelId,
                        'domain' => config('app.url')
                    ];

                    // detailed hotel cache from hotel page
                    $redisHotelRecKey = $temp_order->sessionKey . '.detailedRecHotels';
                    $hotels = [];

                    if (Cache::has($redisHotelRecKey)) {
                        $hotels = json_decode(Cache::get($redisHotelRecKey), true);
                    }

                    $hotel_interest_cam = 'hotelInterest';
                    if (config('app.env') === 'local') {
                        $hotel_interest_cam = 'hotelInterestTest';
                    }

                    $this->dispatch(new UpdateInfusionHotelsData($contact, $contact_fields, $hotel,
                        array_slice($hotels, 0, 3), [$hotel_interest_cam], [$newsTagId]));

                    unset($hotels);
                }
            } catch (\Exception $exception) {
                Log::error($this->ip . ' infusionsfot create contact error: ' . $exception->getMessage());
            }
        }

        return $temp_order;
    }

    /**
     * @param $amount
     * @param $currency
     * @param string $out_code
     * @return float|int
     */
    private function changeCurrency($amount, $currency, $out_code = 'USD')
    {
        $payment = $this->httpGet("http://post.usitrip.com/index.php?mod=api_public&action=currencyConversion&money=$amount&source_code=$currency&out_code=$out_code");
        if ($payment !== false) {
            $payment = json_decode($payment, true);
            if ($payment['Value'] !== false) {
                return round($payment['Value'], 2);
            }
        }

        return null;
    }

    /**
     * @param $occupancies
     * @param $request
     * @return array
     */
    private function buildRooms($occupancies, $request)
    {
        $roomIndex = 1;
        $rooms = [];

        foreach ($occupancies as $occ) {
            $adultCount = $occ['adults'];
            $childCount = $occ['children'];

            for ($k = 0; $k < $occ['rooms']; $k++) {
                $k1 = $k + 1;
                $room['roomReference'] = $request->input('roomRef' . $roomIndex);

                $paxes = [];

                // get adults' name
                for ($i = 1; $i <= $adultCount; $i++) {
                    $nameIndex = $k1 . '_a' . 1;
                    if ($request->input('guest' . $roomIndex . '_a' . $i . '_fn') != "") {
                        $nameIndex = $roomIndex . '_a' . $i;
                    }

                    $paxes[] = [
                        'type' => 'AD',
                        'name' => $request->input('guest' . $nameIndex . '_fn'),
                        'surname' => $request->input('guest' . $nameIndex . '_ln'),
                    ];
                }

                // get children's name
                for ($i = $adultCount + 1; $i <= $adultCount + $childCount; $i++) {
                    $age = isset($occ['paxes'][$i - 1]) && isset($occ['paxes'][$i - 1]['age']) ? $occ['paxes'][$i - 1]['age'] : 6;
                    $nameIndex = $k1 . '_a' . 1;
                    if ($request->input('guest' . $roomIndex . '_a' . $i . '_fn') != "") {
                        $nameIndex = $roomIndex . '_a' . $i;
                    }

                    $paxes[] = [
                        'type' => 'CH',
                        'age' => $age,
                        'name' => $request->input('guest' . $nameIndex . '_fn'),
                        'surname' => $request->input('guest' . $nameIndex . '_ln'),
                    ];
                }

                $room['paxes'] = $paxes;
                $room['name'] = $request->input('room' . $i);

                $rooms[] = $room;

                ++$roomIndex;
            }
        }

        return $rooms;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function pbPaymentData(Request $request)
    {
        $data = $request->input('data');
        $type = $request->input('type');
        $ppResult = json_decode($data, true);
        if ($type === 'request') {
            $payId = $ppResult['paymentID'];
            $data = [
                'transactionId' => $payId,
                'request' => $data,
                'created_at' => date('Y-m-d H:i:s'),
                'transaction_type' => 'Paypal Button',
            ];
            try {
                $id = Payment::insertGetId($data);
                Log::info($this->ip . ' Paypal button付款数据库存储request成功: ' . $id);
            } catch (\Exception $exception) {
                try {
                    Mail::raw($exception->getMessage(), function ($message) {
                        $message->subject('Paypal button付款数据库request存储失败');
                        $message->to(config('staff-emails.ann'));
                    });
                } catch (\Exception $e) {
                    Log::error($this->ip . ' 发送Paypal button付款数据库request存储失败email失败, ' . $e->getMessage());
                }

                return response()->json([
                    'success' => false,
                    'payId' => $payId ? $payId : '',
                ]);
            }
        } else {
            try {
                $payId = $ppResult['id'];
                $payment_history = Payment::where('transactionId', $payId)
                    ->first();
                if (!json_decode($payment_history, true)) {
                    $data = [
                        'transactionId' => $payId,
                        'created_at' => date('Y-m-d H:i:s'),
                        'transaction_type' => 'P',
                    ];
                    try {
                        $id = Payment::insertGetId($data);
                        $payment_history = Payment::where('id', $id)
                            ->first();
                        Log::info($this->ip . ' Paypal button付款数据库response存储成功: ' . $id);
                    } catch (\Exception $e) {
                        try {
                            Mail::raw($e->getMessage(), function ($message) {
                                $message->subject('Paypal button付款数据库response存储失败');
                                $message->to(config('staff-emails.ann'));
                            });
                        } catch (\Exception $e) {
                            Log::error($this->ip . ' 发送Paypal button付款数据库response存储失败email失败, ' . $e->getMessage());
                        }
                    }
                }
                $payment_history->transactionId = $payId;
                $payment_history->ppref = $ppResult['transactions'][0]['related_resources'][0]['sale']['id'];
                $payment_history->partnerId = $this->partnerId;
                $payment_history->booker_email = $ppResult['payer']['payer_info']['email'];
                $payment_history->operation_result = $data;
                $payment_history->amount = $ppResult['transactions'][0]['amount']['total'];
                $payment_history->currency = $ppResult['transactions'][0]['amount']['currency'];
                $payment_history->first_name = $ppResult['payer']['payer_info']['first_name'];
                $payment_history->last_name = $ppResult['payer']['payer_info']['last_name'];
                $payment_history->save();
                Log::info($this->ip . ' Paypal button付款数据库更新result存储成功: ' . $payment_history->id);
            } catch (\Exception $exception) {
                try {
                    $isMobile = $this->isMobileDevice();
                    Log::info($this->ip . '手机?' . $isMobile . ',paypal button 存储数据库失败data：' . json_encode($ppResult));
                    Mail::raw($exception->getMessage(), function ($message) {
                        $message->subject('Paypal button付款数据库更新result存储失败');
                        $message->to(config('staff-emails.ann'));
                    });
                } catch (\Exception $e) {
                    Log::error($this->ip . ' 发送Paypal button付款数据库存储更新result失败email失败, ' . $e->getMessage());
                }

                return response()->json([
                    'success' => false,
                    'payId' => $payId ? $payId : '',
                ]);
            }
        }

        return response()->json([
            'success' => true,
            'payId' => $payId ? $payId : '',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyCoupon(Request $request)
    {
        $message = "We don’t recognize that coupon code";

        if ($request->has(['code', 'hotelId'])) {
            $code = $request->input('code');
            $hotelId = $request->input('hotelId');

            $coupon = Coupon::where('code', $code)
                ->first();
            if (!empty($coupon)) {
                $coupon_valid = true;
                $today = Carbon::now();
                $checkin = $request->has('checkin') ? Carbon::parse($request->input('checkin')) : null;
                if (isset($coupon->start_at) && Carbon::parse($coupon->start_at)
                        ->greaterThan($today)) {
                    $message = 'Coupon is not available yet';
                    $coupon_valid = false;
                }
                if (isset($coupon->expires_at) && Carbon::parse($coupon->expires_at)
                        ->lessThan($today)) {
                    $message = 'Coupon is already expired';
                    $coupon_valid = false;
                }
                if (!empty($checkin) && isset($coupon->checkin_start_date) && isset($coupon->checkin_end_date)) {
                    if ((Carbon::parse($coupon->checkin_start_date)
                            ->gt($checkin) || Carbon::parse($coupon->checkin_end_date)
                            ->lt($checkin))) {
                        $message = 'Coupon is not valid for this checkin date';
                        $coupon_valid = false;
                    }
                }

                if ($coupon_valid) {
                    $hotelIds = explode(',', $coupon->hotelIds);

                    if (in_array($hotelId, $hotelIds)) {
                        session(['coupon' => json_encode($coupon)]);
                        return response()->json([
                            'success' => true,
                            'amount' => $coupon->amount
                        ]);
                    } else {
                        $message = "Coupon not available on this hotel";
                    }
                }
            }
        }

        return response()->json([
            'success' => false,
            'message' => $message
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeCoupon()
    {
        session()->forget('coupon');

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * priceline存在上游在未通知我们的情况下修改返回值格式导致上游预订成功，但是写订单表失败的情况
     * @param $Reorderid
     * @return bool
     */
    public function pricelineBookSuccessDataFailed($Reorderid)
    {
        //a：邮件技术该情况发生需要注意
        try {
            Mail::raw($Reorderid . ' 预订成功没有订单voucher，需要持续关注，并重发预订成功邮件给客人', function ($message) {
                $message->subject('Warning! Priceline get book data failed. Error Message From: ' . $_SERVER['HTTP_HOST'] . ' Site');
                if (config('app.env') == 'production') {
                    $message->to(config('staff-emails.henry'))
                        ->cc(config('staff-emails.ann'))
                        ->cc(config('staff-emails.jie'));
                    if (strrpos($_SERVER['HTTP_HOST'], "117book") > -1 || $_SERVER['HTTP_HOST'] == 'B2B') {
                        //todo: who is this
                        $message->cc(config('staff-emails.bdcn'));
                    }
                } else {
                    $message->to(config('staff-emails.ann'));
                }
            });
        } catch (\Exception $e) {
            Log::error($this->ip . ' Priceline missed book success data email send error: ' . $e->getMessage());
        }
        //b：邮件客户已定上，之后将会再次发送确认email
        Log::info($this->ip . $Reorderid . '----------先发送一封Pre预订成功邮件给客户');
        $this->newPreOrderEmail($Reorderid);
        //c:页面客户显示已定上，之后将会发送确认email
        return response()->json([
            'success' => false,
            'errorId' => 'IT1004'
        ]);
    }

    /**
     * priceline 或其他上游没有返回准确数据，java api在bookRoom结果data返回null时，先邮件给客户一封pre confirmed预计
     * @param $Reorderid
     */
    public function newPreOrderEmail($Reorderid)
    {
        //根据orderTem发送成功下单邮件
        $tempInfo = OrderTem::where('Reorderid', $Reorderid)
            ->first();
        $booker = json_decode($tempInfo->booker, true);
        $searchInfo = json_decode($tempInfo->search_info, true);
        $rooms = json_decode($tempInfo->rooms, true);
        //Email New Order
        try {
            $executionStartTime = microtime(true);
            $partner = 'Customer';
            $hotelName = $tempInfo['goods_name'];
            $totalPrice = $tempInfo['currencyCode'] . ' ' . $tempInfo['amount'];
            $bookerEmail = $booker['email'];
            $bookerName = $booker['firstname'] . ' ' . $booker['lastname'];
            $roomInfo = $rooms[0]['roomDetails'];
            $boardName = $roomInfo['boardName'];
            $roomType = $roomInfo['name'];
            $cancellationInfo = isset($roomInfo['cancellationPolicies']['0']) ? $roomInfo['cancellationPolicies']['0'] : null;
            $cancellation = isset($cancellationInfo['end']) ? $cancellationInfo['end'] : $roomInfo['cancelDate'];
            $cancellation = str_replace("T", " ", mb_substr($cancellation, 0, 19));
            $cancelTime = $cancellation !== '' ? strtotime($cancellation) : 'N/A';
            $orderTime = $tempInfo->created_at;
            if (!$cancellation) {
                $cancellation = "Non-Refundable";
            } elseif ($cancelTime === 'N/A') {
                $cancellation = $cancelTime;
            } elseif ($cancelTime < $orderTime) {
                $cancellation = "Non-Refundable";
            } else {
                $cancellation = "Free Cancellation before " . $cancellation;
            }
            $roomNumber = $roomInfo['roomCount'];
            $checkin = $searchInfo['checkin'];
            $checkout = $searchInfo['checkout'];
            $guestInfo = $rooms[0]['paxes']['0'];
            $firstName1 = $guestInfo['name'];
            $lastName1 = $guestInfo['surname'];
            $guest = $firstName1 . ' ' . $lastName1;
            $ad_total = substr($roomInfo['paxes'], 0, 1);
            $ch_total = substr($roomInfo['paxes'], 3, 1);
            $locale = $booker['language'];
            Log::info($this->ip . ' locale language of user: ' . $locale);
            $host = $booker['http_host'];

            $langId = 0;
            $hostName = 'Usitour';
            $hostNameCn = '';
            if (strpos($host, 'usitrip') !== false) {
                $hostName = 'Usitrip';
                $hostNameCn = '走四方';
            } elseif (strrpos($host, 'supervacation') !== false) {
                $hostName = 'Supervacation';
            } elseif (strrpos($host, '117book') !== false) {
                $hostName = '117book';
                $hostNameCn = $locale == 2 ? '要趣訂' : '要趣订';
            }

            if ($locale == 0) {
                $emailSubject = $hostName . $hostNameCn . '酒店Pre预订确认';
                Log::info($this->ip . ' 发送pre确认中文邮件');
            } elseif ($locale == 2) {
                $langId = 2;
                $emailSubject = $hostName . $hostNameCn . '飯店Pre預定確認';
                Log::info($this->ip . ' 发送pre确认中文邮件');
            } else {
                $langId = 1;
                $emailSubject = $hostName . ' Hotel Pre Confirmation';
                Log::info($this->ip . ' 发送pre确认英文邮件');
            }

            $bcc = config('staff-emails.ann'); //Todd想看所有的117book新预订邮件, config does not comply with RFC 2822, 3.6.2. ？？ use email directly
            if (strrpos($emailSubject, "117book") > -1) {
                $bcc = $this->annbdcn;
            }
            //todo:邮件的内容写得是预订成功，一定是预定成功了吗？
            try {
                Mail::queue('emails.pre-confirmed-order', [
                    'partner' => $partner,
                    'checkin' => $checkin,
                    'checkout' => $checkout,
                    'hotelName' => $hotelName,
                    'guest' => $guest,
                    'roomCount' => $roomNumber,
                    'totalPrice' => $totalPrice,
                    'adNumber' => $ad_total,
                    'chNumber' => $ch_total,
                    'roomType' => $roomType,
                    'boardType' => $boardName,
                    'cancellation' => $cancellation,
                    'host' => $host,
                    'langId' => $langId,
                    'hostName' => $hostName
                ], function ($message) use (
                    $bookerEmail, $bookerName, $emailSubject, $bcc
                ) {
                    $message->subject($emailSubject)
                        ->to($bookerEmail, $bookerName)
                        ->bcc($bcc);
                });
                Log::info($this->ip . ' 发送preConfirmed预定邮件');
            } catch (\Exception $e) {
                Log::error($this->ip . ' send new pre confirmed email error: ' . $e->getMessage());
            }
            $executionEndTime = microtime(true);
            //The result will be in seconds and milliseconds.
            $seconds = $executionEndTime - $executionStartTime;
            Log::info($this->ip . ' TIME to send pre confirmed email for ' . $Reorderid . ' is ' . $seconds . ' seconds');
        } catch (\Exception $e) {
            Log::error($this->ip . ' Send pre confirmed new order email failed, ' . $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $sessionKey
     * @param $Reorderid
     * @return \Illuminate\Http\JsonResponse
     */
    public function salesReserve(Request $request, $sessionKey, $Reorderid) // similar to reserve
    {
        Log::info($this->ip . 'sales reserve reorderid:' . $Reorderid . ' start');
        $redisBookRoomKey = $request->input('displayId') . ':' . urlencode($request->Input('bookIndex')) . 1;
        $roomDetails = Cache::get($redisBookRoomKey);
        $roomDetails = json_decode($roomDetails, true);

        $tempInfo = $this->updateTempOrder($request, $sessionKey, $Reorderid, $roomDetails, 'salesReserve');

        // cache placing order key to prevent book two times
        $placingOrderKey = $Reorderid . 'placingOrder';

        if (Cache::has($placingOrderKey) || !empty($tempInfo->orderReference)) {
            return response()->json([
                'success' => false,
                'message' => '有人已经点了预定，请稍后',
                'errorId' => 'EP1002'
            ]);
        }

        Cache::put($placingOrderKey, true, $this->cacheLifeTime);

        $sus_order = SuspiciousOrder::where('Reorderid', $Reorderid)
            ->get()
            ->last(); // this is the suspicious order
        $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';
        $sus_order->sales_no = $sales_no;
        $sus_order->save();
        $susBooker = json_decode($sus_order->booker, true);
        $salesReserveBooker = json_decode($tempInfo->booker, true);
        $salesReserveBooker['countryCodePhone'] = $susBooker['countryCodePhone'];
        $salesReserveBooker['validPhoneNumber'] = $susBooker['validPhoneNumber'];

        if ($sus_order->amount >= $tempInfo->amount) {
            $user = User::find($this->partnerId);
            $api = new HotelAPI($user);
            $booker = $salesReserveBooker;
            $rooms = json_decode($tempInfo->rooms, true);
            $specialRequest = '';

            $bookRoomResult = $api->bookRooms($booker, $rooms, $specialRequest);

            if (isset($bookRoomResult['success']) && $bookRoomResult['success']) {
                $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';  //117 or 19 will be 0 at this time, since no cookie left
                $sus_order->update([
                    'isLocked' => 0,
                    'isDone' => 1,
                    'sales_no' => $sales_no,
                ]);
                $bookRoomResult = $bookRoomResult['data'];
                if ($bookRoomResult === null) {
                    $sus_order->update([
                        'commend' => Carbon::today()
                                ->format('m/d') . ' 预订成功，但是数据存储失败，技术会实时向上游请求数据 (' . $sales_no . ')' . '&#013;' . $sus_order->commend,
                    ]);
                    $this->pricelineBookSuccessDataFailed($Reorderid);
                } else {
                    $transactionId = $sus_order['payment_id'];
                    Payment::where('transactionId', $transactionId)
                        ->update([
                            'orderReference' => $bookRoomResult['orderReference']
                        ]);

                    $tempInfo->orderReference = $bookRoomResult['orderReference'];
                    $tempInfo->code = $bookRoomResult['code'];
                    $tempInfo->save();

                    Order::where('orderReference', $bookRoomResult['orderReference'])
                        ->update([
                            'infact_cost' => $tempInfo->amount,
                            'payment_type' => 'CC',
                            'payment_status' => 'PAID',
                            'remark' => $tempInfo->remark,
                            'source' => $tempInfo->source,
                            'source_pos' => isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '',
                            'special_request' => $specialRequest,
                            'sales_no' => $tempInfo->sales_no,
                            'user_email' => $tempInfo->user_email,
                            'http_host' => $sus_order['http_host'],
                            'user_language' => $sus_order['language'],  //pass lang to porder table
                        ]);

                    $order = Order::where('orderReference', $bookRoomResult['orderReference'])
                        ->first();

                    if (!empty($order) && $order->provider === 'EBooking') {
                        Log::info('4: salesReserve eb email send' . $order->id);
                        $this->sendEbEmail($order);
                    }
                    //usitour用户给发送预订成功sms
                    if (strrpos($sus_order['http_host'], "usitour") > -1) {
                        try {
                            $booker = json_decode($tempInfo->booker, true);
                            Log::info('for sms $booker info sales reserve : ' . json_encode($booker));
                            //For all new order, send sms
                            $input_country_code = isset($booker['countryCodePhone']) ? $booker['countryCodePhone'] : $booker['phone_country'];
                            $input_mobile = isset($booker['validPhoneNumber']) ? $booker['validPhoneNumber'] : $booker['phone'];
                            $code = SinaUrl::getShort($order->voucher());
                            $sendSms = new sendMessage();
                            $sendSms->sendOrderSms($input_country_code, $input_mobile, $code);
                        } catch (\Exception $e) {
                            Log::error($this->ip . ' sales reserve send new order message error: ' . $e->getMessage());
                        }
                    }

                    $he = new hotelControllerEmail();
                    Log::info('----------' . $request->input('tempType') . ': fraud order book successfully:: ' . $order->id . ' ----------');
                    $he->newOrderEmail($tempInfo, $bookRoomResult);

                    //预定完成，更新infusionsoft contact，以发送feedback邮件
                    $langString = $sus_order['language'] == 0 ? 'cn' : 'en';
                    $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'hotel.usitrip.com';
                    $this->dispatch(new TriggerFeedback($booker['email'], $langString, $domain));

                    Cache::forget($placingOrderKey); // remove placing order key

                    return response()->json([
                        'success' => true,
                        'errorId' => '',
                        'voucherUrl' => url('voucher/' . $bookRoomResult['code'])
                    ]);
                }
            } elseif (isset($bookRoomResult['errorId']) && $bookRoomResult['errorId'] === 'ER1415') { // if it's duplicate booking error from api
                Cache::forget($placingOrderKey); // remove placing order key
                $payment_history_id = Payment::where('transactionId', $sus_order->payment_id)
                    ->first()->id;

                return response()->json([
                    'success' => false,
                    'errorId' => $bookRoomResult['errorId'],
                    'Reorderid' => $Reorderid,
                    'p_id' => $payment_history_id,
                ]);
            } else {
                Cache::forget($placingOrderKey); // remove placing order key

                return response()->json([
                    'success' => true,
                    'errorId' => isset($bookRoomResult['errorId']) ? $bookRoomResult['errorId'] : 'FL1001',
                    'voucherUrl' => ''
                ]);
            }
        } else {
            Cache::forget($placingOrderKey); // remove placing order key

            $bookerEmail = json_decode($tempInfo->booker)->email;
            $url = 'https://' . $sus_order->http_host . '/paypal/extra-pay/' . $tempInfo->Reorderid;

            // update the new amount user should pay
            $tempInfo->amount = $tempInfo->amount - $sus_order->amount;
            $currencyCode = $tempInfo['currency'];
            if ($currencyCode != '') {
                $cny = $this->changeCurrency($tempInfo->amount, $currencyCode, 'CNY');
            } else {
                $cny = $this->changeCurrency($tempInfo->amount, 'USD', 'CNY');
            }
            $tempInfo->cny = $cny;
            $payLineEmail = SuspiciousOrder::where('Reorderid', $Reorderid)
                ->first();
            $language = $payLineEmail->language;
            $host = $payLineEmail->http_host;
            try {
                $tempInfo->save();
                $langId = 0;
                $hostName = 'Usitrip';
                if (strrpos($host, 'usitour') !== false) {
                    $hostName = 'Usitour';
                }
                if ($language === 0) {
                    $subject = '您的酒店预订需补差价，请确认！' . $Reorderid . ' !';
                } elseif ($language === 2) {
                    $subject = '你的酒店預訂需要補差價，請確認！' . $Reorderid . ' !';
                    $url = $url . '?lang=tw';
                    $langId = 2;
                } else {
                    $subject = 'Extra Pay Link for your hotel reservation' . $Reorderid . ' !';
                    $langId = 1;
                }

                Mail::send('emails.extra-pay-link', [
                    'url' => $url,
                    'langId' => $langId,
                    'hostName' => $hostName
                ], function ($message) use ($bookerEmail, $Reorderid, $subject) {
                    $message->to($bookerEmail)
                        ->bcc(config('staff-emails.ann'))
                        ->subject($subject);
                });

                $sus_order->update([
                    'commend' => Carbon::today()
                            ->format('m/d') . ' 已成功发送额外付款链接给客人(' . $sales_no . ')' . '&#013;' . $sus_order->commend,
                ]);

                return response()->json([
                    'success' => true,
                    'errorId' => 'EP1001',
                    'voucherUrl' => ''
                ]);
            } catch (\Exception $e) {
                Log::error($this->ip . ' send email to user for extra pay error' . $e->getMessage());
                return response()->json([
                    'success' => false,
                ]);
            }
        }
    }

    /**
     *  similar to reserve but for citcon not open call back url Ann- 05032018
     * @param \Illuminate\Http\Request $request
     * @param $sessionKey
     * @param $Reorderid
     * @return \Illuminate\Http\JsonResponse
     */
    public function missedSalesReserve(Request $request, $sessionKey, $Reorderid)
    {
        Log::info($this->ip . 'sales reserve for missed order with reorderid:' . $Reorderid . ' start');
        $redisBookRoomKey = $request->input('displayId') . ':' . urlencode($request->Input('bookIndex')) . 1;
        $roomDetails = Cache::get($redisBookRoomKey);
        $roomDetails = json_decode($roomDetails, true);
        $originalPaid = OrderTem::where('Reorderid', $Reorderid)
            ->get()
            ->last()->amount;
        $tempInfo = $this->updateTempOrder($request, $sessionKey, $Reorderid, $roomDetails, 'missedSalesReserve');

        // cache placing order key to prevent book two times
        $placingOrderKey = $Reorderid . 'placingOrder';
        if (Cache::has($placingOrderKey) || !empty($tempInfo->orderReference)) {
            return response()->json([
                'success' => false,
                'message' => '有人已经点了预定，请稍后',
                'errorId' => 'EP1002'
            ]);
        }

        Cache::put($placingOrderKey, true, $this->cacheLifeTime);

        $missed_order = OrderTem::where('Reorderid', $Reorderid)
            ->get()
            ->last(); // missed order paid not booked
        $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';
        $missed_order->sales_no = $sales_no;
        $missed_order->save();
        $nowNeedPay = $tempInfo->amount;
        $okToPay = ($nowNeedPay <= $originalPaid) ? true : false;
        //        判断是否需要补差价
        if ($okToPay) {
            //换成11087的等同值都可以，先这么写以后合并b的missedorder
            $user = User::find($missed_order->partnerId);
            $api = new HotelAPI($user);
            $booker = json_decode($tempInfo->booker, true);
            $rooms = json_decode($tempInfo->rooms, true);
            $specialRequest = ''; //C端客人不用特殊请求
            $bookRoomResult = $api->bookRooms($booker, $rooms, $specialRequest);
            //预订成功的情况
            if (isset($bookRoomResult['success']) && $bookRoomResult['success']) {
                $bookRoomResult = $bookRoomResult['data'];

                $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';
                $missed_order->update([
                    'paid_not_booked' => 2,
                    //0 is default, 1 is paid not booked, 2 is solved paid not booked，3 is declined
                    'sales_no' => $sales_no,
                ]);

                if ($bookRoomResult === null) {
                    $missed_order->update([
                        'commend' => Carbon::today()
                                ->format('m/d') . ' 预订成功，但是数据存储失败，技术会实时向上游请求数据 (' . $sales_no . ')' . '&#013;' . $missed_order->commend,
                    ]);
                    $this->pricelineBookSuccessDataFailed($Reorderid);
                } else {
                    $transactionId = $missed_order['payment_id'];
                    Payment::where('transactionId', $transactionId)
                        ->update([
                            'orderReference' => $bookRoomResult['orderReference']
                        ]);

                    $tempInfo->orderReference = $bookRoomResult['orderReference'];
                    $tempInfo->code = $bookRoomResult['code'];
                    $tempInfo->save();
                    // DETECT IF ALIPAY OR WECHATPAY
                    if ($tempInfo->alipayid != '') {
                        $paymentType = 'ALIPAY';
                    } else {
                        $paymentType = 'WECHATPAY';
                    }

                    $langInt = isset($booker['language']) ? $booker['language'] : 0;

                    Order::where('orderReference', $bookRoomResult['orderReference'])
                        ->update([
                            'infact_cost' => $tempInfo->amount,
                            'payment_type' => $paymentType,
                            'payment_status' => 'PAID',
                            'remark' => $tempInfo->remark,
                            'source' => $tempInfo->source,
                            'source_pos' => isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '',
                            'special_request' => $specialRequest,
                            'sales_no' => $tempInfo->sales_no,
                            'user_email' => $tempInfo->user_email,
                            'http_host' => $this->host,
                            'user_language' => $langInt
                        ]);

                    $order = Order::where('orderReference', $bookRoomResult['orderReference'])
                        ->first();

                    if (!empty($order) && $order->provider === 'EBooking') {
                        Log::info('5: missedSalesReserve eb email' . $order->id);
                        $this->sendEbEmail($order);
                    }

                    $he = new hotelControllerEmail();
                    Log::info('----------' . $request->input('tempType') . ': missed order book successfully: ' . $order->id . ' ----------');
                    $he->newOrderEmail($tempInfo, $bookRoomResult);

                    //预定完成，更新infusionsoft contact，以发送feedback邮件
                    $langString = $langInt === 0 ? 'cn' : 'en';
                    $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'hotel.usitrip.com';
                    $this->dispatch(new TriggerFeedback($booker['email'], $langString, $domain));

                    Cache::forget($placingOrderKey); // remove placing order key

                    return response()->json([
                        'success' => true,
                        'errorId' => '',
                        'voucherUrl' => url('voucher/' . $bookRoomResult['code'])
                    ]);
                }
            } else {
                Cache::forget($placingOrderKey); // remove placing order key
                $errorIdForMissedOrder = isset($bookRoomResult['errorId']) ? $bookRoomResult['errorId'] : 'EP1001';
                $errorMessageForMissedOrder = isset($bookRoomResult['errorMessage']) ? $bookRoomResult['errorMessage'] : '预订失败，请记录错误码在备注中！';
                Log::info($sales_no . ' 点击微信继续下单(' . $Reorderid . ')预订失败:' . $errorIdForMissedOrder . ', ' . $errorMessageForMissedOrder);
                return response()->json([
                    'success' => true,
                    'errorId' => $errorIdForMissedOrder,
                    'errorMessage' => $errorMessageForMissedOrder,
                    'voucherUrl' => ''
                ]);
            }
        } else {
            Cache::forget($placingOrderKey); // remove placing order key
            $leftToPay = $nowNeedPay - $originalPaid;
            if ($tempInfo->alipayid != '') {
                $payTypeshort = 'al';
            } else {
                $payTypeshort = 'we';
            }
            $bookerEmail = $tempInfo->user_email;
            $qrLink = "https://hotel.usitrip.com/citconQr/" . $Reorderid . '/' . $payTypeshort . '/' . $leftToPay;
            $finalQrLink = file_get_contents($qrLink);
            Log::info($this->ip . ' get returned url' . $finalQrLink);
            $hotelName = $tempInfo->goods_name;
            $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';
            $booker = json_decode($tempInfo->booker, true);
            $bookerPhone = $booker['phone'];
            $temp_id = $missed_order->id;
            $missed_order->commend = $originalPaid . "是原来付款金额，请同财务确认是否到账" . $leftToPay . '！！！！！' . '&#013;' . $missed_order->commend;
            $missed_order->sales_no = $sales_no;
            $missed_order->amount = $originalPaid;
            $missed_order->save();
            //Email Ann and 106 china sales group to check missedOrderList, aa to aa
            if (strpos($this->host, 'supervacation') !== false) {
                $emailto = array_merge($this->ann106, $this->missedOrderAmericaAsia);
            } else {
                $emailto = array_merge($this->ann106, $this->missedOrderChinaEmails);
            }
            try {
                Mail::send('emails.usitrip_extra_pay_link', [
                    'url' => $finalQrLink,
                    'leftToPay' => $leftToPay,
                    'hotelName' => $hotelName,
                    'sales_no' => $sales_no,
                    'bookerEmail' => $bookerEmail,
                    'bookerPhone' => $bookerPhone,
                    'temp_id' => $temp_id,
                ], function ($message) use ($bookerEmail, $Reorderid, $sales_no, $emailto) {
                    $message->to($emailto)
                        ->subject($sales_no . '的酒店预订需补差价链接，客人邮箱：' . $bookerEmail . ' !');
                });

                return response()->json([
                    'success' => true,
                    'errorId' => 'EMAIL106',
                    'voucherUrl' => ''
                ]);
            } catch (\Exception $e) {
                Log::error($this->ip . 'extra pay for citcon qr has error: ' . $e->getMessage());
                return response()->json([
                    'success' => false,
                ]);
            }
        }
    }

    /**
     * *  for 117book user reserve citcon pay missed orders only for paid but not booked
     * @param Request $request
     * @param $sessionKey
     * @param $Reorderid
     * @return \Illuminate\Http\JsonResponse
     */

    public function missedSalesReserve2B(Request $request, $sessionKey, $Reorderid) //
    {
        Log::info($this->ip . ' for 117book missed order with reorderid:' . $Reorderid . ' start');
        $redisBookRoomKey = $request->input('displayId') . ':' . urlencode($request->Input('bookIndex')) . 1;
        $roomDetails = Cache::get($redisBookRoomKey);
        $roomDetails = json_decode($roomDetails, true);
        $originalPaid = OrderTem::where('Reorderid', $Reorderid)
            ->get()
            ->last()->amount; //已付款的原价
        $originalspecialRequest = OrderTem::where('Reorderid', $Reorderid)
            ->get()
            ->last()->special_request; //原有的特殊请求
        $tempInfo = $this->updateTempOrder($request, $sessionKey, $Reorderid, $roomDetails, 'missedsalesReserve2B');
        // cache placing order key to prevent book two times
        $placingOrderKey = $Reorderid . 'placingOrder';
        if (Cache::has($placingOrderKey) || !empty($tempInfo->orderReference)) {
            return response()->json([
                'success' => false,
                'message' => '有人已经点了预定，请稍后',
                'errorId' => 'EP1002'
            ]);
        }

        Cache::put($placingOrderKey, true, $this->cacheLifeTime);

        $missed_order = OrderTem::where('Reorderid', $Reorderid)
            ->get()
            ->last(); // this is the missed order paid not booked
        $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';
        $missed_order->sales_no = $sales_no;
        $missed_order->save();
        $nowNeedPay = $tempInfo->amount;
        $okToPay = ($nowNeedPay <= $originalPaid) ? true : false;
        if ($okToPay) {
            $user = User::find($missed_order->partnerId);
            $api = new HotelAPI($user);
            $booker = json_decode($tempInfo->booker, true);
            $rooms = json_decode($tempInfo->rooms, true);
            $specialRequest = $originalspecialRequest;
            $bookRoomResult = $api->bookRooms($booker, $rooms, $specialRequest);
            if (isset($bookRoomResult['success']) && $bookRoomResult['success']) {
                $bookRoomResult = $bookRoomResult['data'];
                $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';
                $missed_order->update([
                    'paid_not_booked' => 2,
                    //0 is default, 1 is paid not booked, 2 is solved paid not booked，3 is declined
                    'sales_no' => $sales_no
                ]);
                if ($bookRoomResult === null) {
                    $missed_order->update([
                        'commend' => Carbon::today()
                                ->format('m/d') . ' 预订成功，但是数据存储失败，技术会实时向上游请求数据 (' . $sales_no . ')' . '&#013;' . $missed_order->commend,
                    ]);
                    $this->pricelineBookSuccessDataFailed($Reorderid);
                } else {
                    $transactionId = $missed_order['payment_id'];
                    Payment::where('transactionId', $transactionId)
                        ->update([
                            'orderReference' => $bookRoomResult['orderReference']
                        ]);
                    $tempInfo->orderReference = $bookRoomResult['orderReference'];
                    $tempInfo->code = $bookRoomResult['code'];
                    $tempInfo->save();
                    // DETECT IF ALIPAY OR WECHATPAY
                    if ($tempInfo->alipayid != '') {
                        $paymentType = 'ALIPAY';
                    } else {
                        $paymentType = 'WECHATPAY';
                    }

                    $langInt = isset($booker['language']) ? $booker['language'] : 0;

                    Order::where('orderReference', $bookRoomResult['orderReference'])
                        ->update([
                            'infact_cost' => $tempInfo->amount,
                            'payment_type' => $paymentType,
                            'payment_status' => 'PAID',
                            'remark' => $tempInfo->remark,
                            'source' => $tempInfo->source,
                            'source_pos' => isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '',
                            'special_request' => $specialRequest,
                            'sales_no' => $tempInfo->sales_no,
                            'user_email' => $tempInfo->user_email,
                            'http_host' => $this->host,
                            'user_language' => $langInt
                        ]);

                    $order = Order::where('orderReference', $bookRoomResult['orderReference'])
                        ->first();

                    if (!empty($order) && $order->provider === 'EBooking') {
                        Log::info('6: missedSalesReserve2B 117book eb email sent' . $order->id);
                        $this->sendEbEmail($order);
                    }

                    //这个是b端专用order Email
                    $this->newOrderEmail($tempInfo, $bookRoomResult);

                    //预定完成，更新infusionsoft contact，以发送feedback邮件
                    $langString = $langInt === 0 ? 'cn' : 'en';
                    $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'hotel.usitrip.com';
                    $this->dispatch(new TriggerFeedback($booker['email'], $langString, $domain));

                    Cache::forget($placingOrderKey); // remove placing order key

                    return response()->json([
                        'success' => true,
                        'errorId' => '',
                        'voucherUrl' => url('voucher/' . $bookRoomResult['code'])
                    ]);
                }

            } else {
                Cache::forget($placingOrderKey); // remove placing order key

                return response()->json([
                    'success' => true,
                    'errorId' => isset($bookRoomResult['errorId']) ? $bookRoomResult['errorId'] : 'ER1301', //订购失败
                    'voucherUrl' => ''
                ]);
            }
        } else {
            Cache::forget($placingOrderKey); // remove placing order key
            $leftToPay = $nowNeedPay - $originalPaid;
            if ($tempInfo->alipayid != '') {
                $payTypeshort = 'al';
            } else {
                $payTypeshort = 'we';
            }
            $bookerEmail = $tempInfo->user_email;
            $qrLink = "http://www.117book.com/citconQr/" . $Reorderid . '/' . $payTypeshort . '/' . $leftToPay;
            $finalQrLink = file_get_contents($qrLink);
            Log::info($this->ip . ' get returned url' . $finalQrLink);
            $hotelName = $tempInfo->goods_name;
            $missed_order->commend = $originalPaid . "是原来付款金额，请同财务确认是否到账" . $leftToPay . '！！！！！&#013;' . $missed_order->commend;
            $missed_order->rep = 628;
            $missed_order->amount = $originalPaid;
            $missed_order->save();
            try {
                Mail::send('emails.117book_extra_pay_link', [
                    'url' => $finalQrLink,
                    'leftToPay' => $leftToPay,
                    'hotelName' => $hotelName,
                ], function ($message) use ($bookerEmail, $Reorderid) {
                    $message->to($bookerEmail)
                        ->cc(config('staff-emails.todd'))
                        ->bcc(config('staff-emails.ann'))
                        ->subject('您的酒店预订需补差价，请确认！' . $Reorderid . ' !');
                });

                return response()->json([
                    'success' => true,
                    'errorId' => 'EXTRAPAY',
                    'voucherUrl' => ''
                ]);
            } catch (\Exception $e) {
                Log::error($this->ip . 'extra pay for citcon qr has error: ' . $e->getMessage());
                return response()->json([
                    'success' => false,
                ]);
            }
        }
    }

    /**
     * @param $request
     * @param $sessionKey
     * @param $Reorderid
     * @param $roomDetails
     * @param $operationType
     * @return mixed
     */
    private function updateTempOrder($request, $sessionKey, $Reorderid, $roomDetails, $operationType)
    {
        $tempInfo = OrderTem::where('Reorderid', $Reorderid)
            ->first();

        $specialRequest = $request->input('specialrequest');
        if ($specialRequest != '') {
            $specialRequest = implode("; ", $specialRequest);
        }
        $redisSearchRoomsKey = $request->input('rooms_key');
        $rooms = json_decode(Cache::get($redisSearchRoomsKey, ''), true);

        if (empty($rooms) || !isset($rooms['searchInfo'])) { // for dynamic search and book,we stored searchInfo in searchRooms
            Log::error($this->ip . ' reserve searchInfo did not find');
            return null;
        }

        $searchInfo = $rooms['searchInfo'];

        Log::info($this->ip . ' searchInfo salesReserve ' . json_encode($searchInfo));

        $occupancies = $searchInfo['occupancies'];

        // 生成rooms信息
        $roomIndex = 1;
        $rooms = [];

        foreach ($occupancies as $occ) {
            for ($k = 0; $k < $occ['rooms']; $k++) {
                $k1 = $k + 1;
                $room['roomReference'] = $request->input('roomRef' . $roomIndex);

                $paxes = [];
                for ($i = 1; $i <= $occ['adults']; $i++) {
                    if ($request->input('guest' . $roomIndex . '_a' . $i . '_fn') != "") {
                        $paxes[] = [
                            'type' => 'AD',
                            'name' => $request->input('guest' . $roomIndex . '_a' . $i . '_fn'),
                            'surname' => $request->input('guest' . $roomIndex . '_a' . $i . '_ln'),
                        ];
                    } else {
                        $paxes[] = [
                            'type' => 'AD',
                            'name' => $request->input('guest' . $k1 . '_a' . 1 . '_fn'),
                            'surname' => $request->input('guest' . $k1 . '_a' . 1 . '_ln'),
                        ];

                    }

                }

                for ($i = $occ['adults'] + 1; $i <= $occ['adults'] + $occ['children']; $i++) {

                    if ($request->input('guest' . $roomIndex . '_a' . $i . '_fn') != "") {
                        $paxes[] = [
                            'type' => 'CH',
                            'age' => 6,
                            'name' => $request->input('guest' . $roomIndex . '_a' . $i . '_fn'),
                            'surname' => $request->input('guest' . $roomIndex . '_a' . $i . '_ln'),
                        ];
                    } else {
                        $paxes[] = [
                            'type' => 'CH',
                            'age' => 6,
                            'name' => $request->input('guest' . $k1 . '_a' . 1 . '_fn'),
                            'surname' => $request->input('guest' . $k1 . '_a' . 1 . '_ln'),
                        ];
                    }
                }

                $room['paxes'] = $paxes;
                $room['name'] = $request->input('room' . $i);
                $rooms[] = $room;
                ++$roomIndex;
            }
        }
        $rooms[0]['roomDetails'] = $roomDetails;

        $bookInfo = [
            'currency' => $roomDetails['currency'],
            'amount' => $roomDetails['netPrice'],
        ];
        if ($request->has('totalPrice')) {
            $amount = $request->input('totalPrice');
        }
        $currencyCode = $bookInfo['currency'];
        $amount = $bookInfo['amount'];
        $USD_cost = $amount;
        if ($currencyCode != 'USD' && $currencyCode != '') {
            $USD_cost = Util::httpGet("http://post.usitrip.com/index.php?mod=api_public&action=currencyConversion&money=" . $amount . "&source_code=" . $bookInfo['currency'] . "&out_code=USD");
            $USD_cost = json_decode($USD_cost, true);
            if ($USD_cost['Value'] !== false) {
                $USD_cost = round($USD_cost['Value'], 2);
            } else {
                $USD_cost = 0;
            }
        }

        if ($request->has('coupon_code')) {
            $coupon = json_decode(session('coupon'));
            $hotelIds = explode(',', $coupon->hotelIds);

            if ($coupon && $coupon->code === $request->input('coupon_code') && in_array($tempInfo->display_hotelId,
                    $hotelIds)) {
                $USD_cost = $USD_cost - $coupon->amount;
            }
        }

        if ($currencyCode != '') {
            $cny = $this->changeCurrency($amount, $currencyCode, 'CNY');
        } else {
            $cny = $this->changeCurrency($amount, 'USD', 'CNY');
        }
        $tempInfo->amount = $USD_cost;
        $tempInfo->cny = $cny;

        $remark = $request->input('booker_remark');
        $remark = mb_substr($remark, 0, 50);

        Log::info($this->ip . ' ' . $operationType . ' update temp_info sessionKey: ' . $sessionKey);

        $hotel = (new HotelDB())->getHotel($request->Input('displayId'));

        $tempInfo->bookIndex = $request->Input('bookIndex');
        $tempInfo->hotelId = $request->input('hotelId'); // have to use hotelId from request as the $hotel is get from displayId
        $tempInfo->display_hotelId = $request->input('displayId');
        $tempInfo->rooms = json_encode($rooms);
        $tempInfo->sessionKey = $sessionKey;
        $tempInfo->goods_name = $hotel->name;
        $tempInfo->goods_description = $hotel->description;
        $tempInfo->remark = $remark;
        $tempInfo->special_request = $specialRequest;

        try {
            $tempInfo->save();
        } catch (\Exception $e) {
            Log::error($this->ip . ' update porder_temp error: ' . $e->getMessage());
        }

        $tempInfo = OrderTem::where('Reorderid', $Reorderid)
            ->first();

        return $tempInfo;
    }

    /**
     * new order email from admin， 只给b端用
     * @param $tempInfo
     * @param $result
     */
    private function newOrderEmail($tempInfo, $result)
    {
        //test Email New Order
        $executionStartTime = microtime(true);
        $orderTable = Order::leftjoin('hotel', 'porder.hotelId', '=', 'hotel.hotelId')
            ->leftJoin('destination', 'hotel.desId', '=', 'destination.desId')
            ->leftJoin('porder_pax', 'porder.orderReference', '=', 'porder_pax.orderReference')
            ->leftJoin('porder_room', 'porder.orderReference', '=', 'porder_room.orderReference')
            ->where('code', $result['code'])
            ->first();

        $partner = Auth::user()->name;
        $hotelName = $tempInfo['goods_name'];
        $emailRaw = $result;
        $orderRef = $orderTable->orderPReference;
        $creationTime = $orderTable->created_at;
        $bookingId = $emailRaw['bookingId'];
        $totalPrice = $emailRaw['currencyCode'] . ' ' . $emailRaw['totalPrice'];
        $bookerEmail = $emailRaw['bookerEmail'];
        $bookerName = $emailRaw['bookerFirstName'] . ' ' . $emailRaw['bookerLastName'];
        $roomInfo = $emailRaw['rooms']['0'];
        $boardName = $roomInfo['boardName'];
        $roomType = $roomInfo['name'];
        $cancellationInfo = $roomInfo['cancellationPolicies']['0'];
        $cancellation = $cancellationInfo['end'];
        $cancellation = str_replace("T", " ", mb_substr($cancellation, 0, 19));
        $cancelTime = strtotime($cancellation);
        $orderTime = strtotime($creationTime);
        if (!$cancellation) {
            $cancellation = "Non-Refundable";
        } elseif ($cancelTime < $orderTime) {
            $cancellation = "Non-Refundable";
        } else {
            $cancellation = "Free Cancellation before " . $cancellation;
        }
        $roomNumber = $orderTable->room_number;
        $dest1 = $orderTable->address;
        $dest2 = $result['destination'];
        $dest = str_replace(", ", "", $dest1) . ", " . $dest2;
        $checkin = strtotime($emailRaw['checkInDate']);
        $checkin = date('M d, Y', $checkin);
        $checkout = strtotime($emailRaw['checkOutDate']);
        $checkout = date('M d, Y', $checkout);
        $guestInfo = $roomInfo['paxes']['0'];
        $firstName1 = $guestInfo['firstName'];
        $lastName1 = $guestInfo['lastName'];
        $guest = $firstName1 . ' ' . $lastName1;
        $paxesArray = $roomInfo['paxes'];
        $ad_total = count(array_filter($paxesArray, function ($pax) { return $pax['type'] == 'AD'; }));
        $ch_total = count(array_filter($paxesArray, function ($pax) { return $pax['type'] == 'CH'; }));
        $user_language = $orderTable->user_language;
        if ($user_language == 0) {
            $emailContent = 'emails.117book_newOrder';
            if ($result['status'] == 'CONFIRMED') {
                $status = "已经被确认";
                $emailSubject = '117book要趣订酒店预订确认 - （订单号';
                Log::info($this->ip . ' 发送确认中文邮件');
            } else {
                $status = "正在处理中";
                $emailSubject = '117book要趣订酒店预订正在处理 - （订单号';
                Log::info($this->ip . ' 发送正在处理中文邮件');
            }
        } else {
            $emailContent = 'emails.117book_newOrder_en';
            if ($result['status'] == 'CONFIRMED') {
                $status = " confirmed";
                $emailSubject = '117book Hotel Confirmation - (booking #';
                Log::info($this->ip . ' 发送确认英文邮件');
            } else {
                $status = " in processing";
                $emailSubject = '117book Hotel is in processing - (booking #';
                Log::info($this->ip . ' 发送正在处理英文邮件');
            }
        }

        try {
            if ($result['status'] == 'CONFIRMED' || $result['status'] == 'ON REQUEST') {
                Mail::queue($emailContent, [
                    'partner' => $partner,
                    'status' => $status,
                    'orderId' => $bookingId,
                    'voucher' => url('voucher/' . $emailRaw['code']),
                    'checkin' => $checkin,
                    'checkout' => $checkout,
                    'hotelName' => $hotelName,
                    'reference' => $orderRef,
                    'guest' => $guest,
                    'roomCount' => $roomNumber,
                    'totalPrice' => $totalPrice,
                    'adNumber' => $ad_total,
                    'chNumber' => $ch_total,
                    'roomType' => $roomType,
                    'boardType' => $boardName,
                    'cancellation' => $cancellation,
                    'hotelAddress' => $dest
                ], function ($message) use ($bookingId, $bookerEmail, $bookerName, $emailSubject, $result) {
                    $message->subject($emailSubject . $bookingId . ')')
                        ->to($bookerEmail, $bookerName)
                        ->bcc(config('staff-emails.ann'));
                });
                Log::info($this->ip . ' 发送预定邮件');
            }
        } catch (\Exception $e) {
            Log::error($this->ip . ' send new order email error: ' . $e->getMessage());
        }
        $executionEndTime = microtime(true);
        //The result will be in seconds and milliseconds.
        $seconds = $executionEndTime - $executionStartTime;
        Log::info($this->ip . ' TIME to email new Order for ' . $bookingId . ' is ' . $seconds . ' seconds');
    }

    /**
     * 只要客户填了邮箱，就记录，统一放一个表里面
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function saveEmailInfo(Request $request)
    {
        $emailAddress = $request->email;
        $returnCustomer = CustomerInformation::where('email_address', $emailAddress)
            ->first();
        //detect if the user is new, never order with us before
        $orderBefore = Order::where('bookerEmail', $emailAddress)
            ->first();
        if (empty($orderBefore)) {
            if (empty($returnCustomer)) {
                $data = [
                    'email_address' => $emailAddress,
                    'host' => $_SERVER['HTTP_HOST'],
                    'ip' => $this->ip,
                    'creation_time' => date('Y-m-d H:i:s'),
                ];
                CustomerInformation::insertGetId($data);
            } else {
                $returnCustomer['count'] = $returnCustomer['count'] + 1;
                $returnCustomer->update();
            }
        } else {
            Log::info($this->ip . ' order before: ' . $emailAddress);
        }
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * @param $payId
     * @param $result
     */
    private function updatepbPaymentHistory($payId, $result)
    {
        Payment::where('transactionId', $payId)
            ->update([
                'orderReference' => $result['orderReference']
            ]);
    }

}
