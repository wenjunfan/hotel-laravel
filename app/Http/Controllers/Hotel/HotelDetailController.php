<?php

namespace App\Http\Controllers\Hotel;

use App\Classes\HotelAPI;
use App\Classes\HotelDB;
use App\Traits\HotelDataRetriever;
use App\Traits\IsMobileDevice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\Gate;
use App\Model\Hotel;
use App\Model\User;
use App\Model\OrderTem;
use App\Model\Destination;
use App\Model\SuspiciousOrder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use App\Http\Controllers\Controller;


use  Illuminate\Support\Facades\Auth;
use  Illuminate\Support\Facades\Log;

/**
 * @resource HotelDetail
 * Class HotelDetailController
 * @package App\Http\Controllers\Hotel
 */
class HotelDetailController extends Controller
{
    use HotelDataRetriever, IsMobileDevice;

    private $partnerId;
    private $cacheLifeTime;
    private $expediaEnabled;

    public function __construct()
    {
        parent::__construct();
        if (strpos($this->host, '117book') > -1) {
            $this->partnerId = Auth::check() ? Auth::user()->id : config('app.partner_id');
        }elseif(strpos($this->host, 'supervacation') >-1){
            $this->partnerId = config('app.aa_partner_id');
        }else {
            $this->partnerId = config('app.partner_id');
        }
        $this->cacheLifeTime = config('app.cache_lifetime');

        $expediaProvider = Gate::where([['provider', '=', '["Expedia"]'], ['display', '=', 1]])->first();
        $this->expediaEnabled = empty($expediaProvider) ? false : true;
        unset($expediaProvider);

    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $hotelId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $hotelId)
    {
        // save sales number
        if ($request->has('no')) {
            session(['sales_no' => $request->input('no')]);
        }

        if ($request->has('contact_id')) {
            setcookie('contact_id', $request->input('contact_id'), time() + (10 * 365 * 24 * 3600), "/", preg_replace('/^(hotel|www)/', '', $_SERVER['SERVER_NAME']));
        }

        if ($request->has('lang')) {
            session(['language' => $request->input('lang') == 'en' ? 1 :  $request->input('lang') == 'tw'? 2 :0]);
        }

        $hotelId = str_replace('.html', '', $hotelId);
        $hotelId = str_replace("H", "", $hotelId);

        $db = new HotelDB();
        $hotel = $db->getHotel($hotelId);

//        if ($hotel->display !== 2) {
//            if($hotel->display !== 9){
//                $hotel = Hotel::where('productId', $hotel->productId)->where('display', 2)->first();
//                if(isset($hotel)){
//                    $hotelId =  $hotel->hotelId;
//                }
//            }else{
//                //todo: (需检查是否有问题) 如果不是主酒店变成主酒店搜索
//            }
//        }

        if ($hotel == null) {
            abort(404);
        }

        $hotelCaches = [];
        $hasCachedHotel = 0;
        $childAges = [];

        // get search params todo:是否严格判断关键词大小写
        if ($request->has(['checkin', 'checkout'])) { // if has passed in params
            $checkin = $request->input('checkin');
            $checkout = $request->input('checkout');
            $roomCount = $request->has('roomCount') ? $request->input('roomCount') : 1;
            $adultCount1 = $request->has('adultCount1') ? $request->input('adultCount1') : 2;
            $childCount1 = $request->has('childCount1') ? $request->input('childCount1') : 0;
            $childAges = $request->has('childAge') ? $request->input('childAge') : [];
            $occupancies = $this->getOccupancies($roomCount, $adultCount1, $childCount1, $childAges);
        } else {
            // if didn't pass params, use default params (include marketing urls with gad)
            if ($request->has('source') && $request->input('source') === 'ad') {
                // if it's from ads, try to get lowest price date from redis
                $redisDateKey = 'HotelsDate:' . $hotelId;
                $checkin = Redis::get($redisDateKey);
            } else {
                $checkin = Carbon::parse('next sunday')
                    ->addDays(7)
                    ->toDateString();
            }

            $checkout = Carbon::parse($checkin)
                ->addDays(1)
                ->toDateString();

            $occupancies = $this->getOccupancies(1, 2, 0, []);
        }

        $searchInfo = [
            'checkin' => $checkin,
            'checkout' => $checkout,
            'occupancies' => $occupancies
        ];

        $sessionKey = $request->has('s') ? $request->input('s') : ''; // api sessionKey
        //只有从列表页进来的link含有sessionKey
        if (!empty($sessionKey)) {
            // recommend hotels (from list or hotel page) cache key
            $redisListHotelCachesKey = $sessionKey . '.hotels';
            /*
             * get dynamic hotel data from search cache
             * link from list page and hotel page recommend hotels must pass both sessionKey and search params
             * hotel page new search only pass params
            */
            if ($request->has('checkin') && Cache::has($redisListHotelCachesKey)) {
                $cachedHotels = json_decode(Cache::get($redisListHotelCachesKey), true);
                $hotelCaches[] = $this->findHotel($cachedHotels, $hotelId);
                $hasCachedHotel = 1;
                unset($cachedHotels);
            }
        }

        $checkin = $searchInfo['checkin'];
        $checkout = $searchInfo['checkout'];
        $roomCount = $searchInfo['occupancies'][0]['rooms'];
        $dayCount = $this->calculateDays($checkin, $checkout);

        if ($hasCachedHotel === 1) {
            $hotelDetails[] = $hotel;
            $hotelMerged = $this->getMergedHotels($hotelDetails, $hotelCaches, 20, $roomCount, $dayCount);
            if (count($hotelMerged) > 0) {
                $hotel = $hotelMerged[0];
                unset($hotelDetails, $hotelCaches, $hotels);
            } else {
                $this->prettifyStaticHotel($hotel);
            }
        } else {
            $this->prettifyStaticHotel($hotel);
        }

        $hotel_name = session('language', 0) == 0 && !empty($hotel->name_zh) ? $hotel->name_zh : $hotel->name;

        $images = [];
        $imgUrlArrayOrig = [];
        $facilities = [];
        // cancun 推广酒店 ids
        $countdown_hotel_array = ['1110778', '1209914', '1049429', '1069286', '1076125', '1063364', '1082990', '1060790', '1056212', '1064508', '1090041', '1157838', '1079759', '1055659'];
        $top_property = 0;

        $isMobile = $this->isMobileDevice();
        $blade = 'hotel';
        if ($isMobile) {
            $blade = 'mobile.hotel';
        }

        $api = new HotelAPI(User::find($this->partnerId));
        $extra_data = $api->getExtraHotel(['hotelId' => [$hotelId]]);

        if (!empty($extra_data['hotel_id'])) {
            $top_property = 1;
            //如果酒店没有Expedia rating
            if (isset($extra_data['hotel_images']) && count($extra_data['hotel_images']) > 0) {
                $extra_images = $extra_data['hotel_images'];
                $extra_img_path = config('constants.image_host') . 'Extra_Image/';
                if (isset($extra_images[0]['url'])) {
                    array_walk($extra_images, function (&$value, $key) use ($extra_img_path) {
                        $value['url'] = $extra_img_path . $value['url'];
                    });
                } else {
                    $extra_images = array_map(function ($value) use ($extra_img_path) {
                        $img['url'] = $extra_img_path . $value;
                        return $img;
                    }, $extra_images);
                }

                $hotel->images = $extra_images;
            }
            if (empty($hotel->guestRating) && isset($extra_data['hotel_rating'])) {
                $hotel->guestRating = $extra_data['hotel_rating'];
            }
            if (empty($hotel->guestReviewCount) && isset($extra_data['review_count'])) {
                $hotel->guestReviewCount = $extra_data['review_count'];
            }
            if (isset($extra_data['hotel_description'])) {
                $hotel->description = session('language', 0) === 0 && isset($extra_data['hotel_description_zh']) ? $extra_data['hotel_description_zh'] : $extra_data['hotel_description'];
            }
            if (isset($extra_data['hotel_facility'])) {
                $hotel_facility = session('language', 0) === 0 && isset($extra_data['hotel_facility_zh']) ? $extra_data['hotel_facility_zh'] : $extra_data['hotel_facility'];
                if (isset($hotel_facility['popular']) && count($hotel_facility['popular']) > 0) {
                    $wifi = null;
                    $parking = null;
                    foreach ($hotel_facility['popular'] as $key => $facility) {
                        if (strpos($facility['facilityCode'], 'wifi') !== false) {
                            $wifi = $facility;
                            unset($hotel_facility['popular'][$key]);
                            if ($parking) {
                                break;
                            }
                        }
                        if (strpos($facility['facilityCode'], 'parking') !== false) {
                            $parking = $facility;
                            unset($hotel_facility['popular'][$key]);
                            if ($wifi) {
                                break;
                            }
                        }
                    }
                    if ($wifi) {
                        array_unshift($hotel_facility['popular'], $wifi);
                    }
                    if ($parking) {
                        array_unshift($hotel_facility['popular'], $parking);
                    }

                    $hotel_facility['popular'] = array_values($hotel_facility['popular']);
                }
                if (isset($hotel_facility['all'])) {
                    $hotel_facility['all'] = array_sort($hotel_facility['all'], function ($item) {
                        return count($item['facilities']);
                    });
                }

                $hotel->hotel_facilities = $hotel_facility;
            }
            if (isset($extra_data['room_items'])) {
                $hotel->rooms = array_values($extra_data['room_items']);
            }
            if (isset($extra_data['hotel_area'])) {
                $hotel->area = session('language', 0) === 0 && isset($extra_data['hotel_area_zh']) ? $extra_data['hotel_area_zh'] : $extra_data['hotel_area'];
            }
            if (isset($extra_data['hotel_reviews']) && count($extra_data['hotel_reviews'])) {
                $hotel->reviews = session('language', 0) === 0 && isset($extra_data['hotel_reviews_zh']) ? $extra_data['hotel_reviews_zh'] : $extra_data['hotel_reviews'];
            }
            if (isset($extra_data['hotel_restaurant'])) {
                $hotel->restaurants = session('language', 0) === 0 && isset($extra_data['hotel_restaurant_zh']) ? $extra_data['hotel_restaurant_zh'] : $extra_data['hotel_restaurant'];
            }
            //hotel policies
            if (isset($extra_data['hotel_check_in_time'])) {
                $hotel->checkin_time = session('language', 0) === 0 && isset($extra_data['hotel_check_in_time_zh']) ? $extra_data['hotel_check_in_time_zh'] :$extra_data['hotel_check_in_time'];
            }
            if (isset($extra_data['hotel_check_out_time'])) {
                $hotel->checkout_time = session('language', 0) === 0 && isset($extra_data['hotel_check_out_time_zh']) ? $extra_data['hotel_check_out_time_zh'] :$extra_data['hotel_check_out_time'];
            }
            if (isset($extra_data['hotel_children_extra_bed'])) {
                $hotel->child_policy = session('language', 0) === 0 && isset($extra_data['hotel_children_extra_bed_zh']) ? $extra_data['hotel_children_extra_bed_zh'] : $extra_data['hotel_children_extra_bed'];
            }
            if (isset($extra_data['hotel_pets'])) {
                $hotel->pet_policy = session('language', 0) === 0 && isset($extra_data['hotel_pets_zh']) ? $extra_data['hotel_pets_zh'] : $extra_data['hotel_pets'];
            }
            if (isset($extra_data['hotel_cards_accepted'])) {
                $hotel->cards_accepted = $extra_data['hotel_cards_accepted'];
            }
        } else {
            // build hotel static data
            $images = $db->getHotelImage($hotelId);
            $facilities = $db->getHotelFacility($hotelId);

            $hotel_img_path = config('constants.image_host') . 'Hotel_Image/';
            foreach ($images as $image) {
                // 注意一共有三个同样的 array_push
                array_push($imgUrlArrayOrig, ['url' => $hotel_img_path . $image->url]);
            }
        }

        $data = [
            'optionName' => 'hotel',
            'sessionKey' => $hasCachedHotel ? $sessionKey : '',
            'Reorderid' => '',
            'searchInfo' => $searchInfo,
            'roomCount' => $roomCount,
            'dayCount' => $dayCount,
            'childAges' => $childAges,
            'hotel' => $hotel,
            'hotel_name' => $hotel_name,
            'imgUrlArray' => isset($hotel['images']) ? $hotel['images'] : $imgUrlArrayOrig,
            'facilities' => $facilities,
            'hasCachedHotel' => $hasCachedHotel,
            'roomDetails' => null,
            'countdown_hotel' => $countdown_hotel_array,
            'top_property' => $top_property,
            'seePr' => Auth::check() ? Auth::user()->see_provider : false,
            'auth_pairing' => Auth::check() && in_array(Auth::user()->email, config('constants.hotel_pair_person', [])),// 是否 匹配酒店或更改酒店信息人员
            'adUser' => Auth::check() &&  (Auth::user()->id == config('app.2b_ad_partner_id')),// 是否117book 广告账号
        ];

        unset($hotel, $images, $facilities, $searchInfo, $api);

        return view($blade, $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $hotelId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectToHotel(Request $request, $hotelId)
    {
        $params = $request->all();
        $paramStr = '';
        foreach ($params as $key => $value) {
            $paramStr .= '&' . $key . '=' . $value;
        }
        $paramStr = substr($paramStr, 1);

        $hotelId = urldecode($hotelId);
        if (strpos($hotelId, ':')) {
            $hotelId = str_replace(':', '', $hotelId) . '.html';
        }

        $url = url('/hotel') . '/' . $hotelId;
        if (!empty($paramStr)) {
            $url .= '?' . $paramStr;
        }

        return redirect($url, 301);
    }

    /**
     * @param $displayId
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reorderHotelView($displayId, $id) // hotelId is display_hotelId
    {
        //如果是usitrip销售肯定有a值
        $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';
        // 如果技术账户没有a值
        if (Auth::user()->email === 'api@117book.com' &&  $sales_no === '') {
            setcookie("a", '117',time()+ (31 * 24 * 60 * 60), "/", preg_replace('/^(hotel|www)/', '', $_SERVER['SERVER_NAME']));
            $sales_no = '117';
        }
        //余总
        if (Auth::user()->id === 10655) {
            $sales_no = '19';
        }
        //美亚
        if (Auth::user()->id == config('app.aa_partner_id')) {
            $sales_no = config('app.aa_partner_id');
        }
        setcookie('a', $sales_no ,time()+ (31 * 24 * 60 * 60), "/", preg_replace('/^(hotel|www)/', '', $_SERVER['SERVER_NAME']));
        //如果是usitour且没有a值
        if (strpos($this->host, 'usitour') !== false && $sales_no === '') {
            $data = $_SERVER['REQUEST_URI'];
            $sales_no = substr($data, strpos($data, "=") + 1);
            if (($pos = strpos($data, "=")) !== FALSE) {
                $sales_no = substr($data, $pos+1);
            }
            //针对有cookie的销售或者余总或者技术，设置usitour的a值
            setcookie('a', $sales_no ,time()+ (31 * 24 * 60 * 60), "/", preg_replace('/^(hotel|www)/', '', $_SERVER['SERVER_NAME']));
        }

        $sus_order = SuspiciousOrder::find($id);

        Log::info($this->ip . ' reorderHotel sales number: ' . $sales_no);

        if (empty($sus_order)) { // for older reorderHotel url
            $sus_order = $sus_order = SuspiciousOrder::where('Reorderid', $id)
                ->get()
                ->last();
            $Reorderid = $id;
        } else {
            $Reorderid = $sus_order->Reorderid;
        }

        if (empty($sus_order)) {
            abort('404', '可疑订单不存在');
        }

        $LockOrderSalesKey = 'SuspiciousOrder_' . $Reorderid . '_Sales';
        Log::info('reserve locked sales number ' . (Cache::has($LockOrderSalesKey) ? Cache::get($LockOrderSalesKey) : 'not get'));

        if (Cache::has($LockOrderSalesKey) && !empty($sales_no) && Cache::get($LockOrderSalesKey) != $sales_no || $sus_order->isDone == 1 || $sus_order->isDeclined == 1 || $sus_order->isFraud) {
            $message = Cache::has($LockOrderSalesKey) ? Cache::get($LockOrderSalesKey) . "正在处理该订单" : "有人正在处理订单";
            if ($sus_order->isDone == 1) {
                $message = "该订单已完成";
            } elseif ($sus_order->isDeclined == 1) {
                $message = "该订单已被拒绝";
            } elseif ($sus_order->isFraud == 1) {
                $message = "该订单已被处理为黑名单";
            }
            abort('404', $message);
        }

        if (!empty($sales_no)) {
            Cache::put($LockOrderSalesKey, $sales_no, 120);
        }

        $sessionKey = uniqid(); // todo: remove this as it will be updated with searchHotel on reorderHotel page
        $sus_order->updated_at = date('Y-m-d H:i:s');
        $sus_order->save();

        $searchInfo = json_decode($sus_order->search_info, true);
        $checkin = $searchInfo['checkin'];
        $checkout = $searchInfo['checkout'];
        $roomCount = $searchInfo['occupancies'][0]['rooms'];
        $dayCount = $this->calculateDays($checkin, $checkout);

        $db = new HotelDB();
        $hotel = $db->getHotel($displayId);
        if ($hotel == null) {
            abort(404);
        }

        $images = $db->getHotelImage($displayId);
        $facilities = $db->getHotelFacility($displayId);

        $this->prettifyStaticHotel($hotel);

        $rooms = json_decode($sus_order->rooms, true);
        $roomDetails = isset($rooms[0]['roomDetails']) ? $rooms[0]['roomDetails'] : null;
        if (!isset($roomDetails['cancelDate']) && isset($roomDetails['cancellationPolicies'])) { // in case cancelDate is not saved to roomDetails in checkRooms
            $roomDetails['cancelDate'] = count($roomDetails['cancellationPolicies']) > 0 ? $roomDetails['cancellationPolicies'][0]['end'] : '';
        }
        $childAges = [];
        if (isset($rooms['paxes'])) {
            foreach ($rooms['paxes'] as $pax) {
                if ($pax['type'] = 'CH') {
                    $childAges[] = $pax['age'];
                }
            }
        }

        $imgUrlArrayOrig = array();
        $hotel_img_path = config('constants.image_host') . 'Hotel_Image/';
        foreach ($images as $image) {
            array_push($imgUrlArrayOrig, ['url' => $hotel_img_path . $image->url]);
        }

        $countdown_hotel_array = ['1110778', '1209914', '1049429', '1069286', '1076125', '1063364', '1082990', '1060790', '1056212', '1064508', '1090041', '1157838', '1079759', '1055659'];
        $data = [
            'optionName' => 'hotel',
            'sessionKey' => $sessionKey,
            'Reorderid' => $Reorderid,
            'hasCachedHotel' => 0,
            'isGad' => false,
            'searchInfo' => $searchInfo,
            'roomCount' => $roomCount,
            'dayCount' => $dayCount,
            'childAges' => $childAges,
            'hotel' => $hotel,
            'images' => $images,
            'imgUrlArray' => $imgUrlArrayOrig,
            'facilities' => $facilities,
            'roomDetails' => $roomDetails,
            'countdown_hotel' => $countdown_hotel_array,
            'seePr' => Auth::check() ? Auth::user()->see_provider : false,
            'auth_pairing' => Auth::check() && in_array(Auth::user()->email, config('constants.hotel_pair_person', [])),// 是否 匹配酒店或更改酒店信息人员
            'adUser' => Auth::check() &&  (Auth::user()->id == config('app.2b_ad_partner_id')),// 是否117book 广告账号
        ];

        $isMobile = $this->isMobileDevice();
        if ($isMobile) {
            return view('mobile.hotel', $data);
        }

        return view('hotel', $data);
    }

    /**
     * @param $displayId
     * @param $Reorderid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reorderMissedHotelView($displayId, $Reorderid) // citcon付款未完成重新下单, hotelId is display_hotelId
    {
        //set id for 117book
        if (strpos($this->host, '117book') !== false) {
            setcookie('a', Auth::check() ? Auth::user()->id : '19',time()+ (1 * 24 * 60 * 60), "/", preg_replace('/^(hotel|www)/', '', $_SERVER['SERVER_NAME']));
        }

        $temp_info = OrderTem::where('Reorderid', $Reorderid)
            ->first();
        $passedpartnerId = $temp_info->partnerId;

        $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';

        if (Auth::user()->email === 'api@117book.com' &&  $sales_no === '') {
            setcookie("a", '117',time()+ (31 * 24 * 60 * 60), "/", preg_replace('/^(hotel|www)/', '', $_SERVER['SERVER_NAME']));
            $sales_no = '117';
        }

        $amount = $temp_info->amount;
        $LockOrderSalesKey = '';
        if (!empty($sales_no)) {
            Cache::put($LockOrderSalesKey, $sales_no, 120);
        }
        Log::info('test if locked sales no ' . Cache::has($LockOrderSalesKey) ? Cache::get($LockOrderSalesKey) : 'not get');

        if (Cache::has($LockOrderSalesKey) && !empty($sales_no) && Cache::get($LockOrderSalesKey) != $sales_no || $temp_info->paid_not_booked != 1) {
            $message = Cache::has($LockOrderSalesKey) ? Cache::get($LockOrderSalesKey) . "正在处理该订单" : "有人正在处理订单";
            if ($temp_info->paid_not_booked != 1) {
                $message = "该订单已完成";
            }
            abort('404', $message);
        }

        $sessionKey = uniqid();
        $searchInfo = json_decode($temp_info->search_info, true);
        $checkin = $searchInfo['checkin'];
        $checkout = $searchInfo['checkout'];
        $roomCount = $searchInfo['occupancies'][0]['rooms'];
        $dayCount = $this->calculateDays($checkin, $checkout);
        $childAges = [];
        if (isset($searchInfo['paxes'])) {
            foreach ($searchInfo['paxes'] as $pax) {
                if ($pax['type'] = 'CH') {
                    $childAges[] = $pax['age'];
                }
            }
        }

        $db = new HotelDB();
        $hotel = $db->getHotel($displayId);
        if ($hotel == null) {
            abort(404);
        }

        $images = $db->getHotelImage($displayId);
        $facilities = $db->getHotelFacility($displayId);

        $this->prettifyStaticHotel($hotel);

        $rooms = json_decode($temp_info->rooms, true);
        $roomDetails = isset($rooms[0]['roomDetails']) ? $rooms[0]['roomDetails'] : null;

        $imgUrlArrayOrig = array();
        $hotel_img_path = config('constants.image_host') . 'Hotel_Image/';
        foreach ($images as $image) {
            array_push($imgUrlArrayOrig, ['url' => $hotel_img_path . $image->url]);
        }

        $countdown_hotel_array = ['1110778', '1209914', '1049429', '1069286', '1076125', '1063364', '1082990', '1060790', '1056212', '1064508', '1090041', '1157838', '1079759', '1055659'];

        $data = [
            'optionName' => 'hotel',
            'sessionKey' => $sessionKey,
            'Reorderid' => $Reorderid,
            'hasCachedHotel' => 0,
            'isGad' => false,
            'searchInfo' => $searchInfo,
            'roomCount' => $roomCount,
            'dayCount' => $dayCount,
            'childAges' => $childAges,
            'hotel' => $hotel,
            'images' => $images,
            'imgUrlArray' => $imgUrlArrayOrig,
            'facilities' => $facilities,
            'roomDetails' => $roomDetails,
            'amount' => $amount,
            'passedpartnerId' => $passedpartnerId,
            'countdown_hotel' => $countdown_hotel_array,
            'seePr' => Auth::check() ? Auth::user()->see_provider : false,
            'auth_pairing' => Auth::check() && in_array(Auth::user()->email, config('constants.hotel_pair_person', [])),// 是否 匹配酒店或更改酒店信息人员
            'adUser' => Auth::check() &&  (Auth::user()->id == config('app.2b_ad_partner_id')),// 是否117book 广告账号
        ];
        $extrapayAmount = $roomDetails['netPrice'];
        if ($amount < $extrapayAmount) {
            $message = "如果客人已同意付款，财务未改金额,请财务更改最新到款";
            abort('404', $message);
        }
        $isMobile = $this->isMobileDevice();
        if ($isMobile) {
            return view('mobile.hotel', $data);
        }

        return view('hotel', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchHotel(Request $request)
    {
        if (!$request->has(['hotelId', 'checkin', 'checkout', 'roomCount', 'dayCount', 'adultCount1', 'childCount1'])) {
            Log::error($this->ip . ' hotel page searchHotel missing params, ' . $request->getUri());
            return response()->json([]);
        }
        $hotelId = $request->input("hotelId");
        $checkin = $request->input('checkin');
        $checkout = $request->input('checkout');
        $roomCount = $request->input('roomCount');
        $dayCount = $request->input('dayCount');
        $adultCount = $request->input('adultCount1');
        $childCount = $request->input('childCount1');
        $childAges = $request->input('childAge');
        $occupancies = $this->getOccupancies($roomCount, $adultCount, $childCount, $childAges);

        $hotel = Hotel::where('hotelId', $hotelId)->first();

        if (!empty($hotel)) {
            if ($hotel->display !== 2 && $hotel->display !== 9) {
                $hotel = Hotel::where('productId', $hotel->productId)
                    ->where('display', 2)
                    ->first();
            }

            if (empty($hotel)) {
                return response()->json([
                    'sessionKey' => '',
                    'hotel' => $hotel
                ]);
            }

            if ($request->input('passedPartnerId') != '') {
                $this->partnerId = $request->input('passedPartnerId');
            }

            $api = new HotelAPI(User::find($this->partnerId));
            $hotelIds[] = $hotel->hotelId;

            $hotelCaches = $api->searchHotelsByIds($hotelIds, $checkin, $checkout, $occupancies);
            unset($api);
            Log::info($this->ip . ' searchHotels response: ' . json_encode($hotelCaches, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));

            if (isset($hotelCaches['data']['hotels']) && count($hotelCaches['data']['hotels']) > 0) {
                $sessionKey = $hotelCaches['data']['sessionKey'];
                $hotelCaches = $this->cacheHotels($hotelCaches['data'], $roomCount, $dayCount);
                $hotelDetails[] = $hotel;

                $hotelMerged = $this->getMergedHotels($hotelDetails, $hotelCaches, 20, $roomCount, $dayCount);

                if (count($hotelMerged)) {
                    return response()->json([
                        'sessionKey' => $sessionKey,
                        'hotel' => $hotelMerged[0]
                    ]);
                }
            } else {
                Log::info($this->ip . ' hotel page search hotel api return null');

                return response()->json([
                    'sessionKey' => '',
                    'hotel' => $hotel
                ]);
            }
        }

        Log::info($this->ip . ' hotel page search hotel db return null');

        return response()->json([
            'sessionKey' => '',
            'hotel' => $hotel
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchRooms(Request $request)
    {
        if (!$request->has(['hotelId', 'checkin', 'checkout', 'roomCount', 'dayCount', 'adultCount1', 'childCount1'])) {
            Log::error($this->ip . ' hotel page searchRooms missing params, ' . $request->getUri());
            return response()->json([]);
        }
        $hotelId = $request->input('hotelId');
        $sessionKey = $request->input('sessionKey');
        $passedpartnerId = $request->input('passedPartnerId');
        $roomCount = $request->input('roomCount');
        $dayCount = $request->input('dayCount');
        $adultCount = $request->input('adultCount1');
        $childCount = $request->input('childCount1');
        $checkin = $request->input('checkin');
        $checkout = $request->input('checkout');
        $childAges = $request->input('childAge');
        $occupancies = $this->getOccupancies($roomCount, $adultCount, $childCount, $childAges);
        $searchInfo = [
            'checkin' => $checkin,
            'checkout' => $checkout,
            'occupancies' => $occupancies
        ];

        if ($passedpartnerId != '') {
            $this->partnerId = $passedpartnerId;
        }

        Log::info($this->ip . ' searchRooms searchInfo: ' . json_encode($searchInfo));

        $api = new HotelAPI(User::find($this->partnerId));

        $roomsRes = $api->searchRooms($sessionKey, $hotelId);
        unset($api);

        if (!isset($roomsRes['success']) || !$roomsRes['success']) {
            return response()->json([]);
        }

        $policies = isset($roomsRes['policies']) ? $roomsRes['policies'] : null;

        $rooms = $roomsRes['data'];
        $this->setupRoomFacilities($rooms, $roomCount, $dayCount);

        // group rooms
        $options = [];
        foreach ($occupancies as $occ) {
            $paxes = $occ['adults'] . 'AD' . $occ['children'] . 'CH';
            $options[] = $this->getRoomOptions($rooms, $paxes);
        }

        $searchRoomsData = [
            'rooms' => $options,
            'searchInfo' => $searchInfo
        ];

        $redisSearchRoomsKey = uniqid();
        Cache::put($redisSearchRoomsKey, json_encode($searchRoomsData), $this->cacheLifeTime);

        return response()->json([
            'success' => true,
            'rooms_key' => $redisSearchRoomsKey,
            'rooms' => $options,
            'policies' => $policies,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recommendHotels(Request $request)
    {
        if (!$request->has(['hotelId', 'checkin', 'checkout', 'roomCount', 'dayCount', 'adultCount1', 'childCount1'])) {
            Log::error($this->ip . ' hotel page search recommend hotels missing params, ' . $request->getUri());
            return response()->json([]);
        }
        $hotelId = $request->input('hotelId');
        $sessionKey = $request->input('sessionKey');
        $detailedRecHotelsKey = $sessionKey . '.detailedRecHotels';
        //if already stored detailed recommend hotels, return
        if (Cache::has($detailedRecHotelsKey)) {
            $hotels = json_decode(Cache::get($detailedRecHotelsKey), true);
            // remove current open hotel from caches
            foreach ($hotels as $key => $hotel) {
                if ($hotel['hotelId'] == $hotelId) {
                    array_splice($hotels, $key, 1);
                    break;
                }
            }

            return response()->json([
                'hotels' => $hotels,
                'sessionKey' => $sessionKey
            ]);
        }

        $passedpartnerId = $request->input('passedPartnerId');
        $roomCount = $request->input('roomCount');
        $dayCount = $request->input('dayCount');
        $adultCount = $request->input('adultCount1');
        $childCount = $request->input('childCount1');
        $checkin = $request->input('checkin');
        $checkout = $request->input('checkout');
        $childAges = $request->input('childAge');
        $occupancies = $this->getOccupancies($roomCount, $adultCount, $childCount, $childAges);
        $destinationId = $request->input('destinationId');

        $hotels = [];
        $hotelCaches = [];

        // stored recommend hotels
        $redisRecHotelCachesKey = $sessionKey . '.hotels';
        if (Cache::has($redisRecHotelCachesKey)) {
            $hotelCaches = json_decode(Cache::get($redisRecHotelCachesKey), true);
        }

        // search for new recommend hotels if no cache exist
        if ($destinationId && count($hotelCaches) === 0) {
            if ($passedpartnerId != '') {
                $this->partnerId = $passedpartnerId;
            }

            $api = new HotelAPI(User::find($this->partnerId));

            Log::info($this->ip . ' hotel page search recommend hotels');
            $hotelCaches = $api->searchHotels($destinationId, [], $checkin, $checkout, $occupancies);
            unset($api);

            if (isset($hotelCaches['errorId'])) {
                return response()->json([]);
            } else {
                if (isset($hotelCaches['data']) && isset($hotelCaches['data']['hotels'])) {
                    $sessionKey = $hotelCaches['data']['sessionKey'];
                    $hotelCaches = $this->cacheHotels($hotelCaches['data'], $roomCount, $dayCount);
                } else {
                    $hotelCaches = [];
                }
            }
        }

        if (count($hotelCaches) > 0) {
            $filters = [
                'orderBy' => 'discount',
                'sort' => 'asc',
                'currentPage' => 1,
                'showAllDiscount' => false,
                'key' => '',
                'rating' => [0, 1, 2, 3, 4, 5],
                'price' => ['0', '1000000', '0', '1000000', $checkin, $checkout],
                'destinationId' => $destinationId,
                'roomCount' => $roomCount,
                'dayCount' => $dayCount,
                'pageHotels' => 5
            ];

            $db = new HotelDB();

            $orderBy = $filters['orderBy'];
            $sort = $filters['sort'];
            $currentPage = $filters['currentPage'];
            $showAllDiscount = $filters['showAllDiscount'];
            $key = $filters['key'];
            $rating = $filters['rating'];
            $price = $filters['price'];
            $destinationId = $filters['destinationId'];
            $pageHotels = $filters['pageHotels'];

            $hotelDetails = $db->filterHotels($hotelCaches, $currentPage, $pageHotels, $orderBy, $sort, $rating, $price, $key, $showAllDiscount, [], $destinationId, []);
            $hotels = $this->getMergedHotels($hotelDetails[1], $hotelCaches, $pageHotels, $filters['roomCount'], $filters['dayCount']);
            unset($hotelDetails);

            // remove current open hotel from caches
            foreach ($hotels as $key => $hotel) {
                if ($hotel->hotelId == $hotelId) {
                    array_splice($hotels, $key, 1);
                    break;
                }
            }
            //cache 5 hotels in case current hotel is in the array
            Cache::put($detailedRecHotelsKey, json_encode($hotels), $this->cacheLifeTime);
            //return 4 rec hotels
            $hotels = array_slice($hotels, 0, 4);
        }

        return response()->json([
            'hotels' => $hotels,
            'sessionKey' => $sessionKey
        ]);
    }

    /**
     * @param $rooms
     * @param $roomCount
     * @param $dayCount
     */
    private function setupRoomFacilities(&$rooms, $roomCount, $dayCount)
    {
        $roomIds = [];

        foreach ($rooms as $room) {
            $roomIds[] = $room['roomId'];
        }

        $db = new HotelDB();
        $facilities = $db->getRoomsFacilities($roomIds);

        foreach ($rooms as $key => &$room) {
            if (strpos($room['roomReference'], 'e:') === false || !config('app.hide_expedia_rooms')) {
                $room['facilities'] = $this->getRoomFacilities($room['roomId'], $facilities);

                $taxRate = config('constants.tax_rate');
                $beforeTax = floor(($room['netPrice'] * (1 - $taxRate)) / $roomCount / $dayCount); //单价
                //按May要求，隐藏单价超过$2500的房型
                if ($beforeTax > 2500) {
                    unset($rooms[$key]);
                    continue;
                }
                $tax = $room['netPrice'] - $beforeTax;

                $room['beforeTax'] = $beforeTax;
                $room['tax'] = $tax;
            } else {
                unset($rooms[$key]);
            }
        }
    }

    /**
     * @param $hotelDetails
     * @param $hotelCaches
     * @param $pageHotels
     * @param $roomCount
     * @param $dayCount
     * @return array
     */
    private function getMergedHotels($hotelDetails, $hotelCaches, $pageHotels, $roomCount, $dayCount)
    {
        $hotelIndex = 0;
        $finalHotels = [];

        // static data prettify
        if (count($hotelDetails) > 0) {
            $destination = Destination::where("desId", $hotelDetails[0]['desId'])->first();
            $country = isset($destination->country) ? $destination->country : null;
        } else {
            $destination = null;
            $country = null;
        }

        try {
            foreach ($hotelDetails as $key => $hotel) {
                $hotel = (object)$hotel;
                $hotelCache = $this->findHotel($hotelCaches, $hotel->hotelId);
                if ($hotelCache == null) {
                    continue;
                }

                $this->prettifyStaticHotel($hotel, $destination, $country); // prettify static data

                // calculate tax and before tax
                $taxRate = config('constants.tax_rate');
                $beforeTax = floor(($hotelCache['minRate'] * (1 - $taxRate)) / $roomCount / $dayCount) * $roomCount * $dayCount; //总价
                $tax = $hotelCache['minRate'] - $beforeTax;

                // decide which discount to use
                $discount = isset($hotelCache['discount']) ? $hotelCache['discount'] : 0;
                if (isset($hotelCache['dynamicDiscount']) && $hotelCache['dynamicDiscount'] > $discount) {
                    $discount = $hotelCache['dynamicDiscount'];
                }

                $originalPrice = $hotelCache['minRate'];
                if ($discount > 0) {
                    $originalPrice = floor(($beforeTax / (1 - $discount)) / $roomCount / $dayCount) * $roomCount * $dayCount; //总价
                }

                $hotel->tax = $tax;
                $hotel->currency = $hotelCache['currency'];
                $hotel->discount = $discount;
                $hotel->minPrice = $beforeTax > 0 ? floor($beforeTax * 100 / $roomCount / $dayCount) / 100 : 0; //税前价格
                $hotel->originalPrice = $originalPrice > 0 ? floor($originalPrice * 100 / $roomCount / $dayCount) / 100 : 0; //税后价格
                $hotel->lowestFlg = $hotelCache['lowestFlg'];
                $hotel->distance = $hotelCache['distance'];
                $hotel->guestRating = $hotelCache['guestRating'];
                $hotel->guestReviewCount = $hotelCache['guestReviewCount'];

                $finalHotels[] = $hotel;

                unset($hotel, $hotelCache);

                if (++$hotelIndex == $pageHotels) {
                    break;
                }
            }
        } catch (\Exception $exception) {
            Log::error("Merge dynamic and static hotel data failed, error: " . $exception->getMessage());
        }

        return $finalHotels;
    }

    /**
     * @param $rooms
     * @param $paxes
     * @return array
     */
    private function getRoomOptions($rooms, $paxes)
    {
        $options = [];
        foreach ($rooms as $room) {
            if ($room['paxes'] == $paxes) {
                // 转房间名称为首字母大写
                $room['name'] = ucwords(strtolower($room['name']));
                $options[] = $room;
            }
        }

        return $options;
    }

    /**
     * @param $roomId
     * @param $facilities
     * @return array
     */
    private function getRoomFacilities($roomId, $facilities)
    {
        $r = [];
        foreach ($facilities as $facility) {
            if ($facility->hotelRoomId == $roomId) {
                $r[] = $facility;
                continue;
            }

            if (count($r) > 0) {
                break;
            }
        }

        return $r;
    }

    /**
     * @param $hotel
     */
    private function prettifyStaticHotel(&$hotel, $destination = null, $country = null) // new function for prettify static hotel data(for single hotel)
    {
        // static data prettify
        if (!$destination) {
            $destination = Destination::where("desId", $hotel->desId)->first();
        }

        if (!$country && isset($destination->country)) {
            $country = $destination->country;
        }

        // setup hotel address
        $address = trim(str_replace(",", " ", $hotel->address));
        $desName = is_null($destination) ? '' : substr($destination->desName, 0, strrpos($destination->desName, ","));
        $hotel->address = ucwords(strtolower($address)) . ', ' . $desName . ', ' . $hotel->zipcode;
        $hotel->name = ucwords($hotel->name);
        $hotel->thumbnail = config('constants.image_host') . (stripos($hotel->thumbnail, '_Image') === false ? 'Hotel_Image/' : '') . $hotel->thumbnail;
        $hotel->destinationName = isset($destination->desName) ? $destination->desName : '';
        $hotel->destinationName_zh = isset($destination->desName_zh) ? $destination->desName_zh : '';
        $hotel->countryName = isset($country->countryName) ? $country->countryName : '';
        $hotel->countryName_zh = isset($country->countryName_zh) ? $country->countryName_zh : '';
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array|mixed
     */
    public function getBookingRooms(Request $request)
    {
        if ($request->has('hotelId')) {
            $params = [
                'hotelId' => (int)$request->input('hotelId'),
                'source' => 'booking'
            ];

            $api = new HotelAPI(User::find($this->partnerId));
            $rooms = $api->getBookingRooms($params);
            unset($api);

            return $rooms;
        }

        return [];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array|mixed
     */
    public function submitPairedRoom (Request $request)
    {
        if ($request->has('hotelId', 'roomId', 'bookingId')) {
            $params = [
                'hotelId' => (int)$request->input('hotelId'),
                'roomId' => (int)$request->input('roomId'),
                'bookingRoomId' => (int)$request->input('bookingId')
            ];

            $api = new HotelAPI(User::find($this->partnerId));
            $response = $api->pairBookingRooms($params);
            unset($api);

            return $response;
        }

        return null;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array|mixed
     */
    public function cancelPairedRoom (Request $request)
    {
        if ($request->has('roomId')) {
            $params = [
                'roomId' => (int)$request->input('roomId')
            ];

            $api = new HotelAPI(User::find($this->partnerId));
            $response = $api->reverseBookingRooms($params);
            unset($api);

            return $response;
        }

        return null;
    }

    /**
     * WIKI 更新酒店信息
     * @param \Illuminate\Http\Request $request
     * @return bool|string|null
     */
    public function submitHotelWiki(Request $request)
    {
        $api = new HotelAPI(User::find($this->partnerId));
        $data = $request->all();
        // no laravel token needed
        if(isset($data['_token'])){
            unset($data['_token']);
        }

        //{"success":"true","data":null,"errorMessage":null,"errorType":null,"errorMessageText":null,"dateStamp":"2019-03-29 13-24-36"}
        $respons = json_decode($api->postHotelWiki($data));
        unset($api);

        $msg = (isset($respons->success ) && $respons->success === "true") ? 'true' : 'false';

        return redirect()->back()->with('msg', $msg );
    }

    /**
     * @param $hotelCaches
     * @param $roomCount
     * @param $dayCount
     * @return array
     */
    private function cacheHotels($hotelCaches, $roomCount, $dayCount) // cache hotels for filter and open hotel detail page
    {
        $hotels = $hotelCaches['hotels'];
        $hotelsCache = [];

        try {
            foreach ($hotels as $key => $hotel) {
                $discount = isset($hotel['discount']) ? $hotel['discount'] : 0;
                if (isset($hotel['dynamicDiscount']) && $hotel['dynamicDiscount'] > $discount) {
                    $discount = $hotel['dynamicDiscount'];
                }

                $taxRate = config('constants.tax_rate');

                $beforeTax = $hotel['minRate'] === -1 ? $hotel['minRate'] : floor(($hotel['minRate'] * (1 - $taxRate)) / $roomCount / $dayCount) * $roomCount * $dayCount;

                $hotelsCache[] = [
                    'id' => $hotel['id'],
                    'currency' => $hotel['currency'],
                    'minRate' => $hotel['minRate'],
                    'beforeTax' => $beforeTax,
                    'discount' => $discount,
                    'lowestFlg' => isset($hotel['lowestFlg']) && $this->expediaEnabled ? $hotel['lowestFlg'] : 0,
                    'distance' => isset($hotel['distance']) && $this->expediaEnabled ? $hotel['distance'] : null,
                    'guestReviewCount' => isset($hotel['guestReviewCount']) && $this->expediaEnabled ? $hotel['guestReviewCount'] : 0,
                    'guestRating' => isset($hotel['guestRating']) && $this->expediaEnabled ? $hotel['guestRating'] : '',
                ];
            }

            $redisHotelCachesKey = $hotelCaches['sessionKey'] . '.hotels'; // api sessionKey
            Cache::put($redisHotelCachesKey, json_encode($hotelsCache), $this->cacheLifeTime);
        } catch (\Exception $e) {
            Log::error('Cache dynamic hotel data failed, error: ' . $e->getMessage());
        }

        return $hotelsCache;
    }

}
