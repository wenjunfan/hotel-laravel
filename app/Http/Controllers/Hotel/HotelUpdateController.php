<?php

namespace App\Http\Controllers\Hotel;

use App\Classes\HotelDB;
use App\Model\HotelImage;
use App\Model\Partner;
use App\Traits\UploadFile;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Hotel;
use App\Classes\HotelAPI;
use App\Model\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HotelUpdateController extends Controller
{
    use UploadFile;
    
    private $partnerId;
    private $cacheLifeTime;

    public function __construct()
    {
        parent::__construct();
        if (strpos($this->host, '117book') > -1) {
            $this->partnerId = Auth::check() ? Auth::user()->id : config('app.partner_id');
        } else {
            $this->partnerId = config('app.partner_id');
        }
        $this->cacheLifeTime = config('app.cache_lifetime');
    }

    public function index($hotelId)
    {
        $hotel = null;
        $is_new = 0;

        if (trim($hotelId)) {
            $hotel = Hotel::find($hotelId);
            
            $api = new HotelAPI(User::find($this->partnerId));
            $extra_data = $api->getExtraHotel(['hotelId' => [$hotelId]]);

            if (!empty($extra_data['hotel_id'])) {
                if (isset($extra_data['hotel_reviews'])) {
                    $hotel_reviews = collect($extra_data['hotel_reviews'])->sortBy(function($review) {
                        return strlen($review['content']);
                    });
                    $hotel['hotel_reviews'] = array_values($hotel_reviews->toArray());
                }
                if (isset($extra_data['hotel_reviews_zh'])) {
                    $hotel_reviews_zh = collect($extra_data['hotel_reviews_zh'])->sortBy(function($review) {
                        return strlen($review['content']);
                    });
                    $hotel['hotel_reviews_zh'] = array_values($hotel_reviews_zh->toArray());
                }
                if (isset($extra_data['hotel_area'])) {
                    $hotel_area = collect($extra_data['hotel_area'])->sortBy(function($area) {
                        return count($area['pois']);
                    });
                    $hotel['hotel_area'] = array_values($hotel_area->toArray());
                }
                if (isset($extra_data['hotel_area_zh'])) {
                    $hotel_area_zh = collect($extra_data['hotel_area_zh'])->sortBy(function($area) {
                        return count($area['pois']);
                    });
                    $hotel['hotel_area_zh'] = array_values($hotel_area_zh->toArray());
                }
                
                foreach($extra_data as $key=>$val) {
                    if (!isset($hotel[$key])) {
                        $hotel[$key] = $val;
                    }
                }

                //由于api返回的hotel_facility下的all是以facility名字为key的object，这里需要清理成indexed array，便于前端操作
                if (isset($hotel['hotel_facility']) && !empty($hotel['hotel_facility']['all'])) {
                    $hotel_facility = $hotel['hotel_facility'];
                    uasort($hotel_facility['all'], function($a, $b) {
                        return count($a['facilities']) - count($b['facilities']);
                    });
                    $hotel_facility_all = $hotel_facility['all'];
                    array_walk($hotel_facility_all, function (&$item, $key) {
                         $item['code'] = $key;
                    });
                    $hotel_facility['all'] = array_values($hotel_facility_all);
                    $hotel['hotel_facility'] = $hotel_facility;
                }
                if (isset($hotel['hotel_facility_zh']) && !empty($hotel['hotel_facility_zh']['all'])) {
                    $hotel_facility_zh = $hotel['hotel_facility_zh'];
                    uasort($hotel_facility_zh['all'], function($a, $b) {
                        return count($a['facilities']) - count($b['facilities']);
                    });
                    $hotel_facility_all_zh = $hotel_facility_zh['all'];
                    array_walk($hotel_facility_all_zh, function (&$item, $key) {
                        $item['code'] = $key;
                    });
                    $hotel_facility_zh['all'] = array_values($hotel_facility_all_zh);
                    $hotel['hotel_facility_zh'] = $hotel_facility_zh;
                }
            } else {
                $is_new = 1;
                $db = new HotelDB();
                $hotel['hotel_images'] = $db->getHotelImage($hotelId);
            }
            $hotel['thumbnail'] = config('constants.image_host') . (stripos($hotel['thumbnail'], '_Image') === false ? 'Hotel_Image/' : '') . $hotel['thumbnail'];
        }

        return view('hotel-update', compact('hotelId', 'hotel', 'is_new'));
    }

    public function updateHotelInfo(Request $request)
    {
        $data = $request->input('data');
        $action = $request->input('action');
        if (trim($action)) {
            $hotelApi = new HotelAPI(Partner::find($this->partnerId));
            $hotel = Hotel::find($data['hotelId'])->first();

            if ($action === 'detail') {
                $result = $hotelApi->updateHotelInfo($data, 'updateHotelsInfo');
            } elseif ($action === 'area') {
                $result = $hotelApi->updateHotelInfo($data, 'updateHotelPoi');
            } elseif ($action === 'facility') {
                $data = $this->fixFacilityData($data);
                $result = $hotelApi->updateHotelInfo($data, 'updateHotelFacilities');
            } elseif ($action === 'restaurant') {
                $result = $hotelApi->updateHotelInfo($data, 'updateHotelRestaurant');
            } elseif ($action === 'thumbnail') {
                $hotel->thumbnail = $data['thumbnail'];
                try {
                    $hotel->save();
                    $result = [
                        'success' => true
                    ];
                } catch (\Exception $e) {
                    Log::error('update hotel thumbnail failed, ' . $e->getMessage());
                    $result = [
                        'success' => false
                    ];
                }
            }

            if(isset($hotel->productId)) {
                //cleanup redis cache for extra data
                $productId = $hotel->productId;
                $this->updateRedisCache($productId);
            }
        }

        if ($result['success']) {
            return response()->json([
                'success' => true
            ]);
        }

        return response()->json([
            'success' => false
        ]);
    }

    private function fixFacilityData($data)
    {
        if (isset($data['hotel_facility']) && isset($data['hotel_facility']['all'])) {
            $all = $data['hotel_facility']['all'];
            $all_new = [];
            foreach ($all as $item) {
                $code = strtolower(preg_replace('/\s+/', '_', $item['code']));;
                $all_new[$code] = [
                    'sortName' => $item['sortName'],
                    'facilities' => isset($item['facilities']) ? $item['facilities'] : []
                ];
            }
            $data['hotel_facility']['all'] = $all_new;
        }
        if (isset($data['hotel_facility_zh']) && isset($data['hotel_facility_zh']['all'])) {
            $all_zh = $data['hotel_facility_zh']['all'];
            $all_new_zh = [];
            foreach ($all_zh as $item) {
                $code = strtolower(preg_replace('/\s+/', '_', $item['code']));
                $all_new_zh[$code] = [
                    'sortName' => $item['sortName'],
                    'facilities' => isset($item['facilities']) ? $item['facilities'] : []
                ];
            }
            $data['hotel_facility_zh']['all'] = $all_new_zh;
        }

        return $data;
    }

    public function updateHotelReviews(Request $request)
    {
        $action = $request->input('action');
        $review = $request->input('review');
        $hotelId = $request->input('hotelId');
        $hotelApi = new HotelAPI(Partner::find($this->partnerId));

        if ($action === 'add') {
            $data = [
                'hotelId' => [$hotelId],
                'reviews' => [$review]
            ];

            $result = $hotelApi->updateHotelInfo($data, 'addHotelReview');
        } elseif ($action === 'update') {
            $data = [
                'hotelId' => [$hotelId],
                'id' => $review['id'],
                'review' => $review
            ];

            $result = $hotelApi->updateHotelInfo($data, 'updateHotelReview');
        } elseif ($action === 'delete') {
            $data = [
                'hotelId' => [$hotelId],
                'id' => $review['id']
            ];

            $result = $hotelApi->updateHotelInfo($data, 'deleteHotelReview');
        }

        if ($result['success']) {
            return response()->json([
                'success' => true
            ]);
        }

        return response()->json([
            'success' => false
        ]);
    }

    public function updateHotelImage(Request $request)
    {
        $data = $request->all();
        $method = $data['method'];

        if (isset($data['hotelId']) && isset($data['productId'])) {
            $hotelId = $data['hotelId'];
            $productId = $data['productId'];
            $is_new = isset($data['is_new']) ? $data['is_new'] : false;
            $image_folder = $is_new == 1 ? 'Hotel_Image/' : 'Extra_Image/';

            if ($method === 'delete') {
                $images = $data['images'];
                if ($is_new == 1) {
                   DB::table('hotel_image')
                       ->where('hotelId', $hotelId)
                       ->whereIn('url', $images)
                       ->delete();
                } else {
                    DB::table('extra_image')
                        ->where('hotel_id', $productId)
                        ->whereIn('img_url', $images)
                        ->delete();

                    $this->updateRedisCache($productId);
                }

                return response()->json([
                    'success' => true
                ]);
            } elseif (isset($data['file']) && isset($data['hotelId'])) {
                if (count($data['file']) === 1 && !empty($data['replaceImg'])) {
                    $file = $data['file'][0];
                    $file_type = $file->getMimeType();
                    if ($file->getSize() > 2000000) {
                        return response()->json([
                            'success' => false,
                            'message' => '图片太大'
                        ]);
                    } elseif (!(strrpos($file_type, 'image') !== false || $file_type === 'application/pdf')) {
                        return response()->json([
                            'success' => false,
                            'message' => '图片类型不支持'
                        ]);
                    }

                    $name = $file->getClientOriginalName();
                    $name = $image_folder . $productId . '/' . str_replace(' ', '-', $name);
                    $url = $this->uploadFile($file, $name);
                    if ($url && isset($data['replaceImg'])) {
                        $replaceImg = $data['replaceImg'];
                        $newImg = str_replace($image_folder, '', $name);
                        if ($is_new == 1) {
                            HotelImage::where('url', $replaceImg)
                                ->update(['url' => $newImg]);
                        } else {
                            DB::table('extra_image')
                                ->where('img_url', $replaceImg)
                                ->update(['img_url' => $newImg]);

                            $this->updateRedisCache($productId);
                        }
                    }

                    return response()->json([
                        'success' => true
                    ]);
                } else {
                    $files = $data['file'];
                    $newImages = [];
                    foreach ($files as $file) {
                        $file_type = $file->getMimeType();
                        $name = $file->getClientOriginalName();
                        if ($file->getSize() > 2000000) {
                            return response()->json([
                                'success' => false,
                                'message' => '图片太大: ' . $name
                            ]);
                        } elseif (!(strrpos($file_type, 'image') !== false || $file_type === 'application/pdf')) {
                            return response()->json([
                                'success' => false,
                                'message' => '图片类型不支持: ' . $name
                            ]);
                        } elseif (strlen($name) >= (79 - strlen((string) $hotelId))) {
                            return response()->json([
                                'success' => false,
                                'message' => '文件名过长: ' . $name
                            ]);
                        }

                        $name = $image_folder . $productId . '/' . str_replace(' ', '-', $name);
                        $url = $this->uploadFile($file, $name);
                        if ($url) {
                            $newImg = str_replace($image_folder, '', $name);
                            if ($is_new == 1) {
                                $newImages[] = [
                                    'hotelId' => $hotelId,
                                    'type' => 'large',
                                    'url' => $newImg
                                ];
                            } else {
                                $newImages[] = [
                                    'hotel_id' => $productId,
                                    'img_url' => $newImg
                                ];
                            }
                        }
                    }
                    if (count($newImages) > 0) {
                        if ($is_new == 1) {
                            DB::table('hotel_image')
                                ->insert($newImages);
                        } else {
                            DB::table('extra_image')
                                ->insert($newImages);

                            $this->updateRedisCache($productId);
                        }
                    }

                    return response()->json([
                        'success' => true
                    ]);
                }
            }
        }

        return response()->json([
            'success' => false,
            'message' => '酒店id参数缺失'
        ]);
    }

    private function updateRedisCache($productId)
    {
        \Redis::del('ExtraHotelData:' . $productId);
    }

}
