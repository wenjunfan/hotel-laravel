<?php

namespace App\Http\Controllers\Hotel;

use App\Model\Board;
use App\Model\Order;
use App\Model\EbRoom;

use App\Http\Controllers\Controller;
use App\Traits\IsMobileDevice;
use Carbon\Carbon;

use Illuminate\Support\Facades\Log;

/**
 * Class VoucherController
 * @package App\Http\Controllers\Hotel
 */
class VoucherController extends Controller
{
    use IsMobileDevice;

    /**
     * @param $code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($code)
    {
        $order = Order::where('code', $code)
                       ->first();

        if (empty($order)) {
            Log::error($this->ip . ' open voucher failed to get order data, redirect to homepage');
            return redirect('/');
        }

        Log::info($this->ip . ' open voucher ' . $order->id . ', url: ' . request()->url());

        $hotel = $order->hotel;
        if (empty($hotel)) {
            abort(404);
        }
        $hotelBackup = $order->realHotel;
        $rooms = $order->rooms;
        $orderTem = $order->orderTem;

        $order->desName = empty($hotel->desName) ? '' : $hotel->desName;
        $order->desName_zh = empty($hotel->desName_zh) ? '' : $hotel->desName_zh;
        $order->city = empty($hotel->city) ? '' : ', ' .ucwords(strtolower($hotel->city));
        $order->zipcode = empty($hotel->zipcode) ? '' : ', ' .$hotel->zipcode;
        $order->addr = ucwords(strtolower($hotel->address)) .  $order->city.  $order->zipcode;
        $order->rating = $hotel->rating;
        $order->hotelPhone = $hotel->phone == '' ? $hotelBackup->phone : $hotel->phone;
        $order->latitude = $hotel->latitude;
        $order->longitude = $hotel->longitude;
        $order->numDays = (strtotime($order->checkoutDate) - strtotime($order->checkinDate)) / 86400;
        $order->hotelNameZh = $hotel->name_zh;

        // get order room info and paxes
        $ebRemark = '';
        $guestNo = 0;
        foreach ($rooms as &$room) {
            $room->paxes = $room->paxes->toArray();
            $children = 0;
            $adult = 0;

            foreach ($room->paxes as $pax) {
                if ($pax['type'] == 'AD' || $pax['type'] == 'M' || $pax['type'] == 'F') {
                    $adult++;
                } elseif ($pax['type'] == 'CH' || $pax['type'] == 'C') {
                    $children++;
                }
            }

            $room->adult = $adult;
            $room->children = $children;

            if (isset($room->roomId)) {
                $roomId = $room->roomId;
                $isEbooking = $order->provider === 'EBooking' ?  true: false;
                if($isEbooking){
                //ebooking的被酒店reject后显示给客人还是confirmed然后alex自己去联系客人另一个订单，alex收到reject邮件，财务看到eb reject
                    $ebRoom = EbRoom::where('hotelId',$order->hotelId)->Where('room_type_id',$roomId)->first();
                    if(!empty($ebRoom) && isset($ebRoom->remark)){
                        $ebRemark = $ebRoom->remark;
                    }
                }
            }

            $formatBoard = Board::where('boardName', $room->boardName)
                                ->first();
            if ($formatBoard != null) {
                $room->formatBoard = $formatBoard->boardName_zh;
            }

            // format the cancellation policy
            $room->cancelPolicies = json_decode($room->cancelPolicies, true);
            $guestNo = $adult + $children + $guestNo;

            $mandatory_fees = isset($orderTem->mandatory_fee)? $orderTem->mandatory_fee : '' ;
            $coupon_code = isset($orderTem->coupon_code) && $orderTem->coupon_code !== '' ? $orderTem->coupon_code : '' ;
            if($coupon_code !== ''){
                $coupon_amount = $orderTem->amount;
                // pass coupon amount and code to voucher - 10252018
                $order->coupon_amount = $coupon_amount;
                $order->coupon_code = $coupon_code;
            }
            // pass mandatory fee to voucher - 02212018
            $order->mandatory_fees = $mandatory_fees;
        }
        $order->guestNo = $guestNo;

        $result = $order->toArray();
        $result['rooms'] = $rooms->toArray();

        foreach ($result['rooms'] as &$room) {
            $room['comments'] = str_replace("<br />", " ", $room['comments']);
            $room['comments'] = str_replace("鈥�", "-", $room['comments']);
        }
        if($ebRemark != ''){
            $room['comments'] = $ebRemark;
        }

        $voucher = $result;

        if ($voucher == null) {
            abort('404', '获取电子票失败');
//            $this->error('获取电子票失败'); // todo: where is error function
        }

        foreach ($voucher['rooms'] as &$room) {
            $new_paxes[] = $room['paxes'][0];
            foreach ($room['paxes'] as &$paxes) {
                $find = 0;
                foreach ($new_paxes as $paxes2) {
                    if (($paxes2['firstName'] == $paxes['firstName']) && ($paxes2['lastName'] == $paxes['lastName'])) {
                        $find = 1;
                    }
                    if (strpos($paxes['firstName'], $paxes2['firstName']) != -1 && ($paxes2['lastName'] == $paxes['lastName'])) {
                        if (strlen($paxes['firstName']) > 3) {
                            if ($paxes['firstName'][strlen($paxes['firstName']) - 2] === " " || $paxes['firstName'][strlen($paxes['firstName']) - 3] === " ") {
                                $find = 1;
                            }
                        }
                    }
                }
                if ($find == 0) {
                    $new_paxes[] = $paxes;
                }
            }
            $room['paxes'] = $new_paxes;
        }

        // get ga data
        $rooms = !empty($orderTem) && isset($orderTem->rooms) ? json_decode($orderTem->rooms, true) : [];
        $roomDetails = isset($rooms[0]) && isset($rooms[0]['roomDetails']) ? $rooms[0]['roomDetails'] : [];

        $anaOptions = false;
        $createdAt = new Carbon($voucher['created_at']);
        $now = Carbon::now();
        $timeDiff = $createdAt->diffInSeconds($now);

        if ($timeDiff < 10 && request()->getHost() === $order['http_host']) { // send ga data when created time diff from now is less then 10 seconds
            $anaOptions = [
                'id'        => $voucher['id'],
                'hotelName' => $hotel->name,
                'country'   => empty($country) ? '' : $country->countryName,
                'city'      => $hotel->city,
                'netPrice'  => $voucher['totalPrice'],
                'tax'       => isset($roomDetails['tax']) ? $roomDetails['tax'] : '',
                'roomName'  => isset($roomDetails['name']) ? $roomDetails['name'] : '',
                'roomCount' => $voucher['room_number'],
                'dayCount'  => $voucher['day_number'],
                'brand'     => strtolower(substr($voucher['provider'], 0, 1)),
                'source'    => $voucher['source'],
            ];
        }

        $voucher['options']  = json_encode($anaOptions);
        Log::info('anaData: ' . $voucher['options']);

        $isMobile = $this->isMobileDevice();
        if ($isMobile) {
            return view('mobile.voucher', $voucher);
        }

        return view('voucher', $voucher);
    }

    /**
     * @param $code
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectToVoucher ($code)
    {
        return redirect('/voucher/' . $code);
    }
}
