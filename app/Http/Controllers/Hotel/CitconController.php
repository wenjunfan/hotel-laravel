<?php

namespace App\Http\Controllers\Hotel;

use App\Classes\CitconPay;
use App\Classes\HotelAPI;
use App\Classes\SinaUrl;
use App\Jobs\TriggerFeedback;
use App\Model\Order;
use App\Model\OrderTem;
use App\Model\User;
use App\Traits\ApiPostTrait;
use App\Traits\HotelDataRetriever;
use Illuminate\Http\Request;
use App\Http\Controllers\Client\Auth\SendMessageController as sendMessage;
use App\Http\Controllers\Hotel\BookController as bookController;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use  Illuminate\Support\Facades\Auth;

/**
 * Class CitconController
 * @package App\Http\Controllers\Hotel
 */
class CitconController extends Controller
{
    use HotelDataRetriever, ApiPostTrait;

    private $partnerId;
    private $cacheLifeTime;
    private $ann106;
    private $annbdcn;
    private $missedOrderChinaEmails;
    private $missedOrderUsEmails;

    public function __construct()
    {
        parent::__construct();

        if (strrpos($this->host, '117book') > -1) {
            $this->partnerId = Auth::check() ? Auth::user()->id : config('app.partner_id');
        }elseif(strpos($this->host, 'supervacation') >-1){
            $this->partnerId = config('app.aa_partner_id');
        }else {
            $this->partnerId = config('app.partner_id');
        }
        $this->cacheLifeTime = config('app.cache_lifetime');
        $this->ann106 = config('staff-emails.ann106');
        $this->annbdcn = config('staff-emails.annbdcn');
        $this->missedOrderChinaEmails = config('staff-emails.china');
        $this->missedOrderUsEmails = config('staff-emails.us');

    }

    /**
     * important: both pc and mobile citcon pay will call this function
     * @param Request $request
     * @param $type
     */
    public function citconpayIpn(Request $request, $type)
    {
        $reply = $request->all();
        $ip = isset($reply['note']) ? $reply['note'] : '';
        Log::info($ip . ' platform ' . $type . ' citcon ipn: ' . json_encode($reply));

        if (isset($reply['reference'])) {
            $Reorderid = $reply['reference'];
            $vendor = $reply['vendor'];
            $tempOrder = OrderTem::where('Reorderid', $Reorderid)
                ->first();
            $placingOrderKey = $Reorderid . 'placingOrder';

            //if citcon charged but not booked
            if (!Cache::has($placingOrderKey)) {
                if (isset($reply['status']) && $reply['status'] === 'success' && empty($tempOrder->orderReference)) {
                    $tempOrder->paid_not_booked = 1;  //for sure, paid not booked
                    if ('we' == $vendor || 'wechatpay' == $vendor) {
                        $paymentId = 'wechatpayid';
                    } elseif ('al' == $vendor || 'alipay' == $vendor) {
                        $paymentId = 'alipayid';
                    }
                    $tempOrder->{$paymentId} = $Reorderid;
                    $tempOrder->save();

                    if ($tempOrder->partnerId != config('app.partner_id')) {
                        if ($tempOrder->partnerId == config('app.aa_partner_id')) {
                            //Email Ann and TODD to check missedOrderList for 117book
                            try {
                                Mail::raw($tempOrder, function ($message) {
                                    $message->subject('新的美亚微信可能支付未下单订单: hotel.supervacation.net/admin/missed-lists');
                                    $message->to(config('staff-emails.ann'))
                                        ->cc(config('staff-emails.jonathan'));
                                });
                            } catch (\Exception $e) {
                                Log::error($this->ip . ' ' . $tempOrder->id . ' 发送新的 ' . config('app.env') . ' 117book可能支付未下单订单失败' . $e->getMessage());
                            }
                        }else{
                            //Email Ann and TODD to check missedOrderList for 117book
                            try {
                                Mail::raw($tempOrder, function ($message) {
                                    $message->subject('新的117book可能支付未下单订单: 117book.com/admin/missed-lists');
                                    $message->to(config('staff-emails.ann'))
                                        ->cc(config('staff-emails.bdcn'));
                                });
                            } catch (\Exception $e) {
                                Log::error($this->ip . ' ' . $tempOrder->id . ' 发送新的 ' . config('app.env') . ' 117book可能支付未下单订单失败' . $e->getMessage());
                            }
                        }

                    } else {
                        //Email Ann and 106 , china and us sales group to check missedOrderList
                        $emailto = array_merge($this->ann106, $this->missedOrderChinaEmails, $this->missedOrderUsEmails);
                        try {
                            Mail::raw($tempOrder, function ($message) use ($emailto) {
                                $message->subject('新的可能支付未下单订单: https://hotel.usitrip.com/affiliate/missed-lists');
                                $message->to($emailto);
                            });
                        } catch (\Exception $e) {
                            Log::error($this->ip . ' ' . $tempOrder->id . ' 发送新的可能支付未下单订单失败' . $e->getMessage());
                        }
                    }
                }
            }
        }
    }

    /**
     *  only touched by citcon h5 payment callback
     * @param $reference
     * @param $vendor
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function continueOrder($reference, $vendor)
    {
        Log::info($this->ip . ' citcon opened h5 callback url');

        return view('continue-order', [
            'optionName' => '',
            'reference' => $reference,
            'vendor' => $vendor,
        ]);
    }

    /**
     * @param $reference
     * @param $vendor
     * @param $amount
     * @return string
     */
    public function citconQr($reference, $vendor, $amount)
    {
        if ($vendor === 'we' || $vendor === 'wechat') {
            $vendor = 'wechatpay';
        } else {
            $vendor = 'alipay';
        }
        $url = CitconPay::qrpay($amount, 'USD', $reference, $vendor);

        return $url;
    }

    /**
     * payment confirmation on user's phone after scanned qr and paid
     */
    public function citconpayConfirm()
    {
        return view('pay-confirm');
    }

    /**
     inquires from pc may happen several times(max: 24, request every 5s)
     * inquires from mobile h5 pay should be only one time
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function citconpayInquire(Request $request)
    {
        $reference = $request->input('reference');
        $transaction = CitconPay::inquire($reference);
        Log::info($this->ip . ' citcon inquire result: ' . json_encode($transaction));

        if (empty($transaction)) {
            return response()->json([
                'success' => false,
            ]);
        }
        $tempInfo = OrderTem::where('Reorderid', $reference)
            ->first();
        $options = [];
        if (!empty($tempInfo)) {
            $searchInfo = json_decode($tempInfo->search_info, true);
            $params = '';
            if (!empty($searchInfo)) {
                $dayCount = $this->calculateDays($searchInfo['checkin'], $searchInfo['checkout']);
                $occ = $searchInfo['occupancies'][0];
                $params = 'checkin=' . $searchInfo['checkin'] . '&checkout=' . $searchInfo['checkout'] . '&dayCount=' . $dayCount . '&roomCount=' . $occ['rooms'] .
                    '&adultCount1=' . $occ['adults'] . '&childCount1=' . $occ['children'];
            }
            Log::info('search param ------ ' . $params);
            $options = [
                'hotelId' => $tempInfo->display_hotelId,
                'hotelName' => $tempInfo->goods_name,
                'params' => $params,
            ];
        }

        return response()->json([
            'success' => true,
            'type' => $transaction->type,
            'status' => $transaction->status,
            'options' => $options,
        ]);
    }

    /**
     * check order status while waiting for placing order on mobile citcon
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkOrder(Request $request)
    {
        $Reorderid = $request->input('reference');
        $orderTem = OrderTem::where('Reorderid', $Reorderid)
            ->first();
        Log::info($this->ip . ' check order status ' . $Reorderid);
        if (!empty($orderTem->orderReference)) {
            return response()->json([
                'success' => true,
                'voucherUrl' => url('voucher/' . $orderTem['code']),
            ]);
        }

        return response()->json([
            'success' => false,
        ]);
    }

    /**
     * citcon pay decline link and send failed email to user
     * @param $Reorderid
     * @return \Illuminate\Http\JsonResponse
     */
    public function declineMissedOrder($Reorderid)
    {
        $missed_order = OrderTem::where('Reorderid', $Reorderid)
            ->get()
            ->last();
        $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';
        if ($missed_order->paid_not_booked == 1 && $missed_order->orderReference == null) {
            $missed_order->paid_not_booked = 3;//0 is default, 1 is paid not booked, 2 is solved paid not booked，3 is declined
            $missed_order->sales_no = $sales_no;
            $missed_order->save();

            $this->sendFailedEmail($missed_order, $Reorderid);

            return response()->json([
                'success' => true,
            ]);

        } else {
            return response()->json([
                'success' => false,
                'message' => '该订单已经被完成，无法拒绝'
            ]);
        }
    }

    /**
     * @param $missed_order
     * @param $Reorderid
     * @return \Illuminate\Http\JsonResponse
     */
    private function sendFailedEmail($missed_order, $Reorderid)
    {
        //citcon refund -- Ann 03052018
        $transactionRef = $missed_order->Reorderid;
        $transaction = CitconPay::inquire($transactionRef);
        $reason = 'missed order decline refund';

        CitconPay::refund($transaction->currency, $transaction->id, $transaction->amount, $reason);
        //email user and accountant Ann book failed
        $locale = session('language', 0);
        $host = $_SERVER['HTTP_HOST'];

        $booker = $missed_order['booker'];
        $bookerArray = json_decode($booker, true);
        $bookerEmail = $bookerArray['email'];

        $langId = 0;
        $hostName = 'Ustrip';
        if(strrpos($host, 'usitour') !== false) {
            $hostName = 'Usitour';
        }
        $fraudEmailSubject = $hostName . '走四方酒店-订单失敗： ' . $Reorderid;

        if ($locale == 2) {
            if (strrpos($host, 'supervacation') !== false) {
                $hostName = 'Supervacation';
                $fraudEmailSubject = '美國亞洲旅遊hotel.supervacation.net酒店-訂單失敗：' . $Reorderid;
            } else {
                $fraudEmailSubject = $hostName . '走四方酒店-訂單失敗：' . $Reorderid;
            }
        } elseif ($locale == 1) {
            $fraudEmailSubject = $hostName . '- Your Order is been Rejected: ' . $Reorderid;
        }
        try {
            Mail::send('emails.order-failed', [
                    'Reorderid' => $Reorderid,
                    'is_aa' => strrpos($host, "supervacation"),
                    'langId' => $langId,
                    'hostName' => $hostName
                ],
                function ($message) use ($fraudEmailSubject, $bookerEmail) {
                    $message->subject($fraudEmailSubject)
                        ->to($bookerEmail)
                        ->bcc(config('staff-emails.ann'));
                });

            return response()->json([
                'success' => true,
            ]);
        } catch (\Exception $e) {
            Log::error($this->ip . ' send fraud email failed' . $e->getMessage());
            return response()->json([
                'success' => false,
            ]);
        }
    }

    /**
     *  * important: both h5 and qr pay will call this function after payment status is confirmed
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function placeOrder(Request $request)
    {
        $Reorderid = $request->input('Reorderid');
        $vendor = $request->input('vendor');
        $tempInfo = OrderTem::where('Reorderid', $Reorderid)
            ->first();

        // prevent citcon book request two times
        $placingOrderKey = $Reorderid . 'placingOrder';

        if (Cache::has($placingOrderKey)) {
            return response()->json([
                'success' => false,
                'message' => 'wait'
            ]);
        }

        Cache::put($placingOrderKey, true, $this->cacheLifeTime);

        if ('we' == $vendor || 'wechatpay' == $vendor) {
            $paymentId = 'wechatpayid';
            $paymentType = 'WECHATPAY';
        } elseif ('al' == $vendor || 'alipay' == $vendor) {
            $paymentId = 'alipayid';
            $paymentType = 'ALIPAY';
        }
        Log::info($this->ip . ' placeOrder with citcon, paymentid: ' . $Reorderid . ' and paymentType: ' . $paymentType);

        $transaction = CitconPay::inquire($Reorderid);

        // if order is not placed, check payment status to continue order
        if ($transaction->type == 'charge' && $transaction->status == 'success' && empty($tempInfo->orderReference)) {
            $tempInfo->paid_not_booked = 0;
            $tempInfo->save();

            $specialRequest = $tempInfo->special_request;
            $user = User::find($this->partnerId);
            $api = new HotelAPI($user);
            $booker = json_decode($tempInfo->booker, true);
            $rooms = json_decode($tempInfo->rooms, true);

            $result = $api->bookRooms($booker, $rooms, $specialRequest);
            Log::info($this->ip . ' citcon Book room result ' . json_encode($result));

            if (isset($result['success']) && $result['success']) {
                $bookRoomResult = $result['data'];
                if($bookRoomResult === null){
                    $pricelineMissData = new BookController();
                    $pricelineMissData->pricelineBookSuccessDataFailed($Reorderid);
                }else{
                    $tempInfo->{$paymentId} = $Reorderid;
                    $tempInfo->code = $result['data']['code'];
                    $tempInfo->orderReference = $result['data']['orderReference'];
                    $tempInfo->save();

                    // it's possible that the backend failed to save porder table,
                    Order::where('orderReference', $result['data']['orderReference'])
                        ->update([
                            'infact_cost' => $tempInfo->amount,
                            'payment_type' => $paymentType,
                            'payment_status' => 'PAID',
                            'remark' => $tempInfo->remark,
                            'source' => $tempInfo->source,
                            'source_pos' =>  isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '',
                            'special_request' => $specialRequest,
                            'sales_no' => $tempInfo->sales_no,
                            'user_email' => $tempInfo->user_email,
                            'display_hotelId' => $tempInfo->display_hotelId,
                            'http_host' => $this->host, //add HTTP host for order table. --052318 Ann
                            'user_language' => session('language', 0), //add user_language for order table --060518 Ann
                        ]);

                    $order = Order::where('orderReference', $result['data']['orderReference'])
                        ->first();

                    if (!empty($order) && $order->provider === 'EBooking') {
                        Log::info('1: placeOrder eb email sent' . $order->id);
                        $this->sendEbEmail($order);
                    }

                    $result = $result['data'];
                    $this->newOrderEmail($tempInfo, $result);

                    //预定完成，更新infusionsoft contact，以发送feedback邮件
                    $langString = $booker['language'] === 0 ? 'cn' : 'en';
                    $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'hotel.usitrip.com';
                    $this->dispatch(new TriggerFeedback($booker['email'], $langString, $domain));

                    Cache::forget($placingOrderKey);

                    return response()->json([
                        'success' => true,
                        'voucherUrl' => url('voucher/' . $result['code'])
                    ]);
                }
            } elseif (!empty($result['errorId']) && ($result['errorId'] === 'ER1415' || $result['errorId'] === 'IT1001')) {
                Cache::forget($placingOrderKey);

                return response()->json([
                    'success' => false,
                    'errorId' => $result['errorId'],
                    'Reorderid' => $Reorderid,
                ]);
            } else {
                Log::info($this->ip . ' place order failed with citcon, start refund');

                $reason = 'citcon order failed, citcon ' . $vendor . ' refund';
                $refundResult = CitconPay::refund($transaction->currency, $transaction->id, $transaction->amount, $reason);
                Log::info($this->ip . ' citcon refund result: ' . json_encode($refundResult));

                Cache::forget($placingOrderKey);

                $CitconBookFailederrorMessage = 'refund failed';
                if (isset($refundResult->refunded) && $refundResult->refunded) {
                    $CitconBookFailederrorMessage = 'fully refunded';
                }
                $errMsg = 'Place order failed, '.$CitconBookFailederrorMessage;
                //api-568 重复订单,退款并告知订单号然后如果确认还是要下单请重新搜索
                if(!empty($result['errorId']) && ($result['errorId'] === 'ER1414')){
                    $errMsg = str_replace("same order.:", '' ,$result['errorMessage']);
                }
                return response()->json([
                    'success' => false,
                    'errorId' => isset($result['errorId']) ? $result['errorId'] : '',
                    'errorMessage' => $errMsg
                ]);
            }
        } elseif (!empty($tempInfo->code)) {
            Cache::forget($placingOrderKey);

            return response()->json([
                'success' => true,
                'voucherUrl' => url('voucher/' . $tempInfo->code)
            ]);
        } else {
            Cache::forget($placingOrderKey);

            return response()->json([
                'success' => false,
                'errorId' => 'IT1002'
            ]);
        }
    }

    /**
     * @param $tempInfo
     * @param $result
     */
    public function newOrderEmail($tempInfo, $result)
    {
        //Email New Order
        $executionStartTime = microtime(true);
        $orderTable = Order::where('code', $result['code'])
            ->leftjoin('hotel', 'porder.hotelId', '=', 'hotel.hotelId')
            ->leftJoin('destination', 'hotel.desId', '=', 'destination.desId')
            ->leftJoin('porder_pax', 'porder.orderReference', '=', 'porder_pax.orderReference')
            ->leftJoin('porder_room', 'porder.orderReference', '=', 'porder_room.orderReference')
            ->first();
        $partner = 'Customer';
        $hotelName = $tempInfo['goods_name'];
        $orderRef = $orderTable->orderPReference;
        $creationTime = $orderTable->created_at;
        $bookingId = $result['bookingId'];
        $totalPrice = $result['currencyCode'] . ' ' . $result['totalPrice'];
        $bookerEmail = $result['bookerEmail'];
        $bookerName = $result['bookerFirstName'] . ' ' . $result['bookerLastName'];
        $roomInfo = $result['rooms']['0'];
        $boardName = $roomInfo['boardName'];
        $roomType = $roomInfo['name'];
        $cancellationInfo = $roomInfo['cancellationPolicies']['0'];
        $cancellation = $cancellationInfo['end'];
        $cancellation = str_replace("T", " ", mb_substr($cancellation, 0, 19));
        $cancelTime = strtotime($cancellation);
        $orderTime = strtotime($creationTime);
        if (!$cancellation) {
            $cancellation = "Non-Refundable";
        } elseif ($cancelTime < $orderTime) {
            $cancellation = "Non-Refundable";
        } else {
            $cancellation = "Free Cancellation before " . $cancellation;
        }
        $roomNumber = $orderTable->room_number;
        $dest1 = $orderTable->address;
        $dest2 = $result['destination'];
        $dest = str_replace(", ", "", $dest1) . ", " . $dest2;
        $checkin = strtotime($result['checkInDate']);
        $checkin = date('M d, Y', $checkin);
        $checkout = strtotime($result['checkOutDate']);
        $checkout = date('M d, Y', $checkout);
        $guestInfo = $roomInfo['paxes']['0'];
        $firstName1 = $guestInfo['firstName'];
        $lastName1 = $guestInfo['lastName'];
        $guest = $firstName1 . ' ' . $lastName1;
        $paxesArray = $roomInfo['paxes'];
        $ad_total = count(array_filter($paxesArray, function ($pax) { return $pax['type'] == 'AD'; }));
        $ch_total = count(array_filter($paxesArray, function ($pax) { return $pax['type'] == 'CH'; }));
        $locale = $orderTable->user_language ? $orderTable->user_language : session('language', 0);
        Log::info($this->ip . ' locale language of user: ' . $locale);

        $host = $orderTable->http_host;
        if (strrpos($host, "usitour") > -1 && locale !== 2) {
             $locale = 1;
        }

        $status = $result['status'] == 'CONFIRMED' ? '已经被确认': '正在处理中';
        if ($locale == 1) {
            $status = $result['status'] == 'CONFIRMED' ? ' is confirmed' : ' is in process';
        } elseif ($locale == 2) {
            $status = $result['status'] == 'CONFIRMED' ? '已經被確認' : '正在處理中';
        }

        $emailContent = 'emails.new-order';
        $hostName = 'Usitour';
        $hostNameCn = '';
        if (strpos($host, 'usitrip') !== false) {
            $hostName = 'Usitrip';
            $hostNameCn = '走四方';
        } elseif (strrpos($host, 'supervacation') !== false) {
            $hostName = 'Supervacation';
        } elseif (strrpos($host, '117book') !== false) {
            $hostName = '117book';
            $hostNameCn = $locale == 2 ? '要趣訂' : '要趣订';
            $emailContent = 'emails.117book_newOrder';
        }

        if ($locale == 0) {
            $langId = 0;
            if ($result['status'] == 'CONFIRMED') {
                $emailSubject = $hostName . $hostNameCn . '酒店预订确认 - （订单号';
                Log::info($this->ip . ' 发送确认中文邮件');
            } else {
                $emailSubject = $hostName . $hostNameCn . '酒店预订正在处理 - （订单号';
                Log::info($this->ip . ' 发送正在处理中文邮件');
            }
        } elseif ($locale === 2) {
            $langId = 2;
            if ($result['status'] == 'CONFIRMED') {
                $emailSubject = $hostName . $hostNameCn . '酒店預訂確認 - （訂單號';
                Log::info($this->ip . ' 发送确认繁体邮件');
            } else {
                $emailSubject = $hostName . $hostNameCn . '酒店預訂正在處理 - （訂單號';
                Log::info($this->ip . ' 发送正在处理繁体邮件');
            }
        } else {
            $langId = 1;
            if ($result['status'] == 'CONFIRMED') {
                $emailSubject = $hostName . ' Hotel Confirmation - (Booking #';
                Log::info($this->ip . ' 发送确认英文邮件');
            } else {
                $emailSubject = $hostName . ' Hotel is in process- (Booking #';
                Log::info($this->ip . ' 发送正在处理英文邮件');
            }
        }

        $bcc = config('staff-emails.ann'); //Todd想看所有的117book新预订邮件, config does not comply with RFC 2822, 3.6.2. ？？ use email directly
        if (strrpos($emailSubject, "117book") > -1) {
            $bcc = $this->annbdcn;
        }

        if (strrpos($host, "usitour") > -1 && $locale !== 2) {
            try {
                $booker = json_decode($tempInfo->booker, true);
                Log::info('for sms $booker info: ' . json_encode($booker));
                //For all new order, send sms
                $input_country_code = isset($booker['countryCodePhone']) ? $booker['countryCodePhone'] : $booker['phone_country'];
                $input_mobile = isset($booker['validPhoneNumber']) ? $booker['validPhoneNumber'] : $booker['phone'];
                $code = SinaUrl::getShort($orderTable->voucher());
                $sendSms = new sendMessage();
                $sendSms->sendOrderSms($input_country_code, $input_mobile, $code);
            } catch (\Exception $e) {
                Log::error($this->ip . ' send new order message error: ' . $e->getMessage());
            }
        }

        try {
            if (strrpos($host, 'supervacation') !== false) {
                $function = 'send';
            }else{
                $function = 'queue';
            }
            if ($result['status'] == 'CONFIRMED' || $result['status'] == 'ON REQUEST') {
                Mail::$function($emailContent, [
                    'partner' => $partner,
                    'orderId' => $bookingId,
                    'status' => $status,
                    'voucher' => $orderTable->voucher(),
                    'checkin' => $checkin,
                    'checkout' => $checkout,
                    'hotelName' => $hotelName,
                    'reference' => $orderRef,
                    'guest' => $guest,
                    'roomCount' => $roomNumber,
                    'totalPrice' => $totalPrice,
                    'adNumber' => $ad_total,
                    'chNumber' => $ch_total,
                    'roomType' => $roomType,
                    'boardType' => $boardName,
                    'cancellation' => $cancellation,
                    'hotelAddress' => $dest,
                    'is_aa' => strrpos($host, 'supervacation'),
                    'langId' => $langId,
                    'hostName' => $hostName
                ], function ($message) use ($bookingId, $bookerEmail, $bookerName, $emailSubject, $result, $bcc) {
                    $message->subject($emailSubject . $bookingId . ')')
                        ->to($bookerEmail, $bookerName)
                        ->bcc($bcc);
                });
                Log::info($this->ip . ' 发送预定邮件');
            }
        } catch (\Exception $e) {
            Log::error($this->ip . ' send new order email error: ' . $e->getMessage());
        }
        $executionEndTime = microtime(true);
        //The result will be in seconds and milliseconds.
        $seconds = $executionEndTime - $executionStartTime;
        Log::info($this->ip . ' TIME to email new Order for ' . $bookingId . ' is ' . $seconds . ' seconds');
    }

}
