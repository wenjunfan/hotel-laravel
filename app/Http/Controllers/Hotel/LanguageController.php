<?php

namespace App\Http\Controllers\Hotel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

/**
 * Class LanguageController
 * @package App\Http\Controllers\Client
 */
class LanguageController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateLanguage(Request $request)
    {
        $langId = (int)$request->input('lang');
        $langStr = 'cn';
        if ($langId == 2) {
            $langStr = 'tw';
        } elseif ($langId == 1) {
            $langStr = 'en';
        }
        session(['language' => $langId]);
        App::setLocale($langStr);

        if ($request->isMethod('get')) {
            return back();
        }

        $url = $request->has('href') ? $request->input('href') : '';
        $url = preg_replace('/[?&]lang=[^&]+$|([?&])lang=[^&]+&/', '$1', $url);
        $url = trim($url, '/#');

        return response()->json([
            'success' => true,
            'url' => $url
        ]);
    }
}
