<?php

namespace App\Http\Controllers\Hotel;

use App\Classes\HotelAPI;
use App\Classes\HotelDB;
use App\Classes\InfusionsoftAPI;
use App\Jobs\UpdateInfusionsoftCityData;
use App\Model\Coupon;
use App\Model\Destination;
use App\Model\Hotel;
use App\Model\SearchHistory;
use App\Model\User;
use App\Traits\IsMobileDevice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Traits\HotelDataRetriever;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * @resource List
 * Class ListController
 * @package App\Http\Controllers\Hotel
 */
class ListController extends Controller
{
    /**
     * traits
     */
    use HotelDataRetriever, IsMobileDevice;

    private $partnerId;
    private $cacheLifeTime;

    /**
     * ListController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        if(strpos($this->host, '117book') >-1){
            $this->partnerId = Auth::check() ? Auth::user()->id : config('app.partner_id');
        }elseif(strpos($this->host, 'supervacation') >-1){
            $this->partnerId = config('app.aa_partner_id');
        }else{
            $this->partnerId = config('app.partner_id');
        }
        $this->cacheLifeTime = config('app.cache_lifetime');
    }

    /**
     * @param Request $request
     * @param $desId
     * @param string $start
     * @param string $end
     * @return \Illuminate\View\View
     */
    public function index(Request $request, $desId, $start = '', $end = '')
    {
        // email incoming users, log id
        if ($request->has('contact_id')) {
            setcookie('contact_id', $request->input('contact_id'), time() + (10 * 365 * 24 * 3600), "/", preg_replace('/^(hotel|www)/', '', $_SERVER['SERVER_NAME']));
        }

        // coupon code
        if ($request->has('code')) {
            $coupon = Coupon::where('code', $request->input('code'))->first();
            if (!empty($coupon)) {
                $today = Carbon::now();
                if ((!isset($coupon->start_at) || Carbon::parse($coupon->start_at)->greaterThan($today)) && (!isset($coupon->expires_at) || Carbon::parse($coupon->expires_at)->lessThan($today))) {
                    session(['coupon' => json_encode($coupon)]);
                }
            }
        }

        if ($request->has('lang')) {
            session(['language' => $request->input('lang') == 'en' ? 1 :  $request->input('lang') == 'tw'? 2 :0]);
        }

        $data = $this->ajaxList($request, $desId, $start, $end);
        $isMobile = $this->isMobileDevice();

        if ($data) {
            if ($isMobile) {
                return view('mobile.list', $data); // new mobile list
            } else {
                return view('list', $data);
            }
        } else {
            return redirect('/');
        }
    }

    /**
     * @param $desId
     * @return array
     */
    private function staticList ($desId) // for robots
    {
        $destinationId = str_replace('.html', '', $desId);
        if (strpos($destinationId, 'D') !== false) {
            $destinationId = str_replace('D', '', $destinationId);
            $hotels = Hotel::where('desId', $destinationId)
                ->whereIn('display', [2, 9])
                ->whereNotNull('name')
                ->get();

            $destination = Destination::where('desId', $destinationId)->first();

            return [
                'destinationName' => !empty($destination) ? $destination->desName : '',
                'destinationCity' => !empty($destination) ? $destination->city : '',
                'destinationId' => $destinationId,
                'hotels' => $hotels
            ];
        } else {
            if (strpos($destinationId, 'H') !== false) {
                $destinationId = str_replace('H', '', $destinationId);
            }
            $hotels = [];
            $hotel = Hotel::where('hotelId', $destinationId)->first();
            $hotels[] = $hotel;
            $destinationCity = '';
            if (!empty($hotel->desId)) {
                $destination = Destination::where('desId', $hotel->desId)->first();
                $destinationCity = $destination->city;
                $hotels[] = Hotel::where('desId', $hotel->desId)
                    ->whereIn('display', [2, 9])->get();
            }

            return [
                'destinationName' => !empty($destination) ? $destination->desName : '',
                'destinationCity' => $destinationCity,
                'destinationId' => $destinationId,
                'hotels' => $hotels
            ];
        }
    }

    /**
     * @param $request
     * @param $desId
     * @param $start
     * @param $end
     * @return array
     */
    public function ajaxList($request, $desId, $start, $end) // get hotels through ajax
    {
        if ($request->has('no')) { // save sales number first
            session(['sales_no' => $request->input('no')]);
        }

        $destinationId = str_replace('.html', '', $desId);
        $scenicId = $request->has('scenicId') ? $request->input('scenicId') : '';
        $scenicName = $request->has('scenicName') ? $request->input('scenicName') : '';
        $destinationName = $request->input('desName');

        $destination = null;
        $hotel = null;
        $isHotelSearch = false;

        // get destination or hotel data first
        if (strpos($destinationId, 'D') !== false) {
            $destination = Destination::where('desId', str_replace('D', '', $destinationId))->first();
            if (empty($destination)) {
                return false;
            }
            $destinationName = $destinationName ? $destinationName : $destination->desName;
        } else {
            $isHotelSearch = true;
            $hotel = Hotel::where('hotelId', str_replace('H', '', $destinationId))->first();
            if (empty($hotel)) {
                return false;
            }
            $destination = $hotel->destination;
            $destinationName = $destinationName ? $destinationName : $hotel->name;
            if (strpos($destinationId, 'H') === false) {
                $destinationId = 'H' . $destinationId;
            }
        }

        // get search params
        if ($request->has('checkin')) {
            $roomCount = $request->input('roomCount');
            $adultCount1 = $request->input('adultCount1');
            $childCount1 = $request->input('childCount1');
            $childAges = $request->input('childAge');
            $occupancies = $this->getOccupancies($roomCount, $adultCount1, $childCount1, $childAges);

            if ($isHotelSearch) {
                $destinationCity = $hotel->city;
            } else {
                $destinationCity = $destination->city;
                if (!empty($scenicId)) { // if scenicId is passed, update destinationName to destination name instead of senicName
                    $destinationName = $destination->desName;
                }
            }

            $searchInfo = [
                'checkin' => $request->input('checkin'),
                'checkout' => $request->input('checkout'),
                'destinationName' => $destinationName, // need this to show mlist and list title when share
                'destinationCity' => $destinationCity, // need this to show mobile list title when share
                'showAllDiscount' => $request->has('discount') ? $request->input('discount') : false,
                'occupancies' => $occupancies
            ];
        } else { // if not passed, get static ones
            $roomCount = 1;
            $adultCount1 = 2;
            $childCount1 = 0;
            $childAges = [12];

            $checkinDate = empty($start) ? Carbon::parse('next sunday')->addDays(7)->toDateString() : $start;
            $checkoutDate = empty($end) || (strtotime($end) < strtotime($checkinDate)) ? Carbon::parse($checkinDate)->addDays(1)->toDateString() : $end;

            $searchInfo = $this->generateStaticParams($checkinDate, $checkoutDate, $destinationName);
            if ($isHotelSearch) {
                $searchInfo['destinationCity'] = $hotel->city;
            } else {
                $searchInfo['destinationCity'] = $destination->city;
            }
        }

        $occupancies = $searchInfo['occupancies'][0];
        $dayCount = $this->calculateDays($searchInfo['checkin'], $searchInfo['checkout']); // all list page functions will use this dayCount

        $data = [
            'optionName' => 'list',
            'destinationName' => $searchInfo['destinationName'], // for pc and mobile v1 to show title
            'destinationCity' => $searchInfo['destinationCity'], // for mobile v1 to show title
            'destinationId' => $destinationId,
            'scenicId' => $scenicId,
            'scenicName' => $scenicName,
            'checkin' => $searchInfo['checkin'],
            'checkout' => $searchInfo['checkout'],
            'roomCount' => $roomCount,
            'dayCount' => $dayCount,
            'adultCount1' => $adultCount1,
            'childCount1' => $childCount1,
            'childAges' => $childAges,
            'showAllDiscount' => $searchInfo['showAllDiscount'],
            'orderBy' => $request->has('orderBy') ? $request->input('orderBy') : 'discount',
            'sort' => $request->has('sort') ? $request->input('sort') : 'desc',
            'key' => $request->has('key') ? $request->input('key') : '',
            'pagePos' => $request->has('pagePos') ? $request->input('pagePos') : '1',
            'rating' => $request->has('rating') ? $request->input('rating') : '',
            'price' => $request->has('price') ? $request->input('price') : '',
            'scenicArea' => $request->has('scenicArea') ? $request->input('scenicArea') : '', // passed when share url
            'chainNames' => $request->has('chainNames') && !empty($request->input('chainNames')) ? $request->input('chainNames') : '',
        ];

        unset($occupancies);

        return $data;
    }

    /**
     * @param Request $request
     * @param $desId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectToList(Request $request, $desId)
    {
        $params = $request->all();
        $paramStr = '';
        foreach ($params as $key => $value) {
            $paramStr .= '&' . $key . '=' . $value;
        }
        $paramStr = substr($paramStr, 1);

        $desId = urldecode($desId);
        if (strpos($desId, ':')) {
            $desId = str_replace(':', '', $desId) . '.html';
        }
        $url = url('/list') . '/' . $desId;
        if (!empty($paramStr)) {
            $url .= '?' . $paramStr;
        }

        return redirect(trim($url), 301);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function tmpHotels(Request $request)
    {
        $destinationId = $request->input('desId');
        $sessionKey = "tmphotelsSession" . $destinationId;
        $redisTmpHotelsKey = $destinationId . ':' . $sessionKey . '.tmpHotels';

        if (Cache::has($redisTmpHotelsKey)) {
            $hotelDetails = json_decode(Cache::get($redisTmpHotelsKey), true);
        } else {
            $db = new HotelDB();
            if (strpos($destinationId, 'D') !== false) {
                $destinationId = str_replace("D", "", $destinationId);
            } else {
                $hotelId = str_replace("H", "", $destinationId);
                $hotel = Hotel::where('hotelId', $hotelId)
                    ->first();

                if (empty($hotel)) {
                    return response()->json([
                        'sessionKey' => $sessionKey,
                        'totalHotels' => 0,
                        'pageHotels' => 20,
                        'currentPage' => 1,
                        'totalPages' => 1,
                        'hotels' => []
                    ]);
                }

                $destinationId = $hotel->desId;
            }

            $hotelName = $request->input('hotelName');
            $hotelName_en = $request->input('hotelName_en');
            $hotelName_zh = $request->input('hotelName_zh');

            if (empty($hotelName_en)) {
                $hotelName_en = $hotelName;
                $hotelName_zh = $hotelName;
            }
            $hotelDetails = $db->tmpHotels($destinationId, $hotelName_en, $hotelName_zh)->toArray();

            if (count($hotelDetails) == 0) {
                return response()->json([]);
            }
            Cache::put($redisTmpHotelsKey, json_encode($hotelDetails), $this->cacheLifeTime);
        }

        // return 20 hotels
        $totalHotels = count($hotelDetails);
        $pageHotels = 20;
        $currentPage = 1;
        $totalPages = ceil($totalHotels / $pageHotels);

        $hotels = $this->getTempHotels($hotelDetails, $pageHotels);

        $result = [
            'sessionKey' => $sessionKey,
            'totalHotels' => $totalHotels,
            'pageHotels' => $pageHotels,
            'currentPage' => $currentPage,
            'totalPages' => $totalPages,
            'hotels' => $hotels
        ];

        return response()->json($result);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchHotel(Request $request)
    {
        $destinationId = $request->input("desId");
        $hotelId = str_replace("H", "", $destinationId);
        $checkin = $request->input('checkin');
        $checkout = $request->input('checkout');
        $roomCount = $request->input('roomCount');
        $adultCount1 = $request->input('adultCount1');
        $childCount1 = $request->input('childCount1');
        $childAges = $request->input('childAge');
        $occupancies = $this->getOccupancies($roomCount, $adultCount1, $childCount1, $childAges);
        $dayCount = $request->input('dayCount');

        $hotel = Hotel::where('hotelId', $hotelId)->with('destination')->first();

        if (empty($hotel)) {
            return null;
        }

        $user = User::find($this->partnerId);
        $api = new HotelAPI($user);
        $hotelIds[] = $hotelId;

        Log::info($this->ip . ' list page search hotel');
        $hotelCaches = $api->searchHotelsByIds($hotelIds, $checkin, $checkout, $occupancies);

        if (isset($hotelCaches['data']['hotels']) && count($hotelCaches['data']['hotels']) > 0) {
            $sessionKey = $hotelCaches['data']['sessionKey'];
            $hotelCaches = $this->cacheHotels($hotelCaches['data'], $roomCount, $dayCount);
            $hotelDetails[] = $hotel;

            $hotels = $this->getFilteredPageHotels($hotelDetails, $hotelCaches, $roomCount, $dayCount); // merge dynamic and static data

            if (count($hotels) > 0) {
                return response()->json([
                    'sessionKey' => $sessionKey,
                    'hotel'      => $hotels[0]
                ]);
            }
        }

        $hotel['thumbnail'] = config('constants.image_host') . (stripos($hotel['thumbnail'], '_Image') === false ? 'Hotel_Image/' : '') . $hotel['thumbnail'];
        $hotel['minPrice'] = 0;

        if (isset($hotel['destination'])) {
            $destination = $hotel['destination'];
            // setup hotel address
            $address = trim(str_replace(",", " ", $hotel['address']));
            $desName = is_null($destination) ? '' : substr($destination['desName'], 0, strrpos($destination['desName'], ","));
            $hotel['address'] = ucwords(strtolower($address)) . ', ' . $desName . ', ' . $hotel['zipcode'];
        }

        return response()->json([
            'success' => true,
            'sessionKey' => '',
            'hotel' => $hotel
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchHotelsRecommend(Request $request)
    {
        $user = User::find($this->partnerId);
        $api = new HotelAPI($user);
        $destinationId = $request->input('desId');
        $checkin = $request->input('checkin');
        $checkout = $request->input('checkout');
        $roomCount = $request->input('roomCount');
        $dayCount = $request->input('dayCount');
        $adultCount1 = $request->input('adultCount1');
        $childCount1 = $request->input('childCount1');
        $childAges = $request->input('childAge');
        $occupancies = $this->getOccupancies($roomCount, $adultCount1, $childCount1, $childAges);

        // cache search information
        $searchInfo = [
            'destinationId' => $destinationId,
            'checkin' => $checkin,
            'checkout' => $checkout,
            'occupancies' => $occupancies,
        ];

        if (strpos($destinationId, 'D') !== false) {
            $destinationId = str_replace("D", "", $destinationId);
        }

        // this part is deprecated, as we'll always search by destination id for recommend hotels
        $hotelId = "";
        if (strpos($destinationId, 'H') !== false) {
            $hotelId = str_replace("H", "", $destinationId); // need this id to find hotel from searchHotels

            $hotel = Hotel::where('hotelId', str_replace("H", "", $destinationId))
                ->with('destination')
                ->whereIn('display', [2, 9])
                ->first();

            if (!empty($hotel->destination)) {
                $destinationId = $hotel->desId; // to get recommend hotels
            } else {
                return response()->json([]);
            }
        }

        $searchHistory = new \stdClass();
        $searchHistory->searchHotelId = $hotelId;
        $searchHistory->searchDestId = $destinationId;
        $this->saveSearchHistory($searchHistory, $this->partnerId);

        Log::info($this->ip . ' list page search recommend hotels');
        $hotelCaches = $api->searchHotels($destinationId, [], $checkin, $checkout, $occupancies);

        unset($searchHistory, $occupancies, $api);

        if (isset($hotelCaches['errorId'])) {
            return response()->json($hotelCaches);
        } else {
            $pageHotels = $request->has('numLimit') ? $request->input('numLimit') : 20;
            $currentPage = 1;
            $hotels = [];

            if (isset($hotelCaches['data'])) {
                $hotelCaches = $hotelCaches['data'];
                $sessionKey = $hotelCaches['sessionKey'];

                if (isset($hotelCaches['hotels'])) {
                    $hotelCaches = $this->cacheHotels($hotelCaches, $roomCount, $dayCount); //important: being used for filtering hotels
                    $totalHotels = count($hotelCaches);
                } else {
                    $totalHotels = 0;
                }
                $totalPages = ceil($totalHotels / $pageHotels);
            } else {
                $sessionKey = '';
                $totalHotels = 0;
                $totalPages = 1;
            }

            $result = [
                'success' => true,
                'sessionKey' => $sessionKey,
                'totalHotels' => $totalHotels,
                'pageHotels' => $pageHotels,
                'currentPage' => $currentPage,
                'totalPages' => $totalPages,
                'hotels' => $hotels
            ];

            unset($hotelCaches, $searchInfo);

            return response()->json($result);
        }
    }

    /**
     * @param $hotelCaches
     * @param $roomCount
     * @param $dayCount
     * @return array
     */
    private function cacheHotels($hotelCaches, $roomCount, $dayCount) // cache hotels for filter and open hotel detail page
    {
        $hotels = $hotelCaches['hotels'];
        $hotelsCache = [];
        $expediaProvider = DB::table('gate')->where('provider', '["Expedia"]')->first();
        // show lowest flag on hotel or not
        $expediaEnabled = !is_null($expediaProvider) && $expediaProvider->display == 1 ? true : false;

        foreach ($hotels as $key => $hotel) {
            $discount = isset($hotel['discount']) ? $hotel['discount'] : 0;
            if (isset($hotel['dynamicDiscount']) && $hotel['dynamicDiscount'] > $discount) {
                $discount = $hotel['dynamicDiscount'];
            }

            $taxRate = config('constants.tax_rate');

            $beforeTax = $hotel['minRate'] === -1 ? $hotel['minRate'] : floor(($hotel['minRate'] * (1 - $taxRate)) / $roomCount / $dayCount) * $roomCount * $dayCount;

            $hotelsCache[] = [ // cache hotels to be used for filtering on list page only
                'id' => $hotel['id'],
                'currency' => $hotel['currency'],
                'minRate' => $hotel['minRate'],
                'beforeTax' => $beforeTax,
                'discount' => $discount,
                'lowestFlg' => isset($hotel['lowestFlg']) && $expediaEnabled ? $hotel['lowestFlg'] : 0,
                'distance' => isset($hotel['distance']) && $expediaEnabled ? $hotel['distance'] : null,
                'guestReviewCount' => isset($hotel['guestReviewCount']) && $expediaEnabled ? $hotel['guestReviewCount'] : 0,
                'guestRating' => isset($hotel['guestRating']) && $expediaEnabled ? $hotel['guestRating'] : '',
            ];
        }

        $redisHotelCachesKey = $hotelCaches['sessionKey'] . '.hotels'; // api sessionKey
        Cache::put($redisHotelCachesKey, json_encode($hotelsCache), $this->cacheLifeTime);

        return $hotelsCache;
    }

    /**
     * @param Request $request
     * @param $sessionKey
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterHotels(Request $request, $sessionKey) // pc and mobile v1 filter hotels asynchronously
    {
        $db = new HotelDB();
        $orderBy = $request->input('orderBy');
        $sort = $request->input('sort');
        $currentPage = $request->input('pagePos');
        $showAllDiscount = $request->has('discount') ? $request->input('discount') : false; // to tell filter to show all discounted hotels at first or not
        $key = $request->input('key');
        $pageHotels = $request->has('numLimit') ? $request->input('numLimit') : 20; // jie - you can pass this number to limit hotels for each page
        $rating = $request->input('rating');
        $chainNames = $request->input('chainNames'); // chain names array
        $price = count($request->input('price')) > 0 ? $request->input('price') : [1, 5]; // price range number array, min 1, max 5
        $roomCount = $request->input('roomCount');
        $dayCount = $request->input('dayCount');
        $checkin = $request->input('checkin');
        $checkout = $request->input('checkout');
        $adultCount1 = $request->input('adultCount1');
        $childCount1 = $request->input('childCount1');
        $childAges = $request->input('childAge');

        $price = $this->getPrice($price, $roomCount, $checkin, $checkout);

        $redisHotelCachesKey = $sessionKey . '.hotels';
        if (!Cache::has($redisHotelCachesKey)) {
            return response()->json([
                'success' => false,
                'message' => 'Session expired, please search again'
            ]);
        }

        $hotelCaches = json_decode(Cache::get($redisHotelCachesKey), true);
        $scenicArea = $request->has('scenicArea') ? $request->input('scenicArea') : [];
        $destinationId = $request->has('desId') ? $request->input('desId') : '';

        $hotelDetails = $db->filterHotels($hotelCaches, $currentPage, $pageHotels, $orderBy, $sort,
            $rating, $price, $key, $showAllDiscount, $scenicArea, $destinationId, $chainNames);

        $totalHotels = $hotelDetails[0];
        $hotelChains = $hotelDetails[2]; // hotel chains of current total hotels
        $totalPages = ceil($totalHotels / $pageHotels);

        $hotels = $this->getFilteredPageHotels($hotelDetails[1], $hotelCaches, $roomCount, $dayCount); // merge dynamic and static data

        unset($hotelDetails, $hotelCaches);

        $result = [
            'success' => true,
            'sessionKey' => $sessionKey,
            'totalHotels' => $totalHotels,
            'pageHotels' => $pageHotels,
            'currentPage' => $currentPage,
            'totalPages' => $totalPages,
            'hotels' => $hotels,
            'hotelChains' => $hotelChains // chain object array
        ];

        $redisContactUpdatedKey = $sessionKey . '.updated';
        if (!Cache::has($redisContactUpdatedKey) && isset($_COOKIE['contact_id']) && $_COOKIE['contact_id'] !== null) {
            if (config('app.enable_email_campaign')) {
                try {
                    //get infusionsoft contact
                    $infusionsoft = new InfusionsoftAPI();
                    $contact = $infusionsoft->client->contacts()->find($_COOKIE['contact_id']);

                    //update infusionsoft contact
                    if (!empty($contact)) {
                        $occupancies = $this->getOccupancies($roomCount, $adultCount1, $childCount1, $childAges);
                        $searchInfo = [
                            'destinationId' => $destinationId,
                            'checkin' => $checkin,
                            'checkout' => $checkout,
                            'occupancies' => $occupancies,
                        ];

                        $langId = session('language', 0);
                        $contact_fields = [
                            'language' => $langId == 0 ? 'cn' : ($langId == 1 ? 'en' : 'tw'),
                            'search_info' => $searchInfo,
                            'domain' => isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'hotel.usitrip.com'
                        ];

                        $destination = null;
                        if (strpos($destinationId, 'D') !== false) {
                            $destination = Destination::where('desId', str_replace('D', '', $destinationId))
                                ->first();
                        } else {
                            $hotelId = str_replace('H', '', $destinationId);
                            $hotel = Hotel::where('hotelId', $hotelId)
                                ->first();
                            $destination = $hotel->destination;
                        }

                        $city_interest_cam = 'cityInterest';
                        if (config('app.env') === 'local') {
                            $city_interest_cam = 'cityInterestTest';
                        }

                        $this->dispatch(new UpdateInfusionsoftCityData($contact, $contact_fields, $destination, $hotels, [$city_interest_cam], [101]));
                        Cache::put($redisContactUpdatedKey, 1, $this->cacheLifeTime);
                    }
                } catch (\Exception $exception) {
                    Log::error($this->ip . ' update infusionsoft contact interested search city or hotel failed, ' . $exception->getMessage());
                }
            }
        }

        return response()->json($result);
    }

    /**
     * @param Request $request
     * @param $sessionKey
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterMapHotels(Request $request, $sessionKey)
    {
        $db = new HotelDB();
        $roomCount = $request->input('roomCount');
        $dayCount = $request->input('dayCount');

        $redisHotelCachesKey = $sessionKey . '.hotels';
        if (!Cache::has($redisHotelCachesKey)) {
            return response()->json([
                'success' => false,
                'message' => 'Session expired, please search again'
            ]);
        }

        $hotelCaches = json_decode(Cache::get($redisHotelCachesKey), true);

        $map = [
            'northeast_lat' => $request->input('northeast_lat'),
            'northeast_lng' => $request->input('northeast_lng'),
            'southwest_lat' => $request->input('southwest_lat'),
            'southwest_lng' => $request->input('southwest_lng'),
            'zoom' => $request->input('zoom'),
        ];

        $hotelDetails = $db->filterMapHotels($hotelCaches, $map);

        $totalHotels = $hotelDetails[0];

        $hotels = $this->getFilteredPageHotels($hotelDetails[1], $hotelCaches, $roomCount, $dayCount); // merge dynamic and static data

        unset($hotelDetails, $hotelCaches);

        $result = [
            'success' => true,
            'sessionKey' => $sessionKey,
            'totalHotels' => $totalHotels,
            'hotels' => $hotels,
        ];

        return response()->json($result);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function scenicArea(Request $request) // get scenic area asynchronously
    {
        $db = new HotelDB();
        $destinationId = $request->input('destinationId');
        $details = $db->getScenicArea($destinationId);
        $result = [
            'data' => $details
        ];

        return response()->json($result);

    }

    /**
     * @param $hotelDetails
     * @param $hotelCaches
     * @param $pageHotels
     * @return array
     */
    private function getTempHotels($hotelDetails, $pageHotels) // for tmp hotels to get loading hotels
    {
        $hotelIndex = 0;
        $finalHotels = [];

        foreach ($hotelDetails as $hotel) {
            $finalHotels[] = [
                'provider' => $hotel['provider'],
                'hotelId' => $hotel['hotelId'],
                'name' => $hotel['name'],
                'name_zh' => $hotel['name_zh'],
                'rating' => $hotel['rating'],
                'category' => $hotel['category'],
                'type' => $hotel['type'],
                'chain' => $hotel['chain'],
                'desId' => $hotel['desId'],
                'thumbnail' => config('constants.image_host') . (stripos($hotel['thumbnail'], '_Image') === false ? 'Hotel_Image/' : '') . $hotel['thumbnail'],
                'city' => $hotel['city'],
                'address' => strtolower($hotel['address']),
                'zipcode' => $hotel['zipcode'],
                'latitude' => $hotel['latitude'],
                'longitude' => $hotel['longitude'],
                'minPrice' => $hotel['minPrice'],
                'tax' => isset($hotel['tax']) ? $hotel['tax'] : 0,
                'currency' => $hotel['currency'],
            ];

            if (++$hotelIndex == $pageHotels) {
                break;
            }
        }

        return $finalHotels;
    }

    /**
     * @param $price
     * @param $roomCount
     * @param $checkin
     * @param $checkout
     * @return array|mixed
     */
    private function getPrice($price, $roomCount, $checkin, $checkout)
    {
        $priceRange = [
            1 => [0, 75],
            2 => [75, 124],
            3 => [125, 199],
            4 => [200, 299],
            5 => [300, 100000],
        ];

        if (count($price) === 1) {
            $price = $priceRange[$price[0]];
        } else {
            $min = $priceRange[$price[0]];
            $max = $priceRange[$price[1]];
            $price = array_merge($min, $max);
            asort($price);
        }

        $min = $price[0];
        $max = $price[count($price)-1];
        $price = [];
        $price[0] = $min;
        $price[1] = $max;
        $price[2] = $min * $roomCount;
        $price[3] = $max * $roomCount;
        $price[4] = $checkin;
        $price[5] = $checkout;

        return $price;
    }

    /**
     * @param $result
     * @param $partnerId
     */
    private function saveSearchHistory($result, $partnerId)
    {
        $searchHistory = new SearchHistory();

        $searchHistory->partner_id = $partnerId;
        $searchHistory->search_hotel_id = $result->searchHotelId;
        $searchHistory->search_dest_id = $result->searchDestId;

        $searchHistory->save();
    }

}
