<?php

/**
 * Utility Static Controller
 *
 *
 * @author    Mark <2853759768@qq.com>
 * @copyright 2015 usitrip.com
 * @version   Release: 1.0
 */

namespace App\Http\Controllers;

/**
 * Class Util
 * @package App\Http\Controllers
 */
class Util
{
    /**
     * @param int $length
     * @return string
     */
    public static function UUID($length = 3)
    {
        $res = sprintf("%04x", mt_rand(0, 0x0fff) | 0x4000);
        for ($i = 1; $i < $length; $i++) {
            $res .= sprintf("%04x", mt_rand(0, 0xffff));
        }

        return $res;
    }

    /**
     * @param $url
     * @return mixed
     */
    public static function httpGet($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }

}