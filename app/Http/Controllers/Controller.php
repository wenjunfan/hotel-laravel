<?php

namespace App\Http\Controllers;

use App\Traits\GetIpAddress;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests, GetIpAddress;

    public $ip;
    public $host;

    public function __construct()
    {
        $this->ip = $this->getIpAddr();
        $this->host = \request()->getHost();
    }
}
