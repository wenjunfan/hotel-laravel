<?php

namespace App\Http\Controllers\Affiliate;

use App\Http\Controllers\Controller;
use App\Model\Distributor;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * Class AccountController
 * @package App\Http\Controllers\Affiliate
 */
class AccountController extends Controller
{
    /**
     * @param Request $request
     * @return Request
     */
    public function index(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');
        $keywords = $request->input('keywords');

        return $this->getData($from, $to, $keywords);
    }

    /**
     * 说明：
     * @param string $from : created_at
     * @param string $to : created_at
     * @param string $keywords
     * @return Request
     */
    public function getData($from = '', $to = '', $keywords = '')
    {
        $users = Distributor::with('partner');
        if ($from != '') {
            $users = $users->where('created_at', '>=', $from);
        }

        if ($to != '') {
            $users = $users->where('created_at', '<=', $to);
        }

        if ($keywords != "") {
            $users = $users->where('distributor_name', 'like', '%' . $keywords . '%');
        }

        $users = $users->orderBy('id', 'desc')
            ->get();

        return view('affiliate.accounts', [
            'optionName' => 'affiliate-accounts',
            'users' => $users,
            'from' => isset($from) ? substr($from, 0, 10) : '',
            'to' => isset($to) ? substr($to, 0, 10) : '',
            'keywords' => isset($keywords) ? $keywords : '',
        ]);
    }

    /**
     * todo: deprecated
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json([
                'success' => false,
                'message' => '用户注册信息有错误'
            ]);
        }
        if ($request->has('ratio')) {
            $user->affiliateRate = $request->input('ratio');
            Log::info($request->input('ratio'));
        }
        if ($request->has('remark')) {
            $oldCommend = $user->remark === null ? "" : $user->remark;
            $newCommend = $request->input('remark') . '<br>' . $oldCommend;
            $user->remark = $newCommend;
        }
        if ($request->has('admin')) {
            if ($user->admin === 5) {
                if ($user->host_name == 'Usitrip') {
                    $link = 'https://hotel.usitrip.com/affiliate/welcome';
                } else {
                    $link = 'https://hotel.usitour.com/affiliate/welcome';
                }

                $newUserEmail = 'emails.affiliate_newUser';
                $subject = '欢迎加入走四方人人赚';
                $langId = 0;
                if ($user->language == 2) {
                    $subject = '歡迎加入走四方人人賺';
                    $langId = 2;
                } elseif ($user->language == 1) {
                    $newUserEmail = 'emails.affiliate_newUser_en';
                    $subject = 'Welcome to affiliate partnership';
                    $langId = 1;
                }
                $receiverEmail = $user->email;
                try {
                    Mail::queue($newUserEmail,
                        ['NewUserName' => $user->name, 'link' => $link, 'domain' => $user->host_name, 'langId' => $langId],
                        function ($message) use ($subject, $receiverEmail) {
                            $message->to($receiverEmail)
                                ->bcc(config('staff-emails.ann'), 'Ann')
                                ->subject($subject);
                        });
                    Log::info("send New User Email successfully");
                } catch (\Exception $e) {
                    Log::info("not send New User Email" . $e);
                    Log::error('send new user email failed: ' . $e->getMessage());
                }
            }
            $user->admin = $request->input('admin');
        }

        try {
            $user->save();
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            Log::error('save user update failed: ' . $e->getMessage());
            return response()->json(['success' => false]);
        }

    }

}

