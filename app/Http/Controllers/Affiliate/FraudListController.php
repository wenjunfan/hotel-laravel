<?php

namespace App\Http\Controllers\Affiliate;

use App\Classes\SinaUrl;
use Exception;
use App\Model\Country;
use App\Model\Hotel;
use App\Model\Order;
use App\Model\Config;
use App\Model\OrderTem;

use App\Model\SuspiciousOrder;
use App\Model\PhoneCountry;
use App\Http\Controllers\Controller;
use App\Traits\IsLandLineTrait;
use App\Traits\PaymentTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Client\Auth\SendMessageController as sendMessage;
use App\Traits\SendMessageTrait;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class FraudListController
 * @package App\Http\Controllers\Affiliate
 */
class FraudListController extends Controller
{
    use IsLandLineTrait, PaymentTrait, SendMessageTrait;

    protected $orderLimit;

    public function __construct()
    {
        parent::__construct();
        $this->orderLimit = (int)config('constants.order_limit');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aa_host = 'supervacation';
        if (Auth::user()->id == config('app.aa_partner_id')) {
            $orderLists = SuspiciousOrder::limit($this->orderLimit)
                ->orderBy('id', 'desc')
                ->where('http_host', 'like', '%' . $aa_host . '%')
                ->get();
        } else {
            $orderLists = SuspiciousOrder::limit($this->orderLimit)
                ->orderBy('id', 'desc')
                ->where('http_host', 'not like', '%' . $aa_host . '%')
                ->get();
        }

        $configs = Config::all();
        $optionName = 'fraud-list';

        $sales_no = '';
        //技术
        if (Auth::user()->email === 'api@117book.com') {
            $sales_no = '117';
        }
        //余总
        if (Auth::user()->id === 10655) {
            $sales_no = '19';
        }
        //美亚
        if (Auth::user()->id == config('app.aa_partner_id')) {
            $sales_no = config('app.aa_partner_id');
        }
        $CookieName = "a";    // Cookie's name
        $CookieValue = isset($_COOKIE['a']) ? $_COOKIE['a'] : $sales_no; // Cookie's value
        $CookieDirectory = "/";        // Cookie directory ("/" for all directories)
        $DaysCookieShallLast = 31;     // Days before expiration (decimal number okay.)

        $usitourDomain = str_replace('usitrip', 'usitour', $_SERVER{'HTTP_HOST'});
        $CookieDomain = '.' . preg_replace('/^hotel\./', '', $usitourDomain);
        $CookieDomain = preg_replace('/:\d+$/', '', $CookieDomain);
        $lasting = ($DaysCookieShallLast <= 0) ? "" : time() + ($DaysCookieShallLast * 24 * 60 * 60);
        //如果不是英文大团的客服，没有usitour的a值的时候，我们根据usitrip的去设置usitour的期限一个月。
        setcookie($CookieName, $CookieValue, $lasting, $CookieDirectory, $CookieDomain);

        return view('affiliate.fraud-list', compact([
            'optionName',
            'orderLists',
            'configs',
            'CookieValue',
        ]));
    }

    /**
     * add comment for fraud order
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fixOrderList(Request $request)
    {
        $orderList = SuspiciousOrder::where('id', $request->input('id'))
            ->first();
        $oldCommend = $orderList->commend;
        // 附件服务器时间
        $newCommend = Carbon::now()
                ->toDateTimeString() . '&#013;' . $request->input('commend') . '&#013;' . $oldCommend;
        $orderList->commend = $newCommend;
        try {
            $orderList->update([
                'commend' => $newCommend,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'updated!',
                'id' => $orderList->id,
                'commend' => $orderList->commend,
            ]);
        } catch (Exception $e) {
            Log::error('fix order list error: ' . $e->getMessage());
            return response()->json([
                'success' => false,
                'message' => 'comment order list error',
                'id' => $orderList->id,
                'commend' => $newCommend,
            ]);
        }
    }

    /**
     * change fraud config var
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeConfig(Request $request)
    {
        $id = $request->input('id');
        $configs = Config::where('id', $id)
            ->get()
            ->first();
        $value = $request->input('value');
        Log::info('set config id ' . $id . '\'s value to ' . $value);
        try {
            if ($request->has('value')) {
                $configs->value = $request->input('value');
                $configs->timestamps = false;
                $configs->save();
            }
            $configs = Config::where('key', '<>', '')
                ->get();
            $configs = json_encode($configs, true);
            $redis = Redis::set('Config', $configs);
            $changedRedis = Redis::get('Config');
            Log::info('test update redis status: ' . json_encode($redis));
            Log::info('test updated redis config:' . json_encode($changedRedis));

            return response()->json([
                'success' => true,
                'message' => 'updated!'
            ]);
        } catch (Exception $e) {
            Log::error('error for config change: ' . $e->getMessage());
            return response()->json([
                'success' => false,
                'message' => 'error for config change!'
            ]);
        }
    }

    /**
     * find done orderId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderid($id)
    {
        $orderList = SuspiciousOrder::where('id', $id)
            ->first();
        $porderTem = OrderTem::select('Reorderid', 'orderReference')
            ->where('Reorderid', $orderList->Reorderid)
            ->first();
        $porder = Order::select('orderReference', 'id')
            ->where('orderReference', $porderTem->orderReference)
            ->first();
        $array = json_decode(json_encode($porder), true);
        if ($array) {
            return response()->json($array['id']);
        } else {
            return response()->json([]);
        }
    }

    /**
     * get fraud  detail info
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function moreInfo($id)
    {
        $suspiciousOrder = SuspiciousOrder::find($id);
        $cardDetail = json_decode($suspiciousOrder->cc_info, true);
        $searchedInfo = json_decode($suspiciousOrder->search_info, true);
        $bookerInfo = json_decode($suspiciousOrder->booker, true);

        // start check Land Line if failed to save when reserve
        // check countryCodePhone first
        if (isset($cardDetail['countryCodePhone'])) {
            $full_phone_num = $cardDetail['countryCodePhone'] . $cardDetail['phone']; // +14143502296
            if (!isset($suspiciousOrder->phone_num_type) || $suspiciousOrder->phone_num_type == '') {
                $twilioResult = $this->getPhoneType($full_phone_num) ? $this->getPhoneType($full_phone_num)
                    ->getData() : '';// param eg.: +14143502296
                Log::info('twilio result ' . json_encode($twilioResult));
                if ($twilioResult != '') {
                    $suspiciousOrder->phone_num_type = $twilioResult->phoneType;
                    Log::info('twilio result phone_num_type' . $twilioResult->phoneType);
                    $suspiciousOrder->phone_num_country = $twilioResult->phoneCountryCode;
                    // save to db
                    $suspiciousOrder->save();
                }
            }
        }// end check Land Line

        //check if new user
        $previousOrder = Order::where('bookerPhone', $cardDetail['phone'])
            ->orWhere('bookerPhone', $bookerInfo['phone'])
            ->orWhere('bookerEmail', $bookerInfo['email'])
            ->first();
        $isNewUser = !empty($previousOrder) ? false : true;

        $cc_error_count = $suspiciousOrder->cc_error_count;
        if ($cc_error_count == null) {
            $cc_error_count = 'n/a';
        } else {
            $cc_error_count = $cc_error_count - 1;
        }

        $tempInfo = $suspiciousOrder->orderTem;

        if (isset($tempInfo['booker_local_time'])) {
            $date = explode("(", $tempInfo->booker_local_time);
            $localTime = Carbon::parse($date[0])
                ->toDateTimeString();
        } else {
            $localTime = '';
        }

        $cardCountry = ucwords(strtolower($tempInfo["cardCountry"]));
        $days = Carbon::parse($searchedInfo['checkin'])
            ->diffInDays(Carbon::parse($searchedInfo['checkout']));
        $daybetween = Carbon::parse($searchedInfo['checkin'])
            ->diffInDays($suspiciousOrder->created_at);
        $people = $searchedInfo["occupancies"][0]["adults"] + $searchedInfo["occupancies"][0]["children"];
        //酒店国家
        $hotel = Hotel::where('hotelId', $tempInfo['hotelId'])
            ->select('countryId', 'city', 'state')
            ->first();
        $countryName = Country::where('countryId', $hotel['countryId'])
            ->select('countryName')
            ->first();
        if ($hotel["state"] == '') {
            $state = '';
        } else {
            $state = $hotel["state"] . ', ';
        }
        $address = $hotel["city"] . ", " . $state . $countryName["countryName"];
        //电话国家
        $phoneCountry = '';

        // detect sms country from twilio, if failed, find from db phoneCountry
        if ($suspiciousOrder['phone_num_country'] != '') {
            $phoneCountry = $suspiciousOrder['phone_num_country'];
        } elseif (isset($cardDetail['countryCodePhone'])) {
            $phoneCountry = PhoneCountry::where('phone_code', $cardDetail['countryCodePhone'])
                ->first();
            $phoneCountry = $phoneCountry['country_code'];
        }

        if ($suspiciousOrder->language == 1) {
            $userLang = 'English';
        } else {
            $userLang = '中文';
        }

        //信用卡street
        $street = $cardDetail['street'];
        //信用卡zipCode
        $zipCode = $cardDetail['zip'];
        $cityState = $cardDetail['city'] . ', ' . $cardDetail['state'];

        //error list
        $errorList = $suspiciousOrder->error_list;
        $errorScore = count(json_decode($errorList, true));
        //ip国家
        $ipCountry = $tempInfo["client_ip"];

        // set IP address and API access key
        $ip = $tempInfo["client_ip"];
        // free plan limited to 10,000 request, separate fraud check with book process to avoid exceed limit
        $access_key1 = '6e765ee64feeaf3d86309081c930964f'; //2355958065@qq.com
        $access_key2 = 'd2c8e633ef2881a47529b535630146fa'; //iannievan@gmail.com
        $access_key3 = '26d3742b04b2e315c4fe763b3ac31d96';  //wenjun.fan.ann@gmail.com
        $api_result = $this->ipstack($ip, $access_key1);

        Log::info('ip result 1: ann qq mail ' . json_encode($api_result));
        if (isset($api_result['error'])) {
            $api_result = $this->ipstack($ip, $access_key2);
            Log::info('ip result 2: iannievan gmail ' . json_encode($api_result));
            if (isset($api_result['error'])) {
                $api_result = $this->ipstack($ip, $access_key3);
                Log::info('ip result 3: wenjun.fan.ann gmail ' . json_encode($api_result));
                if (isset($api_result['error'])) {
                    // Output ip location
                    $ipLocation = '查看城市的api错误，请通知it团队！';
                    //USE US AND EMAIL TECH TEAM ALL THREE API USAGE RUN OUT
                    try {
                        Mail::raw(json_encode($api_result['error']), function ($message) {
                            $message->subject('All three ipstack accounts ran out of usage');
                            $message->to(config('staff-emails.tech'));
                        });
                        Log::info('All three ipstack accounts ran out of usage');
                    } catch (Exception $e) {
                        Log::error('Error with sending All three ipstack accounts ran out of usage to us ' . $e->getMessage());
                    }
                } else {
                    // Output ip location
                    $ipLocation = $api_result['city'] . ', ' . $api_result['region_code'] . ', ' . $api_result['country_code'] . ', ' . $api_result['zip'];
                }
            } else {
                // Output ip location
                $ipLocation = $api_result['city'] . ', ' . $api_result['region_code'] . ', ' . $api_result['country_code'] . ', ' . $api_result['zip'];
            }
        } else {
            // Output ip location
            $ipLocation = $api_result['city'] . ', ' . $api_result['region_code'] . ', ' . $api_result['country_code'] . ', ' . $api_result['zip'];
        }

        $phoneCity = '';

        // num-verify to get phone city
        // set API Access Key以下2个是numverify查电话的
        $access_key = '28f8fa5a341ccd56501d6dfd48ba8a55'; //wenjun.fan.ann@gmail.com
        //$access_key = '8ef7b2802d101b82d7a9b443bf3e9f09'; //iannievan@gmail.com

        // set phone number
        $phone_number = $full_phone_num;

        // Initialize CURL:
        $ch = curl_init('http://apilayer.net/api/validate?access_key=' . $access_key . '&number=' . $phone_number . '');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);

        // Decode JSON response:
        $validationResult = json_decode($json, true);

        // Access and use your preferred validation result objects
        if ($validationResult['valid']) {
            $phoneCity = $validationResult['location'];
            if ($phoneCity !== '') {
                $phoneCity = ', ' . $phoneCity;
            }
            //if failed to detect number type base on twilio, use num-verify as backup
            if (!$suspiciousOrder->phone_num_type || $suspiciousOrder->phone_num_type == '') {
                $suspiciousOrder->phone_num_type = $validationResult['line_type'];
                $suspiciousOrder->phone_num_country = $validationResult['country_code'];
                //更新后danger_sign为2的话去除
                if ($suspiciousOrder->danger_sign == 2 && $validationResult['line_type'] == 'mobile') {
                    $suspiciousOrder->danger_sign = 0;
                }
                $suspiciousOrder->save();
                $phoneCountry = $validationResult['country_code'];
            }
        }

        //Pass value to blade
        return response()->json([
            'success' => true,
            'phoneCountry' => $phoneCountry,
            'phoneCity' => $phoneCity,
            'errorScore' => $errorScore,
            'cc_error_count' => $cc_error_count,
            'error_list' => $errorList,
            'localTime' => $localTime,
            'days' => $days,
            'people' => $people,
            'address' => $address,
            'rooms' => $searchedInfo["occupancies"][0]["rooms"],
            'daybetween' => $daybetween,
            'cardCountry' => $cardCountry,
            'hotelName' => $tempInfo["goods_name"],
            'ipCountry' => $ipCountry,
            'userLang' => $userLang,
            'checkin' => $searchedInfo["checkin"],
            'checkout' => $searchedInfo["checkout"],
            'zipCode' => $zipCode,
            'phone_num_type' => $suspiciousOrder->phone_num_type,
            'full_phone_num' => $full_phone_num,
            'ipLocation' => $ipLocation,
            'isNewUser' => $isNewUser,
            'cityState' => $cityState,
            'street' => $street,
            'userEmail' => $bookerInfo['email'],
        ]);
    }

    /**
     * @param $ip
     * @param $access_key
     * @return mixed
     */
    public function ipstack($ip, $access_key)
    {

        // Initialize CURL:
        $ch = curl_init('http://api.ipstack.com/' . $ip . '?access_key=' . $access_key . '');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);
        // Decode JSON response
        return json_decode($json, true);
    }

    /**
     * 更新fraud黑名单订单为白名单
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmNotFraudOrder($id)
    {
        Log::info($this->ip . ' is confirm fraud for id: ' . $id);
        $sus = SuspiciousOrder::find($id);
        //        save phone
        $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';
        try {
            $sus->update([
                'isLocked' => 0,
                'danger_sign' => 0,
                'sales_no' => $sales_no,
            ]);

            return response()->json([
                'success' => true
            ]);
        } catch (\Exception $e) {
            Log::error($this->ip . ' error with setting cc card from blacklist to white' . $e->getMessage());
        }
        return response()->json([
            'success' => false,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSuspiciousOrderEmailAgain($id)
    {
        $data = SuspiciousOrder::where('id', $id)
            ->first();
        Log::info($this->ip . ' sendSuspiciousOrderEmailAgain , id is ' . $id);
        $language = $data->language;
        $host = $data->http_host;

        //base on language and http host to send email with different language and title img
        $cc = json_decode($data->cc_info);
        $searchedInfo = json_decode($data->search_info);
        $emails = config('staff-emails.ann');
        $bookerInfo = json_decode($data->booker);
        $tem = $data = OrderTem::where('Reorderid', $data->Reorderid)
            ->first();
        $bookInfo = json_decode($data->rooms)[0];

        $langId = 0;
        $subject = '您的酒店预订正在处理中,请上传证件确认预订!';
        $hostName = 'Usitrip';
        if(strrpos($host, 'usitour') !== false) {
            $hostName = 'Usitour';
        }
        if ($language == 2) {
            $langId = 2;
            $subject = '你的飯店預訂正在處理中,請上傳證件確認預訂!';
        } elseif ($language == 1) {
            $langId = 1;
            $subject = 'Your hotel reservation is in process, please upload file to confirm!';
        }

        try {
            Mail::send('emails.suspicious-order-email-again', [
                'data' => $data,
                'cc' => $cc,
                'tem' => $tem,
                'bookInfo' => $bookInfo,
                'bookerInfo' => $bookerInfo,
                'searchedInfo' => $searchedInfo,
                'is_aa' => strrpos($host, 'supervacation'),
                'langId' => $langId,
                'hostName' => $hostName
            ], function ($message) use ($bookerInfo, $emails, $subject) {
                $message->to($bookerInfo->email)
                    ->bcc($emails)
                    ->subject($subject);
            });
            Log::info($this->ip . ' send suspicious order email ok');
            return response()->json([
                'success' => true
            ]);

        } catch (\Exception $e) {
            Log::error($this->ip . ' send suspicious order email failed, ' . $e->getMessage());
            return response()->json([
                'success' => false
            ]);
        }
    }

    /**
     * get fraud  detail info
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSmsAgain($id)
    {
        $result = Order::where('id', $id)
            ->first();
        $tempInfo = OrderTem::where('code', $result['code'])
            ->first();
        $booker = json_decode($tempInfo->booker, true);
        try {
            Log::info('for sms $booker info: ' . json_encode($booker));
            //For all new order, send sms
            $input_country_code = isset($booker['countryCodePhone']) ? $booker['countryCodePhone'] : $booker['phone_country'];
            $input_mobile = isset($booker['validPhoneNumber']) ? $booker['validPhoneNumber'] : $booker['phone'];
            $voucherUrl = SinaUrl::getShort($result->voucher());
            $this->sendSms($input_country_code, $input_mobile, $voucherUrl);
            return response()->json([
                'success' => true
            ]);
        } catch (Exception $e) {
            Log::error($this->ip . ' send sms again failed, ' . $e->getMessage());
            return response()->json([
                'success' => false
            ]);
        }
    }

    /**
     * @param $Reorderid
     * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function showUserFileView($Reorderid)
    {
        $sus_order = SuspiciousOrder::where('Reorderid', $Reorderid)
            ->get()
            ->last();
        $fileUrl = empty($sus_order->file_url) ? '' : $sus_order->file_url;
        if (empty($fileUrl)) {
            abort(404, "用户尚未上传文件");
            return false;
        } else {
            return redirect($fileUrl);
        }
    }

}
