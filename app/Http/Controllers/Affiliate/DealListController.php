<?php

namespace App\Http\Controllers\Affiliate;

use App\Model\Coupon;
use App\Traits\UploadFile;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

/**
 * Class DealListController
 * @package App\Http\Controllers\Affiliate
 */
class DealListController extends Controller
{
    use UploadFile;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::with(['couponHistory.hotel' => function($query) {
            $query->select(['hotelId', 'name', 'name_zh']);
        }])->get();

        $optionName = 'deal-list';

        return view('affiliate.deal-list', compact('coupons', 'optionName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $coupon = new Coupon($request->all());

        try {
            $coupon->save();

            return response()->json([
                'success' => true,
                'id' => $coupon->id
            ]);
        } catch (\Exception $e) {
            Log::error('Save coupon failed, ' . $e->getMessage());
            $message = 'Save coupon failed, ' . $e->getMessage();
        }

        return response()->json([
            'success' => false,
            'message' => $message
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $coupon = Coupon::find($id);

        if (!empty($coupon)) {
            $coupon->update($request->all());

            try {
                $coupon->save();

                return response()->json([
                    'success' => true,
                    'id' => $coupon->id
                ]);
            } catch (\Exception $e) {
                Log::error('Update coupon failed, ' . $e->getMessage());
                $message = "Failed to update coupon, " . $e->getMessage();
            }
        } else {
            $message = 'Coupon not found';
        }

        return response()->json([
            'success' => false,
            'message' => $message
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadImages(Request $request, $id)
    {
        try {
            $img_pc = $request->img_pc;
            $img_mobile = $request->img_mobile;

            $aws_folder_path = config('app.env') === 'local' ? 'coupon_img_test' : 'coupon_img';

            if ($img_pc && $img_mobile) {
                $this->uploadFile($img_pc, $aws_folder_path . '/coupon_' . $id . '_pc.png');
                $this->uploadFile($img_mobile, $aws_folder_path . '/coupon_' . $id . '_mobile.png');
            }

            return response()->json([
                'success' => true
            ]);
        } catch (\Exception $e) {
            Log::error('save coupon images failed, ' . $e->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'Save images failed, ' . $e->getMessage()
            ]);
        }

    }
}
