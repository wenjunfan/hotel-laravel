<?php

namespace App\Http\Controllers\Affiliate;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\HotelAPI;
use App\Model\User;
use App\Model\Hotel;

/**
 * Class PriceRatioController
 * @package App\Http\Controllers\Affiliate
 */
class PriceRatioController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $hotelSearchList =  Hotel::where('provider', 'EBooking')->select('hotelId', 'name')->get();
        $optionName = 'price-ratio';

        return view('affiliate.price-ratio', compact('hotelSearchList', 'optionName'));
    }

    /**
     * ALAX get hotels from Java API
     * @param \Illuminate\Http\Request $request
     * @return array|mixed
     */
    public function getPriceRatioHotels(Request $request)
    {
        $ids = $request->input('ids');
//        dd($request->all());
        // 获取 java api hotel ratio数据
        $api = new HotelAPI(auth()->user());
//        $ids = [1063212, 1049443, 1535796];
//        $ids = [1063212];
        $hotelList = $api->getHotelRatio($ids);
        unset($api);

        return $hotelList;

    }

    /**
     * ALAX post hotels from Java API
     * @param \Illuminate\Http\Request $request
     * @return array|mixed
     */
    public function updateHotelsPriceRatio(Request $request)
    {
        $ids = $request->input('ids');
        $update_data = $request->input('data_ratio');

        $api = new HotelAPI(auth()->user());
        $hotel = $api->postHotelRatio($ids, $update_data);

        return $hotel;

    }
}
