<?php

namespace App\Http\Controllers\Affiliate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;

use App\Model\OrderTem;

use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

/**
 * Class PotentialListController
 * @package App\Http\Controllers\Affiliate
 */
class PotentialListController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $isFollow = $request->input('isFollow');
        $isDone = $request->input('isDone');
        $email = $request->input('email');
        $fromUsitour = $request->input('fromUsitour');

        return $this->getData($isFollow, $isDone, $email,$fromUsitour);
    }

    /**
     * @param $isFollow
     * @param $isDone
     * @param $email
     * @param $fromUsitour
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getData($isFollow , $isDone, $email,$fromUsitour)
    {
        $certainTimeBefore = Carbon::now()->subSeconds(1200);
        $potentialLists =  OrderTem::with('linkedorderid')
            ->where('code', null)
            ->where('created_at', '<=' ,$certainTimeBefore)
            ->whereNotIn('client_ip', config('constants.innerIps'))  // ip白名单
            ->where('partnerId' ,'11087');

        if ($email != '') {
            $potentialLists = $potentialLists->where('user_email', $email);
        }

        if($fromUsitour != ''){
        if($fromUsitour === '1'){
            $potentialLists = $potentialLists->where('booker', 'like', '%usitour%');
        }else{
            $potentialLists = $potentialLists->where('booker', 'like', '%usitrip%');
            }
        }

        if ($isFollow != '') {
            $potentialLists = $potentialLists->where('isFollow', $isFollow);
        }

        if ($isDone != '') {
            $potentialLists = $potentialLists->where('isDone', $isDone);
        }

        $potentialLists = $potentialLists->orderBy('id', 'desc')
            ->offset(0)
            ->limit(300)
            ->get();

        $optionName = 'potential-lists';

        return view('affiliate.potential-lists', compact([
                'optionName',
                'email',
                'isFollow',
                'isDone',
                'count',
                'potentialLists',
                'fromUsitour',
            ])
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fixOrderPotentialList(Request $request)
    {
        $potentialdOrder = OrderTem::with('linkedorderid')->where('id', $request->input('id'))
            ->first();
        $oldCommend = $potentialdOrder->commend;
        $newCommend = $request->input('commend') . '&#013;' . $oldCommend;
        $potentialdOrder->commend = $newCommend;
        try {
            $potentialdOrder->commend = $newCommend;
            $potentialdOrder->save();
            return response()->json([
                'success' => true,
                'message' => 'updated!'
            ]);
        } catch (Exception $e) {
            Log::error('fix potential order list error: ' . $e->getMessage());
            return response()->json([
                'success' => false,
                'message' => 'comment potential order list failed!'
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function isDoneOrder(Request $request) {
        $potentialdOrder = OrderTem::with('linkedorderid')->where('id', $request->input('id'))->first();
        try {
            $potentialdOrder->isDone =$request->input('isDone');
            $potentialdOrder->update();
            return response()->json([
                'success' => true,
                'message' => 'updated!'
            ]);
        } catch (Exception $e) {
            Log::error('potential order isDone error: ' . $e->getMessage());
            return response()->json([
                'success' => false,
                'message' => 'potential order isDone failed'
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function isFollowedOrder(Request $request) {
        $potentialdOrder = OrderTem::with('linkedorderid')->where('id', $request->input('id'))
            ->first();
        try {
            $potentialdOrder->isFollow = $request->input('isFollow');
            $potentialdOrder->rep= $request->input('rep');
            $potentialdOrder->followed_at = Carbon::now()->toDateTimeString();//只有这个记时间， 其他的使用commend记录时间
            $potentialdOrder->save();
            return response()->json([
                'success' => true,
                'message' => 'updated!'
            ]);
        }
        catch(Exception $e) {
            Log::error('is followed order update error: ' . $e->getMessage());
            return response()->json([
                'success' => false,
                'message' => 'is followed order update failed',
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addLinkedOrderId(Request $request)
    {
        $linked_order_id = $request->input('linked_order_id');
        $sales_no = $request->input('sales_no');
        $id = $request->input('id');
        Log::info('sales ' .$sales_no . ' starts add linked id: '. $linked_order_id . 'for temp_order: ' .$id);

        $potentialdOrder = OrderTem::with('linkedorderid')->where('id' , $id)->first();
        $potentialdOrder->linked_order_id = $linked_order_id;
        $potentialdOrder->sales_no = $sales_no;
        $potentialdOrder->isDone = 1;

        try {
            $potentialdOrder->save();

            return response()->json([
                'success' => true,
                'message' => 'updated!'
            ]);
        }
        catch(Exception $e) {
           Log::error('add Linked Order Id error: ' . $e->getMessage());
            return response()->json([
                'success' => false,
                'message' => 'add Linked Order Id failed'
            ]);
        }
    }
}
