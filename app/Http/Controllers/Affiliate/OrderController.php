<?php

namespace App\Http\Controllers\Affiliate;

use App\Http\Controllers\Controller;
use App\Model\Distributor;
use App\Model\Partner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Model\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 *  分销商 订单列表
 * Class OrderController
 * @package App\Http\Controllers\Affiliate
 */
class OrderController extends Controller
{
    protected $orderLimit;
    protected $creationStart; // 下单 开始时间
    protected $creationEnd; // 下单 结束时间

    public function __construct()
    {
        parent::__construct();
        $this->orderLimit = (int)config('constants.order_limit');
        $this->creationStart = (new Carbon('America/Los_Angeles'))->startOfYear()->toDateTimeString();// 本年1月1日 "2018-01-01 10:56:53"
        $this->creationEnd = (new Carbon('America/Los_Angeles'))->endOfDay()->toDateTimeString();// 至今 "2018-05-21 10:56:53"
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $source
     * @return \Illuminate\Http\Request
     */
    public function index(Request $request, $source)
    {
        if ($request->has('from')) {
            $createdFrom = $request->input('from') . ' 00:00:00';
        } else {
            $createdFrom = $this->creationStart;
        }
        if ($request->has('to')) {
            $createdTo = $request->input('to') . ' 23:59:59';
        } else {
            $createdTo = $this->creationEnd;
        }

        $checkinFrom = $request->input('cfrom');
        $checkinTo = $request->input('cto');
        $sales_no = $request->input('sales_no');
        $orderStatus = $request->input('orderStatus');

        return $this->getData($source, $createdFrom, $createdTo, $checkinFrom, $checkinTo, $sales_no, $orderStatus);
    }

    /**
     * 说明：
     * @param string $source : 分销商
     * @param string $createdFrom : created_at
     * @param string $createdTo : created_at
     * @param string $checkinFrom : checkinDate
     * @param string $checkinTo : checkinDate
     * @param string $sales_no // 销售号
     * @param string $orderStatus
     * @return Request
     */
    public function getData($source, $createdFrom = '', $createdTo = '', $checkinFrom = '', $checkinTo = '', $sales_no = '', $orderStatus = '')
    {

        if(!in_array(Auth::user()->email, config('constants.marketing'), true) && (Auth::user()->admin === 4 || Auth::user()->admin === 5 )){
            $distributor = Distributor::where('partner_id', Auth::user()->id)->get()->first();
            $real = $distributor->distributor_code;
            Log::info('distributor_source' .$real);
            //检查 非管理员或者财务source和登录的distributor的source是不是match
            if($source !== $real && Auth::user()->email !== config('constants.marketing')){
                abort('404' , 'No Access!');
            }
        }

        $orders = Order::with('distributor', 'rooms', 'orderTem');
        $sum_able_cash = 0;
        $sum_unpaid = 0;
        $able_cash_out_amount = 0;
        $able_cash_out = false;
        $isMobile = false;
        if (Auth::check() && Auth::user()->admin == 4 && \Agent::isMobile()) {
            $isMobile = true;
        }
        if ( !empty($sales_no) && $source != 'All117BookAffiliatePartner' ) {
            $orders = $orders->where('sales_no', $sales_no)
                ->where('source', $source);
        } elseif ($source == 'All117BookAffiliatePartner' &&  !empty($sales_no)) {
            $orders = $orders->where('sales_no', $sales_no);
        } elseif ($source !== 'All117BookAffiliatePartner' && empty($sales_no)) {
            $orders = $orders->where('source', $source);
            $distributor = Distributor::with('partner')->where('distributor_code', $source)->get()->first();
            try {
                $orderTable = Order::with('distributor', 'rooms', 'orderTem')->where('source', $source);
                //start save info for affiliate user cashed sum and unpaid sum, also pass to db
                $sum_allConfirmed = $orderTable->where('status', 'CONFIRMED')->sum('totalPrice');
                $sum_allConfirmed_avaliable_to_pay = $orderTable->where('status', 'CONFIRMED')->where('checkoutDate', '<', Carbon::now())->sum('totalPrice');
                $sum_paid = $orderTable->where('isAffiliated', 1)->sum('totalPrice');
                $sum_unpaid = floor(($sum_allConfirmed - $sum_paid) * (1 - config('constants.tax_rate')));
                session([
                    'affiliate_balance_total' => $sum_allConfirmed,
                    'affiliate_balance_paid' => $sum_paid,
                    'affiliate_balance_unpaid' => $sum_unpaid
                ]);
                $sum_able_cash = floor($sum_unpaid * $distributor->partner->affiliateRate);
                $able_cash_out_amount = floor(($sum_allConfirmed_avaliable_to_pay * $distributor->partner->affiliateRate) * (1 - config('constants.tax_rate')));
                $able_cash_out = (int)$able_cash_out_amount >= 50 ? true : false;
                $distributor->total_contributed = $sum_allConfirmed;
                $distributor->total_cashed = $sum_paid;
                $distributor->total_unpaid = $sum_unpaid;
                $distributor->update();
                Log::info("Auth user " . $distributor->id . " update total balance" . $sum_allConfirmed . " update paid balance" . $sum_paid . ", unpaid balance" . $sum_unpaid);
            } catch (\Exception $e) {
                Log::error('failed to update paid and unpaid for Affiliate user: ' . $distributor->id . 'reason: ' . $e->getMessage());
            }
        } else {
            $orders->where(function($query){
                $query->where('sales_no', '<>', '')
                    ->orWhere('source','<>', '');
            });
        }

        // 订单开始时间
        if ($createdFrom != '') {
            $orders = $orders->where('created_at', '>=', $createdFrom. ' 00:00:00');
        }

        // 订单结束时间
        if ($createdTo != '') {
            $orders = $orders->where('created_at', '<=', $createdTo. ' 23:59:59');
        }

        if ($checkinFrom != '') {
            $orders = $orders->where('checkinDate', '>=', $checkinFrom);
        }

        if ($checkinTo != '') {
            $orders = $orders->where('checkinDate', '<=', $checkinTo);
        }

        if ($orderStatus != "") {
            switch ($orderStatus) {
                case 1:
                    $orders = $orders->where('status', 'CONFIRMED')
                        ->where('isAffiliated', '1');
                    break;
                case 2:
                    $orders = $orders->where('status', 'CONFIRMED')
                        ->where('isAffiliated', '0');
                    break;
                case 3:
                    $orders = $orders->where('status', '<>', 'CONFIRMED');
                    break;
                default:
            }
        } else {
            // 默认 显示全部订单
            $orderStatus = 4;
        }

        $orders = $orders->orderBy('id', 'desc')
            ->offset(0)
            ->limit($this->orderLimit)
            ->get();

        $optionName = 'orders';
        $select_value = $orderStatus;
        $count = $orders->count(); // 返回数量，max: orderLimit
        $from = $createdFrom;
        $to = $createdTo;
        $cfrom = $checkinFrom;
        $cto = $checkinTo;
        $distributors = Distributor::with('partner')->get();
        $sum_able_cash = $sum_able_cash;
        $sum_unpaid = $sum_unpaid;
        $able_cash_out = $able_cash_out;
        $able_cash_out_amount = $able_cash_out_amount;
        $isMobile = $isMobile;

        return view('affiliate.orders', compact([
                'optionName',
                'orders',
                'source',
                'from',
                'to',
                'cfrom',
                'cto',
                'select_value',
                'sales_no',
                'count',
                'distributors',
                'sum_unpaid',
                'sum_able_cash',
                'able_cash_out',
                'able_cash_out_amount',
                'isMobile',
            ])
        );
    }

    public function cashing($amount){
    
        $user = Auth::user()->name;
        $data = $user. '请求兑现分销佣金' . $amount.'请财务联系用户('.  Auth::user()->email .')兑现方式';
        if($amount < 50){
            return response()->json([
                'success' => false,
                'message' => 'amount lower than 50, not valid',
            ]);
        }
        try {
            Mail::raw($data, function ($message) use ($user, $amount) {
                $message->subject($user . '请求兑现分销佣金!');
                $message->to(config('staff-emails.acco'));
                $message->bcc(config('staff-emails.ann'));
            });
            Log::info($this->ip . ' 请求兑现分销佣金');
            return response()->json([
                'success' => true,
            ]);
        } catch (Exception $e) {
            Log::error($this->ip . ' error with sending 请求兑现分销佣金 ' . $e->getMessage());
            return response()->json([
                'success' => false,
            ]);
        }
    }

}
