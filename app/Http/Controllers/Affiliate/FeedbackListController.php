<?php

namespace App\Http\Controllers\Affiliate;

use App\Model\UserFeedback;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

/**
 * Class FeedbackListController
 * @package App\Http\Controllers\Affiliate
 */
class FeedbackListController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $feedback = UserFeedback::all();
        $optionName = 'feedback-list';

        return view('affiliate.feedback-list', compact('feedback', 'optionName'));
    }
}
