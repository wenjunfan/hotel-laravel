<?php

namespace App\Http\Controllers\Affiliate;

use App\Http\Controllers\Controller;
use App\Model\Distributor;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
/**
 * Class ProfileController
 * @package App\Http\Controllers\Affiliate
 */
class ProfileController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profileView ()
    {
        return view('affiliate.profile', [
            'optionName' => 'profile'
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAvatar(Request $request)
    {
        $user = Auth::user();

        if ($user && $request->has('avatar')) {
            $user->avatar = $request->input('avatar');
            $user->save();

            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'fail', 'message' => 'Please login first']);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateProfile(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            return redirect('affiliate/profile')->with('message', 'Please Login first');
        }

        $rules = [
            'name'           => 'required',
            'contact_person' => 'required|min:2',
            'mobile_phone'   => 'required|min:3',
            'company_address' => 'required|min:3',
        ];

        if ($user->email != $request->input('email')) {
            $rules['email'] = 'email|unique:users';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('affiliate/profile')
                ->withErrors($validator)
                ->withInput();
        }

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->company_address = $request->input('company_address');
        $user->contact_person = $request->input('contact_person');
        $user->mobile_phone = $request->input('mobile_phone');
        // 用户填满了信息，可以激活用户了
        if($user->company_address !== '' && $user->contact_person!== '' &&$user->mobile_phone!== '' && $user->admin === 5){
            $id = $user->id;
            try {
                Mail::raw($id . '填满了信息,可以激活', function ($message) use ($id) {
                    $message->to(config('staff-emails.ann'))
                        ->subject('ok active affiliate user: ' .   $id );
                });
                Log::info("send ok active affiliate user email successfully");
            } catch (\Exception $e) {
                Log::error('send ok active affiliate user email failed: ' . $e->getMessage());
            }
        }

        $user->save();

        if($user->admin == 5 || $user->admin == 4){
            $distributor = Distributor::where('partner_id', $user->id)->first();
            if(!empty($distributor) && $distributor->distributor_name !== $request->input('name')){
                Log::info($distributor->id . ' has distributor name change request, from '.$distributor->distributor_name . ' to ' . $request->input('name'));
                try{
                    $distributor->distributor_name = $request->input('name');
                    $distributor->save();
                }catch(\Exception $e){
                   Log::error('Save distributor name error: '. $e->getMessage());
                }
            }
        }

        return redirect('affiliate/profile')->with('message', 'Success Update Profile');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function welcome(Request $request)
    {
        if(Auth::user()->admin === 4 ) {
            return view('affiliate/welcome-affiliate');
        }else{
            return redirect('affiliate/profile');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function resetPassword(Request $request)
    {
        Auth::logout();

        return redirect('affiliate/password/reset');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function guideView()
    {
        return view('guide', ['optionName' => 'guide']);
    }
}
