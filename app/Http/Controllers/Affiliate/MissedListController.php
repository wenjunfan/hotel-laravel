<?php

namespace App\Http\Controllers\Affiliate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\OrderTem;
use App\Classes\CitconPay;
use Auth;
use Illuminate\Support\Facades\Log;

use Carbon\Carbon;

/**
 * Class MissedListController
 * @package App\Http\Controllers\Affiliate
 */
class MissedListController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // TODO query需要优化
        $aa_host = 'supervacation';
        if (Auth::user()->id == config('app.aa_partner_id')) {
            $missedLists = OrderTem::where(function ($query) {
                $query->whereIn('paid_not_booked', [1, 4])
                    ->where('partnerId', 11676)
                    ->whereNull('orderReference');
            })
                ->orWhere(function ($query) {
                    $query->whereIn('paid_not_booked', [2, 3])
                        ->where('partnerId', 11676);
                })
                ->with('order')
                ->orderBy('id', 'desc')
                ->get();

        } else {
            $missedLists = OrderTem::where(function ($query) {
                $query->whereIn('paid_not_booked', [1, 4])
                    ->where('partnerId', 11087)
                    ->whereNull('orderReference');
            })
                ->orWhere(function ($query) {
                    $query->whereIn('paid_not_booked', [2, 3])
                        ->where('partnerId', 11087);
                })
                ->with('order')
                ->orderBy('id', 'desc')
                ->get();

        }

        return view('affiliate.missed-lists', [
            'optionName' => 'missed-lists',
            'missedLists' => $missedLists,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fixOrderMissList(Request $request)
    {
        $missedOrderList = OrderTem::where('id', $request->input('id'))
            ->first();
        $oldCommend = $missedOrderList->commend;
        $newCommend = $request->input('commend') . '&#013;' . $oldCommend;
        $missedOrderList->commend = $newCommend;
        try {
            $missedOrderList->commend = $newCommend;
            $missedOrderList->save();
            return response()->json([
                'success' => true, 'message' => 'updated!'
            ]);
        } catch (\Exception $e) {
            log::error('fix missed order list error: ' . $e->getMessage());
            return response()->json([
                'success' => false,
            ]);
        }
    }

    /**
     * @param $Reorderid
     * @return \Illuminate\Http\JsonResponse
     */
    public function statusCitconPmt($Reorderid)
    {
        $transaction = CitconPay::inquire($Reorderid);

        if ($transaction->type === 'charge' && $transaction->status === 'success') {
            return response()->json([
                'success' => true,
                'data' => $transaction->type
            ]);
        } else {
            //如果非可继续下单的，直接移除missed order
            try {
                $missedOrderList = OrderTem::where('Reorderid', $Reorderid)
                    ->first();
                $missedOrderList->paid_not_booked = 0;
                $missedOrderList->update();
            } catch (\Exception $e) {
                Log::error($this->ip . ' update paid not booked failed' . $e->getMessage());
            }

            return response()->json([
                'success' => false,
                'data' => 'Not Paid'
            ]);
        }
    }
}
