<?php

namespace App\Http\Controllers\Affiliate;

use App\Http\Controllers\Controller;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Affiliate
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function index()
	{
		return view('affiliate.dashboard', [
			'optionName'   => 'dashboard',
		]);
	}
}
