<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Exception;
use App\Model\Feedback;
use App\Model\Order;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;

/**
 * Class FeedbackController
 * @package App\Http\Controllers\Admin
 */
class FeedbackController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $feedbacks = Feedback::with('order', 'order.partner')->get();

        return view('admin.feedbacks', [
            'optionName' => 'feedbacks',
            'feedbacks' => $feedbacks,
            'select_value' => 'all',
            'keywords' => '',
            'from' => '',
            'to' => '',
            'provider' => 'all'
        ]);
    }

    /**
     * @param $provider
     * @param $processStatus
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexnn($provider = all, $processStatus = all)
    {
        return $this->betweennn($provider,'', '', $processStatus);
    }

    /**
     * @param $provider
     * @param $from
     * @param $to
     * @param $processStatus
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function betweennn($provider, $from, $to, $processStatus)
    {
        if ($provider != 'all') {
            $feedbacks = Feedback::whereHas('order', function ($q) use ($provider) {
                $q->where('provider', '=', $provider);
            })
                ->with('order');
        } else {
            $feedbacks = Feedback::with('order', 'order.partner');
        }

        if ($from != '') {
            $feedbacks = $feedbacks->where('created_at', '>=', $from);
        }

        if ($to != '') {
            $feedbacks = $feedbacks->where('created_at', '<=', $to);
        }

        if ($processStatus != 'all') {
            $feedbacks = $feedbacks->where('isDone', $processStatus);

        }

        $feedbacks = $feedbacks->orderBy('porder_id', 'desc')
            ->get();
        $select_value = $processStatus;

        return view('admin.feedbacks', [
            'optionName'   => 'feedbacks',
            'feedbacks'    => $feedbacks,
            'from'         => $from,
            'to'           => $to,
            'select_value' => $select_value,
            'keywords'     => '',
            'provider'     => $provider,
        ]);
    }

    /**
     * @param $provider
     * @param $from
     * @param $processStatus
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fromnn( $provider, $from, $processStatus)
    {
        return $this->betweennn($provider, $from, '', $processStatus);
    }

    /**
     * @param $provider
     * @param $to
     * @param $processStatus
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tonn($provider, $to, $processStatus)
    {
        return $this->betweennn($provider, '', $to, $processStatus);
    }

    /**
     * @param $orderId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ordernn($orderId)
    {
        $feedbacks = Feedback::where('porder_id', $orderId)->get();
        $provider = "all";
        $from = "";
        $to = "";
        $select_value = 'all';

        return view('admin.feedbacks', [
            'optionName' => 'feedbacks',
            'feedbacks' => $feedbacks,
            'from' => $from,
            'to' => $to,
            'select_value' => $select_value,
            'keywords' => $orderId,
            'provider' => $provider,
        ]);

    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fixFeedback(Request $request) {
        $orderId = $request->input('orderId');
        $feedback = Feedback::where('porder_id', $orderId)->first();

        if ($feedback) {
            $oldNotes = $feedback->solution;
            $newNotes = $request->input('solution') . '&#013;'. $oldNotes;
            $feedback->fill($request->all());
            $feedback->solution = $newNotes;
        } else {
            $feedback = new Feedback;
            $feedback->porder_id = $orderId;
            $feedback->fill($request->all());
        }

        try {
            $feedback->save();

            return response()->json([
                'success' => true,
                'message' => 'updated!'
            ]);
        }
        catch(Exception $e) {
            Log::error('fix feedback error: ' . $e->getMessage());
            return response()->json([
                'success' => false,
                'message' => 'Not updated'
            ]);
        }
    }

    /**
     * Feedback find orderId
     * @param $orderId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrder($orderId) {
        $order = Order::find($orderId);

        if ($order) {
            return response()->json($order);
        } else {
            return response()->json([]);
        }
    }
}
