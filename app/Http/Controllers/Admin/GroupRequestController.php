<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Http\Controllers\Controller;
use App\Model\GroupRequest;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;

/**
 * Class groupRequestController
 * @package App\Http\Controllers\Admin
 */
class groupRequestController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $groupRequests = GroupRequest::where('destination_one', '<>' ,'')
            ->get();

        return view('admin.group-request', [
            'optionName'    => 'groupRequests',
            'groupRequests' => $groupRequests,
            'select_value'  => 'all',
            'isFollow' => 'all',
            'from'          => '',
            'to'            => ''
        ]);
    }

    /**
     * @param $isFollow
     * @param $processStatus
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexn($isFollow = all, $processStatus = all)
    {
        return $this->between($isFollow, '', '', $processStatus);
    }

    /**
     * @param $isFollow
     * @param $from
     * @param $to
     * @param $processStatus
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function between($isFollow, $from, $to, $processStatus)
    {
        if ($isFollow != 'all') {
            $groupRequests = GroupRequest::where('isFollow', '=', $isFollow);
        } else {
            $groupRequests = GroupRequest::where('id','<>','');
        }
        if ($from != '') {
            $groupRequests = $groupRequests->where('created_at', '>=', $from);
        }

        if ($to != '') {
            $groupRequests = $groupRequests->where('created_at', '<=', $to);
        }

        if ($processStatus != 'all') {
            $groupRequests = $groupRequests->where('isDone', $processStatus);
        }

        $groupRequests = $groupRequests->orderBy('id', 'desc')
            ->get();

        $select_value = $processStatus;

        return view('admin.group-request', [
            'optionName'    => 'groupRequests',
            'groupRequests' => $groupRequests,
            'from'          => $from,
            'to'            => $to,
            'select_value' => $select_value,
            'isFollow' => $isFollow,
        ]);
    }

    /**
     * @param $isFollow
     * @param $from
     * @param $processStatus
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function from($isFollow, $from, $processStatus)
    {
        return $this->between($isFollow, $from, '', $processStatus);
    }

    /**
     * @param $isFollow
     * @param $to
     * @param $processStatus
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function to($isFollow, $to, $processStatus)
    {
        return $this->between($isFollow, '', $to, $processStatus);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fixGroupRequest(Request $request)
    {
        $groupRequest = GroupRequest::where('id', $request->input('id'))
            ->first();
        $oldSolutions = $groupRequest->solution;
        $newSolutions = $request->input('solution') . '&#013;' . $oldSolutions;
        $amount = $request->input('amount');
        $groupRequest->solution = $newSolutions;
        $groupRequest->amount = $amount;
        try {
            $groupRequest->update();

            return response()->json([
                'success' => true,
                'message' => 'updated!'
            ]);
        } catch (Exception $e) {
            Log::error('comment group request error: ' . $e->getMessage());
            return response()->json([
                'success' => false,
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function isDoneOrder(Request $request)
    {
        GroupRequest::where('id', $request->input('id'))
            ->update(['isDone' => $request->input('isDone')]);

        return response()->json([
            'success' => true,
            'message' => 'updated!'
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function isFollowedOrder(Request $request)
    {
        $groupRequest = GroupRequest::where('id', $request->input('id'))
            ->first();

        $isFollow = $request->input('isFollow');
        $rep = $request->input('rep');

        $isFollow = mb_substr($isFollow, 0, 1);
        $rep = mb_substr($rep, 0, 10);

        $groupRequest->isFollow = $isFollow;
        $groupRequest->rep = $rep;
        try {
            $groupRequest->update();

            return response()->json([
                'success' => true,
                'message' => 'updated!'
            ]);
        } catch (Exception $e) {
            Log::error('is followed order update error: ' . $e->getMessage());
            return response()->json([
                'success' => false,
            ]);
        }
    }

}
