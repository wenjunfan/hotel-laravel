<?php

namespace App\Http\Controllers\Admin;

use Exception;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\OrderTem;
use App\Classes\CitconPay;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;

/**
 * Class MissedListController
 * @package App\Http\Controllers\Admin
 */
class MissedListController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //只有117和usitrip有
        $CookieName = "a";    // Cookie's name
        $CookieValue = isset($_COOKIE['a']) ? $_COOKIE['a'] : '19'; // Cookie's value 19 不分钱
        $CookieDirectory = "/";        // Cookie directory ("/" for all directories)
        $DaysCookieShallLast = 31;     // Days before expiration (decimal number okay.)

        $CookieDomain = '.' . preg_replace('/^hotel\./', '', $_SERVER{'HTTP_HOST'});
        $CookieDomain = preg_replace('/:\d+$/', '', $CookieDomain);
        $lasting = ($DaysCookieShallLast <= 0) ? "" : time() + ($DaysCookieShallLast * 24 * 60 * 60);

        setcookie($CookieName, $CookieValue, $lasting, $CookieDirectory, $CookieDomain);

        $missedLists = OrderTem::where(function ($query) {
            $query->whereIn('paid_not_booked', [1, 4])
                ->whereNotIn('partnerId', [11087, 11676])
                ->whereNull('orderReference');
        })
            ->orWhere(function ($query) {
                $query->whereNotIn('partnerId', [11087, 11676])
                    ->whereIn('paid_not_booked', [2, 3]);
            })
            ->with('partner', 'order')
            ->orderBy('id', 'desc')
            ->get();

        return view('admin.missed-lists', [
            'optionName' => 'missed-lists', 'missedLists' => $missedLists, 'CookieValue' => $CookieValue,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fix117bookMissList(Request $request)
    {
        $missedOrderList = OrderTem::where('id', $request->input('id'))
            ->first();
        $oldCommend = $missedOrderList->commend;
        $newCommend = $request->input('commend') . '&#013;' . $oldCommend;
        $missedOrderList->commend = $newCommend;
        try {
            $missedOrderList->commend = $newCommend;
            $missedOrderList->save();
            return response()->json([
                'success' => true, 'message' => 'updated!'
            ]);
        } catch (Exception $e) {
            Log::error('fix missed order list error: ' . $e->getMessage());
            return response()->json([
                'success' => false, 'message' => 'fix missed order list'
            ]);
        }
    }

    /**
     * @param $Reorderid
     * @return \Illuminate\Http\JsonResponse
     */
    public function declineMissedOrder2B($Reorderid)//citcon pay decline link and send failed email to user
    {
        $missed_order = OrderTem::where('Reorderid', $Reorderid)
            ->get()
            ->last();
        $sales_no = isset($_COOKIE['a']) ? $_COOKIE['a'] : '';
        if ($missed_order->paid_not_booked == 1 && $missed_order->orderReference == null) {
            $missed_order->paid_not_booked = 3;//0 is default, 1 is paid not booked, 2 is solved paid not booked，3 is declined
            $missed_order->sales_no = $sales_no;
            $missed_order->save();

            $this->sendFailedEmail2b($missed_order, $Reorderid);

            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false, 'message' => '该订单已经被完成，无法拒绝'
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateNewCitconAmount2B(Request $request)
    {
        $missed_order = OrderTem::where('id', $request->id)
            ->get()
            ->last();
        $missed_order->amount = $request->new_amount_confirmed;
        $missed_order->commend = $request->new_commend;
        $missed_order->save();
        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * @param $missed_order
     * @param $Reorderid
     * @return \Illuminate\Http\JsonResponse
     */
    private function sendFailedEmail2b($missed_order, $Reorderid)
    {
        //citcon refund -- Ann 03052018
        $transactionRef = $missed_order->Reorderid;
        $transaction = CitconPay::inquire($transactionRef);
        $reason = 'missed order decline refund';

        CitconPay::refund($transaction->currency, $transaction->id, $transaction->amount, $reason);
        //email user and accountant Ann book failed
        $locale = session('language', 0);

        $booker = $missed_order['booker'];
        $bookerArray = json_decode($booker, true);
        $bookerEmail = $bookerArray['email'];
        if ($locale == 0) {
            $fraudEmailContent = 'emails.117book_order_failed';
            $fraudEmailSubject = '117book- 酒店订单失败： ' . $Reorderid;
        } else {
            $fraudEmailContent = 'emails.117book_order_failed_en';
            $fraudEmailSubject = '117book- Your Hotel Order is been Rejected: ' . $Reorderid;
        }
        try {
            Mail::send($fraudEmailContent, ['Reorderid' => $Reorderid,], function ($message) use ($fraudEmailSubject, $bookerEmail) {
                $message->subject($fraudEmailSubject);
                $message->to($bookerEmail);
                $message->bcc(config('staff-emails.ann'));
            });

            return response()->json([
                'success' => true,
            ]);
        } catch (Exception $e) {
            Log::error($this->ip . ' send fraud email failed' . $e->getMessage());

            return response()->json([
                'success' => false,
            ]);
        }
    }

    /**
     * @param $Reorderid
     * @return \Illuminate\Http\JsonResponse
     */
    public function statusCitconPmt2B($Reorderid)
    {
        $transaction = CitconPay::inquire($Reorderid);
        $transactionArray = (array)$transaction;
        if (($transactionArray['type']) == 'refund') {
            //如果是退款的，直接移除missed order
            try {
                $missedOrderList = OrderTem::where('Reorderid', $Reorderid)
                    ->first();
                $missedOrderList->paid_not_booked = 0;
                $missedOrderList->update();
            } catch (Exception $e) {
                Log::error($this->ip . '  update paid not bbooked failed' . $e->getMessage());
            }
        }
        if (json_encode($transaction) == null) {
            return response()->json([
                'success' => false,
            ]);
        } else {
            return response()->json([
                'success' => true, 'data' => $transactionArray['type']
            ]);
        }
    }
}
