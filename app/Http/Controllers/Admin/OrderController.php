<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\Partner;
use App\Model\OrderPax;
use App\Model\User;
use App\Model\OrderCtrip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;

/**
 * Class OrderController
 * @package App\Http\Controllers\Admin
 */
class OrderController extends Controller
{
    protected $creationStart; // 订单创建 开始时间
    protected $creationEnd; // 订单创建 结束时间
    protected $checkinStart; // 入住 开始日期
    protected $checkinEnd; // 入住 结束日期
    protected $orderLimit; //最多显示多少条

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->checkinStart = (new Carbon('America/Los_Angeles'))->endOfDay()
            ->toDateTimeString();
        $this->checkinEnd = (new Carbon('America/Los_Angeles'))->addWeek(1)
            ->toDateTimeString(); // 接下来一周入住的订单的订单 "yyyy-mm-dd hh:mm:ss"// op查看最多的filter 方式
        $this->orderLimit = (int)config('constants.order_limit');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $createdFrom = $request->input('from');
        $createdTo = $request->input('to');
        $checkinFrom = $request->input('checkin_from');
        $checkinTo = $request->input('checkin_to');

        if ($request->has('from')) {
            $createdFrom = $createdFrom . ' 00:00:00';
        } else {
            $createdFrom = '';
        }
        if ($request->has('to')) {
            $createdTo = $createdTo . ' 23:59:59';
        } else {
            $createdTo = '';
        }
        // init createdFrom and createTo
        if ($request->has('checkin_from')) {
            if (strpos($checkinFrom, '00:00:00') === false) {
                $checkinFrom = $checkinFrom . ' 00:00:00';
            }
        } else {
            $checkinFrom = substr($this->checkinStart, 0, 10) . ' 00:00:00';
        }
        if ($request->has('checkin_to')) {
            if (strpos($checkinTo, ' 23:59:59') === false) {
                $checkinTo = $checkinTo . ' 23:59:59';
            }
        } else {
            $checkinTo = substr($this->checkinEnd, 0, 10) . ' 23:59:59';
        }

        //财务审核状态
        if ($request->has('financeStatus')) {
            $financeStatus = $request->input('financeStatus');
        } else {
            $financeStatus = 5;
        }

        // 订单状态
        if ($request->has('orderStatus')) {
            $orderStatus = $request->input('orderStatus');
        } else {
            $orderStatus = 6;
        }

        //訂單號
        if ($request->has('keywords')) {
            $keywords = $request->input('keywords');
        } else {
            $keywords = '';
        }

        //分銷商id
        if ($request->has('partnerId')) {
            $partnerId = $request->input('partnerId');
        } else {
            $partnerId = '';
        }

        //酒店名稱
        if ($request->has('hotelName')) {
            $hotelName = $request->input('hotelName');
        } else {
            $hotelName = '';
        }

        //酒店入住人姓名
        $guestName = $request->has('guestName') ? $request->input('guestName') : '';
        //上游確認號
        $orderPReference = $request->has('orderPReference') ? $request->input('orderPReference') : '';

        return $this->getData($partnerId, $createdFrom, $createdTo, $checkinFrom, $checkinTo, $keywords, $financeStatus,
            $orderStatus, $hotelName, $guestName, $orderPReference);
    }

    /**
     * @param string $partnerId
     * @param string $createdFrom
     * @param string $createdTo
     * @param string $checkinFrom
     * @param string $checkinTo
     * @param string $keywords
     * @param int $orderStatus
     * @param int $financeStatus
     * @param string $hotelName
     * @param string $guestName
     * @param string $orderPReference
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getData($partnerId = '', $createdFrom = '', $createdTo = '', $checkinFrom = '', $checkinTo = '', $keywords = '', $financeStatus = 5, $orderStatus = 6, $hotelName = '', $guestName = '', $orderPReference = '')
    {
        $users = User::all();
        $orders = Order::with('payment.user', 'rooms.paxes', 'user', 'orderTem');

        if ($keywords) {
            $orderPReference = $partnerId = $createdFrom = $createdTo = $checkinFrom = $checkinTo = $hotelName = $guestName = "";
            $orderStatus = 6;
            $financeStatus = 5;
            $orders = $orders->where('id', $keywords);
        } elseif ($orderPReference != '') {
            // orderPReference 上游提供的订单号，可能多个号合并在一起, 需要模糊查询
            $keywords = $partnerId = $createdFrom = $createdTo = $checkinFrom = $checkinTo = $hotelName = $guestName = "";
            $orderStatus = 6;
            $financeStatus = 5;
            $orders = $orders->where('orderPReference', 'like', '%' . $orderPReference . '%');
        } else {
            if ($partnerId != '') {
                $orders = $orders->where('partnerId', $partnerId);
            }
            if ($hotelName != '') {
                $orders->where('hotelName', 'like', '%' . $hotelName . '%');
            }
            if ($guestName != '') {
                $orders->whereHas('rooms.paxes', function ($query) use ($guestName) {
                    $query->where('firstName', 'like', '%' . $guestName . '%')
                        ->orWhere('lastName', 'like', '%' . $guestName . '%');
                });
            }
            if ($createdFrom != '') {
                if (strpos($createdFrom, '00:00:00') === false) {
                    $createdFrom = $createdFrom . ' 00:00:00';
                }
                $orders = $orders->where('created_at', '>=', $createdFrom);
            }
            if ($createdTo != '') {
                if (strpos($createdTo, '23:59:59') === false) {
                    $createdTo = $createdTo . ' 23:59:59';
                }
                $orders = $orders->where('created_at', '<=', $createdTo);
            }

            if ($checkinFrom != '') {
                if (strpos($checkinFrom, '00:00:00') === false) {
                    $checkinFrom = $checkinFrom . ' 00:00:00';
                }
                $orders = $orders->where('checkinDate', '>=', $checkinFrom);
            }

            if ($checkinTo != '') {
                if (strpos($checkinTo, '23:59:59') === false) {
                    $checkinTo = $checkinTo . ' 23:59:59';
                }
                $orders = $orders->where('checkinDate', '<=', $checkinTo);
            }

            if ($financeStatus == 1) {// 已审
                $orders = $orders->where('isChecked', 1)
                    ->where('status', 'CONFIRMED');
            } elseif ($financeStatus == 2) {// 未审
                $orders = $orders->where('isChecked', 0)
                    ->where('status', 'CONFIRMED');
            } elseif ($financeStatus == 3) {//已退
                $orders = $orders->where('isReturned', 1)
                    ->where('status', '<>', 'CONFIRMED');
            } elseif ($financeStatus == 4) {//需退
                $orders = $orders->where('payment_type', 'PO')
                    ->where('isChecked', 1)
                    ->where('isReturned', '<>', 1)
                    ->where('status', '<>', 'CONFIRMED');
            }

            if ($orderStatus == 1) {
                $orders = $orders->where('status', 'ON REQUEST');
            } elseif ($orderStatus == 2) {
                $orders = $orders->where('status', 'CONFIRMED');
            } elseif ($orderStatus == 3) {
                $orders = $orders->where('payment_status', 'PENDING');
                $orders = $orders->where('status', 'CANCELLED');
            } elseif ($orderStatus == 4) {
                $orders = $orders->where('payment_status', '<>', 'PENDING');
                $orders = $orders->where('status', 'CANCELLED');
            } elseif ($orderStatus == 7) {
                $orders = $orders->where('status', 'REJECTED');
            } elseif ($orderStatus == 5) {
                $orders = $orders->where('status', 'FAILED');
            }
        }

        $orders = $orders->orderBy('id', 'desc')->with(['requestlog'])
            ->offset(0)
            ->limit($this->orderLimit)
            ->get();

        $select_value = $orderStatus;

        return view('admin.orders', [
            'optionName' => 'orders',
            'partnerId' => $partnerId,
            'orderPReference' => $orderPReference,
            'users' => $users,
            'orders' => $orders,
            'from' => $createdFrom,
            'to' => $createdTo,
            'checkin_from' => $checkinFrom,
            'checkin_to' => $checkinTo,
            'keywords' => $keywords,
            'financeStatus' => $financeStatus,
            'hotelName' => $hotelName,
            'guestName' => $guestName,
            'select_value' => $select_value
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPartnerInfo($id)
    {
        $partner = Partner::where('id', $id)
            ->first();
        return response()->json([
            'partner' => $partner
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGuestName($id)
    {
        $order = Order::with('paxes')
            ->where('id', $id)
            ->first();
        $guests = $order->paxes;
        return response()->json([
            'guests' => $guests
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveGuestName(Request $request)
    {
        $order = Order::where('id', $request->input('id'))
            ->first();
        $guests = OrderPax::where('orderReference', $order->orderReference)
            ->get();

        try {
            for ($i = 0; $i < count($guests); $i++) {
                $guests[$i]->firstName = $request->input($i . '_fn');
                $guests[$i]->lastName = $request->input($i . '_ln');
                $guests[$i]->save();
            }
            $emailto = $order->bookerEmail;
            $hostName = strpos($order->http_host, 'Usitour') !== false ? 'Usitour' : strpos($order->http_host, '117book') !== false ? '117Book' : 'Usitrip';

            $langId = 0;
            $subject = '尊敬的客户，您的订单' . $order->id . '入住人姓名已更改成功 -' . $hostName;
            if ($order->language == 1) {
                $langId = 1;
                $subject = 'Dear Customer, your order ' . $order->id . ' has successfully amend the guest name - ' . $hostName;
            } elseif ($order->language == 2) {
                $langId = 2;
                $subject = '尊敬的客戶,您的飯店訂單' . $order->id . ' 已成功修改入住人姓名 - ' . $hostName;
            }

            if ($order->partnerId == 10021) {
                $ctripid = OrderCtrip::where('orderReference', $order->orderReference)->first()->ctrip_reservation_number;
                $subject = '尊敬的携程，您的订单' . $ctripid . '入住人姓名已更改成功 - ' . $hostName;
            }

            try {
                Mail::queue('emails.name-change', [
                    'id' => $order->id,
                    'url' => $order->voucher(),
                    'langId' => $langId,
                    'hostName' => $hostName
                ], function ($message) use ($subject, $emailto, $hostName) {
                    $message->subject($subject)
                        ->to($emailto)
                        ->bcc(config('app.env') == 'production' ? config('staff-emails.annbdcn') : config('staff-emails.ann'));
                });
            } catch (\Exception $e) {
                Log::error($this->ip . $request->input('id') . '商务部更新名字成功，邮件发送失败' . $e->getMessage());
            }

            return response()->json([
                'success' => true,
                'message' => 'updated!'
            ]);
        } catch (\Exception $e) {
            Log::error($this->ip . $request->input('id') . '商务部更新名字失败: ' . $e->getMessage());
            return response()->json([
                'success' => false,
                'message' => 'update Failed!'
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fixOrder(Request $request)
    {
        $order = Order::with('rooms')
            ->where('id', $request->input('id'))
            ->first();

        $remark = $request->input('remark');
        $remark = mb_substr($remark, 0, 1000);
        $order->remark = $remark;
        $order->save();

        return response()->json([
            'success' => true,
            'message' => 'updated!'
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fixOrderChannel(Request $request)
    {
        $order = Order::with('rooms')
            ->where('id', $request->input('id'))
            ->first();

        $channel_remark = $request->input('channel_remark');
        $channel_remark = mb_substr($channel_remark, 0, 1000);
        $order->channel_remark = $channel_remark;
        $order->save();

        return response()->json([
            'success' => true,
            'message' => 'updated!'
        ]);
    }
}
