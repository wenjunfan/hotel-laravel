<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Distributor;
use App\Model\User;
use App\Model\Order;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

use Carbon\Carbon;

/**
 * Class AccountController
 * @package App\Http\Controllers\Admin
 */
class AccountController extends Controller
{

    protected $creationStart; // 账号创建 开始时间
    protected $creationEnd; // 账号创建 结束时间

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        //为方式iOS内存超出问题，只展示5个月的数据
        $this->creationStart = (new Carbon('America/Los_Angeles'))->endOfDay()->subMonth(5)
            ->toDateTimeString();// 本年1月1日 "2018-01-01 10:56:53"

        $this->creationEnd = (new Carbon('America/Los_Angeles'))->endOfDay()
            ->toDateTimeString();// 至今 "2018-05-21 10:56:53"
    }

    /**
     * 通过url 传参：
     * http://117book.test/admin/accounts?creationStart=2018-02-01&creationEnd=2018-07-01&keywordType=1&keyword=走四方&payType=repay&accountStatus=1
     * http://117book.test/admin/accounts?creationStart=2018-01-01&creationEnd=2018-08-01&keywordType=4&keyword=0.03&payType=prepay&accountStatus=1
     * 注意：如果关键词搜索不为空，则不限制日期。
     * keyword 关键词，
     * keywordType 关键词类别，
     * payType 付款方式,
     * accountStatus 账户状态,
     * creationStart 创建账户开始,
     * creationEnd 创建账户结束
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $requestData = $request->all();
        // 账号创建 开始时间
        if ($request->has('creationStart')) {
            $requestData['creationStart'] = $request->input('creationStart') . ' 00:00:00';
        } else {
            $requestData['creationStart'] = $this->creationStart;
        }
        // 账号创建 结束时间
        if ($request->has('creationEnd')) {
            $requestData['creationEnd'] = $request->input('creationEnd') . ' 23:59:59';
        } else {
            $requestData['creationEnd'] = $this->creationEnd;
        }
        // other filter conditions
        // 关键词
        if ($request->has('keyword') && $request->has('keywordType') && $requestData['keyword'] != '') {
            $requestData['keyword'] = $request->input('keyword');
            $requestData['keywordType'] = $request->input('keywordType');
        }
        // 账户状态
        if ($request->has('accountStatus')) {
            $requestData['accountStatus'] = $request->input('accountStatus');
        }
        // 付款方式
        if ($request->has('payType')) {
            $requestData['payType'] = $request->input('payType');
        }

        $data = $requestData;

        return $this->getData($data);
    }

    /**
     * @param $requestData
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getData($requestData)
    {
        $optionName = 'accounts';
        // 如果有关键词搜索，则自动忽略日期
        if (isset($requestData['keywordType']) && isset($requestData['keyword']) && str_replace(" ", "", $requestData['keyword']) !== '') {
            $keyword = $requestData['keyword'];
            switch ($requestData['keywordType']) {
                case '1': // 用户名、公司名、联系人
                    $users = User::where('name', 'like', '%' . $keyword . '%')
                        ->orWhere('company_name', 'like', '%' . $keyword . '%')
                        ->orWhere('contact_person', 'like', '%' . $keyword . '%');
                    break;
                case '2': // 用户id
                    $users = User::where('id', (int)$keyword);
                    break;
                case '3': // 电话
                    $users = User::where('mobile_phone', 'like', '%' . $keyword . '%')
                        ->orWhere('company_phone', 'like', '%' . $keyword . '%');
                    break;
                case '4': // 佣金比例
                    $users = User::where('priceRatio', $keyword);
                    break;
                case '5': // 客户邮箱
                    $users = User::where('email', 'like', '%' . $keyword . '%');
                    break;
                case '6': // 用户权限
                    if ($keyword === 'BDD') {
                        $users = User::where('id', 11313);
                    } elseif ($keyword === 'BD') {
                        $users = User::where('admin', 3)
                            ->where('id', '<>', 11313);
                    } else {
                        $level = 5;
                        if ($keyword === '普通') {
                            $level = 0;
                        } elseif ($keyword === '超级') {
                            $level = 1;
                        } elseif ($keyword === '财务') {
                            $level = 2;
                        } elseif ($keyword === '分销') {
                            $level = 4;
                        } elseif ($keyword === '新分销申请') {
                            $level = 5;
                        }
                        $users = User::where('admin', $level);
                    }
                    break;
                case '7': // 客户国家
                    $country_code = 'US';
                    if ($keyword == '中国用户' || $keyword == '中国' || $keyword == 'China' || $keyword == 'CN') {
                        $country_code = 'CN';
                    }
                    $users = User::where('Country_Code', $country_code);
                    break;
                default:
                    // 按没有关键词过滤（时间）
                    $users = User:: where('created_at', '>=', $requestData['creationStart'])
                        ->where('created_at', '<=', $requestData['creationEnd']);
                    break;
            }
        } else {
            // 没有关键词搜索，先按日期过滤
            $users = User:: where('created_at', '>=', $requestData['creationStart'])
                ->where('created_at', '<=', $requestData['creationEnd']);
        }
        // 其他过滤条件： 付款方式，账户状态
        if (isset($requestData['payType'])) {

            $users = $users->where('paymentType', ucfirst($requestData['payType']));
        }
        if (isset($requestData['accountStatus'])) {
            $users = $users->where('active', (int)$requestData['accountStatus']);
        }

        $users = $users->orderBy('id', 'desc')
            ->get();

        return view('admin.accounts', [
            'optionName' => $optionName,
            'users' => $users,
            'from' => substr($requestData['creationStart'], 0, 10),
            'to' => substr($requestData['creationEnd'], 0, 10),
            'keywords' => isset($requestData['keyword']) ? $requestData['keyword'] : '',
            'creationStart' => substr($requestData['creationStart'], 0, 10),
            'creationEnd' => substr($requestData['creationEnd'], 0, 10),
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return response()->json([
                'success' => false,
                'message' => '用户注册信息有错误',
            ]);
        }

        if ($request->has('ratio')) {
            $user->priceRatio = $request->input('ratio');
            $user->save();
        } else {
            if ($request->has('paymentType')) {
                $user->paymentType = $request->input('paymentType');
                $user->save();
            } else {
                if ($request->has('see_provider')) {
                    $user->see_provider = $request->input('see_provider');
                    $user->save();
                } else {
                    if ($request->has('active')) {
                        $user->active = $request->input('active');
                        if ($request->input('active') == 1) {
                            $ActiveUserName = $user->name;
                            $userEmail = $user->email;
                            $langId = 0;
                            if ($user->Country_Code === 'CN') {
                                $subject = '117book-您的账户已经成功激活';
                            } else {
                                $langId = 1;
                                $subject = '117book-Your Account is been activated successfully';
                            }
                            try {
                                Mail::send('emails.117book_new_active_user', [
                                    'ActiveUserName' => $ActiveUserName,
                                    'langId' => $langId,
                                ], function ($message) use ($ActiveUserName, $userEmail, $subject) {
                                    $message->to($userEmail, $ActiveUserName)
                                        ->cc('bd@117book.com', 'business development specialist')
                                        ->bcc('bd@usitrip.com', 'Jimmy BD')
                                        ->bcc('181@usitrip.com', 'Allen BD')
                                        ->subject($subject);
                                });
                                Log::info("send New User Activate Email successfully");
                            } catch (\Exception $e) {
                                Log::info("not send New User Activate Email" . $e->getMessage());
                            }
                        }
                        if ($user->active && empty($user->api_password)) {
                            $user->api_password = $this->generateRandomString();
                        }
                        $user->save();
                    } else {
                        if ($request->has('admin')) {
                            $user->admin = $request->input('admin');
                            $user->save();
                        } else {
                            if ($request->has('Country_Code')) {
                                if ($request->input('Country_Code') == 'CN') {
                                    $user->priceRatio = 0.05;
                                } else {
                                    if ($request->input('Country_Code') == 'US') {
                                        $user->priceRatio = 0.07;
                                    }
                                }
                                $user->Country_Code = $request->input('Country_Code');
                                $user->save();
                            }
                        }
                    }
                }
            }
        }

        //remove the cache from redis
        \Redis::del('PARTNER:' . $user->email);

        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * @param int $length
     * @return string
     */
    private function generateRandomString($length = 32)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fixUser(Request $request)
    {
        $user = User::where('id', $request->input('id'))
            ->first();

        $company_name = $request->input('company_name');
        $company_phone = $request->input('company_phone');
        $channel = $request->input('channel');
        $cardholder = $request->input('cardholder');
        $remark = $request->input('remark');

        $company_name = mb_substr($company_name, 0, 60);
        $company_phone = mb_substr($company_phone, 0, 60);
        $channel = mb_substr($channel, 0, 60);
        $cardholder = mb_substr($cardholder, 0, 60);
        $remark = mb_substr($remark, 0, 1000);

        $user->company_name = $company_name;
        $user->company_phone = $company_phone;
        $user->channel = $channel;
        $user->cardholder = $cardholder;
        $user->remark = $remark;

        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'updated!',
        ]);

    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAffiliate(Request $request)
    {
        //Add distributor
        $id = $request->input('id');
        $user = User::where('id', $id)
            ->first();

        //        每次重新激活，apply当前的affiliate-rate
        $user->affiliateRate = config('constants.affiliate_rate'); //0.05
        $previousDistributor = Distributor::where('partner_id', $id)
            ->first();

        // 如果曾经没有开通过分销账户
        if (empty($previousDistributor)) {
            $distributor_code = $this->generateRandomString();
            $distributorTable = new Distributor();
            $distributorTable->distributor_code = strtolower($distributor_code);
            $distributorTable->partner_id = $id;
            $distributorTable->distributor_name = $user->name;
            $distributorTable->life_time = 30;
            try {
                $distributorTable->save();
            } catch (\Exception $e) {
                Log::error('add distributor failed for user ' . $id . ' reason: ' . $e->getMessage());
            }
        } elseif ($user->admin === 5) {
            if ($user->host_name == 'Usitrip') {
                $link = 'https://hotel.usitrip.com/affiliate/welcome';
            } else {
                $link = 'https://hotel.usitour.com/affiliate/welcome';
            }

            $newUserEmail = 'emails.affiliate_newUser';
            $subject = '欢迎加入走四方人人赚';
            $langId = 0;
            if ($user->language == 1) {
                $newUserEmail = 'emails.affiliate_newUser_en';
                $subject = 'Welcome to affiliate partnership';
                $langId = 1;
            } elseif ($user->language == 2) {
                $subject = '歡迎加入走四方人人賺';
                $langId = 2;
            }
            $receiverEmail = $user->email;
            try {
                Mail::queue($newUserEmail,
                    ['NewUserName' => $user->name, 'link' => $link, 'domain' => $user->host_name, 'langId' => $langId],
                    function ($message) use ($subject, $receiverEmail) {
                    $message->to($receiverEmail)
                        ->bcc(config('staff-emails.ann'), 'Ann test')
                        ->subject($subject);
                });
                Log::info("send New User Email successfully");
            } catch (\Exception $e) {
                Log::error('send new user email failed: ' . $e->getMessage());
            }
        }

        $user->admin = 4;
        try {
            $user->save();
        } catch (\Exception $e) {
            Log::error('failed to change user to distributor' . $id . ' reason: ' . $e->getMessage());
        }

        return response()->json([
            'success' => true,
            'message' => 'updated!',
        ]);
    }

    /**
     * updateAllUserFinanceRecords
     * Update all purchase history amounts and the amount haven't been confirmed received by accountant
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAllUserFinanceRecords()
    {
        $failedUpdateUsersId = [];
        //start save info for post pay user paid sum and unpaid sum, also pass to db
        $users = Order::where('partnerId', '<>', '')
            ->groupBy('partnerId')
            ->get();
        //            Log::info('------------' . json_encode($users, true));
        foreach ($users as $user) {
            $partnerTable = User::where('id', $user->partnerId)
                ->get()
                ->first();
            if (isset($partnerTable) && $partnerTable->active === 1) {
                $sum_original_paid = Order::where('partnerId', $user->partnerId)
                    ->where('isChecked', 1)
                    ->sum('totalPrice');
                $sum_returned = Order::where('partnerId', $user->partnerId)
                    ->Where('isReturned', 1)
                    ->sum('totalPrice');
                $sum_paid = $sum_original_paid - $sum_returned;
                $sum_allConfirmed = Order::where('partnerId', $user->partnerId)
                    ->where('status', 'CONFIRMED')
                    ->sum('totalPrice');
                $sum_unpaid = $sum_allConfirmed - $sum_paid;
                $partnerTable->balance = $sum_allConfirmed;
                $partnerTable->postpay_paid = $sum_paid;
                $partnerTable->postpay_unpaid = $sum_unpaid;
                try {
                    log::info("Update User " . $user->partnerId . " update paid balance" . $sum_paid . ", unpaid balance" . $sum_unpaid);
                    $partnerTable->update();
                } catch (\Exception $e) {
                    $failedUpdateUsersId[] = $user->partnerId;
                    Log::error($user->partnerId . ' update user finance status info failed, ' . $e->getMessage());
                    continue;
                }
            }
        }
        if (count($failedUpdateUsersId) > 0) {
            return response()->json([
                'success' => false,
                'message' => json_encode($failedUpdateUsersId) . ' please check those user ids, they did not update finance status successfully',
            ]);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'updated!',
            ]);
        }
    }
}

