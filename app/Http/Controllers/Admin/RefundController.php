<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\AlipayRefund;
use App\Model\Partner;
use App\Model\PartnerTransaction;
use App\Model\Order;
use App\Model\Refund;
use App\Model\User;
use Illuminate\Http\Request;

/**
 * Class RefundController
 * @package App\Http\Controllers\Admin
 */
class RefundController extends Controller
{
    /**
     * @param $partnerId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function index($partnerId)
	{
		return $this->between($partnerId, '', '');
	}

    /**
     * @param $partnerId
     * @param $from
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function between($partnerId, $from, $to)
	{
		$users = User::all();
		$refunds = Refund::with('user');

		if ($partnerId != 'all') {
			$refunds = $refunds->where('partnerId', $partnerId);
		}

		if ($from != '') {
			$refunds = $refunds->where('created_at', '>=', $from);
		}

		if ($to != '') {
			$refunds = $refunds->where('created_at', '<=', $to);
		}

		$refunds = $refunds->orderBy('created_at', 'desc')
		                   ->get();

		return view('admin.refunds', ['optionName' => 'refunds', 'users' => $users, 'refunds' => $refunds, 'partnerId' => $partnerId, 'from' => $from, 'to' => $to]);
	}

    /**
     * @param $partnerId
     * @param $from
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function from($partnerId, $from)
	{
		return $this->between($partnerId, $from, '');
	}

    /**
     * @param $partnerId
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function to($partnerId, $to)
	{
		return $this->between($partnerId, '', $to);
	}

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
	public function refundConfirm(Request $request)
	{
		$order = Order::with('rooms')
		                   ->where('id', $request->input('id'))
		                   ->first();
		if ($order == null) {
			return response()->json([
				'success' => false,
				'message' => '没有找到订单记录' . $request->input('id')
			]);
		}

		//TODO:change to citcon refund
		AlipayRefund::where('order_reference', $order->orderReference)
		  ->update([
			  'status' => 'REFUND'
		  ]);

		//pass to balance
		$balance = Partner::where('id', $order->partnerId)
		             ->first()
			->balance;
		$balanceNew = (double)$balance + (double)$order->totalNetPrice;

		$transaction_fee = (double)$order->totalNetPrice * 0.035;
		$refund_balance = $order->totalNetPrice - (double)$transaction_fee;

		//write partner_transaction
        PartnerTransaction::insert([
			  'orderReference' => $order->orderReference,
			  'orderId'        => $order->id,
			  'partnerId'      => $order->partnerId,
			  'currencyCode'   => $order->currencyCode,
			  'description'    => 'alipaycancel',
			  'balance'        => $balance,
			  'amount'         => $refund_balance,
			  'netPrice'       => -$refund_balance
		  ]);

		Order::where('orderReference', $order->orderReference)
		          ->update([
			          'payment_status' => 'REFUND',
		          ]);

		return response()->json([
			'success' => true,
			'message' => "REFUND"
		]);
	}

    /**
     * @param $partnerId
     * @param $from
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function Alipay_between($partnerId, $from, $to)
	{
		$users = User::all();
		$refunds = AlipayRefund::with('order', 'partner')
		                            ->get();

		if ($partnerId != 'all') {
			$refunds = $refunds->where('partner_id', $partnerId);
		}

		if ($from != '') {
			$refunds = $refunds->where('alipay_refund.created_at', '>=', $from);
		}

		if ($to != '') {
			$refunds = $refunds->where('alipay_refund.created_at', '<=', $to);
		}

		return view('admin.alipay-refunds', ['optionName' => 'alipayRefunds', 'users' => $users, 'refunds' => $refunds, 'partnerId' => $partnerId, 'from' => $from, 'to' => $to]);
	}

    /**
     * @param $partnerId
     * @param $from
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function alipay_from($partnerId, $from)
	{
		return $this->alipay_between($partnerId, $from, '');
	}

    /**
     * @param $partnerId
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function alipay_to($partnerId, $to)
	{
		return $this->alipay_between($partnerId, '', $to);
	}

    /**
     * @param $partnerId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function alipay_index($partnerId)
	{
		return $this->alipay_between($partnerId, '', '');
	}

}
