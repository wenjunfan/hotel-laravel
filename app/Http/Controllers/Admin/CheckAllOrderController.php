<?php

namespace App\Http\Controllers\Admin;

use App\Model\Distributor;
use App\Model\Order;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

/**
 * Class CheckAllOrderController
 * @package App\Http\Controllers\Admin
 */
class CheckAllOrderController extends Controller
{
//    财务查询到账记录，只需要订单日期和退房日期
    protected $creationStart; // 订单创建 开始时间
    protected $creationEnd; // 订单创建 结束时间
    protected $checkinStart; // 入住 开始日期
    protected $checkinEnd; // 入住 结束日期
    protected $checkoutStart; // 退房 开始日期
    protected $checkoutEnd; // 退房 结束日期

    public function __construct()
    {
        parent::__construct();
        $this->creationStart = (new Carbon('last week',
            'America/Los_Angeles'))->toDateTimeString();
        $this->creationEnd = (new Carbon('America/Los_Angeles'))->endOfDay()->toDateTimeString();// 上周至今 的订单 "yyyy-mm-dd hh:mm:ss" 财务查看最多的filter 方式
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $is_admin_n_accounting = (Auth::user()->admin == 2 || Auth::user()->admin == 1);
        if(!$is_admin_n_accounting){
            abort('404','非财务或超级用户没有权限');
        }
        $requestData = $request->all();
        // 订单创建 开始时间
        if ($request->has('creationStart')) {
            $requestData['creationStart'] = $request->input('creationStart') . ' 00:00:00';
        } else {
            $requestData['creationStart'] = $this->creationStart;
        }
        // 订单创建 结束时间
        if ($request->has('creationEnd')) {
            $requestData['creationEnd'] = $request->input('creationEnd') . ' 23:59:59';
        } else {
            $requestData['creationEnd'] = $this->creationEnd;
        }
        // 入住 开始日期
        if ($request->has('checkinStartDate')) {
            $requestData['checkinStartDate'] = $request->input('checkinStartDate') . ' 00:00:00';
        }
        // 入住 结束日期
        if ($request->has('checkinEndDate')) {
            $requestData['checkinEndDate'] = $request->input('checkinEndDate') . ' 23:59:59';
        }
        // 退房 开始日期
        if ($request->has('checkoutStartDate')) {
            $requestData['checkoutStartDate'] = $request->input('checkoutStartDate') . ' 00:00:00';
        }
        // 退房 结束日期
        if ($request->has('checkoutEndDate')) {
            $requestData['checkoutEndDate'] = $request->input('checkoutEndDate') . ' 23:59:59';
        }
        // other filter condition
        if ($request->has('partnerId')) {
            $requestData['partnerId'] = $request->input('partnerId');
        }
        if ($request->has('distributorSource')) {
            $requestData['distributorSource'] = $request->input('distributorSource');
        }
        if ($request->has('orderStatus')) {
            $requestData['orderStatus'] = $request->input('orderStatus');
        }

        if ($request->has('orderPReference')) {
            $requestData['orderPReference'] = $request->input('orderPReference');
        }
        if ($request->has('salesId')) {
            $requestData['salesId'] = $request->input('salesId');
        }
        if ($request->has('orderId')) {
            $requestData['orderId'] = $request->input('orderId');
        }
        if ($request->has('paymentMethod')) {
            $requestData['paymentMethod'] = $request->input('paymentMethod');
        }

        return $this->getData($requestData);
    }

    /**
     * @param $requestData
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getData($requestData)
    {
        $optionName = 'checkOrders';
        $users = User::where('active', 1)->select('id', 'name', 'company_name')->get();
        $distributors = Distributor::where('partner_id' ,'<>','')->select('id', 'distributor_code', 'partner_id', 'distributor_name')->get();
        $orders = Order::where('porder.partnerId','<>','');
        $creationStart = explode(" ", $requestData['creationStart'])[0];
        $creationEnd = explode(" ", $requestData['creationEnd'])[0];
        // filter data: orderId, checkinStart, checkinEnd, checkoutStart, checkoutEnd, partnerId, orderStatus, orderPReference
        // 以下两个搜索是整个数据库的搜索
        if(isset($requestData['orderId'])){
            $orders = $orders->where('id', $requestData['orderId']);
        }elseif(isset($requestData['orderPReference'])){
            // orderPReference 上游提供的订单号，可能多个号合并在一起, 需要模糊查询
            $orders = $orders->where('orderPReference', 'like', '%'.$requestData['orderPReference'].'%');
        }else{
            $orders = $orders->where('created_at', '>=', $requestData['creationStart'])
                           ->where('created_at', '<=', $requestData['creationEnd']);
            // checkinStart
            if(isset($requestData['checkinStart'])){
                $orders = $orders->where('checkinDate','>=', $requestData['checkinStart']);
            }
            // checkinEnd
            if(isset($requestData['checkinEnd'])){
                $orders = $orders->where('checkinDate','<=', $requestData['checkinEnd']);
            }
            // checkoutStart
            if(isset($requestData['checkoutStart'])){
                $orders = $orders->where('checkoutDate','>=', $requestData['checkoutStart']);
            }
            // checkoutEnd
            if(isset($requestData['checkoutEnd'])){
                $orders = $orders->where('checkoutDate','<=', $requestData['checkoutEnd']);
            }
            // partnerId
            if(isset($requestData['partnerId']) && $requestData['partnerId'] !== '0'){
                $orders = $orders->where('partnerId', $requestData['partnerId']);
            }

            // paymentMethod
            if(isset($requestData['paymentMethod']) && $requestData['paymentMethod'] !== ''){
                if($requestData['paymentMethod'] === 'EB'){
                    $orders = $orders->where('payment_type', '');
                }else{
                    $orders = $orders->where('payment_type', $requestData['paymentMethod']);
                }
            }

            // provider
            if(isset($requestData['provider']) && $requestData['provider'] !== ''){
                $orders = $orders->where('provider', $requestData['provider']);
            }

            // distributorSource
            if(isset($requestData['distributorSource']) && $requestData['distributorSource'] !== '0'){
                if($requestData['distributorSource'] === '8e736079578b4e948a4bf694581ddab5' ){ //dealmoon有别的方式的广告
                    $orders = $orders->where('source', $requestData['distributorSource'])->orWhere('source', 'dealmoon');
                }else{
                    $orders = $orders->where('source', $requestData['distributorSource']);
                }
            }

           if(isset($requestData['salesId'])){
                //TODO::missedOrder和suspiciousOrder也要?
                $orders =$orders->where('sales_no',  'like', '%'.$requestData['salesId'].'%');
            }
            // order status
            if(isset($requestData['orderStatus'])){
                switch ($requestData['orderStatus'])
                {
                    case 1://处理中
                        $orders = $orders->where('status', 'ON REQUEST');
                        break;
                    case 2: //已确认
                        $orders = $orders->where('status', 'CONFIRMED');
                        break;
                    case 3://已取消退款中
                        $orders = $orders->where('payment_status', 'PENDING');
                        $orders = $orders->where('status', 'CANCELLED');
                        break;
                    case 4://已取消
                        $orders = $orders->where('payment_status', '<>', 'PENDING');
                        $orders = $orders->where('status', 'CANCELLED');
                        break;
                    case 7: //EB拒绝
                        $orders = $orders->where('status', 'REJECTED')->where('provider','EBooking');
                        break;
                    case 5://失败
                        $orders = $orders->where('status', 'REJECTED')->where('provider','<>','EBooking')->orwhere('status','FAILED');
                        break;
                    default:
                        break;
                }
            }
        }
        $orders = $orders->orderBy('id', 'desc')
            ->with(['distributor', 'rooms', 'requestlog', 'orderTem', 'payment', 'orderTem.suspicious', 'feedback','hotel','hotel.destination','hotel.destination.country','linked_id','ctrip'])
            ->with([ 'partner' => function ($query) {
                    // Malformed UTF-8 characters, possibly incorrectly encoded ，partner表返回值有这个报错
                    $query->select('id', 'email', 'name', 'company_name','cardholder');// partner有些字段有编码问题，因此只选需要的field
                },
            ])
            ->get()
            ->toArray();

        $orders = json_encode($orders);

        return view('admin.check-all-orders', compact('orders', 'optionName', 'users', 'creationStart', 'creationEnd','distributors'));
    }

    /**
     *  财务添加备注
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAccountingComment(Request $request){
        $order = Order::where('id', $request->input('id'))->first();
        $oldCommend = $order->finance_remark === null ? "" : $order->finance_remark;
        $newCommend = $request->input('finance_remark') . '<br>' . $oldCommend;
        $order->finance_remark = $newCommend;
        try {
            $order->save();
            return response()->json([
                'success' => true,
                'message' => '更新成功',
                'finance_remark' => $order->finance_remark
            ]);
        } catch (\Exception $e) {
            Log::error('fix order finance error: ' . $e->getMessage());
            return response()->json([
                'success' => false,
            ]);
        }
    }

    // TODO 以下四个update status的methods可以合并
    /**
     * 检查余总是否已经存钱入银行
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateIsBanked(Request $request)
    {
        Order::where('id', $request->input('id'))
            ->update(['isBanked' => $request->input('isBanked')]);

        return response()->json([
            'success' => true,
            'message' => 'updated!'
        ]);
    }

    /**
     * 检查是否已经向上游结算
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateIsPayed(Request $request)
    {
        Order::where('id', $request->input('id'))
            ->update(['isPayed' => $request->input('isPayed')]);

        return response()->json([
            'success' => true,
            'message' => 'updated!'
        ]);
    }

    /**
     * 检查是否已经收到客户的付款
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateIsChecked(Request $request)
    {
        $order = Order::where('id', $request->input('id'))->first();
        $order['isChecked'] = $request->input('isChecked');
        //paypal的服务对于取消或者确认收款，银行同步
        if($order['payment_type'] === 'CC' || $order->payment_type === 'PB'){
            $order['isBanked'] = $request->input('isChecked');
        }
        $order->update();
        return response()->json([
            'success' => true,
            'message' => 'updated!'
        ]);
    }

    /**
     * 检查是否退还postpay用户上次已经付过的取消订单款项
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateIsReturned(Request $request)
    {
        Order::where('id', $request->input('id'))
            ->update(['isReturned' => $request->input('isReturned')]);

        return response()->json([
            'success' => true,
            'message' => 'updated!'
        ]);
    }

    /**
     * 检查是否结算过了分销账户的佣金
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateIsAffiliated(Request $request)
    {
        Order::where('id', $request->input('id'))
            ->update(['isAffiliated' => $request->input('isAffiliated')]);

        return response()->json([
            'success' => true,
            'message' => 'updated!'
        ]);
    }

}
