<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Payment;
use App\Model\User;

/**
 * Class PaymentController
 * @package App\Http\Controllers\Admin
 */
class PaymentController extends Controller
{
    /**
     * @param $partnerId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function index($partnerId)
	{
		return $this->between($partnerId, '', '');
	}

    /**
     * @param $partnerId
     * @param $from
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function between($partnerId, $from, $to)
	{
		$users = User::all();
		$payments = Payment::with('user');

		if ($partnerId != 'all') {
			$payments = $payments->where('partnerId', $partnerId);
		}

		if ($from != '') {
			$payments = $payments->where('created_at', '>=', $from);
		}

		if ($to != '') {
			$payments = $payments->where('created_at', '<=', $to);
		}

		$payments = $payments->orderBy('created_at', 'desc')
		                     ->get();

		return view('admin.payments', ['optionName' => 'payments', 'users' => $users, 'payments' => $payments, 'partnerId' => $partnerId, 'from' => $from, 'to' => $to]);
	}

    /**
     * @param $partnerId
     * @param $from
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function from($partnerId, $from)
	{
		return $this->between($partnerId, $from, '');
	}

    /**
     * @param $partnerId
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function to($partnerId, $to)
	{
		return $this->between($partnerId, '', $to);
	}
}
