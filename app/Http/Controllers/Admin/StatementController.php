<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Statement;
use App\Model\User;

/**
 * Class StatementController
 * @package App\Http\Controllers\Admin
 */
class StatementController extends Controller
{
    /**
     * @param $partnerId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function index($partnerId)
	{
		return $this->between($partnerId, '', '');
	}

    /**
     * @param $partnerId
     * @param $from
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function between($partnerId, $from, $to)
	{
		$balance = 0;
		$users = User::all();
		$statements = Statement::with('user');

		if ($partnerId != 'all') {
			$statements = $statements->where('partnerId', $partnerId);
			$user = User::find($partnerId);
			$balance = $user->balance;
		}

		if ($from != '') {
			$statements = $statements->where('created_at', '>=', $from);
		}

		if ($to != '') {
			$statements = $statements->where('created_at', '<=', $to);
		}

		$statements = $statements->orderBy('created_at', 'desc')
		                         ->get();

		return view('admin.statements', ['optionName' => 'statements', 'users' => $users, 'balance' => $balance, 'statements' => $statements, 'partnerId' => $partnerId, 'from' => $from, 'to' => $to]);
	}

    /**
     * @param $partnerId
     * @param $from
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function from($partnerId, $from)
	{
		return $this->between($partnerId, $from, '');
	}

    /**
     * @param $partnerId
     * @param $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function to($partnerId, $to)
	{
		return $this->between($partnerId, '', $to);
	}
}
