<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;

/**
 * Class LangToBMiddleware
 * @package App\Http\Middleware
 */
class LangToBMiddleware
{
    /**
     * Handle an incoming request.
     * All web routes go through this middleware
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // app language base on language session
        if (session()->has('language')) {
            if (1 === (int)session('language', 0)) {
                App::setLocale('en');
            } elseif ( 2 === (int)session('language', 0)) {
                App::setLocale('tw');
            }else {
                App::setLocale('cn');
            }
        } else {
            // init language session
            session(['language' => 0]);
            App::setLocale('cn');
        }

        return $next($request);
    }
}
