<?php

namespace App\Http\Middleware;

use App\Model\Distributor;
use App\Model\User;
use Log;
use App;
use Illuminate\Support\Facades\Auth;
use Closure;

class DistributorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('par')){
            $source = $request->has('par') ? $request->get('par') : '';
        }elseif($request->has('utm_source')){
            $source = $request->has('utm_source') ? $request->get('utm_source') : '';
        }else{
            $source = $request->has('source') ? $request->get('source') : '';
        }

        if (!empty($source)) {
            $distributor = Distributor::where('distributor_code', $source)->first();
            if (!empty($distributor)) {
                setcookie("source", $source, time() + $distributor->life_time * 24 * 60 * 60 * 30, '/', preg_replace('/^(hotel|www)/', '', $_SERVER['SERVER_NAME']));
                // store distributor source for one month
            }else{
                setcookie("source", $source, time() + 30 * 24 * 60 * 60, '/', preg_replace('/^(hotel|www)/', '', $_SERVER['SERVER_NAME'])); // store distributor source for one month
            }
        }

        if(isset($_SERVER['HTTP_HOST']) && strrpos($_SERVER['HTTP_HOST'], 'supervacation') !== false){
            $partnerId = 11676;
        }else{
            $partnerId = 11087;
        }

        $distributor = Distributor::where('partner_id', $partnerId)->first();
        if (!empty($distributor)) {
            setcookie("source", $distributor['distributor_code'], time() + $distributor->life_time * 24 * 60 * 60 * 30, '/', preg_replace('/^(hotel|www)/', '', $_SERVER['SERVER_NAME'])); // store distributor source for one month
        }

        // only update source position if it's  exist or value is NOT empty
        if (session()->has('source_pos') && !empty(session('source_pos', ''))) {
            $position =  $request->has('pos') ? $request->get('pos') : '';
            session(['source_pos' => $position]);
            Log::info('come from source: ' . $source . ', position: ' . session('source_pos'));
        }
        //上面部分幾乎沒有用,目前都是只有名字沒方位
        if($request->has('utm_campaign') && $request->get('utm_campaign')  !== ''){
            $utm_campaign =  $request->has('utm_campaign') ? $request->get('utm_campaign') : '';
            setcookie("utm_campaign",$utm_campaign, time() +  24 * 60 * 60 * 30, '/', preg_replace('/^(hotel|www)/', '', $_SERVER['SERVER_NAME']));
        }
        return $next($request);
    }
}
