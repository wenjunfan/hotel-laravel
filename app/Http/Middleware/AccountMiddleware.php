<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class AccountMiddleware {
    /**
     * Handle an incoming request.
     * Depend on admin middleware
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //affiliate大部分后台的权限
        if (Auth::user()->admin === 1 || Auth::user()->admin === 2 || in_array(Auth::user()->email, config('constants.marketing'), true) || Auth::user()->email === 'hotel@usitrip.com') {
            return $next($request);
        } elseif(Auth::user()->id === 11676 && (strrpos($_SERVER['REQUEST_URI'], 'fraud') !== false || strrpos($_SERVER['REQUEST_URI'], 'missed')) ){ //aa affiliate
            return $next($request);
        }else {
            //普通分销用户后台
            abort(404, "Invalid request, please contact us for assistant");
            return false;
        }
    }
}
