<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LangMiddleware
{
    /**
     * Handle an incoming request.
     * All web routes go through this middleware
     *
     * 0: chinese
     * 1: english
     * 2: traditional chinese
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $my_referer =isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER']:false;
        $host = $request->getHost();

        // 117book need login to view information except voucher
        if (strrpos($_SERVER['REQUEST_URI'], 'voucher') === false && str_contains($host, '117book') && Auth::guest() && strrpos($_SERVER['REQUEST_URI'], 'jumpAd') === false ) {
            return redirect()->guest('login');
        }
     	$language = 0;
        // check domain first
        if(str_contains($host, 'supervacation') ||  ($my_referer && (str_contains($my_referer, 'tw.')||str_contains($my_referer, 'mt.') ))) {
            //优先大团繁体站和美亚来的酒店,统一使用繁体
            session(['language' => 2]);
            App::setLocale('tw');
            $language = 2;
        }elseif($request->has('lang') || session()->has('language')){
            //优先使用语言参数，其次是session
            if ($request->has('lang')) {
                $langStr = $request->input('lang');
                if ($langStr == 'tw') {
                    $language = 2;
                } elseif ($langStr == 'en') {
                    $language = 1;
                }
            }elseif (session()->has('language')) {
                $language = session('language');
            }
            if($language === 2) {
                session(['language' => 2]);
                App::setLocale('tw');
            } elseif ($language === 1){
                session(['language' => 1]);
                App::setLocale('en');
            }
        }elseif(str_contains($host, 'usitour') && session('language') == null && !$request->has('lang')) {
            session(['language' => 1]);
            App::setLocale('en');
            $language = 1;
        }else{
            session(['language' => 0]);
            App::setLocale('cn');
        }

        view()->composer('*', function ($view) use ($language) {
            $view->with([
                'language' => $language
            ]);
        });
        return $next($request);
    }
}
