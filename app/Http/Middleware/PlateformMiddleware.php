<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Jenssegers\Agent\Agent;

class PlateformMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $agent = new Agent();
        if ($agent->is('iPhone')) {
            $platform = $agent->platform();
            $version = $agent->version($platform);
            Log::info('platform middleware ios platform version: ' . $version);

            $version = intval(str_replace('_', '', substr($version, 0, 2)));

            if ($version <= 9) {
                // todo: should consider redirect other mobile actions to old version
                return redirect()->action('HomeController@index', $request->all());
            } else {
                return $next($request);
            }
        } else {
            return $next($request);
        }
    }
}
