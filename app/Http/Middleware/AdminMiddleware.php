<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->admin) {
            return $next($request);
        } else {
            $host = $request->getHost();
            // 117book need login to view information
            if (str_contains($host, '117book') && Auth::guest()) {
                return redirect()->guest('/');
            }
            if (Auth::check() && !Auth::user()->admin) {
                Auth::logout();
                //in case already log in before as admin, and now not admin anymore
                abort(404, "用户没有权限");
                return false;
            }
            return redirect()->guest('/affiliate/login');
        }
    }
}
