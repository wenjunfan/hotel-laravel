<?php

namespace App\Classes;

use App\Model\Destination;
use App\Model\Hotel;
use App\Model\HotelFacility;
use App\Model\HotelImage;
use App\Model\RoomFacility;
use App\Model\ScenicArea;
use App\Model\User;
use Illuminate\Support\Facades\Log;


class HotelDB
{
    public function getHotelOptions($destinationId, $keyword, $count, $user)
    {
        $trimmedKey = trim($keyword);
        $keyArr = explode(" ", $trimmedKey);

        $sqlWhereEn = " 1=1 ";
        $sqlWhereCn = " 1=1 ";
        foreach ($keyArr as $value) {
            if (strlen($value) > 0) {
                $sqlWhereEn .= " and name like '%" . $value . "%'";
                $sqlWhereCn .= " and name_zh like '%" . $value . "%'";
            }
        }

        $sqlWhere = "(" . $sqlWhereEn . ") or (" . $sqlWhereCn . ")";

        $provider = User::select('providers')
                        ->where('email', $user->email)
                        ->get()
                        ->first();

        $provider = $provider->toArray();
        $provider = $provider['providers'];
        $providers = explode(',', $provider);

        foreach ($providers as &$provider) {
            $provider = trim($provider, '["]');
        }

        if (0 != $destinationId) {
            $out = Hotel::select('hotelId', 'hotel.desId', 'hotel.display', 'name', 'name_zh', 'destination.state', 'map_country.countryPId')
                        ->where('hotel.desId', $destinationId)
                        ->where('hotel.display', '<>', 0)
                        ->whereRaw($sqlWhere)
                        ->take($count)
                        ->leftJoin('destination', 'hotel.desId', '=', 'destination.desId')
                        ->leftJoin('map_country', 'map_country.countryId', '=', 'destination.countryId')
                        ->where('hotel.display', '<>', 0)
                        ->get();

            return $out;
        } else {
            $out = Hotel::select('hotelId', 'hotel.desId', 'name', 'hotel.display', 'name_zh', 'destination.state', 'map_country.countryPId')
                        ->where('hotel.display', '<>', 0)
                        ->whereRaw($sqlWhere)
                        ->take($count)
                        ->leftJoin('destination', 'hotel.desId', '=', 'destination.desId')
                        ->leftJoin('map_country', 'map_country.countryId', '=', 'destination.countryId')
                        ->where('hotel.display', '<>', 0)
                        ->get();

            return $out;
        }
    }

    public function getHotel($hotelId)
    {
        return Hotel::where('hotelId', $hotelId)
                    ->first();
    }

    public function getHotelImage($hotelId)
    {
        return HotelImage::where('hotelId', $hotelId)
                         ->where('type', 'large')
                         ->orderBy('imgIndex', 'asc')
                         ->get();
    }

    public function getHotelFacility($hotelId)
    {
        return HotelFacility::select('type_zh', 'title_zh', 'type', 'title', 'val', 'fee')
                            ->where('hotelId', $hotelId)
                            ->orderBy('type_zh')
                            ->get();
    }

    public function getRoomsFacilities($roomIds)
    {
        if (count($roomIds) == 0) {
            return [];
        }

        return RoomFacility::select('hotelRoomId', 'type_zh', 'title_zh', 'val', 'fee')
                           ->whereIn('hotelRoomId', $roomIds)
                           ->orderBy('hotelRoomId')
                           ->orderBy('type_zh')
                           ->get();
    }

    // This function will return upstream hotels' static data by search db with all these filters passed
    public function filterHotels($cacheHotels, $currentPage, $pageSize, $orderBy, $sort, $rating, $price, $key,
        $showAllDiscount, $scenicAreaIds, $destinationId, $chainNames)
    {
        $price[0] = $price[2];
        $price[1] = $price[3];
        $temp = strtotime($price[5]) - strtotime($price[4]);
        $temp = $temp / (60 * 60 * 24);
        $price[0] = $price[2] * $temp;
        $price[1] = $price[3] * $temp;

        if ($sort == 'asc') { // sort ascending depends on beforeTax
            if (0 < count($cacheHotels) - 1) {
                $this->quickSort($cacheHotels, 0, count($cacheHotels) - 1);
            }
        } else { // sort descending depends on beforeTax
            if (0 < count($cacheHotels) - 1) {
                $this->quickSort1($cacheHotels, 0, count($cacheHotels) - 1);
            }
        }

        $return = array();
        $ids = []; // to get db hotels data
        $i = 0;
        $apiHotels = []; // to store lowest flag after filter by price range
        $soldOutHotelIds = []; // to split sold out hotels by the end and attach to end of array

        if ($price[0] !== 0 || $price[1] !== 100000) { // if has price filter
            foreach ($cacheHotels as $hotel) { // filter by price range
                if ($hotel['beforeTax'] <= $price[1] && $hotel['beforeTax'] >= $price[0]) {
                    $apiHotels[] = $hotel;
                    $ids[] = $hotel['id']; // get hotel ids ordered by beforeTax
                }

                unset($hotel);
                $i++;
            }
        } else {
            $apiHotels = $cacheHotels;
            $ids = array_column($apiHotels, 'id');
            $availableHotels = array_filter($apiHotels, function ($hotel) {
                return $hotel['beforeTax'] > 0;
            });
            $availableHotelIds = array_column($availableHotels, 'id');
            $soldOutHotelIds = array_diff($ids, $availableHotelIds);

            unset($availableHotels, $availableHotelIds);
        }

        if (count($rating) === 0) {
            $rating = [1, 2, 3, 4, 5];
        }
        if (0 == $rating[0]) {
            $rating[] = -1;
        }
        if (in_array(5, $rating)) {
            $rating[] = 6;
        }

        $return[0] = 0;
        if (empty($key)) { // if passed key is empty
            $select = Hotel::whereIn('hotelId', $ids)
                ->whereIn('rating', $rating);
        } else { // filter with key
            $select = Hotel::whereIn('hotelId', $ids)
                ->whereIn('rating', $rating)
                ->where(function ($query) use ($key) {
                    $query->where('name', 'like', '%' . $key . '%')
                        ->orWhere('name_zh', 'like', '%' . $key . '%')
                        ->orWhere('city', 'like', '%' . $key . '%')
                        ->orWhere('address', 'like', '%' . $key . '%')
                        ->orWhere('chain', 'like', '%' . $key . '%')
                        ->orWhere('chain_zh', 'like', '%' . $key . '%');
                });
        }

        $select = $select->with([
            'destination' => function ($query) {
                $query->join('country', 'destination.countryId', '=', 'country.countryId')
                    ->select('desId', 'desName', 'desName_zh', 'country.countryName', 'country.countryName_zh');
            }
        ]);

        if ($orderBy === 'name' || $orderBy === 'rating') {
            $select = $select->orderBy($orderBy, $sort);
        }

        $return[1] = $select->get()->toArray();
        $return[0] = count($return[1]);

        $returnNew = [];

        $scenic = null;
        // if order by  distance and has more than one scenic, convert to order by discount
        if (count($scenicAreaIds) > 0 && $orderBy === 'distance') {
            if (count($scenicAreaIds) > 1) {
                $orderBy = 'discount';
            } else {
                $scenic = ScenicArea::find($scenicAreaIds[0]);
                if (empty($scenic) || !(isset($scenic['northeast_lat']) && isset($scenic['northeast_lng']))) {
                    $orderBy = 'discount';
                }
            }
        }

        if ($orderBy == 'discount') { // order by discount
            $soldOutHotels = [];
            foreach ($apiHotels as $apiHotel) { // get lowestFlg from api return
                foreach ($return[1] as $hotel) {
                    if ($hotel['hotelId'] == $apiHotel['id']) {
                        $hotel['lowestFlg'] = $apiHotel['lowestFlg'];
                        if ($apiHotel['beforeTax'] > 0) {
                            $returnNew[] = $hotel;
                        } else {
                            $soldOutHotels[] = $hotel; // mark sold out hotels
                        }
                    }

                    unset($hotel);
                }
                unset($apiHotel);
            }

            unset($apiHotels);

            $discountLimit = 10;
            if (strpos($destinationId, 'D') !== false) { // group by discount and non-discount hotels
                $desId = str_replace('D', '', $destinationId);
                if ($desId == 45828) {
                    $desId = 46858;
                }

                // 1. split by desId, same desId go first
                $sameDesHotelsTemp = array_filter($returnNew, function ($hotel) use ($desId) { // group by desId
                    return $hotel['desId'] == $desId;
                });
                $diffDesHotelsTemp = array_diff_key($returnNew, $sameDesHotelsTemp);

                // 2. same desId hotels split by discount, first 10 or all discounted go first
                $sameDesHotels = $this->groupHotelsByDiscount($sameDesHotelsTemp, $discountLimit, $showAllDiscount);

                // 3. diff desId hotels split by lowestFlg, lowest flag go first
                $diffDesHotels = $this->groupHotelsByLowestFlg($diffDesHotelsTemp);

                $returnNew = array_merge($sameDesHotels, $diffDesHotels);

                unset($sameDesHotelsTemp, $diffDesHotelsTemp, $sameDesHotels, $diffDesHotels);
            } else {
                $returnNew = $this->groupHotelsByDiscount($returnNew, $discountLimit, $showAllDiscount);
            }
        } else {
            $soldOutHotels = array_filter($return[1], function ($hotel) use ($soldOutHotelIds) {
                return array_search($hotel['hotelId'], $soldOutHotelIds);
            });

            $ids = array_diff($ids, $soldOutHotelIds);
            if ($orderBy == 'minPrice') { // order by minPrice
                foreach ($ids as $id) {
                    foreach ($return[1] as $hotel) {
                        if ($hotel['hotelId'] == $id) {
                            $returnNew[] = $hotel;
                        }
                        unset($hotel);
                    }
                }
            } elseif ($orderBy === 'distance' && !empty($scenic)) {
                $returnNew = $this->orderHotelsByDistance($return[1], $scenic['northeast_lat'] - 0.03, $scenic['northeast_lng'] - 0.03);
            } else { // other order
                foreach ($return[1] as $hotel) {
                    foreach ($ids as $id) {
                        if ($hotel['hotelId'] == $id) {
                            $returnNew[] = $hotel;
                        }
                    }
                    unset($hotel);
                }
            }
        }

        $i = 0;
        $scenicHotels = []; // filter hotels with passed scenic ids

        $returnNew = array_merge($returnNew, $soldOutHotels); // merge in sold out hotels to be listed by the end

        if (count($scenicAreaIds) > 0) { // filter by scenicIds
            $scenicAreaArr = $this->getScenicAreaByIds($scenicAreaIds);

            if (count($scenicAreaArr) > 0) {
                $scenicHotels = $this->getScenicAreaHotels($returnNew, $scenicAreaArr);
            }

            $returnNew = $scenicHotels;

            unset($scenicHotels, $scenicAreaArr);
        }

        $hotelChains = $this->getHotelChains($returnNew);

        if (count($chainNames) > 0) { // filter hotels by chain names
            $returnNew = array_filter($returnNew, function ($hotel) use ($chainNames) {
                return in_array($hotel['chain'], $chainNames) || in_array($hotel['chain_zh'], $chainNames);
            });

            $newHotelChains = $this->getHotelChains($returnNew);

            foreach ($hotelChains as &$chain) {
                $newHotelChain = array_first($newHotelChains, function ($key, $newChain) use ($chain) {
                    return $newChain->name === $chain->name || $newChain->name_zh === $chain->name_zh;
                });

                if (!empty($newHotelChain)) {
                    $chain->count = $newHotelChain->count;
                } else {
                    $chain->count = '+' . $chain->count;
                }

                unset($newHotelChain);
            }

            unset($newHotelChains);
        }

        $return[0] = count($returnNew);

        $returnNew1 = [];
        foreach ($returnNew as $returnnew) { // get hotels for requested page
            if ($i < $pageSize * $currentPage && $i >= $pageSize * ($currentPage - 1)) {
                $returnNew1[] = $returnnew;
            }

            unset($returnnew);
            $i++;
        }

        $return[1] = $returnNew1;
        $return[2] = $hotelChains;

        unset($returnNew, $returnNew1, $hotelChains);

        return $return;
    }

    public function filterMapHotels($cacheHotels, $map)
    {
        $return = [];
        $apiHotels = $cacheHotels;
        $ids = array_column($apiHotels, 'id');

        $return[1] = Hotel::whereIn('hotelId', $ids)
            ->with(['destination' => function($query) {
                $query->join('country', 'destination.countryId', '=', 'country.countryId')
                    ->select('desId', 'desName', 'desName_zh', 'country.countryName', 'country.countryName_zh');
            }])
            ->orderBy('rating', 'desc')
            ->get();
        $return[0] = $return[1]->count();

        $returnNew = [];
        $return[1] = json_decode(json_encode($return[1]), true);
        $return[1] = json_decode(json_encode($return[1]), true);

        // order by discount
        $soldOutHotels = [];
        foreach ($apiHotels as $apiHotel) { // get lowestFlg from api return
            foreach ($return[1] as $hotel) {
                if ($hotel['hotelId'] == $apiHotel['id']) {
                    $hotel['lowestFlg'] = $apiHotel['lowestFlg'];
                    if ($apiHotel['beforeTax'] > 0) {
                        $returnNew[] = $hotel;
                    } else {
                        $soldOutHotels[] = $hotel; // mark sold out hotels
                    }
                }

                unset($hotel);
            }
            unset($apiHotel);
        }

        unset($apiHotels);
        $returnNew = $this->groupHotelsByDiscount($returnNew, 10, true);
        $returnNew = array_merge($returnNew, $soldOutHotels); // merge in sold out hotels to be listed by the end

        // filter by map bounds
        $mapHotels = [];
        foreach ($returnNew as $hotel) {
            if (isset($hotel['latitude']) && isset($hotel['longitude'])) {
                if ($map['northeast_lat'] > $hotel['latitude'] && $hotel['latitude'] > $map['southwest_lat'] && $map['northeast_lng'] > $hotel['longitude'] && $hotel['longitude'] > $map['southwest_lng']) {
                    $mapHotels[] = $hotel;
                }
            } else {
                continue;
            }
        }

        $length_limit = 20;
        if ($map['zoom'] >= 10 && $map['zoom'] <= 14) {
            $length_limit = config('constants.map_hotels_limit.' . $map['zoom'], 100);
        } elseif ($map['zoom'] > 14) {
            $length_limit = null; // do not limit
        }

        // $length_limit = null; // do not limit hotels
        if (!empty($length_limit)) {
            $mapHotels = array_slice($mapHotels, 0, $length_limit);
        }

        $return[0] = count($mapHotels);
        $return[1] = $mapHotels;
        unset($returnNew);

        return $return;
    }

    private function getHotelChains($hotels)
    {
        $hotelChains = [];

        foreach ($hotels as $hotel) { // get returned hotel chain object array
            $exist_chain = null;

            if (!empty($hotelChains) && (!empty($hotel['chain']) || !empty($hotel['chain_zh']))) { // check if it's chain already in array
                foreach ($hotelChains as $chain) {
                    if (!empty($chain->name) && $chain->name === $hotel['chain']) {
                        $chain->count++;
                        $exist_chain = $chain;
                    } elseif (!empty($chain->name_zh) && $chain->name_zh === $hotel['chain_zh']) {
                        $chain->count++;
                        $exist_chain = $chain;
                    }
                }
            }

            if (empty($exist_chain) && (!empty($hotel['chain']) || !empty($hotel['chain_zh']))) { // if chain not exist in array and it's valid
                $chain = new \stdClass();
                $chain->name = $hotel['chain'];
                $chain->name_zh = $hotel['chain_zh'];
                $chain->count = 1;
                $hotelChains[] = $chain;
            }

            unset($exist_chain, $hotel, $chain);
        }

        return $hotelChains;
    }

    public function partition(&$arr, $leftIndex, $rightIndex)
    {
        $pivot = $arr[($leftIndex + $rightIndex) / 2]['beforeTax'];

        while ($leftIndex <= $rightIndex) {
            while ($arr[$leftIndex]['beforeTax'] < $pivot) {
                $leftIndex++;
            }
            while ($arr[$rightIndex]['beforeTax'] > $pivot) {
                $rightIndex--;
            }
            if ($leftIndex <= $rightIndex) {
                $tmp = $arr[$leftIndex];
                $arr[$leftIndex] = $arr[$rightIndex];
                $arr[$rightIndex] = $tmp;
                $leftIndex++;
                $rightIndex--;
            }
        }

        return $leftIndex;
    }

    // sort ascending depends on beforeTax
    public function quickSort(&$arr, $leftIndex, $rightIndex)
    {
        $index = $this->partition($arr, $leftIndex, $rightIndex);
        if ($leftIndex < $index - 1) {
            $this->quickSort($arr, $leftIndex, $index - 1);
        }
        if ($index < $rightIndex) {
            $this->quickSort($arr, $index, $rightIndex);
        }
    }

    public function partition1(&$arr, $leftIndex, $rightIndex)
    {
        $pivot = $arr[($leftIndex + $rightIndex) / 2]['beforeTax'];

        while ($leftIndex <= $rightIndex) {
            while ($arr[$leftIndex]['beforeTax'] > $pivot) {
                $leftIndex++;
            }
            while ($arr[$rightIndex]['beforeTax'] < $pivot) {
                $rightIndex--;
            }
            if ($leftIndex <= $rightIndex) {
                $tmp = $arr[$leftIndex];
                $arr[$leftIndex] = $arr[$rightIndex];
                $arr[$rightIndex] = $tmp;
                $leftIndex++;
                $rightIndex--;
            }
        }

        return $leftIndex;
    }

    // sort descending depends on beforeTax
    public function quickSort1(&$arr, $leftIndex, $rightIndex)
    {
        $index = $this->partition1($arr, $leftIndex, $rightIndex);
        if ($leftIndex < $index - 1) {
            $this->quickSort1($arr, $leftIndex, $index - 1);
        }
        if ($index < $rightIndex) {
            $this->quickSort1($arr, $index, $rightIndex);
        }
    }

    public function tmpHotels($destinationId, $hotelName_en, $hotelName_zh)
    {
        $model = Hotel::where('desId', $destinationId)
                      ->where('provider', 'Expedia');

        if (!empty($hotelName_en)) {
            if (!empty($hotelName_zh)) {
                $model = $model->where(function ($query) use ($hotelName_en, $hotelName_zh) {
                    $query->where('name', 'like', "%$hotelName_en%")
                          ->orWhere('name_zh', 'like', "%$hotelName_zh%");
                });
            } else {
                $model = $model->where('name', 'like', "%$hotelName_en%");
            }
        }

        return $model->orderBy('discount', 'desc')
                     ->limit(20)
                     ->get();
    }

    public function getScenicArea($destinationId)
    {
        return ScenicArea::where('destination_id', $destinationId)
                         ->whereNotNull('northeast_lat')
                         ->orderBy('sequence', 'desc')
                         ->get();
    }

    public function getScenicAreaByIds($scenicAreaIds)
    {
        return ScenicArea::whereIn('id', $scenicAreaIds)
                         ->orderBy('sequence', 'asc')
                         ->get();
    }

    private function getScenicAreaHotels($hotels, $scenicAreaArr)
    {
        $scenicHotels = [];
        foreach ($hotels as $hotel) {
            if (isset($hotel['latitude']) && isset($hotel['longitude'])) {
                foreach ($scenicAreaArr as $scenicArea) {
                    if (isset($scenicArea['northeast_lat']) && isset($scenicArea['northeast_lng']) && isset($scenicArea['southwest_lat']) && isset($scenicArea['southwest_lng'])) {
                        if ($scenicArea['northeast_lat'] > $hotel['latitude'] && $hotel['latitude'] > $scenicArea['southwest_lat'] && $scenicArea['northeast_lng'] > $hotel['longitude'] && $hotel['longitude'] > $scenicArea['southwest_lng']) {
                            $scenicHotels[] = $hotel;
                            break;
                        }
                    } else {
                        continue;
                    }
                }
            } else {
                continue;
            }
        }

        return $scenicHotels;
    }

    private function groupHotelsByDiscount($hotels, $discountLimit, $showAllDiscount)
    {
        $hotelsWithDiscount = array_filter($hotels, function ($hotel) {
            return $hotel['discount'] > 0;
        });

        $firstPageDiscounts = $hotelsWithDiscount;
        if (($showAllDiscount === false || $showAllDiscount === 'false') && count($hotelsWithDiscount) > $discountLimit) {
            $firstPageDiscounts = array_slice($hotelsWithDiscount, 0, $discountLimit, true);
        }

        $hotelsWithoutDiscount = array_diff_key($hotels, $firstPageDiscounts);
        $hotelsWithoutDiscount = $this->groupHotelsByLowestFlg($hotelsWithoutDiscount);

        return array_merge($firstPageDiscounts, $hotelsWithoutDiscount);
    }

    private function groupHotelsByLowestFlg($hotels)
    {
        $hotelsWithLowestFlg = array_filter($hotels, function ($hotel) {
            return $hotel['lowestFlg'] > 0;
        });
        $hotelsWithoutLowestFlg = array_diff_key($hotels, $hotelsWithLowestFlg);

        return array_merge($hotelsWithLowestFlg, $hotelsWithoutLowestFlg);
    }

    private function orderHotelsByDistance($hotels, $lat, $lng)
    {
        foreach ($hotels as $key => &$hotel) {
            if (isset($hotel['latitude']) && isset($hotel['longitude'])) {
                $distance = $this->distance($lat, $lng, $hotel['latitude'], $hotel['longitude']);
                $hotel['scenicDistance'] = $distance;
                unset($distance);
            }
        }

        $distancedHotels = array_filter($hotels, function ($hotel) {
            return array_has($hotel, 'scenicDistance');
        });
        $unDistancedHotels = array_diff_key($hotels, $distancedHotels);

        usort($distancedHotels, function ($a, $b){
            return $a['scenicDistance'] * 100 - $b['scenicDistance'] * 100;
        });

        $hotels = array_merge($distancedHotels, $unDistancedHotels);

        unset($distancedHotels, $unDistancedHotels);
        return $hotels;
    }

    private function distance($lat1, $lon1, $lat2, $lon2)
    {
        $radLat1 = pi() * $lat1 / 180;
        $radLat2 = pi() * $lat2 / 180;
        $theta = $lon1 - $lon2;
        $radTheta = pi() * $theta / 180;
        $dist = sin($radLat1) * sin($radLat2) +  cos($radLat1) * cos($radLat2) * cos($radTheta);
        $dist = acos($dist);
        $dist = $dist * 180 / pi();
        $miles = $dist * 60 * 1.1515;

        return round($miles * 1.609344, 2); // km
    }

}
