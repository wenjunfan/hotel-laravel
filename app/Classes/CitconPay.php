<?php

namespace App\Classes;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CitconPay {
	/**
	 * This is a private function to setup citconpay
	 * @return array
	 */
	private static function citconpaySetupHeader()
	{
		$citconAccount = [
			'endpoint' => config('app.citcon_endpoint'),
			'token'    => config('app.citcon_token'),
		];

		$curlSetupArray = array(
			CURLOPT_POST           => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTPHEADER     => array(
				'Authorization: Bearer ' . $citconAccount['token'],
				'Access-Control-Allow-Origin: *',
			),
		);

		return [
			'citconEndpoint' => $citconAccount['endpoint'],
			'curlSetupArray' => $curlSetupArray,
		];
	}

	private static function get_client_ip()
	{
		if (function_exists('apache_request_headers')) {
			$headers = apache_request_headers();
		} else {
			$headers = $_SERVER;
		}

		if (array_key_exists('X-Forwarded-For', $headers) && filter_var($headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
			$the_ip = $headers['X-Forwarded-For'];
		} elseif (array_key_exists('HTTP_X_FORWARDED_FOR', $headers) && filter_var($headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
			$the_ip = $headers['HTTP_X_FORWARDED_FOR'];
		} else {

			$the_ip = filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
		}

		return $the_ip;
	}

	public static function qrpay($amount, $currency, $reference, $vendor)
	{
		$domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : config('app.url');
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://';
		$baseUrl = $protocol . $domain . "/ccpay";

		$ipn_url = $baseUrl . "/ipn/pc";
		$callback_url = $baseUrl . "/confirm";
		Log::info('citcon qr callback url: ' . $callback_url);

		if (config('app.env') == 'local') {
			$amount = 1; // convert to 1 cent for testing
		} else {
			$amount = $amount * 100;
		}

        $ip = self::get_client_ip();

		$params =
			"vendor=" . urlencode($vendor) .
			"&currency=" . urlencode($currency) .
			"&amount=$amount" .
			"&reference=" . urlencode($reference) .
			"&ipn_url=" . urlencode($ipn_url) .
			"&callback_url=" . urlencode($callback_url) .
			"&allow_duplicates=yes" .
            "&note=" . $ip;

        $headerSetupArray = self::citconpaySetupHeader();
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $headerSetupArray['citconEndpoint'] . "/pay_qr");
		curl_setopt_array($curl, $headerSetupArray['curlSetupArray']);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

		$result = curl_exec($curl);
		curl_close($curl);

		return $result . 'hide_logo=true';
	}

	public static function h5pay($amount, $currency, $reference, $vendor, $isWechatBrowser = false)
	{
		$domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : config('app.url');
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://';
		$baseUrl = $protocol . $domain;

		$ipn_url = $baseUrl . "/ccpay/ipn/mobile";
        $langId = session('language');
        $langStr = 'cn';
        switch ($langId) {
            case 1:
                $langStr = 'en';
            case 2:
                $langStr = 'tw';
        }
		$callback_url = $baseUrl . "/continue/" . $reference . '/' . $vendor . '?lang=' . $langStr;
		Log::info('citcon h5 callback url: ' . $callback_url);

		if (config('app.env') == 'local') {
			$amount = 1; // convert to 1 cent for testing
		} else {
			$amount = $amount * 100;
		}

        $ip = self::get_client_ip();

        $params =
			"vendor=" . urlencode($vendor) .
			"&currency=" . urlencode($currency) .
			"&amount=$amount" .
			"&reference=" . urlencode($reference) .
			"&ipn_url=" . urlencode($ipn_url) .
			"&callback_url=" . urlencode($callback_url) .
			"&allow_duplicates=yes" .
            "&note=" . $ip;

		if (!$isWechatBrowser) {
			$params .=
				"&mweb=yes" .
				"&client_ip=" . urlencode($ip);
		}

		$headerSetupArray = self::citconpaySetupHeader();
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $headerSetupArray['citconEndpoint'] . "/pay");
		curl_setopt_array($curl, $headerSetupArray['curlSetupArray']);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

		$result = curl_exec($curl);
		curl_close($curl);

		return $result;
	}

	public static function inquire($reference)
	{
		$headerSetupArray = self::citconpaySetupHeader();

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $headerSetupArray['citconEndpoint'] . "/inquire");
		curl_setopt_array($curl, $headerSetupArray['curlSetupArray']);
		$params =
			"reference=" . urlencode($reference) .
			"&inquire_method=real";
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
		$transaction = curl_exec($curl);
		curl_close($curl);

		return json_decode($transaction);
	}

	public static function refund($currency, $transactionId, $amount, $reason = '')
	{
		$headerSetupArray = self::citconpaySetupHeader();
		$params =
			"transaction_id=" . urlencode($transactionId) .
			"&amount=$amount" .
			"&currency=" . urlencode($currency) .
			"&reason=" . urlencode($reason);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $headerSetupArray['citconEndpoint'] . "/refund");
		curl_setopt_array($curl, $headerSetupArray['curlSetupArray']);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

		$result = curl_exec($curl);
		curl_close($curl);
        $actuallamount = $amount*0.01;
        try {
            Mail::raw($result, function ($message) use ($transactionId, $currency,$actuallamount, $reason) {
                $message->subject('Citcon Refund for id ' . $transactionId . ' (' . $currency. ' '. $actuallamount. ')');
                $message->to(config('staff-emails.ann'));
            });
        } catch (\Exception $e) {
            Log::info(' error with Citcon refund email to 550 ' . $e->getMessage());
        }

		return json_decode($result); // it has two properties: refunded and status
	}
}