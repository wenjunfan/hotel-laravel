<?php

namespace App\Classes;

use Illuminate\Support\Facades\Log;

/*
 * sina&sougo的官方短连接下架了，换成破解的
 */
class SinaUrl{
    //新浪APPKEY http://open.weibo.com/apps/3581104441/info/basic
    const APPKEY='3581104441';
    //CURL
    private static function CURLQueryString($url){
        //设置附加HTTP头
        $addHead=array("Content-type: application/json");
        //初始化curl
        $curl_obj=curl_init();
        //设置网址
        curl_setopt($curl_obj,CURLOPT_URL,$url);
        //附加Head内容
        curl_setopt($curl_obj,CURLOPT_HTTPHEADER,$addHead);
        //是否输出返回头信息
        curl_setopt($curl_obj,CURLOPT_HEADER,0);
        //将curl_exec的结果返回
        curl_setopt($curl_obj,CURLOPT_RETURNTRANSFER,1);
        //设置超时时间
        curl_setopt($curl_obj,CURLOPT_TIMEOUT,8);
        //执行
        $result=curl_exec($curl_obj);
        //关闭curl会话
        curl_close($curl_obj);
        return $result;
    }

    //处理返回结果
     private static function doWithResult($result,$field){
        $result=json_decode($result,true);
        return isset($result[0][$field])?$result[0][$field]:'';
    }

    //获取Sina  t.cn/长链接（19年11月失效）
    public static function getSinaLong($url){
        $url='http://api.t.sina.com.cn/short_url/expand.json?source='.self::APPKEY.'&url_short='.$url;
        $result=self::CURLQueryString($url);
        return self::doWithResult($result,'url_long');
    }

    //获取Sina t.cn/短链接（19年11月失效）
    public static function getSinaShort($url){
        $url='http://api.t.sina.com.cn/short_url/shorten.json?source='.self::APPKEY.'&url_long='.$url;
        $result=self::CURLQueryString($url);
        Log::info('test sina short url '. $result);
        return self::doWithResult($result,'url_short');
    }

    //获取Tencent url.cn/短链接（19年12月失效）
    public static function getTencentShort($url){
        $url = 'http://sa.sogou.com/gettiny?url=' . $url;
        $result = self::CURLQueryString($url);
        Log::info('生成Tencent短连接' .$result);
        return $result;
    }

    //获取t.cn/ & url.cn/短链接
    //破解的qingmeidwz还可以,而且新浪和腾讯都可以https://www.codetd.com/en/article/8317201
    //http://pay.jump-api.cn/tcn/web/?uid=5aqIXNZfNd&fid=zss&site_id=qingmeidwz.cn&role=admin&url_long=http://api.t.sina.com.cn/short_url/expand.json
    //http://pay.jump-api.cn/urlcn/panel?uid=xjoZ3CutdI&fid=zss&site_id=qingmeidwz.cn&role=adminurl_long=http://api.t.sina.com.cn/short_url/expand.json

    //获取Sina t.cn/短链接
    public static function gettcnShort($url){
        $url = 'http://qingmeidwz.cn/shorten.php?url_long=' . $url; //t.cn
        $result = self::CURLQueryString($url);
        Log::info('生成 t.cn/短连接' .$result);
        return $result;
    }

    //获取url.cn/短链接
    public static function geturlcnShort($url){
        $url = 'http://qingmeidwz.cn/wxshorturl.do?url_long=' . $url; //url.cn/
        $result = self::CURLQueryString($url);
        Log::info('生成url.cn/短连接' .$result);
        return $result;
    }

    public static function getShort($url)
    {
       $result = Self::gettcnShort($url);
       return $result;
    }
}
