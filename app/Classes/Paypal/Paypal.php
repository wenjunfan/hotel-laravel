<?php

namespace App\Classes\Paypal;

use Illuminate\Support\Facades\Log;

/*
 * paypal for b site
 */

class Paypal
{
    public static function auth($creditCardInfo, $amount)
    {
        $account = config('constants.paypal_account');
        if (config('app.env') == 'local') {
            $amount = 1;
            $account = config('constants.paypal_account_test');
        }

        $res = "METHOD=doDirectPayment" . "&VERSION=" . $account['version'] . "&USER=" . urlencode($account['userName']) . "&PWD=" . urlencode($account['password']) .
            "&SIGNATURE=" . urlencode($account['signature']) . "&PAYMENTACTION=Authorization" . "&AMT=" . $amount .
            "&CREDITCARDTYPE=" . $creditCardInfo->type . "&ACCT=" . $creditCardInfo->number .
            "&EXPDATE=" . $creditCardInfo->month . $creditCardInfo->year . "&CVV2=" . $creditCardInfo->cvc .
            "&FIRSTNAME=" . urlencode($creditCardInfo->firstName) . "&LASTNAME=" . urlencode($creditCardInfo->lastName) .
            "&STREET=" . urlencode($creditCardInfo->street) . "&STREET2=" . urlencode($creditCardInfo->street2) . "&CITY=" . urlencode($creditCardInfo->city) .
            "&STATE=" . urlencode($creditCardInfo->state) . "&ZIP=" . urlencode($creditCardInfo->zip) . "&COUNTRYCODE=" . $creditCardInfo->countryCode . "&CURRENCYCODE=USD";

        return self::request($res, $account);
    }

    public static function capture($authorizationId, $amount)
    {
        $account = config('constants.paypal_account');
        if (config('app.env') == 'local') {
            $amount = 1;
            $account = config('constants.paypal_account_test');
        }

        $res = "METHOD=DoCapture" . "&VERSION=" . $account['version'] . "&USER=" . urlencode($account['userName']) . "&PWD=" . urlencode($account['password']) .
            "&SIGNATURE=" . urlencode($account['signature']) . "&AUTHORIZATIONID=" . $authorizationId . "&COMPLETETYPE=Complete" . "&AMT=" . $amount;

        return self::request($res, $account);
    }

    public static function doVoid($authorizationId)
    {
        $account = config('constants.paypal_account');
        if (config('app.env') == 'local') {
            $account = config('constants.paypal_account_test');
        }

        $res = "METHOD=DoVoid" . "&VERSION=" . $account['version'] . "&USER=" . urlencode($account['userName']) . "&PWD=" . urlencode($account['password']) .
            "&SIGNATURE=" . urlencode($account['signature']) . "&AUTHORIZATIONID=" . $authorizationId;

        return self::request($res, $account);
    }

    public static function refund($transactionId, $refundType, $amount, $note)
    {
        $account = config('constants.paypal_account');
        if (config('app.env') == 'local') {
            $amount = 1;
            $account = config('constants.paypal_account_test');
        }

        $res = "METHOD=RefundTransaction" . "&VERSION=" . $account['version'] . "&USER=" . urlencode($account['userName']) . "&PWD=" . urlencode($account['password']) .
            "&SIGNATURE=" . urlencode($account['signature']) . "&TRANSACTIONID=" . urlencode($transactionId) . "&REFUNDTYPE=" . urlencode($refundType) .
            "&NOTE=" . urlencode($note) . "&CURRENCYCODE=USD";

        if (strtoupper($refundType) == "PARTIAL") { // Full or Partial
            $res = $res . "&AMT=" . $amount;
        }

        return self::request($res, $account);
    }

    private static function request($res, $account)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $account['endpoint']);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        //turning off the server and peer verification(TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        //setting the res as POST FIELD to curl
        curl_setopt($ch, CURLOPT_POSTFIELDS, $res);
        //getting response from server
        Log::info('before exec curl');
        $response = curl_exec($ch);
        Log::info('after exec curl');
        if (curl_errno($ch)) {
            Log::info('curl has error');

            // moving to display page to display curl errors
            return [
                "success" => false,
                "request" => $res,
                "errorNo" => curl_errno($ch),
                "errorMessage" => curl_error($ch),
            ];
        } else {
            Log::info('curl does not has error');
            //closing the curl
            curl_close($ch);
            Log::info('curl closed');

            return [
                "success" => true,
                "request" => $res,
                "response" => $response,
            ];
        }
    }
}
