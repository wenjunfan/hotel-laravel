<?php

namespace App\Classes\Paypal;

use Illuminate\Support\Facades\Log;

class PayResult extends PaypalResult
{
    public $request;
    public $ack;
    public $version;
    public $build;
    public $correlationId;
    public $errors = "";
    public $avsCode = "";
    public $cvv2Match = "";
    public $transactionId = "";
    public $amount;
    public $currency;
    public $created_at;
    public $authorizationId;

    public function __construct($request, $nvpstr)
    {
        $this->request = $request;
        $r = $this->deformat($nvpstr);

        $this->ack = $r['ACK'];
        $this->version = $r['VERSION'];
        $this->build = $r['BUILD'];
        $this->correlationId = $r['CORRELATIONID'];

        if (isset($r['AMT'])) {
            $this->amount = $r['AMT'];
        }
        if (isset($r['CURRENCYCODE'])) {
            $this->currency = $r['CURRENCYCODE'];
        }
        $this->created_at = $r['TIMESTAMP'];
        //$this->created_at = strtotime(date('Y-m-d h:i:s'));
        Log::info('gg0000001');
        if ($this->ack == "Success") {
            if (isset($r['AVSCODE'])) {
                $this->avsCode = $r['AVSCODE'];
            } else {
                $this->avsCode = "";
            }
            if (isset($r['CVV2MATCH'])) {
                $this->cvv2Match = $r['CVV2MATCH'];
            } else {
                $this->cvv2Match = "";
            }
            if (isset($r['AUTHORIZATIONID'])) {
                $this->authorizationId = $r['AUTHORIZATIONID'];
            } else {
                $this->authorizationId = $r['TRANSACTIONID'];
            }
            if (isset($r['TRANSACTIONID'])) {
                $this->transactionId = $r['TRANSACTIONID'];
            }
        } else {
            $errors = [];

            for ($i = 0; ; $i++) {
                if (array_key_exists('L_ERRORCODE' . $i, $r)) {
                    $errors[] = [
                        "errorCode"    => $r['L_ERRORCODE' . $i],
                        "shortMessage" => $r['L_SHORTMESSAGE' . $i],
                        "longMessage"  => isset($r['L_LONGMESSAGE' . $i]) ? $r['L_LONGMESSAGE' . $i] : "",
                        "severityCode" => $r['L_SEVERITYCODE' . $i],
                    ];
                } else {
                    break;
                }
            }

            $this->errors = json_encode($errors);
        }

        Log::info('gg0000002');
    }
}
