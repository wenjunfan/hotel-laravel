<?php

namespace App\Classes;

use App\Jobs\SendAlertEmail;
use App\Traits\HttpGet;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Infusionsoft\Infusionsoft;
use Infusionsoft\Token;

class InfusionsoftAPI
{
    use HttpGet;

    public $token;
    public $client;

    public function __construct(Request $request = null)
    {
        $this->client = new Infusionsoft(config('constants.infusionsoft'));

        if ($request && $request->has('code')) {
            $redirect_url = config('app.url') . '/infusionsoft/auth';
            if ($request && $request->has('email')) {
                $params = [
                    'email' => $request->input('email'),
                    'lang' => $request->input('lang')
                ];
                $redirect_url .= '?' . http_build_query($params);
            }
            $this->client->setRedirectUri($redirect_url);
            return false;
        }

        try {
            if (!Cache::has('infusionsoft_token')) {
                $redirect_url = config('app.url') . '/infusionsoft/auth';
                if ($request && $request->has('subscribeEmail')) {
                    $params = [
                        'email' => $request->input('inf_field_Email'),
                        'lang' => session('language', 0) ? 'cn' : 'en',
                    ];
                    $redirect_url .= '?' . http_build_query($params);
                }

                $this->client->setRedirectUri($redirect_url);
                $this->authenticateWithCode($request ? $request->path() : '');
                return false;
            } else {
                $this->token = Cache::get('infusionsoft_token');
            }

            $this->client->setToken($this->token);

            if ($this->client->isTokenExpired()) {
                $this->refreshAccessToken();
            }
        } catch (\Exception $exception) {
            Log::error('Infusionsoft error: ' . $exception->getMessage());
        }

        return false;
    }

    // temporarily use this method to create a record if not exist
    private function authenticateWithCode($original_path)
    {
        if (config('app.enable_email_campaign')) {
            $content = 'Infusionsoft token config missing in redis cache, queue name: ' . config('app.email_queue') .
                ' <a href="' . $this->client->getAuthorizationUrl() . '">Click here to authorize</a>';

            dispatch(new SendAlertEmail(config('staff-emails.ann'), 'infusionsoft access token missing', null, $content));
        }
    }

    private function refreshAccessToken()
    {
        try {
            $token = $this->client->refreshAccessToken();

            $this->token = $token;

            Cache::forever('infusionsoft_token', $this->token);
        } catch (\Exception $exception) {
            Log::error('infusionsoft refresh token failed');
        }
    }

    /**
     * Manually authenticate app
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function authenticate(Request $request)
    {
        $code = $request->input('code');
        if (!empty($code)) {
            try {
                $this->token = $this->client->requestAccessToken($code);
                Cache::forever('infusionsoft_token', $this->token);

                if ($request->has('email')) {
                    $this->createContact($request->input('email'), [
                        [
                            'id' => 18,
                            'content' => 'en'
                        ]
                    ], 'authenticate');
                }
            } catch (\Exception $exception) {
                Log::error('authenticate infusionsoft failed, ' . $exception->getMessage());
            }
        }

        return redirect(config('app.url'));
    }

    /**
     * Being used by client side request and authenticate callback
     * @param string $email
     * @param array $custom_fields
     * @return array|string
     */
    public function createContact($email, $custom_fields, $reason = '')
    {
        if ($this->client->getToken()) {
            if (!empty($email)) {
                // Save the serialized token to the current session for subsequent requests
                $_SESSION['token'] = serialize($this->client->getToken());

                try {
                    // check whether contact already exist to decide whether to send welcome email
                    $ids = $this->client->contacts('xml')->findByEmail($email, []);
                    $is_new = empty($ids);

                    $params = [
                        'duplicate_option' => 'Email',
                        "opt_in_reason" => $reason,
                        'email_addresses' => [
                            [
                                'email' => $email,
                                'field' => 'EMAIL1'
                            ]
                        ],
                        'custom_fields' => $custom_fields
                    ];

                    $contact = $this->client->contacts()->create($params, true);

                    if (isset($contact->id)) {
                        // only send welcome email to new user
                        if ($is_new) {
                            $this->achieveGoals($contact->id, ['createContact']);
                        }

                        // save contact id for future interactions
                        if (isset($_SERVER['SERVER_NAME'])) {
                            setcookie('contact_id', $contact->id, time() + (10 * 365 * 24 * 3600), "/", preg_replace('/^(hotel|www)/', '', $_SERVER['SERVER_NAME']));
                        }
                    }

                    return $contact;
                } catch (\Exception $exception) {
                    Log::error('infusionsoft create contact failed ' . $exception->getMessage());
                    return null;
                }
            }
        } else {
            $this->authenticateWithCode('');
        }
    }

    public function updateContact($contact, $default_fields, $custom_fields)
    {
        $email = $contact['email_addresses'][0]['email'];

        $fields = [
            'duplicate_option' => 'Email',
            'email_addresses' => [
                [
                    'email' => $email,
                    'field' => 'EMAIL1'
                ]
            ],
            'custom_fields' => $custom_fields
        ];

        if (count($default_fields) > 0) {
            $fields = array_merge($fields, $default_fields);
        }

        return $this->client->contacts()->create($fields, true);

    }

    /**
     * @param $contactId
     * @param $tagIds: must be array
     */
    public function applyTags($contactId, $tagIds)
    {
        if (config('app.enable_email_campaign')) {
            $contact = $this->client->contacts()->find($contactId);

            if ($contact) {
                $contact->addTags($tagIds);
            }
        }
    }

    /**
     * @param $name: campaign name
     * @param $contact_id
     */
    public function achieveGoals($contact_id, $names)
    {
        if (config('app.enable_email_campaign')) {
            if (count($names) > 0) {
                foreach ($names as $name) {
                    $this->client->funnels()->achieveGoal('zw451', $name, $contact_id);
                }
            }
        }
    }

}
