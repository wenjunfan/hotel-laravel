<?php

namespace App\Classes\PaypalNew;

use Illuminate\Support\Facades\Log;

/*
 * paypal for c site
 */
class PaypalNew
{

    public static function auth($creditCardInfo, $amount)
    {

        $account = config('constants.paypal_new_account');

        $res =
            "USER=" . $account['user'] .
            "&VENDOR=" . $account['vendor'] .
            "&PARTNER=" . $account['partner'] .
            "&PWD=" . $account['pwd'] .
            "&TRXTYPE=A" .
            "&TENDER=C" .
            "&ACCT=" . $creditCardInfo->number .
            "&EXPDATE=" . $creditCardInfo->month . $creditCardInfo->year .
            "&CVV2=" . $creditCardInfo->cvc .
            "&AMT=" . $amount .
            "&VERBOSITY=HIGH" .
            "&BILLTOFIRSTNAME=" . $creditCardInfo->firstName .
            "&BILLTOLASTNAME=" . $creditCardInfo->lastName .
            "&BILLTOSTREET=" . $creditCardInfo->street .
            "&BILLTOSTREET2=" . $creditCardInfo->street2 .
            "&BILLTOCITY=" . $creditCardInfo->city .
            "&BILLTOSTATE=" . $creditCardInfo->state .
            "&BILLTOZIP=" . $creditCardInfo->zip;

        return self::request($res, $account['endpoint']);
    }

    public static function capture($transactionId, $amount)
    {
        $account = config('constants.paypal_new_account');

        $res =
            "USER=" . $account['user'] .
            "&VENDOR=" . $account['vendor'] .
            "&PARTNER=" . $account['partner'] .
            "&PWD=" . $account['pwd'] .
            "&TRXTYPE=D" .
            "&TENDER=C" .
            "&ORIGID=" . $transactionId .
            "&AMT=" . $amount .
            "&VERBOSITY=HIGH" .
            "&CAPTURECOMPLETE=Y";

        return self::request($res, $account['endpoint']);
    }

    public static function doVoid($transactionId, $amount)
    {
        $account = config('constants.paypal_new_account');

        $res =
            "USER=" . $account['user'] .
            "&VENDOR=" . $account['vendor'] .
            "&PARTNER=" . $account['partner'] .
            "&PWD=" . $account['pwd'] .
            "&TRXTYPE=V" .
            "&TENDER=C" .
            "&AMT=" . $amount .
            "&ORIGID=" . $transactionId .
            "&VERBOSITY=HIGH" .
            "&CAPTURECOMPLETE=Y";

        return self::request($res, $account['endpoint']);
    }

    public static function refund($transactionId, $refundType, $amount)
    {
        $account = config('constants.paypal_new_account');

        $res =
            "USER=" . $account['user'] .
            "&VENDOR=" . $account['vendor'] .
            "&PARTNER=" . $account['partner'] .
            "&PWD=" . $account['pwd'] .
            "&TRXTYPE=C" .
            "&TENDER=C" .
            "&VERBOSITY=HIGH" .
            "&ORIGID=" . $transactionId;

        if (strtoupper($refundType) == "PARTIAL") { // pass amount if it's partial refund
            $res = $res .
                "&AMT=" . $amount;
        }

        return self::request($res, $account['endpoint']);
    }

    private static function request($res, $endpoint)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);

        //turning off the server and peer verification(TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        //setting the res as POST FIELD to curl
        curl_setopt($ch, CURLOPT_POSTFIELDS, $res);

        // Log::info('paypal curl request: ' . print_r($res, true));

        //getting response from server
        $response = curl_exec($ch);
        // Log::info('paypal curl response: ' . print_r($response, true));

        if (curl_errno($ch)) {
            Log::info('paypal curl has error: ' . curl_error($ch));

            // moving to display page to display curl errors
            return [
                "success"      => false,
                "request"      => $res,
                "errorNo"      => curl_errno($ch),
                "errorMessage" => curl_error($ch),
            ];
        } else {
            // Log::info('paypal curl does not has error');
            curl_close($ch);
            // Log::info('paypal curl closed');

            return [
                "success"  => true,
                "request"  => $res,
                "response" => $response,
            ];
        }
    }
}
