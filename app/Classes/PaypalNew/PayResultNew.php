<?php

namespace App\Classes\PaypalNew;

class PayResultNew extends PaypalResultNew {
    public $request;
    public $result;
    public $transactionId;
    public $respMsg;
    public $authCode;
    public $avsAddr;
    public $avsZip;
    public $cvv2Match;
    public $ppref;
    public $correlationId;
    public $iavs;
    public $created_at;

    public function __construct($request, $nvpstr)
    {
        $this->request = $request;
        $r = $this->deformat($nvpstr);

        $this->result = $r['RESULT'];
        $this->transactionId = isset($r['PNREF']) ? $r['PNREF'] : '';
        $this->respMsg = $r['RESPMSG'];

        if (isset($r['PPREF'])) {
            $this->ppref = $r['PPREF'];
        }
        $this->created_at = strtotime(date('Y-m-d h:i:s'));
        if (isset($r['AUTHCODE'])) {
            $this->authCode = $r['AUTHCODE'];
        } else {
            $this->authCode = "";
        }
        if (isset($r['CVV2MATCH'])) {
            $this->cvv2Match = $r['CVV2MATCH'];
        } else {
            $this->cvv2Match = "";
        }
        if (isset($r['CORRELATIONID'])) {
            $this->correlationId = $r['CORRELATIONID'];
        } else {
            $this->correlationId = "";
        }
        if (isset($r['AVSADDR'])) {
            $this->avsAddr = $r['AVSADDR'];
        }
        if (isset($r['AVSZIP'])) {
            $this->avsZip = $r['AVSZIP'];
        }
    }
}
