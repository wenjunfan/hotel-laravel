<?php

namespace App\Classes\PaypalNew;

class RefundResultNew extends PaypalResultNew {

    public $request;
    public $result;
    public $ppref;
    public $respMsg;
    public $created_at;
    public $correlationId;
    public $transactionId;

    public function __construct($request, $nvpstr)
    {
        $this->request = $request;
        $response = $this->deformat($nvpstr);

        $this->result = $response['RESULT'];
        $this->transactionId = $response['PNREF'];
        $this->respMsg = $response['RESPMSG'];
        if (isset($response['PPREF'])) {
            $this->ppref = $response['PPREF'];
        }
        if (isset($response['CORRELATIONID'])) {
            $this->correlationId = $response['CORRELATIONID'];
        }
        if (isset($response['TRANSTIME'])) {
            $this->created_at = $response['TRANSTIME'];
        } else {
            $this->created_at = strtotime(date('Y-m-d h:i:s'));
        }
    }
}
