<?php

namespace App\Classes;

use App\Model\OrderTem;
use App\Model\Order;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Exception;

/**
 * Class HotelAPI
 * @package App\Classes
 */
class HotelAPI
{
    /**
     * @var null
     */
    private $user = null;
    /**
     * @var mixed|string
     */
    private $remote_api = '';

    private $language = 'en';

    /**
     * HotelAPI constructor.
     * @param $user
     */

    function __construct($user)
    {
        if ($user) {
            $this->user = $user;
            $this->remote_api = config('app.usi_hotels_api');
        } else {
            exit;
        }

        $langInt = session('language', 1);
        if ($langInt == 2) {
            $this->language = 'tw';
        } elseif ($langInt == 0) {
            $this->language = 'cn';
        }
    }

    /**
     * Deprecated
     *
     * @param $booker
     * @param $query
     * @param $client
     * @return array|mixed
     */
    public function reserveRooms($booker, $query, $client)
    {
        return $this->getResult('reserve', [
            'booker' => $booker,
            'clientReference' => $client,
            'query' => $query
        ]);
    }

    /**
     * Deprecated
     *
     * @param $hotelIds
     * @param $checkin
     * @param $checkout
     * @param $occupancies
     * @return array|mixed
     */
    public function listRooms($hotelIds, $checkin, $checkout, $occupancies)
    {
        return $this->getResult('listRooms', [
            'checkIn' => $checkin,
            'checkOut' => $checkout,
            'hotelId' => $hotelIds,
            'occupancies' => $occupancies
        ]);
    }

    /**
     * Deprecated
     *
     * @param $hotelId
     * @param $checkin
     * @param $checkout
     * @param $rateKeys
     * @return array|mixed
     */
    public function checkPrice($hotelId, $checkin, $checkout, $rateKeys)
    {
        return $this->getResult('checkPrice', [
            'checkIn' => $checkin,
            'checkOut' => $checkout,
            'hotelId' => $hotelId,
            'rateKeys' => $rateKeys
        ]);
    }

    /**
     * Deprecated
     *
     * @param $orderReference
     * @return array|mixed
     */
    public function getOrderStatus($orderReference)
    {
        return $this->getResult('getOrderStatus', [
            'orderReference' => $orderReference
        ]);
    }

    /**
     * search hotel by hotelId
     *
     * @param $hotelIds
     * @param $checkin
     * @param $checkout
     * @param $occupancies
     * @return array|mixed
     */
    public function searchHotelsByIds($hotelIds, $checkin, $checkout, $occupancies)
    {
        $name = 'searchHotels';
        $data = [
            'checkIn' => $checkin,
            'checkOut' => $checkout,
            'hotelId' => $hotelIds,
            'occupancies' => $occupancies
        ];
        $url = $this->remote_api . '/' . $name;
        $input = [
            'auth' => $this->auth(),
            'data' => $data,
        ];

        $r = $this->post($url, $input);

        if ($r === false) {
            return [
                'success' => false,
                'errorMessage' => '网络缘故，暂时无法预订这家酒店，请稍后再试或者选择其他的酒店。'
            ];
        }

        return json_decode($r, true);
    }

    /**
     * search hotels by destinationId or hotelId
     *
     * @param $destinationId
     * @param $hotelIds
     * @param $checkin
     * @param $checkout
     * @param $occupancies
     * @return array|mixed
     */
    public function searchHotels($destinationId, $hotelIds, $checkin, $checkout, $occupancies)
    {
        if (count($hotelIds) > 0) {
            return $this->searchHotelsByIds($hotelIds, $checkin, $checkout, $occupancies);
        } else {
            return $this->searchHotelsByDestination($destinationId, $checkin, $checkout, $occupancies);
        }
    }

    /**
     * search hotels by destinationId
     *
     * @param $destinationId
     * @param $checkin
     * @param $checkout
     * @param $occupancies
     * @return array|mixed
     */
    public function searchHotelsByDestination($destinationId, $checkin, $checkout, $occupancies)
    {
        $name = 'searchHotels';
        $data = [
            'checkIn' => $checkin,
            'checkOut' => $checkout,
            'destinationId' => $destinationId,
            'language' => $this->language,
            'occupancies' => $occupancies
        ];
        $url = $this->remote_api . '/' . $name;
        $input = [
            'auth' => $this->auth(),
            'data' => $data,
        ];

        $r = $this->post($url, $input);

        if ($r === false) {
            return [
                'success' => false,
                'errorMessage' => '网络缘故，暂时无法预订这家酒店，请稍后再试或者选择其他的酒店。'
            ];
        }
        return json_decode($r, true);
    }

    /**
     * search for available rooms
     * @param $sessionKey
     * @param $hotelId
     * @return array|mixed
     */
    public function searchRooms($sessionKey, $hotelId)
    {
        $name = 'searchRooms';
        $hotel_Id = [];
        $hotel_Id[] = $hotelId;
        $data = [
            'sessionKey' => $sessionKey,
            'hotelId' => $hotel_Id,
            'language' => $this->language,
        ];
        $url = $this->remote_api . '/' . $name;
        $input = [
            'auth' => $this->auth(),
            'data' => $data,
        ];

        $r = $this->post($url, $input);

        if ($r === false) {
            return [
                'success' => false,
                'errorMessage' => '网络缘故，暂时无法预订这家酒店，请稍后再试或者选择其他的酒店。'
            ];
        }
        return json_decode($r, true);
    }

    /**
     * check room availability with room reference
     *
     * @param $references
     * @return array|mixed
     */
    public function checkRooms($references)
    {
        $name = 'checkRooms';
        $data = [
            'rooms' => $references,
	         'language'   => $this->language,
        ];
        $url = $this->remote_api . '/' . $name;
        $input = [
            'auth' => $this->auth(),
            'data' => $data,
        ];

        $r = $this->post($url, $input);
        if($r === null){
            try{
                Mail::raw(json_encode($input), function ($message) {
                    $message->subject(config('app.env') . ': CheckRoom Failed, please check API');
                    $message->to(config('staff-emails.henry'))->cc(config('staff-emails.ann'))->cc(config('staff-emails.jie'));
                });
            }catch (Exception $e) {
                Log::error('send CheckRoom error email failed: ' . $e->getMessage());
            }
        }

        if ($r === false || $r === null) {
            return [
                'success'      => false,
                'errorId'      => 'IT1003',
                'errorMessage' => '网络缘故，暂时无法预订这家酒店，请稍后再试或者选择其他的酒店。'
            ];
        }

        return json_decode($r, true);
    }

    /**
     * reserve rooms from upper api
     * @param $booker
     * @param $rooms
     * @param $specialRequest
     * @return array|mixed
     */
    public function bookRooms($booker, $rooms, $specialRequest)
    {
        $name = 'book';

        if (isset($rooms[0]['roomDetails'])) {
            unset($rooms[0]['roomDetails']);
        }

        if (isset($booker['countryCodePhone'])) {
            $booker['phone'] = $booker['countryCodePhone'] . $booker['validPhoneNumber'];
        } elseif (isset($booker['phone_country'])) {
            $booker['phone'] = $booker['phone_country'] . $booker['phone'];
        }

        $data = [
            'booker' => $booker,
            'rooms' => $rooms,
            'specialRequest' => $specialRequest,
            'clientReference' => '117 Book',
        ];
        $url = $this->remote_api . '/' . $name;

        if (!config('app.expedia_bookable') && strpos($rooms[0]['roomReference'], 'e:') !== false) {
            $message = 'Expedia booking is disabled';
            Log::info('manually book failed message: ' . $message);

            return [
                'success' => false,
                'errorMessage' => $message
            ];
        } else {
            $input = [
                'auth' => $this->auth(),
                'data' => $data,
            ];

            $r = $this->post($url, $input);

            if ($r === false) {
                return [
                    'success'      => false,
                    'errorId'      => 'IT1001',
                    'errorMessage' => '网络状况不佳,请联系客服查看订单详情'
                ];
            }

            return json_decode($r, true);
        }
    }

    /**
     * book rooms after confirm not duplicate
     *
     * @param $booker
     * @param $rooms
     * @param $specialRequest
     * @return array|mixed
     */
    public function bookRoomsDu($booker, $rooms, $specialRequest)
    {
        $name = 'book';
        $data = [
            'booker' => $booker,
            'rooms' => $rooms,
            'specialRequest' => $specialRequest,
            'clientReference' => '117 Book',
            'duplicate' => '1',
        ];
        $url = $this->remote_api . '/' . $name;

        if (!config('app.expedia_bookable') && strpos($rooms[0]['roomReference'], 'e:') !== false) {
            $message = 'Expedia booking is disabled';
            Log::info('manually book failed message: ' . $message);

            return [
                'success'      => false,
                'errorMessage' => $message
            ];
        } else {
            $input = [
                'auth' => $this->auth(),
                'data' => $data,
            ];

            $r = $this->post($url, $input);

            if ($r === false) {
                return [
                    'success'      => false,
                    'errorId'      => 'IT1001',
                    'errorMessage' => '网络状况不佳,请联系客服查看订单详情'
                ];
            }

            return json_decode($r, true);
        }
    }

    /**
     * @param $orderReference
     * @return array|mixed
     */
    public function cancelOrder($orderReference)
    {
        $name = 'cancel';
        $data = [
            'orderReference' => $orderReference,
        ];
        $url = $this->remote_api . '/' . $name;
        $input = [
            'auth' => $this->auth(),
            'data' => $data,
        ];

        $r = $this->post($url, $input);

        if ($r === false) {
            return [
                'success'      => false,
                'errorMessage' => '网络缘故，取消订单失败。'
            ];
        }

        return json_decode($r, true);
    }

    /**
     * register user on tours site
     *
     * @param $orderReference
     * @param $booker
     * @return array|mixed
     */
    public function regUser($orderReference, $booker)
    {
        $name = 'reguser';
        $data = [
            'orderReference' => $orderReference,
            'booker'         => $booker
        ];
        $url = $this->remote_api . '/' . $name;
        $input = [
            'auth' => $this->auth(),
            'data' => $data,
        ];

        $r = $this->post($url, $input);

        if ($r === false) {
            Log::info($orderReference . ' --- Network caused regUser failed : ' . $booker);
            return [
                'success'      => false,
                'errorMessage' => '网络缘故，注册大团用户失败。'
            ];
        }

        return json_decode($r, true);
    }

    /**
     * check book rooms result when book failed
     *
     * @param $orderReference
     * @return array|mixed
     */
    public function checkReservation($orderReference)
    {
        //第一次预定成功第二次失败,直接返回失败,不然java会返回第一次预定成功的信息,并导致上游无变化自己數據庫的信息被更新
        $bookedb4NotNow = OrderTem::where('orderReference', $orderReference)->get();
        if($bookedb4NotNow){
            $duplicateId = Order::where('orderReference',$orderReference)->first()->id;
            Log::info(' ------Booking failed, but was success before with same room reference, : '.$orderReference.', ' . $duplicateId);
            return [
                'success' => false,
                'errorId' => 'ER1414',
                'errorMessage' =>  $duplicateId
            ];
        }else{
            $result = $this->getJavaResult('checkReservation', [
                'orderReference' => $orderReference,
            ]);
            Log::info('----- java checkReservation result ------ ' . json_encode($result));
            return $result;
        }
    }

    /**
     * fraud check when book rooms
     *
     * @param $data
     * @return array|mixed
     */
    public function checkUser($data)
    {
        $name = 'fraudCheck';
        $url = $this->remote_api . '/' . $name;
        $input = [
            'auth' => $this->auth(),
            'data' => $data,
        ];

        $r = $this->post($url, $input);

        if ($r === false) {
            return [
                'success' => false,
                'errorMessage' => '网络缘故，fraud check失败。'
            ];
        }

        return json_decode($r, true);
    }

    /**
     * get extra hotel info on hotel detail page
     */
    public function getExtraHotel($data)
    {
        $url = $this->remote_api . '/getExtraHotelData';
        $input = [
            'auth' => $this->auth(),
            'data' => $data
        ];

        $r = $this->post($url, $input);

        if ($r === false) {
            return [
                'success' => false,
                'errorMessage' => '网络缘故，请求酒店数据失败。'
            ];
        }

        return json_decode($r, true);
    }

    public function getBookingRooms($data)
    {
        $url = $this->remote_api . '/getRooms';
        $input = [
            'auth' => $this->auth(),
            'data' => $data
        ];

        $r = $this->post($url, $input);

        if ($r === false) {
            return [
                'success' => false,
                'errorMessage' => '网络缘故，请求booking房型数据失败。'
            ];
        }

        return json_decode($r, true);
    }

    public function pairBookingRooms($data)
    {
        $url = $this->remote_api . '/importRooms';
        $input = [
            'auth' => $this->auth(),
            'data' => $data
        ];

        $r = $this->post($url, $input);

        if ($r === false) {
            return [
                'success' => false,
                'errorMessage' => '网络缘故，匹配booking房型数据失败。'
            ];
        }

        return json_decode($r, true);
    }

    public function reverseBookingRooms($data)
    {
        $url = $this->remote_api . '/reverseRooms';
        $input = [
            'auth' => $this->auth(),
            'data' => $data
        ];

        $r = $this->post($url, $input);

        if ($r === false) {
            return [
                'success' => false,
                'errorMessage' => '网络缘故，取消booking房型匹配数据失败。'
            ];
        }

        return json_decode($r, true);
    }

    /**
     * C端后台，获取一个或多个酒店的 ratio
     *  /affiliate/price-ratio
     *    "data": { "hotelId": [ 1063212, 1049443, 1535796 ] }
     * @param $data
     * @return array|mixed
     */
    public function getHotelRatio($data)
    {

        $data = [
            'auth' => $this->auth(),
            'data' => ['hotelId' => $data],
        ];
        $url = $this->remote_api . '/getHotelsInfo';

        return $this->post($url, $data);
    }

    /**
     * C端后台，更新一个或多个酒店的 一个或多个ratio
     * request data demo
     * {
     *      "auth": {
     *      "username": "***@117book.com",
     *      "password": "******"
     *      },
     *      "data": {
     *           "hotelId":  [ "1535796" ],
     *           "name": "Holiday Inn El Monte - Los Angeles",
     *           "name_zh": "艾尔蒙特假日酒店",
     *           "description": "Holiday Inn El Monte ",
     *           "description_zh": "酒店位于埃尔蒙特（El Monte）",
     *           "priceRatio": 100,
     *           "priceRatioToC": 50,
     *           "priceRatioToCtrip": 20,
     *           "priceRatioToAli": 10,
     *           "priceRatioTemp": -50
     *      }
     * }
     * @param $ids
     * @param $update_data
     * @return bool|string|null
     */
    public function postHotelRatio($ids, $update_data)
    {
        $data = [
            'auth' => $this->auth(),
            'data' => $update_data,
        ];
        $data['data']['hotelId'] = $ids;
        unset($data['data']['name'], $data['data']['name_zh'], $data['data']['address']); // 删掉数组可能存在的，但不需要更新的值
        $url = $this->remote_api . '/updateHotelsPriceRatio';

        return $this->post($url, $data);
    }

    /**
     * C端后台，更新一个酒店的信息
     * request data demo
     * {
     *      "auth": {
     *      "username": "***@117book.com",
     *      "password": "******"
     *      },
     *      "data": {
     *           "hotelId":  [ "1535796" ],
     *           "name": "Holiday Inn El Monte - Los Angeles",
     *      }
     * }
     * @param $ids
     * @param $update_data
     * @return bool|string|null
     */
    public function postHotelWiki($update_data)
    {
        $data = [
            'auth' => $this->auth(),
            'data' => $update_data,
        ];

        $data['data']['hotelId'] = [$update_data['hotelId']];
        // bu default, update all hotel with same hotel name
        $data['data']['modifyAllSameHotels'] = true;
        $url = $this->remote_api . '/updateHotelsInfo';

        return $this->post($url, $data);
    }

    public function updateHotelInfo($data, $action)
    {
        $url = $this->remote_api . '/' . $action;
        $input = [
            'auth' => $this->auth(),
            'data' => $data
        ];

        $r = $this->post($url, $input);

        if ($r === false) {
            return [
                'success' => false,
                'errorMessage' => '网络缘故，更新酒店信息失败。'
            ];
        }

        return json_decode($r, true);
    }

    /**
     * @param $name
     * @param $data
     * @return array|mixed
     */
    private function getJavaResult($name, $data)
    {
        $url = $this->remote_api . '/' . $name;
        $input = [
            'auth' => $this->auth(),
            'data' => $data,
        ];

        $r = $this->post($url, $input);

        if ($r === false) {
            return [
                'success'      => false,
                'errorId'      => 'IT1001',
                'errorMessage' => '网络缘故，暂时无法预订这家酒店，请稍后再试或者选择其他的酒店。'
            ];
        }

        return json_decode($r, true);
    }

    /**
     * @param $name
     * @param $data
     * @return array|mixed
     */
    private function getResult($name, $data)
    {
        $url = config('constants.hotel_api_url') . '/' . $name;
        $input = [
            'auth' => $this->auth(),
            'data' => $data,
        ];
        $r = $this->post($url, $input);

        if ($r === false) {
            return [
                'success' => false,
                'errorMessage' => '网络缘故，暂时无法预订这家酒店，请稍后再试或者选择其他的酒店。'
            ];
        }

        return json_decode($r, true);
    }

    /**
     * @return array
     */
    private function auth()
    {
        return [
            'username' => $this->user->email,
            'password' => $this->user->api_password,
        ];
    }

    /**
     * @param $url
     * @param $input
     * @return bool|string|null
     */
    private function post($url, $input)
    {
        $response = null;

        if (is_array($input)) {
            $input = json_encode($input);
        }
        $response = $this->doPost($url, $input);

        if ($response === false) {
            // send email if first try of post failed
            $name = substr($url, strripos($url, '/') + 1);
            //若第一次预定失败，不去尝试第二次，防止重复预定
            if ($name == "book") {
                try {
                    $input = json_decode($input, true);
                    $input['auth']['partnerId'] = $this->user->id;
                    $input = json_encode($input);
                    $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? trim($_SERVER['HTTP_X_FORWARDED_FOR']) : '';
                    if (!$ip) {
                        $ip = isset($_SERVER['REMOTE_ADDR']) ? trim($_SERVER['REMOTE_ADDR']) : '';
                    }

                    Log::info($ip . ': Network caused book failure, send email');
                    if (config('app.env') === 'production') {
                        Mail::raw($input, function ($message) {
                            $message->subject('Network caused book failure');
                            if (config('app.env') == 'production') {
                                $message->to(config('staff-emails.henry'))
                                    ->cc(config('staff-emails.ann'))
                                    ->cc(config('staff-emails.jie'));
                            } else {
                                $message->to(config('app.default_mail_to'));
                            }
                        });
                    }
                } catch (Exception $e) {
                    Log::error('send network error email failed: ' . $e->getMessage());
                }
            } else {
                // try once more for internet interrupt
                return $this->doPost($url, $input);
            }
        }

        return $response;
    }

    /**
     * @param $url
     * @param $data
     * @return bool|string
     */
    private function doPost($url, $data)
    {
        $array = explode('/', $url);
        $name = array_pop($array);

        $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? trim($_SERVER['HTTP_X_FORWARDED_FOR']) : '';
        if (!$ip) {
            $ip = isset($_SERVER['REMOTE_ADDR']) ? trim($_SERVER['REMOTE_ADDR']) : '';
        }
        $a = explode('|', str_replace(',', '|', $ip));
        $ip = trim($a[0]);

        if (!in_array($name, [
            'getRooms',
            'getExtraHotelData'
        ])) {
            Log::info($ip . ' ' . $name . ' request: ' . print_r($data, true));
        }

        $optional_headers = "Content-Type: application/json\r\n" . "X-FORWARDED-FOR:" . $ip;

        $params = [
            'http' => [
                'method' => 'POST',
                'content' => $data
            ]
        ];

        if ($optional_headers !== null) {
            $params['http']['header'] = $optional_headers;
        }

        $ctx = stream_context_create($params);

        try {
            $fp = @fopen($url, 'rb', false, $ctx);

            if (!$fp) {
                return false;
            }

            $response = @stream_get_contents($fp);

            if (!in_array($name, ['searchHotels', 'getRooms', 'searchRooms', 'getExtraHotelData'])) {
                Log::info($ip . ' ' . $name . ' response: ' . $response);
            }

            fclose($fp);

            return $response;
        } catch (Exception $e) {
            // Log::error($ip . ' error open url ' . $e->getMessage());

            return false;
        }
    }
}
