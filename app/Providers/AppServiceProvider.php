<?php

namespace App\Providers;

use Illuminate\Mail\MailServiceProvider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     * @return void
     */
    public function boot()
    {
        $host = request()->getHost();
        $protocal = request()->secure() ? 'https://' : 'http://';

        // default app name and url is for c_host
        if (strpos($host, config('app.b_host_keyword')) !== false) {
            //根据host强制转config变量
            config([
                'app.b_host' => $host,
                'app.name' => '117book',
                'constants.tax_rate' => 0,
                'app.url' => $protocal . $host
            ]);

            // need to register mail service again when change config manually here
            config(['mail' => config('mail_b')]);
            config(['mail.from.name' => $host]);
            (new MailServiceProvider(app()))->register();
        } elseif (strpos($host, config('app.c_en_host_keyword')) !== false) {
            // for c site, app name is always hotel, but url would be different
            // for c site, tax rate is base on config tax rate
            config([
                'app.c_en_host' => $host,
                'app.url' => $protocal . $host,
                'app.name' => 'hotel'
            ]);
            if (config('constants.tax_rate') == 0) {
                config([
                    'constants.tax_rate' => 0.15,  // in case not able to get the tax rate config sometime
                ]);
            }
            config(['mail.from.name' => $host]);
            (new MailServiceProvider(app()))->register();
        } else {
            config([
                'app.c_host' => $host,
                'app.url' => $protocal . $host,
                'app.name' => 'hotel'
            ]);
            if (config('constants.tax_rate') == 0) {
                config([
                    'constants.tax_rate' => 0.15,  // in case not able to get the tax rate config sometime
                ]);
            }
            config(['mail.from.name' => $host]);
            (new MailServiceProvider(app()))->register();
        }

        view()->composer('*', function ($view) {
            $view_name = str_replace('.', '-', $view->getName());
            view()->share('view_name', $view_name);

            $view->with([
                'should_track' => config('app.enable_tracking') && config('app.name') === 'hotel' ? 1 : 0,
                'is_usitour' => isset($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'], 'usitour') !== false ? 1 : 0,
                'is_b' => config('app.name') === '117book' ? 1 : 0,
                'is_aa' => isset($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'], 'supervacation') !== false ? 1 : 0,
                'language' => (int)session('language', 0),
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
