<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class ResendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:resend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resend failed emails in failed jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $failed = $this->laravel['queue.failer']->all();
        if (count($failed) > 0) {
            Artisan::call('queue:retry', ['id' => ['all']]);
            $this->info(date('D, Y-m-d H:i:s') . ' resent ' . count($failed) . ' emails');
        } else {
            $this->info(date('D, Y-m-d H:i:s') . ' no failed jobs');
        }
    }
}
