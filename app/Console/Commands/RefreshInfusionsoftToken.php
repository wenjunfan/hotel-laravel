<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Infusionsoft\Infusionsoft;

class RefreshInfusionsoftToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'infusionsoft:refreshToken';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh infusionsoft token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (Cache::has('infusionsoft_token')) {
            $token = Cache::get('infusionsoft_token');

            $infusionsoft = new Infusionsoft(config('constants.infusionsoft'));

            $infusionsoft->setToken($token);

            try {
                $new_token = $infusionsoft->refreshAccessToken();

                Cache::forever('infusionsoft_token', $new_token);
            } catch (\Exception $exception) {
                $this->info(date('D, Y-m-d H:i:s') . ' refresh infusionsoft token got error: ' . $exception->getMessage());
            }
        } else {
            $this->info(date('D, Y-m-d H:i:s') . ' infusionsoft token missing in redis');

            // generate new token
            try {
                Mail::raw('infusionsoft token config missing in redis cache when refresh token, server info: ' . php_uname(), function ($msg) {
                    $msg->to(config('staff-emails.jie'))
                        ->subject('infusionsoft access token missing');
                });
            } catch (\Exception $exception) {
                $this->info(date('D, Y-m-d H:i:s') . ' send infusionsoft token missing email failed: ' . $exception->getMessage());
            }
        }

        $this->info(date('D, Y-m-d H:i:s') . ' infusionsoft token refreshed');
    }
}
