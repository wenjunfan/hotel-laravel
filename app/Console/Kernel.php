<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
        Commands\RefreshInfusionsoftToken::class,
        Commands\ResendEmails::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('infusionsoft:refreshToken')
            ->dailyAt('00:00') // refresh token everyday at midnight
            ->appendOutputTo(base_path('schedules.log'));

        $schedule->command('emails:resend')
            ->cron('0 */3 * * *')
            ->appendOutputTo(base_path('schedules.log'))
            ->withoutOverlapping();
    }
}
