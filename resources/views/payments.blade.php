@extends( !$is_b  ?  'layouts.master' : 'layouts.master-b' )

<?php
    function decodeErrorMessage($error) {
        $message = "";
        if (empty($error)) {
           return $message;
        }

        $errors = json_decode($error, true);

        foreach($errors as $e) {
            if (!empty($message)) {
                $message .= '<br>';
            }

            $message .= '<strong>errorCode: </strong>' . $e['errorCode'] . '<br>';
            $message .= '<strong>shortMessage: </strong>' . $e['shortMessage'] . '<br>';
            $message .= '<strong>longMessage: </strong>' . $e['longMessage'] . '<br>';
            $message .= '<strong>severityCode: </strong>' . $e['severityCode'];
        }

        return $message;
    }
?>

@section('title', '扣款记录')

@section('css')
<link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row animated fadeInRight">
    <div class="col-lg-6 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                查询扣款记录
            </div>
            <div>
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" id="search-form" name="search-form">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="from">起始日期</label>

                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="from" name="from" type="text" value="{{$from}}" class="form-control" placeholder="年-月-日">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="to">结束日期</label>

                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="to" name="to" type="text" class="form-control" value="{{$to}}" placeholder="年-月-日">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-primary btn-block" type="button" onclick="search()"><i class="fa fa-search"></i> 查询</button>
                            </div>
                            <!--div class="col-sm-6">
                                <button class="btn btn-warning btn-block" type="reset">重置</button>
                            </div-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                扣款记录
            </div>
            <div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-payments" >
                        <thead>
                        <tr>
                            <th>订单流水</th>
                            <th>卡号</th>
                            <th>付款状态</th>
                            <th>交易流水</th>
                            <th>关联号</th>
                            <th>交易金额</th>
                            <th>错误信息</th>
                            <th>交易时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                    @foreach($payments as $payment)
                        <tr class="gradeX">
                            <td>{{$payment->orderReference}}</td>
                            <td>
			     @if (isset($payment->request()['ACCT']))
                                *{{substr($payment->request()['ACCT'], 12)}}
                            @else
    
                            @endif
			    </td>
                            <td>
                            @if ($payment->ack == 'Success')
                                <span class="label label-primary">成功</span>
                            @else
                                <span class="label label-warning">失败</span>
                            @endif
                            </td>
                            <td>{{$payment->transactionId}}</td>
                            <td>{{$payment->correlationId}}</td>
                            <td>{{$payment->currency . ' ' . $payment->amount}}</td>
                            <td>{!!decodeErrorMessage($payment->errors)!!}</td>
                            <td id="{{$payment->id}}"></td>
                            <td>
                                <a target="_blank" href="{{url('/order/' . $payment->orderReference)}}" class="btn btn-info btn-circle"><i class="fa fa-info"></i></a>
                            </td>
                        </tr>
                    @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<script type="text/javascript">
    jQuery(function($) {
        $('#from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('.dataTables-payments').DataTable({
            "order": [[7, "desc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: '扣款记录'},
                {extend: 'pdf', title: '扣款记录'},
                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    });

    @foreach($payments as $payment)
        toLocalTime('{{$payment->id}}', '{{$payment->created_at}}');
    @endforeach

    function toLocalTime(id, t) {
        var local = new Date(t);
        var localTime = local.getFullYear() + "-" + (local.getMonth() + 1) + "-" + local.getDate() + " " + local.getHours() + ":" + local.getMinutes() + ":" + local.getSeconds();
        $("#" + id).html(localTime);
    }

    function search() {
        var from = $("#from").val();
        var to = $("#to").val();

        if (from != "" && to != "") {
            self.location = "{{url('/payments/from')}}/" + from + "/to/" + to;
        }
        else if (from != "") {
            self.location = "{{url('/payments/from')}}/" + from;
        }
        else if (to != "") {
            self.location = "{{url('/payments/to')}}/" + to;
        }
        else {
            self.location = "{{url('/payments')}}";
        }
    }
</script>
@endsection
