@extends('layouts.master-b')

<?php
function avatarSelected($profileName, $name)
{
	return $profileName == $name ? 'circle-border' : '';
}

function profileValue($name)
{
	return old($name) == '' ? Auth::user()->$name : old($name);
}

?>

@section('title', 'Profile')

@section('css')
	<link href="{{ asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
	<link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
	<style>
		@media (min-width: 1920px) {
			.all-wrapper {
				margin: auto;
				max-width: 1800px; }
		}
	</style>
@endsection

@section('content')
	<div class="row all-wrapper animated fadeInRight">
		<div class="col-lg-4 col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<strong>@lang('profile.Title')</strong>
				</div>
				<div class="ibox-content">
					<table>
						<tr>
							<td width="60" height="60" align="right" valign="top">@lang('profile.first')
								：
							</td>
							<td valign="top">@lang('profile.firsti')</td>
						</tr>

						<tr>
							<td width="60" height="60" align="right" valign="top">@lang('profile.second')
								：
							</td>
							<td valign="top">@lang('profile.secondi')</td>
						</tr>

						<tr>
							<td width="60" height="60" align="right" valign="top">@lang('profile.third')
								：
							</td>
							<td valign="top">@lang('profile.thirdi')</td>
						</tr>

						<tr>
							<td width="80" height="20" align="right">@lang('profile.hotline')
								:
							</td>
							<td> @lang('profile.hotlinePhonea')</td>
						</tr>
						<tr>
							<td width="80" height="0" align="right"></td>
							<td>@lang('profile.hotlinePhoneb')</td>
						</tr>
						<tr>
							<td width="80" height="0" align="right"></td>
							<td>@lang('profile.hotlinePhonec')</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					@lang('profile.password')
				</div>
				<div>
					<div class="ibox-content">
						<form class="form-horizontal" role="form" method="POST" action="{{ url('/resetPassword') }}">
							{!! csrf_field() !!}
							<button type="submit" class="btn btn-primary btn-block">
								<i class="fa fa-envelope"></i> @lang('profile.passwordb')
							</button>
						</form>
					</div>
				</div>
			</div>

			<div class="ibox float-e-margins">
				<div class="ibox-title">
					@lang('profile.fileManagement')
				</div>
				<div>
					<div class="ibox-content">
                        <a href="{{ url('/profile/files') }}" class="btn btn-info" target="_blank">‌营业执照及其他文件管理</a>
                        {{--<a href="{{ url('/profile/migrate') }}" class="btn btn-info" target="_blank">migrate all files</a>--}}
					</div>
				</div>
			</div>

		</div>
		<div class="col-lg-8 col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<strong>@lang('profile.company')  </strong>
				</div>
				<div>
					<div class="ibox-content">
						<form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/profile/update') }}">
							{!! csrf_field() !!}

							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">@lang('profile.username')
									<span class="text-danger">&nbsp;*</span></label>

								<div class="col-sm-9 col-md-4">
									<input type="text" name="name" value="{{ profileValue('name') }}" class="form-control">

									@if ($errors->has('email'))
										<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">@lang('profile.email')
									<span class="text-danger">&nbsp;*</span></label>
								<div class="col-sm-9 col-md-6">
									<input type="text" name="email" value="{{ profileValue('email') }}" class="form-control" readonly>
									@if ($errors->has('email'))
										<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">@lang('profile.companyname')
									<span class="text-danger">&nbsp;</span></label>
								<div class="col-sm-9 col-md-6">
									<input type="text" name="company_name" value="{{ profileValue('company_name') }}" class="form-control">
									@if ($errors->has('company_name'))
										<span class="help-block">
										<strong>{{ $errors->first('company_name') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('company_address') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">@lang('profile.companyadd')
									<span class="text-danger">&nbsp;</span></label>
								<div class="col-sm-9 col-md-6">
									<input type="text" name="company_address" value="{{ profileValue('company_address') }}" class="form-control">

									@if ($errors->has('company_address'))
										<span class="help-block">
										<strong>{{ $errors->first('company_address') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('company_phone') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">@lang('profile.companycell')
									<span class="text-danger">&nbsp;</span></label>

								<div class="col-sm-9 col-md-4">
									<input type="text" name="company_phone" value="{{ profileValue('company_phone') }}" class="form-control">

									@if ($errors->has('company_phone'))
										<span class="help-block">
										<strong>{{ $errors->first('company_phone') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('contact_person') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">@lang('profile.contactor')
									<span class="text-danger">&nbsp;*</span></label>

								<div class="col-sm-9 col-md-4">
									<input type="text" name="contact_person" value="{{ profileValue('contact_person') }}" class="form-control">

									@if ($errors->has('contact_person'))
										<span class="help-block">
										<strong>{{ $errors->first('contact_person') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">@lang('profile.cell')
									<span class="text-danger">&nbsp;*</span></label>

								<div class="col-sm-9 col-md-4">
									<input type="text" name="mobile_phone" value="{{ profileValue('mobile_phone') }}" class="form-control">

									@if ($errors->has('mobile_phone'))
										<span class="help-block">
										<strong>{{ $errors->first('mobile_phone') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('wechat') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">@lang('profile.wechat')
									&nbsp;&nbsp;</label>

								<div class="col-sm-9 col-md-4">
									<input type="text" name="wechat" value="{{ profileValue('wechat') }}" class="form-control">

									@if ($errors->has('wechat'))
										<span class="help-block">
										<strong>{{ $errors->first('wechat') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('tax') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">@lang('profile.tax')
									&nbsp;&nbsp;</label>

								<div class="col-sm-9 col-md-4">
									<input type="text" name="tax" value="{{ profileValue('tax') }}" class="form-control">

									@if ($errors->has('tax'))
										<span class="help-block">
										<strong>{{ $errors->first('tax') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('channel') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">@lang('profile.representative')
									<span class="text-danger">&nbsp;*</span></label>

								<div class="col-sm-9">
									<div class="radio radio-inline">
										<input type="radio" id="inlineRadio1" value="181" name="channel" {{ profileValue('channel') == '181' ? 'checked' : ''}}>
										<label for="inlineRadio1">@lang('profile.hotlinea')</label>
									</div>
									<div class="radio radio-inline">
										<input type="radio" id="inlineRadio2" value="668" name="channel" {{ profileValue('channel') == '668' ? 'checked' : ''}}>
										<label for="inlineRadio2">@lang('profile.hotlineb')</label>
									</div>
									<div class="radio radio-inline">
										<input type="radio" id="inlineRadio3" value="35" name="channel" {{ profileValue('channel') == '35' ? 'checked' : ''}}>
										<label for="inlineRadio3">@lang('profile.hotlinec')</label>
									</div>
									@if ($errors->has('channel'))
										<span class="help-block">
															<strong>{{ $errors->first('channel') }}</strong>
														</span>
									@endif
								</div>

							</div>

							<div class="hr-line-dashed"></div>

							<div class="form-group">
								<div class="col-sm-6 col-sm-offset-3">
									<button class="btn btn-primary" type="submit">
										<i class="fa fa-save"></i>@lang('profile.save')
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
    @include('business.includes.js-profile')
@endsection
