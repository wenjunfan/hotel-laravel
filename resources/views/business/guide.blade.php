@extends('layouts.master-b')

@section('title', 'Help Center')

@section('css')
{{--todo--}}

@endsection

@section('content')
    <div class="row animated fadeInRight">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="ibox">
                <div class="ibox-noborder">

                    {!! csrf_field() !!}
                    <div class="article-title" style="text-align:left; margin-bottom:25px">
                        <h1>
                            @lang('guide.pageTitle')
                        </h1>
                    </div>
                    <strong><font size="3"> @lang('guide.Title')</font></strong> <br><br>
                    <p>
                        @lang('guide.first')
                    </p>
                    <p>
                        @lang('guide.second')      </p>
                    <p>
                        @lang('guide.third')  </p>

                    <br/>@lang('guide.hotline') : <br>
                    @lang('guide.hotlinea'):  <strong style="text-decoration:none;">
                        ‌+1 626-522-2906</strong><br>
                    @lang('guide.hotlineb'):  <strong style="text-decoration:none;">
                        +1 626-389-0705</strong><br>
                    @lang('guide.hotlinec'): <strong style="text-decoration:none;">
                        ‌+1 626-434-5267</strong> <br><br>
                    <p>
                        <br> <strong><font size="3">@lang('guide.qa')</font>
                        </strong> <br><br> Q：@lang('guide.q1')<br>
                        A：@lang('guide.a1')<br><br>
                        Q：@lang('guide.q2')<br>
                        A：@lang('guide.a2')<br><br>
                        Q：@lang('guide.q3')<br>
                        A：@lang('guide.a3')<br><br>
                        Q：@lang('guide.q4')<br>
                        A：@lang('guide.a4')<br><br>
                        Q：@lang('guide.q5')<br>
                        A：@lang('guide.a5')<br><br>
                        Q：@lang('guide.q6')<br>
                        A：@lang('guide.a6')<br><br>
                        Q：@lang('guide.q7')<br>
                        A：@lang('guide.a7')<br><br>
                        Q：@lang('guide.q8')<br>
                        A：@lang('guide.a8')<br>
                    </p>

                </div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')


    <script type="text/javascript">

        jQuery(function ($) {

        });

    </script>
@endsection
