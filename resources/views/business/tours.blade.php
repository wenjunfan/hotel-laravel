@extends('layouts.master-b')

@section('title', 'tours')

@section('css')
    <style>
        body {overflow: hidden;-ms-scroll-limit: 0 0 0 0;}
        .animated {padding-top: 5px;}
        iframe {height: 100vh;}
    </style>
@endsection

@section('content')
    <div class="row">
        <iframe id="tourFrame" name="tourFrame" src="{{ $src }}" frameborder="0" width="100%"></iframe>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            var navHeight = $('.navbar').height();
            $('#tourFrame').css('height', (window.innerHeight - navHeight) + 'px');
            window.addEventListener('resize', function () {
                $('#tourFrame').css('height', (window.innerHeight - navHeight) + 'px');
            });
        });
    </script>
@endsection