@extends('layouts.master-b')

<?php
function decodeErrorMessage($error)
{
    $message = "";
    if (empty($error)) {
        return $message;
    }

    $errors = json_decode($error, true);

    foreach ($errors as $e) {
        if (!empty($message)) {
            $message .= '<br>';
        }

        $message .= '<strong>errorCode: </strong>' . $e['errorCode'] . '<br>';
        $message .= '<strong>shortMessage: </strong>' . $e['shortMessage'] . '<br>';
        $message .= '<strong>longMessage: </strong>' . $e['longMessage'] . '<br>';
        $message .= '<strong>severityCode: </strong>' . $e['severityCode'];
    }

    return $message;
}
?>

@section('title', 'Credit Card Refunds')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/admin/plugins.checkorders.css') }}">
@endsection

@section('content')
    <div class="row animated fadeInRight">
        <div class="col-lg-6 col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-noborder">
                    <strong>@lang('refunds.title')</strong>
                </div>
                <div>
                    <div class="ibox-content">
                        <form class="form-horizontal" role="form" id="search-form" name="search-form">
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="from">@lang('refunds.sd')</label>

                                <div class="col-sm-9">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="from" name="from" type="text" value="{{$from}}" class="form-control" placeholder="@lang('refunds.YMD')">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="to">@lang('refunds.ed')</label>

                                <div class="col-sm-9">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="to" name="to" type="text" class="form-control" value="{{$to}}" placeholder="@lang('refunds.YMD')">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button class="btn btn-primary btn-block" type="button" onclick="search()"><i class="fa fa-search"></i> @lang('refunds.Search')</button>
                                </div>
                                <!--div class="col-sm-6">
                                    <button class="btn btn-warning btn-block" type="reset">重置</button>
                                </div-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="ibox float-e-margins">
                <div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-refunds">
                                <thead>
                                <tr>
                                    <th style="width:150px;">@lang('refunds.BookingID')</th>
                                    <th>@lang('refunds.orders')</th>
                                    <th>@lang('refunds.cn')</th>
                                    <th class="hidden">@lang('refunds.cr')</th>
                                    <th>@lang('refunds.status')</th>
                                    <th>@lang('refunds.transactions')</th>
                                    <th>@lang('refunds.refunds')</th>
                                    <th>@lang('refunds.rn')</th>
                                    <th>@lang('refunds.ar')</th>
                                    <th>@lang('refunds.em')</th>
                                    <th>@lang('refunds.refundst')</th>
                                    <th>@lang('refunds.action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($refunds as $refund)
                                    <tr class="gradeX">
                                        <td>{{$refund->id}}</td>
                                        <td>{{$refund->orderReference}}</td>
                                        <td>
                                            @if (isset($refund->payment->ACCT))
                                                *{{substr($refund->payment->ACCT, -4)}}</td>
                                        @endif
                                        <td class="hidden">{{$refund->currency . ' ' . $refund->grossRefundAmount}}</td>
                                        <td>
                                            @if ($refund->ack == 'Success')
                                                <span class="label label-primary">@lang('refunds.s')</span>
                                            @else
                                                <span class="label label-warning">@lang('refunds.f')</span>
                                            @endif
                                        </td>
                                        <td>{{$refund->transactionId}}</td>
                                        <td>{{$refund->refundTransactionId}}</td>
                                        <td>{{$refund->correlationId}}</td>
                                        <td>{{$refund->currency . ' ' . $refund->totalRefundedAmount}}</td>
                                        <td>{!!decodeErrorMessage($refund->errors)!!}</td>
                                        <td id="{{$refund->id}}"></td>
                                        <td>
                                            @if ($refund->orderReference != '')
                                                <a target="_blank" href="{{url('/order/' . $refund->orderReference)}}" class="btn btn-info btn-circle"><i class="fa fa-info"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/js/admin/plugins.checkorders.js') }}"></script>
    @include('business.includes.js-refunds')
@endsection
