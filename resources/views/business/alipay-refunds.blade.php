@extends('layouts.master-b')

@section('title', 'Alipay Refunds')

@section('css')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row animated fadeInRight">
        <div class="col-lg-6 col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-noborder">
                    <strong>@lang('alipay-refunds.title')</strong>
                </div>
                <div>
                    <div class="ibox-content">
                        <form class="form-horizontal" role="form" id="search-form" name="search-form">
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="from">@lang('alipay-refunds.sd')</label>

                                <div class="col-sm-9">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="from" name="from" type="text" value="{{$from}}" class="form-control" placeholder="@lang('alipay-refunds.YMD')">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="to">@lang('alipay-refunds.ed')</label>

                                <div class="col-sm-9">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="to" name="to" type="text" class="form-control" value="{{$to}}" placeholder="@lang('alipay-refunds.YMD')">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button class="btn btn-primary btn-block" type="button" onclick="search()"><i class="fa fa-search"></i> @lang('alipay-refunds.Search')</button>
                                </div>
                                <!--div class="col-sm-6">
                                    <button class="btn btn-warning btn-block" type="reset">重置</button>
                                </div-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="ibox float-e-margins">

                <div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-refunds">
                                <thead>
                                <tr>
                                    <th>@lang('alipay-refunds.BookingID')</th>
                                    <th>@lang('alipay-refunds.orders')</th>
                                    <th>@lang('alipay-refunds.ar')</th>
                                    <th>@lang('alipay-refunds.handling')</th>
                                    <th>@lang('alipay-refunds.arefund')</th>
                                    <th>@lang('alipay-refunds.status')</th>
                                    <th>@lang('alipay-refunds.refundst')</th>
                                    <!--th>操作</th-->
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($refunds as $refund)
                                    <tr class="gradeX">
                                        <td>
                                            @if (isset($refund->order->id))
                                                {{$refund->order->id}}
                                            @endif
                                        </td>
                                        <td>{{$refund->order_reference}}</td>
                                        <td>{{$refund->currency . ' ' . $refund->refund_amount}}</td>
                                        <td>{{$refund->currency . ' ' . $refund->transaction_fee}}</td>
                                        <td>{{$refund->currency . ' ' . $refund->refund_balance}}</td>
                                        <td>
                                            @if ($refund->status == 'REFUND')
                                                <span class="label label-primary">@lang('alipay-refunds.Refunded')</span>
                                            @elseif ($refund->status == 'PENDING')
                                                <span class="label label-warning">@lang('alipay-refunds.Processing')</span>
                                            @endif
                                        </td>
                                        <td id="{{$refund->id}}"></td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

    <script type="text/javascript">
        jQuery(function ($) {
            $('#from').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: "yyyy-mm-dd"
            });

            $('#to').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: "yyyy-mm-dd"
            });

            $('.dataTables-refunds').DataTable({
                "order": [[6, "desc"]],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: '{{ trans('alipay-refunds.title') }}'},
                    {extend: 'pdf', title: '{{ trans('alipay-refunds.title') }}'},
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ]
            });
        });

        @foreach($refunds as $refund)
        toLocalTime('{{$refund->id}}', '{{$refund->created_at}}');

        @endforeach

        function toLocalTime (id, t) {
            var local = new Date(t);
            var localTime = local.getFullYear() + "-" + (local.getMonth() + 1) + "-" + local.getDate() + " " + local.getHours() + ":" + local.getMinutes() + ":" + local.getSeconds();
            $("#" + id).html(localTime);
        }

        function search () {
            var from = $("#from").val();
            var to = $("#to").val();

            if (from != "" && to != "") {
                self.location = "{{url('/refunds/afrom')}}/" + from + "/ato/" + to;
            } else if (from != "") {
                self.location = "{{url('/refunds/afrom')}}/" + from;
            } else if (to != "") {
                self.location = "{{url('/refunds/ato')}}/" + to;
            } else {
                self.location = "{{url('/alipay/refunds')}}";
            }
        }
    </script>
@endsection
