@extends('layouts.master-b')

@section('title')
   @lang('profile.fileManagement')
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/business/plugins.files.css') }}">
    <style>
        .files-control {
            border-bottom: 1px solid #dce0e0;
            margin-bottom: 25px;
            padding-bottom: 10px;
            width: 100%;
            background: #fff;
            z-index: 1;
        }
        .new-file {
            display: inline-block !important;
        }
        .files-control.sticky {
            position: fixed;
            top: 0;
            right: 0;
            padding: 10px 20px;
        }
        .row {
            margin-bottom: 30px;
        }
        div[class*="col-"] {
            text-align: center;
        }
        .file-content {
            display: inline-block;
            width: 90%;
            vertical-align: top;
        }
        object {
            width: 100%;
        }
        div[class*="col-"] {
            text-align: center;
        }
        .icheckbox_square-green {
            vertical-align: top;
        }
        .sweet-alert button.cancel {
            background-color: #b6b6b6;
        }
    </style>
@endsection

@section('content')
    <div id="fileApp">
        {{--files controller: upload and remove--}}
        <div class="files-control row">
            <div class="col-sm-6" style="text-align: left;">
                <form id="uploadNewForm" action="{{ url('profile/files/upload') }}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    @if($id)
                        <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <span class="text-info">@lang('profile.uploadFile')</span>
                    <input type="file" name="file" id="file" class="new-file" accept="image/png, image/jpeg, application/pdf" required>
                    <button type="button" class="btn btn-info" @click="uploadCert">Upload</button>
                    <span>@lang('profile.supportType')</span>
                </form>
            </div>
            <div class="col-sm-6" style="text-align: right;">
                <span class="text-danger">@lang('profile.deleteNote')</span>
                <button class="btn btn-danger" @click="removeFiles">@lang('profile.delete')</button>
            </div>
        </div>

        {{--files gallary--}}
        <form class="files-wrapper" id="filesGallery" method="post" action="{{ url('profile/files/remove') }}" enctype="multipart/form-data">
            {!! csrf_field() !!}
            @if($id)
                <input type="hidden" name="id" value="{{ $id }}">
            @endif
            @if(count($files) > 0)
                <div class="row" v-for="(fileGroup, groupIndex) in files" :key="'fileGroup'+groupIndex">
                    <div class="file-item col-sm-3" v-for="(file, index) in fileGroup" :key="'file'+index">
                        <input type="checkbox" name="file[]" class="file-check" :value="file">
                        <div class="file-content" v-if="file.toLowerCase().indexOf('.pdf') > -1">
                            <object :data="file" type="application/pdf">
                                <iframe :src="'https://docs.google.com/viewer?url=' + file + '&embedded=true'" frameborder="0"></iframe>
                            </object>
                        </div>
                        <div class="file-content" v-else>
                            <img :src="file"  width="100%" height="150" alt="">
                        </div>
                        <a @click="previewFile(file)" title="click to view">
                            @{{ decodeURI(file.split('/')[4]) }}
                        </a>
                    </div>
                </div>
            @else
                @lang('profile.noFile')
            @endif
        </form>
        <div class="modal fade" id="viewFileModal" tabindex="-1" role="dialog" aria-labelledby=viewFileModalTitle" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="viewFileTitle">Modal title</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/js/business/plugins.files.js') }}"></script>
    <script type="text/javascript">
        var files = {!! json_encode($files); !!};

        new Vue({
            el: '#fileApp',
            data: {
                files: files,
                selectedImg: []
            },
            mounted: function() {
                $('.file-check').iCheck({
                    checkboxClass: 'icheckbox_square-green'
                });

                let originH = $('.files-wrapper').offset().top;
                let $filesControl = $('.files-control');
                $(window).scroll(function () {
                    let scrollTop = $(this).scrollTop();
                    if (scrollTop > originH) {
                        $filesControl.css('width', $('#page-wrapper').outerWidth());
                        $filesControl.addClass('sticky');
                    } else {
                        $filesControl.css('width', '100%');
                        $filesControl.removeClass('sticky');
                    }
                })
            },
            methods: {
                uploadCert: function() {
                    let $form = $('#uploadNewForm');
                    let validation = $form.validate({
                        errorElement: 'span',
                        errorClass: 'text-danger'
                    });
                    if (validation.valid()) {
                        var photo = $('#file').prop('files')[0];
                        var formData = new FormData();

                        formData.append('file', photo, photo.name);
                        formData.append('_token', $('meta[name="csrf_token"]').attr('content'));

                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', $form.attr('action'), true);
                        xhr.send(formData);

                        xhr.onreadystatechange = function () {
                            if (xhr.readyState === 4 && xhr.status == 200) {
                                var response = $.parseJSON(xhr.response);
                                if (response.success) {
                                    swal(response.message);
                                } else {
                                    swal(response.errorMessage);
                                }
                            } else {
                                swal('Somthing went wrong');
                            }
                        };
                    }
                },
                removeFiles: function() {
                    let selected = $('input:checked');
                    if (selected.length > 0) {
                        swal({
                            title: '@lang( 'profile.confirmDelete')' + selected.length + '@lang('profile.files')',
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                        }, function (val) {
                            if (val) {
                                $('#filesGallery').submit();
                            }
                        });
                    }
                },
                previewFile: function(src){
                    $('#viewFileModal').modal('show');
                    $('.modal-title').html(src.split('/')[4]);
                    if(src.toLowerCase().indexOf('.pdf') > -1){
                        $('.modal-body').html('<iframe src="https://docs.google.com/viewer?url=' + src + '&embedded=true"  frameborder="0" width="100%" height="800px"></iframe>');
                    }else{
                        src = encodeURI(src);
                        $('.modal-body').html('<img src='+src+' style="width: 100%;">');
                    }
                }
            }
        })
    </script>
@endsection