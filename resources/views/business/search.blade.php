@extends('layouts.master-b')

@section('title')
    @lang('search.page_title')
@endsection

@section('css')
    <meta name="description" content="@lang('search.page_description')">
    <meta name="Keywords" content="@lang('search.page_keyword')"/>
    <link rel="stylesheet" href="{{ asset('assets/css/business/plugins.search.css') }}">
@endsection

@section('content')
    <div class="all-wrapper">
        <div class="row" id="toptop">
            <div class="ibox-noborder" Style="margin-top:40px;">
                {{--title--}}
                <div id="form-hide0" style="margin-bottom:10px;">
                    @if($language == 0)
                        <label class="slogan" style="margin-top:10px; font-size:2em; color:#484848; font-weight: 300"> 探索全球数千家酒店的
                            <small style="color:#f1791e; font-size:28px;">最优惠</small>
                            价格。 </label>
                    @endif
                    @if($language == 1)
                        <label class="slogan" style="margin-top:10px;font-size:30px; color:#484848; font-weight: 300"> Explore the
                            <small style="color:#f1791e; font-size:30px;">best deals</small>
                            <br/>for thousands of hotels worldwide. </label>
                    @endif
                </div>
                <br/>
                <!--搜索form start-->
                <form class="form-horizontal" role="form" id="search-form" name="search-form" onkeydown="if(event.keyCode==13){return false;}">
                    <div class="row">
                        <div class="searchform" style="margin:0 25px 0 15px;">
                            <div class="col-sm-4" id="destinationNameWrapper" style="border-right:1px solid #dce0e0; height:100%;">
                                <!--目的地-->
                                <div class="form-group">
                                    <div class="col-sm-12 extraTopPadding" style="margin-left:-13px;">
										<span class="span-hide">
										   <label style="font-size:14px; margin-top:3px;" class="control-label rounded" for="destinationName">@lang('search.Destination')</label>
										</span>
                                        <input onClick="selectText(this)" type="text" class="form-control search_input" style="padding: 0px;" name="destinationName" value="" id="destinationName" placeholder="@lang('search.DestinationInput')" autocomplete="off" tip="">
                                        <input type="hidden" name="destinationId" id="destinationId" value="">
                                    </div>
                                    @include('includes.home.destination-list')
                                </div>
                            </div>
                            <div class="col-sm-3 extraTopPadding" style="margin-left:-0.9%">
                                <!--入住日期-->
                                <div class="form-group ">
                                    <div class="col-sm-12" style="margin-left:-2px;">
									<span class="span-hide">
                                        <label style="font-size:14px; margin-top:3px; font-family: Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti; color:#484848;" class="control-label rounded" for="checkin">@lang('search.When')</label>
                                        </span>
                                        <div class="input-group date">
                                            <input id="checkin" name="checkin" type="text" class="form-control search_input datepicker datesmaller" onchange="updateCheckin()" value=""
                                                    placeholder="@lang('search.Checkin')" autocomplete="off">
                                            <span class="input-group-addon">
                                                <i class="fa fa-long-arrow-alt-right"></i>
                                            </span>
                                            <input id="checkout" name="checkout" type="text" class="form-control search_input datesmaller" onchange="updateCheckout()" value=""
                                                    placeholder="@lang('search.Checkout')" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--房间数量-->
                            <div class="col-sm-5" id="searchform-rightBlock" style="margin-left:-0.9%">
                                <div class="form-group  focus col-ms-12" style="height:30%; color:#676a6c;">
                                    <div class="col-sm-2 rounded leftrightboder extraTopPadding" style="border-right:1px solid #dce0e0">
									<span class="span-hide">
										<label style="font-size:14px; margin-top:3px;" class="control-label" for="roomCount">@lang('search.RoomCount')</label>
										</span> <select id="roomCount" name="roomCount" style="border:none; background-image: url(//www.connectedtoindia.com/assets/images/dropdown-arrow1.png); color:#484848;" class="form-control selectpicker search_input roomsmaller" value='1'>
                                            <option value="1" selected="selected">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9+</option>
                                        </select>
                                    </div>
                                    <div class="related_1_content related_2_content related_3_content related_4_content related_5_content related_6_content related_7_content related_8_content related_9_content">
                                        <div class="col-sm-4 rounded extraTopPadding">
                                            <span class="span-hide">
                                                <label style="font-size:14px; margin-top:3px;" class="control-label">
                                                    <span class="smalltag">@lang('search.Guest')</span>
                                                    <span class="smalltag1">@lang('search.Guest1')</span>
                                                </label>
                                            </span> <select id="adultCount1" style="border:none;background-image: url(//www.connectedtoindia.com/assets/images/dropdown-arrow1.png);  color:#484848;" name="adultCount1" class="form-control selectpicker roomsmaller" value='1'>
                                                <option value="1" selected="selected">
                                                    1 @lang('search.Adult')</option>
                                                <option value="2">
                                                    2 @lang('search.Adults')</option>
                                                <option value="3">
                                                    3 @lang('search.Adults')</option>
                                                <option value="4">
                                                    4 @lang('search.Adults')</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3 extraTopPadding">
                                            <div class="span-hide">
                                                <label style="font-size:14px; margin-top:3px;" class="control-label">&nbsp;</label>
                                            </div>
                                            <select id="childCount1" style="border:none; background-image: url(//www.connectedtoindia.com/assets/images/dropdown-arrow1.png);  color:#484848;" name="childCount1" class="form-control selectpicker roomsmaller" value="0" onchange="showChildAge(this.value, 1)">
                                                <option value="0" selected="selected">
                                                    0 @lang('search.Child')</option>
                                                <option value="1">
                                                    1 @lang('search.Child')</option>
                                                <option value="2">
                                                    2 @lang('search.Children')</option>
                                                <option value="3">
                                                    3 @lang('search.Children')</option>
                                                <option value="4">
                                                    4 @lang('search.Children')</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3 extraTopPadding">
                                            <button type="button" id="searchButton" class="btn pull-right searchbtn btn-primary btn-block ladda-button ladda-button-search" data-style="expand-right" style="padding-left:0px; padding-right:0px; margin-right:-15px; margin-top:15px;height:40px" onclick="search()">@lang('search.SearchHotel')</button>
                                        </div>
                                    </div>
                                    <div class="related_10_content">
                                        <div class="col-sm-10 extraTopPadding">
                                            <button type="button" id="groupsearchButton" class="btn pull-right searchbtn btn-primary btn-block ladda-button ladda-button-search" data-style="expand-right" style="padding-left:0px; padding-right:0px; margin-right:-15px; margin-top:15px;height:40px" data-toggle="modal" data-target="#myModalHorizontal">@lang('search.groupsearch')</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--目的地推荐-->
                        <div class="col-lg-12" id="form-hide4" style="padding-top:60px">
                            <div class="border-bottomfocus" style="width:99.2%; margin-bottom:25px;">
                                <div>
                                    <div class="btn-group ">
                                        <p class="pull-left col-mm-3" style="font-size:16px;padding-left:10px; margin-top:10px;">@lang('search.Recommendation')</p>
                                    </div>
                                    <div class="btn-group pull-right Continents" style="margin-top:5px;">
                                        <a id="button-america" href="#America" class="col-mm-3" onclick="var idname='button-america'; focus1(idname)" style="outline:none; padding:8px; font-size:15px; color:#484848; margin-left:10px; margin-right:10px;">@lang('search.Americas')</a>
                                        <a id="button-europe" href="#Europe" class=" col-mm-3" onclick="var idname='button-europe'; focus1(idname)" style="outline:none; padding:8px; font-size:15px;color:#484848;margin-left:10px;margin-right:10px;">@lang('search.Europe')</a>
{{--                                        <a id="button-asia" href="#Asia" class=" col-mm-3" onclick="var idname='button-asia'; focus1(idname)" style="outline:none; padding:8px; font-size:15px;color:#484848;margin-left:10px;margin-right:10px;">@lang('search.Asia')</a>--}}
                                        <a id="button-others" href="#Others" class=" col-mm-3" onclick="var idname='button-others'; focus1(idname)" style="outline:none; padding:8px; font-size:15px;color:#484848;">@lang('search.Oceania')</a>
                                    </div>
                                </div>
                            </div>

                            <div class="slidebox RecommendsCities smaller ">
                                <div id="America">
                                    @if($language != 0)
                                        <a href="#" class="darken" style="margin-left:1.5%; margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/UnitedStates-sml.jpg')}}" style="width:290px; height:400px;   margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:9.0%; color:#ffffff; font-size:18px;" class="SanFrancisco" type="button" value="San Francisco" onclick="showResult('San Francisco, CA USA', '46867');"/>
                                                <input style="top:55%; left:9.5%; color:#ffffff; font-size:18px;" class="LosAngeles" type="button" value="Los Angeles" onclick="showResult('Los Angeles, CA, USA', '46852');"/>
                                                <input style="top:65%; left:10.0%; color:#ffffff; font-size:18px;" class="LasVegas" type="button" value="Las Vegas" onclick="showResult('Las Vegas, NV USA', '46851');"/>
                                                <input style="top:75%; left:10.2%; color:#ffffff; font-size:18px;" class="NewYork" type="button" value="New York" onclick="showResult('New York, NY, USA', '46858');"/>
                                                <input style="top:85%; left:10.6%; color:#ffffff; font-size:18px;" class="Chicago" type="button" value="Chicago" onclick="showResult('Chicago, IL USA', '46840');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:70px;top:170px;">United States<br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Canada-sml.jpg')}}" style="width:290px;height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:34.2%; color:#ffffff; font-size:18px;" class="Vancouver" type="button" value="Vancouver" onclick="showResult('Vancouver, British Columbia, Canada', '6380');"/>
                                                <input style="top:55%; left:34.6%; color:#ffffff; font-size:18px;" class="Montreal" type="button" value="Montreal" onclick="showResult('Montreal, Québec, Canada', '3177');"/>
                                                <input style="top:65%; left:34.8%; color:#ffffff; font-size:18px;" class="Toronto" type="button" value="Toronto" onclick="showResult('Toronto, Ontario, Canada', '5670');"/>
                                                <input style="top:75%; left:35%; color:#ffffff; font-size:18px;" class="Calgary" type="button" value="Calgary" onclick="showResult('Calgary, Alberta, Canada', '6397');"/>
                                                <input style="top:85%; left:35%; color:#ffffff; font-size:18px;" class="Quebec" type="button" value="Quebec" onclick="showResult('Quebec, Québec, Canada', '6352');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:100px; top:170px;">Canada<br/>&nbsp; &nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Mexico-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:58.9%; color:#ffffff; font-size:18px;" class="Cancun" type="button" value="Cancun" onclick="showResult('Cancun, Quintana Roo, Mexico', '1394');"/>
                                                <input style="top:55%; left:57.6%; color:#ffffff; font-size:18px;" class="MexicoCity" type="button" value="Mexico City" onclick="showResult('Mexico City, Mexico', '3597');"/>
                                                <input style="top:65%; left:56.6%; color:#ffffff; font-size:18px;" class="PuertoVallarta" type="button" value="Puerto Vallarta" onclick="showResult('Puerto Vallarta, Jalisco, Mexico', '11339');"/>
                                                <input style="top:75%; left:57.6%; color:#ffffff; font-size:18px;" class="OaxacaCity" type="button" value="Oaxaca City" onclick="showResult('Oaxaca City, Oaxaca, Mexico', '4208');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:105px; top:170px;">Mexico
                                                <br/>&nbsp; &nbsp;</p>
                                        </a>
                                        <a href="#" class="darken">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Brazil-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:80.5%; color:#ffffff; font-size:18px;" class="RiodeJaneiro" type="button" value="Rio de Janeiro" onclick="showResult('Rio de Janeiro, State of Rio de Janeiro, Brazil', '4836');"/>
                                                <input style="top:55%; left:81.9%; color:#ffffff; font-size:18px;" class="SaoPaulo" type="button" value="Sao Paulo" onclick="showResult('Sao Paulo, State of São Paulo, Brazil', '5336');"/>
                                                <input style="top:65%; left:82.4%; color:#ffffff; font-size:18px;" class="Salvador" type="button" value="Salvador" onclick="showResult('Salvador, State of Bahia, Brazil', '7100');"/>
                                                <input style="top:75%; left:82.9%; color:#ffffff; font-size:18px;" class="Brasilia" type="button" value="Brasilia" onclick="showResult('Brasilia, Federal District, Brazil', '894');"/>
                                                <input style="top:85%; left:80.9%; color:#ffffff; font-size:18px;" class="Belo Horizonte" type="button" value="Belo Horizonte" onclick="showResult('Belo Horizonte, State of Minas Gerais, Brazil', '659');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:110px;top:170px;">Brazil
                                                <br/>&nbsp; &nbsp;</p>
                                        </a>
                                    @else
                                        <a href="#" class="darken" style="margin-left:1.5%; margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/UnitedStates-sml.jpg')}}" style="width:290px; height:400px;   margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:11.5%; color:#ffffff; font-size:18px;" class="SanFrancisco" type="button" value="旧金山" onclick="showResult('San Francisco, California, United States 旧金山, 加利福尼亚州, 美国', '46867');"/>
                                                <input style="top:55%; left:11.5%; color:#ffffff; font-size:18px;" class="LosAngeles" type="button" value="洛杉矶" onclick="showResult('Los Angeles, California, United States 洛杉矶, 加利福尼亚州, 美国', '46852');"/>
                                                <input style="top:65%; left:10.1%; color:#ffffff; font-size:18px;" class="LasVegas" type="button" value="拉斯维加斯" onclick="showResult('Las Vegas, Nevada, United States 拉斯维加斯, 内华达, 美国', '46851');"/>
                                                <input style="top:75%; left:12.2%; color:#ffffff; font-size:18px;" class="NewYork" type="button" value="纽约" onclick="showResult('New York, United States 纽约州, 美国', '46858');"/>
                                                <input style="top:85%; left:11.5%; color:#ffffff; font-size:18px;" class="Chicago" type="button" value="芝加哥" onclick="showResult('Chicago, Illinois, United States 芝加哥, 伊利诺伊州, 美国', '46840');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:125px;top:170px;">美国<br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Canada-sml.jpg')}}" style="width:290px;height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:35%; color:#ffffff; font-size:18px;" class="Vancouver" type="button" value="温哥华" onclick="showResult('Vancouver, British Columbia, Canada 温哥华, 不列颠哥伦比亚省, 加拿大', '6380');"/>
                                                <input style="top:55%; left:34.2%; color:#ffffff; font-size:18px;" class="Montreal" type="button" value="蒙特利尔" onclick="showResult('Montreal, Québec, Canada 蒙特利尔, 魁北克省, 加拿大', '3177');"/>
                                                <input style="top:65%; left:35%; color:#ffffff; font-size:18px;" class="Toronto" type="button" value="多伦多" onclick="showResult('Toronto, Ontario, Canada 多伦多, 安大略省, 加拿大', '5670');"/>
                                                <input style="top:75%; left:34.2%; color:#ffffff; font-size:18px;" class="Calgary" type="button" value="卡尔加里" onclick="showResult('Calgary, Alberta, Canada 卡尔加里, 艾伯塔省, 加拿大', '6397');"/>
                                                <input style="top:85%; left:35%; color:#ffffff; font-size:18px;" class="Quebec" type="button" value="魁北克" onclick="showResult('Quebec, Québec, Canada 魁北克, 魁北克省, 加拿大', '6352');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:110px; top:170px;">@lang('search.Canada')
                                                <br/>&nbsp; &nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country//Mexico-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:59.7%; color:#ffffff; font-size:18px;" class="Cancun" type="button" value="坎昆" onclick="showResult('Cancun, Quintana Roo, Mexico 坎昆, 金塔纳罗奥, 墨西哥', '1394');"/>
                                                <input style="top:55%; left:58.4%; color:#ffffff; font-size:18px;" class="MexicoCity" type="button" value="墨西哥城" onclick="showResult('Mexico City, Mexico 墨西哥城, 聯邦區, 墨西哥', '3597');"/>
                                                <input style="top:65%; left:57.4%; color:#ffffff; font-size:18px;" class="PuertoVallarta" type="button" value=" 巴亚尔塔港" onclick="showResult('Puerto Vallarta, Jalisco, Mexico 巴亚尔塔港 , 哈利斯科州, 墨西哥', '11339');"/>
                                                <input style="top:75%; left:59.0%; color:#ffffff; font-size:18px;" class="OaxacaCity" type="button" value="瓦哈卡" onclick="showResult('Oaxaca, Mexico 瓦哈卡, 墨西哥', '4208');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:110px; top:170px;">@lang('search.Mexico')
                                                <br/>&nbsp; &nbsp;</p>
                                        </a>
                                        <a href="#" class="darken">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Brazil-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:81.5%; color:#ffffff; font-size:18px;" class="RiodeJaneiro" type="button" value="里约热内卢" onclick="showResult('Rio De Janeiro, State of Rio de Janeiro, Brazil 里约热内卢, 里约热内卢州, 巴西', '4836');"/>
                                                <input style="top:55%; left:83%; color:#ffffff; font-size:18px;" class="SaoPaulo" type="button" value="圣保罗" onclick="showResult('Sao Paulo, State of São Paulo, Brazil 圣保罗, 圣保罗州, 巴西', '5336');"/>
                                                <input style="top:65%; left:82.4%; color:#ffffff; font-size:18px;" class="Salvador" type="button" value="萨尔瓦多" onclick="showResult('Salvador, State of Bahia, Brazil 萨尔瓦多, 巴伊亚州, 巴西', '7100');"/>
                                                <input style="top:75%; left:82.4%; color:#ffffff; font-size:18px;" class="Brasilia" type="button" value="巴西利亚" onclick="showResult('Brasilia, Federal District, Brazil 巴西利亚, 联邦区, 巴西', '894');"/>
                                                <input style="top:85%; left:80.9%; color:#ffffff; font-size:18px;" class="Belo Horizonte" type="button" value="贝洛奥里藏特" onclick="showResult('Belo Horizonte, State of Minas Gerais, Brazil 贝洛奥里藏特, 米纳斯吉拉斯, 巴西', '659');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:120px;top:170px;">@lang('search.Brazil')
                                                <br/>&nbsp; &nbsp;</p>
                                        </a>
                                    @endif
                                </div>
                                <div id="Europe">
                                    @if($language != 0)
                                        <a href="#" class="darken" style="margin-left:1.5%; margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Italy-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:11.5%; color:#ffffff; font-size:18px;" class="Rome" type="button" value="Rome" onclick="showResult('Rome, Lazio, Italy', '4881');"/>
                                                <input style="top:55%; left:11.3%; color:#ffffff; font-size:18px;" class="Venice" type="button" value="Venice" onclick="showResult('Venice, Veneto, Italy', '5943');"/>
                                                <input style="top:65%; left:10.8%; color:#ffffff; font-size:18px;" class="Florence" type="button" value="Florence" onclick="showResult('Florence, Tuscany, Italy', '1921');"/>
                                                <input style="top:75%; left:11.4%; color:#ffffff; font-size:18px;" class="Milan" type="button" value="Milan" onclick="showResult('Milan, Lombardy, Italy', '3646');"/>
                                                <input style="top:85%; left:10.5%; color:#ffffff; font-size:18px;" class="Agrigento" type="button" value="Agrigento" onclick="showResult('Agrigento, Sicily, Italy', '182');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:120px; top:170px;"> Italy <br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Spain-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:34.0%; color:#ffffff; font-size:18px;" class="Barcelona" type="button" value="Barcelona" onclick="showResult('Barcelona, Catalonia, Spain', '555');"/>
                                                <input style="top:55%; left:34.9%; color:#ffffff; font-size:18px;" class="Madrid" type="button" value="Madrid" onclick="showResult('Madrid, Community of Madrid, Spain', '3543');"/>
                                                <input style="top:65%; left:34.5%; color:#ffffff; font-size:18px;" class="Valencia" type="button" value="Valencia" onclick="showResult('Valencia, Valencian Community, Spain', '5901');"/>
                                                <input style="top:75%; left:34.5%; color:#ffffff; font-size:18px;" class="Granada" type="button" value="Granada" onclick="showResult('Granada, Andalusia, Spain', '2188');"/>
                                                <input style="top:85%; left:35.0%; color:#ffffff; font-size:18px;" class="Seville" type="button" value="Seville" onclick="showResult('Seville, Andalusia, Spain', '24009');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:110px; top:170px;">Spain<br/>&nbsp; &nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Germany-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:59.1%; color:#ffffff; font-size:18px;" class="Berlin" type="button" value="Berlin" onclick="showResult('Berlin, Germany', '804');"/>
                                                <input style="top:55%; left:58.6%; color:#ffffff; font-size:18px;" class="Munich" type="button" value="Munich" onclick="showResult('Munich, Bavaria, Germany', '3799');"/>
                                                <input style="top:65%; left:55.7%; color:#ffffff; font-size:18px;" class="FrankfurtamMain" type="button" value="Frankfurt am Main" onclick="showResult('Frankfurt am Main, Hesse, Germany', '6616');"/>
                                                <input style="top:75%; left:58.4%; color:#ffffff; font-size:18px;" class="Hamburg" type="button" value="Hamburg" onclick="showResult('Hamburg, Germany', '2307');"/>
                                                <input style="top:85%; left:58.4%; color:#ffffff; font-size:18px;" class="Dresden" type="button" value="Dresden" onclick="showResult('Dresden, Saxony, Germany', '1645');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:95px;top:170px;">Germany<br/>&nbsp; &nbsp;</p>
                                        </a>
                                        <a href="#" class="darken">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/France-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:83.5%; color:#ffffff; font-size:18px;" class="Paris" type="button" value="Paris" onclick="showResult('Paris, Île-de-France, France', '1625');"/>
                                                <input style="top:55%; left:83.6%; color:#ffffff; font-size:18px;" class="Nice" type="button" value="Nice" onclick="showResult('Nice, Provence-Alpes-Côte d Azur, France', '3954');"/>
                                                <input style="top:65%; left:82.5%; color:#ffffff; font-size:18px;" class="Marseille" type="button" value="Marseille" onclick="showResult('Marseille, Provence-Alpes-Côte d Azur, France', '3542');"/>
                                                <input style="top:75%; left:83.6%; color:#ffffff; font-size:18px;" class="Lyon" type="button" value="Lyon" onclick="showResult('Lyon, Auvergne-Rhône-Alpes, France', '3366');"/>
                                                <input style="top:85%; left:82.9%; color:#ffffff; font-size:18px;" class="Avignon" type="button" value="Avignon" onclick="showResult('Avignon, Provence-Alpes-Côte d Azur, France', '387');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:110px;  top:170px;">France<br/>&nbsp; &nbsp;</p>
                                        </a>
                                    @else
                                        <a href="#" class="darken" style="margin-left:1.5%; margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Italy-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:12%; color:#ffffff; font-size:18px;" class="Rome" type="button" value="罗马" onclick="showResult('Rome, Lazio, Italy 罗马, 拉齐奥, 意大利', '4881');"/>
                                                <input style="top:55%; left:11.3%; color:#ffffff; font-size:18px;" class="Venice" type="button" value="威尼斯" onclick="showResult('Venice, Veneto, Italy 威尼斯, 威尼托, 意大利', '5943');"/>
                                                <input style="top:65%; left:10.8%; color:#ffffff; font-size:18px;" class="Florence" type="button" value="佛罗伦萨" onclick="showResult('Florence, Tuscany, Italy 佛罗伦萨, 托斯卡纳, 意大利', '1921');"/>
                                                <input style="top:75%; left:12%; color:#ffffff; font-size:18px;" class="Milan" type="button" value="米兰" onclick="showResult('Milan, Lombardy, Italy 米兰, 伦巴第, 意大利', '3646');"/>
                                                <input style="top:85%; left:10.2%; color:#ffffff; font-size:18px;" class="Agrigento" type="button" value="阿格里真托" onclick="showResult('Agrigento, Sicily, Italy 阿格里真托, 西西里, 意大利', '182');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:110px; top:170px;">意大利<br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Spain-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files" alt=""/>
                                                <input style="top:45%; left:34.5%; color:#ffffff; font-size:18px;" class="Barcelona" type="button" value="巴塞罗纳" onclick="showResult('Barcelona, Catalonia, Spain 巴塞罗纳, 加泰罗尼亚, 西班牙', '555');"/>
                                                <input style="top:55%; left:34.9%; color:#ffffff; font-size:18px;" class="Madrid" type="button" value="马德里" onclick="showResult('Madrid, Community of Madrid, Spain 马德里, 马德里, 西班牙', '3543');"/>
                                                <input style="top:65%; left:34.5%; color:#ffffff; font-size:18px;" class="Valencia" type="button" value="瓦伦西亚" onclick="showResult('Valencia, Valencian Community, Spain 瓦伦西亚, 巴伦西亚省, 西班牙', '5901');"/>
                                                <input style="top:75%; left:34.5%; color:#ffffff; font-size:18px;" class="Granada" type="button" value="格拉纳达" onclick="showResult('Granada, Andalusia, Spain 格拉纳达, 安达卢西亚, 西班牙', '2188');"/>
                                                <input style="top:85%; left:34.5%; color:#ffffff; font-size:18px;" class="Seville" type="button" value="塞维利亚" onclick="showResult('Seville, Andalusia, Spain 塞维利亚, 安达卢西亚, 西班牙', '24009');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:110px; top:170px;">西班牙<br/>&nbsp; &nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Germany-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:59.7%; color:#ffffff; font-size:18px;" class="Berlin" type="button" value="柏林" onclick="showResult('Berlin, Germany 柏林, 德国', '804');"/>
                                                <input style="top:55%; left:59%; color:#ffffff; font-size:18px;" class="Munich" type="button" value="慕尼黑" onclick="showResult('Munich, Bavaria, Germany 慕尼黑, 拜仁, 德国', '3799');"/>
                                                <input style="top:65%; left:55.6%; color:#ffffff; font-size:18px;" class="FrankfurtamMain" type="button" value="美因河畔法兰克福" onclick="showResult('Frankfurt am Main, Hesse, Germany 美因河畔法兰克福, 黑森, 德国', '6616');"/>
                                                <input style="top:75%; left:59.7%; color:#ffffff; font-size:18px;" class="Hamburg" type="button" value="汉堡" onclick="showResult('Hamburg, Germany 汉堡, 德国', '2307');"/>
                                                <input style="top:85%; left:58.3%; color:#ffffff; font-size:18px;" class="Dresden" type="button" value="德累斯顿" onclick="showResult('Dresden, Saxony, Germany 德累斯顿, 萨克森自由州, 德国', '1645');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:120px;top:170px;">德国<br/>&nbsp; &nbsp;</p>
                                        </a>
                                        <a href="#" class="darken">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/France-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:83.5%; color:#ffffff; font-size:18px;" class="Paris" type="button" value="巴黎" onclick="showResult('Paris, Île-de-France, France 巴黎, 法兰西堡, 法国', '1625');"/>
                                                <input style="top:55%; left:83.5%; color:#ffffff; font-size:18px;" class="Nice" type="button" value="尼斯" onclick="showResult('Nice, France 尼斯, 普罗旺斯-阿尔卑斯-蓝色海岸, 法国', '3954');"/>
                                                <input style="top:65%; left:83.5%; color:#ffffff; font-size:18px;" class="Marseille" type="button" value="马赛" onclick="showResult('Marseille, France 马赛, 普罗旺斯-阿尔卑斯-蓝色海岸, 法国', '3542');"/>
                                                <input style="top:75%; left:83.5%; color:#ffffff; font-size:18px;" class="Lyon" type="button" value="里昂" onclick="showResult('Lyon, Auvergne-Rhône-Alpes, France 里昂, 奥弗涅-罗纳-阿尔卑斯大区, 法国', '3366');"/>
                                                <input style="top:85%; left:82.4%; color:#ffffff; font-size:18px;" class="Avignon" type="button" value="阿维尼翁" onclick="showResult('Avignon, France 阿维尼翁, 普罗旺斯-阿尔卑斯-蓝色海岸, 法国', '387');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:120px; top:170px;">法国<br/>&nbsp; &nbsp;</p>
                                        </a>
                                    @endif
                                </div>
                                <div id="Asia">
                                    @if($language != 0)
                                        <a href="#" class="darken" style=" margin-left:1.5%;margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/China-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:11.4%; color:#ffffff; font-size:18px;" class="Beijing" type="button" value="Beijing" onclick="showResult('Beijing, China', '4321');"/>
                                                <input style="top:55%; left:10.5%; color:#ffffff; font-size:18px;" class="Shanghai" type="button" value="Shanghai" onclick="showResult('Shanghai, China', '5262');"/>
                                                <input style="top:65%; left:10.0%; color:#ffffff; font-size:18px;" class="HongKong" type="button" value="Hong Kong" onclick="showResult('Hong Kong, China', '2428');"/>
                                                <input style="top:75%; left:10.1%; color:#ffffff; font-size:18px;" class="Guangzhou" type="button" value="Guangzhou" onclick="showResult('Guangzhou, Guangdong, China', '2229');"/>
                                                <input style="top:85%; left:10.7%; color:#ffffff; font-size:18px;" class="Chengdu" type="button" value="Chengdu" onclick="showResult('Chengdu, Sichuan, China', '1332');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:112px; top:170px;"> China <br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Thailand-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:34.4%; color:#ffffff; font-size:18px;" class="Bangkok" type="button" value="Bangkok" onclick="showResult('Bangkok, Thailand', '709');"/>
                                                <input style="top:55%; left:33.8%; color:#ffffff; font-size:18px;" class="ChiangMai" type="button" value="Chiang Mai" onclick="showResult('Chiang Mai, Thailand', '1286');"/>
                                                <input style="top:65%; left:33.8%; color:#ffffff; font-size:18px;" class="SuratThani" type="button" value="Surat Thani" onclick="showResult('Surat Thani, Thailand', '5884');"/>
                                                <input style="top:75%; left:34.8%; color:#ffffff; font-size:18px;" class="Pattaya" type="button" value="Pattaya" onclick="showResult('Pattaya, Chon Buri, Thailand ', '4622');"/>
                                                <input style="top:85%; left:33.8%; color:#ffffff; font-size:18px;" class="KrabiTown" type="button" value="Krabi Town" onclick="showResult('Krabi Town, Thailand', '2928');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:95px; top:170px;">Thailand<br/>&nbsp; &nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Indonesia-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:58.9%; color:#ffffff; font-size:18px;" class="Ubud" type="button" value="Ubud" onclick="showResult('Ubud，Indonesia', '34042');"/>
                                                <input style="top:55%; left:58.2%; color:#ffffff; font-size:18px;" class="Jakarta" type="button" value="Jakarta" onclick="showResult('Jakarta, Indonesia', '2708');"/>
                                                <input style="top:65%; left:59.1%; color:#ffffff; font-size:18px;" class="Legian" type="button" value="Kuta" onclick="showResult('Legian, Kuta, Indonesia', '30365');"/>
                                                <input style="top:75%; left:58.0%; color:#ffffff; font-size:18px;" class="Seminyak" type="button" value="Seminyak" onclick="showResult('Seminyak - Bali, Bali, Indonesia', '5472');"/>
                                                <input style="top:85%; left:58.1%; color:#ffffff; font-size:18px;" class="Denpasar" type="button" value="Denpasar" onclick="showResult('Bali-Denpasar, Bali, Indonesia Bali-Denpasar', '9798');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:85px;  top:170px;">Indonesia<br/>&nbsp; &nbsp;</p>
                                        </a>
                                        <a href="#" class="darken">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Japan-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:83.1%; color:#ffffff; font-size:18px;" class="Tokyo" type="button" value="Tokyo" onclick="showResult('Tokyo, Japan', '5755');"/>
                                                <input style="top:55%; left:83.2%; color:#ffffff; font-size:18px;" class="Kyoto" type="button" value="Kyoto" onclick="showResult('Kyoto, Kyoto Prefecture, Japan', '2976');"/>
                                                <input style="top:65%; left:83.2%; color:#ffffff; font-size:18px;" class="Osaka" type="button" value="Osaka" onclick="showResult('Osaka, Osaka Prefecture, Japan', '4158');"/>
                                                <input style="top:75%; left:83.1%; color:#ffffff; font-size:18px;" class="Nagoya" type="button" value="Nagoya" onclick="showResult('Nagoya, Aichi Prefecture, Japan', '3941');"/>
                                                <input style="top:85%; left:82.6%; color:#ffffff; font-size:18px;" class="Nagasaki" type="button" value="Nagasaki" onclick="showResult('Nagasaki, Nagasaki Prefecture, Japan', '3944');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:110px;top:170px;">Japan<br/>&nbsp; &nbsp;</p>
                                        </a>
                                    @else
                                        <a href="#" class="darken" style=" margin-left:1.5%;margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/China-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:12%; color:#ffffff; font-size:18px;" class="Beijing" type="button" value="北京" onclick="showResult('Beijing, China 北京市, 中国', '4321');"/>
                                                <input style="top:55%; left:12%; color:#ffffff; font-size:18px;" class="Shanghai" type="button" value="上海" onclick="showResult('Shanghai, China 上海市, 中国', '5262');"/>
                                                <input style="top:65%; left:12%; color:#ffffff; font-size:18px;" class="HongKong" type="button" value="香港" onclick="showResult('Hong Kong, China 香港, 中国', '2428');"/>
                                                <input style="top:75%; left:12%; color:#ffffff; font-size:18px;" class="Guangzhou" type="button" value="广州" onclick="showResult('Guangzhou, Guangdong, China 广州市, 广东省, 中国', '2229');"/>
                                                <input style="top:85%; left:12%; color:#ffffff; font-size:18px;" class="Chengdu" type="button" value="成都" onclick="showResult('Chengdu, Sichuan, China 成都市, 四川省, 中国', '1332');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:123px; top:170px;">中国<br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Thailand-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:35.6%; color:#ffffff; font-size:18px;" class="Bangkok" type="button" value="曼谷" onclick="showResult('Bangkok, Thailand 曼谷, 泰国', '709');"/>
                                                <input style="top:55%; left:35.0%; color:#ffffff; font-size:18px;" class="ChiangMai" type="button" value="清迈府" onclick="showResult('Chiang Mai, Thailand 清迈府, 泰国', '1286');"/>
                                                <input style="top:65%; left:34.3%; color:#ffffff; font-size:18px;" class="SuratThani" type="button" value="素叻他尼" onclick="showResult('Surat Thani, Thailand 素叻他尼, 泰国', '5884');"/>
                                                <input style="top:75%; left:35.0%; color:#ffffff; font-size:18px;" class="Pattaya" type="button" value="芭堤雅" onclick="showResult('Pattaya, Chon Buri, Thailand 芭堤雅, 春武里府, 泰国', '4622');"/>
                                                <input style="top:85%; left:35.6%; color:#ffffff; font-size:18px;" class="KrabiTown" type="button" value="甲米" onclick="showResult('Krabi, Thailand 甲米, 泰国', '2928');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:120px; top:170px;">泰国<br/>&nbsp; &nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Indonesia-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:58.9%; color:#ffffff; font-size:18px;" class="Ubud" type="button" value="乌布德" onclick="showResult('Ubud, Bali, Indonesia 乌布德, 巴厘岛, 印度尼西亚', '34042');"/>
                                                <input style="top:55%; left:58.9%; color:#ffffff; font-size:18px;" class="Jakarta" type="button" value="雅加达" onclick="showResult('Jakarta, Indonesia 雅加达, 印度尼西亚', '2708');"/>
                                                <input style="top:65%; left:58.3%; color:#ffffff; font-size:18px;" class="Legian" type="button" value="库塔海滩" onclick="showResult('Kuta - Bali, Bali, Indonesia 巴厘岛库塔, 巴厘岛, 印度尼西亚', '30365');"/>
                                                <input style="top:75%; left:57.7%; color:#ffffff; font-size:18px;" class="Seminyak" type="button" value="水明漾海滩" onclick="showResult('Seminyak - Bali, Bali, Indonesia 水明漾海滩, 巴厘岛, 印度尼西亚', '5472');"/>
                                                <input style="top:85%; left:58.9%; color:#ffffff; font-size:18px;" class="Denpasar" type="button" value="登巴萨" onclick="showResult('Bali-Denpasar, Bali, Indonesia 登巴萨, 巴厘岛, 印度尼西亚', '9798');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:82px;  top:170px;">印度尼西亚<br/>&nbsp; &nbsp;</p>
                                        </a>
                                        <a href="#" class="darken">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Japan-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:83.5%; color:#ffffff; font-size:18px;" class="Tokyo" type="button" value="东京" onclick="showResult('Tokyo, Japan 东京, 日本', '5755');"/>
                                                <input style="top:55%; left:83.5%; color:#ffffff; font-size:18px;" class="Kyoto" type="button" value="京都" onclick="showResult('Kyoto, Kyoto Prefecture, Japan 京都市, 京都府, 日本', '2976');"/>
                                                <input style="top:65%; left:83.5%; color:#ffffff; font-size:18px;" class="Osaka" type="button" value="大阪" onclick="showResult('Osaka, Osaka Prefecture, Japan 大阪市, 大阪府, 日本', '4158');"/>
                                                <input style="top:75%; left:82.8%; color:#ffffff; font-size:18px;" class="Nagoya" type="button" value="名古屋" onclick="showResult('Nagoya, Aichi Prefecture, Japan 名古屋市, 爱知县, 日本', '3941');"/>
                                                <input style="top:85%; left:83.5%; color:#ffffff; font-size:18px;" class="Nagasaki" type="button" value="长崎" onclick="showResult('Nagasaki, Nagasaki Prefecture, Japan 长崎， 长崎县， 日本', '3944');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:118px;top:170px;">日本<br/>&nbsp; &nbsp;</p>
                                        </a>
                                    @endif
                                </div>
                                <div id="Others">
                                    @if($language != 0)
                                        <a href="#" class="darken" style=" margin-left:1.5%;margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Australia-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:11.2%; color:#ffffff; font-size:18px;" class="Sydney" type="button" value="Sydney" onclick="showResult('Sydney, New South Wales, Australia', '5335');"/>
                                                <input style="top:55%; left:10.3%; color:#ffffff; font-size:18px;" class="Melbourne" type="button" value="Melbourne" onclick="showResult('Melbourne, Victoria, Australia', '3834');"/>
                                                <input style="top:65%; left:10.8%; color:#ffffff; font-size:18px;" class="Brisbane" type="button" value="Brisbane" onclick="showResult('Brisbane, Queensland, Australia', '748');"/>
                                                <input style="top:75%; left:11.4%; color:#ffffff; font-size:18px;" class="Perth" type="button" value="Perth" onclick="showResult('Perth, Western Australia, Australia', '4372');"/>
                                                <input style="top:85%; left:11.0%; color:#ffffff; font-size:18px;" class="Hobart" type="button" value="Hobart" onclick="showResult('Hobart, Tasmania, Australia', '2319');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:95px; top:170px;"> Australia <br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country//NewZealand-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:34.2%; color:#ffffff; font-size:18px;" class="Auckland" type="button" value="Auckland" onclick="showResult('Auckland, New Zealand', '372');"/>
                                                <input style="top:55%; left:33.8%; color:#ffffff; font-size:18px;" class="Wellington" type="button" value="Wellington" onclick="showResult('Wellington, New Zealand', '6122');"/>
                                                <input style="top:65%; left:33.6%; color:#ffffff; font-size:18px;" class="Queenstown" type="button" value="Queenstown" onclick="showResult('Queenstown, Otago, New Zealand', '4694');"/>
                                                <input style="top:75%; left:33.6%; color:#ffffff; font-size:18px;" class="Christchurch" type="button" value="Christchurch" onclick="showResult('Christchurch, Canterbury, New Zealand', '1162');"/>
                                                <input style="top:85%; left:34.7%; color:#ffffff; font-size:18px;" class="Rotorua" type="button" value="Rotorua" onclick="showResult('Rotorua, Bay Of Plenty, New Zealand', '4945');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:75px; top:170px;">New Zealand<br/>&nbsp; &nbsp;</p>
                                        </a>
                                    @else
                                        <a href="#" class="darken" style=" margin-left:1.5%;margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country/Australia-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:12.2%; color:#ffffff; font-size:18px;" class="Sydney" type="button" value="悉尼" onclick="showResult('Sydney, New South Wales, Australia 悉尼, 新南威尔士州, 澳大利亚', '5335');"/>
                                                <input style="top:55%; left:11.5%; color:#ffffff; font-size:18px;" class="Melbourne" type="button" value="墨尔本" onclick="showResult('Melbourne, Victoria, Australia 墨尔本, 維多利亞省, 澳大利亚', '3834');"/>
                                                <input style="top:65%; left:10.8%; color:#ffffff; font-size:18px;" class="Brisbane" type="button" value="布里斯班" onclick="showResult('Brisbane, Queensland, Australia 布里斯班, 昆士兰州, 澳大利亚', '748');"/>
                                                <input style="top:75%; left:12.2%; color:#ffffff; font-size:18px;" class="Perth" type="button" value="珀斯" onclick="showResult('Perth, Western Australia, Australia 珀斯, 西澳大利亚州, 澳大利亚', '4372');"/>
                                                <input style="top:85%; left:11.5%; color:#ffffff; font-size:18px;" class="Hobart" type="button" value="荷巴特" onclick="showResult('Hobart, Tasmania, Australia 荷巴特, 塔斯马尼亚州, 澳大利亚', '2319');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:100px; top:170px;"> 澳大利亚 <br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                        </a>
                                        <a href="#" class="darken" style="margin-right:1%;">
                                            <div class="show-cities" style="text-align:center">
                                                <img src="{{asset('img/country//NewZealand-sml.jpg')}}" style="width:290px; height:400px; margin:0 ;cursor:pointer;" class="img-files"/>
                                                <input style="top:45%; left:35%; color:#ffffff; font-size:18px;" class="Auckland" type="button" value="奥克兰" onclick="showResult('Auckland, New Zealand 奥克兰, 新西兰', '372');"/>
                                                <input style="top:55%; left:35%; color:#ffffff; font-size:18px;" class="Wellington" type="button" value="威灵顿" onclick="showResult('Wellington, New Zealand 威灵顿, 新西兰', '6122');"/>
                                                <input style="top:65%; left:35%; color:#ffffff; font-size:18px;" class="Queenstown" type="button" value="皇后镇" onclick="showResult('Queenstown, Otago, New Zealand 皇后镇, 奥塔哥, 新西兰', '4694');"/>
                                                <input style="top:75%; left:35%; color:#ffffff; font-size:18px;" class="Christchurch" type="button" value="基督城" onclick="showResult('Christchurch, Canterbury, New Zealand 基督城, 坎特伯雷, 新西兰', '1162');"/>
                                                <input style="top:85%; left:34.4%; color:#ffffff; font-size:18px;" class="Rotorua" type="button" value="罗托鲁瓦" onclick="showResult('Rotorua, Bay Of Plenty, New Zealand 罗托鲁瓦, 丰盛湾, 新西兰', '4945');"/>
                                            </div>
                                            <p class="img-details" style="margin-left:107px; top:170px;">新西兰<br/>&nbsp; &nbsp;</p>
                                        </a>
                                    @endif
                                </div>
                            </div>

                            <!--test: if viewport smaller than 1400-->
                            <div class="slidebox RecommendsCities smallest">
                                <div id="America">
                                    <a href="#" class="darken" style="margin-left:1.5%; margin-right:1%;">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/UnitedStates-sml.jpg')}}" style="width:225px; height:300px;   margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:8.0%; color:#ffffff; font-size:18px;" class="SanFrancisco" type="button" value="San Francisco" onclick="showResult('San Francisco, CA USA', '46867');"/>
                                            <input style="top:58%; left:8.5%; color:#ffffff; font-size:18px;" class="LosAngeles" type="button" value="Los Angeles" onclick="showResult('Los Angeles, CA, USA', '46852');"/>
                                            <input style="top:68%; left:9.0%; color:#ffffff; font-size:18px;" class="LasVegas" type="button" value="Las Vegas" onclick="showResult('Las Vegas, NV USA', '46851');"/>
                                            <input style="top:78%; left:9.2%; color:#ffffff; font-size:18px;" class="NewYork" type="button" value="New York" onclick="showResult('New York, NY, USA', '46858');"/>
                                            <input style="top:88%; left:9.6%; color:#ffffff; font-size:18px;" class="Chicago" type="button" value="Chicago" onclick="showResult('Chicago, IL USA', '46840');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:90px; top:160px;">USA <br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    </a> <a href="#" class="darken" style="margin-right:1%;">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/Canada-sml.jpg')}}" style="width:225px;height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:32.5%; color:#ffffff;font-size:18px;" class="Vancouver" type="button" value="Vancouver" onclick="showResult('Vancouver, British Columbia, Canada', '6380');"/>
                                            <input style="top:58%; left:32.9%; color:#ffffff;font-size:18px;" class="Montreal" type="button" value="Montreal" onclick="showResult('Montreal, Québec, Canada', '3177');"/>
                                            <input style="top:68%; left:33.1%; color:#ffffff;font-size:18px;" class="Toronto" type="button" value="Toronto" onclick="showResult('Toronto, Ontario, Canada', '5670');"/>
                                            <input style="top:78%; left:33.3%; color:#ffffff;font-size:18px;" class="Calgary" type="button" value="Calgary" onclick="showResult('Calgary, Alberta, Canada', '6397');"/>
                                            <input style="top:98%; left:33.3%; color:#ffffff;font-size:18px;" class="Quebec" type="button" value="Quebec" onclick="showResult('Quebec, Québec, Canada', '6352');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:70px; top:160px;">Canada <br/>&nbsp; &nbsp;</p>
                                    </a> <a href="#" class="darken" style="margin-right:1%;">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country//Mexico-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:56.9%; color:#ffffff; font-size:18px;" class="Cancun" type="button" value="Cancun" onclick="showResult('Cancun, Quintana Roo, Mexico', '1394');"/>
                                            <input style="top:58%; left:55.6%; color:#ffffff; font-size:18px;" class="MexicoCity" type="button" value="Mexico City" onclick="showResult('Mexico City, Mexico', '3597');"/>
                                            <input style="top:68%; left:54.6%; color:#ffffff; font-size:18px;" class="PuertoVallarta" type="button" value="Puerto Vallarta" onclick="showResult('Puerto Vallarta, Jalisco, Mexico', '11339');"/>
                                            <input style="top:78%; left:55.6%; color:#ffffff; font-size:18px;" class="OaxacaCity" type="button" value="Oaxaca City" onclick="showResult('Oaxaca City, Oaxaca, Mexico', '4208');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:70px; top:160px;">Mexico <br/>&nbsp; &nbsp;</p>
                                    </a> <a href="#" class="darken">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/Brazil-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:77.5%; color:#ffffff; font-size:18px;" class="RiodeJaneiro" type="button" value="Rio de Janeiro" onclick="showResult('Rio de Janeiro, State of Rio de Janeiro, Brazil', '4836');"/>
                                            <input style="top:58%; left:78.9%; color:#ffffff; font-size:18px;" class="SaoPaulo" type="button" value="Sao Paulo" onclick="showResult('Sao Paulo, State of São Paulo, Brazil', '5336');"/>
                                            <input style="top:68%; left:79.4%; color:#ffffff; font-size:18px;" class="Salvador" type="button" value="Salvador" onclick="showResult('Salvador, State of Bahia, Brazil', '7100');"/>
                                            <input style="top:78%; left:79.9%; color:#ffffff; font-size:18px;" class="Brasilia" type="button" value="Brasilia" onclick="showResult('Brasilia, Federal District, Brazil', '894');"/>
                                            <input style="top:88%; left:77.9%; color:#ffffff; font-size:18px;" class="Belo Horizonte" type="button" value="Belo Horizonte" onclick="showResult('Belo Horizonte, State of Minas Gerais, Brazil', '659');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:80px;top:160px;">Brazil <br/>&nbsp; &nbsp;</p>
                                    </a>

                                </div>
                                <div id="Europe">
                                    <a href="#" class="darken" style="margin-left:1.5%; margin-right:1%;">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/Italy-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:10.5%; color:#ffffff; font-size:18px;" class="Rome" type="button" value="Rome" onclick="showResult('Rome, Italy', '4881');"/>
                                            <input style="top:58%; left:10.3%; color:#ffffff; font-size:18px;" class="Venice" type="button" value="Venice" onclick="showResult('Venice, Italy', '5943');"/>
                                            <input style="top:68%; left:9.8%; color:#ffffff; font-size:18px;" class="Florence" type="button" value="Florence" onclick="showResult('Florence, Italy', '1921');"/>
                                            <input style="top:78%; left:10.4%; color:#ffffff; font-size:18px;" class="Milan" type="button" value="Milan" onclick="showResult('Milan, Italy', '3646');"/>
                                            <input style="top:88%; left:9.5%; color:#ffffff; font-size:18px;" class="Agrigento" type="button" value="Agrigento" onclick="showResult('Agrigento, Italy', '182');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:86px; top:160px;"> Italy <br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    </a> <a href="#" class="darken" style="margin-right:1%;">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/Spain-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:32.5%; color:#ffffff; font-size:18px;" class="Barcelona" type="button" value="Barcelona" onclick="showResult('Barcelona, Spain', '555');"/>
                                            <input style="top:58%; left:33.4%; color:#ffffff; font-size:18px;" class="Madrid" type="button" value="Madrid" onclick="showResult('Madrid, Spain', '3543');"/>
                                            <input style="top:68%; left:33%; color:#ffffff; font-size:18px;" class="Valencia" type="button" value="Valencia" onclick="showResult('Valencia, Spain', '5901');"/>
                                            <input style="top:78%; left:33%; color:#ffffff; font-size:18px;" class="Granada" type="button" value="Granada" onclick="showResult('Granada, Spain', '2188');"/>
                                            <input style="top:88%; left:33.5%; color:#ffffff; font-size:18px;" class="Seville" type="button" value="Seville" onclick="showResult('Seville, Spain', '24009');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:80px; top:160px;">Spain<br/>&nbsp; &nbsp;</p>
                                    </a> <a href="#" class="darken" style="margin-right:1%;">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/Germany-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:57.5%; color:#ffffff; font-size:18px;" class="Berlin" type="button" value="Berlin" onclick="showResult('Berlin, Germany', '804');"/>
                                            <input style="top:58%; left:57%; color:#ffffff; font-size:18px;" class="Munich" type="button" value="Munich" onclick="showResult('Munich, Germany', '3799');"/>
                                            <input style="top:68%; left:53.4%; color:#ffffff; font-size:18px;" class="FrankfurtamMain" type="button" value="Frankfurt am Main" onclick="showResult('Frankfurt am Main, Germany', '6616');"/>
                                            <input style="top:78%; left:56.4%; color:#ffffff; font-size:18px;" class="Hamburg" type="button" value="Hamburg" onclick="showResult('Hamburg, Germany', '2307');"/>
                                            <input style="top:88%; left:56.7%; color:#ffffff; font-size:18px;" class="Dresden" type="button" value="Dresden" onclick="showResult('Dresden, Germany', '1645');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:65px;top:160px;">Germany<br/>&nbsp; &nbsp;</p>
                                    </a> <a href="#" class="darken">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/France-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:81%; color:#ffffff; font-size:18px;" class="Paris" type="button" value="Paris" onclick="showResult('Paris，France', '1625');"/>
                                            <input style="top:58%; left:81.1%; color:#ffffff; font-size:18px;" class="Nice" type="button" value="Nice" onclick="showResult('Nice, France', '3954');"/>
                                            <input style="top:68%; left:80%; color:#ffffff; font-size:18px;" class="Marseille" type="button" value="Marseille" onclick="showResult('Marseille, France', '3542');"/>
                                            <input style="top:78%; left:81.1%; color:#ffffff; font-size:18px;" class="Lyon" type="button" value="Lyon" onclick="showResult('Lyon, France', '3366');"/>
                                            <input style="top:88%; left:80.4%; color:#ffffff; font-size:18px;" class="Avignon" type="button" value="Avignon" onclick="showResult('Avignon, Oaxaca, France', '387');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:75px;  top:160px;">France<br/>&nbsp; &nbsp;</p>
                                    </a>
                                </div>
                                <div id="Asia">
                                    <a href="#" class="darken" style=" margin-left:1.5%;margin-right:1%;">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/China-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:11.0%; color:#ffffff; font-size:18px;" class="Beijing" type="button" value="Beijing" onclick="showResult('Beijing, China', '4321');"/>
                                            <input style="top:58%; left:10.1%; color:#ffffff; font-size:18px;" class="Shanghai" type="button" value="Shanghai" onclick="showResult('Shanghai, China', '5262');"/>
                                            <input style="top:68%; left:9.6%; color:#ffffff; font-size:18px;" class="HongKong" type="button" value="Hong Kong" onclick="showResult('Hong Kong, China', '2428');"/>
                                            <input style="top:78%; left:9.7%; color:#ffffff; font-size:18px;" class="Guangzhou" type="button" value="Guangzhou" onclick="showResult('Guangzhou, China', '2229');"/>
                                            <input style="top:88%; left:10.3%; color:#ffffff; font-size:18px;" class="Chengdu" type="button" value="Chengdu" onclick="showResult('Chengdu, China', '1332');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:85px; top:160px;"> China <br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    </a> <a href="#" class="darken" style="margin-right:1%;">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/Thailand-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:32.6%; color:#ffffff; font-size:18px;" class="Bangkok" type="button" value="Bangkok" onclick="showResult('Bangkok, Thailand', '709');"/>
                                            <input style="top:58%; left:32%; color:#ffffff; font-size:18px;" class="ChiangMai" type="button" value="Chiang Mai" onclick="showResult('Chiang Mai, Thailand', '1286');"/>
                                            <input style="top:68%; left:32%; color:#ffffff; font-size:18px;" class="SuratThani" type="button" value="Surat Thani" onclick="showResult('Surat Thani, Thailand', '5884');"/>
                                            <input style="top:78%; left:33%; color:#ffffff; font-size:18px;" class="Pattaya" type="button" value="Pattaya" onclick="showResult('Pattaya, Thailand', '4622');"/>
                                            <input style="top:88%; left:32%; color:#ffffff; font-size:18px;" class="KrabiTown" type="button" value="Krabi Town" onclick="showResult('Krabi Town, Thailand', '2928');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:63px; top:160px;">Thailand<br/>&nbsp; &nbsp;</p>
                                    </a> <a href="#" class="darken" style="margin-right:1%;">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/Indonesia-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:57.5%; color:#ffffff; font-size:18px;" class="Ubud" type="button" value="Ubud" onclick="showResult('Ubud，Indonesia', '34042');"/>
                                            <input style="top:58%; left:56.6%; color:#ffffff; font-size:18px;" class="Jakarta" type="button" value="Jakarta" onclick="showResult('Jakarta, Indonesia', '2708');"/>
                                            <input style="top:68%; left:54.8%; color:#ffffff; font-size:18px;" class="Legian" type="button" value="Legian & Kuta" onclick="showResult('Legian, Kuta, Indonesia', '30365');"/>
                                            <input style="top:78%; left:55.8%; color:#ffffff; font-size:18px;" class="Seminyak" type="button" value="Seminyak" onclick="showResult('Seminyak, Indonesia', '5472');"/>
                                            <input style="top:88%; left:55.9%; color:#ffffff; font-size:18px;" class="Denpasar" type="button" value="Denpasar" onclick="showResult('Denpasar, Indonesia', '9798');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:60px;  top:160px;">Indonesia<br/>&nbsp; &nbsp;</p>
                                    </a> <a href="#" class="darken">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/Japan-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:81.1%; color:#ffffff; font-size:18px;" class="Tokyo" type="button" value="Tokyo" onclick="showResult('Tokyo, Japan', '5755');"/>
                                            <input style="top:58%; left:81.2%; color:#ffffff; font-size:18px;" class="Kyoto" type="button" value="Kyoto" onclick="showResult('Kyoto, Japan', '2976');"/>
                                            <input style="top:68%; left:81.2%; color:#ffffff; font-size:18px;" class="Osaka" type="button" value="Osaka" onclick="showResult('Osaka, Japan', '4158');"/>
                                            <input style="top:78%; left:81.1%; color:#ffffff; font-size:18px;" class="Nagoya" type="button" value="Nagoya" onclick="showResult('Nagoya, Japan', '3941');"/>
                                            <input style="top:88%; left:80.6%; color:#ffffff; font-size:18px;" class="Nagasaki" type="button" value="Nagasaki" onclick="showResult('Nagasaki, Japan', '3944');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:80px;top:160px;">Japan<br/>&nbsp; &nbsp;</p>
                                    </a>
                                </div>
                                <div id="Others">
                                    <a href="#" class="darken" style=" margin-left:1.5%;margin-right:1%;">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/Australia-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:10.5%; color:#ffffff; font-size:18px;" class="Sydney" type="button" value="Sydney" onclick="showResult('Sydney, Australia', '5335');"/>
                                            <input style="top:58%; left:9.3%; color:#ffffff; font-size:18px;" class="Melbourne" type="button" value="Melbourne" onclick="showResult('Melbourne, Australia', '3834');"/>
                                            <input style="top:68%; left:10.1%; color:#ffffff; font-size:18px;" class="Brisbane" type="button" value="Brisbane" onclick="showResult('Brisbane, Australia', '748');"/>
                                            <input style="top:78%; left:10.8%; color:#ffffff; font-size:18px;" class="Perth" type="button" value="Perth" onclick="showResult('Perth, Australia', '4372');"/>
                                            <input style="top:88%; left:10.3%; color:#ffffff; font-size:18px;" class="Hobart" type="button" value="Hobart" onclick="showResult('Hobart, Australia', '2319');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:63px; top:160px;"> Australia <br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    </a> <a href="#" class="darken" style="margin-right:1%;">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country//NewZealand-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:32.2%; color:#ffffff; font-size:18px;" class="Auckland" type="button" value="Auckland" onclick="showResult('Auckland, New Zealand', '372');"/>
                                            <input style="top:58%; left:31.8%; color:#ffffff; font-size:18px;" class="Wellington" type="button" value="Wellington" onclick="showResult('Wellington, New Zealand', '6122');"/>
                                            <input style="top:68%; left:31.6%; color:#ffffff; font-size:18px;" class="Queenstown" type="button" value="Queenstown" onclick="showResult('Queenstown, New Zealand', '4694');"/>
                                            <input style="top:78%; left:31.6%; color:#ffffff; font-size:18px;" class="Christchurch" type="button" value="Christchurch" onclick="showResult('Christchurch, New Zealand', '1162');"/>
                                            <input style="top:88%; left:32.7%; color:#ffffff; font-size:18px;" class="Rotorua" type="button" value="Rotorua" onclick="showResult('Rotorua, New Zealand', '4945');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:40px; top:160px;">New Zealand<br/>&nbsp; &nbsp;</p>
                                    </a> <a href="#" class="darken" style="margin-right:1%;">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country/Egypt-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:57.4%; color:#ffffff; font-size:18px;" class="Cairo" type="button" value="Cairo" onclick="showResult('Cairo, Egypt', '1237');"/>
                                            <input style="top:58%; left:56.1%; color:#ffffff; font-size:18px;" class="Alexandria" type="button" value="Alexandria" onclick="showResult('Alexandria, Egypt', '154');"/>
                                            <input style="top:68%; left:56.1%; color:#ffffff; font-size:18px;" class="Hurghada" type="button" value="Hurghada" onclick="showResult('Hurghada, Egypt', '2487');"/>
                                            <input style="top:78%; left:57.4%; color:#ffffff; font-size:18px;" class="Luxor" type="button" value="Luxor" onclick="showResult('Luxor, Egypt', '3327');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:80px;  top:160px;">Egypt<br/>&nbsp; &nbsp;</p>
                                    </a> <a href="#" class="darken">
                                        <div class="show-cities" style="text-align:center">
                                            <img src="{{asset('img/country//Morocco-sml.jpg')}}" style="width:225px; height:300px; margin:0 ;cursor:pointer;" class="img-files"/>
                                            <input style="top:48%; left:78.8%; color:#ffffff; font-size:18px;" class="Marrakesh" type="button" value="Marrakesh" onclick="showResult('Marrakesh, Morocco', '3678');"/>
                                            <input style="top:58%; left:81.5%; color:#ffffff; font-size:18px;" class="Fez" type="button" value="Fez" onclick="showResult('Fez, Morocco', '6607');"/>
                                            <input style="top:68%; left:78.7%; color:#ffffff; font-size:18px;" class="Casablanca" type="button" value="Casablanca" onclick="showResult('Casablanca, Morocco', '1138');"/>
                                            <input style="top:78%; left:79.8%; color:#ffffff; font-size:18px;" class="Tangier" type="button" value="Tangier" onclick="showResult('Tangier, Morocco', '5587');"/>
                                        </div>
                                        <p class="img-details" style="margin-left:60px;top:160px;">Morocco<br/>&nbsp; &nbsp;</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--酒店详情 End-->
    </div>
    {{--团体订购 modal--}}
    @include('components.group-request-form')
@endsection

@section('scripts')
    <script type="text/javascript">
        var page_lang = '{{ $language }}';
    </script>

    <script src='{{ asset('assets/js/business/plugins.search.js') }}'></script>

    {{--single page script--}}
    @include('business.includes.js-search')

@endsection
