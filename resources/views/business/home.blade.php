<!DOCTYPE html>
<html lang="{{ $language === 0 ? 'zh-Hans' : $language === 2 ? 'zh-Hant': 'en' }}">
<head>
    <meta charset="utf-8">
    <title>{{config('app.name')}} - {{trans('home_b.title')}}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{trans('home_b.meta_description')}}">
    <meta name="author" content="117book">
    <meta name="_token" content="{{ csrf_token() }}">
    <link href="{{asset('117book.ico')}}" rel="shortcut icon">
    <link rel="stylesheet" href="{{ asset('assets/css/business/plugins.home.css') }}?v=100">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
</head>
<body id="page-top" class="landing-page @if(\App::getLocale() === 'en') en-page @endif">
{{--1. 导航栏--}}
<div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container" style="padding-bottom:10px;">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
                    <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/')}}">
                    @if(\App::getLocale() === 'cn')
                        <img src="img/landing/Logo-CN.png"/>
                    @else
                        <img src="img/landing/Logo-EN.png"/>
                    @endif
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll scroll-active" href="#page-top">{{ trans('home_b.home')}}</a>
                    </li>
                    <li>
                        <a class="page-scroll scroll-active" href="#partnerReviews">{{ trans('home_b.reviews')}}</a>
                    </li>
                    <li>
                        <a class="page-scroll scroll-active" href="#strengths">{{ trans('home_b.strengths')}}</a>
                    </li>
                    <li>
                        <a class="page-scroll scroll-active" href="#orderFlow">{{ trans('home_b.how_to_book')}}</a>
                    </li>
                    <li>
                        <a class="page-scroll scroll-active" href="#aboutUs">{{ trans('home_b.about_us')}}</a>
                    </li>
                    <li>
                        <a class="page-scroll scroll-active" href="#contactUs">{{ trans('home_b.contact_us')}}</a>
                    </li>
                    @if(\App::getLocale() === 'cn')
                        <li>
                            <a class="page-scroll" href="{{ url('/lang/update?lang=1') }}">ENGLISH</a>
                        </li>
                    @else
                        <li>
                            <a class="page-scroll" href="{{ url('/lang/update?lang=0') }}">中文</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</div>
{{--2. 广告banner pc--}}
<div id="myCarousel" class="carousel slide carousel-inner hidden-xs" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        @for($i=0; $i<7; $i++)
            <li data-target="#myCarousel" data-slide-to="{{$i}}" @if($i===0) class="active" @endif></li>
        @endfor
    </ol>
    <!-- Wrapper for slides -->
    <div class="container">
        <div class="carousel-caption col-mm-12">
            <h1>{{trans('home_b.slogan')}}</h1>
            <br/>
            <h4 class="sub-title">
                {!! trans('home_b.slogan_subtitle') !!}</h4>
            <div class="mobile-height"></div>
            @if(Auth::check())
                <p class="text-hello">
                    {{trans('home_b.hello')}}，{{Auth::user()->name}}</p>
                <a class="btn btn-lg btn-primary" href="{{ url('search') }}" role="button">{{trans('home_b.my_account')}}</a>
                <a class="btn btn-lg btn-primary btn-outline" href="{{ url('logout') }}" role="button">{{trans('home_b.sign_out')}}</a>
            @else
                <a id="loginsmall" class="grouprequestcn btn btn-lg btn-outline btn-primary" href="{{url('/login')}}" role="button">{{trans('home_b.login')}}</a>
                <a class="grouprequestcn btn btn-lg btn-primary" href="{{url('/register')}}" role="button">{{trans('home_b.sign_up')}}</a>
                <br/>
                <a class="grouprequestcn btn btn-lg btn-primary btn-outline btn-v" data-toggle="modal" data-target="#myModalHorizontal">
                    {{trans('home_b.group_order')}} <span style="font-size:11px;" class="hideatsmall">{{trans('home_b.group_order_info')}} </span> </a>
            @endif
        </div>

        <!-- login area -->
        @if(!Auth::check())
            <div class="carousel-login">
                <form method="POST" action="{{ url('/login') }}">
                    {!! csrf_field() !!}
                    <div class="box">
                        <div class="form-group">
                            <label style="letter-spacing:3px;">{{trans('home_b.login')}}</label>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control" {{ $errors->has('email') ? ' style=margin-bottom:10px;' : '' }} name="email" value="{{ old('email') }}"
                                   placeholder="{{trans('home_b.input_email')}}" autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block" style="margin-top:-10px; color:#fff; ">
										<strong style="font-weight:300!important;">{{ $errors->first('email') }}</strong>
                                        <!--strong>{{ $errors->first('email') }}</strong-->
										</span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" {{ $errors->has('email') ? ' style=margin-bottom:10px;' : '' }} name="password"
                                   placeholder="{{trans('home_b.input_pwd')}}">
                            @if ($errors->has('password'))
                                <span class="help-block">
										<strong style="font-weight:300!important;">{{ $errors->first('password') }}</strong>
                                        <!--strong>{{ $errors->first('password') }}</strong-->
										</span>
                            @endif
                        </div>
                        <div class="form-group text-center">
                            <button type="submit"
                                    style="margin-top:30px; height:40px; border-radius:0px; font-size:16px; letter-spacing:2px; font-weight:400; text-shadow:1px 1px grey;  border-color:#f1791e;"
                                    class="btn btn-primary btn-block">
                                {{trans('home_b.partner_login')}}
                            </button>
                            <div class="row">
                                <div class="pull-left">
										<span class="checkbox i-checks">
											<label> <input type="checkbox" name="remember">{{trans('home_b.remember_me')}}</label>
										</span>
                                </div>
                                <div class="pull-right">
                                    <span class="checkbox"><a class="forget" href="{{ url('/password/reset') }}">{{trans('home_b.forgot_password')}}</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        @endif
    </div>
    <div class="carousel-inner" role="listbox">
        {{-- hotle ids for sliders --}}
        @php
            $ids = ['1063926', '1535796', '1063212', '1063312', '1059783','1096422', '1066046'];
        @endphp
        @for($i=1; $i<8; $i++)
            <div class="item item{{$i}}@if($i===1) active @endif" onclick="jumpAd('hotel', {{$ids[$i-1]}})">
            </div>
        @endfor
    </div>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
{{--2. 广告banner mobile--}}
<div id="inSlider" class="carousel carousel-fade visible-xs-block" data-ride="carousel">
    {{--
    <ol class="carousel-indicators">
        <li data-target="#inSlider" data-slide-to="0" class="active"></li>
        <li data-target="#inSlider" data-slide-to="1"></li>
    </ol>
    --}}
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption col-mm-12">
                    <h1>{{trans('home_b.slogan')}}</h1>
                    <br/>
                    <h4 class="sub-title">
                        {!! trans('home_b.slogan_subtitle') !!}</h4>
                    <div class="mobile-height"></div>
                    @if(Auth::check())
                        <p class="text-hello">
                            {{trans('home_b.hello')}}，{{Auth::user()->name}}</p>
                        <a class="btn btn-lg btn-primary" href="{{ url('search') }}" role="button">{{trans('home_b.my_account')}}</a>
                        <a class="btn btn-lg btn-primary btn-outline" href="{{ url('logout') }}" role="button">{{trans('home_b.sign_out')}}</a>
                    @else
                        <a id="loginsmall" class="grouprequestcn btn btn-lg btn-outline btn-primary" href="{{url('/login')}}" role="button">{{trans('home_b.login')}}</a>
                        <a class="grouprequestcn btn btn-lg btn-primary" href="{{url('/register')}}" role="button">{{trans('home_b.sign_up')}}</a>
                        <br/>
                        <a class="grouprequestcn btn btn-lg btn-primary btn-outline btn-v" data-toggle="modal" data-target="#myModalHorizontal">
                            {{trans('home_b.group_order')}} <span style="font-size:11px;" class="hideatsmall">{{trans('home_b.group_order_info')}} </span> </a>
                    @endif
                </div>

                <!-- login area -->
                @if(!Auth::check())
                    <div class="carousel-login">
                        <form method="POST" action="{{ url('/login') }}">
                            {!! csrf_field() !!}
                            <div class="box">
                                <div class="form-group">
                                    <label style="letter-spacing:3px;">登录</label>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input type="email" class="form-control" {{ $errors->has('email') ? ' style=margin-bottom:10px;' : '' }} name="email" value="{{ old('email') }}" placeholder="电子邮箱" autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block" style="margin-top:-10px; color:#fff; ">
										<strong style="font-weight:300!important;">{{ $errors->first('email') }}</strong>
                                        <!--strong>{{ $errors->first('email') }}</strong-->
										</span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input type="password" class="form-control" {{ $errors->has('email') ? ' style=margin-bottom:10px;' : '' }} name="password" placeholder="密码">
                                    @if ($errors->has('password'))
                                        <span class="help-block" style="margin-top:-10px; color:#fff; margin-bottom:-20px;">
										<strong style="font-weight:300!important;">{{ $errors->first('password') }}</strong>
                                        <!--strong>{{ $errors->first('password') }}</strong-->
										</span>
                                    @endif
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" style="margin-top:30px; height:40px; border-radius:0px; font-size:16px; letter-spacing:2px; font-weight:400; text-shadow:1px 1px grey;  border-color:#f1791e;" class="btn btn-primary btn-block">
                                        会员登录
                                    </button>
                                    <div class="row" style="margin-right:2px;">
                                        <div class="pull-left">
										<span class="checkbox i-checks">
											<label style="font-size:13px; letter-spacing:2px;"> <input type="checkbox" name="remember"> &nbsp;记住密码 </label>
										</span>
                                        </div>
                                        <div class="pull-right">
                                            <span class="checkbox" style="font-size:13px; letter-spacing:2px;"><a class="forget" href="{{ url('/password/reset') }}">忘记密码？</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @endif

            </div>
            <!-- Set background for slide in css, for mobile jump to best western -->
            <div class="header-back one" style="height:500px;" onclick="jumpAd('hotel', 1063926)"></div>
        </div>
        {{--
        <div class="item">
            <div class="container">
                <div class="carousel-caption blank">
                    <h1>We create meaningful <br/> interfaces that inspire.</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back two"></div>
        </div>
        --}}
    </div>
    {{--
    <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    --}}
</div>
{{--3. 客户评价--}}
<section id="partnerReviews" class="comments gray-section" style="margin-top: 0;">
    <div class="container">
        <div class="row section-line">
            <div class="col-lg-12 text-center">
                <div class="orange-line-bold"></div>
                <h2>{{trans('home_b.reviews_title')}}</h2>
                <h3>
                    <span class="navy reviews-subtitle">{{trans('home_b.reviews_subtitle')}}</span>
                </h3>
            </div>
        </div>
        <div class="row features-block">
            @for($i = 1; $i < 4; $i++)
            <div class="col-lg-4">
                <div class="bubble" style="letter-spacing:1px;">
                    {{trans('home_b.reviews_'. $i .'_1')}}
                </div>
                <div class="comments-avatar">
					<span class="pull-left">
                        @if($i===1)
						<img alt="image" src="img/landing/Ctrip-logo.png">
                        @elseif($i==2)
                        <img alt="image" src="img/landing/Qunar-logo.png">
                        @else
                        <img alt="image" src="img/landing/Ali-logo.png">
                        @endif
					</span>
                    <div class="media-body">
                        <div class="commens-name">
                            {{trans('home_b.reviews_'. $i .'_2')}}
                        </div>
                        <small class="text-muted" style="letter-spacing:1px; color:#767676">
                            {{trans('home_b.reviews_'. $i .'_3')}}
                        </small>
                    </div>
                </div>
            </div>
            @endfor

        </div>
    </div>
</section>
{{--4. 我们的优势--}}
<section id="strengths" class="container services">
    <div class="row">
        @for($i = 1; $i < 5; $i++)
        <div class="col-sm-3">
            <h2 class="text-center">{!! trans('home_b.strengths_'. $i .'_1') !!}</h2>
            <div class="orange-line-bold"></div>
            <p class="text-center strengths-content">{!! trans('home_b.strengths_'. $i .'_2') !!}</p>
        </div>
            @endfor
    </div>
</section>
{{--5. 我们的足迹地图--}}
<section id="footprint" class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2>{{trans('home_b.strengths_title')}}</h2>
            <h3 class="navy">{{trans('home_b.strengths_subtitle')}}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 text-center wow zoomIn animated">
            @if(\App::getLocale() === 'cn')
                <img id="pc" src="img/landing/Map-CN.png" alt="Maps" class="img-responsive">
                <img id="mobile" src="img/landing/Map-CN.png" alt="Maps" class="img-responsive">
            @else
                <img id="pc" src="img/landing/Map-EN.png" alt="Maps" class="img-responsive">
                <img id="mobile" src="img/landing/Map-EN.png" alt="Maps" class="img-responsive">
            @endif
        </div>
    </div>
</section>
{{--6. 订购流程--}}
<section id="orderFlow" class="timeline gray-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="orange-line-bold"></div>
                <h2>{{trans('home_b.book_title')}}</h2>
                <h3>
                    <span class="navy book_subtitle">{{trans('home_b.book_subtitle')}}</span>
                </h3>
            </div>
        </div>
        <div class="row features-block">

            <div class="col-lg-12">
                <div id="vertical-timeline" class="vertical-container light-timeline center-orientation">

                    @for($i = 1; $i < 5; $i++)
                    <div class="vertical-timeline-block">
                        <div class="vertical-timeline-icon navy-bg">
                            @if($i===1)
                            <i class="fa fa-user-plus"></i>
                            @elseif($i===2)
                            <i class="fa fa-sign-in-alt"></i>
                            @elseif($i===3)
                            <i class="fa fa-search"></i>
                            @else
                            <i class="fa fa-dollar-sign"></i>
                            @endif
                        </div>

                        <div class="vertical-timeline-content">
                            <h3><span class="navy book_step_title" style="color:#f1791e; letter-spacing:2px;">{{trans('home_b.book_'. $i .'_2')}}</span></h3>
                            <p style="font-size:14px; color:#767676; letter-spacing:2px;">{{trans('home_b.book_'. $i .'_3')}}</p>
                            <span class="vertical-date" style="font-size:18px; color: #767676;">{{trans('home_b.book_'. $i .'_1')}}</span>
                        </div>
                    </div>
                    @endfor
                </div>
            </div>

        </div>
    </div>

</section>
{{--7. 登入插入banner--}}
<section id="" class="navy-section testimonials" style="margin-top: 0">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 text-left wow zoomIn animated">
                <h2 class="ad_banner_title">{{trans('home_b.ad_banner_title')}}</h2>
                <div class="testimonials-text">
                    <h3 class="ad_banner_subtitle">{{trans('home_b.ad_banner_subtitle')}}</h3>
                </div>

            </div>
            <div class="col-offset-7 col-lg-5 text-right">
                @if(Auth::check())
                    <a class="btn btn-lg btn-primary" style="margin-top:40px; text-shadow:1px 1px grey; letter-spacing:2px; height:40px; border-radius:0px; border-color:#f1791e;"
                       href="{{ url('search') }}" role="button">{{trans('home_b.btn_check_now')}}</a>
                @else
                    <a class="btn btn-lg btn-primary" href="{{url('/register')}}" role="button">{{trans('home_b.sign_up')}}</a>
                    <a class="btn btn-lg btn-primary btn-outline" href="{{url('/login')}}" role="button">{{trans('home_b.partner_login')}}</a>
                @endif
            </div>
        </div>
    </div>

</section>
{{--8. 关于我们--}}
<section id="aboutUs" class="contact" style="background-image: none;margin-top: 0;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="orange-line-bold"></div>
                <h1 style="color:#484848; letter-spacing:2px; font-weight:500; font-size:26px;">{{trans('home_b.about_title')}}</h1>
                <h2>
                    <span class="navy">{{trans('home_b.about_subtitle')}}</span>
                </h2>
            </div>
        </div>
        <div class="row features-block">
            <div class="col-lg-7 features-text wow fadeInLeft animated">
                <p class="about-content">{!! trans('home_b.about_content') !!}</p>
            </div>
            <div class="col-lg-5 text-right m-t-n-lg wow zoomIn animated">
                <img src="img/landing/Desktop-CN.png" class="img-responsive" alt="dashboard">
            </div>
        </div>
    </div>

</section>
{{--9. 联系我们--}}
<section id="contactUs" class="gray-section contact">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="orange-line-bold"></div>
                <h1 style="color:#484848; letter-spacing:2px; font-weight:500; font-size:26px;">{{trans('home_b.contact_title')}}</h1>
                <h2>
                    <span class="navy">{{trans('home_b.contact_subtitle')}}</span>
                </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3 wow fadeInLeft animated">
                <div class="team-member">
                    <div class="row">
                        <div class="col-sm-3">
                            <i class="fa fa-5x fa-phone-square-alt"></i>
                        </div>
                        <div class="col-sm-9 text contact-column">
                            <h3>{{trans('home_b.hotline')}}</h3>
                            <p>({{trans('home_b.country_us')}}) 626-389-0705</p>
                            <p>({{trans('home_b.country_us')}}) 626-434-5267</p>
                            <p>({{trans('home_b.country_us')}}) 626-522-2906</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 wow fadeInLeft animated">
                <div class="team-member">
                    <div class="row">
                        <div class="col-sm-3">
                            <i class="fab fa-5x fa-qq"></i>
                        </div>
                        <div class="col-sm-9 text contact-column">
                            <h3>QQ</h3>
                            <p>({{trans('home_b.country_us')}}) Jet 2355652771</p>
                            <p>({{trans('home_b.country_us')}}) Alex 2355652773</p>
                            <p>({{trans('home_b.country_cn')}}) Liang 2355891218</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 wow fadeInLeft animated">
                <div class="team-member">
                    <div class="row">
                        <div class="col-sm-3">
                            <i class="fa fa-5x fa-envelope"></i>
                        </div>
                        <div class="col-sm-9 text contact-column">
                            <h3>{{trans('home_b.contact_email')}}</h3>
                            <p>({{trans('home_b.country_us')}}) bd@117book.com</p>
                            <p>({{trans('home_b.country_us')}}) hotels@117book.com</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 wow fadeInLeft animated">
                <div class="team-member">
                    <img src="img/landing/QR1.png" class="img-responsive img-wechat" >
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-9 col-lg-offset-1 text-center m-t-lg m-b-lg">
                <p style="font-size:14px; color:#767676">
                    Copyright &copy; <span id="currentYear"></span> 117book.com |
                    117book @if(\App::getLocale() === 'cn')要趣订 @endif All rights reserved.
                    @if(\App::getLocale() === 'cn')|<a href="http://beian.miit.gov.cn" target="_blank"> 粤ICP备17081986号-1</a> @endif</p>
            </div>
        </div>
    </div>
</section>
@include('components.group-request-form')

<script src="{{ asset('assets/js/business/plugins.home.js') }}?v=1000"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
  var prchecktemp;
  var prindex = 1;
  $(document).ready(function() {
    $('body').scrollTop(0);
    $('#phone').mask('999-999-9999', {placeholder: 'XXX-XXX-XXXX'});
    $('#checkin_one').datepicker({
      startDate: '0d',
      todayBtn: 'linked',
      keyboardNavigation: false,
      forceParse: false,
      calendarWeeks: false,
      autoclose: true,
      format: 'yyyy-mm-dd',
    });
    $('#checkout_one').datepicker({
      startDate: '1d',
      todayBtn: 'linked',
      keyboardNavigation: false,
      forceParse: false,
      calendarWeeks: false,
      autoclose: true,
      format: 'yyyy-mm-dd',
    });
    $('#checkin_two').datepicker({
      startDate: '0d',
      todayBtn: 'linked',
      keyboardNavigation: false,
      forceParse: false,
      calendarWeeks: false,
      autoclose: true,
      format: 'yyyy-mm-dd',
    });
    $('#checkout_two').datepicker({
      startDate: '1d',
      todayBtn: 'linked',
      keyboardNavigation: false,
      forceParse: false,
      calendarWeeks: false,
      autoclose: true,
      format: 'yyyy-mm-dd',
    });
    $('#currentYear').text((new Date()).getFullYear());
    groupRequestSetTime();
    $('.i-checks').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green',
    });

    $('body').scrollspy({
      target: '.navbar-fixed-top',
      offset: 80,
    });

    $('a.scroll-active').bind('click', function(event) {
      var link = $(this);
      $('html, body').stop().animate({
        scrollTop: $(link.attr('href')).offset().top - 50,
      }, 500);
      event.preventDefault();
      $('#navbar').collapse('hide');
    });
  });

  var cbpAnimatedHeader = (function() {
    var docElem = document.documentElement,
        header = document.querySelector('.navbar-default'),
        didScroll = false,
        changeHeaderOn = 20;

    function init() {
      window.addEventListener('scroll', function(event) {
        if (!didScroll) {
          didScroll = true;
          setTimeout(scrollPage, 250);
        }
      }, false);
    }

    function scrollPage() {
      var sy = scrollY();
      if (sy >= changeHeaderOn) {
        $(header).addClass('navbar-scroll');
      } else {
        $(header).removeClass('navbar-scroll');
      }
      didScroll = false;
    }

    function scrollY() {
      return window.pageYOffset || docElem.scrollTop;
    }

    init();

  })();

  new WOW().init();

  function jumpAd(type, id) {
    window.location.href = 'jumpAds/' + type + '/' + id;
  }
</script>
</body>
</html>
