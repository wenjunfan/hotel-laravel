<?php
$todayOrderCount = 0;
$todayOrderAmount = 0;

$yestodayOrderCount = 0;
$yestodayOrderAmount = 0;

$monthOrderCount = 0;
$monthOrderAmount = 0;

$lastMonthOrderCount = 0;
$lastMonthOrderAmount = 0;

$days = [];
$tempDays = [];

foreach($past30Days as $day) {
    $tempDays[$day->day] = $day->count . '-' . $day->price;
}

for ($i=29; $i >= 0; $i--) {
    $day = date("m-d", strtotime('-'. $i . 'day'));

    if (isset($tempDays[$day])) {
        $days[$day] = $tempDays[$day];
    }
    else {
        $days[$day] = '0-0';
    }
}

$months = [];
$tempMonths = [];

foreach($past12Months as $month) {
    $tempMonths[$month->month] = $month->count . '-' . $month->price;
}

for ($i=11; $i >= 0; $i--) {
    $month = date("Y-m", strtotime('-'. $i . 'month'));

    if (isset($tempMonths[$month])) {
        $months[$month] = $tempMonths[$month];
    }
    else {
        $months[$month] = '0-0';
    }
}

$labels1 = '';
$count1 = '';
$amount1 = '';
$pos = 0;

foreach($days as $k => $v) {
    ++$pos;

    if ($labels1 != '') {
        $labels1 .= ', ';
        $count1 .= ', ';
        $amount1 .= ', ';
    }

    $labels1 .= '"' . $k . '"';

    $arr = explode('-', $v);

    $count1 .= $arr[0];
    $amount1 .= round($arr[1] / 100, 2);

    if ($pos == 29) {
        $yestodayOrderCount = $arr[0];
        $yestodayOrderAmount = $arr[1];
    }
    else if ($pos == 30) {
        $todayOrderCount = $arr[0];
        $todayOrderAmount = $arr[1];
    }
}

$labels1 = '[' . $labels1 . ']';
$count1 = '[' . $count1 . ']';
$amount1 = '[' . $amount1 . ']';


$labels2 = '';
$count2 = '';
$amount2 = '';
$pos = 0;

foreach($months as $k => $v) {
    ++$pos;

    if ($labels2 != '') {
        $labels2 .= ', ';
        $count2 .= ', ';
        $amount2 .= ', ';
    }

    $labels2 .= '"' . $k . '"';

    $arr = explode('-', $v);

    $count2 .= $arr[0];
    $amount2 .= round($arr[1] / 100, 2);

    if ($pos == 11) {
        $lastMonthOrderCount = $arr[0];
        $lastMonthOrderAmount = $arr[1];
    }
    else if ($pos == 12) {
        $monthOrderCount = $arr[0];
        $monthOrderAmount = $arr[1];
    }
}

$labels2 = '[' . $labels2 . ']';
$count2 = '[' . $count2 . ']';
$amount2 = '[' . $amount2 . ']';

?>

@extends('layouts.master-b')

@section('title', 'Dashoboard')

@section('css')
@endsection

@section('content')
    <div class="row animated fadeInRight">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right" style="background-color:#1ab394;">当天</span>
                    <h5>订单数量</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{ $todayOrderCount }}</h1>

                    @if($yestodayOrderCount == 0)
                        @if($todayOrderCount == 0)
                            <div class="stat-percent font-bold text-success" style="color:#1ab394;">0% <i class="fa fa-level-up-alt"></i></div>
                        @else
                            <div class="stat-percent font-bold text-success" style="color:#1ab394;">100% <i class="fa fa-level-up-alt"></i></div>
                        @endif
                    @else
                        @if($todayOrderCount >= $yestodayOrderCount)
                            <div class="stat-percent font-bold text-success" style="color:#1ab394;">{{ round(($todayOrderCount-$yestodayOrderCount)/$yestodayOrderCount*100)}}% <i class="fa fa-level-up-alt"></i></div>
                        @else
                            <div class="stat-percent font-bold text-primary">{{ round(($yestodayOrderCount-$todayOrderCount)/$yestodayOrderCount*100)}}% <i class="fa fa-level-down-alt"></i></div>
                        @endif
                    @endif

                    <small>新增订单</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right" style="background-color:#1ab394;">当天</span>
                    <h5>营业额</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{ $todayOrderAmount }}</h1>

                    @if($yestodayOrderAmount == 0)
                        @if($todayOrderAmount == 0)
                            <div class="stat-percent font-bold text-success" style="color:#1ab394;">0% <i class="fa fa-level-up-alt"></i></div>
                        @else
                            <div class="stat-percent font-bold text-success" style="color:#1ab394;">100% <i class="fa fa-level-up-alt"></i></div>
                        @endif
                    @else
                        @if($todayOrderAmount >= $yestodayOrderAmount)
                            <div class="stat-percent font-bold text-success" style="color:#1ab394;">{{ round(($todayOrderAmount-$yestodayOrderAmount)/$yestodayOrderAmount*100)}}% <i class="fa fa-level-up-alt"></i></div>
                        @else
                            <div class="stat-percent font-bold text-primary">{{ round(($yestodayOrderAmount-$todayOrderAmount)/$yestodayOrderAmount*100)}}% <i class="fa fa-level-down-alt"></i></div>
                        @endif
                    @endif

                    <small>订单金额</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-primary pull-right">本月</span>
                    <h5>订单数量</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{ $monthOrderCount }}</h1>

                    @if($lastMonthOrderCount == 0)
                        @if($monthOrderCount == 0)
                            <div class="stat-percent font-bold text-success" style="color:#1ab394;">0% <i class="fa fa-level-up-alt"></i></div>
                        @else
                            <div class="stat-percent font-bold text-success" style="color:#1ab394;">100% <i class="fa fa-level-up-alt"></i></div>
                        @endif
                    @else
                        @if($monthOrderCount >= $lastMonthOrderCount)
                            <div class="stat-percent font-bold text-success" style="color:#1ab394;">{{ round(($monthOrderCount-$lastMonthOrderCount)/$lastMonthOrderCount*100)}}% <i class="fa fa-level-up-alt"></i></div>
                        @else
                            <div class="stat-percent font-bold text-primary">{{ round(($lastMonthOrderCount-$monthOrderCount)/$lastMonthOrderCount*100)}}% <i class="fa fa-level-down-alt"></i></div>
                        @endif
                    @endif

                    <small>新增订单</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-primary pull-right">本月</span>
                    <h5>营业额</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{ $monthOrderAmount }}</h1>
                    @if($lastMonthOrderAmount == 0)
                        @if($monthOrderAmount == 0)
                            <div class="stat-percent font-bold text-success" style="color:#1ab394;">0% <i class="fa fa-level-up-alt"></i></div>
                        @else
                            <div class="stat-percent font-bold text-success" style="color:#1ab394;">100% <i class="fa fa-level-up-alt"></i></div>
                        @endif
                    @else
                        @if($monthOrderAmount >= $lastMonthOrderAmount)
                            <div class="stat-percent font-bold text-success" style="color:#1ab394;">{{ round(($monthOrderAmount-$lastMonthOrderAmount)/$lastMonthOrderAmount*100)}}% <i class="fa fa-level-up-alt"></i></div>
                        @else
                            <div class="stat-percent font-bold text-primary">{{ round(($lastMonthOrderAmount-$monthOrderAmount)/$lastMonthOrderAmount*100)}}% <i class="fa fa-level-down-alt"></i></div>
                        @endif
                    @endif

                    <small>订单金额</small>
                </div>
            </div>
        </div>
    </div>

    <div class="row animated fadeInRight">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    前一个月订单/金额
                    <span class="label label-danger pull-right">金额(x100)</span>
                    <span class="label label-primary pull-right" style="background-color:#1ab394;">数量</span>
                </div>
                <div>
                    <div class="ibox-content">
                        <div>
                            <canvas id="lineChart" height="44"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row animated fadeInRight">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    过去一年订单/金额
                    <span class="label label-danger pull-right">金额(x100)</span>
                    <span class="label label-primary pull-right" style="background-color:#1ab394;">数量</span>
                </div>
                <div>
                    <div class="ibox-content">
                        <div>
                            <canvas id="barChart" height="44"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/morris/raphael-2.1.0.min.js') }}"></script>
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            var lineData = {
                labels: {!! $labels1 !!}, //["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "Example dataset",
                        fillColor: "rgba(255,85,85,0.5)",
                        strokeColor: "rgba(255,85,85,1)",
                        pointColor: "rgba(255,85,85,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(255,85,85,1)",
                        data: {!! $amount1 !!}, //[65, 59, 80, 81, 56, 55, 40]
                    },
                    {
                        label: "Example dataset",
                        fillColor: "rgba(26,179,148,0.5)",
                        strokeColor: "rgba(26,179,148,0.7)",
                        pointColor: "rgba(26,179,148,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: {!! $count1 !!}, //[28, 48, 40, 19, 86, 27, 90]
                    }
                ]
            };

            var lineOptions = {
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: true,
                pointDotRadius: 4,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
            };


            var ctx = document.getElementById("lineChart").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

            var barData = {
                labels: {!! $labels2 !!}, //["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(26,179,148,0.5)",
                        strokeColor: "rgba(26,179,148,0.8)",
                        highlightFill: "rgba(26,179,148,0.75)",
                        highlightStroke: "rgba(26,179,148,1)",
                        data: {!! $count2 !!}, //[28, 48, 40, 19, 86, 27, 90]
                    },
                    {
                        label: "My First dataset",
                        fillColor: "rgba(255,85,85,0.5)",
                        strokeColor: "rgba(255,85,85,0.8)",
                        highlightFill: "rgba(255,85,85,0.75)",
                        highlightStroke: "rgba(255,85,85,1)",
                        data: {!! $amount2 !!}, //[65, 59, 80, 81, 56, 55, 40]
                    }
                ]
            };

            var barOptions = {
                scaleBeginAtZero: true,
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                barShowStroke: true,
                barStrokeWidth: 2,
                barValueSpacing: 5,
                barDatasetSpacing: 1,
                responsive: true
            }


            var ctx = document.getElementById("barChart").getContext("2d");
            var myNewChart = new Chart(ctx).Bar(barData, barOptions);
        });

    </script>
@endsection
