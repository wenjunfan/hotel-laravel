{{--//www.117book.com/orders--}}
@extends('layouts.master-b')

@section('title', 'Orders Management')

@section('css')
    <link href="{{ asset('assets/css/admin/plugins.orders.css') }}" rel="stylesheet">
    <style>
        th, td {
            padding: 6px 5px !important;
            word-wrap: break-word;
            text-align: center;
        }

        td {
            font-size: 12px;
        }

        .th-1, .th-12 {
            width: 50px !important;
        }

        .th-2 {
            width: 116px !important;
        }

        .th-4 {
            width: 136px !important;
        }

        .th-5 {
            width: 146px !important;
        }

        .th-6 {
            width: 60px !important;
        }

        .th-8 {
            width: 80px !important;
        }

        .th-9 {
            width: 76px !important;
        }

        .th-10 {
            width: 200px !important;
        }

        .float-e-margins .btn {
            margin-bottom: 0;
        }

    </style>
@endsection

@section('content')
    <div class="row animated fadeInRight">
        {{--start filter--}}
        <div class="col-lg-12" style="margin-bottom:0px;">
            <div class="ibox float-e-margins">
                {{--title--}}
                <div class="ibox-noborder" style="margin-top:20px;">
                    <strong> @lang('orders.CheckOrder') </strong>
                </div>
                {{--start filter--}}
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" id="search-form" name="search-form">
                        {!! csrf_field() !!}
                        <div class="form-group col-lg-9">
                            <div style="margin-bottom:50px;">
                                {{--下单日--}}
                                <label class="col-sm-1 control-label" for="from">@lang('orders.BookingDate'):</label>
                                <div class="col-sm-2 ">
                                    <div class="input-group date">
                                        <input id="from" name="from" type="text" value="{{$from}}"
                                               class="form-control"
                                               placeholder="@lang('orders.YMD')">
                                        <span class="input-group-addon" style="padding-right:0px;">
                                            <i class="fa fa-long-arrow-alt-right"></i></span>
                                    </div>
                                </div>
                                <div class="col-sm-2 " style="padding-left:0px;">
                                    <div class="input-group date"
                                         class="padding-left:0px; padding-right:0px; margin-left;-15px;">
                                        <input id="to" name="to" type="text" class="form-control" value="{{$to}}"
                                               placeholder="@lang('orders.YMD')">
                                    </div>
                                </div>
                                <label class="col-sm-1 pull-left control-label">@lang('orders.State'):</label>
                                {{--订单状态--}}
                                <div class="col-sm-2">
                                    <select class="form-control" id="orderStatus" name="orderStatus" value="{{$orderStatus}}">
                                        <option value="6" @if($orderStatus == 6) selected @endif>@lang('orders.All')</option>
                                        <option value="1">@lang('orders.Processing')</option>
                                        <option value="2">@lang('orders.Confirmed')</option>
                                        <option value="3">@lang('orders.BeingRefunded')</option>
                                        <option value="4">@lang('orders.Refunded')</option>
                                        <option value="5">@lang('orders.Unknown')</option>
                                    </select>
                                </div>
                                {{--search button--}}
                                <div class="col-sm-2">
                                    <a class="btn btn-default btn-block" type="button" href="/orders">@lang('orders.reset-all')</a>
                                </div>
                            </div>
                            <div>
                                {{--入住日--}}
                                <label class="col-sm-1 pull-left control-label" for="from">@lang('orders.CheckInDate'):</label>
                                <div class="col-lg-2 ">
                                    <div class="input-group date">
                                        <input id="cfrom" name="cfrom" type="text" value="{{$cfrom}}"
                                               class="form-control"
                                               placeholder="@lang('orders.YMD')"><span
                                            class="input-group-addon" style="padding-right:0px;"><i
                                                class="fa fa-long-arrow-alt-right"></i></span>
                                    </div>
                                </div>
                                <div class="col-sm-2 " style="padding-left:0px;">
                                    <div class="input-group date"
                                         class="padding-left:0px; padding-right:0px; margin-left;-15px;">
                                        <input id="cto" name="cto" type="text" class="form-control" value="{{$cto}}"
                                               placeholder="@lang('orders.YMD')">
                                    </div>
                                </div>
                                {{--订单号--}}
                                <label
                                    class="col-sm-1 pull-left control-label">@lang('orders.BookingID')
                                    ：</label>
                                <div class="col-sm-2">
                                    <input class="form-control" type="text" id="keywords" name="keywords"
                                           value="{{$keywords}}"/>
                                </div>
                                {{--search button--}}
                                <div class="col-sm-2">
                                    <button class="btn btn-primary btn-block" type="button" onclick="search()"><i
                                            class="fa fa-search"></i>@lang('orders.Search')</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-3" style="margin-bottom: 0;">
                            @if(Auth::user()->paymentType == 'Postpay')
                                <div class="alert alert-warning alert-dismissible" role="alert">
                                    <button aria-label="Close" class="close" data-dismiss="alert" type="button">
                                        <span aria-hidden="true">&times;</span></button>
                                    <strong>Postpay @lang('orders.summary')
                                        : </strong><br/>@lang('orders.TotalPaid'):
                                    ${{session('balance_paid')}}</br> @lang('orders.TotalUnpaid')
                                    : ${{session('balance_unpaid')}}
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
                {{--end filter--}}
            </div>
        </div>
        {{--end filter--}}
        {{--start order list--}}
        <div class="col-lg-12 col-md-12">
            <div class="ibox float-e-margins">
                @lang('orders.OrderList')
                <div>
                    <div id="order-list" class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-orders">
                                <thead>
                                <tr>
                                    {{--1 订单号--}}
                                    <th class="th-1">@lang('orders.BookingID')</th>
                                    {{--2 下单日--}}
                                    <th class="th-2">@lang('orders.BookingDate')</th>
                                    {{--3 酒店名--}}
                                    <th class="th-3">@lang('orders.HotelName')</th>
                                    {{--4 入住/退房--}}
                                    <th class="th-4">@lang('orders.CheckIn/Out')</th>
                                    {{--5 取消政策--}}
                                    <th class="th-5">@lang('orders.Cancellation')</th>
                                    {{--6 订单金额--}}
                                    <th class="th-6">@lang('orders.NetPrice')</th>
                                    {{--7 订单状态--}}
                                    <th class="th-7">@lang('orders.State')</th>
                                    @if(Auth::user()->id == '10601' || Auth::user()->id == '10565' || Auth::user()->id == '10672' || Auth::user()->admin != 0)
                                        {{--8 凭证号--}}
                                        <th class="th-8">凭证号</th>
                                    @endif
                                    @if(Auth::user()->paymentType == 'Postpay')
                                        {{--9 付款日期--}}
                                        <th class="th-9">@lang('orders.duedate')</th>
                                    @endif
                                    {{--10 操作--}}
                                    <th class="th-10">@lang('orders.Action')</th>
                                    {{--11 预订人--}}
                                    <th class="th-11">@lang('orders.Name')</th>
                                    {{--12 支付方式--}}
                                    <th class="th-12">@lang('orders.PayType')</th>
                                    {{--13 备注--}}
                                    <th class="th-13">@lang('orders.specialrequest')
                                        /@lang('orders.Remarks')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        {{--1 订单号--}}
                                        <td>
                                            <strong>{{$order->id}}</strong>
                                        </td>
                                        {{--2 下单日--}}
                                        <td>
                                            {{$order->created_at}}
                                        </td>
                                        {{--3 酒店名--}}
                                        <td>
                                            {{\Illuminate\Support\Str::words($order->hotelName, 8, '...')}} @if($language===0)
                                                <br>{{$order->hotelName_zh}} @endif
                                        </td>
                                        {{--4 入住/退房--}}
                                        <td>
                                            {{$order->checkinDate}} / {{$order->checkoutDate}}
                                        </td>
                                        {{--5 取消政策--}}
                                        <td>
                                            @php
                                                $now = time();
                                                $orderdate=strtotime($order['created_at']);
                                                $room = $order->rooms->first();
                                                $policies= json_decode($room['cancelPolicies'], true);

                                                if (count($policies) != 0 && isset($policies[0]["end"])) {
                                                $canceldate=$policies[0]["end"];
                                                    $end = substr(str_replace("T"," ",$canceldate),0,19);
                                                    $enddate = strtotime($end);
                                                    date_default_timezone_set('America/Los_Angeles');
                                                    if($now<$enddate){
                                                        if($language===1){
                                                            echo "Free cancellation before<br>";
                                                            echo "<strong>".date('Y-m-d H:i:s', $enddate)."</strong>";
                                                            echo "<br>(hotel local time)";
                                                        }
                                                        else{
                                                            echo date('Y-m-d H:i:s', $enddate);
                                                            echo " <br>前免费取消(酒店当地时间)";
                                                        }
                                                    }
                                                    else{
                                                        if($language===1){
                                                            echo "Non-refundable";
                                                            }else{
                                                                echo "不可取消";
                                                            }
                                                    }
                                                }
                                                else {
                                                    if($language===1){
                                                        echo "Non-refundable";
                                                    } else {
                                                        echo "不可取消";
                                                    }
                                                }
                                            @endphp
                                        </td>
                                        {{--6 订单金额--}}
                                        <td>
                                            @if ($order->payment_type == 'ALIPAY')
                                                @if ($order->infact_cost_cn == '' || null == $order->infact_cost_cn)
                                                    {{$order->currencyCode . ' ' . $order->totalPrice}}
                                                @else
                                                    {{'CNY' . ' ' . $order->infact_cost_cn}}
                                                @endif
                                            @else
                                                @if ($order->infact_cost == '' || null == $order->infact_cost)
                                                    {{$order->currencyCode . ' ' . $order->totalPrice}}
                                                @else
                                                    {{'USD' . ' ' . $order->infact_cost}}
                                                @endif
                                            @endif
                                            <br/>
                                            @if ($order->isChecked == 1 && $order->status == 'CONFIRMED' && $order->payment_type == 'PO')
                                                <span class="text-info">(@lang('orders.Paid') √)</span>
                                            @elseif ($order->isChecked != 1 && $order->status == 'CONFIRMED'&& $order->payment_type == 'PO')
                                                (@lang('orders.Unpaid') X)
                                            @elseif ($order->isReturned == 1)
                                                (@lang('orders.returned') √√)
                                            @endif

                                        </td>
                                        {{--7 订单状态--}}
                                        <td>
                                            @if($order->id == 1153 || $order->id == 1677)
                                                <span class="label label-warning">Cancelled With Fees</span>
                                            @else
                                                <span
                                                    class="label {{$order->status == 'CONFIRMED' ? 'label-info' : ($order->status == 'CANCELLED' ? 'label-warning' : ($order->status == 'ON REQUEST' ? 'label-success' : 'label-danger'))}}">
								                    @if ($order->status == 'CONFIRMED')
                                                        @lang('orders.Confirmed')
                                                    @elseif ($order->status == 'CANCELLED')
                                                        @if($order->payment_status == 'PENDING')
                                                            @lang('orders.BeingRefunded')
                                                        @elseif($order->payment_status == 'REFUND')
                                                            @if($order->payment_type == 'PO')
                                                                @lang('orders.alreadyc')
                                                            @else
                                                                @lang('orders.Refunded')
                                                            @endif
                                                        @else
                                                            @if($order->payment_type == 'PO')
                                                                @lang('orders.alreadyc')
                                                            @else
                                                                @lang('orders.Refunded')
                                                            @endif
                                                        @endif
                                                    @elseif ($order->status == 'ON REQUEST')
                                                        @lang('orders.Processing')
                                                    @else
                                                        @lang('orders.Unknown')
                                                    @endif
								                </span>
                                            @endif
                                            @if ($order->status == 'ON REQUEST')
                                                <a target="_blank" href="{{url('/voucher/' . $order->code)}}"
                                                   class="btn btn-xs">@lang('orders.Receipt')
                                                </a>
                                            @endif
                                        </td>
                                        @if(Auth::user()->id == '10601' || Auth::user()->id == '10565' || Auth::user()->id == '10672'|| Auth::user()->admin != 0)
                                        {{--8 凭证号--}}
                                        <td>{{str_replace(',', ' ', $order->orderPReference) }}
					@if(Auth::user()->id == '10601' || Auth::user()->id == '10565' || Auth::user()->id == '10672')
                                            <br/><span style="color:red">{{$order->provider}}</span>
                                        @endif
					</td>
                                        @endif
                                        @if(Auth::user()->paymentType == 'Postpay')
                                        {{--9 付款日期--}}
                                        <td>
                                            @if ($order->status == 'CONFIRMED' && $order->payment_type == 'PO')
                                                @php
                                                    $orderdate=strtotime($order['created_at']);
                                                    $duedate = strtotime("+7 day", $orderdate);
                                                    $room = $order->rooms->first();
                                                    $policies= json_decode($room['cancelPolicies'], true);

                                                    if (count($policies) != 0 && isset($policies[0]["end"])) {
                                                        $canceldate = $policies[0]["end"];
                                                        $end = substr(str_replace("T"," ",$canceldate),0,19);
                                                        $enddate = strtotime($end);
                                                        //echo date('M d, Y H:i:s', $enddate);
                                                    }
                                                    //echo date('Y-m-d H:i:s', $now);
                                                    if($enddate<$duedate && $enddate>$orderdate){
                                                            echo date('M d, Y', $enddate);
                                                    }elseif($enddate>$duedate){
                                                            echo date('M d, Y', $duedate);
                                                    }
                                                    else{
                                                        echo date('M d, Y', $orderdate);
                                                    }
                                                @endphp
                                            @endif
                                        </td>
                                        @endif
                                        {{--10 操作--}}
                                        <td class="text-left">
                                            <a target="_blank" href="{{url('/order/' . $order->orderReference)}}"
                                               class="btn btn-default btn-xs">@lang('orders.OrderInfo')</a>
                                            @if ($order->status == 'CONFIRMED')
                                                <a target="_blank" href="{{url('voucher/' . $order->code)}}"
                                                   class="btn btn-success btn-xs">@lang('orders.Voucher')</a>
                                                @if(strtotime($end) > $now)
                                                    <span id="cancel_{{$order->orderReference}}">
                                                <a onclick="cancelOrder('{{$order->orderReference}}','{{$order->payment_type}}')"
                                                   class="btn btn-danger btn-xs">@lang('orders.Cancel')</a>
                                                </span>
                                                @endif
                                                @if ($order->id == '495')
                                                    <a target="_blank" href="/business/extra-pay/payment-type"
                                                       class="btn btn-default btn-xs text-danger">@lang('orders.extraPay')</a>
                                                @endif
                                            @endif
                                        </td>
                                        {{--11 预订人--}}
                                        <td>
                                            {{$order->bookerLastName . ' ' . $order->bookerFirstName}}
                                        </td>
                                        {{--12 支付方式--}}
                                        <td>
                                            @if ($order->payment_type == 'ALIPAY')
                                                @lang('orders.Alipay')
                                            @elseif ($order->payment_type == 'WECHATPAY')
                                                @lang('orders.WechatPay')
                                            @elseif ($order->payment_type == 'CC')
                                                @lang('orders.CreditCard')
                                                @php
                                                    $payment= $order->payment->first();
                                                @endphp
                                                <p style="font-size:13px;">
                                                    @if(isset($payment->cc_number))
                                                        (#{{substr($payment->cc_number,-4)}})
                                                    @endif
                                                </p>
                                            @elseif ($order->payment_type == 'PO')
                                                PostPay
                                            @elseif ($order->partnerId == '10028')
                                                @lang('orders.client')
                                            @else
                                                @lang('orders.Unpaid')
                                            @endif
                                        </td>
                                        {{--13 备注--}}
                                        <td>
					    @if(Auth::user()->email == 'bd@117book.com' || Auth::user()->email == '210@usitrip.com')
					     @if(isset($order->channel_remark)&& $order->channel_remark !='' &&  $order->channel_remark != null )<p style="color:red">OP備註:</p>
					    @endif<p style="color:blue;">{{$order->channel_remark}}</p>
					    @endif
                                            @if(isset($order->special_request)&& $order->special_request !='' &&  $order->special_request != null )
                                                <p style="color:red">@lang('orders.specialrequest'):</p> <p
                                                    style="text-decoration:underline ">{{$order->special_request}}</p>
                                                <textarea readonly id="remark{{$order->id}}" name="remark{{$order->id}}"
                                                          rows="1" cols="25" maxlength="100"
                                                          style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($order->remark)){{$order->remark}}@endif</textarea>
                                                <button name="roEdit" class="label btn" id="roEditBtn{{$order->id}}"
                                                        onclick="fixOrder1({{$order->id}})"><i
                                                        class="fa fa-edit"></i></button>
                                                <button name="roSave" id="roSaveBtn{{$order->id}}"
                                                        class="label btn hidden" style="margin-top:2px;"
                                                        onclick="fixOrder({{$order->id}})">@lang('orders.Update')</button>
                                            @else
                                                <textarea readonly id="remark{{$order->id}}" name="remark{{$order->id}}"
                                                          rows="2" cols="25" maxlength="100"
                                                          style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($order->remark)){{$order->remark}}@endif</textarea>
                                                <button name="roEdit" class="label btn" id="roEditBtn{{$order->id}}"
                                                        onclick="fixOrder1({{$order->id}})"><i
                                                        class="fa fa-edit"></i></button>
                                                <button name="roSave" id="roSaveBtn{{$order->id}}"
                                                        class="label btn hidden" style="margin-top:2px;"
                                                        onclick="fixOrder({{$order->id}})">@lang('orders.Update')</button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--end order list--}}
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/js/admin/plugins.orders.js') }}"></script>
    {{--single page script--}}
    @include('business.includes.js-orders')
@endsection
