<script>
    var lock;
    var ajaxstate,
      angelastate;
    var dhxhr;
    var dxhr;
    var kxhr;
    var txhr;
    var sxhr;
    var hxhr;
    var rating = [0, 1, 2, 3, 4, 5];
    var price = [0, 1000000];
    var roomNum = 1;
    var days = 1;
    var orderBy = 'minPrice';
    var sortMethod = 'desc';
    //var sortMethod = 'asc';
    var checktemp;
    var index = 1;
    var prchecktemp;
    var prindex = 1;
    var sessionKey; // sessionKey from destination search
    var hotelLength;
    var hotelIndex = 0;
    var hotels;
    var interval;
    var searchCount = 0;
    var searchedHotel = null;
    var isHotelSearch = false;
    var searchHotelsCount = 0;
    var isCh = '{{$language}}' === '0';
    var maxCheckinText = !isCh ? 'Check-in Date should in two years from now' : '入住日期需在两年内';
    var is_usitour = {{$is_usitour}};

    function selectText(element) {
        $('.dest_wrapper .dest_result').hide();
        $('.dest_wrapper').show();
        $('.dest_wrapper .dft_dest').show();
    }

    function goTop(minHeight) {
        $(".backtotop").hide();
        minHeight = minHeight ? minHeight : 400;
        $(window).scroll(function () {
            var s = $(window).scrollTop();

            if (s > minHeight) {
                $(".backtotop").fadeIn(100);
            }
            else {
                $(".backtotop").fadeOut(200);
            }
        });
    }

    function updateCheckin() {
        var checkin = $("#checkin").val();
        var today = new Date();
        var today = today.getFullYear() + "-" + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + "-" + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
        var checkin1 = checkin.replace(/-/g, '');
        var today1 = today.replace(/-/g, '');

        if (checkin1 < today1) {
            $("#checkin").datepicker('setDate', today);
        }
        var checkin = $("#checkin").val();
        var checkout = $("#checkout").val();
        checkin1 = checkin.replace(/-/g, '');
        checkout1 = checkout.replace(/-/g, '');

        if (parseInt(checkin1) >= parseInt(checkout1)) {
            checkin = checkin.split('-');
            var checkin = new Date(checkin[0], (checkin[1] - 1), checkin[2]);
            var n = 1;
            var checkout = new Date(checkin - 0 + n * 86400000);
            var checkout = checkout.getFullYear() + "-" + (checkout.getMonth() + 1 > 9 ? checkout.getMonth() + 1 : '0' + (checkout.getMonth() + 1)) + "-" + (checkout.getDate() < 10 ? '0' + checkout.getDate() : checkout.getDate());
            $("#checkout").datepicker('setDate', checkout);
        }

        $("#checkout").datepicker("setStartDate", checkin);

        var a1 = parseInt(checkin1);
        var a2 = parseInt(checktemp);

        if (a1 == a2 && index != 1 && index != 2) {
            $("#checkout").datepicker('show');
        }
        checktemp = checkin1;
        index++;
    }

    function updateCheckout() {
        var checkout = $("#checkout").val();
        var checkin = $("#checkin").val();
        var today = new Date();
        var today = today.getFullYear() + "-" + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + "-" + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
        var checkout1 = checkout.replace(/-/g, '');
        var today1 = today.replace(/-/g, '');
        var checkin1 = checkin.replace(/-/g, '');
        if (checkout1 <= checkin1) {
            var dcheckin = new Date(checkin);
            var tomorrow = new Date(dcheckin.getTime() + 2 * (24 * 60 * 60 * 1000));
            var tomorrow1 = tomorrow.getFullYear() + "-" + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + "-" + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());
            //alert(tomorrow1);
            $("#checkout").datepicker('setDate', tomorrow1);
        }
        var checkin = $("#checkin").val();
        var checkout = $("#checkout").val();
        var today = new Date();
        var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));

        today1 = today.getFullYear() + "" + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + "" + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
        tomorrow1 = tomorrow.getFullYear() + "" + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + "" + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());
        checkin1 = checkin.replace(/-/g, '');
        checkout1 = checkout.replace(/-/g, '');

        if (parseInt(checkin1) >= parseInt(checkout1)) {
            if (parseInt(checkout1) === parseInt(today1)) {
                swal({
                    title: "退房日期不能早于明天",
                    type: "warning"
                }, function () {
                    setTimeout(function () {$('#checkin').focus();}, 1);
                });

                td = today.getFullYear() + "-" + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + "-" + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
                tm = tomorrow.getFullYear() + "-" + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + "-" + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());

                $("#checkin").datepicker('setDate', td);
                $("#checkout").datepicker('setDate', tm);
            } else {
                //  checkout = checkout.split('-');
                //  var checkout = new Date(checkout[0], (checkout[1]-1), checkout[2]);
                //  var n = 1;
                //          var checkin = new Date(checkout - 0 - n * 86400000);//
                //          var checkin = checkin.getFullYear()+"-"+(checkin.getMonth()+1 > 9 ? checkin.getMonth()+1 : '0'+(checkin.getMonth()+1))+"-"+ (checkin.getDate() <10 ? '0'+checkin.getDate() : checkin.getDate());
                //          $("#checkin").datepicker('setDate', checkin);

            }
        }
    }

    function setupHotelAutoComplete(options) {
        $("#hotelId").val("");

        var pairs = {};
        for (var i = 0; i < options.length; i++) {
            var zh = options[i].name_zh == null ? "" : "^" + options[i].name_zh;
            pairs[options[i].hotelId] = options[i].name + zh + ", " + options[i].state + ", " + options[i].countryPId;
            pairs[options[i].hotelId] = pairs[options[i].hotelId].replace(/null\,/, "");
            $("#destinationId").val(opt.data);

        }

        var lookup = $.map(pairs, function (value, key) {
            return {
                value: value,
                data: key
            };
        });

        $('#hotelName').autocomplete({
            lookup: lookup,
            onSelect: function (opt) {
                var i = opt.value.indexOf("^");
                $("#hotelId").val(opt.data);

                if (i == -1) {
                    @if($language == 0)
                    $("#hotelName_en").val(opt.value);
                    $("#hotelName_zh").val("");
                    @endif
                    @if($language == 1)
                    $("#hotelName_en").val(opt.value);
                    @endif
                } else {
                    @if($language == 0)
                    $("#hotelName_en").val(opt.value.substring(0, i));
                    $("#hotelName_zh").val(opt.value.substring(i + 1));
                    @endif
                    @if($language == 1)
                    $("#hotelName_en").val(opt.value.substring(0, i));
                    @endif
                }
            }
        });

        $('#hotelName').focus();
    }

    //initialize search form
    function check() {
        var today = new Date();
        var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
        td = today.getFullYear() + "-" + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + "-" + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
        tm = tomorrow.getFullYear() + "-" + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + "-" + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());

        $("#destinationName").val("");
        $("#hotelName").val("");
        $("#checkin").datepicker('setDate', td);
        $('#checkout').datepicker('setDate', tm);
        $("#roomCount").val(1);
        $("#adultCount1").val(2);
        $("#childCount1").val(0);
        $("#destinationId").val("");
        $("#hotelId").val("");
        $('#hotelName_en').attr("value", "");
        $('#hotelName_zh').attr("value", "");

        checktemp = $("#checkin").val().replace(/-/g, '');
    }

    //show child age
    function showChildAge(childNo, room) {
        var roomNo = Number(room);
        var childNo = Number(childNo);
        var html = "";

        if (childNo > 0) {

            for (var j = 1; j < childNo + 1; j++) {

                if (j == 1) {
                    html += "<label class='font-normal col-sm-3 control-label'>儿童年龄</label>";
                } else {
                    html += "<label class='font-normal col-sm-3 control-label'>&nbsp;</label>";
                }

                html += "<div class='col-sm-9'>"
                  + "    <select id='age" + roomNo + "_" + j + "' name='age" + roomNo + "_" + j + "' class='form-control' value='1'>"
                  + "     <option value='1'>1岁</option>"
                  + "     <option value='2'>2岁</option>"
                  + "     <option value='3'>3岁</option>"
                  + "     <option value='4'>4岁</option>"
                  + "     <option value='5'>5岁</option>"
                  + "     <option value='6'>6岁</option>"
                  + "     <option value='7'>7岁</option>"
                  + "     <option value='8'>8岁</option>"
                  + "     <option value='9'>9岁</option>"
                  + "     <option value='10'>10岁</option>"
                  + "     <option value='11'>11岁</option>"
                  + "     <option value='12'>12岁</option>"
                  + "     <option value='13'>13岁</option>"
                  + "     <option value='14'>14岁</option>"
                  + "     <option value='15'>15岁</option>"
                  + "     <option value='16'>16岁</option>"
                  + "     <option value='17'>17岁</option>"
                  + "    </select>"
                  + "</div>";
            }
        }

        $('#childAge' + roomNo).html(html);
    }

    <!--搜索中rating-->
    //show available hotels
    function search() {
        var checkin = $("#checkin").val();
        var checkout = $("#checkout").val();
        $('#checkout').datepicker('hide');
        days = calculateDays(checkin, checkout);

        searchHotelsCount = 0;

        /* a quick date format validation */
        var dateformat = /^(\d{4})-(\d{2})-(\d{2})$/;
        if (!checkin.match(dateformat) || !checkout.match(dateformat)) {
            swal({
                title: "{{ trans('search.format') }}",
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "ok"
            });
            return false;
        } else {
            //checkin date validation
            var parts = checkin.split('-');

            var year = parts[0];
            var month = parts[1];
            var day = parts[2];

            var $day = (day.charAt(0) == '0') ? day.charAt(1) : day;
            var $month = (month.charAt(0) == '0') ? month.charAt(1) : month;

            //checkout date validation
            var parts1 = checkout.split('-');

            var year1 = parts1[0];
            var month1 = parts1[1];
            var day1 = parts1[2];

            var $day1 = (day1.charAt(0) == '0') ? day1.charAt(1) : day1;
            var $month1 = (month1.charAt(0) == '0') ? month1.charAt(1) : month1;

            var now = new Date();
            var currentYear = now.getFullYear();

            if ($day > 31 || $day < 1 || $month > 12 || $month < 1 || year < currentYear) {
                swal({
                    title: "{{ trans('search.checkinvalid') }}",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "ok"
                });
                return false;
            }
            if ($day1 > 31 || $day1 < 1 || $month1 > 12 || $month1 < 1 || year1 < currentYear) {
                swal({
                    title: "{{ trans('search.checkoutvalid') }}",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "ok"
                });
                return false;
            }

        }

          var newDate = new Date();
          var maxdm = newDate.toISOString().substr(4, 6);
          var maxCheckin = (newDate.getFullYear()+2)+ maxdm;

          if(checkin > maxCheckin){
            swal({
              title: maxCheckinText,
              type: "warning",
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "ok",
            });
            return false;
          }

        if (days > 30) {
            swal({
                title: "{{ trans('search.limit') }}",
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "ok",
            }, function () {
                //  window.location.href = "{{url('/search')}}";
                //setTimeout(function(){$('#checkout').focus();}, 1);
            });
            return false;
        }
        if ($("#roomCount").val() == "10" && $("#destinationId").val().length != "") {
            $("#destinationId").val(0);
            swal({
                title: "{{ trans('search.groupalert') }}",
                type: "warning",
            }, function () {
                setTimeout(function () {$('#roomCount').focus();}, 1);
            });
            return false;
        }

        if ($("#destinationId").val() == "") {
            $("#destinationId").val(0);
        }

        if ($("#destinationId").val() == 0) {
            swal({
                title: "{{ trans('search.noDestinationInput') }}",
                type: "warning"
            }, function () {
                setTimeout(function () {$('#destinationName').focus();}, 1);
            });
            return false;
        }

        sortMethod = 'desc';

        if ($("#checkin").val() === "") {
            swal({
                title: "请先选择入住日期",
                type: "warning"
            }, function () {
                setTimeout(function () {$('#checkin').focus();}, 1);
            });
            return false;
        }

        if ($("#checkout").val() === "") {
            swal({
                title: "请先选择退房日期",
                type: "warning"
            }, function () {
                setTimeout(function () {$('#checkout').focus();}, 1);
            });
            return false;
        }

        var l = $('.ladda-button-search').ladda();
        l.ladda('start');

        //$("#hotelResearch").html("");
        $("#hotelPage").html("");
        $("#hotelSort").html("");
        $("#hotelFilter").html("");
        $("#recommendedHotelsBlock").html("");
        $("#recommendedHotelsBlock").attr("style", "");
        $('#searchedHotelBlock').html("");

        //get rooms and days
        var checkin = $("#checkin").val();
        var checkout = $("#checkout").val();
        roomNum = $("#roomCount").val();
        days = calculateDays(checkin, checkout);

        var destinationId = $('#destinationId').val();
        if (destinationId.indexOf('H') < 0 && destinationId.indexOf('D') < 0) {
            destinationId = 'D' + destinationId;
        }

        var $searchForm = $('#search-form');
        var listLink = "{{url('/list')}}/" + destinationId + ".html";
        $searchForm.attr('action', listLink);
        $searchForm.submit();
    }

    function calculateDays(checkin, checkout) {
        var date1 = new Date(checkin);
        var date2 = new Date(checkout);
        var date3 = date2.getTime() - date1.getTime();
        var days = Math.floor(date3 / (24 * 3600 * 1000));
        return days;
    }

    function focus1(idname) {
        if (idname == 'button-america') {
            $("#button-america").addClass("btn-bottomfocus");
            $("#button-europe").removeClass("btn-bottomfocus");
            $("#button-asia").removeClass("btn-bottomfocus");
            $("#button-others").removeClass("btn-bottomfocus");
        }
        if (idname == 'button-europe') {
            $("#button-america").removeClass("btn-bottomfocus");
            $("#button-europe").addClass("btn-bottomfocus");
            $("#button-asia").removeClass("btn-bottomfocus");
            $("#button-others").removeClass("btn-bottomfocus");
        }
        if (idname == 'button-asia') {
            $("#button-america").removeClass("btn-bottomfocus");
            $("#button-europe").removeClass("btn-bottomfocus");
            $("#button-asia").addClass("btn-bottomfocus");
            $("#button-others").removeClass("btn-bottomfocus");
        }
        if (idname == 'button-others') {
            $("#button-america").removeClass("btn-bottomfocus");
            $("#button-europe").removeClass("btn-bottomfocus");
            $("#button-asia").removeClass("btn-bottomfocus");
            $("#button-others").addClass("btn-bottomfocus");
        }

    }

    function showResult(destinationName, destinationId) {
        $("#destinationName").val(destinationName);
        $("#destinationId").val(destinationId);
        $("#searchButton").click();
    }

    $('[class^="related"]').not(':first').hide();

    $("#roomCount").on('change', function () {
        $(".related_" + this.value + "_content").show().siblings('[class^="related"]').hide();
    });


    // All document ready events should be put here
    $(function () {
        //热门目的地列表
        U_hotels.destination('destinationName', 'destinationId');

        var hash = window.location.hash !== '' ? window.location.hash : '#America';

        if (hash === '#America') {
            $("#button-america").addClass("btn-bottomfocus");
            $("#button-europe").removeClass("btn-bottomfocus");
            $("#button-asia").removeClass("btn-bottomfocus");
            $("#button-others").removeClass("btn-bottomfocus");
        }
        if (hash === '#Europe') {
            $("#button-america").removeClass("btn-bottomfocus");
            $("#button-europe").addClass("btn-bottomfocus");
            $("#button-asia").removeClass("btn-bottomfocus");
            $("#button-others").removeClass("btn-bottomfocus");
        }
        if (hash === '#Asia') {
            $("#button-america").removeClass("btn-bottomfocus");
            $("#button-europe").removeClass("btn-bottomfocus");
            $("#button-asia").addClass("btn-bottomfocus");
            $("#button-others").removeClass("btn-bottomfocus");
        }
        if (hash === '#Others') {
            $("#button-america").removeClass("btn-bottomfocus");
            $("#button-europe").removeClass("btn-bottomfocus");
            $("#button-asia").removeClass("btn-bottomfocus");
            $("#button-others").addClass("btn-bottomfocus");
        }

        //推荐城市default美洲
        $(window).on('hashchange', function () {
            var tabContainers = $('.RecommendsCities > div'),
              hash = window.location.hash !== '' ? window.location.hash : '#America';

            tabContainers.hide();
            tabContainers.filter(hash).show();
            $('.Continents button a').removeClass('selected');
            $('a[href="' + hash + '"]', '.Continents').addClass('selected');
        }).trigger('hashchange');

        $('.backtotop').html("<div class=\"fixed\"><a href=\"#page-wrapper\" class=\"btn btn-success btn-lg\"><i class='fa fa-arrow-circle-up'></i></a></div>");
        goTop(400);

        // Datepicker setup
        $('#checkin').datepicker({
            startDate: '0d',
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#checkout').datepicker({
            startDate: '1d',
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });
      $('#phone').mask('999-999-9999', {placeholder: 'XXX-XXX-XXXX'});
      $('#checkin_one').datepicker({
        startDate: '0d',
        todayBtn: 'linked',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: 'yyyy-mm-dd',
      });
      $('#checkout_one').datepicker({
        startDate: '1d',
        todayBtn: 'linked',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: 'yyyy-mm-dd',
      });
      $('#checkin_two').datepicker({
        startDate: '0d',
        todayBtn: 'linked',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: 'yyyy-mm-dd',
      });
      $('#checkout_two').datepicker({
        startDate: '1d',
        todayBtn: 'linked',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: 'yyyy-mm-dd',
      });
        // Destination Autocomplete setup
        check();
        groupRequestSetTime();
        $(window).resize(function () {
            var height = $(window).height();
            height = height * 0.8;
            $('.fixed').css({'top': height});
        });

        var typingTimer;                //timer identifier
        var doneTypingInterval = 300;  //delay search time (in ms)
        var $destinationInput = $('#destinationName');

        $destinationInput.on('input', function (e) {
            $(this).unbind('focus');
            $(this).unbind('blur');
            $('input[name="scenicName"]').val('');
            $('input[name="scenicId"]').val('');

            var keyword = encodeURIComponent($(this).val().trim());

            clearTimeout(typingTimer);

            typingTimer = setTimeout(function () {
                if (keyword === "") {
                    $("div[class='autocomplete-suggestions']").remove();
                    return false;
                }

                if (dhxhr && dhxhr.readyState != 4) {
                    dhxhr.abort();
                }

                dhxhr = $.getJSON("{{url('/destination/suggestions')}}?keyword=" + keyword, function (data) {
                    setupAutocomplete(data, isCh, 'destinationName', 'destinationId');
                });
            }, doneTypingInterval);
        });

        $destinationInput.on('focus', function() {
            $(this).select();
        });

        //on keydown, clear the countdown
        $destinationInput.on('keydown', function () {
            clearTimeout(typingTimer);
        });

        // Hotel name filter setup
        $('#hotelName').on('input', function (e) {
            @if($language == 0)
            $('#hotelName_en').attr("value", "");
            $('#hotelName_zh').attr("value", "");
            $('#code').attr("value", "");
            $('#hotelId').attr("value", "");
            @endif
            @if($language == 1)
            $('#hotelName_en').attr("value", "");
            $('#hotelId').attr("value", "");
            $('#code').attr("value", "");
                    @endif

            var keyword = $(this).val();

            if (keyword === "") {
                setupHotelAutoComplete([]);
                return false;
            }

            var desId = $("#destinationId").val();
            if (desId === "") {
                desId = 0;
            }
            desId = 0;
            if (kxhr && kxhr.readyState != 4) {
                kxhr.abort();
            }

            kxhr = $.getJSON("{{url('/destination/suggestions')}}/" + desId + "/" + keyword + "/10", function (result) {
                setupHotelAutoComplete(result);
            });

        });

        $('.RecommendsCities input').mouseover(function () {
            $(this).css("text-decoration", "underline");
        });

        $('.RecommendsCities input').mouseout(function () {
            $(this).css("text-decoration", "none");
        });
    });

</script>