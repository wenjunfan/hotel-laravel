<script type="text/javascript">
    jQuery(function($) {
        $("#orderStatus").val("{{$orderStatus}}");

        $('#from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#cfrom').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#cto').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('.dataTables-orders').DataTable({
            "order": [[1, "desc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: '{{ trans('orders.CheckOrder') }}'},
                {extend: 'pdf', title: '{{ trans('orders.CheckOrder') }}'},
                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                          .addClass('compact')
                          .css('font-size', 'inherit');
                    }
                }
            ]
        });

    });

    var funcs = [
        function one() {

        },
        function two() {

        },
    ];

    $('button').data('counter', 0) // begin counter at zero
      .click(function() {
          var counter = $(this).data('counter');
          funcs[ counter ]();
          $(this).data('counter', counter < funcs.length-1 ? ++counter : 0);
      });

    $('[name="roEdit"]').on('click', function() {
        var prev = $(this).prev('textarea'),
          ro   = prev.prop('readonly');
        prev.prop('readonly', !ro).focus();
    });

    function localize(t) {
        var h = t.replace("T", " ");
        h = h.substring(0, 19);

        var hotel = new Date(h);
        var local = new Date(t);

        return h;
    }

    function today(t) {
        var h = t.replace("T", " ");
        var hotel = new Date(h);
        var today = new Date();

        return hotel > today;
    }

    function convertCancelPolicies(reference, payment_type,room, policies, currencyCode) {//Arthur 050517
        if (policies == "" || policies == null) {
            return;
        }

        try {
            var html = $("#" + reference).html();
            var html1 = $("#o" + reference).html();
            var obj = jQuery.parseJSON(policies);

            $.each(obj, function(i, o) {
                var amount = o.amount;
                var end = o.end;

                if (html != "") {
                    html = "";
                }
                if(amount == 0) {
                    var cancel = new Date(end).getTime();
                    var now = new Date().getTime();
                    if(cancel>now){
                        html += localize(end) + " {{ trans('orders.free-can') }}<br>";
                    } else{
                        html += " {{ trans('orders.non-ref') }}";
                    }
                }
                else {
                    html += localize(end) + " {{ trans('orders.charge') }}" + currencyCode + amount + "<br>";
                }
                $("#" + reference).html(html);
                var cancel = new Date(end).getTime();
                var now = new Date().getTime();
                /*if(cancel>now)
                {
                     html1="	<span id=\"cancel_"+ reference +"\"><a onclick=\"cancelOrder('"+ reference +"', \'"+payment_type+"\')\" class=\"\" type=\"button\">{{ trans('orders.Cancel') }}</a></span>"//Arthur 050517
				 }*/


                $("#o" + reference).html(html1);
            });
        }
        catch(err) {
        }
    }

    function search () {
        var from = $("#from").val();
        var to = $("#to").val();
        var cfrom = $("#cfrom").val();
        var cto = $("#cto").val();
        var keywords = $("#keywords").val();
        var orderStatus = $("#orderStatus").val();
        self.location = "{{url('/orders')}}"
          + '?from=' + from
          + '&to=' + to
          + '&cfrom=' + cfrom
          + '&cto=' + cto
          + '&keywords=' + keywords
          + '&orderStatus=' + orderStatus
        ;
    }

    function cancelOrder(id, payment_type) {
        var text = "";
        if (payment_type != "ALIPAY") {
            {{--var text = "{{ trans('orders.alipaycharge') }}" //没有了吧？和sofia确认下，以后要不要收取--}}
        }
        swal({
            title: "{{ trans('orders.surecancel') }}",
            text: text + "{{ trans('orders.checkb4can') }}",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "{{ trans('orders.cancelyes') }}",
            confirmButtonColor: "#007FF8",
            confirmButtonText: "{{ trans('orders.cancelno') }}",
            closeOnConfirm: true
        }, function(isConfirm) {
            if(!isConfirm){
                FullscreenSpinner.create();
                $.post("{{url('/order/cancel')}}", { '_token': $("input[name='_token']").val(), 'id': id }, function(data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal({
                            title: "{{ trans('orders.alreadyc') }}",
                            text: data.message,
                            type: "success"
                        }, function() {
                            location.reload();
                        });
                    }
                    else {
                        swal("{{ trans('orders.failedc') }}", "" , "error");
                    }
                });
            }
        });
    }

    function checkOrder(id){
        FullscreenSpinner.create();
        $.post("{{url('/order/check')}}", { '_token': $("input[name='_token']").val(), 'id': id }, function(data){
            FullscreenSpinner.destroy();
            if (data.success) {
                location.reload();
            } else {
                swal("Up to date", data.message, "success");
            }
        });
    }

    function fixOrder1(id) {
        $("#roEditBtn"+id).toggleClass("hidden");
        $("#roSaveBtn"+id).toggleClass("hidden");
    }

    function fixOrder(id) {
        var remark = document.getElementById('remark'+id).value;
        FullscreenSpinner.create();

        $.post("{{url('/business/order/update')}}", { '_token': $("input[name='_token']").val(), 'id': id , 'remark':remark }, function(data) {
            FullscreenSpinner.destroy();
            if (data.success) {
                swal("{{ trans('orders.Updates') }}", data.message, "success");
                //location.reload();
                document.getElementById('remark'+id).readOnly=true;
                $("#roEditBtn"+id).toggleClass("hidden");
                $("#roSaveBtn"+id).toggleClass("hidden");
            }
            else {
                swal("{{ trans('orders.Updatef') }}", data.message, "success");
            }
        });
    }

    function updateOrderStatus(orderRef){  //几乎没有上游这么返回了，不让客户自己刷新， 功能作废待定
        swal({
            title: "Tips",
            text: "Order processing, thanks for waiting.",
            type: "warning"
        }, function() {
            location.reload();
        });
    }
</script>