<script type="text/javascript">
    jQuery(function ($) {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
    });

    function changeAvatar(avatar) {
        $.ajax({
            method: "POST",
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
            url: "{{url('/avatar')}}",
            data: {'avatar': avatar},
            success: function (data) {
                if (data.status == "success") {
                    swal({
                        title: "用户头像已经更新",
                        text: "",
                        type: 'success'
                    }, function () {
                        self.location = "{{ url('profile') }}";
                    });
                }
                else {
                    swal(data.message);
                }
            },
            error: function (xhr, e) {
                swal(xhr.status + " " + e);
            },
        });
    }

    @if (session('message'))
        swal({
            title: "",
            text: "{{ session('message') }}",
            type: '{{ session('success') }}' == true ? 'success' : 'error'
        });
    @endif

</script>