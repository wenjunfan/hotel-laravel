@extends( !$is_b  ?  'layouts.master' : 'layouts.master-b' )

@section('title', 'Credit Card Pay')

@section('css')
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">

@endsection

@section('content')
    <div style="30PX; padding-right:30PX;">
        <div class="ibox float-e-margins">
            {!! csrf_field() !!}
            <div class="ibox-noborder" style="padding-top:30px; color:#484848; font-size:20px;text-align:center;">
                @lang('paypal-ex.title')
            </div>
            <div>

                <div class="ibox-content">
                    <form class="form-horizontal" style="margin-top:20px;" role="form" id="creditform">
                        {!! csrf_field() !!}
                        <input type="hidden" name="Reorderid" value="{{$Reorderid}}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="number">@lang('paypal-ex.cn')</label>

                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="number" value="" placeholder="@lang('paypal-ex.cn')"/> <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="cardType">@lang('paypal-ex.title')</label>

                            <div class="col-sm-9 form-inline">
                                <select id="cardType" name="cardType" class="form-control">
                                    <option value="Visa" }>Visa</option>
                                    <option value="MasterCard">MasterCard</option>
                                    <option value="Discover">Discover</option>
                                    <option value="Amex">American Express</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="expirationDate">@lang('paypal-ex.ed')</label>

                            <div class="col-sm-9 form-inline">
                                <select id="month" name="month" class="form-control">
                                    <option value="1">1 @lang('paypal-ex.month1')</option>
                                    <option value="2">2 @lang('paypal-ex.month1')</option>
                                    <option value="3">3 @lang('paypal-ex.month1')</option>
                                    <option value="4">4 @lang('paypal-ex.month1')</option>
                                    <option value="5">5 @lang('paypal-ex.month1')</option>
                                    <option value="6">6 @lang('paypal-ex.month1')</option>
                                    <option value="7">7 @lang('paypal-ex.month1')</option>
                                    <option value="8">8 @lang('paypal-ex.month1')</option>
                                    <option value="9">9 @lang('paypal-ex.month1')</option>
                                    <option value="10">10 @lang('paypal-ex.month1')</option>
                                    <option value="11">11 @lang('paypal-ex.month1')</option>
                                    <option value="12">12 @lang('paypal-ex.month1')</option>
                                </select> <select id="year" name="year" class="form-control">
                                    <option value="2020">2020 @lang('paypal-ex.year1')</option>
                                    <option value="2021">2021 @lang('paypal-ex.year1')</option>
                                    <option value="2022">2022 @lang('paypal-ex.year1')</option>
                                    <option value="2023">2023 @lang('paypal-ex.year1')</option>
                                    <option value="2024">2024 @lang('paypal-ex.year1')</option>
                                    <option value="2025">2025 @lang('paypal-ex.year1')</option>
                                    <option value="2026">2026 @lang('paypal-ex.year1')</option>
                                    <option value="2027">2027 @lang('paypal-ex.year1')</option>
                                    <option value="2028">2028 @lang('paypal-ex.year1')</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="CVC">@lang('paypal-ex.cvc')</label>

                            <div class="col-sm-9 form-inline">
                                <input type="text" class="form-control" name="CVC" value="" placeholder="CVC"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="firstName">@lang('paypal-ex.name')</label>

                            <div class="col-sm-9 form-inline">
                                <input type="text" class="form-control" name="firstName" value="" placeholder="@lang('paypal-ex.lname')"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="street">@lang('paypal-ex.add')</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="street" value="" placeholder="@lang('paypal-ex.st')"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="street2">@lang('paypal-ex.add2')</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="street2" value="" placeholder="@lang('paypal-ex.apt')"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="city">@lang('paypal-ex.city')</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="city" value="" placeholder="@lang('paypal-ex.cityname')"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="state">@lang('paypal-ex.county')</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="state" value="" placeholder="@lang('paypal-ex.countyname')"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="zip">@lang('paypal-ex.zip')</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="zip" value="" placeholder="@lang('paypal-ex.zip')"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="countryCode">@lang('paypal-ex.country')</label>
                            <div class="col-sm-9 form-inline">
                                <select id="countryCode" name="countryCode" class="form-control">
                                    <option value="US" selected>@lang('paypal-ex.USA')</option>
                                    <option value="CH">@lang('paypal-ex.CN')</option>
                                </select>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="button" class="btn btn-primary btn-block pull-right ladda-button ladda-button-book" data-style="expand-right" onclick="NoCreditcardsPay(this);">@lang('paypal-ex.pmt')</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/plugins/validate/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/ladda/spin.min.js') }}"></script>
    <script src="{{ asset('js/plugins/ladda/ladda.min.js') }}"></script>
    <script src="{{ asset('js/plugins/ladda/ladda.jquery.min.js') }}"></script>

    <script>
        jQuery(function ($) {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });

            $("#creditform").validate({
                rules: {
                    number: {
                        required: true,
                        minlength: 16,
                        maxlength: 16,
                        number: true
                    },
                    CVC: {
                        required: true,
                        minlength: 3,
                        maxlength: 4,
                        number: true
                    },
                    firstName: {
                        required: true
                    },
                    lastName: {
                        required: true
                    },
                    street: {
                        required: true,
                        minlength: 6
                    },
                    city: {
                        required: true,
                        minlength: 2
                    },
                    state: {
                        required: true,
                        minlength: 2
                    }
                }
            });

        });

        function NoCreditcardsPay (obj) {
            var r = $("#creditform").valid();
            if (!r) {
                swal("{{ trans('paypal-ex.sa') }}");
                return;
            }
            var l = Ladda.create(obj);
            l.start();

            var url = "{{url('/business/extra-pay/credit-card')}}";
            $.post(url, $("#creditform").serialize(), function (data) {

                l.stop();
                if (data.success) {
                    swal("付款成功", data.message, "success");
                } else {
                    swal("{{ trans('paypal-ex.failed') }}", data.message, "error");
                }
            });
        }
    </script>
@endsection
