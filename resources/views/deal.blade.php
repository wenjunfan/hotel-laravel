@extends($is_b ? 'layouts.master-b' : 'layouts.master')

@section('title', 'Hotel Deals List')

@section('css')
    <link rel="stylesheet" href="{{ asset('/assets/css/pc/deal.css') }}">
@endsection

@section('content')
    @if(!$is_b && !$is_aa)
        @include('components.nav-bar')
    @endif
    <div id="dealsApp" v-cloak>
        <div class="top-search-bar">
            <div class="container">
                <form id="search-form" action="#" class="row">
                    <div class="display-block" v-show="!isEditing" @click="isEditing = !isEditing">
                        <div class="destination-name">
                            <label>@lang('deal.search_destination')</label>
                            <p class="param-data">@{{ destination.name }}</p>
                        </div>
                        <div class="dates">
                            <label>@lang('deal.search_dates')</label>
                            <p class="param-data">@{{ searchData.checkin }} - @{{ searchData.checkout }}</p>
                        </div>
                        <div class="rooms">
                            <label>@lang('deal.search_rooms')</label>
                            <p class="param-data">
                                @{{ searchData.roomCount }}
                                <span v-if="langStr === 'cn'">
                                    房间
                                </span>
                                <span v-else>
                                    <span v-if="searchData.roomCount > 1">Rooms</span><span v-else>Room</span>
                                </span>,
                                @{{ searchData.adultCount1 }}
                                <span v-if="langStr === 'cn'">
                                    成人
                                </span>
                                <span v-else>
                                    <span v-if="searchData.adultCount1 > 1">Adults</span><span v-else>Adult</span>
                                </span>,
                                @{{ searchData.childCount1 }}
                                <span v-if="langStr === 'cn'">
                                    小孩
                                </span>
                                <span v-else>
                                    <span v-if="searchData.childCount1 > 1">Children</span><span v-else>Child</span>
                                </span>
                            </p>
                        </div>
                    </div>
                    <div class="search-block" v-show="isEditing">
                        <div class="destination-name">
                            <label for="desName">@lang('deal.search_destination')</label>
                            <input type="hidden" name="desId" v-model="destination.id">
                            <auto-complete
                                    :destination-name="destination.name"
                                    :destination-id="destination.id"
                                    :scenic-name="destination.scenicName"
                                    :scenic-id="destination.scenicId"
                                    :input-placeholder="langStr === 'cn' ? '请输入目的地' : 'Where to go'"
                                    :input-class="['param-data', 'destination-input']"
                                    @update-destination="updateDestination">
                            </auto-complete>
                        </div>
                        <div class="checkin-date">
                            <label for="checkin">@lang('deal.search_checkin')</label>
                            <input type="text" name="checkin" id="checkin" class="param-data" placeholder="checkin date" autocomplete="off" v-model="searchData.checkin">
                        </div>
                        <div class="checkout-date">
                            <label for="checkout">@lang('deal.search_checkout')</label>
                            <input type="text" name="checkout" id="checkout" class="param-data" placeholder="checkout date" autocomplete="off" v-model="searchData.checkout">
                        </div>
                        <div class="rooms-wrapper">
                            <label for="selects">@lang('deal.search_rooms')</label>
                            <div class="rooms-display">
                                <p @click="editRooms = !editRooms">
                                    @{{ searchData.roomCount }}
                                    <span v-if="langStr === 'cn'">
                                        房间
                                    </span>
                                    <span v-else>
                                        <span v-if="searchData.roomCount > 1">Rooms</span><span v-else>Room</span>
                                    </span>,
                                    @{{ searchData.adultCount1 }}
                                    <span v-if="langStr === 'cn'">
                                        成人
                                    </span>
                                    <span v-else>
                                        <span v-if="searchData.adultCount1 > 1">Adults</span><span v-else>Adult</span>
                                    </span>,
                                    @{{ searchData.childCount1 }}
                                    <span v-if="langStr === 'cn'">
                                        小孩
                                    </span>
                                    <span v-else>
                                        <span v-if="searchData.childCount1 > 1">Children</span><span v-else>Child</span>
                                    </span>
                                </p>
                                <div class="rooms-dropdown" v-show="editRooms">
                                    <div>
                                        <label for="roomCount">@lang('deal.search_room_count'):</label>
                                        <button type="button" class="control-minus" @click="searchData.roomCount--;return false;" :disabled="searchData.roomCount === 1">-</button>
                                        <input type="text" name="roomCount" placeholder="room number" v-model="searchData.roomCount">
                                        <button type="button" class="control-plus" @click="searchData.roomCount++;return false;" :disabled="searchData.roomCount === 8">+</button>
                                    </div>
                                    <div>
                                        <label for="adultCount1">@lang('deal.search_adult_count'):</label>
                                        <button type="button" class="control-minus" @click="searchData.adultCount1--;return false;" :disabled="searchData.adultCount1 === 1">-</button>
                                        <input type="text" name="adultCount1" placeholder="adult number" v-model="searchData.adultCount1">
                                        <button type="button" class="control-plus" @click="searchData.adultCount1++;return false;" :disabled="searchData.adultCount1 === 4">+</button>
                                    </div>
                                    <div>
                                        <label for="childCount1">@lang('deal.search_child_count'):</label>
                                        <button type="button" class="control-minus" @click="searchData.childCount1--;return false;" :disabled="searchData.childCount1 === 0">-</button>
                                        <input type="text" name="childCount1" placeholder="child number" v-model="searchData.childCount1">
                                        <button type="button" class="control-plus" @click="searchData.childCount1++;return false;" :disabled="searchData.childCount1 === 4">+</button>
                                    </div>
                                    <div class="child-age-wrapper" id="childAgeWrapper" v-show="searchData.childCount1 > 0">
                                        <label class="number-label child-age-label">@lang('home.childAge')</label>
                                        <div id="childAgeContent">
                                            <select name="childAge[]" class="child-age-select" v-for="(childAge, index) in childAges"
                                                    @input="changeAge(childAge, index, $event)" :value="parseInt(childAge)">
                                                <option value="0"><1 @lang('home.yearsOld')</option>
                                                <option v-for="n in 17" :value="n">@{{ n }} @lang('home.yearsOld')</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="search-btn" v-show="isEditing" @click="submitSearch">
                        @lang('deal.search_submit')
                    </button>
                    <button type="button" :class="['change-btn', isEditing ? 'editing' : '']" @click="isEditing = !isEditing">
                        <span v-if="isEditing">@lang('deal.search_cancel')</span>
                        <span v-else>@lang('deal.search_change')</span>
                    </button>
                </form>
            </div>
        </div>

        @if($coupon->hide_image !== 1)
            <div class="coupon-banner container">
                <div class="row">
                    <div class="coupon-banner-bg"
                            style="background-image: url('{{ 'https://s3-us-west-1.amazonaws.com/usitrip/' . (config('app.env') === 'local' ? 'coupon_img_test/' : 'coupon_img/') . 'coupon_'.$coupon->id.'_pc.png' }}')"
                            data-target="#policyModal">
                    </div>
                    <div class="coupon-banner-text">
                        <h1 class="coupon-title">@if($language == 0) {{ $coupon->title_zh }} @else {{ $coupon->title }} @endif</h1>
                        <p class="coupon-name">@if($language == 0) {{ $coupon->name_zh }} @else {{ $coupon->name }} @endif</p>
                        <p class="coupon-code">
                            @if($language == 0)
                                使用折扣码 <span>{{ $coupon->code }}</span>
                            @else
                                With coupon code <span>{{ $coupon->code }}</span>
                            @endif
                        </p>
                        <p class="coupon-details">
                            @if($language == 0) 点击查看 <a href="">详情</a> @else Click to <a href="">view details</a> @endif
                        </p>
                    </div>
                </div>
            </div>
        @endif

        <div class="container">
            <div class="row">
                @if($coupon->hide_image !== 1)
                    <div class="coupon-flow">
                        <h2 class="coupon-flow-title">
                            @if($language === 1)
                                <span>Use the coupon code {{ $coupon->code }} and book with even better prices!</span>
                            @else
                                <span>输入优惠码 {{ $coupon->code }} 获得酒店优惠</span>
                            @endif
                        </h2>
                        <div class="coupon-step-wrapper">
                            <div class="coupon-step-1 coupon-step">
                                <div class="icon-wrapper">
                                    <i class="icon-list icon"></i>
                                </div>
                                <p class="coupon-step-desc">@lang('deal.coupon_step1')</p>
                            </div>
                            <div class="icon-arrow-wrapper">
                                <i class="icon-arrow"></i>
                            </div>
                            <div class="coupon-step-2 coupon-step">
                                <div class="icon-wrapper">
                                    <i class="icon-coupon icon"></i>
                                </div>
                                <p class="coupon-step-desc">@lang('deal.coupon_step2')</p>
                            </div>
                            <div class="icon-arrow-wrapper">
                                <i class="icon-arrow"></i>
                            </div>
                            <div class="coupon-step-3 coupon-step">
                                <div class="icon-wrapper">
                                    <i class="icon-bed icon"></i>
                                </div>
                                <p class="coupon-step-desc">@lang('deal.coupon_step3')</p>
                            </div>
                        </div>
                    </div>
                @endif

                @if(count($hotelGroups) > 0)
                    @php
                    $params = 'checkin=' . $checkin . '&checkout=' . $checkout . '&roomCount=1&adultCount1=2&childCount1=0';
                    @endphp
                    <div class="hotels-wrapper">
                        @if(count($hotelGroups) > 1)
                        <div class="destination-block">
                            <h2>@lang('deal.popular_destination')</h2>
                            <div class="destination-wrapper">
                                @foreach($hotelGroups as $hotelGroup)
                                    <a href="#" data-target="#{{ str_replace(' ', '-', $hotelGroup['destination']['city']) }}" class="destination-toggler">{{ $hotelGroup['destination']['city'] }}</a>
                                @endforeach
                            </div>
                        </div>
                        @endif

                        @foreach($hotelGroups as $hotelGroup)
                            <div id="{{ str_replace(' ', '-', $hotelGroup['destination']['city']) }}" class="hotel-group row">
                                <div class="hotel-group-header col-xs-12 col-md-12">
                                    <h2>{{ $hotelGroup['destination']['city'] }}</h2>
                                    <a href="{{ url('/list') . '/D' . $hotelGroup['destination']['desId'] . '.html?' . $params }}" target="_blank" class="pull-right">@lang('deal.more_hotels')</a>
                                </div>

                                @foreach($hotelGroup['hotels'] as $index => $hotel)
                                    <div class="hotel-item col-xs-12 col-md-4" id="hotelItem{{ $hotel['hotelId'] }}">
                                        <img src="{{ config('constants.image_host') . (stripos($hotel['thumbnail'], '_Image') === false ? 'Hotel_Image/' : '') . $hotel['thumbnail'] }}" alt="{{ $hotel['name'] }}" height="220" width="100%">
                                        <span id="discountLabel{{ $hotel['hotelId'] }}" class="discount-label"></span>
                                        <div class="hotel-item-details">
                                            <h3 class="hotel-name">{{ $hotel['name'] }} @if($language === 0 && !empty($hotel['name_zh']))({{ $hotel['name_zh'] }})@endif</h3>
                                            <div class="hotel-rating-wrapper">
                                                <i class="hotel-rating hotel-rating-{{ $hotel['rating'] }}"></i>
                                            </div>
                                            <div class="hotel-price">
                                                ...
                                            </div>
                                            <a class="book-btn" href="{{ url('/hotel') . '/' . $hotel['hotelId'] . '.html?' . $params }}" target="_blank">@lang('deal.book_now')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                @endif

                <div class="policy-modal modal fade" id="policyModal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header text-center">
                                <h2>@lang('deal.policy_title')</h2>
                                <i class="modal-close" @click="closePolicy()"></i>
                            </div>
                            <div class="modal-body">
                                @lang('deal.policy_content', ['start' => $coupon->checkin_start_date, 'end' => $coupon->checkin_end_date])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var coupon = {!! $coupon !!},
            couponId = coupon.id,
            appUrl = '{{ config('app.url') }}',
            checkin = '{{ $checkin }}',
            checkout = '{{ $checkout }}';
    </script>

    <script type="text/javascript" src="{{ url('/assets/js/pc/all.deal.js') }}"></script>
@endsection