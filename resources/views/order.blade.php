@extends( !$is_b  ?  'layouts.admin' : 'layouts.master-b' )
<?php
/**
 * @var $order
 * @var $numbers
 **/
$hotel = $order['hotel'];
?>
@section('title', 'Order Detail')
@section('css')
<link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">

<meta charset="utf-8">
<style>
    #invoice{
        margin: 10px 10%;
        display:block;
    }
    .col-sm-10 p{
        font-weight:400;
        font-size:14px;
    }
    @media print {
        nav.navbar-default.navbar-static-side, button.btn.btn-success{
            display:none;
        }
        #invoice{
            margin: 0;
            display:block;
        }
        .animated.fadeInRight {
            margin-left: -70px;
        }
        .col-lg-8 {
            width: 66.66666667%;
            float:left;
        }
        .col-lg-4 {
            width: 33.33333333%;
            float:left;
        }
        .col-sm-6 {
            width: 50%;
            float:left;
        }
        .col-md-12 {
            width: 100%;
            float: left;
        }
        .col-sm-10 {
            width: 83.33333333%;
            float:left;
        }
        .invoice-head {
            display: block;
        }
    }
</style>
@endsection

@section('content')
<button class="btn btn-success" onclick="window.print();">print</button>
<div id="invoice">
{!! csrf_field() !!}
	<div class="invoice-head">
		<div class="col-sm-6">
			@if($order->partnerId == 11087)
				@if($language == 2)
					<img class="col-mg-6" height="85" src="/img/general/logo/logo-usitour-tw.png" alt="" style="margin-bottom:30px;"/>
				@else
					@if(isset($order->http_host) && strrpos($order->http_host, 'usitour') !== false)
						<img class="col-mg-6" height="85" src="//s3-eu-west-1.amazonaws.com/tpd/logos/58b75d500000ff00059da083/0x0.png" alt="logo" style="margin-bottom:30px;"/>
					@else
						<img class="col-mg-6" height="85" src="{{asset('img/general/logo/logo-usitrip.png')}}" alt="logo" style="margin-bottom:30px;"/>
					@endif
				@endif
			@elseif($order->partnerId == 11676)
				<img class="col-mg-6" height="85" src="{{asset('/img/partnerImgs/aaLogo.png')}}" alt="logo"
                     style="margin-bottom:30px;"/>
			@else
				<img class="col-mg-6" height="85" src="{{asset('img/business/new-logo.png')}}" alt="logo" style="margin-bottom:30px;"/>
			@endif
			<br>
			<h4>Bill to:</h4>
			@if($order->partnerId == 11087)
                {{$order->bookerFirstName. ' ' .$order->bookerLastName}}
			@else
			    {{$order->partner->company_name}}<br/>{{$order->partner->company_address}}
			@endif
		</div>
		<div class="col-sm-6 text-right">
			<h1 style="font-size:50px;margin-top:40px; margin-bottom:30px; font-weight:500;">INVOICE</h1>
			<h2>Invoice No.:  {{ $order->id }}  &nbsp;
				@if($order->status == "CONFIRMED")
					<span class="label label-info">CONFIRMED</span>
				@elseif($order->status == "CANCELLED")
					@if($order->payment_status == 'PENDING')
					<span class="label label-warning">PENDING REFUND</span>
					@elseif($order->payment_status == 'REFUND')
						@if($order->payment_type == 'PO')
						<span class="label label-warning">CANCELLED</span>
						@else
						<span class="label label-warning">REFUNDED</span>
						@endif
					@else
						@if($order->payment_type == 'PO')
						<span class="label label-warning">CANCELLED</span>
						@else
						<span class="label label-warning">REFUNDED</span>
						@endif
					@endif
				@elseif($order->status == 'ON REQUEST')
					<span class="label label-success">IN PROCESSING</span>
				@elseif($order->status == 'FAILED')
					<span class="label label-danger">UNKNOWN</span>
				@else
					<span class="label label-danger">UNKNOWN</span>
				@endif
			</h2>
			<h2>
				Due Date：
					<?php
						$room= $order->rooms->first();
						$orderdate=strtotime($order->created_at);
						$duedate = strtotime("+7 day", $orderdate);
						$canceldate= json_decode($room->cancelPolicies, true)[0]['end'];
						$end = substr($canceldate,0,10);
						$enddate = strtotime($end);

						if($enddate<$duedate && $enddate>$orderdate)
						{
							echo date('M d, Y', $enddate);
						}elseif($enddate>$duedate){
							echo date('M d, Y', $duedate);
						}else{
							echo date('M d, Y', $orderdate);
						}
					?>
			</h2>
		</div>
	</div>
	<div class="col-md-12" style="margin-top:30px;">
		<div class="panel panel-default" >
			<div class="panel-heading" style="height: 40px; background-color:rgb(229, 241, 255)">
			<p class="col-sm-10">Description</p>
			<p class="col-sm-2">Amount</p>
		</div>
		<div class="panel-body">
		<?php
			$i = 1;
			$room= $order->rooms->first();
			{
	    ?>
		<div class="col-sm-10">
			<p >Hotel Name：{{$hotel['name']}}</p>
			<p style="font-weight:400; font-size:14px;">Hotel Address：{{ucwords(strtolower(str_replace(",","",$hotel['address'])))}}{{empty($hotel->city)? '': ', ' .ucwords(strtolower($hotel->city))}}{{empty($hotel->zipcode)? '': ', ' .$hotel->zipcode}}</p>
			<p style="font-weight:400; font-size:14px;">{{isset($hotel['phone']) ? 'Hotel Phone #：' . $hotel['phone'] : ''}} </p>
				<?php
				$j = 1;
				foreach($room['paxes'] as $p){
				?>
				<p style="font-weight:400; font-size:14px;">Customer Name {{$j++}}：{{$p['firstName']}} {{$p['lastName']}}</p>
				<?php
				}
				?>
			<p>Check-in：{{ $order->checkinDate }} </p>
			<p>Check-out：{{ $order->checkoutDate }} </p>
			<p>Room Type: {{$room['name']}} </p>
			<p>Board Type: {{$room['boardName']}} </p>
			<p>Rooms: {{$numbers}} </p>
			<p>Nights: {{ $order->day_number }} </p>
		</div>
		<div class="col-sm-2">
			@if($order->payment_type == "ALIPAY")
				@if($order->infact_cost_cn == null || $order->infact_cost_cn == "")
					{{ $order->currencyCode }}
					<?php $totalPrice = $order->totalPrice;?>
				@else
					CNY
					<?php $totalPrice = $order->infact_cost_cn;?>
				@endif
			@else
				@if($order->infact_cost == null || $order->infact_cost == "")
					{{ $order->currencyCode }}
					<?php $totalPrice = $order->totalPrice;?>
				@else
					USD
                    <?php $totalPrice = $order->infact_cost;?>
				@endif
			@endif
            <?php
                $totalDays = $order->day_number;
                $totalRooms = $numbers;
                $num = $totalDays ? round($totalPrice/$totalDays/$totalRooms,2) : 0;
                echo $num .'/day/night'
            ?>
		</div>
		</div>
		<?php
			}
		?>
	</div>
		<div class="panel panel-default col-lg-8" style="padding-left:0;padding-right:0;">
			<div class="panel-heading" style="height: 40px; background-color:rgb(229, 241, 255)">&nbsp;&nbsp;&nbsp;&nbsp;Other Notices</div>
					<div class="panel-body col-sm-8" style="margin-left:15px;">
					<p>1. Please make check payable to <strong>Unintedstars International Ltd </strong></p>
					<p>2. Please mail check to 17870 Castleton St. Suite 358-388, City of Industry, CA, 91748. USA </p>
					<p>3. If you have any questions about this invoice, please contact accounting@usitrip.com</p>
			</div>
		</div>
		<div class="col-lg-4" style="text-align:right; margin-top:10px; font-size:20px;" >
		<p>Subtotal:
		@if($order->payment_type == "ALIPAY")
			 @if($order->infact_cost_cn == null || $order->infact_cost_cn == "")
				{{ $order->currencyCode }} {{ $order->totalPrice }}
			 @else
					CNY {{ $order->infact_cost_cn }}
			 @endif
		@else
			 @if($order->infact_cost == null || $order->infact_cost == "")
				{{ $order->currencyCode }} {{ $order->totalPrice }}
			 @else
				USD {{ $order->infact_cost }}
			 @endif
		@endif
		</p>
		@if($order->partnerId == 10743)
		<p>Others(Tax/GST): USD 0</p>
		@endif
		<p>Total Due:
		@if($order->status == "CONFIRMED" && $order->payment_type == "PO" && $order->isChecked == '')
			@if($order->payment_type == "ALIPAY")
				 @if($order->infact_cost_cn == null || $order->infact_cost_cn == "")
					{{ $order->currencyCode }} {{ $order->totalPrice }}
				 @else
                    CNY {{ $order->infact_cost_cn }}
				 @endif
			@else
				 @if($order->infact_cost == null || $order->infact_cost == "")
					{{ $order->currencyCode }} {{ $order->totalPrice }}
				 @else
					USD {{ $order->infact_cost }}
				 @endif
			@endif
		</p>
		@elseif($order->payment_type == "CC")
			Paid with Credit Card
                <?php
				$payment= $order->payment->first();
				{
				?>
                    <p style="font-size:13px;">
                        @if(isset($payment->cc_number))(ending in *{{substr($payment->cc_number, -4)}})@endif
                    </p>
                <?php
				}
                ?>
		@else
			0
		@endif
</div>
</div>
<div class="col-md-12" style="text-align:center; margin-top:10px; font-size:16px;" >
<P style="text-align: center; padding:0 50px; font-size:15px;">Cost of remitting payment to us must be borne by you. We do not accept any deduction from invoices by you for bank charges and/or money transfer fees.</P>
<P style="text-align: center; padding:0 50px;font-size:15px; "><strong>Please note:</strong> this payment is for a pre-paid reservation that we are responsible to charge and collect. No additional payment should be made in relation to this reservation (other than extra services that you may request at the desk or specific local taxes or fees that may be applicable and are required to be charged locally)</P>
<P style="text-align: center; font-size:15px; ">Thank you for your business !</P>
</div>
</div>
@endsection

@section('scripts')
@endsection



