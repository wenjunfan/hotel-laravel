@extends( !$is_b  ?  'layouts.master' : 'layouts.master-b' )

@section('title', '账户流水')

@section('css')
<link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row animated fadeInRight">
    <div class="col-lg-6 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                查询流水
            </div>
            <div>
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" id="search-form" name="search-form">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="from">起始日期</label>

                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="from" name="from" type="text" class="form-control" value="{{$from}}" placeholder="年-月-日">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="to">结束日期</label>

                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="to" name="to" type="text" class="form-control" value="{{$to}}" placeholder="年-月-日">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-primary btn-block" type="button" onclick="search()"><i class="fa fa-search"></i> 查询</button>
                            </div>
                            <!--div class="col-sm-6">
                                <button class="btn btn-warning btn-block" type="reset">重置</button>
                            </div-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                流水列表<!--，当前账户余额 <span class="label label-danger">${{ $balance }}</span-->
            </div>
            <div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-statements" >
                        <thead>
                        <tr>
                            <th>订单号</th>
                            <th>订单金额</th>
                            <th>备注</th>
                            <th>流水时间</th>
                        </tr>
                        </thead>
                        <tbody>
                    @foreach($statements as $statement)
                        <tr class="gradeX">
                            <td>
                               {{$statement->orderId == -1 ? '' : $statement->orderId}}
                            </td>
                            <td>
                               {{$statement->currencyCode . ' ' . $statement->netPrice}}
                            </td>
                            <td>
                               {{$statement->description}}
                            </td>
                            <td>
                               {{$statement->created_at}}
                            </td>
                        </tr>
                    @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<script type="text/javascript">
    jQuery(function($) {
        $('#from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('.dataTables-statements').DataTable({
            "order": [[3, "desc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: '账户流水'},
                {extend: 'pdf', title: '账户流水'},
                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    });

    function search() {
        var from = $("#from").val();
        var to = $("#to").val();

        if (from != "" && to != "") {
            self.location = "{{url('/statements/from')}}/" + from + "/to/" + to;
        }
        else if (from != "") {
            self.location = "{{url('/statements/from')}}/" + from;
        }
        else if (to != "") {
            self.location = "{{url('/statements/to')}}/" + to;
        }
        else {
            self.location = "{{url('/statements')}}";
        }
    }
</script>
@endsection
