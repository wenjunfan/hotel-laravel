@extends( !$is_b  ?  'layouts.master' : 'layouts.master-b' )

<?php
$totalPrice = 0;
$room = $rooms[0];
?>

{{--  sigle page title   --}}
@section('title')
    @if	($language == 0)
        {{ $hotel->name . '预订_' . $hotel->name .$hotel->name_zh. '_价格_房型_设施_环境_地址查询-走四方旅游网' }}
    @elseif($language == 2)
        {{ $hotel->name . '預定_' . $hotel->name. '_價格_房型_設施_環境_地址查詢-走四方旅遊網' }}
    @else
        {{ $hotel->name . ' Booking_' . $hotel->name . ' Price_Room Type_Facilities_Environment_location Searching' }}
    @endif
@endsection

@section('css')
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <link rel="canonical" href="{{ config('app.url') }}/book/{{ $hotel->hotelId }}">
    {{--webpack css--}}
    <link href="{{ asset('assets/css/pc/plugins.book.css') }}" rel="stylesheet">
@endsection

@section('content')
    <style>
        /*iframe在ie有样式的冲突，只在当页为了paypal btn显示iframe*/
        iframe {
                display: block;
        }
        #hotel-rating{
            display: none;
        }
    </style>
    {{--1: top lock price and marketing--}}
    @include('includes.book.book-action-top')
    <div class="book-box">
        {{--<!--左侧 start-->--}}
        <div class="book-info-box border-box">
            <form class="form-horizontal" role="form" method="post" id="book-form">
                {!! csrf_field() !!}
                <input type="hidden" name="bookIndex" value="{{$bookIndex}}">
                <input type="hidden" name="sessionKey" value="{{$sessionKey}}">
                <input type="hidden" name="displayId" value="{{$displayId or ''}}">
                <input type="hidden" name="booker_local_time" id="booker_local_time">
                <input type="hidden" name="total_mandatory_fees" id="total_mandatory_fees">
                <input type="hidden" name="coupon_code" id="couponCode" value="">
                <div class="book-step-one">
                    <i class="usitrip_pc_all_img red-circle-icon pull-left"></i>
                    <div class="grey-line pull-left"></div>
                    <i class="usitrip_pc_all_img grey-circle-icon pull-left"></i>
                    <div class="grey-line pull-left"></div>
                    <i class="usitrip_pc_all_img grey-check-icon pull-left"></i>
                </div>
                <p class="book-explain-step">
                    <span class="step-now">{{trans('book.step_one')}}</span>
                    <span class="step-next">{{trans('book.step_two')}}</span><span class="step-final">{{trans('book.step_three')}}</span>
                </p>
                <div>
                    <p class="book-detail-title">{{trans('book.bdetail')}}:</p>
                    <p class="good-choice">{{trans('book.good_choice')}}</p>
                    {{-- 住客信息 start --}}
                @include('includes.book.book-guest-info')
                {{-- 住客信息 end --}}
                    {{-- 联系人信息 start --}}
                @include('includes.book.book-booker-info')
                {{-- 联系人信息 end --}}
                </div>
                {{-- 付款方式start --}}
                @include('includes.book.book-payment-method')

            {{-- 付款方式end --}}
            </form>
        </div>
        {{--<!--左侧 end-->--}}
        @include('includes.book.use-term')
        @include('components.processing-modal')
        <div id="qr_modal" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content-citcon">
                    <div class="modal-header">
                        <button type="button" class="close" id="weChatCloseBtn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        @if($language == 0)
                            <h1 class="modal-title">请使用 <strong id="paymentType"></strong> 扫描</h1>
                        @else
                            <h1 class="modal-title">Please scan with your <span id="paymentType"></span></h1>
                        @endif
                    </div>
                    <div class="modal-body">
                        <h3>
                            @if($is_aa)
                                supervacation.net
                            @else
                                Unitedstars International Ltd.
                            @endif
                        </h3>
                        <img id="qr_img" src="" alt="">
                    </div>
                </div>
            </div>
        </div>
        <!--右侧酒店信息 start-->
    @include('includes.book.book-hotel-detail')
    <!--右侧酒店信息 end-->
    </div>
@endsection

@section('scripts')
    <script>
        var langBook = {!! json_encode(trans('book')) !!};
        var lang_cancel_yes = "{{trans('orders.cancelyes')}}";
        var lang_cancel_no = "{{trans('orders.cancelno')}}";
        var lang_back_to_search = "{{trans('search.backtoSearch')}}";
        var lang_login_expired = "{{trans('search.loginExpired')}}";
        var lang_login = "{{trans('search.login')}}";

        var paypalError = '{{$language == 1 ? "Please try other payment method, Paypal button has error: " : "请选择其他的支付方式，Paypal快捷支付有错误: "}}';

        {{--main data--}}
        var room = {!! json_encode(isset($room['room']) ? $room['room'] : []) !!};
        var hotel = {!! $hotel !!};
        var roomCount = parseInt({!! $room['room']['roomCount'] !!});
        var dayCount = parseInt({!! $room['room']['dayCount'] !!});
        var roomStatus = "<?=$room['room']['status']?>";
        var roomCurrency = '{{$room['room']['currency']}}';

        var app_env= "{{config('app.env') === 'production' ? 'production' : 'sandbox'}}"; // sandbox | production;
        var locale = '{{$language === 0 ? 'zh_CN' : $language === 2 ? 'zh_TW' : 'en_US'}}';  //paypal button shown language
        var app_pp_btn = "{{config('app.paypal_button')}}";
        var singleRoomNightPrice = '<?= ceil($room['room']['netPrice'] * 100 / $room['room']['dayCount'] / $room['room']['roomCount']) / 100.0 ?>';
        var singleRoomNightPriceB4Tax = '<?= isset($singleRoomNightPriceB4Tax) ? $singleRoomNightPriceB4Tax : '' ?>';
        var singleRoomNightTax = '<?= isset($singleRoomNightTax) ? $singleRoomNightTax : '' ?>';

        var checkin = '{{ $checkin }}';
        var checkout = '{{ $checkout }}';
        var hotelPath = '{{ $hotelPath }}';
        var source = '<?= isset($_COOKIE['source']) ? $_COOKIE['source'] : ''; ?>';
        var sessionKey = "{{$sessionKey}}";
        var displayId = '{{ $displayId }}';
        var rooms_key = '{{ $rooms_key }}';

    </script>
    {{--paypalBtn的js,需要先加载--}}
    {{--<script src="https://www.paypalobjects.com/api/checkout.min.js"></script>--}}
    {{--webpack js--}}
    <script src='{{ asset('/assets/js/pc/plugins.book.js') }}?1001'></script>
@endsection
