{{--fraud用户补差价页面 PC--}}
@extends( !$is_b  ?  'layouts.master' : 'layouts.master-b' )

<?php
$Suspicious = \App\Model\SuspiciousOrder::where('Reorderid', $Reorderid)->first();
$searchedInfo = json_decode($Suspicious->search_info, true);
$porder_tem = DB::table('porder_tem')->where('Reorderid', $Reorderid)->first();
$orgPrice = $Suspicious->currency . ' ' . $Suspicious->amount;
$hotelName = $porder_tem->goods_name;
$canOrNot = $roomDetails['rateClass'];
$cancelDate = isset($roomDetails['cancelDate']) ? substr(str_replace("T", " ", $roomDetails['cancelDate']), 0, 19) : '';
$totalPrice = $roomDetails['netPrice'];
$taxRate = config('constants.tax_rate');
$beforeTax = floor($totalPrice * (1-$taxRate)/ $roomDetails['dayCount'] / $roomDetails['roomCount']);
$tax = $totalPrice - ($beforeTax * $roomDetails['dayCount'] * $roomDetails['roomCount']);
$leftPrice = $totalPrice - $Suspicious->amount;
?>

@section('title', 'Continue Booking - Extra Payment')

@section('css')
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">
	<style>
		.f-13 {
			font-size: 13px;
		}
		.text-i {
			margin-top: 5px;
		}
        .form-card{
            padding: 10px;
            margin-bottom: 50px;
            border: #eeeeee 1px solid;
        }
        .title-main{
            padding: 30px 0;
            color: #333333;
            font-size: 26px;
            text-align: center;
        }
        .declineBtn{
            border: #dddddd 1px solid;
        }
	</style>
@endsection

@section('content')
	<div class="page_padding" id="app" style="width:1200px; margin:0 auto;">
		<div class="ibox float-e-margins hidden-print all-wrapper">
			{!! csrf_field() !!}
			<h1 class="ibox-noborder title-main" >
				{{trans('paypal.title')}}
			</h1>
			<div class="row">
                {{--left card info--}}
				<div class="col-md-8 form-card">
					<div class="ibox-content">
						<form class="form-horizontal" style="margin-top:20px;" role="form" id="creditform">
							{!! csrf_field() !!}
							<div class="form-group">
								<label class="col-sm-3 control-label" for="number">{{trans('refunds.cn')}}
								</label>
								{{--卡号--}}
								<div class="col-sm-6">
									<div class="input-group">
										<input type="text" class="form-control" name="number" value="" placeholder="{{trans('paypal.cn')}}" v-model="cardData.number"/>
										<span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
									</div>
									<div class="input-group">
										<label class="f-13 text-warning text-i">{{trans('paypal.use_previous_card')}} （******{{substr($ccNumber,4,4)}}）</label>
									</div>
								</div>
							</div>
							{{--信用卡在线付款--}}
							<div class="form-group">
								<label class="col-sm-3 control-label" for="cardType">{{trans('paypal.ct')}}</label>
								<div class="col-sm-9 form-inline">
									<img style="height:30px;" src="/img/payment/all-major-credit-cards.png"/>
									<input type="hidden" id="cardType" name="cardType" class="form-control" value=""/>
								</div>
							</div>
							{{--有效日期--}}
							<div class="form-group">
								<label class="col-sm-3 control-label" for="expirationDate">{{trans('paypal.ed')}}</label>
								<div class="col-sm-9 form-inline">
									<select id="month" name="month" class="form-control" v-model="cardData.month">
										<option value="01">{{trans('paypal.month')}}
											1{{trans('paypal.month1')}}</option>
										<option value="02">{{trans('paypal.month')}}
											2{{trans('paypal.month1')}}</option>
										<option value="03">{{trans('paypal.month')}}
											3{{trans('paypal.month1')}}</option>
										<option value="04">{{trans('paypal.month')}}
											4{{trans('paypal.month1')}}</option>
										<option value="05">{{trans('paypal.month')}}
											5{{trans('paypal.month1')}}</option>
										<option value="06">{{trans('paypal.month')}}
											6{{trans('paypal.month1')}}</option>
										<option value="07">{{trans('paypal.month')}}
											7{{trans('paypal.month1')}}</option>
										<option value="08">{{trans('paypal.month')}}
											8{{trans('paypal.month1')}}</option>
										<option value="09">{{trans('paypal.month')}}
											9{{trans('paypal.month1')}}</option>
										<option value="10">{{trans('paypal.month')}}
											10{{trans('paypal.month1')}}</option>
										<option value="11">{{trans('paypal.month')}}
											11{{trans('paypal.month1')}}</option>
										<option value="12">{{trans('paypal.month')}}
											12{{trans('paypal.month1')}}</option>
									</select>

									<select id="year" name="year" class="form-control" v-model="cardData.year">
										<option value="20">{{trans('paypal.year')}}
											2020{{trans('paypal.year1')}}</option>
										<option value="21">{{trans('paypal.year')}}
											2021{{trans('paypal.year1')}}</option>
										<option value="22">{{trans('paypal.year')}}
											2022{{trans('paypal.year1')}}</option>
										<option value="23">{{trans('paypal.year')}}
											2023{{trans('paypal.year1')}}</option>
										<option value="24">{{trans('paypal.year')}}
											2024{{trans('paypal.year1')}}</option>
										<option value="25">{{trans('paypal.year')}}
											2025{{trans('paypal.year1')}}</option>
										<option value="26">{{trans('paypal.year')}}
											2026{{trans('paypal.year1')}}</option>
										<option value="27">{{trans('paypal.year')}}
											2027{{trans('paypal.year1')}}</option>
										<option value="28">{{trans('paypal.year')}}
											2028{{trans('paypal.year1')}}</option>
									</select>
								</div>
							</div>
							{{--CVS验证码--}}
							<div class="form-group">
								<label class="col-sm-3 control-label" for="CVC">{{trans('paypal.cvc')}}</label>
								<div class="col-sm-9 form-inline">
									<input type="text" class="form-control" name="CVC" value="" placeholder="CVC" v-model="cardData.cvc"/>
								</div>
							</div>
							<hr>
							<div class="form-group form-inline">
								<div class="col-md-offset-3 col-md-4">
									<button type="button" class="btn loadingBtn btn-primary btn-block pull-right btn-lg" data-style="expand-right" id="bt" onclick="extrasubmitPayment()">{{trans('paypal.pmt')}}</button>
                                    <br>
                                    <button style="margin-top:30px;" class="btn btn-muted declineBtn btn-lg" onclick="fraudEmail()">{{trans('paypal.requestDecline')}}</button>
								</div>
							</div>
						</form>
					</div>
				</div>
                {{--right order detail--}}
				<div class="col-md-4" >
					<div class="form-group ibox-content no-border">
						<div style="border: 1px solid #e7eaec;">
							<br>
							<div class="caption" style="padding:0px 15px;">
								<h3 style="font-size:18px; font-weight:500;">
									<span id="hotel-name">{{$hotelName}}</span>
								</h3>
								<p style="padding-top:5px;">
									<span id="hotel-rating"><span class="entypo-star"></span><span class="entypo-star"></span><span class="entypo-star"></span></span>
								</p>
							</div>
							<div class="caption" style="padding:0px 15px 10px 15px;">
								<h3 style="font-size:14px;"></h3>
							</div>
							<div class="caption" style="padding:1px 15px;">
								<label><h3 style="font-size:14px;">{{trans('book.checkin')}}:</h3></label>
								<label style="float:right; font-size:14px;" class="control-label">{{$searchedInfo['checkin']}}</label>
							</div>
							<div class="caption" style="padding:1px 15px;">
								<label><h3 style="float:right; font-size:14px;">{{trans('book.checkout')}}:</h3></label>
								<label style="right:31px; position:absolute; font-size:14px;" class="control-label">{{$searchedInfo['checkout']}}</label>
							</div>

							<!--取消政策start-->
							<div class="border-bottom" style="border-bottom: 1px solid lightgrey !important; padding-bottom:15px; padding-left:15px;">
								<h3 style="font-weight:400; font-size:14px;">
									{{trans('book.canc')}}:
								</h3>
								<br/>
								<div id="hotel-cancel" style="color:#0065b8; font-size:14px">
									@if($canOrNot == '可取消' || $cancelDate)
										{{trans('book.freeb')}} {{$cancelDate}} {{trans('book.ltime')}}
									@else
										{{trans('book.non-ref')}}
									@endif
								</div>
							</div>
							<!--取消政策end-->
							<!--价格明细start-->
							<div style="padding-top:15px; margin-right:0;">
								<h3 style="padding-left:15px; font-weight:400; font-size:18px; padding-bottom:12px;">
									{{trans('book.soc')}}</h3>
								<label class="col-sm-12" style="padding-left:15px;font-size:14px;">{{trans('book.rooms')}}：
                                    <span style="font-size:16px; font-weight:500; color:#484848;" class="pull-right">{{$roomDetails['roomCount']}}</span>
								</label>
								<label class="col-sm-12" style="padding-left:15px;font-size:14px;">{{trans('book.nights')}}：
                                    <span style="font-size:16px; font-weight:500; color:#484848;" class="pull-right">{{$roomDetails['dayCount']}}</span>
								</label>
								<label class="col-sm-12" style=" font-size:14px;">{{trans('book.roomcost')}}：
                                    <span style="font-size:16px; font-weight:500; color:#484848;" class="pull-right">{{$roomDetails['currency']}}
										<span class="roomPrice">{{$beforeTax}}</span>
									</span>
								</label>
								<label class="col-sm-12" style="font-size:14px;">{{trans('book.tax')}}：
                                    <span style="font-size:16px; font-weight:500; color:#484848;" class="pull-right">
										{{$roomDetails['currency']}}<span id="roomTax">&nbsp;{{$tax}}</span>
									</span>
									<br> </label>
								<label style="font-size:15px; padding-left:15px; color:#484848;">{{trans('book.totalcharge')}}
									： </label>
								<span style="float:right;  margin-right:10px;" class="text-right text-danger">
									<span style="font-weight:500">{{$roomDetails['currency']}}</span>&nbsp;<span class="totalPrice" style="font-weight:500">{{$totalPrice}}</span>
								</span>
								<br/>
								<label style="font-size:15px; padding-left:15px; color:#484848;">{{trans('orders.TotalPaid')}}： </label>
								<span style="float:right; margin-top:-5px; margin-right:10px;" class="pull-right">
									<span style="font-weight:500"></span><span class="compare" style="font-weight:500">({{$orgPrice}})</span>
								</span>
							</div>
							<hr/>
							<label style="font-size:15px; padding-left:15px; color:#484848;">{{trans('paypal.leftToPay')}}
								： </label>
							<h2 style="float:right; margin-top:-15px; margin-right:10px;" class="text-right">
								<span style="font-weight:500; font-size:30px;"></span><span class="compare" style="font-size:30px; font-weight:500">{{$roomDetails['currency'] . ' ' .$leftPrice}}</span>
							</h2>
							<!--价格明细end-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		{{--start language--}}
      var lang_changeCard = "{{ trans('paypal.changecard') }}";
      var lang_phoneNotEnter = "{{ trans('paypal.phone_not_enter') }}";
      var lang_codeNotEnter = "{{ trans('paypal.code_not_enter') }}";
      var lang_sendCode = "{{ trans('paypal.sendCode') }}";
      var lang_vrfCode = "{{ trans('paypal.vrfCode') }}";
      var lang_valid = "{{ trans('paypal.valid') }}";
      var lang_headTitle = "{{trans('paypal.headtitle')}}";
      var lang_tooManyTimes = "{{trans('paypal.tooManyTimes')}}";
      var lang_phoneRecheck = "{{trans('paypal.phonerecheck')}}";
      var lang_sa = "{{trans('paypal.sa')}}";
      var lang_doNotRefresh = "{{trans('paypal.do_not_refresh')}}";
      var lang_phoneNotValid = "{{trans('paypal.phonenotvalid')}}";
      var lang_cardInDan = "{{trans('paypal.cardInDan')}}";
      var lang_pleaseUploadFile = "{{trans('paypal.pleaseUploadFile')}}";
      var lang_soldout = "{{trans('book.soldout')}}";
      var lang_limit = "{{trans('book.limit')}}";
      var lang_timeout = "{{trans('book.timeout')}}";
      var lang_auth = "{{trans('book.auth')}}";
      var lang_duplicate = "{{trans('book.duplicate')}}";
      var lang_cnetwork = "{{trans('book.cnetwork')}}";
      var lang_confirmDu = "{{trans('book.confirmDu')}}";
      var lang_confirm = "{{trans('book.confirm')}}";
      var lang_cancel = "{{trans('book.cancel')}}";
      var lang_failed = "{{trans('book.failed')}}";
      var lang_cfailed = "{{trans('book.cfailed')}}";
      var lang_backtoSearch = "{{trans('search.backtoSearch')}}";
      var lang_priceChange = "{{trans('search.priceChange')}}";
	  {{-- end language --}}
		jQuery(function ($) {
			$('.i-checks').iCheck({
				checkboxClass: 'icheckbox_square-green',
				radioClass: 'iradio_square-green',
			});

            $("#creditform").validate({
                rules: {
                    number: {
                        required: true,
                        minlength: 15,
                        maxlength: 16,
                        number: true
                    },
                    CVC: {
                        required: true,
                        minlength: 3,
                        maxlength: 4,
                        number: true
                    }
                }
            });

			$('.declineBtn').on('click', function () {
				$('.declineBtn').button('loading');
			});

		});

		function fraudEmail() {
			$.post("{{url('/potential-order/decline/' . $Reorderid)}}", {
				'_token': $("input[name='_token']").val(),
			}, function (data) {
				if (data.success) {
					$('.declineBtn').button('reset');
					swal({
						title: "{{trans('paypal.declineok')}}",
						type: "success",
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "OK",
					}, function () {
						window.location.href = "{{url('/search')}}";
					});
				} else {
					$('.declineBtn').button('reset');
					swal({
						title: "{{trans('paypal.declinefailed')}}",
						type: "warning",
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "OK",
					}, function () {
						window.location.href = self.location;
					});
				}
			});
		}

		function extrasubmitPayment() {
            if (!$("#creditform").valid()) {
                swal("{{trans('paypal.sa')}}");
                return;
            }else{
                $('.loadingBtn').button('loading');
			}

            $.post('{{url('/paypal/extra-pay/submit/' . $Reorderid)}}', $('#creditform').serialize(), function (data) {
				extracheckOrderStatus(data)
			});
		}

		function extracheckOrderStatus (data) {
            if (data.voucherUrl) {
                window.location.replace(data.voucherUrl);
            }else if (data === '') {
              var message;
              if(language === 0){
                message = '您已成功预订，我们会稍后发送您的预订成功邮件';
              }else{
                message = 'Thanks! Your hotel reservation is confirmed, and your confirmation Email is on the way';
              }
              swal({
                title: message,
                type: 'success',
                confirmButtonColor: '#DD6B55',
                allowOutsideClick: false,
                confirmButtonText: 'OK',
              }, function() {
                window.location.href ='/';
              });
            }  else if (data.errorId === "CC15006") {
                swal({
                    title: data.errorMessage,
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK",
                });
            }  else if (data.errorId === "ER1414") {
                swal({
                    title: "{{trans('paypal.clicked_wait')}}",
                    type: "warning",
                });
            } else if (data["errorId"] === "CC15005") {
                swal({
                    title: "{{trans('book.auth')}}",
                    type: "warning",
                    text: data["message"],
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK",
                }, function () {
                    window.location.href = self.location;
                });
            } else if (data['errorId'] === 'ER1415') { // for duplicate order check
                swal({
                    title: "可能是重复订单，请勿关闭该窗口，询问用户是否继续下单后选择继续或取消",
                    type: 'warning',
                    confirmButtonText: "{{trans('book.confirm')}}",
                    showCancelButton: true,
                    cancelButtonText: "{{trans('book.cancel')}}",
                }, function (confirmed) {
                    if (confirmed) {
                        $.post('/duplicate/proceed', {
                            '_token': $('meta[name="csrf_token"]').attr('content'),
                            'action': 'continue',
                            'Reorderid': data.Reorderid,
                            'vendor': 'cc',
                            'p_id': data.p_id,
                        }, function (data) {
                            checkOrderStatus(data);
                        });
                    } else {
                        $.post('/duplicate/proceed', {
                            '_token': $('meta[name="csrf_token"]').attr('content'),
                            'action': 'cancel',
                            'Reorderid': data.Reorderid,
                            'vendor': 'cc',
                            'p_id': data.p_id,
                        }, function (data) {
                            if (!data.success) {
                                data['message'] = '{{trans('book.failed')}}';
                            }

                            swal({
                                title: data.message,
                                type: 'warning',
                                confirmButtonText: "OK",
                            }, function () {
                                window.location.href = '/';
                            });
                        });
                    }
                });
            }  else if (data.errorId === "ER1401" || data.errorId === "ER1301") {
                swal({
                    title: "{{trans('book.soldout')}}",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('search.backtoSearch')}}",
                }, function () {
                    window.location.href = "{{url('/search')}}";
                });
            } else if (data["errorId"] === "ER1411" || data["errorId"] === "ER1402") {
                swal({
                    title: "{{trans('search.priceChange')}}",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('search.backtoSearch')}}",
                }, function () {
                    window.location.href = "{{url('/search')}}";
                });
            }else if (data["errorId"] === "IT1001") {
                swal({
                    title: "{{trans('book.cnetwork')}}",
                    type: "warning",
                    text: data.errorMessage,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('search.backtoSearch')}}",
                }, function () {
                    window.location.href = "{{url('/search')}}";
                });
            } else if (data.errorId) {
                swal({
                    title: data.errorId,
                    type: "warning",
                    text: data.errorMessage,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('search.backtoSearch')}}",
                }, function () {
                    window.location.href = "{{url('/search')}}";
                });
            } else {
                $('#declineBtn').button('reset');
                swal({
                    title: "Try again",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK",
                }, function () {
                    window.location.href = self.location;
                });
            }
        }

		// temp add vue
		var app = new Vue({
			el: '#app',
			data: {
				message: 'Hello Vue!',
				cardData: {
					number: '',
					cardType: 'Visa',
					month: '01',
					year: '18',
					cvc: '',
					phone: '',
					firstName: '',
					lastName: '',
					street: '',
					street2: '',
					city: '',
					state: '',
					zip: '',
					countryCode: 'US'
				}
			}
		});
        var checkIn = '';
        var checkOut = "";
        var lang_headTitle = "";
        var hotelRating = "";
        var hotelCategory = "";
        var phoneCountryCode = "";
        var phoneNum = "";

    </script>
    <script src="{{ asset('assets/js/pc/plugins.paypal.js') }}"></script>
@endsection
