{{--this hotel template has its own scss file, but share js with hotel default template--}}
@extends( !$is_b  ?  'layouts.master' : 'layouts.master-b' )
{{-- page variables --}}
<?php
$hotel_name = $language === 0 ? ($hotel->name_zh ? $hotel->name_zh : $hotel->name) : $hotel->name;
$domain = $is_usitour ? 'Usitour' : $is_aa ? 'America Asia' :'Usitrip';
// 由于controller hotel部分变量转义过，因此单独从数据库调取hotel信息给编辑使用
if($auth_pairing){
    $hotel_db = \App\Model\Hotel::where('hotelId', $hotel->hotelId)->select(['name', 'name_zh', 'city', 'address', 'zipcode', 'rating', 'description', 'description_zh'])->first();
}
?>

@section('title', $language === 0 ? '' . $hotel_name . '预订,' . $hotel_name . '特价,折扣,优惠-'.$domain.'走四方旅游网' : $language === 2 ? '' . $hotel->name . '預定,' . $hotel->name . '特價,折扣,優惠-'.$domain.'走四方旅遊網':'' . $hotel->name . '  - Hotel Deals & Photos - '.$domain)

@section('metas')
    <meta name="Keywords" content="{{trans('tdk.keywords_hotel', ['name' => $hotel_name])}}">
    <meta name="description" content="{{trans('tdk.description_hotel', ['name' => $hotel_name])}}">
@endsection

@section('gtag_events')
    @if(config('app.enable_tracking') && !$is_b && !$is_aa)
        <!-- Event snippet for 酒店预订 conversion pageIn your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
        @php
            $book_con_label = $is_usitour ? 'AW-825564842/NF5XCO2d3oUBEKq91IkD' : 'AW-825564842/k0XkCNfbxXoQqr3UiQM';
        @endphp
        <script>
            function gtag_report_conversion_book (url) {
                var callback = function () {
                    if (typeof (url) != 'undefined') {
                        window.location = url;
                    }
                };
                gtag('event', 'conversion', {
                    'send_to': '{{ $book_con_label }}',
                    'value': 1.0,
                    'currency': 'USD',
                    'transaction_id': '',
                    'event_callback': callback
                });
                return false;
            }
        </script>

        {{--adwords remarketing tag--}}
        <script>
            var today = new Date();
            todayStr = today.getFullYear() + '-' + today.getMonth() + 1 + '-' + today.getDate();
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            tomorrowStr = tomorrow.getFullYear() + '-' + tomorrow.getMonth() + 1 + '-' + tomorrow.getDate();
            var checkin = window.localStorage ? window.localStorage.getItem('checkin') : todayStr;
            var checkout = window.localStorage ? window.localStorage.getItem('checkout') : tomorrowStr;

            gtag('event', 'page_view', {
                'send_to': 'AW-825564842',
                'hrental_enddate': checkout,
                'hrental_id': '{{$hotel->hotelId}}',
                'hrental_startdate': checkin,
                'hrental_totalvalue': {{ $hotel->originalPrice or 0 }}
            });
        </script>
    @endif
@endsection

@section('css')
    <link rel="canonical" href="{{ config('app.url') }}/hotel/{{ $hotel->hotelId }}.html">
    {{--webpack css--}}
    <link href="{{ asset('assets/css/pc/plugins.hotel.css') }}" rel="stylesheet">
    {{--customized css--}}
    <link href="{{ asset('assets/css/pc/hotel.css') }}" rel="stylesheet">
@endsection

@section('content')
    @if(!$is_b && !$is_aa)
        @include('components.nav-bar')
    @endif
    @if(session()->has('message'))
        <div class="alert alert-danger sessionAlert">
            {{ session()->get('message') }}
        </div>
    @endif
    {{--top search--}}
    <div class="hotel-search-top">
        <div class="all-wrapper">
            <div class="pull-left back-see-all-hotels"><a onclick="backList({{$hotel->desId}})"><i class="fa fa-angle-left"></i> @lang('hotel.view_all_hotel') </a></div>
            <div class="cursor-pointer" onclick="goToByScroll('start-room-info')">
                <div class="pull-left break-top-line"></div>
                <div class="pull-left"><i class="small-hotel-icon usitrip_pc_all_img "></i><span class="hotel-city">{{ucfirst(strtolower($hotel->city))}}</span></div>
                <div class="pull-left break-top-dot"></div>
                <div class="pull-left" id="searched-date"></div>
                <div class="pull-left break-top-dot"></div>
                <div class="pull-left" id="searched-roomCount"></div> @lang('hotel.guest_room')
                @if(!$is_aa)
                <p class="pull-right usitrip-fade">@lang('hotel.question')</p>
                @endif
            </div>
        </div>
    </div>

    {{--float anchor bar--}}
    <div id="float-top" class="hidden">
        <div id="go-to-section-slide">
            <ul class="go-to-nav-top navbar-nav">
                <li class="top-hover">
                    <a class="page-scroll padding-left-none" onclick="goToByScroll('start-room-info')">@lang('hotel.title_room_type')</a>
                </li>
                @if(isset($hotel['area']))
                    <li class="top-hover">
                        <a class="page-scroll" onclick="goToByScroll('hotel-area-info')">@lang('hotel.title_area_info')</a>
                    </li>
                @endif
                @if(isset($hotel['hotel_facilities']) || count($facilities) > 0)
                    <li class="top-hover">
                        <a class="page-scroll" onclick="goToByScroll('hotel-detail-facilities')">@lang('hotel.title_hotel_facilities')</a>
                    </li>
                @endif
                @if(isset($hotel['reviews']))
                    <li class="top-hover">
                        <a class="page-scroll" onclick="goToByScroll('hotel-reviews')">@lang('hotel.review')</a>
                    </li>
                @endif
                <li class="top-hover">
                    <a class="page-scroll" onclick="goToByScroll('hotel-detail-policy')">@lang('hotel.policy')</a>
                </li>
                @if(isset($hotel['restaurants']))
                    <li class="top-hover">
                        <a class="page-scroll" onclick="goToByScroll('hotel-restaurants')">@lang('hotel.restaurants')</a>
                    </li>
                @endif
                <li class="top-hover">
                    <a class="page-scroll" onclick="goToByScroll('hotel-detail-map')">@lang('hotel.location')</a>
                </li>
                <li class="top-hover">
                    <a class="page-scroll" onclick="goToByScroll('nearHotel')">@lang('hotel.around')</a>
                </li>
                <li class="top-hover pull-right go-top">
                    @lang('hotel.back_to_top')
                    <i class="usitrip_pc_all_img arrow-back-to-top"></i>
                </li>
            </ul>
        </div>
        <div class="slide-bottom-part">
            <div class="float-top-hotel-info">
                <span class="float-hotel-name">
                    {{ $hotel->name }}
                    @if($language == 0)
                        @if($hotel->name_zh != "")
                            （{{$hotel->name_zh}}）
                        @endif
                    @endif
                </span> <span id="float-searched-info" class="searched-info-float pull-right"></span>
            </div>
        </div>
    </div>

    <div class="all-wrapper" id="hotelDetailApp">
        {{--热卖倒计时--}}
        @if($is_usitour && in_array($hotel->hotelId, $countdown_hotel))
            <img src="/img/general/countdown/cancun-countdown-pc.png" alt="hotel cancun ad">
            <button class="count-down-btn">
                Sales End In: <span id="countdown"></span>
            </button>
        @endif

        {{--顶部酒店信息--}}
        <div class="hotel-top-info">
            <div class="hotel-metainfo hotel-top-title-section">
                <div class="h-info-head-left">
                    <h1 class="text-left hotel-top-title-all">
                        <span class="pull-left hotel-top-title-name" class="tooltip"
                              title="{{ $hotel->name }}@if($language == 0 && $hotel->name_zh != '')({{$hotel->name_zh}})@endif">
                            {{ $hotel->name }} @if($language == 0 && $hotel->name_zh != '')({{$hotel->name_zh}})@endif
                        </span>
                        <span class="pull-left" id="hotel-rating"> </span>
                        @if(isset($top_property) && $top_property)
                            <span class="label-top-property">Top Property</span>
                        @endif
                    </h1>
                    <div class="hotel-top-title-address"><i class="fa fa-map-marker-alt"></i> {{ ucwords(strtolower($hotel->address))}}</div>
                    <p class="expedia-rate">
                        <span class='usitrip-fade'>
                            @if($hotel->guestRating)
                                @if($hotel->guestRating > 4.7)
                                    @lang('hotel.exceptional')
                                @elseif($hotel->guestRating >= 4.4)
                                    @lang('hotel.wonderful')
                                @elseif($hotel->guestRating >= 4.0)
                                    @lang('hotel.excellent')
                                @else
                                    @lang('hotel.very_good')
                                @endif
                            @else
                                @lang('hotel.expedia_rate')
                            @endif
                        </span>
                        <span id='guest-rating' class='usitrip-hover-tag'>
                            {{$hotel->guestRating or trans('hotel.not_available')}}
                        </span>
                        <span class="guest-review-anchor" id="guest_review_anchor" @if(isset($hotel['reviews'])) onclick="goToByScroll('hotel-reviews')"
                              style="cursor: pointer;" @endif>
                            (<span id="guest-review-count">{{$hotel->guestReviewCount == null ? 0: $hotel->guestReviewCount}}</span> @lang('hotel.expedia_rate_review'))
                        </span>
                    </p>
                </div>
            </div>
            <div class="hotel-passed-price-section">
                <div class="hotel-passed-price">
                    <p class="price-per-night-top">@lang('hotel.hotel_room_night_price')</p>
                    <p class="price-block">
                        {{--if no discount do not show--}}
                        <span class="usitrip-fade original-price-line-through">@if($hotel->discount > 0){{$hotel->currency == 'USD'? '$' : $hotel->currency}}{{floor($hotel->minPrice / (1 - $hotel->discount))}}@endif</span>
                        <span class="hotel-min-price usitrip-hover-tag"><img style="width:75px; height:35px; " src="/img/general/hotel_min_price_loading.gif"
                                                                             alt="price loading"/></span>
                    </p>
                </div>
                <a class="btn auto-select-book-btn" onclick="goToByScroll('start-room-info')">@lang('hotel.reserve')</a>
                <p class="label-price-gurantee">Price Guarantee</p>
            </div>
        </div>

        {{--酒店图片--}}
        <div class="hotel-smaller-photo-gallery">
            <div class="ibox">
                <div class="hotel-pic-left">
                    @php
                        if($language == 2){
                            if($is_aa){
                                  $thumbnail = '/img/error/aa_img_error.gif';
                            }else{
                                $thumbnail = '/img/error/tw_img_error.png';
                            }
                        }else{
                         if($is_usitour){
                                $thumbnail = '/img/error/usitour_img_error.png';
                            }else if($is_b){
                                 $number = rand(1, 10);
                                 $thumbnail = '/img/error/DefaultImage_'.$number.'.jpg';
                             }else{
                                $thumbnail = '/img/error/hotel_img_error.jpg';
                            }
                        }
                    @endphp

                    @if (count($imgUrlArray))
                        <button class="image-view-all" data-toggle="modal" data-target="#hotelAllPicturesModal">
                            @lang('hotel.view_all_photos')<br/>
                            ({{count($imgUrlArray)}}@lang('hotel.num_of_photos'))
                        </button>
                        <div id="hotel-image-carousel" class="carousel slide image-carousel" data-ride="carousel">
                            {{--超过42张图的话indicator则以白点形式显示--}}
                            <ol class="carousel-indicators @if(count($imgUrlArray) <= 42) carousel-image-wrapper @endif">
                                @if(count($imgUrlArray) <= 42)
                                    @foreach($imgUrlArray as $i => $image)
                                        <li data-target="#hotel-image-carousel" data-slide-to="{{ $i }}" class="carousel-image {{ $i === 0 ? 'active' : '' }}">
                                            <img src="{{ $image['url'] }}" alt="" width="100%" height="100%">
                                        </li>
                                    @endforeach
                                @else
                                    @foreach($imgUrlArray as $i => $image)
                                        <li data-target="#hotel-image-carousel" data-slide-to="{{ $i }}" class="{{ $i === 0 ? 'active' : '' }}"></li>
                                    @endforeach
                                @endif
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                @foreach($imgUrlArray as $i => $image)
                                    <div class="item {{ $i === 0 ? 'active' : '' }}">
                                        <img class="d-block w-100" src="{{ $image['url'] }}" alt="slide" width="100%" height="auto">
                                    </div>
                                @endforeach
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#hotel-image-carousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#hotel-image-carousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    @endif

                </div>
            </div>
        </div>

        {{--酒店介绍--}}
        <div class="hotel-description-right">
            <div class="hotel-description item relative">
                @if(!$hotel->description && !$hotel->description_zh)
                    <p class="hotel-description-title">@lang('hotel.overview')</p>
                    <p class="hotel-description-detail">@lang('hotel.noo')</p>
                @else
                    <p class="hotel-description-title">@lang('hotel.overview')</p>
                    <p class="hotel-description-detail">{!! ($language === 0 && $hotel->description_zh ? $hotel->description_zh : $hotel->description) !!}</p>
                @endif
            </div>
            {{--热门酒店服务与设施--}}

            @if(isset($hotel['hotel_facilities']) && isset($hotel['hotel_facilities']['popular']))
                <div class="facilities-banner relative">
                    @foreach($hotel['hotel_facilities']['popular'] as $index => $facility)
                        <div class="icon-wrapper" title="{{ $facility['facilityName'] }}">
                            <i class="icon-{{ str_replace('_', '-', $facility['facilityCode']) }}"></i>
                            <p>{{ $facility['facilityName'] }}</p>
                        </div>
                        @break($index === 4)
                    @endforeach
                    <div class="icon-wrapper icon-all-inclusive-wrapper hidden">
                        <i class="icon-all-inclusive"></i>
                        <p>All Inclusive</p>
                    </div>
                    <div class="icon-wrapper icon-facility-more-wrapper" onclick="goToByScroll('hotel-detail-facilities')">
                        <i class="icon-facility-more"></i>
                        <p>More</p>
                    </div>
                </div>
            @endif
        </div>

        {{--Hotel go to section --}}
        <div id="go-to-section">
            <ul class="go-to-nav">
                <li class="go-to-nav-item">
                    <a class="page-scroll" onclick="goToByScroll('start-room-info')">@lang('hotel.title_room_type')</a>
                </li>
                @if(isset($hotel['area']))
                    <li class="go-to-nav-item">
                        <a class="page-scroll" onclick="goToByScroll('hotel-area-info')">@lang('hotel.title_area_info')</a>
                    </li>
                @endif
                @if(isset($hotel['hotel_facilities']) || count($facilities) > 0)
                    <li class="go-to-nav-item">
                        <a class="page-scroll" onclick="goToByScroll('hotel-detail-facilities')">@lang('hotel.title_hotel_facilities')</a>
                    </li>
                @endif
                @if(isset($hotel['reviews']))
                    <li class="go-to-nav-item">
                        <a class="page-scroll" onclick="goToByScroll('hotel-reviews')">@lang('hotel.review')</a>
                    </li>
                @endif
                <li class="go-to-nav-item">
                    <a class="page-scroll" onclick="goToByScroll('hotel-detail-policy')">@lang('hotel.policy')</a>
                </li>
                @if(isset($hotel['restaurants']))
                    <li class="go-to-nav-item">
                        <a class="page-scroll" onclick="goToByScroll('hotel-restaurants')">@lang('hotel.restaurants')</a>
                    </li>
                @endif
                <li class="go-to-nav-item">
                    <a class="page-scroll" onclick="goToByScroll('hotel-detail-map')">@lang('hotel.location')</a>
                </li>
                <li class="go-to-nav-item">
                    <a class="page-scroll" onclick="goToByScroll('nearHotel')">@lang('hotel.around')</a>
                </li>
            </ul>
        </div>

        {{--搜索框--}}
        <div id="start-room-info" class="hotel-search">
            <form class="hotel-search-form" role="form" id="search-form" name="search-form">
                <input type="hidden" id="hotelId" name="hotelId" value="{{ $hotel->hotelId }}">
                <input type="hidden" id="destinationId" name="destinationId" value="{{ $hotel->desId }}">
                <input type="hidden" id="sessionKey" name="sessionKey" value="{{ $sessionKey }}">
                <input type="hidden" id="dayCount" name="dayCount" value="{{ $dayCount }}">
                <input type="hidden" id="roomCount" name="roomCount" value="{{ $roomCount }}">
                <input type="hidden" id="adultCount1" name="adultCount1" value="">
                <input type="hidden" id="childCount1" name="childCount1" value="">
                <input type="hidden" id="passedPartnerId" name="passedPartnerId" value="{{isset($passedpartnerId)? $passedpartnerId : ''}}">

                <p class="hotel-search-title">
                    @lang('hotel.table_search_title')
                </p>
                <div class="row hotel-search-wrapper">
                    <!--入住日期-->
                    <div class="col-sm-2">
                        <label class="label-datepicker">@lang('book.checkin')</label>
                        <div class="input-group date">
                            <input id="checkin" name="checkin" class="datepicker hotel-search-date-input" value="" onchange="updateCheckin()" placeholder="YYYY-MM-DD"
                                   autocomplete="off">
                        </div>
                    </div>
                    <!--退房日期-->
                    <div class="col-sm-2 hotel-search-checkout">
                        <label class="label-datepicker">@lang('book.checkout')</label>
                        <div class="input-group date">
                            <input id="checkout" name="checkout" class="datepicker hotel-search-date-input" value="" onchange="updateCheckout()" placeholder="YYYY-MM-DD"
                                   autocomplete="off">
                        </div>
                    </div>
                    <div class="col-sm-3 hotel-search-rooms">
                        <label class="label-rooms">@lang('hotel.room_guest')</label>
                        <div class="hotel-search-guest-box" id="hotel-room-guest-toggle">
                            <span id="tRoom">{{ $roomCount }}</span> <span id="tRoom_text">@lang('hotel.room'),</span> <span id="tAdult"></span> <span id="tAdult_text">@lang('hotel.adult'),</span>
                            <span id="tChild"></span> <span id="tChild_text">@lang('hotel.children')</span>
                        </div>
                        {{--Dialog 选择房间数，成人数，儿童数量--}}
                        <div id="searchDialog" style="display: none;">
                            <div class="hotel-search-guest-wrapper">
                                <div style="height:45px;">
                                    <label class="control-label number-label" style="width:30px; float:left" for="roomCount">@lang('hotel.search_t_count_room2')</label>
                                    <div class="input-group number-spinner" style="width:90px; float:left">
                                        <span class="input-group-btn">
                                            <a class="btn btn-subtract" id="roomCountSubtract" data-dir="dwn">-</a>
                                        </span>
                                        <input type="text" disabled name="roomCount" class="room-count-disable-in-dialog form-control text-center input-number"
                                                value="{{ $roomCount }}" max=8 min=1>
                                        <span class="input-group-btn">
                                            <a class="btn btn-add" id="roomCountAdd" data-dir="up">+</a>
                                        </span>
                                    </div>
                                </div>
                                {{--adult count--}}
                                <div style="height:45px;">
                                    <label class="control-label number-label" style="width:30px; float:left">@lang('hotel.search_t_count_adult')</label>
                                    <div class="input-group number-spinner" style="width:90px; float:left">
                            <span class="input-group-btn">
                                <a class="btn btn-subtract" id="adultCountSubtract" data-dir="dwn">-</a>
                            </span>
                                        <input type="text" disabled name="adultCount1" class="adult-count-disable-in-dialog form-control text-center input-number" value="2" max=4
                                                min=1>
                                        <span class="input-group-btn">
                                <a class="btn btn-add" id="adultCountAdd" data-dir="up">+</a>
                            </span>
                                    </div>
                                </div>
                                {{--child count--}}
                                <div style="height:45px;">
                                    <label class="control-label number-label" style="width:30px; float:left">@lang('hotel.search_t_count_child')</label>
                                    <div class="input-group number-spinner" style="width:90px; float:left">
                                    <span class="input-group-btn">
                                        <a class="btn btn-subtract" id="childCountSubtract" data-dir="dwn">-</a>
                                    </span>
                                        <input type="text" disabled name="childCount1" class="child-count-disable-in-dialog form-control text-center input-number" value="0" max=4
                                                min=0>
                                        <span class="input-group-btn">
                                            <a class="btn btn-add" id="childCountAdd" data-dir="up">+</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group child-age-wrapper hide" id="childAgeWrapper">
                                    <label class="number-label child-age-label">@lang('home.childAge')</label>
                                    <div id="childAgeContent">
                                        <select name="childAge[]" class="child-age-select">
                                            @for($i=0; $i<18; $i++)
                                                <option value="{{ $i }}" @if($i === 12) selected @endif>{{ $i === 0 ? '<1' : $i }} @lang('home.yearsOld')</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--房间detail-->
                    <!--提交按钮-->
                    <div class="col-sm-2 hotel-search-submit-wrapper">
                        <button type="button" class="btn ladda-button ladda-button-search update-hotel-search-btn" data-style="expand-right"
                                onclick="updateSearch({{$hotel->hotelId}})">
                            @lang('hotel.update_search_btn')
                        </button>
                    </div>
                </div>
            </form>
        </div>

        {{--房型--}}
        <table class='table hotel-room-box hotel-section-wrapper'>
            <thead>
            <tr>
                <th width='20%' class='hotel-table-title usitrip-mobile-bg'>@lang('hotel.hotel_t_room_type')</th>
                <th width='26.67%' class='hotel-table-title usitrip-mobile-bg'>@lang('hotel.hotel_t_bed_type')</th>
                <th width='26.67%' class='hotel-table-title usitrip-mobile-bg'>@lang('hotel.hotel_t_commend')</th>
                <th width='26.66%' class='hotel-table-title usitrip-mobile-bg table-align-right'>@lang('hotel.hotel_t_price')</th>
            </tr>
            @if($roomDetails['name'] != '' || substr($roomDetails['roomReference'],0,2) == 'eb')
                <?php
                $canOrNot = $roomDetails['rateClass'];
                $cancelDate = isset($roomDetails['cancelDate']) ? substr(str_replace("T", " ", $roomDetails['cancelDate']), 0, 19) : '';
                $roomCount = $roomDetails['roomCount'];
                $dayCount = $roomDetails['dayCount'];
                $nightPrice = $roomDetails['netPrice'];
                $finalNetPrice = floor($nightPrice * (1 - config('constants.tax_rate')) / $dayCount / $roomCount);
                ?>
                @if(isset($_COOKIE['a']))
                    <tr class="selectedOne">
                        <td>{{$roomDetails['name']}}</td>
                        <td>
                            @if(isset($roomDetails['bedType']))
                                {{$roomDetails['bedType']}}
                            @endif
                        </td>
                        <td>
                            <p>
                                @if(strpos($roomDetails['boardName'],"free") > -1 && strpos($roomDetails['boardName'],"breakfast") > -1)
                                    <i class='fa fa-coffee usitrip-hover-tag padding-right-5'></i><span>@lang('hotel.free_breakfast')</span>
                                @endif
                            </p>
                            <p>
                                @if($canOrNot == '可取消')
                                    <i class='fa fa-check usitrip-hover-tag padding-right-5'></i><span>@lang('hotel.free_cancel')</span></br>
                                    <span class='padding-left-19'>@lang('hotel.free_cancel_until'){{$cancelDate}}</span></br>
                                @else
                                    <i class='fa fa-check usitrip-hover-tag padding-right-5'></i>取消政策详情请见预订页</br>
                                @endif
                                <i class='fa fa-check usitrip-hover-tag padding-right-5'></i>无预定或信用卡手续费</br><i class='fa fa-check usitrip-hover-tag padding-right-5'></i>立即确认
                            </p>
                        </td>
                        <td class='table-align-right'>
                            <p class='hotel-room-price'>{{$roomDetails['currency'] == 'USD'? '$': $roomDetails['currency']}} {{$finalNetPrice}}</p>
                            <button type='submit' id="declineBtn" style="color:white;" onclick="fraudEmail()" class="btn btn-primary bookButton">
                                拒绝
                            </button>
                        </td>
                    </tr>
                @else
                    <tr class="selectedOne">
                        <td colspan="4">
                            @if(!$is_b)
                                亲爱的销售，为了记录您的工号，@if($is_aa)请刷新本页@else请先去走四方的后台登入后回到本页@endif进行下单或者拒绝的操作， 谢谢配合：）
                            @else
                                为了查看原有房型，请刷新本页进行下单或者拒绝的操作， 谢谢配合：）
                            @endif
                        </td>
                    </tr>
                @endif
            @endif
            </thead>
            {{--动态房型--}}
            <tbody id="roomOption" class="bg-white"></tbody>
        </table>

        {{--人人赚--}}
        @if(!$is_b && !$is_aa)
            <div id="affiliateBox" style="margin-bottom:25px;">
                <a href="/affiliate/register"><img title="affiliate register link" src="/img/general/affiliate/affiliate_banner_sm_{{$language}}.png" width="1200px"></a>
            </div>
        @endif

        {{--附近信息--}}
        @if(isset($hotel['area']))
            <div id="hotel-area-info" class="hotel-area-wrapper hotel-section-wrapper">
                <h4 class="hotel-section-title">
                    <span class="relative">
                        @lang('hotel.area_info')
                    </span>
                </h4>
                <div class="hotel-area row">
                    <div class="col-sm-6">
                        @foreach($hotel['area'] as $items)
                            <div class="hotel-area-title hotel-area-list">{{ $items['sortName'] }}</div>
                            @if(!empty($items['pois']))
                                @foreach($items['pois'] as $item)
                                    <div class="hotel-area-item hotel-area-list">
                                        <span>{{ $item['name'] or '' }}</span> <span class="fr">{{ $item['distance'] or '' }}</span>
                                    </div>
                                @endforeach
                            @endif
                        @endforeach
                    </div>
                    <div class="col-sm-6 hotel-area-column-2"></div>
                </div>
                <div class="hotel-area-toggler-wrapper row">
                    <div class="col-sm-12">
                        <a href="#" class="hotel-area-toggler hidden">Show more</a>
                    </div>
                </div>
            </div>
        @endif

        {{--所有酒店服务与设施--}}
        @if(isset($hotel['hotel_facilities']))
            <div id="hotel-detail-facilities" class="hotel-section-wrapper">
                <h4 class="hotel-section-title">
                    <span class="relative">@lang('hotel.title_hotel_facilities')</span>
                </h4>
                <div class="hotel-facilities">
                    @if(isset($hotel['hotel_facilities']) && !empty($hotel['hotel_facilities']['popular']))
                        <div class="hotel-facilities-popular">
                            <h5 class="facility-popular-title">@lang('hotel.popular_facility')</h5>
                            <div class="facility-popular-wrapper facility-small">
                                @foreach($hotel['hotel_facilities']['popular'] as $facility)
                                    <div class="facility-popular-item facility-item">
                                        <i class="facility-icon facility-icon-{{ $facility['facilityCode'] }}"></i>
                                        {{ $facility['facilityName'] }}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if(isset($hotel['hotel_facilities']) && isset($hotel['hotel_facilities']['all']))
                        <div class="hotel-facilities-all facility-small row">
                            @foreach($hotel['hotel_facilities']['all'] as $title => $facility_group)
                                @if(!empty($facility_group['facilities']))
                                    <div class="col-sm-4">
                                        <h5 class="facility-group-title facility-item">
                                            <i class="facility-icon facility-icon-{{ strtolower(preg_replace('/[\s|_]/', '-', $title)) }}"></i>
                                            {{ $facility_group['sortName'] }}
                                        </h5>
                                        <div class="facility-group-wrapper @if(count($facility_group['facilities']) > 10) collapsed @endif">
                                            @foreach($facility_group['facilities'] as $index=>$facility_item)
                                                <p class="facility-group-item">{{ $facility_item }}</p>
                                            @endforeach

                                            @if(count($facility_group['facilities']) > 10)
                                                <a href="#" class="facility-group-more">Show more</a>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        @else
            @if(count($facilities) > 0)
                @include('includes.hotel.hotel-facility')
            @endif

            <!--中文的翻译-->
            @if($language == 0)
                <div class="hotel-trans hotel-dynamic-policy-box">
                    <div class="hotel-trans-border-top">
                        <i class="hotel-room-translate  usitrip_pc_all_img">酒店常用术语翻译</i>
                    </div>
                    <ul class="whattheyoffers">
                        <li>Luxury/Deluxe 豪华</li>
                        <li>Double Room 双人间·大床房</li>
                        <li>Business 商务</li>
                        <li>Twin Room 双人间·双床房</li>
                        <li>King Bed/Size 特大号床(约1.8米宽)</li>
                        <li>Queen Bed/Size 大号床(约1.5米宽)</li>
                        <li>Executive 行政</li>
                        <li>Sing Bed 单人床(约1.2米宽)</li>
                        <li>Junior Suite 简套房</li>
                        <li>Run Of House 抢住房</li>
                        <li>Suite 套房</li>
                        <li>Studio 工作室</li>
                    </ul>
                </div>
            @endif
        @endif

        {{--评价--}}
        @if(isset($hotel['reviews']))
            @php
                $reviews = array_chunk($hotel['reviews'], 2);
            @endphp
            @if(count($reviews) > 0)
                <div id="hotel-reviews" class="hotel-reviews-wrapper hotel-section-wrapper">
                    <h4 class="hotel-section-title">
                    <span class="relative">@lang('hotel.review')</span>
                    </h4>

                    <div id="carousel-review" class="hotel-review carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            @for($i = 0; $i < count($reviews); $i++)
                                <li data-target="#carousel-review" data-slide-to="{{ $i }}" class="@if($i === 0)active @endif">{{ $i + 1 }}</li>
                            @endfor
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            @foreach($reviews as $index => $reviewArr)
                                <div class="item @if($index === 0) active @endif">
                                    <div class="row">
                                        @foreach($reviewArr as $review)
                                            <div class="review-item col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <p class="review-avatar"><i></i></p>
                                                        <p>By {{ $review['name'] or '' }}</p>
                                                        <p>{{ $review['review_date'] or '' }}</p>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <p class="review-rate-wrapper"><i class="review-rate review-rate-{{ $review['rate'] }}"></i></p>
                                                        <p>{{ $review['content'] or ''}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-review" role="button" data-slide="prev">
                            <i class="fa fa-chevron-left"></i> <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-review" role="button" data-slide="next">
                            <i class="fa fa-chevron-right"></i> <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

            @endif
        @endif

        {{--酒店政策--}}
        <div id="hotel-detail-policy" class="hotel-policy-wrapper hotel-section-wrapper">
            <h4 class="hotel-section-title">
                <span class="relative">@lang('hotel.policy')</span>
            </h4>
            <div class="hotel-dynamic-policy-box">
                <div class="ibox-noborder hotel-policy-box">
                    @if($hotel['checkin_time'])
                        <div class="hotel-policy-time hotel-policy-item">
                            <h3 class="hotel-policy-head">@lang('hotel.hotel_checkin_start')</h3>
                            <p class="hotel-policy-text">{{ $hotel['checkin_time'] }} </p>
                        </div>
                    @else
                        <div id="dynamic-checkin-start-box" class="hotel-policy-time hotel-policy-item hidden"></div>
                        <div id="dynamic-checkin-end-box" class="hotel-policy-time hotel-policy-item hidden"></div>
                    @endif
                    @if($hotel['checkout_time'])
                        <div class="hotel-policy-time hotel-policy-item">
                            <h3 class="hotel-policy-head">@lang('hotel.hotel_checkout')</h3>
                            <p class="hotel-policy-text">{{ $hotel['checkout_time'] }}</p>
                        </div>
                    @else
                        <div id="dynamic-checkout-box" class="hotel-policy-time hotel-policy-item hidden"></div>
                    @endif
                    @if($hotel['child_policy'])
                        <div class="hotel-policy-child hotel-policy-item">
                            <h3 class="hotel-policy-head">@lang('hotel.hotel_child')</h3>
                            <p class="hotel-policy-text">{{ $hotel['child_policy'] }}</p>
                        </div>
                    @else
                        <div id="dynamic-child-box" class="hotel-policy-child hotel-policy-item hidden"></div>
                    @endif
                    @if($hotel['pet_policy'])
                        <div class="hotel-policy-pet hotel-policy-item">
                            <h3 class="hotel-policy-head">@lang('hotel.hotel_pet')</h3>
                            <p class="hotel-policy-text">{{ $hotel['pet_policy'] }}</p>
                        </div>
                    @else
                        <div id="dynamic-pet-box" class="hotel-policy-pet hotel-policy-item hidden"></div>
                    @endif

                    <div class="hotel-policy-cancellation hotel-policy-item">
                        <h3 class="hotel-policy-head">@lang('hotel.1p')</h3>
                        <p class="hotel-policy-text">@lang('hotel.1pp') </p>
                    </div>
                    <div class="hotel-policy-price hotel-policy-item">
                        <h3 class="hotel-policy-head">@lang('hotel.2p')</h3>
                        <p class="hotel-policy-text">@lang('hotel.2pp')</p>
                    </div>
                    <div class="hotel-policy-bed hotel-policy-item">
                        <h3 class="hotel-policy-head">@lang('hotel.3p')</h3>
                        <p class="hotel-policy-text">@lang('hotel.3pp')</p>
                    </div>
                    <div class="hotel-policy-others hotel-policy-item">
                        <h3 class="hotel-policy-head">@lang('hotel.4p')</h3>
                        <p class="hotel-policy-text">@lang('hotel.4pp')</p>
                    </div>
                    @if($hotel['cards_accepted'])
                        <div class="hotel-policy-cards hotel-policy-item">
                            <h3 class="hotel-policy-head">@lang('hotel.hotel_cards')</h3>
                            <div class="hotel-policy-text">
                                @if($hotel['cards_accepted']['cards'])
                                    <p>
                                        @foreach($hotel['cards_accepted']['cards'] as $card)
                                            <i class="icon-card icon-card-{{ $card }}"></i>
                                        @endforeach
                                    </p>
                                @endif
                                @if($hotel['cards_accepted']['description'])
                                    <p class="hotel-policy-cards-desc">
                                        {{ $language === 0 && $hotel['cards_accepted']['description_zh'] ? $hotel['cards_accepted']['description_zh'] : $hotel['cards_accepted']['description'] }}
                                    </p>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        {{--餐厅--}}
        @if(isset($hotel['restaurants']))
            <div id="hotel-restaurants" class="hotel-restaurants-wrapper hotel-section-wrapper">
                <h4 class="hotel-section-title">
                    <span class="relative">@lang('hotel.restaurants')</span>
                </h4>
                <div class="row hotel-restaurants">
                    @foreach($hotel['restaurants'] as $restaurant)
                        <div class="hotel-restaurants-item col-sm-4">
                            <h5 class="restaurant-name">{{ $restaurant['name'] }}</h5>
                            @if(isset($restaurant['food']))
                                <div>@lang('hotel.food'): {{ $restaurant['food'] }}</div>
                            @endif
                            @if(isset($restaurant['menu']))
                                <div>@lang('hotel.menu'): {{ $restaurant['menu'] }}</div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

        {{--位置--}}
        <div id="hotel-detail-map">
            <h4 class="hotel-section-title"><span>@lang('hotel.location')</span></h4>
            <div class="ibox">
                <div class="ibox-noborder hotel-map-box">
                    <div class="row">
                        <div id="infowindow" class="infowindow"></div>
                        <div id="map" class="hotel-map"></div>
                    </div>
                </div>
            </div>
        </div>

        {{--推荐酒店--}}
        @if($roomDetails['name'] == '')
            <div class="nearby-hotels">
                <div class="all-wrapper" id="nearHotel">
                    <h4 class="hotel-section-title"><span>@lang('hotel.around')</span></h4>
                    <div class="row" id="hotelsBlock"></div>
                </div>
            </div>
        @endif

        @if($auth_pairing)
            <a href="/hotel/update/{{$hotel->hotelId}}" target="_blank" class="btn btn-danger hotel-update-button @if($is_b) margin-left @endif">更新酒店信息</a>
        @endif
    </div>
    {{--图片modal，需在body尾部或外面引用，避免safari和edge的兼容问题--}}
    <div id="hotelAllPicturesModal" class="modal modal-all-img fade" role="dialog">
        <div class="modal-dialog-all-img">
            <div class="modal-content-all-img">
                <div>
                    <button id="all-img-close" type="button" class="close modal-img-close" data-dismiss="modal">&times;</button>
                    <h4 class="hotel-img-title">
                        {{ $hotel->name }}
                        @if($language == 0)
                            @if($hotel->name_zh != "")
                                （{{$hotel->name_zh}}）
                            @endif
                        @endif
                        <span id="img-hotel-rating" class="img-star-white"> </span>
                    </h4>
                    <p id="view-all-img-icon" class="hidden">
                        <i class="view-photo-gallery usitrip_pc_all_img" onclick="backToAllImg()"></i> <span id="view-all-img-text">@lang('hotel.view_all_photos')</span>
                    </p>
                </div>
                <div class="modal-body-all-img">
                    <div class="all-images">
                        @if(count($imgUrlArray) > 0)
                            @foreach($imgUrlArray as $i => $hotel_image)
                                <a onclick="replaceViewAllToSingle('{{ $hotel_image["url"] }}', {{$i}})" class="{{ $i === 0 ? 'active' : '' }} hotel-view-all-imgs"
                                   style="background: url({{  $hotel_image['url'] }}) center center no-repeat;"></a>
                            @endforeach
                        @endif
                    </div>
                    <div class="single-image hide"></div>
                </div>
            </div>
        </div>
    </div>
    {{--  show more room images  --}}
    <div class="room-modal modal fade" id="roomInfoModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="room-modal-close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    <h4 class="room-modal-title"></h4>
                    <div class="room-modal-content">
                        <div class="room-modal-images"></div>
                        <div class="room-modal-details"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($adUser)
         @include('includes.hotel.hotel-registration-form')
    @endif
@endsection

@section('scripts')
    <script>
        var langHotel = {!! json_encode(trans('hotel')) !!};
        var lang_noResult = "{{trans('search.noResult')}}";
        var lang_format = "{{trans('search.format')}}";
        var lang_checkInvalid = "{{trans('search.checkinvalid')}}";
        var lang_checkOutValid = "{{trans('search.checkoutvalid')}}";
        var lang_limit = "{{trans('search.limit')}}";
        var lang_loginExpired = "{{trans('search.loginExpired')}}";
        var lang_reserve = "{{trans('search.Reserve')}}";
        var lang_roomSoldOut = "{{trans('search.roomSoldOut')}}";
        var lang_bookNotes = "{{trans('hotel.book_notes')}}";
        // end language
        var showPr = '{{ isset($seePr) ? $seePr : false }}';
        var hotelName = "{{$hotel->name}}";
        var roomCount = parseInt('{{ $roomCount }}');
        var dayCount = parseInt('{{ $dayCount }}');
        var childAges = {!! json_encode($childAges) !!};
        var searchInfo = {!! json_encode($searchInfo) !!};
        var imgUrlArray = {!! json_encode($imgUrlArray) !!};
        var countdown_hotel = {!! json_encode($countdown_hotel) !!};
        var searchedHotel = {!! $hotel !!};
        var hotelId = '{{$hotel->hotelId}}';
        var sessionKey = '{{$sessionKey}}';
        var is_usitour = parseInt('{{$is_usitour}}');
        var hotelRating = "{{ $hotel->rating }}";
        var hotelCategory = "{{ $hotel->category }}";
        var hotelDesId = "{{$hotel->desId}}";
        var hasCachedHotel = '{{$hasCachedHotel}}';
        var reorderId = '{{$Reorderid}}';
        var myCookie = '{{isset($sales_no) ? $sales_no : null}}';
        var extraImgPath = '{{ config('constants.image_host') . 'Extra_Image/' }}';
        var is_pairing = '{{ Auth::check() && in_array(Auth::user()->email, config('constants.hotel_pair_person', [])) }}';
        // for book button
        var userId = '{{  Auth::check() && Auth::user()->id ? Auth::user()->id : 11087 }}';
        var adUserId = '{{ config('app.2b_ad_partner_id') }}';
    </script>

    {{--webpack js --}}
    <script src="{{ asset('assets/js/pc/plugins.hotel.js') }}"></script>
    @php
        $lang_code = 'en';
        if($language === 0) {
           $lang_code = 'zh-CN';
        } elseif($language === 2) {
            $lang_code = 'zh-TW';
        }
    @endphp
    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_maps_api_key') }}&callback=initMap&language={{$lang_code}}" async defer></script>

    {{-- show update hotel info msg--}}
    @if(session()->has('msg'))
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script>
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            @if(session()->get('msg') == 'true' )
                toastr.success("Update success!");
            @else
                toastr.error("Update failed!!");
            @endif
        </script>
    @endif

@endsection
