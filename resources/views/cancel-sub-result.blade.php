@extends( !$is_b  ?  'layouts.master' : 'layouts.master-b' )

@section('title', 'Cancel Subscribe')

@section('css')
    <style>
        .unsub-block {
            margin-top: 250px;
            display: flex;
            justify-content: center;
            font-size: 18px;
        }
        .unsub-success {
            border: 1px solid #a8aaac;
            width: 410px;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 20px 25px;
        }
        .unsub-success img {
            margin-right: 20px;
        }
        .unsub-fail {
            color: #e4393c;
            border: 1px solid #a8aaac;
            padding: 20px 25px;
        }
    </style>
@endsection

@section('content')
    <div class="unsub-block">
        @if($success === true)
            <div class="unsub-success">
                @if($language === 0)
                    <span>
                        <img src="{{ asset('/img/general/subscribe/subscribe.png') }}" alt="取消成功">
                    </span>
                    <span>您已经取消订阅走四方邮件服务，如需获得更多酒店优惠信息，请<a href="https://www.usitrip.com">登录走四方旅游网。</a></span>
                @else
                    <span>
                        <img src="{{ asset('/img/general/subscribe/subscribe.png') }}" alt="取消成功">
                    </span>
                    <span>You have successfully unsubscribed from Usitour email service. Please visit <a href="https://hotel.usitour.com">https://hotel.usitour.com</a> for special promotions.</span>
                @endif
            </div>
        @else
            <div class="unsub-fail">
                @if($language === 0)
                    <span>
                        <img src="{{ asset('/img/general/subscribe/warning.png') }}" alt="取消成功">
                    </span>
                    <span>取消订阅失败</span>
                @else
                    <span>
                        <img src="{{ asset('/img/general/subscribe/warning.png') }}" alt="取消成功">
                    </span>
                    <span>Fail to unsubscribe</span>
                @endif
            </div>
        @endif
    </div>
@endsection
