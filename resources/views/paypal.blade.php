@extends( !$is_b  ?  'layouts.master' : 'layouts.master-b' )
<?php
if (config('app.name') === config('app.b_host_keyword')) {
    $year20 = '20'; //两个paypal要求的数据year不同，b端的要求2018 。。。
} else {
    $year20 = '';  //两个paypal要求的数据year不同，c端的要求18 。。。
}
?>

{{--  sigle page title   --}}
@section('title')
    @if	($language == 0)
        {{ $hotelInfo['name'] . '预订_' . $hotelInfo['name'] .$hotelInfo['name_zh']. '_价格_房型_设施_环境_地址_付款-走四方旅游网' }}
    @elseif	($language == 2)
            {{ $hotelInfo['name'] . '預定_' . $hotelInfo['name'] . '_價格_房型_設施_環境_地址_付款-走四方旅遊網' }}
    @else
        {{ $hotelInfo['name'] . ' Booking_' . $hotelInfo['name'] . ' Price_Room Type_Facilities_Environment_location Reserving' }}
    @endif
@endsection

@section('css')
    {{--webpack css--}}
    <link href="{{ asset('assets/css/pc/plugins.paypal.css') }}" rel="stylesheet">
@endsection

@section('content')
    @include('includes.book.book-action-top')
    <div class="page_padding screenWidth" id="app">
        <div class="ibox float-e-margins">
            {!! csrf_field() !!}
            {{--左侧 start--}}
            <div class="book-info-box">
                @include('includes.paypal.paypal-top-step-description')
                <form class="form-horizontal" role="form" id="creditform">
                    {!! csrf_field() !!}
                    <input type="hidden" name="Reorderid" id="Reorderid" value="{{$Reorderid}}">
                    <input type="hidden" id="language" name="language" value="{{$language}}">
                    <p class="subHead">{{ trans('paypal.cardInfo') }}</p>
                    <br/>
                    {{--1：信用卡信息--}}
                    @include('includes.paypal.paypal-credit-card-info')
                    {{--2:地址信息--}}
                    @include('includes.paypal.paypal-address-info')
                </form>
                @include('components.processing-modal')
            </div>
            {{--左侧 end--}}
            {{--右侧 start--}}
            @include('includes.paypal.paypal-hotel-detail')
        </div>
        {{--授权书modal--}}
        @include('includes.paypal.paypal-authorize-modal')
    </div>
@endsection

@section('scripts')
    {{--这里可以定义本页需要的php变量转js变量--}}
    <script>
        var source = '<?= isset($_COOKIE['source']) ? $_COOKIE['source'] : ''; ?>';
        var hotelPath = '{{ $hotelPath }}';
        var hotelRating = "{{ $hotelInfo->rating }}";
        var hotelCategory = "{{ $hotelInfo->category }}";
        var host = '<?= isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '' ?>';
        var Recorder_id = '{{$Reorderid}}'; // for function checkValidatePhone
        var phoneCountryCode = '{{$phoneCountryCode}}';
        var phoneNum = '{{$phone}}';
        {{--vue调用下面两个变量，不要写到jquery里面--}}
        var searchInfo = {!! $tempInfo->search_info !!};
        var checkIn = isUsitour ? searchInfo.checkin.substr(5, 10)+'-'+searchInfo.checkin.substr(0, 4) : searchInfo.checkin ;
        var checkOut = isUsitour ? searchInfo.checkout.substr(5, 10)+'-'+searchInfo.checkout.substr(0, 4) :searchInfo.checkout;

        {{--start language--}}
        var langPaypal = {!! json_encode(trans('paypal')) !!};
        var lang_soldout = "{{trans('book.soldout')}}";
        var lang_limit = "{{trans('book.limit')}}";
        var lang_timeout = "{{trans('book.timeout')}}";
        var lang_auth = "{{trans('book.auth')}}";
        var lang_duplicate = "{{trans('book.duplicate')}}";
        var lang_cnetwork = "{{trans('book.cnetwork')}}";
        var lang_confirmDu = "{{trans('book.confirmDu')}}";
        var lang_confirm = "{{trans('book.confirm')}}";
        var lang_cancel = "{{trans('book.cancel')}}";
        var lang_failed = "{{trans('book.failed')}}";
        var lang_cfailed = "{{trans('book.cfailed')}}";
        var lang_backtoSearch = "{{trans('search.backtoSearch')}}";
        var lang_priceChange = "{{trans('search.priceChange')}}";
        {{-- end language --}}
    </script>
    {{--webpack js--}}
    <script src="{{ asset('assets/js/pc/plugins.paypal.js') }}?v=1000a"></script>
@endsection
