{{--此页面给销售检查fraud订单，检查确认订单信息然后book的页面--}}
@extends( !$is_b  ?  'layouts.master' : 'layouts.master-b' )

<?php
$totalPrice = 0;
$room = $rooms[0];
$childNo = $room['occupancy']['children'];
?>

@section('title', 'Hotel Re-Booking')

@section('metas')
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
@endsection

@section('css')
    {{--webpack css--}}
    <link href="{{ asset('assets/css/pc/plugins.book.css') }}?10082018a" rel="stylesheet">
    <style>
        .book-check-left {
            padding: 30px;
            background-color: #fff;
        }
        .book-check-right {
            overflow-y: auto;
        }
        .compare{
            padding: 20px;
        }
        .img-responsive-display {
            width: 100%;
            max-height: 300px;
        }
    </style>
@endsection

@section('content')
    @include('includes.book.book-action-top')
    <div class="all-wrapper">
        <div class="row" style="margin:35px auto;">
            {{--<!--左侧 start-->--}}
            <div class="col-md-7 book-check-left">
                <div class="ibox">
                    <div class="ibox-noborder" style="margin-right:15px;">
                        <form class="form-horizontal" role="form" method="post" id="book-form">
                            {!! csrf_field() !!}
                            <input type="hidden" name="bookIndex" value="{{$bookIndex}}"> <input type="hidden" name="sessionKey" value="{{$sessionKey}}"> <input type="hidden" name="displayId" value="{{$displayId}}">
                            <input type="hidden" id="passedpartnerId" name="passedpartnerId" value="{{isset($passedpartnerId) ? $passedpartnerId: '' }}">
                            <input type="hidden" name="coupon_code" id="couponCode" value="{{ $coupon_code }}">
                            {{--<!--住客信息 start-->--}}
                            <div>
                                <h3 style="font-weight:400; font-size:18px; border-bottom:1px solid lightgrey; padding-bottom:12px;color:#484848;">
                                    {{trans('book.bdetail')}}: </h3>
                                <div>
                                    <?php
                                    for($i = 0; $i < $room['room']['roomCount']; $i++)
                                    {
                                    $adultNo = $room['occupancy']['adults'];
                                    $childNo = $room['occupancy']['children'];
                                    $roomNo = $room['occupancy']['rooms'];
                                    $totalPrice = $room['room']['netPrice'];
                                    $totalGuest = $adultNo + $childNo;

                                    $guest = $guests[$i];
                                    $guestEach = $guest[0];
                                    $guestOneFn = $guestEach['name'];
                                    $guestOneLn = $guestEach['surname'];
                                    ?>
                                    <div style="padding-left:15px;">
                                        <input type="hidden" name="roomRef<?=$i + 1?>" id="roomRef" value="<?php echo $room['room']['roomRef']; ?>"> <input type="hidden" name="room<?=$i + 1?>" value="<?=$room['room']['name']?>">
                                        <h4 style="margin-top:30px;">
                                            <strong style="font-weight:500; font-size:16px;">{{trans('book.room')}}<?=$i + 1?></strong><label>: &nbsp;<label>{{$adultNo}}
                                                    &nbsp;{{trans('book.adult')}}
                                                    ,&nbsp;</label><label>{{$childNo}}
                                                    &nbsp;{{trans('book.children')}}
                                                    ,&nbsp;</label><span Style="text-transform:lowercase;" class=""><?=$room['room']['name']?></span>,&nbsp;<span Style="text-transform:lowercase;" class=""><?=$room['room']['boardName']?></span></label>
                                        </h4>
                                    </div>
                                    <?php
                                    for($j = 1; $j <= $adultNo; $j++){
                                    if ($adultNo > 1) {
                                        $adultNo = 1;
                                    }
                                    ?>
                                    <div class="form-group" id="hotel-booker">
                                        <label class="col-sm-3 control-label" style="text-align:left; padding-left:15px; font-size:14px; margin-bottom:10px;color:#484848;">{{trans('book.pguest')}}
                                            :</label>
                                        <div class="col-sm-4">
                                            <input disabled class="form-control" style="margin-right:15px; border:1px solid #e7eaec;margin-bottom:10px;" type="text" placeholder="{{trans('book.fname')}}" name="guest<?=$i + 1?>_a1_fn" id="guest<?=$i + 1?>_a1_fn" value="{{$guestOneFn or ''}}"/>
                                        </div>
                                        <div class="col-sm-5">
                                            <input disabled style="border:1px solid #e7eaec;margin-bottom:10px;" class="form-control " type="text" placeholder="{{trans('book.lname')}}" name="guest<?=$i + 1?>_a1_ln" id="guest<?=$i + 1?>_a1_ln" value="{{$guestOneLn or ''}}"/>
                                        </div>
                                    </div>
                                    <?php
                                    }}
                                    ?>

                                    <div class="form-group">
                                        <span class="col-sm-12" style="text-align:left; color:red; padding-left:15px; padding-bottom:5px;"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;{{trans('book.tip')}}</span>
                                    </div>
                                    {{--<!--联系人信息 start-->--}}
                                    <div style="margin-top:20px;">
                                        <h3 style="font-weight:400; font-size:18px; border-bottom:1px solid lightgrey; padding-bottom:12px;">{{trans('book.contactinfo')}}</h3>
                                        <div style="padding-top:15px; padding-bottom:14px;">
                                            <div class="form-group" id="hotel-booker">
                                                <label class="col-sm-3 control-label" style="text-align:left;  font-size:14px;padding-left:15px">{{trans('book.cname')}}
                                                    &nbsp;</label>
                                                <div class="col-sm-4">
                                                    <input disabled style="margin-right:15px; border:1px solid #e7eaec;" class="form-control" type="text" placeholder="{{trans('book.fname')}}" name="booker_fn" value="{{$booker['firstname'] or ''}}"/>
                                                </div>
                                                <div class="col-sm-5">
                                                    <input  disabled style="margin-right:15px; border:1px solid #e7eaec;" class="form-control" type="text" name="booker_ln" placeholder="{{trans('book.lname')}}" value="{{$booker['lastname'] or ''}}">
                                                </div>
                                            </div>
                                            <div class="form-group" id="hotel-booker">
                                                <label class="col-sm-3 control-label" style="text-align:left; font-size:14px; padding-left:15px">{{trans('book.cphone')}}
                                                    :&nbsp;</label>
                                                <div class="col-sm-2">
                                                    <input  disabled style="margin-right:15px; border:1px solid #e7eaec;" class="form-control" type="text" name="booker_phone_country" value="{{$booker['phone_country'] or ''}}"></div>
                                                <div class="col-sm-7">
                                                    <input disabled  style="margin-right:15px; border:1px solid #e7eaec;" class="form-control" type="text" name="booker_phone" placeholder="{{trans('book.phone')}}" value="{{$booker['phone'] or ''}}">
                                                </div>
                                            </div>
                                            <div class="form-group" id="hotel-booker">
                                                <label class="col-sm-3 control-label" style="text-align:left;  font-size:14px;padding-left:15px">{{trans('book.cemail')}}
                                                    :&nbsp;</label>
                                                <div class="col-sm-9">
                                                    <input  disabled style="margin-right:15px; border:1px solid #e7eaec;" class="form-control" type="text" name="booker_email" placeholder="{{trans('book.bEmailr')}}" value="{{$booker['email'] or ''}}">
                                                </div>
                                            </div>
                                            <div class="form-group" id="hotel-booker">
                                                <label class="col-sm-3 control-label" style="text-align:left;  font-size:14px;padding-left:15px">{{trans('book.notes')}}
                                                    :</label>
                                                <div class="col-sm-9">
                                                    <input style="margin-right:15px; border:1px solid #e7eaec;" class="form-control" type="text" name="booker_remark" placeholder="{{trans('book.noteoption')}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--联系人信息 end-->
                                </div>
                            </div>
                            <!--住客信息 end-->

                            <!--付款方式start-->
                            <div class="form-group">
                                <div class="row" style="padding-left:15px; padding-top:20px;padding-right:15px;">
                                    <input type="hidden" name="hotelId" value="{{ $hotelId }}">
                                </div>
                                <button type="button" class="btn btn-primary pull-left btn-block ladda-button ladda-button-book" data-style="expand-right" id="bt" name="bt" disabled="true" onclick="bookRooms();" style="width:100%; font-size:16px;">
                                    价格一致或者客人同意补款，点击下单或者发送补差价邮件链接
                                </button>
                            </div>
                            <!--付款方式end-->
                        </form>
                    </div>
                </div>
            </div>
            {{--<!--左侧 end-->--}}
            {{--<!--右侧 start-->--}}
            <div class="col-md-5 book-check-right">
                <div>
                    <div class="ibox-content">
                        <div  style="background: #fff;">
                            <div class="caption" style="padding:0px 15px;">
                                <h3 style="font-size:18px; font-weight:500;">
							<span id="hotel-name">
								{{ $hotel->name }}
                                @if($language == 0 && $hotel->name_zh != "")
                                    ({{$hotel->name_zh}})
                                @endif
							</span>
                                </h3>
                                <p style="padding-top:5px;"><span id="hotel-rating">{{ $hotel->rating }}</span></p>
                            </div>
                            <div class="caption" style="padding:0px 15px 10px 15px;">
                                <medium style="font-size:14px;">
                                    <span class="entypo-direction"></span>&nbsp;<span id="hotel-address">{{str_replace(",","",$hotel->address)}}
                                        , {{ is_null($hotel->destination) ? '' :$hotel->destination->desName }}</span>
                                </medium>
                            </div>
                            <div class="caption" style="padding:1px 15px;">
                                <label>
                                    <medium style="font-size:14px;">{{trans('book.checkin')}}
                                        :
                                    </medium>
                                </label><label style="float:right; font-size:14px;" class="control-label">{{$checkin}}</label>
                            </div>
                            <div class="caption" style="padding:1px 15px;">
                                <label>
                                    <medium style="font-size:14px;">{{trans('book.checkout')}}
                                        :
                                    </medium>
                                </label> <label style="float:right; font-size:14px;" class="control-label">{{$checkout}}</label>
                            </div>

                            {{--<!--取消政策start-->--}}
                            <div class="border-bottom" style="border-bottom: 1px solid lightgrey !important; padding-bottom:15px; padding-left:15px;">
                                <medium style="font-weight:400; font-size:14px;">
                                    {{trans('book.canc')}}:
                                </medium>
                                <br/> <br>
                                <div id="hotel-cancel" style="color:#0065b8; font-size:14px">
                                    <span id="hotel-cancel">
                                           <img src="/img/general/book-loading-gif.gif" style="height:55px; margin-top:-25px; margin-bottom:-25px;" />
                                    </span>
                                </div>
                            </div>
                            {{--<!--取消政策end-->--}}
                            {{--<!--价格明细start-->--}}
                            <div style="padding-top:15px; margin-right:5px;">
                                <h3 style="padding-left:15px; font-weight:400; font-size:18px; padding-bottom:12px;">{{trans('book.soc')}}</h3>
                                <label class="col-sm-12" style="padding-left:15px;font-size:14px;">{{trans('book.rooms')}}
                                    ：
                                    <large style="font-size:16px; font-weight:500; color:#484848;"><?=$room['room']['roomCount']?></large>
                                </label> <br/><br/> <label class="col-sm-12" style="padding-left:15px;font-size:14px;">{{trans('book.nights')}}
                                    ：
                                    <large style="font-size:16px; font-weight:500; color:#484848;"><?=$room['room']['dayCount']?></large>
                                </label> <br/><br/> <label class="col-sm-12" style=" font-size:14px;">{{trans('book.roomcost')}}
                                    ：
                                    <large style="font-size:16px; font-weight:500; color:#484848;"><?=$room['room']['currency']?>
                                        <span class="roomPrice"><?=ceil(
                                                $room['room']['beforeTax'] * 100 / $room['room']['roomCount'] / $room['room']['dayCount']
                                            ) / 100.0 ?></span>
                                    </large>
                                </label> <br/><br/>
                                <label class="col-sm-12" style="font-size:14px;">
                                    {{trans('book.tax')}}
                                    ：
                                    <large style="font-size:16px; font-weight:500; color:#484848;">
                                        <?php echo $room['room']['currency']; ?>
                                        &nbsp;<span id="roomTax">{{$room['room']['tax']}}</span>
                                    </large>
                                    <br/> </label> <br/> <br/>
                                <div class="applied-coupon-block hide col-sm-12">
                                    <p class="color-green">@lang('book.coupon_applied_title')</p>
                                    <p class="coupon-amount color-green"><span>@lang('book.coupon_applied_desc')</span><span id="couponAmount" class="pull-right"></span></p>
                                    <br/>
                                </div>
                                <label style="font-size:15px; padding-left:15px; color:#484848;">{{trans('book.totalcharge')}}
                                    ： </label>
                                <large style="float:right; margin-top:-15px; margin-right:30px;" class="text-right text-danger">
                                    <span style="font-size:30px; font-weight:500">{{$room['room']['currency']}}</span>&nbsp; <span class="totalPrice" style="font-size:30px; font-weight:500">{{$totalPrice}}</span>
                                </large>
                            </div>
                            {{--<!--价格明细end-->--}}
                            <p class="compare">
                                已付款：{{ $paidCurrency . ' ' . $paidAmount }}
                            </p>
                            {{--<!--注意事项start-->--}}
                            <div class="border-noborder" style="padding-left:15px; margin-top:30px;">
                                <h2 style="font-weight:400; font-size:14px;">{{trans('book.fineprint')}} :</h2>
                                <div id="hotel-comment" style="font-size:14px; text-align: left;
	-moz-text-align-last: left; /* Code for Firefox */
	text-align-last: left;">
                                    <?php
                                    if (array_key_exists(
                                            "comments",
                                            $room['room']
                                        ) && (!empty($room['room']['comments']))) {
                                        foreach ($room['room']['comments'] as $comment) {
                                            echo $comment['comments'];
                                        }
                                    } else {
                                        echo "None";
                                    }
                                    ?>
                                </div>

                            </div>
                            {{--<!--注意事项end-->--}}
                        </div>
                    </div>
                </div>
            </div>
        @include('components.processing-modal')
        {{--<!--右侧 end-->--}}
        </div>
    </div>
@endsection

@section('scripts')
    {{--webpack js--}}
    <script src="{{ asset('assets/js/pc/plugins.book-check.js') }}?v=1000a"></script>
    <script type="text/javascript">
        var source = '<?= isset($_COOKIE['source']) ? $_COOKIE['source'] : ''; ?>';
        var sessionKey = '{{$sessionKey or ''}}';
        var roomRef = '{{$room['room']['roomRef']}}';
        var totalPrice = 0;
        var coupon_amount = {{ $coupon_amount }};
        var rooms_key = '{{ $rooms_key }}';

        jQuery(function ($) {
            var nowPrice = $('.totalPrice').text();

            $('.ladda-button-book').prop('disabled', true);
            doRoomCheck();
            $('.ladda-button-book').prop('disabled', false);

            $("#hotel-rating").html(buildRating('{{ empty($hotel->rating) ? 'null' : $hotel->rating }}', "{{ $hotel->category }}"));

            $.validator.addMethod("accept", function (value, element, param) {
                return value.match(new RegExp("." + param + "$")) || value == "";
            }, '{{trans('book.englishonly')}}');

            $.validator.addMethod("phone", function (value, element) {
                // allow only number
                return this.optional(element) || /^[0-9]+$/.test(value);
            }, '{{trans('book.numberOnly')}}');

            $("#book-form").validate({
                rules: {
                    booker_ln: {
                        required: true,
                        minlength: 2
                    },
                    booker_fn: {
                        required: true,
                        minlength: 2
                    },
                    booker_phone: {
                        required: true,
                        minlength: 6,
                        phone: true
                    },
                    booker_email: {
                        required: true,
                        email: true
                    },
                    <?php
                        for($i = 1; $i <= $room['room']['roomCount']; $i++)
                        {
                        $adultNo = $room['occupancy']['adults'];
                        for($j = 2; $j <= $adultNo; $j++)
                        {
                        ?>
                    guest{{$i}}_a1_ln: {
                        required: true,
                        minlength: 2
                    },
                    guest{{$i}}_a1_fn: {
                        required: true,
                        minlength: 1
                    },
                    guest{{$i}}_a{{$j}}_ln:
                      {
                          required: {
                              depends: function () {
                                  return $("#guest{{$i}}_a{{$j}}_fn").val() != '';
                              }
                          },
                          minlength: 2
                      },

                    guest{{$i}}_a{{$j}}_fn:
                      {
                          required: {
                              depends: function () {
                                  return $("#guest{{$i}}_a{{$j}}_ln").val() != '';
                              }
                          },
                          minlength: 1
                      },
                    <?php
                    }
                    }
                    ?>
                }

            });
        });

        //如果有special request
        var collapsedSizep = '5px';
        $('.specialrequest').each(function () {
            var x = this.scrollHeight;
            var div = $(this);
            if (x > 10) {
                div.css('height', collapsedSizep);
                @if($language == 0)
                div.after(
                  '<a style="padding-top:5px; padding-left:15px; width:160px; font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti; outline:none;" id="seemore" class="specialrequest text-left"  onclick=this.outerHTML="" href="#" >{{trans('book.addrequestoptional')}}</a>');
                @endif
                @if($language == 1)
                div.after(
                  '<a style="padding-top:5px; padding-left:15px; width:235px; font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti; outline:none;" id="seemore" class="specialrequest text-left"  onclick=this.outerHTML="" href="#" >{{trans('book.addrequestoptional')}}</a>');
                        @endif
                var link = div.next();
                link.click(function (p) {
                    p.stopPropagation();
                    if (link.text() !== '') {
                        link.text('');
                        div.animate({'height': x});
                    }
                });
            }

        });

        function doRoomCheck () {
            var rf = new Array();
            var status = "<?=$room['room']['status']?>";

            for (var i = 0; i < '<?=$room['room']['roomCount']?>'; i++) {
                rf[i] = {"roomReference": roomRef};
            }

            $('.ladda-button-book').ladda().ladda('start');

            $.post("/rooms/check", {
                '_token': $("input[name='_token']").val(),
                'references': JSON.stringify(rf),
                'roomCount': "{{$room['room']['roomCount']}}",
                'dayCount': "{{$room['room']['dayCount']}}",
                'displayId': "{{$displayId}}",
                'passedpartnerId': $('#passedpartnerId').val(),
                'isBookCheck': true
            }, function (data) {
                var checkroomPrice = data.netPrice;
                $('.ladda-button-book').ladda().ladda('stop');
                if (data.hasOwnProperty('success') && !data["success"]) {
                    if ("type" in data && data['type'] === "SESSION_TIMEOUT") {
                        swal({
                            title: "{{trans('book.loginExpired')}}",
                            type: "warning",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "{{trans('book.login')}}",
                        }, function () {
                            window.location.href = "{{url('/')}}";
                        });
                    }
                } else if (data.errorId === "ER1401" || data.errorId === "ER1301") {
                    swal({
                        title: "{{trans('book.soldout')}}",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK",
                    }, function () {
                        window.location.href = "{{url('/search')}}";

                    });
                } else if (data["errorId"] === "ER1413") {
                    swal({
                        title: "{{trans('book.limit')}}",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK",
                    }, function () {
                        window.location.href = "{{url('/search')}}";

                    });
                } else if (data["errorId"] === "ER1411" || data["errorId"] === "ER1402") {
                    swal({
                        title: "{{trans('book.pchange')}}",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK",
                    }, function () {
                        window.location.href = "{{url('/search')}}";

                    });
                } else if (data["errorId"] === "ER1004") {
                    swal({
                        title: "{{trans('book.timeout')}}",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK",
                    }, function () {
                        window.location.href = "{{url('/search')}}";

                    });
                } else if (data["errorId"] === "ER1501") {
                    swal({
                        title: "{{trans('book.soldout')}}",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK",
                    }, function () {
                        window.location.href = "{{url('/search')}}";

                    });
                } else {
                    var roomCount = data.length;
                    var html = "";
                    var html2 = "";
                    var cp = "";
                    //房间已定完
                    if (data.length === 0) {
                        cp = "{{trans('book.soldout')}}";
                        swal({
                            title: "{{trans('book.soldout')}}",
                            type: "warning",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "OK",
                        }, function () {
                            window.location.href = "{{url('/search')}}";
                        });
                    } else {
                        $.each(data, function (i, field) { // makes no sense to loop as we currently can only serve one room type
                            var count = 0;
                            if (field.cancellationPolicies && field.cancellationPolicies.length > 0) {
                                value = 0;
                                $.each(field.cancellationPolicies, function (j, cancel) {
                                    if (cancel.amount === 0 && count === 0) {
                                        value = 1;
                                        //	html += "{{trans('book.freeb')}} "+ cancel.end.substr(0,19).replace(/T/g," ") + "{{trans('book.ltime')}}<br>";
                                        cp = "{{trans('book.freeb')}} " + cancel.end.substr(0, 19).replace(/T/g, " ") + "{{trans('book.ltime')}}<br>";
                                        count++;
                                    }
                                    else {
                                        cp = "{{trans('book.non-ref')}}";
                                    }
                                });
                                if (0 === value) {
                                    cp = "{{trans('book.non-ref')}}";
                                }
                            } else {
                                cp = "{{trans('book.non-ref')}}";
                            }
                            document.getElementById("roomRef").value = field.roomReference;

                            if (field.comments && field.comments[0] && field.comments[0].comments && field.comments[0].comments != undefined) {
                                html2 += field.comments[0].comments;
                            } else {
                                <?php
                                    if(array_key_exists(
                                    "comments",
                                    $room['room']
                                ) && (!empty($room['room']['comments']))) {
                                    foreach($room['room']['comments'] as $comment ){
                                    ?>
                                  html2 += $comment['comments'];
                                <?php
                                    }
                                    }else{
                                    ?>
                                  html2 += "none";
                                <?php
                                }
                                ?>
                            }

                            if (field.rateClass === '可取消') {
                                $('#ccPaymentLabel').removeClass('ccPaymentLabel');
                            }
                            var res = '';
                            if (field.netPrice) {
                                temp = parseInt(<?=$room['room']['roomCount']?>);
                                temp2 = field.netPrice;
                                temp1 = parseInt(temp2 / temp);
                                res = parseInt(temp1 * temp);
                                $(".roomPrice").text(field.beforeTax / temp / '<?=$room['room']['dayCount']?>');
                                $("#roomTax").text(field.tax);
                                $(".totalPrice").text(res);

                                $('#book-form').append('<input type="hidden" name="totalPrice"  value="' + res + '"/>');
                            } else {
                                temp = parseInt(<?=$room['room']['roomCount']?>);
                                temp1 = '<?=ceil($room['room']['netPrice'] * 100 / $room['room']['roomCount'] / $room['room']['dayCount']) / 100.0 ?>';

                                res = temp1 * temp;
                                res = parseInt(res);
                                $(".roomPrice").text(temp1);
                                $(".totalPrice").text(res);
                                $('#book-form').append('<input type="hidden" name="totalPrice"  value="' + res + '"/>');
                            }

                            totalPrice = res;
                        });

                        $('.applied-coupon-block').removeClass('hide');
                        $('#couponAmount').html('-$' + coupon_amount);
                        $('.totalPrice').text(totalPrice - coupon_amount);
                    }

                    html += cp;

                    $("#hotel-cancel").html(html);
                    $("#hotel-comment").html(html2);
                    var leftheight = $('.book-check-left').height() + 60;
                    $('.book-check-right').css('height', leftheight);
                }
            });

        }

        function buildRating (rating, category) {
            var r = Number(rating);
            if (r == NaN) {
                return category;
            }
            if (r >= 5) {
                r = 5
            }
            var h = '';
            for (var i = 0; i < r; i++) {
                h += '<span class="entypo-star"></span>'
            }

            return h;
        }

        function bookRooms () {
            var temp = 'po';
            var radio = document.getElementsByName("imgsel");
            for (i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    temp = radio[i].value;
                }
            }
            var ggg = $('#book-form');
            //	 $('#book-form').append('<input type="hidden" name="tempType"  value="' + temp + '"/>');
            ggg.append('<input type="hidden" name="tempType"  value="' + temp + '"/>');

            var r = $("#book-form").valid();

            if (!r) {
                swal("{{trans('book.plzcomplete')}}");
                return;
            }

            preventClick();
            if (window.location.href.indexOf("missedOrder") > -1) {
                if (is117book) {
                    var url = "{{url('/missed-order/reserve/business/'.$sessionKey . '/' . $Reorderid.'?rooms_key='.$rooms_key)}}";
                } else {
                    var url = "{{url('/reserve/sales/missed-order/'.$sessionKey . '/' . $Reorderid.'?rooms_key='.$rooms_key)}}";
                }
            } else {
                var url = "{{url('/reserve/sales/'.$sessionKey . '/' . $Reorderid.'?rooms_key='.$rooms_key)}}";
            }
            // Find disabled inputs, and remove the "disabled" attribute
            var disabled = ggg.find(':input:disabled').removeAttr('disabled');
            // serialize the form
            var serialized = ggg.serialize();
            $.post(url, serialized, function (data) {
                checkOrderStatus(data);
            });
            disabled.attr('disabled','disabled');
        }

        function checkOrderStatus (data) {
            if (data.success && data.voucherUrl) {
                window.location.replace(data.voucherUrl);
            }else if (data === '') {
              swal({
                title: '已成功预订，但是上游信息有误，我们已经通知技术，稍后发送预订成功邮件给客户,请备注谢谢',
                type: 'success',
                confirmButtonColor: '#DD6B55',
                allowOutsideClick: false,
                confirmButtonText: 'OK',
              }, function() {
                window.location.href = "{{url('/affiliate/fraud-list')}}";
              });
            }  else {
                ableClick();
                if (data.errorId === "ER1401" || data.errorId === "ER1301") {
                    swal({
                        title: "上游报错:房间售罄或者酒店无人confirm，请告知客人,并回到酒店详情页拒单退款",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "返回酒店详情页",
                    }, function () {
                        window.location.href = "{{url('/hotel/missed/reorder').'/'.$displayId.'/'.$Reorderid}}";
                    });
                } else if (data["errorId"] === "ER1413") {
                    swal("{{trans('book.limit')}}", data.message, "error");
                }else if (data["errorId"] === "FL1001") {
                  swal({
                    title: '订购失败,请拒单退款',
                    type: "success",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "ok",
                  }, function () {
                    window.location.href = "{{url('/affiliate/fraud-list')}}";
                  });
                } else if (data["errorId"] === "EP1001") {
                    swal({
                        title: '已经成功发送酒店额外付款链接邮件',
                        type: "success",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "返回后台等待客户的额外付款结果或者查看订购是否失败",
                    }, function () {
                        window.location.href = "{{url('/affiliate/fraud-list')}}";
                    });
                } else if (data["errorId"] === "EMAIL106") { //c端的citcon补差价
                    swal({
                        title: "已经成功发送酒店额外付款链接邮件给550和各位国内销售，请根据邮件内容继续下单",
                        type: "success",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "返回后台等待客户的付款结果",
                    }, function () {
                        window.location.href = "{{url('/affiliate/missed-lists')}}";
                    });
                } else if (data["errorId"] === "EXTRAPAY") { //b端的citcon补差价
                    swal({
                        title: "已经成功发送酒店额外付款链接邮件给客人，请联系财务查看补款情况下单",
                        type: "success",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "返回后台等待客户的付款结果",
                    }, function () {
                        window.location.href = "{{url('/admin/missed-lists')}}";
                    });
                } else if (data["errorId"] === "ER1411" || data["errorId"] === "ER1402" || data["errorId"] === "ER1302") {
                    swal({
                        title: "{{trans('book.pchange')}}",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{trans('book.backtoSearch')}}",
                    }, function () {
                        window.location.href = "{{url('/search')}}";
                    });
                } else if (data["errorId"] === "ER1501") {
                    swal({
                        title: "{{trans('book.soldout')}}",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{trans('book.backtoSearch')}}",
                    }, function () {
                        window.location.href = "{{url('/search')}}";
                    });
                } else if (data["errorId"] === "ER1414") {
                    swal({
                        text: "{{trans('book.duplicate')}}".replace('xxx', data['errorMessage']),
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{trans('book.backtoSearch')}}",
                    }, function () {
                        window.location.href = "{{url('/search')}}";
                    });
                } else if (data["errorId"] === "ER1004") {
                    swal({
                        title: "{{trans('book.timeout')}}",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{trans('book.backtoSearch')}}",
                    }, function () {
                        window.location.href = "{{url('/search')}}";
                    });
                } else if (data["errorId"] === "IT1001") {
                    swal({
                        title: "{{trans('book.cnetwork')}}",
                        type: "warning",
                        text: data.message,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{trans('book.backtoSearch')}}",
                    }, function () {
                        window.location.href = "{{url('/search')}}";
                    });
                } else if (data['errorId'] === 'ER1415') { // for duplicate order check
                    swal({
                        title: "可能是重复订单，请勿关闭该窗口，询问用户是否继续下单后选择继续或取消",
                        type: 'warning',
                        confirmButtonText: "{{trans('book.confirm')}}",
                        showCancelButton: true,
                        cancelButtonText: "{{ trans('book.cancel') }}",
                    }, function (confirmed) {
                        if (confirmed) {
                            preventClick();
                            $.post('/duplicate/proceed', {
                                '_token': $('meta[name="csrf_token"]').attr('content'),
                                'action': 'continue',
                                'Reorderid': data.Reorderid,
                                'vendor': 'cc',
                                'p_id': data.p_id,
                            }, function (data) {
                                checkOrderStatus(data);
                            });
                        } else {
                            $.post('/duplicate/proceed', {
                                '_token': $('meta[name="csrf_token"]').attr('content'),
                                'action': 'cancel',
                                'Reorderid': data.Reorderid,
                                'vendor': 'cc',
                                'p_id': data.p_id,
                            }, function (data) {
                                if (!data.success) {
                                    data['message'] = '{{ trans('book.failed') }}';
                                }

                                swal({
                                    title: data.message,
                                    type: 'warning',
                                    confirmButtonText: "OK",
                                }, function () {
                                    window.location.href = '/';
                                });
                            });
                        }
                    });
                } else {
                    swal({
                        title: "{{trans('book.cfailed')}}",
                        text: data.message,
                        type: "error",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{trans('search.backtoSearch')}}",
                    }, function () {
                        window.location.href = "{{url('/search')}}";
                    });
                }
            }
        }

        function preventClick () {
            $('.ladda-button-book').ladda().ladda('start');
            $('#openProcessingModal').click();
            document.getElementById("bt").disabled = true;
        }

        function ableClick () {
            $('.ladda-button-book').ladda().ladda('stop');
            $('#closeProcessingModal').click();
            document.getElementById("bt").disabled = false;
        }
    </script>
@endsection
