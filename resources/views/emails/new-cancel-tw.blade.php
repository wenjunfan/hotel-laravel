<div  style="margin: 0 auto; width:40%; min-width:320px;  font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
	<div>
		<div style="text-align: left; margin-bottom:25px" >
			@if($is_aa)
				<img style="margin-top:50px; margin-bottom:30px;" src="http://hotel.supervacation.net/img/partnerImgs/aaLogo.png"/>
			@else
				<img src="https://hotel.usitour.com/img/general/logo/logo-usitour-tw.png"/>
			@endif
			<div style="padding-top:10px;padding-bottom:10px;border-radius:0px; text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500; margin-bottom:30px; text-align:center">
				您的訂房已經取消成功
			</div>
		</div>
		<p style="font-size:16px;">
			您好,
		</p>

		<p style="font-size:15px;">
			您的訂房 (訂單號: {{$bookingId}})已經取消成功!
		</p>

		<p style="line-height:25px; font-size:15px;">如果您在預訂時使用了信用卡支付,退款預計會在3-7個工作日內退還到您使用的卡上(取決於不同銀行的結算時間)
			<br/> 如果您在預訂時使用了微信或支付寶支付,退款根據網絡狀況會在一天內發放;
		<p style="border-top: 1px solid lightgrey; font-size:15px;"></p>
		<p style="margin-bottom:15px; font-size:15px;">
			訂房信息
		</p>
		<p style="font-size:13px;">
			<b style="font-size:13px;">{{$hotelName}}</b>
		</p>
		<p style="font-size:13px;">
		{{$address}}
		<p style="font-size:13px;">
			入住日期：
			<b style="font-size:13px;">{{$checkIn}}</b> &nbsp;
			退房日期：
			<b style="font-size:13px;">{{$checkOut}}</b>
		</p>
		<p style="font-size:13px;">
			確認號 : <b style="font-size:13px;">{{$orderRef}}</b>
		</p>
		<p style="border-top: 1px solid lightgrey"></p>

		<p style="margin-bottom:15px; font-size:15px">
			預訂信息
		</p>
		<table style="font-size:13px; line-height:30px; margin-left:-3px;">
			<tr>
				<td style="vertical-align: top;">
					主入住人：
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$guest}}</b>
					<p>{{$adultCount}} 成人, {{$childCount}}兒童</p>
				</td>
			</tr>
			<tr>
				<td>
					房型:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$roomType}}</b>
				</td>
			</tr>
			<tr>
				<td>
					早餐類型:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$boardName}}</b>
				</td>
			</tr>
			<tr>
				<td>
					房間數 :
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$roomNumber}}</b>
				</td>
			</tr>
			<tr>
				<td>
					取消政策:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$cancellation}}</b>
				</td>
			</tr>
			<tr>
				<td>
					總價格:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$totalPrice}}</b>
				</td>
			</tr>
		</table>
		<p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
		<p style="font-size:15px;">此郵件僅用作取消預訂的確認,不可將此作為退款憑證等其他任何用途.如有其它問題,也請隨時聯繫我們</p>
		<br/>
		@include('emails.footer-tw')
	</div>
</div>
