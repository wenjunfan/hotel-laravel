<div style="margin: 0 auto; width:40%; min-width:320px; font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
	<div>
		<div style="text-align: left; margin-bottom:25px" >
			<img style="height:50px;margin-top:50px; margin-bottom:30px;" src="http://www.117book.com/img/landing/Logo-EN.png" />
			<div style="padding-top:10px;padding-bottom:10px;border-radius:0px; text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500; margin-bottom:30px; text-align:center">
				Your hotel reservation has been successfully canceled!
			</div>
		</div>
		<p style="font-size:16px;">
			Hello {{$partnerName}},
		</p>

		<p style="font-size:15px;">
			Your hotel reservation (Booking # {{$bookingId}}) has been successfully canceled!
		</p>

		<p style="line-height:25px; font-size:15px;">If you used a credit card at the time of booking, the amount paid is refunded to your card. It may take 3-5 business days to appear depending on the card company involved;
			<br/> If you used Alipay at the time of booking, the amount paid will be refunded to your Alipay account as soon as possible after the financial review by our accountant.
			<br/> For our Postpay members, if this order is not paid yet, the order amount will not be added into your next bill; if the order has been paid, then the amount will be refunded in the next bill.</p>
		<p style="border-top: 1px solid lightgrey; font-size:15px;"></p>
		<p style="margin-bottom:15px; font-size:15px;">
			Hotel Overview
		</p>
		<p style="font-size:13px;">
			<b style="font-size:13px;">{{$hotelName}}</b>
		</p>
		<p style="font-size:13px;">
		{{$address}}
		<p style="font-size:13px;">
			Check in:
			<b style="font-size:13px;">{{$checkIn}}</b> &nbsp;
			check out:
			<b style="font-size:13px;">{{$checkOut}}</b>
		</p>
		<p style="font-size:13px;">
			Reference #: <b style="font-size:13px;">{{$orderRef}}</b>
		</p>
		<p style="border-top: 1px solid lightgrey"></p>

		<p style="margin-bottom:15px; font-size:15px">
			Booking Overview
		</p>
		<table style="font-size:13px; line-height:30px; margin-left:-3px;">
			<tr>
				<td style="vertical-align: top;">
					Main Guest:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$guest}}</b>
					<p>{{$adultCount}}adult， {{$childCount}}child</p>
				</td>
			</tr>
			<tr>
				<td>
					Room Type:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$roomType}}</b>
				</td>
			</tr>
			<tr>
				<td>
					Board Type:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$boardName}}</b>
				</td>
			</tr>
			<tr>
				<td>
					Room:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$roomNumber}}</b>
				</td>
			</tr>
			<tr>
				<td>
					Cancellation Policy:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$cancellation}}</b>
				</td>
			</tr>
			<tr>
				<td>
					Total Price:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$totalPrice}}</b>
				</td>
			</tr>
		</table>
		<p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
		<p STYLE="font-size:15px;">This email is for booking cancellation confirmation only, and cannot be used as a proof of refund or any other purposes. If you have any questions, please do not hesitate to contact us.</p>
		<br/>
		<p style="margin-bottom:30px;">Thank you,<br/>
			www.117Book.com</p>
		<small style="color:#787878">17870 Castleton St. Suite 358-388</small><br/>
		<small style="color:#787878">City of Industry, CA 91748</small><br/>
		<small style="color:#787878">Phone: 626-522-2906  / 626-434-5267  </small><br/>
		<small style="color:#787878">Email:bd@117book.com</small><br/>
		<p style="color:#787878; font-size:8px;">This email was sent from a notification-only email address. Please do not reply.</p>
	</div>
</div>
