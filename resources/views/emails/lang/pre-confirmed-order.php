<?php

$lang['hello'][0] = "您好,";
$lang['reservation'][0] = '您的酒店预订<b style="color:#f1791e">成功，我们正在为您准备voucher</b>，稍后将为您提供含酒店凭证的预订确认邮件。';
$lang['hotelInfo'][0] = "酒店信息";
$lang['checkIn'][0] = "入住日期";
$lang['checkOut'][0] = "退房日期";
$lang['bookInfo'][0] = "预订信息";
$lang['mainGuest'][0] = "主入住人：";
$lang['pax'][0] = ":adult 成人，:child 儿童";
$lang['room'][0] = "房型:";
$lang['board'][0] = "早餐类型:";
$lang['roomCount'][0] = "房间数:";
$lang['cancelPolicy'][0] = "取消政策:";
$lang['total'][0] = "总价格:";
$lang['notes'][0] = "此邮件仅用作预订处理中确认，不可将此作为酒店凭证等其他任何用途。如有其他问题，也请随时联系我们。";
$lang['phone'][0] = "电话: 626-898-7800 / 626-434-5267";
$lang['email'][0] = "邮箱";
$lang['notice'][0] = "此邮件由系统自动发出，请勿直接回复。";

$lang['hello'][1] = "Hello,";
$lang['reservation'][1] = 'Thanks! Your hotel reservation<b style="color:#f1791e"> is Confirmed. We are preparing Voucher for your</b>, and will sent another Confirmation Email contains voucher to you later.';
$lang['hotelInfo'][1] = "Hotel Overview";
$lang['checkIn'][1] = "Check in";
$lang['checkOut'][1] = "Check out";
$lang['bookInfo'][1] = "Booking Overview";
$lang['mainGuest'][1] = "Main Guest:";
$lang['pax'][1] = ":adult adult， :child child";
$lang['room'][1] = "Room Type:";
$lang['board'][1] = "Board Type:";
$lang['roomCount'][1] = "Room:";
$lang['cancelPolicy'][1] = "Cancellation Policy:";
$lang['total'][1] = "Total Price:";
$lang['notes'][1] = "This email is for booking confirmation only, and cannot be used as a hotel voucher or any other purposes. If you have any questions, please do not hesitate to contact us.";
$lang['phone'][1] = "Phone: 626-898-7658 / 626-434-5267";
$lang['email'][1] = "Email";
$lang['notice'][1] = "This email was sent from a notification-only email address. Please do not reply.";
