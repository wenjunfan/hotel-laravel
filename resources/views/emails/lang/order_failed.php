<?php

$lang['hello'][0] = "尊敬的客人， 您的订单";
$lang['reserveFailed'][0] = "预订失败。如有疑问，请联系117book客服。";
$lang['notice'][0] = "此邮件由系统自动发出，请勿直接回复。";

$lang['hello'][1] = "Dear Customer, your order";
$lang['reserveFailed'][1] = "has been rejected. Please contact our 117book customer representative for more information.";
$lang['notice'][1] = "This email was sent from a notification-only email address. Please do not reply.";