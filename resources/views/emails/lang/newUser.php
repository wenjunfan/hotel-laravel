<?php

$lang['hello'][0] = ":user 您好,";
$lang['subject'][0] = "感谢您成为我们的会员! 我们在48小时内会有客服人员协助您激活账号.";
$lang['subject2'][0] = "等待的同时，也请贵司完善账户信息并准备以下材料:";
$lang['list1'][0] = "1. 签署在附件中的用户协议,了解内容后, 请在英文正本12&18页签字.";
$lang['list2'][0] = "2. 新用户在协议最后的支付方式只需填写常用信用卡的持卡人名字即可, 其他可选择填写.";
$lang['list3'][0] = "3. 提供营业执照和TAX ID.";
$lang['login'][0] = "立即登录";
$lang['title'][0] = "下面让我们一起了解117Book预订的乐趣所在";
$lang['feature1'][0] = "- 简洁页面 流畅预订";
$lang['feature2'][0] = "- 货比多家 即时比价";
$lang['feature3'][0] = "- 支付灵活 快捷方便";
$lang['feature4'][0] = "- 专业团队 为您服务";
$lang['feature5'][0] = "- 贴心功能 实时高效";
$lang['welcome'][0] = "业务上如需配合的地方欢迎随时联系我们，十分期待与您的合作！";
$lang['notice'][0] = "此邮件由系统自动发出，请勿直接回复。";

$lang['hello'][1] = "Hello :user,";
$lang['subject'][1] = "Thank you for joining 117book! Our customer support team will be in contact within 48 hours to help you for the activation.";
$lang['subject2'][1] = "Meanwhile, please complete your account information and provide the following documents:";
$lang['list1'][1] = "1. Please review and sign the User Agreement attached";
$lang['list2'][1] = "2. Please provide frequently-used cardholder’s name in the payment section of the User Agreement.";
$lang['list3'][1] = "3. Please provide a copy of your Business License and company’s Tax ID";
$lang['login'][1] = "log In";
$lang['title'][1] = "Here are the benefits you can enjoy with 117book";
$lang['feature1'][1] = "- User-friendly Booking System";
$lang['feature2'][1] = "- Real-time Fetching Best Price";
$lang['feature3'][1] = "- Convenient & Secure Payment Process";
$lang['feature4'][1] = "- Dedicated Team Ready On Your Side";
$lang['feature5'][1] = "- Thoughtful Functions with High Performance";
$lang['welcome'][1] = "You are welcome to contact us anytime for your business needs. We look forward to working with you!";
$lang['notice'][1] = "This email was sent from a notification-only email address. Please do not reply.";
