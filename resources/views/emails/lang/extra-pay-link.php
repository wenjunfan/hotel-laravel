<?php

$lang['hello'][0] = "尊敬的用户您好,";
$lang['title'][0] = "请点击下方按钮，补差价，完成预订。";
$lang['link'][0] = "补差价下单链接";
$lang['copy'][0] = "(如果上面的链接无法点击,您可以复制:";
$lang['paste'][0] = "并粘帖到浏览器的地址栏中访问)";
$lang['book'][0] = "您将在补差价页面看到新的酒店预订详细信息，如果确认无误，可以付款直接下单。谢谢。";
$lang['notes'][0] = "此邮件仅用作预订請求确认，不可将此作为酒店凭证等其他任何用途。如有其他问题，也请随时联系我们。";
$lang['phone'][0] = "电话: 626-898-7800 / 626-434-5267";
$lang['email'][0] = "邮箱";
$lang['notice'][0] = "此邮件由系统自动发出，请勿直接回复。";

$lang['hello'][1] = "Dear Customer, ";
$lang['title'][1] = "Please click below button to make extra payment for your reservation. ";
$lang['link'][1] = "Extra Pay Link";
$lang['copy'][1] = "(If clicking the link above does not work, please copy:";
$lang['paste'][1] = 'and paste the URL in a new browser window instead.)';
$lang['book'][1] = 'You will find your new booking detail on the extra pay page.';
$lang['notes'][1] = "This email is for booking confirmation only, and cannot be used as a hotel voucher or any other purposes. If you have any questions, please do not hesitate to contact us.";
$lang['phone'][1] = "Phone: 626-898-7658 / 626-434-5267";
$lang['email'][1] = "Email";
$lang['notice'][1] = "This email was sent from a notification-only email address. Please do not reply.";

$lang['hello'][2] = "尊敬的用戶您好,";
$lang['title'][2] = "請點擊下方按鈕，補差價，完成預訂。";
$lang['link'][2] = "補差價下單鏈接";
$lang['copy'][2] = "(如果上面的鏈接無法點擊,您可以復制:";
$lang['paste'][2] = "並粘帖到瀏覽器的地址欄中訪問)";
$lang['book'][2] = "您將在補差價頁面看到新的飯店預訂詳細信息，如果確認無誤，可以付款直接下單。謝謝。";
$lang['notes'][2] = "此郵件僅用作預訂額外付款請求確認，不可將此作為飯店憑證等其他任何用途。如有其他問題，也請隨時聯系我們。";
$lang['phone'][2] = "24小時專線: +888 887 2816 (美) / +886-2-5594-1486 (臺)";
$lang['email'][2] = "Email";
$lang['notice'][2] = "此郵件為系統自動發送，請勿直接回復";
