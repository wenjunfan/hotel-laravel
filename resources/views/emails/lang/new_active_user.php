<?php

$lang['hello'][0] = ':user 您好';
$lang['content'][0] = '恭喜您, 您的帐户已经被激活, 您现在可以查看酒店价格和预订了.';
$lang['pay'][0] = '117book支持信用卡、Paypal，微信、支付宝等支付方式，我们也支持Postpay.';
$lang['login'][0] = '点击登录';
$lang['notice'][0] = '此邮件为系统自动发送，请勿直接回复';

$lang['hello'][1] = 'Congratulations! :user';
$lang['content'][1] = 'Your account already has been activated. You may now log onto this site 117book.com to access many of its wonderful features. 117book has partner with 420,000 hotels worldwide. Please log to check it out. To log in simply enter the user name and password which you selected during your registration process.';
$lang['pay'][1] = 'We accept credit card, Paypal, WechatPay, Alipay and also Postpay.';
$lang['login'][1] = 'Click Here to Login';
$lang['notice'][1] = 'This email was sent from a notification-only email address. Please do not reply.';
