<?php

$lang['hello'][0] = "您的酒店预订入住人姓名已经修改成功";
$lang['hello2'][0] = "您好,";
$lang['reservation'][0] = "您的酒店预订 (订单号: :id)修改入住人姓名成功，以下是新的入住凭证。";
$lang['voucher'][0] = "入住凭证";
$lang['notes'][0] = "此邮件仅用作修改入住人的确认,不可将此作为其他任何用途.如有其它问题,也请随时联系我们.";
$lang['phone'][0] = "电话: 626-898-7800 / 626-434-5267";
$lang['email'][0] = "邮箱";
$lang['notice'][0] = "此邮件由系统自动发出，请勿直接回复。";

$lang['hello'][1] = "Your hotel reservation has successfully amend the guest name!";
$lang['hello2'][1] = "Hello,";
$lang['reservation'][1] = "Your hotel reservation (Booking # :id) has successfully amend the guest name, below is your new voucher!";
$lang['voucher'][1] = "Voucher";
$lang['notes'][1] = "This email is for booking guest name amendment only, and cannot be used as any other purposes. If you have any questions, please do not hesitate to contact us.";
$lang['phone'][1] = "Phone: 626-898-7658 / 626-434-5267";
$lang['email'][1] = "Email";
$lang['notice'][1] = "This email was sent from a notification-only email address. Please do not reply.";

$lang['hello'][2] = "您的飯店預定入住人姓名已經修改成功";
$lang['hello2'][2] = "您好,";
$lang['reservation'][2] = "您的飯店預訂(訂單號: :id)修改入住人成功,一下是新的入住憑證。";
$lang['voucher'][2] = "入住憑證";
$lang['notes'][2] = "此郵件僅用於修改飯店入住人姓名的確認,不可將次作為其他任何用途,如有其他問題也請聯係我們.";
$lang['phone'][2] = "24小時專線: +888 887 2816 (美) / +886-2-5594-1486 (臺)";
$lang['email'][2] = "Email";
$lang['notice'][2] = "此郵件為系統自動發送，請勿直接回復";
