<?php

$lang['hello'][0] = "尊敬的客人， 您的订单";
$lang['reserveFailed'][0] = "预订失败. 如有疑问，请联系:company客服。";
$lang['notes'][0] = "此邮件由系统自动发出，请勿直接回复。";

$lang['hello'][1] = "Dear Customer， Your order";
$lang['reserveFailed'][1] = "has been rejected. Please contact our customer representative for more information.";
$lang['notes'][1] = "This email was sent from a notification-only email address. Please do not reply.";

$lang['hello'][2] = "尊敬的客人， 您的訂單";
$lang['reserveFailed'][2] = "預訂失敗. 如有疑問，請聯系:company客服。";
$lang['notes'][2] = "此郵件由系統自動發出，請勿直接回復。";
