<?php
include resource_path('views/emails/lang/newOrder.php');
$langId = isset($langId) ? $langId : 0;
?>
<div style="margin: 0 auto; width:40%; min-width:320px; font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
	<div>
		<div style="text-align: left; margin-bottom:25px" >
				<img alt="logo" style="height:50px;margin-top:50px;  margin-bottom:30px;"
						src="@if($langId == 0) http://www.117book.com/img/landing/Logo-CN.png @else http://www.117book.com/img/landing/Logo-EN.png @endif"/>
		</div>
		<p style="font-size:16px;">
			{{ str_replace(':user', $partner, $lang['hello'][$langId]) }},
		</p>

		<p style="margin-bottom:30px;">
			{{ $lang['reservation'][$langId] }}<b style="color:#f1791e">{{$status}}</b>！{{ $lang['orderId'][$langId] }}:<b>{{$orderId}} </b>
		</p>
		<a type="button" href="{{$voucher}}" class="btn btn-primary" style="padding:5px 10px; border-radius:4px;text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500;">
			{{ $lang['voucher'][$langId] }}
		</a>
		<p style="margin-top:35px; border-top: 1px solid lightgrey"></p>
		<p style="margin-bottom:15px; font-size:15px;">
			{{ $lang['hotelInfo'][$langId] }}
		</p>
		<p style="font-size:13px;">
			<b style="font-size:13px;">{{$hotelName}}</b>
		</p>
		<p style="font-size:13px;">
		{{$hotelAddress}}
		</P>
		<p style="font-size:13px;">
			{{ $lang['checkIn'][$langId] }}：
			<b style="font-size:13px;">{{$checkin}}</b> &nbsp;
			{{ $lang['checkOut'][$langId] }}：
			<b style="font-size:13px;">{{$checkout}}</b>
		</p>
		<p style="font-size:13px;">
			{{ $lang['reference'][$langId] }}: <b style="font-size:13px;">{{$reference}}</b>
		</p>
		<p style="border-top: 1px solid lightgrey"></p>

		<p style="margin-bottom:15px; font-size:15px">
			{{ $lang['bookingInfo'][$langId] }}
		</p>
		<table style="font-size:13px; line-height:30px; margin-left:-3px;">
			<tr>
				<td style="vertical-align: top;">
					{{ $lang['mainGuest'][$langId] }}：
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$guest}}</b>
					<p>{{$adNumber}} {{ $lang['adult'][$langId] }}， {{$chNumber}} {{ $lang['child'][$langId] }}</p>
				</td>
			</tr>
			<tr>
				<td>
					{{ $lang['room'][$langId] }}:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$roomType}}</b>
				</td>
			</tr>
			<tr>
				<td>
					{{ $lang['board'][$langId] }}:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$boardType}}</b>
				</td>
			</tr>
			<tr>
				<td>
					{{ $lang['roomCount'][$langId] }}:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$roomCount}}</b>
				</td>
			</tr>
			<tr>
				<td>
					{{ $lang['cancelPolicy'][$langId] }}:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$cancellation}}</b>
				</td>
			</tr>
			<tr>
				<td>
					{{ $lang['total'][$langId] }}:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$totalPrice}}</b>
				</td>
			</tr>
		</table>
		<p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
		<p style="font-size:15px;">{{ $lang['notes'][$langId] }}</p>
		<br/>
		<p style="margin-bottom:30px;">Thank you,<br/>
				www.117Book.com</p>
				<small style="color:#787878">17870 Castleton St. Suite 358-388</small><br/>
				<small style="color:#787878">City of Industry, CA 91748</small><br/>
				<small style="color:#787878">Phone: 626-522-2906 / 626-434-5267 / 626-389-0705 </small><br/>
				<p style="color:#787878; font-size:8px;">{{ $lang['notice'][$langId] }}</p>
	</div>
</div>
