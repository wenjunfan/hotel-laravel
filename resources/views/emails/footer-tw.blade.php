@if($is_aa)
    <p style="margin-bottom:30px;">Thank you,<br/>www.supervacation.net</p>
    <small style="color:#787878">117 E Garvey Ave., </small><br/>
    <small style="color:#787878">Monterey Park, CA 91755 U.S.A</small><br/>
    <small style="color:#787878">總公司電話：626-571-2988/免費電話：1-888-628-2871/訂位電話：626-607-0170</small><br/>
    <p style="color:#787878; font-size:8px;">此郵件為系統自動發送,請勿直接回復</p>
@else
    <table style="width: 100%;margin-bottom:30px;">
        <tbody>
        <tr>
            <td style="width: 70%;">
                <p>Thank you,<br/>www.usitrip.com</p>
            </td>
            <td style="width: 10%" align="right">
                <a href="http://www.facebook.com/usitripofficial">
                    <img src="http://www.117book.com/img/email/facebook.png" alt="usitour facebook" width="44">
                </a>
            </td>
            <td style="width: 10%" align="right">
                <a href="http://nav.cx/iakRO6J">
                    <img src="http://www.117book.com/img/email/line.png" alt="usitour line" width="44">
                </a>
            </td>
            <td style="width: 10%" align="right">
                <a href="http://www.instagram.com/usitrip_official/">
                    <img src="http://www.117book.com/img/email/instagram.png" alt="usitour instagram" width="44">
                </a>
            </td>
        </tr>
        </tbody>
    </table>
    <small style="color:#787878">17870 Castleton St. Suite 358-388</small><br/>
    <small style="color:#787878">City of Industry, CA 91748</small><br/>
    <small style="color:#787878">24小時專線: +888 887 2816 (美) / +886-2-5594-1486 (臺) </small><br/>
    <small style="color:#787878">Email: service@usitour.com</small><br/>
    <p style="color:#787878; font-size:8px;">此郵件為系統自動發送，請勿直接回復</p>
@endif