<?php
include resource_path('views/emails/lang/affiliate_newUser.php');
$langId = isset($langId) ? $langId : 0;
$hostName = isset($domain) ? $domain : 'Usitrip';
$host = 'hotel.usitrip.com';
if(strpos(strtolower($hostName), 'usitour') !== false) {
    $host = 'hotel.usitour.com';
} elseif (strpos(strtolower($hostName), '117book') !== false) {
    $host = 'www.117book.com';
}
$logoUrl = 'https://s3-us-west-1.amazonaws.com/usitrip/mail/img/logo.png';
$affiliateUrl = 'https://s3-us-west-1.amazonaws.com/usitrip/mail/img/renren_cn.jpg';
$langStr = 'cn';
if ($langId == 1) {
    $logoUrl = 'https://s3-us-west-1.amazonaws.com/usitrip/mail/img/en_img2.png';
    $langStr = 'en';
} elseif ($langId == 2) {
    $logoUrl = 'https://hotel.usitour.com/img/general/logo/logo-usitour-tw.png';
    $affiliateUrl = 'https://s3-us-west-1.amazonaws.com/usitrip/mail/img/renren_en.jpg';
    $langStr = 'tw';
}
?>
<!DOCTYPE html>
<html lang="zh-Hans">
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>{{ $lang['title'][$langId] }}</title>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet"/>
</head>
<body class="" style="background-color: rgb(230,230,230); font-family: '微软雅黑',Arial, Helvetica, sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="font-size:18px; color:#1a1a1a; border-collapse: separate; mso-table-lspace: 0; mso-table-rspace: 0; background-color: rgb(230,230,230); width: 100%;" width="100%" bgcolor="rgb(230,230,230)">
    <tr>
        <td>
            <!--content-->
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600px;" style="overflow:hidden; border-collapse: collapse; background-color:white; margin-top:20px; margin-bottom:20px;">
                <tr>
                    <td>
                        <!--main_content-->
                        <table border="0" cellpadding="0" cellspacing="0" width="560px;" style="border-collapse: collapse; background-color:white; margin-top:20px; margin-bottom:20px; margin-left:20px; margin-right:20px;">
                            <!--top-->
                            <tr>
                                <td>
                                    <p style="margin-bottom:0; font-size:18px !important;">{{ str_replace(':domain', $domain, $lang['thanks'][$langId]) }}</p>
                                </td>
                            </tr>

                            <!--logo&contact-->
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="560px;" style="border-collapse: collapse; background-color:white; margin-top:40px; margin-bottom:60px;">
                                        <tr>
                                            <!--logo-->
                                            <td width="354px;">
                                                <a href="{{ (strpos($host, '117book') == false ? 'https://' : 'http://') . $host . '/?lang=' . $langStr }}" style="display:block;" target="_blank" title="{{ $domain }}">
                                                    <img src="{{ $logoUrl }}" alt="Usitrip logo" title="Usitrip logo" style="border: none; -ms-interpolation-mode: bicubic; max-width: 100%; margin-top: 10px;"/>
                                                </a>
                                                <p style="font-size:18px !important; margin-top:0; margin-bottom:0; color:#0080ed;">{{ $lang['saveMoney'][$langId] }}</p>
                                            </td>
                                            <!--contact-->
                                            <td>
                                                <p style="font-size:18px !important; text-align:right; margin-top:20px; margin-bottom:0;">{{ $lang['service'][$langId] }}</p>
                                                <p style="font-size:18px !important; text-align:right; margin-top:0; margin-bottom:0;">{{ $lang['phone1'][$langId] }}</p>
                                                <p style="font-size:18px !important; text-align:right; margin-top:0; margin-bottom:0;">{{ $lang['phone2'][$langId] }}</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <p style="width: 600px; height:200px; margin:0;padding:0;background-image:url({{ $affiliateUrl }});"> </p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <p style="margin-top:40px;">{{ $lang['congratulation'][$langId] }}</p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <a style="color:#2681ff; text-decoration:underline;" href="{{$link}}" title="{{$link}}">
                                        {{$link}}
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <p>{{ $lang['description'][$langId] }}</p>
                                </td>
                            </tr>

                            <!--special offer-->
                            @if($langId == 0)
                                <tr>
                                    <td>
                                        <a href="https://hotel.usitrip.com/" target="_blank" style="display:block; margin-bottom:20px; text-decoration:none; color:#1a1a1a;" title="https://hotel.usitrip.com/">
                                            <img src="https://s3-us-west-1.amazonaws.com/usitrip/mail/img/special_offer.png" alt="special_offer"/>
                                        </a>
                                    </td>
                                </tr>
                        @endif

                        <!--最省钱-->
                            <tr>
                                <td>
                                    <p class="best_saving" style="font-weight: bold; font-size:24px; color:#0080ed; text-align:center; margin-top:0;">{{ $lang['saveMoney'][$langId] }}</p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <p class="service_pc" style="text-align: center; margin-top:0;">
                                        <img src="https://s3-us-west-1.amazonaws.com/usitrip/mail/img/service-01.png" alt="hotline"/>
                                    </p>
                                </td>
                            </tr>

                            <!--二维码-->
                            <tr>
                                <td>
                                    <!--3 scans-->
                                    @if($langId == 0)
                                        <table class="scan" style="border-collapse: separate; mso-table-lspace: 0px; mso-table-rspace: 0px; width: 440px; margin-left:60px; margin-right:60px; margin-top:20px; margin-bottom:20px;" border="0" cellpadding="0" cellspacing="0" width="520px">
                                            <tr>
                                                <td style="vertical-align: top; text-align: center;" valign="top">
                                                    <img src="https://s3-us-west-1.amazonaws.com/usitrip/mail/img/wechat1.png" alt="微信订阅号"/>
                                                    <p class="san_type" style="margin-top:0; margin-bottom:0; text-align:center; font-size:18px;">微信订阅号</p>
                                                </td>

                                                <td style="vertical-align: top; text-align: center;" valign="top">
                                                    <img src="https://s3-us-west-1.amazonaws.com/usitrip/mail/img/weibo1.png" alt="微博二维码"/>
                                                    <p class="san_type" style="margin-top:0; margin-bottom:0; text-align:center; font-size:18px;">微博二维码</p>
                                                </td>

                                                <td style="vertical-align: top; text-align:center;" valign="top">
                                                    <img src="https://s3-us-west-1.amazonaws.com/usitrip/mail/img/facebook1.png" alt="Facebook"/>
                                                    <p class="san_type" style="margin-top:0; margin-bottom:0; text-align:center; font-size:18px;">Facebook</p>
                                                </td>
                                            </tr>
                                        </table>
                                    @elseif($langId == 2)
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td style="width: 10%" align="right">
                                                    <a href="http://www.facebook.com/usitour">
                                                        <img src="http://www.117book.com/img/email/facebook.png" alt="usitour facebook" width="44">
                                                    </a>
                                                </td>
                                                <td style="width: 10%" align="right">
                                                    <a href="http://nav.cx/iakRO6J">
                                                        <img src="http://www.117book.com/img/email/line.png" alt="usitour line" width="44">
                                                    </a>
                                                </td>
                                                <td style="width: 10%" align="right">
                                                    <a href="http://www.instagram.com/usitour_official/">
                                                        <img src="http://www.117book.com/img/email/instagram.png" alt="usitour instagram" width="44">
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    @endif
                                </td>
                            </tr>

                            <!-- end main content  -->
                            <!-- signature  -->
                            <tr>
                                <td style="font-size: 18px; vertical-align: top; text-align:center;" valign="top">
                                    <p style="margin-top:20px; margin-bottom:0;">{{ $host }} | {{ $lang['companyHotel'][$langId] }}</p>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 14px; vertical-align: top; text-align:center;" valign="top">
                                    <p style="margin-top:10px;margin-bottom:0;">Copyright &copy; 2008-{{\Carbon\Carbon::now()->year}} {{ $host }}</p>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 14px; vertical-align: top; text-align:center;" valign="top">
                                    <p style="margin-top:5px; margin-bottom: 40px;">{{ $lang['companyName'][$langId] }} All rights reserved</p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <p style="margin-top:0; margin-bottom:0; padding-top:20px; text-align: center; font-size:14px; border-top:1px dashed #636363;">{{ $lang['notice'][$langId] }}</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>