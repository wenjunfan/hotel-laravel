<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Welcome to Usitrip</title>
    <!--[if mso]>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif !important;
        }
    </style>
    <![endif]-->
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet"/>
    <!--<![endif]-->
    <style>
        @media only screen and (max-width: 620px) {
            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important;
            }

            table[class=body] .content {
                padding: 0 !important;
            }

            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important;
            }

            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }

            table[class=body] .btn table {
                width: 100% !important;
            }

            table[class=body] .btn a {
                width: 100% !important;
            }

            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
        }

        @media all {
            .ExternalClass {
                width: 100%;
            }

            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }
        }
    </style>
</head>
<body class="" style="background-color: #ffffff; font-family: Arial, Helvetica, sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="font-size:18px; color:#1a1a1a; border-collapse: separate; mso-table-lspace: 0; mso-table-rspace: 0; background-color: rgb(230,230,230); width: 100%;" width="100%" bgcolor="rgb(230,230,230)">
    <tbody><tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600px;" style="overflow:hidden; border-collapse: collapse; background-color:white; margin-top:20px; margin-bottom:20px;">
                <tbody><tr>
                    <td>
                        <div style="text-align: left; margin-bottom:25px">
                            <img alt="logo" style="height:50px;margin-top:50px; margin-left:20px;" src="http://www.117book.com/img/landing/Logo-EN.png">
                        </div>
                        <table border="0" cellpadding="0" cellspacing="0" width="560px;" style="border-collapse: collapse; background-color:white; margin:20px;">
                            <tbody>
                            <tr>
                                <td>
                                    <p style="margin-top:0; margin-bottom:0; font-size:24px;">Dear {{$fullName}}: </p>
                                    <p style="margin-top:40px; margin-bottom:0; font-size:18px; font-weight: bold;">You had submitted a new group hotel request! </p>
                                    <p style="margin-bottom:0; font-size:18px; font-weight: bold;">The request information as blow:</p>
                                    <p style="border-top:#aaaaaa 1px dashed;"></p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Destination: {{$destinationOne}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Check-in: {{$checkinOne}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Check-out: {{$checkoutOne}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Second Destination (Optional): {{$destinationTwo}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Second Check-in (Optional): {{$checkinTwo}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Second Check-out (Optional): {{$checkoutTwo}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Group Code (Optional): {{$groupCode}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Rooms Per Night (9+): {{$roomsPerNight}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Ideal Star Rating: {{$starRating}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Ideal Nightly Budget: {{$nightBudget}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Full Name: {{$fullName}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Company Name (Optional): {{$companyName}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Email: {{$email}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Phone: {{$phone}}</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px; color: #666666">Additional Comments/Requests (Optional):{{$comments}} </p>
                                    <p style="border-top:#aaaaaa 1px dashed;"></p>
                                    <p style="margin-top:40px; margin-bottom:0;color:#ea8523; font-size:18px;">Our Customer Representative will be in touch soon,</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px;">Best,</p>
                                    <p style="margin-top:10px; margin-bottom:0; font-size:18px;">117book.com Team</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 18px; vertical-align: top; text-align:center;" valign="top">
                                    <p style="margin-top:40px; margin-bottom:0;">117book.com</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 14px; vertical-align: top; text-align:center;" valign="top">
                                    <p style="margin-top:10px;margin-bottom:0;">Copyright © <span style="border-bottom:1px dashed #ccc;z-index:1"  onclick="return false;" data="2008-2018">2008-2019</span> 117book.com</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 14px; vertical-align: top; text-align:center;" valign="top">
                                    <p style="margin-top:5px; margin-bottom: 40px;">117book.com All rights reserved</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p style="margin-top:0; margin-bottom:0; padding-top:20px; text-align: center; font-size:14px; border-top:1px dashed #636363;">
                                        this is an automatically generated email, please do not reply.
                                    </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody></table>
</body>
</html>