<?php
include resource_path('views/emails/lang/name-change.php');
$langId = isset($langId) ? $langId : 0;
$hostName = isset($hostName) ? $hostName : 'Usitrip';
$logoUrl = 'http://www.117book.com/img/email/Email-Logo.png';
if ($langId == 1) {
    $logoUrl = 'http://www.117book.com/img/email/Email-Logo-usitrip-en.png';
} elseif ($langId == 2) {
    $logoUrl = 'https://hotel.usitour.com/img/general/logo/logo-usitour-tw.png';
}
$email = "service@usitrip.com";
if ($hostName === 'Usitour') {
    $email = 'service@usitour.com';
} elseif ($hostName === '117book') {
    $email = "bd@117book.com";
}
?>
<div style="margin: 0 auto; width:40%; min-width:320px;  font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
    <div>
        <div style="text-align: left; margin-bottom:25px">
            <img style="margin-top:50px; margin-bottom:30px;" src={{$logoUrl}}/>
            <p style="padding-top:10px;padding-bottom:10px;border-radius:0px; text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500; margin-bottom:30px; text-align:center">{{ $lang['hello'][$langId] }}</p>
        </div>
        <p style="font-size:16px;">{{ $lang['hello2'][$langId] }}</p>
        <p style="font-size:15px;">{{ str_replace(':id', $id, $lang['reservation'][$langId]) }}</p>
        <a href="http://{{$url}}">{{ $lang['voucher'][$langId] }}</a>
        <p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
        <p STYLE="font-size:15px;">{{ $lang['notes'][$langId] }}</p> <br/>
        <p style="margin-bottom:30px;">Thank you,<br/> UnitedStars International Ltd</p>
        <small style="color:#787878">17870 Castleton St. Suite 358-388</small>
        <br/>
        <small style="color:#787878">City of Industry, CA 91748</small>
        <br/>
        <small style="color:#787878">{{ $lang['phone'][$langId] }}</small>
        <br/>
        <small style="color:#787878">{{ $lang['email'][$langId] }}: {{ $email }}</small>
        <br/>
        <p style="color:#787878; font-size:8px;">{{ $lang['notice'][$langId] }}</p>
    </div>
</div>
