<div style="margin: 0 auto; width:40%; min-width:320px;  font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
	<div>
		<div style="text-align: left; margin-bottom:25px" >
			<img style="margin-top:50px; margin-bottom:30px;" src="{{$img}}" />
			<div style="padding-top:10px;padding-bottom:10px;border-radius:0px; text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500; margin-bottom:30px; text-align:center">
				您的酒店预订经过商务部处理，已经成功取消(全部或部分)
			</div>
		</div>
		<p style="font-size:16px;">
			{{$partnerName}} 您好,
		</p>
		<p style="font-size:15px;">
			您的酒店预订 (订单号: {{$bookingId}}) 已经取消成功 ！
		</p>
		<p style="border-top: 1px solid lightgrey; font-size:15px;"></p>
		<p style="margin-bottom:15px; font-size:15px;">
			酒店信息
		</p>
		<p style="font-size:15px;">
			酒店名称： {{$hotelName}}
		</p>
		<p style="font-size:15px;">
			Reference#： {{$orderRef}}
		</p>
		<p style="font-size:15px;">
			订单金额： {{$totalPrice}}
		</p>
		<p style="font-size:15px;">
			支付方式： {{$paymentType}}
		</p>
		<p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
		<p style="font-size:15px;">此邮件仅用作取消预订的确认,不可将此作为退款凭证等其他任何用途.如有其它问题,也请随时联系我们.</p>
		<br/>
		<p style="margin-bottom:30px;">Thank you,<br/>
			{{$httpHost}}</p>
		<small style="color:#787878">17870 Castleton St. Suite 358-388</small><br/>
		<small style="color:#787878">City of Industry, CA 91748</small><br/>
		<small style="color:#787878">电话: 626-434-5267 </small><br/>
		<small style="color:#787878">邮箱: service@usitrip.com/ bd@117book.com/ service@usitour.com</small><br/>
		<p style="color:#787878; font-size:8px;">此邮件为系统自动发送，请勿直接回复.</p>
	</div>
</div>