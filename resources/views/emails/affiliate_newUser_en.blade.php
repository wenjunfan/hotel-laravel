<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Welcome to {{$domain}}</title>

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet"/>
</head>
<body class="" style="background-color: rgb(230,230,230); font-family: '微软雅黑',Arial, Helvetica, sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="font-size:18px; color:#484848; border-collapse: separate; mso-table-lspace: 0; mso-table-rspace: 0; background-color: rgb(230,230,230); width: 100%;" width="100%" bgcolor="rgb(230,230,230)">
    <tr>
        <td>
            <!--content-->
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600px;" style="overflow:hidden; border-collapse: collapse; background-color:white; margin-top:20px; margin-bottom:20px;">
                <tr>
                    <td>
                        <!--top_content-->
                        <table border="0" cellpadding="0" cellspacing="0" width="600px;" style="border-collapse: collapse; background-color:white;">
                            <tr>
                                <td>
                                    <p style="color:#2681ff; font-size:12px;margin-left:20px;">Yay! Welcome to {{$domain}}!</p>
                                </td>

                                <td>
                                    <p style="color:#2681ff; font-size:12px;margin-right:20px;text-align:right;">Contact us:+1 626-898-7658</p>
                                </td>
                            </tr>
                        </table>
                        <hr style="margin:0;">
                        <!--main_content-->
                        <table border="0" cellpadding="0" cellspacing="0" width="560px;" style="border-collapse: collapse; background-color:white; margin-top:20px; margin-bottom:20px; margin-left:20px; margin-right:20px;">
                            <!--logo-->
                            <tr>
                                <td>
                                    <a href="http://www.{{$domain}}.com" style="display:block; width:260px; height:78px; margin-left:150px; background-image:url('https://s3-us-west-1.amazonaws.com/usitrip/mail/img/en_img2.png'); background-position:0 0; margin-top:20px; margin-bottom:40px; " target="_blank" title="Usitour logo"> </a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <p style="width: 600px; height:200px; margin:0;padding:0;background-image:url('https://s3-us-west-1.amazonaws.com/usitrip/mail/img/renren_en.jpg');"> </p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <p style="margin-top:40px;"> Congratulations! Your qualification has been approved. Please click the following link to learn how to start earning commissions. </p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <a style="color:#2681ff; text-decoration:underline;" href="{{$link}}" title="{{$link}}">{{$link}}</a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <p> You will receive your "affiliate link" after the application. Start to earn your first 5% commission (excluding tax)! </p>
                                </td>
                            </tr>

                            <!--help-->
                            <tr>
                                <td>
                                    <p style="text-align:center; font-size:24px; margin-top:40px; margin-bottom:0;">Need Help?</p>
                                </td>

                            </tr>

                            <tr>
                                <td>
                                    <p style="text-align:center; margin-top:0;">Please contact us<br>to
                                        <span style="text-decoration:underline;">+1 626-989-7658</span><br> or
                                        <span style="text-decoration:underline;">service@usitour.com</span></p>
                                </td>
                            </tr>

                            <!--social icons-->
                            <tr>
                                <td>
                                    <a href="https://www.facebook.com/usitour" style="display:inline-block; width:45px; height:45px;  background-image:url('https://s3-us-west-1.amazonaws.com/usitrip/mail/img/en_img2.png'); background-position:-180px -100px; margin-left:100px; margin-top:20px; " target="_blank" title="https://www.facebook.com/usitour"> </a>
                                    <a href="https://twitter.com/OfficialUsitour" style="display:inline-block; width:45px; height:45px;  background-image:url('https://s3-us-west-1.amazonaws.com/usitrip/mail/img/en_img2.png'); background-position:-260px -100px; margin-left:60px; margin-top:20px; " target="_blank" title="https://twitter.com/OfficialUsitour"> </a>
                                    <a href="https://www.instagram.com/usitour_official/" style="display:inline-block; width:45px; height:45px;  background-image:url('https://s3-us-west-1.amazonaws.com/usitrip/mail/img/en_img2.png'); background-position:-180px -180px; margin-left:60px; margin-top:20px;" target="_blank" title="https://www.instagram.com/usitour_official/"> </a>
                                    <a href="https://www.pinterest.com/UsiTour/" style="display:inline-block; width:45px; height:45px;  background-image:url('https://s3-us-west-1.amazonaws.com/usitrip/mail/img/en_img2.png'); background-position:-260px -180px; margin-left:60px; margin-top:20px;" target="_blank" title="https://www.pinterest.com/UsiTour/"> </a>
                                </td>
                            </tr>

                            <!--link-->
                            <tr>
                                <td>
                                    <a href="https://hotel.usitour.com/affiliate/login" style="display:inline-block; margin-left:56px; width:118px; text-align:center; color:#2681ff; height:30px; line-height:30px; text-decoration:none; margin-top:40px;  border-right:1px solid #c3c3c3;" target="_blank" title="https://hotel.usitour.com/affiliate/login">Login</a>
                                    <a href="https://www.usitour.com/about_us.html" style="display:inline-block; width:91px; text-align:center; color:#2681ff; height:30px; line-height:30px; text-decoration:none; margin-top:40px;  border-right:1px solid #c3c3c3;" target="_blank" title="https://www.usitour.com/about_us.html">About Us</a>
                                    <a href="https://www.usitour.com/privacy_policy.html" style="display:inline-block; width:128px; text-align:center; color:#2681ff; height:30px; line-height:30px; text-decoration:none; margin-top:40px; border-right:1px solid #c3c3c3;" target="_blank" title="https://www.usitour.com/privacy_policy.html">Privacy Policy</a>
                                    <a href="https://www.usitour.com/faq_question.html" style="display:inline-block; width:104px; text-align:center; color:#2681ff; height:30px; line-height:30px; text-decoration:none; margin-top:40px;" target="_blank" title="https://www.usitour.com/faq_question.html">Help/FAQs</a>
                                </td>
                            </tr>
                        </table>
                        <hr style="margin:0;">
                        <!--top_content-->
                        <table border="0" cellpadding="0" cellspacing="0" width="600px;" style="border-collapse: collapse; background-color:white;">
                            <tr>
                                <td>
                                    <p style="text-align:center;">Copyright &copy; 2008-{{\Carbon\Carbon::now()->year}} {{$domain}}.com | All Rights Reserved</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>