<?php
include resource_path('views/emails/lang/order_failed.php');
$langId = isset($langId) ? $langId : 1;
?>
<div width="100%" style="
                        font-family:-apple-system,BlinkMacSystemFont, Helvetica Neue, Microsoft YaHei, PingFang SC, Microsoft YaHei, Source Han Sans SC, Noto Sans CJK SC, WenQuanYi Micro Hei, sans-serif !important;
                        font-weight:300;
                        color: #999999;
                        background-color:#ffffff;
                        margin:0 !important;
                        padding:0 !important;">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
        <tr>
            <td align="center" width="100%">
                <table width="700" align="center" cellspacing="0" cellpadding="0" border="0" style="background-color:#ffffff;margin:0 auto;padding:0 auto;border:#e4e5ea 1px solid;color:#333333;font-size:16px;">
                    <tbody>
                    {{--header--}}
                    <tr>
                        <td style="padding-left:25px;padding-right:25px; padding-bottom:10px;">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td width="144" height="80" style="vertical-align:middle;width:144px;height:80px;padding-top:15px;padding-bottom:6px">
                                        <img src="@if($langId == 0) http://www.117book.com/img/landing/Logo-CN.png @else http://www.117book.com/img/landing/Logo-EN.png @endif" alt="">
                                    </td>
                                    <td style="vertical-align:middle;text-align:center;padding-top:15px;padding-left:20px;color: #666666;font-size:18px;font-weight:300;text-decoration: none;">

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    {{--/header--}}
                    {{--mail content--}}
                    <tr>
                        <td style="padding:50px 25px;
            background-color:#ffffff;
            text-align: center;
            font-size: 16px;
            line-height: 26px;
            font-family:-apple-system,BlinkMacSystemFont, Helvetica Neue, Microsoft YaHei, PingFang SC, Microsoft YaHei, Source Han Sans SC, Noto Sans CJK SC, WenQuanYi Micro Hei, sans-serif !important;
            font-weight:300;">
                            {{ $lang['hello'][$langId] }}<span style="color:deepskyblue">{{ $Reorderid }} </span>{{ $lang['reserveFailed'][$langId] }}
                        </td>
                    </tr>

                    {{--footer--}}
                    <tr style="background-color:#f6f7fa">
                        <td style="padding-left:20px;padding-top:20px;padding-bottom:20px;font-size:12px;font-weight: bold;font-family:-apple-system,BlinkMacSystemFont, Helvetica Neue, Microsoft YaHei, PingFang SC, Microsoft YaHei, Source Han Sans SC, Noto Sans CJK SC, WenQuanYi Micro Hei, sans-serif !important;
                      ">{{ $lang['notice'][$langId] }}</td>
                    </tr>
                    {{--/footer--}}
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>