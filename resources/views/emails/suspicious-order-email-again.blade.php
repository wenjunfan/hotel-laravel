<?php
include resource_path('views/emails/lang/suspicious-order-email.php');
$langId = isset($langId) ? $langId : 0;
$hostName = isset($hostName) ? $hostName : 'Usitrip';
$logoUrl = 'http://www.117book.com/img/email/Email-Logo.png';
if ($langId == 1) {
    $logoUrl = 'http://www.117book.com/img/email/Email-Logo-usitrip-en.png';
} elseif ($langId == 2) {
    $logoUrl = 'https://hotel.usitour.com/img/general/logo/logo-usitour-tw.png';
}
$email = "service@usitrip.com";
if ($hostName === 'Usitour') {
    $email = 'service@usitour.com';
    $logoUrl = 'https://hotel.usitrip.com/img/email/Email-Logo-en.png';
} elseif ($hostName === '117book') {
    $email = "bd@117book.com";
}
?>
<div style="margin: 0 auto; width:40%; min-width:320px; font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
    <div>
        <div>
            <img style="margin-top:50px; margin-bottom:30px;" src="{{ $logoUrl }}"/>
        </div>
        <p style="font-size:16px;">
            {{ str_replace(':user', $bookerInfo->firstname. ' ' .$bookerInfo->lastname, $lang['hello'][$langId]) }}
        </p>
        <p style="margin-bottom:30px;">{!! $lang['reservation'][$langId] !!}</p>
        <img style="height:100px;" src="@if($langId == 0)http://www.117book.com/img/general/fraud/DemoCn.jpg @else http://www.117book.com/img/general/fraud/DemoEn.jpg @endif"/>
        <br/><br/>
        <a type="button" href="https://hotel.usitrip.com/{{$data->hotelId}}/{{$searchedInfo->checkin}}/{{$searchedInfo->checkout}}.html" class="btn btn-primary" style="padding:5px 10px; border-radius:4px;text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500;">{{ $lang['checkHotel'][$langId] }}</a>
        <br/><br/>
        <a type="button" href="https://hotel.usitrip.com/file-upload/{{$data->Reorderid}}" class="btn btn-success" style="padding:5px 10px; border-radius:4px;text-decoration:none; background: #005bac !important; border-color: #005bac; color: #fff !important; font-size:15px; font-weight:500;">{{ $lang['upload'][$langId] }}</a>
        <p style="margin-top:35px; border-top: 1px solid lightgrey"></p>
        <p style="margin-bottom:15px; font-size:15px;">{{ $lang['cardNum'][$langId] }}: <b style="color:#f1791e">{{substr($cc->number,-4)}}</b>
        </p>
        <p style="margin-bottom:15px; font-size:15px;">{{ $lang['hotelInfo'][$langId] }}</p>
        <p style="font-size:13px;">
            <b style="font-size:13px;">{{$tem->goods_name}}</b></p>
        <p style="font-size:13px;">{{ $lang['checkIn'][$langId] }}: <b style="font-size:13px;">{{$searchedInfo->checkin}}</b>
            {{ $lang['checkOut'][$langId] }}: <b style="font-size:13px;">{{$searchedInfo->checkout}}</b></p>
        <p style="font-size:13px;">{{ $lang['tempOrder'][$langId] }}: <b style="font-size:13px;">{{$data->Reorderid}}</b></p>
        <p style="border-top: 1px solid lightgrey"></p>
        <p style="margin-bottom:15px; font-size:15px">{{ $lang['bookInfo'][$langId] }}</p>
        <table style="font-size:13px; line-height:30px; margin-left:-3px;">
            <tr>
                <td style="vertical-align: top;">{{ $lang['mainGuest'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td>
                    <b>{{$bookInfo->paxes[0]->name.' '.$bookInfo->paxes[0]->surname}}</b></td>
            </tr>
            <tr>
                <td>{{ $lang['room'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>{{$bookInfo->roomDetails->name}}</b></td>
            </tr>
            <tr>
                <td>{{ $lang['board'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>{{$bookInfo->roomDetails->boardName}}</b></td>
            </tr>
            <tr>
                <td>{{ $lang['roomCount'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>{{$bookInfo->roomDetails->roomCount}}</b></td>
            </tr>
            <tr>
                <td>{{ $lang['cancelPolicy'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>@if($bookInfo->roomDetails->cancelDate != '')
                            {{str_replace("T", " ", mb_substr($bookInfo->roomDetails->cancelDate, 0, 19))}}
                        @else
                            Non-Refundable
                        @endif
                    </b></td>
            </tr>
            <tr>
                <td>{{ $lang['total'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td>
                    <b>{{$bookInfo->roomDetails->netPrice.' '.$bookInfo->roomDetails->currency}}</b></td>
            </tr>
        </table>
        <p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
        <p style="font-size:15px;">{{ $lang['notes'][$langId] }}</p> <br/>
        <p style="margin-bottom:30px;">Thank you,<br/>
            @if(isset($domain)) {{ $domain }}@elseif($hostName == 'Usitour') www.usitour.com @else www.usitrip.com @endif</p>
        <small style="color:#787878">17870 Castleton St. Suite 358-388</small>
        <br/>
        <small style="color:#787878">City of Industry, CA 91748</small>
        <br/>
        <small style="color:#787878">{{ $lang['phone'][$langId] }}</small>
        <br/>
        <small style="color:#787878">{{ $lang['email'][$langId] }}: {{ $email }}</small>
        <br/>
        <p style="color:#787878; font-size:8px;">{{ $lang['notice'][$langId] }}</p>
    </div>
</div>