<div width="100%" style="
                        font-family:-apple-system,BlinkMacSystemFont, Helvetica Neue, Microsoft YaHei, PingFang SC, Microsoft YaHei, Source Han Sans SC, Noto Sans CJK SC, WenQuanYi Micro Hei, sans-serif !important;
                        font-weight:300;
                        color: #999999;
                        background-color:#ffffff;
                        margin:20px !important;
                        padding:0 !important;">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tbody>
			<tr>
				<td align="center" width="100%">
					<table width="700" align="center" cellspacing="0" cellpadding="0"
							border="0"
							style="background-color:#ffffff;margin:0 auto;padding:0 auto;border:#e4e5ea 1px solid;color:#333333;font-size:16px;">
						<tbody>
							<tr style="background-color:#f6f7fa">
								<td style="padding-left:25px;padding-right:25px; padding-bottom:10px;">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tbody>
											<tr>
												<td width="144" height="80"
														style="vertical-align:middle;width:144px;height:80px;padding-top:15px;padding-bottom:6px">
													@if($is_aa)
														<img style="margin-top:50px; margin-bottom:30px;" src="http://hotel.supervacation.net/img/partnerImgs/aaLogo.png"/>
													@else
														<img style="margin-top:50px; margin-bottom:30px;" src="http://www.117book.com/img/email/Email-Logo.png"/>
													@endif
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="padding:25px 25px;
            background-color:#ffffff;
            text-align: center;
            font-size: 16px;
            line-height: 26px;
            font-family:-apple-system,BlinkMacSystemFont, Helvetica Neue, Microsoft YaHei, PingFang SC, Microsoft YaHei, Source Han Sans SC, Noto Sans CJK SC, WenQuanYi Micro Hei, sans-serif !important;
            font-weight:300;">
									<p style="font-size:16px;">
										Hi All：<br/>
										有新的待查看订单！
									</p>
									<p>请去
										<a href="@if($is_aa) http://hotel.supervacation.net/affiliate/fraud-list @else https://hotel.usitrip.com/affiliate/fraud-list @endif">
											@if($is_aa) 美亚 @else 走四方 @endif 酒店页的后台
										</a>查看订单详情!</p>
								</td>
							</tr>
							<tr>
								<td style="padding:25px 25px;
            background-color:#ffffff;
            text-align: center;
            font-size: 16px;
            line-height: 26px;
            font-family:-apple-system,BlinkMacSystemFont, Helvetica Neue, Microsoft YaHei, PingFang SC, Microsoft YaHei, Source Han Sans SC, Noto Sans CJK SC, WenQuanYi Micro Hei, sans-serif !important;
            font-weight:300;">
									查看用戶的上传文件<a href="{{$data['fileUrl']}}" target="_blank" class="btn" style="padding:5px 10px; border-radius:4px;text-decoration:none; background: deepskyblue !important; border-color: deepskyblue; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500;">链接</a>
								</td>
							</tr>
							<tr>
								<td style="padding:25px 25px;
            background-color:#ffffff;
            text-align: center;
            font-size: 16px;
            line-height: 26px;
            font-family:-apple-system,BlinkMacSystemFont, Helvetica Neue, Microsoft YaHei, PingFang SC, Microsoft YaHei, Source Han Sans SC, Noto Sans CJK SC, WenQuanYi Micro Hei, sans-serif !important;
            font-weight:300;">
									去酒店详情页<a href="{{$data['reorderUrl']}}" target="_blank" class="btn" style="padding:5px 10px; border-radius:4px;text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500;">{{$data['reorderUrl']}}</a>处理订单
								</td>
							</tr>
							<tr style="background-color:#f6f7fa">
								<td style="padding-left:20px;padding-top:20px;padding-bottom:20px;font-size:12px;font-weight: bold;font-family:-apple-system,BlinkMacSystemFont, Helvetica Neue, Microsoft YaHei, PingFang SC, Microsoft YaHei, Source Han Sans SC, Noto Sans CJK SC, WenQuanYi Micro Hei, sans-serif !important;
                      ">
									此邮件由系统自动发出，请勿直接回复。
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>
