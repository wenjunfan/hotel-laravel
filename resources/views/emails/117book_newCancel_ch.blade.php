

<div style="margin: 0 auto; width:40%; min-width:320px;  font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
	<div>
		<div style="text-align: left; margin-bottom:25px" >
			<img style="height:50px;margin-top:50px; margin-bottom:30px;" src="http://www.117book.com/img/landing/Logo-CN.png" />
			<div style="padding-top:10px;padding-bottom:10px;border-radius:0px; text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500; margin-bottom:30px; text-align:center">
				您的酒店预订已经取消成功
			</div>
		</div>
		<p style="font-size:16px;">
			{{$partnerName}} 您好,
		</p>

		<p style="font-size:15px;">
			您的酒店预订 (订单号: {{$bookingId}})已经取消成功 ！
		</p>

		<p style="line-height:25px; font-size:15px;">如果您在预订时使用了信用卡支付,退款预计会在3-5个工作日内退还到您使用的卡上(取决于不同银行的结算时间);
			<br/> 如果您在预订时使用了支付宝支付,退款会尽快在财务审核后退还到您的支付宝账户里;
			<br/> 如果您是Postpay用户,若该订单未支付,则该订单金额将不计入下一次的结算账单；若该订单已支付,则在下一次账单中退还.</p>
		<p style="border-top: 1px solid lightgrey; font-size:15px;"></p>
		<p style="margin-bottom:15px; font-size:15px;">
			酒店信息
		</p>
		<p style="font-size:13px;">
			<b style="font-size:13px;">{{$hotelName}}</b>
		</p>
		<p style="font-size:13px;">
		{{$address}}
		<p style="font-size:13px;">
			入住日期：
			<b style="font-size:13px;">{{$checkIn}}</b> &nbsp;
			退房日期：
			<b style="font-size:13px;">{{$checkOut}}</b>
		</p>
		<p style="font-size:13px;">
			确认号: <b style="font-size:13px;">{{$orderRef}}</b>
		</p>
		<p style="border-top: 1px solid lightgrey"></p>

		<p style="margin-bottom:15px; font-size:15px">
			预订信息
		</p>
		<table style="font-size:13px; line-height:30px; margin-left:-3px;">
			<tr>
				<td style="vertical-align: top;">
					主入住人：
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$guest}}</b>
					<p>{{$adultCount}} 成人, {{$childCount}}儿童</p>
				</td>
			</tr>
			<tr>
				<td>
					房型:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$roomType}}</b>
				</td>
			</tr>
			<tr>
				<td>
					早餐类型:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$boardName}}</b>
				</td>
			</tr>
			<tr>
				<td>
					房间数:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$roomNumber}}</b>
				</td>
			</tr>
			<tr>
				<td>
					取消政策:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$cancellation}}</b>
				</td>
			</tr>
			<tr>
				<td>
					总价格:
				</td>
				<td style="width:30px;"></td>
				<td>
					<b>{{$totalPrice}}</b>
				</td>
			</tr>
		</table>
		<p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
		<p STYLE="font-size:15px;">此邮件仅用作取消预订的确认,不可将此作为退款凭证等其他任何用途.如有其它问题,也请随时联系我们.</p>
		<br/>
		<p style="margin-bottom:30px;">Thank you,<br/>
			www.117Book.com</p>
		<small style="color:#787878">17870 Castleton St. Suite 358-388</small><br/>
		<small style="color:#787878">City of Industry, CA 91748</small><br/>
		<small style="color:#787878">Phone: 626-522-2906  / 626-434-5267  </small><br/>
		<small style="color:#787878">Email:bd@117book.com</small><br/>
		<p style="color:#787878; font-size:8px;">此邮件为系统自动发送，请勿直接回复.</p>
	</div>
</div>
