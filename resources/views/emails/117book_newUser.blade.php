<?php
include resource_path('views/emails/lang/newUser.php');
$langId = isset($langId) ? $langId : 0;
?>
<div class="sizeofcontent" style="margin: 0 auto; width:40%; min-width:320px;  font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
    <div>
        <div>
            <div style="text-align: left; margin-bottom:25px">
                <img alt="logo" style="height:50px;margin-top:50px;  margin-bottom:30px;"
                        src="@if($langId == 0) http://www.117book.com/img/landing/Logo-CN.png @else http://www.117book.com/img/landing/Logo-EN.png @endif"/>
            </div>
            <p style="font-size:16px;">{{str_replace(':user', $NewUserName, $lang['hello'][$langId])}}</p>
            <p style="padding-bottom:10px;">{{$lang['subject'][$langId]}}</p>
            <p>{{$lang['subject2'][$langId]}}</p>
            <p>{{$lang['list1'][$langId]}}</p>
            <p style="line-height:25px;">{{$lang['list2'][$langId]}}</p>
            <p style="margin-bottom:30px;">{{$lang['list3'][$langId]}}</p>
            <a type="button" href="http://www.117book.com/login" class="btn" style="padding:8px 10px; border-radius:4px; text-decoration:none; background:#f1791e !important; border-color:transparent; color:#fff !important; margin-left:0px; font-size:16px; font-weight:500; margin-bottom:55px; ">{{$lang['login'][$langId]}}</a>
            <p style="margin-top:30px;margin-bottom:30px;">{{$lang['title'][$langId]}}</p>
            <p style="line-height:10px;">{{$lang['feature1'][$langId]}}</p>
            <p style="line-height:10px;">{{$lang['feature2'][$langId]}}</p>
            <p style="line-height:10px;">{{$lang['feature3'][$langId]}}</p>
            <p style="line-height:10px;">{{$lang['feature4'][$langId]}}</p>
            <p style="line-height:10px;">{{$lang['feature5'][$langId]}}</p>
            <p style="margin-top:25px; margin-bottom:20px;">{{$lang['welcome'][$langId]}}</p>
            <p style="margin-bottom:30px;">Thank you,<br/> www.117book.com</p>
            <small style="color:#787878">17870 Castleton St. Suite 358-388</small>
            <br/>
            <small style="color:#787878">City of Industry, CA 91748</small>
            <br/>
            <small style="color:#787878">Phone: 626-522-2906 / 626-434-5267 / 626-389-0705</small>
            <br/>
            <small style="color:#787878">Email:bd@117book.com</small>
            <br/> <br/> <p style="color:#787878; font-size:8px;">{{$lang['notice'][$langId]}}</p>
        </div>
    </div>
</div>