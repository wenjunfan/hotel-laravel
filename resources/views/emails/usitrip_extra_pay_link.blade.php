<div style="margin: 0 auto; width:40%; min-width:320px; font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
	<div>
		<div style="text-align: left; margin-bottom:25px" >
			<img src="http://www.117book.com/img/email/Email-Logo.png"/>
		</div>
		<p style="font-size:16px;">
			hello,
		</p>
		<p>{{$sales_no}}正在处理微信额外付款订单: {{$temp_id}}，酒店：{{$hotelName}}， 客人需要补差价{{$leftToPay}}.</p>
		<p>
			请点击下方按钮,生成二维码，发送链接给客人{{$bookerEmail}}, 或者致电{{$bookerPhone}}
		</p>
		<a type="button" href="{{str_replace('https//','',$url)}}" class="btn btn-primary" style="padding:5px 10px; border-radius:4px;text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500;">补差价下单链接
		</a>
		<p>(如果上面的链接无法点击,您可以复制:<span style="color:red; font-size:10px;">{{$url}}</span>, 并粘帖到浏览器的地址栏中访问)</p>
		<br/><br/>
		请同财务确认补差价完成继续下单，或者客人希望拒单返款，请退回未完订单拒单。
		<p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
		<p style="font-size:15px;">如有任何疑问，请联系550。</p>
		<br/>
		<p style="margin-bottom:30px;">Thank you,<br/>
			www.usitrip.com</p>
		<small style="color:#787878">17870 Castleton St. Suite 358-388</small><br/>
		<small style="color:#787878">City of Industry, CA 91748</small><br/>
		<small style="color:#787878">电话: 626-898-7800  /  626-434-5267 </small><br/>
		<small style="color:#787878">邮箱: service@usitrip.com</small><br/>
		<p style="color:#787878; font-size:8px;">此邮件为系统自动发送，请勿直接回复</p>
	</div>
</div>