<table border="0" cellpadding="0" cellspacing="0" class="body" style="font-size:18px; color:#1a1a1a; border-collapse: separate; mso-table-lspace: 0; mso-table-rspace: 0; background-color: rgb(230,230,230); width: 100%;" width="100%" bgcolor="rgb(230,230,230)">
	<tbody>
	<tr>
		<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" width="600px;" style="overflow:hidden; border-collapse: collapse; background-color:white; margin-top:20px; margin-bottom:20px;">
				<tbody>
				<tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" width="560px;" style="border-collapse: collapse; background-color:white; margin-top:20px; margin-bottom:20px; margin-left:20px; margin-right:20px;">
							<tbody>
							<tr>
								<td>
									<p style="margin-top:0; margin-bottom:0; font-size:24px;">{{$hotelName}} </p>
									<p style="margin-top:40px; margin-bottom:0; font-size:18px; font-weight: bold;">Your hotel has a new reservation <span style="color:red;">cancellation</span> request! The order number is {{$bookingId}}</p>
									<p style="margin-top:10px; margin-bottom:0; font-size:18px;">Click on the link to manage your rooms: <a style="text-decoration:underline; color:#2681ff;" href="http://ebooking.117book.com" rel="noopener" target="_blank">http://ebooking.117book.com</a>.</p>
									<p style="margin-top:40px; margin-bottom:0; font-size:18px;">Best,</p>
									<p style="margin-top:10px; margin-bottom:0; font-size:18px;">117book Team</p>
								</td>
							</tr>
							<tr>
								<td style="font-size: 18px; vertical-align: top; text-align:center;" valign="top">
									<p style="margin-top:40px; margin-bottom:0;">117book.com</p>
								</td>
							</tr>
							<tr>
								<td style="font-size: 14px; vertical-align: top; text-align:center;" valign="top">
									<p style="margin-top:10px;margin-bottom:0;">Copyright &copy; <span style="border-bottom:1px dashed #ccc;z-index:1" t="7" onclick="return false;" data="2008-{{\Carbon\Carbon::now()->year}}">2008-{{\Carbon\Carbon::now()->year}}</span> 117book.com</p>
								</td>
							</tr>
							<tr>
								<td style="font-size: 14px; vertical-align: top; text-align:center;" valign="top">
									<p style="margin-top:5px; margin-bottom: 40px;">117book.com All rights reserved</p>
								</td>
							</tr>
							<tr>
								<td>
									<p style="margin-top:0; margin-bottom:0; padding-top:20px; text-align: center; font-size:14px; border-top:1px dashed #636363;">
										this is an automatically generated email, please do not reply.
									</p>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
</table>