
<div class="sizeofcontent"  style="margin: 0 auto; width:40%; min-width:320px; font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
	<div >
		<div>
			<div style="text-align: left; margin-bottom:25px" >
				<img style="height:60px;margin-top:50px;  margin-bottom:30px;" src="http://www.117book.com/img/email/Email-Logo-usitrip-en.png" />

			</div>
			<p style="font-size:16px;">Hello {{$NewUserName}},</p>
			<p style="padding-bottom:10px;">
				Thank you for joining hotel.usitrip! Our customer support team will be in contact within 48 hours to help you for the activation.
			</p>
			<p>
				Meanwhile, please complete your account information and provide the following documents:
			</p>
			<p>
				1. Please review and sign the User Agreement attached
			</p>
			<p style="line-height:25px;">
				2. Please provide frequently-used cardholder’s name in the payment section of the User Agreement.
			</p>
			<p style="margin-bottom:30px;">
				3. Please provide a copy of your Business License and company’s Tax ID
			</p>
			<a type="button" href="//www.hotel.usitrip.com/login" class="btn" style="padding:8px 10px; border-radius:4px; text-decoration:none; background:#f1791e !important; border-color:transparent; color:#fff !important; margin-left:0px; font-size:16px; font-weight:500; margin-bottom:55px; ">
				log In
			</a>
			<p style="margin-top:30px;margin-bottom:30px;">Here are the benefits you can enjoy with hotel.usitrip</p>
			<p style="line-height:10px;">-  User-friendly Booking System</p>
			<p style="line-height:10px;">-	Real-time Fetching Best Price</p>
			<p style="line-height:10px;">-	Convenient & Secure Payment Process</p>
			<p style="line-height:10px;">-	Dedicated Team Ready On Your Side</p>
			<p style="line-height:10px;">-	Thoughtful Functions with High Performance</p>
			<p style="margin-top:25px; margin-bottom:20px;">
				You are welcome to contact us anytime for your business needs. We look forward to working with you!
			</p>

			<p style="margin-bottom:30px;">Thank you,<br/>
				www.usitrip.com</p>
			<small style="color:#787878">17870 Castleton St. Suite 358-388</small><br/>
			<small style="color:#787878">City of Industry, CA 91748</small><br/>
			<small style="color:#787878">Phone: 626-898-7800  /  626-434-5267 </small><br/>
			<small style="color:#787878">Email: service@usitrip.com</small><br/>
			<br/>
			<p style="color:#787878; font-size:8px;">This email was sent from a notification-only email address. Please do not reply.</p>
		</div>
	</div>
</div>