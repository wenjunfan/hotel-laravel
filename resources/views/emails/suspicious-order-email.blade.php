<?php
include resource_path('views/emails/lang/suspicious-order-email.php');
$langId = isset($langId) ? $langId : 0;
$hostName = isset($hostName) ? $hostName : 'Usitrip';
$logoUrl = 'http://www.117book.com/img/email/Email-Logo.png';
if ($langId == 1) {
    $logoUrl = 'http://www.117book.com/img/email/Email-Logo-usitrip-en.png';
} elseif ($langId == 2) {
    $logoUrl = 'https://hotel.usitour.com/img/general/logo/logo-usitour-tw.png';
}
$email = "service@usitrip.com";
if ($hostName === 'Usitour') {
    $email = 'service@usitour.com';
    $logoUrl = 'https://hotel.usitrip.com/img/email/Email-Logo-en.png';
} elseif ($hostName === '117book') {
    $email = "bd@117book.com";
}
if ($is_aa) {
    $hostName = '美國亞洲旅行社';
}
?>
<div style="margin: 0 auto; width:40%; min-width:320px; font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
    <div>
        <div>
            <img style="margin-top:50px;" src="{{ $logoUrl }}"/>
        </div>
        <h2>{{ $lang['title'][$langId] }}</h2>
        <p style="font-size:16px;"> {{ str_replace(':user', $data['rooms'][0]['paxes'][0]['name'].' '.$data['rooms'][0]['paxes'][0]['surname'], $lang['hello2'][$langId]) }}</p>
        <p style="margin-bottom:30px;font-size:16px;">{{ str_replace(':company', $hostName, $lang['process'][$langId]) }}</p>
        <p style="font-size:16px;line-height: 22px;letter-spacing: 1px;">{!! $lang['subject'][$langId] !!}</p><br/>
        <a type="button" href="https://hotel.usitrip.com/file-upload/{{$data['Reorderid']}}" class="btn btn-success" style="padding:5px 10px; border-radius:4px;text-decoration:none; background: #005bac !important; border-color: #005bac; color: #fff !important; font-size:15px; font-weight:500;">{{ $lang['upload'][$langId] }}</a>
        <p style="margin-top:35px; border-top: 1px solid lightgrey"></p>
        <p style="margin-bottom:15px; font-size:15px;">{{ $lang['hotelInfo'][$langId] }}</p>
        <p style="font-size:13px;">
            <b style="font-size:13px;">{{$data['hotelName']}}</b></p>
        <p style="font-size:13px;"> {{ $lang['checkIn'][$langId] }}:
            <b style="font-size:13px;">{{$data['searchInfo']['checkin']}}</b> {{ $lang['checkOut'][$langId] }}:
            <b style="font-size:13px;">{{$data['searchInfo']['checkout']}}</b></p>
        <p style="font-size:13px;"> {{ $lang['tempOrder'][$langId] }}:
            <b style="font-size:13px;">{{$data['Reorderid']}}</b></p>
        <p style="border-top: 1px solid lightgrey"></p>

        <p style="margin-bottom:15px; font-size:15px">{{ $lang['bookInfo'][$langId] }}</p>
        <table style="font-size:13px; line-height:30px; margin-left:-3px;">
            <tr>
                <td style="vertical-align: top;">{{ $lang['mainGuest'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td>
                    <b>{{$data['rooms'][0]['paxes'][0]['name'].' '.$data['rooms'][0]['paxes'][0]['surname']}}</b></td>
            </tr>
            <tr>
                <td>{{ $lang['room'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>{{$data['rooms'][0]['roomDetails']['name']}}</b>
                </td>
            </tr>
            <tr>
                <td>{{ $lang['board'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td>
                    <b>{{$data['rooms'][0]['roomDetails']['boardName']}}</b></td>
            </tr>
            <tr>
                <td>{{ $lang['roomCount'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td>
                    <b>{{$data['rooms'][0]['roomDetails']['roomCount']}}</b></td>
            </tr>
            <tr>
                <td>{{ $lang['cancelPolicy'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>{{$cancellation}}</b></td>
            </tr>
            <tr>
                <td>{{ $lang['total'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>{{$data['amount'].' '.$data['currency']}}</b>
                </td>
            </tr>
        </table>
        <p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
        <p style="font-size:15px;">{{ $lang['notes'][$langId] }}</p> <br/>
        <p style="margin-bottom:30px;">Thank you,<br/>
            @if(isset($domain)) {{ $domain }}@elseif($hostName == 'Usitour') www.usitour.com @else www.usitrip.com @endif</p>
        <small style="color:#787878">17870 Castleton St. Suite 358-388</small>
        <br/>
        <small style="color:#787878">City of Industry, CA 91748</small>
        <br/>
        <small style="color:#787878">{{ $lang['phone'][$langId] }}</small>
        <br/>
        <small style="color:#787878">{{ $lang['email'][$langId] }}: {{ $email }}</small>
        <br/>
        <p style="color:#787878; font-size:8px;">{{ $lang['notice'][$langId] }}</p>
    </div>
</div>