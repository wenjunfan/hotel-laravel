<div style="margin: 0 auto; width:40%; min-width:320px;  font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
	<div>
		<div style="text-align: left; margin-bottom:25px" >
			<img style="margin-top:50px; margin-bottom:30px;" src="{{$img}}" />
			<div style="padding-top:10px;padding-bottom:10px;border-radius:0px; text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500; margin-bottom:30px; text-align:center">
				Your Hotel Reservation has been cancelled (Fully or partially) by our Hotel Department.
			</div>
		</div>
		<p style="font-size:16px;">
			Dear {{$partnerName}},
		</p>
		<p style="font-size:15px;">
			Your Hotel Reservation (Order Id: {{$bookingId}}) has been cancelled (Fully or partially)!
		</p>
		<p style="border-top: 1px solid lightgrey; font-size:15px;"></p>
		<p style="margin-bottom:15px; font-size:15px;">
			Hotel Information
		</p>
		<p style="font-size:15px;">
			Hotel Name： {{$hotelName}}
		</p>
		<p style="font-size:15px;">
			Reference#： {{$orderRef}}
		</p>
		<p style="font-size:15px;">
			Order Amount： {{$totalPrice}}
		</p>
		<p style="font-size:15px;">
			Payment Method： {{$paymentType}}
		</p>
		<p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
		<p style="font-size:15px;">This email is for booking cancellation confirmation only, and cannot be used as a proof of refund or any other purposes. If you have any questions, please do not hesitate to contact us.</p>
		<br/>
		<p style="margin-bottom:30px;">Thank you,<br/>
			{{$httpHost}}</p>
		<small style="color:#787878">17870 Castleton St. Suite 358-388</small><br/>
		<small style="color:#787878">City of Industry, CA 91748</small><br/>
		<small style="color:#787878">Phone: 626-434-5267 </small><br/>
		<small style="color:#787878">Email: service@usitrip.com/ bd@117book.com/ service@usitour.com</small><br/>
		<p style="color:#787878; font-size:8px;">This email was sent from a notification-only email address. Please do not reply.</p>
	</div>
</div>