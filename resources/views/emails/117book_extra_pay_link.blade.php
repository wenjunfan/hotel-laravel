<div style="margin: 0 auto; width:40%; min-width:320px; font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
	<div>
		<div style="text-align: left; margin-bottom:25px" >
			<img style="height:60px;margin-top:50px; margin-bottom:30px;" src="http://www.117book.com/img/landing/Logo-EN.png"/>
		</div>
		<p style="font-size:16px;">
			尊敬的用户您好,
		</p>
		<p>您的预订{{$hotelName}}需要补差价，{{$leftToPay}}.</p>
		<p>
			请点击下方按钮,生成二维码，补差价以完成预订。
		</p>
		<a type="button" href="{{str_replace('https//','',$url)}}" class="btn btn-primary" style="padding:5px 10px; border-radius:4px;text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500;">补差价下单链接
		</a>
		<p>(如果上面的链接无法点击,您可以复制:<span style="color:red; font-size:10px;">{{$url}}</span>, 并粘帖到浏览器的地址栏中访问)</p>
		<br/><br/>
			补差价完成或者您希望拒单返款，请联系	美国渠道经理Todd（工号738） ‌+1 626-389-0705 Ext-512 以继续您的预订或者退款请求，谢谢。
		<p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
		<p style="font-size:15px;">此邮件仅用作预订請求确认，不可将此作为酒店凭证等其他任何用途。如有其他问题，也请随时联系我们。</p>
		<br/>
		<p style="margin-bottom:30px;">Thank you,<br/>
			www.117book.com</p>
		<small style="color:#787878">17870 Castleton St. Suite 358-388</small><br/>
		<small style="color:#787878">City of Industry, CA 91748</small><br/>
		<small style="color:#787878">电话: 626-522-2906  / 626-434-5267  </small><br/>
		<small style="color:#787878">邮箱: bd@117book.com</small><br/>
		<p style="color:#787878; font-size:8px;">此邮件为系统自动发送，请勿直接回复</p>
	</div>
</div>