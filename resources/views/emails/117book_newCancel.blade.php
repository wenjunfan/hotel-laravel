<div>
	<p>
		BookingId: {{$bookingId}}
	</p>
	<p>
		PartnerId & name: {{$partnerId}}, {{$partnerName}}
	</p>
	<p>
		Order Reference: {{$orderRef}}
	</p>
	<p>
		Status: {{$status}}
	</p>
	<p>
		Hotel Id & Name: {{$hotelId}}, {{$hotelName}}
	</p>
	<p>
		Total Price: {{$totalPrice}}
	</p>
	<p>
		Provider:{{$provider}}
	</p>
	<p>
		Room Type:{{$roomType}}
	</p>
</div>