
<div class="sizeofcontent" style="margin: 0 auto; width:40%; min-width:320px; font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
	<div >
		<div>
			<div style="text-align: left; margin-bottom:25px" >
				<img style="height:60px;margin-top:50px;  margin-bottom:30px;" src="http://www.117book.com/img/email/Email-Logo.png" />

			</div>
			<p style="font-size:16px;">{{$NewUserName}} 您好,</p>
			<p style="padding-bottom:10px;">
				感谢您成为我们的会员! 我们在48小时内会有客服人员协助您激活账号.
			</p>
			<p>
				等待的同时，也请贵司完善账户信息并准备以下材料:
			</p>
			<p>
				1. 签署在附件中的用户协议,了解内容后, 请在英文正本12&18页签字.
			</p>
			<p style="line-height:25px;">
				2. 新用户在协议最后的支付方式只需填写常用信用卡的持卡人名字即可, 其他可选择填写.
			</p>
			<p style="margin-bottom:30px;">
				3. 提供营业执照和TAX ID.
			</p>
			<a type="button" href="//www.hotel.usitrip.com/login" class="btn" style="padding:8px 10px; border-radius:4px; text-decoration:none; background:#f1791e !important; border-color:transparent; color:#fff !important; margin-left:0px; font-size:16px; font-weight:500; margin-bottom:55px; ">
				立即登录
			</a>
			<p style="margin-top:30px;margin-bottom:30px;">下面让我们一起了解hotel.usitrip预订的乐趣所在</p>
			<p style="line-height:10px;">- 简洁页面 流畅预订</p>
			<p style="line-height:10px;">- 货比多家 即时比价</p>
			<p style="line-height:10px;">- 支付灵活 快捷方便</p>
			<p style="line-height:10px;">- 专业团队 为您服务</p>
			<p style="line-height:10px;">- 贴心功能 实时高效</p>
			<p style="margin-top:25px; margin-bottom:20px;">
				业务上如需配合的地方欢迎随时联系我们，十分期待与您的合作！
			</p>

			<p style="margin-bottom:30px;">Thank you,<br/>
				www.usitrip.com</p>
			<small style="color:#787878">17870 Castleton St. Suite 358-388</small><br/>
			<small style="color:#787878">City of Industry, CA 91748</small><br/>
			<small style="color:#787878">电话: 626-898-7800  /  626-434-5267 </small><br/>
			<small style="color:#787878">邮箱: service@usitrip.com</small><br/>
			<br/>
			<p style="color:#787878; font-size:8px;">此邮件为系统自动发送，请勿直接回复</p>
		</div>
	</div>
</div>