<?php
include resource_path('views/emails/lang/new-order.php');
$langId = isset($langId) ? $langId : 1;
$hostName = isset($hostName) ? $hostName : 'Usitrip';
$logoUrl = 'http://www.117book.com/img/email/Email-Logo.png';
if ($langId == 1) {
    $logoUrl = 'http://www.117book.com/img/email/Email-Logo-usitrip-en.png';
} elseif ($langId == 2) {
    $logoUrl = 'https://hotel.usitour.com/img/general/logo/logo-usitour-tw.png';
}
$email = "service@usitrip.com";
if ($hostName === 'Usitour') {
    $email = 'service@usitour.com';
    $logoUrl = 'https://hotel.usitrip.com/img/email/Email-Logo-en.png';
} elseif ($hostName === '117book') {
    $email = "bd@117book.com";
    $logoUrl = 'http://www.117book.com/img/landing/Logo-CN.png';
}
?>
<div style="margin: 0 auto; width:40%; min-width:320px; font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
    <div>
        <div style="text-align: left; margin-bottom:25px">
            <img style="margin-top:50px; margin-bottom:30px;" src="{{ $logoUrl }}"/>
        </div>
        <p style="font-size:16px;">{{ $lang['hello'][$langId] }}</p>
        <p style="margin-bottom:30px;">{{ $lang['reservation'][$langId] }}
            <b style="color:#f1791e">{{$status}}</b>. {{ $lang['orderId'][$langId] }}: <b>{{$orderId}} </b></p>
        <a type="button" href="{{$voucher}}" class="btn btn-primary" style="padding:5px 10px; border-radius:4px;text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500;">{{ $lang['seeVoucher'][$langId] }}</a>
        <p style="margin-top:35px; border-top: 1px solid lightgrey"></p>
        <p style="margin-bottom:15px; font-size:15px;">{{ $lang['hotelInfo'][$langId] }}</p>
        <p style="font-size:13px;">
            <b style="font-size:13px;">{{$hotelName}}</b></p>
        <p style="font-size:13px;">
            {{$hotelAddress}}
        </p>
        <p style="font-size:13px;">{{ $lang['checkIn'][$langId] }}:
            <b style="font-size:13px;">{{$checkin}}</b> &nbsp; {{ $lang['checkOut'][$langId] }}:
            <b style="font-size:13px;">{{$checkout}}</b></p>
        <p style="font-size:13px;"> {{ $lang['confirmationNo'][$langId] }}: <b style="font-size:13px;">{{$reference}}</b></p>
        <p style="border-top: 1px solid lightgrey"></p>

        <p style="margin-bottom:15px; font-size:15px">{{ $lang['reserveInfo'][$langId] }}</p>
        <table style="font-size:13px; line-height:30px; margin-left:-3px;">
            <tr>
                <td style="vertical-align: top;">{{ $lang['mainGuest'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>{{$guest}}</b>
                    <p>{{ str_replace([':adult', ':child'], [$adNumber, $chNumber], $lang['pax'][$langId]) }}</p></td>
            </tr>
            <tr>
                <td>{{ $lang['room'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>{{$roomType}}</b></td>
            </tr>
            <tr>
                <td>{{ $lang['board'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>{{$boardType}}</b></td>
            </tr>
            <tr>
                <td>{{ $lang['roomCount'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>{{$roomCount}}</b></td>
            </tr>
            <tr>
                <td>{{ $lang['cancelPolicy'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>{{$cancellation}}</b></td>
            </tr>
            <tr>
                <td>{{ $lang['total'][$langId] }}</td>
                <td style="width:30px;"></td>
                <td><b>{{$totalPrice}}</b></td>
            </tr>
        </table>
        <p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
        <p style="font-size:15px;">{{ $lang['notes'][$langId] }}</p> <br/>
        <p style="margin-bottom:30px;">Thank you,<br/>
            @if(isset($domain)) {{ $domain }}@elseif($hostName == '117book') www.117book.com @elseif($hostName == 'Usitour') www.usitour.com @else www.usitrip.com @endif</p>
        <small style="color:#787878">17870 Castleton St. Suite 358-388</small>
        <br/>
        <small style="color:#787878">City of Industry, CA 91748</small>
        <br/>
        <small style="color:#787878">{{ $lang['phone'][$langId] }}</small>
        <br/>
        <small style="color:#787878">{{ $lang['email'][$langId] }}: {{ $email }}</small>
        <br/>
        <p style="color:#787878; font-size:8px;">{{ $lang['notice'][$langId] }}</p>
    </div>
</div>
