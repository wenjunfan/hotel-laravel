<?php
include resource_path('views/emails/lang/extra-pay-link.php');
$langId = isset($langId) ? $langId : 0;
$hostName = isset($hostName) ? $hostName : 'Usitrip';
$logoUrl = 'https://hotel.usitrip.com/img/email/Email-Logo.png';
if($langId == 1) {
    $logoUrl = 'https://hotel.usitrip.com/img/email/Email-Logo-usitrip-en.png';
} elseif($langId == 2) {
    $logoUrl = 'https://hotel.usitrip.com/img/general/logo/logo-usitour-tw.png';
}
$email = "service@usitrip.com";
if ($hostName === 'Usitour') {
    $email = 'service@usitour.com';
    $logoUrl = 'https://hotel.usitrip.com/img/email/Email-Logo-en.png';
}
?>
<div style="margin: 0 auto; width:40%; min-width:320px; font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
    <div>
        <div style="text-align: left; margin-bottom:25px">
            <img src="{{ $logoUrl }}"/>
        </div>
        <p style="font-size:16px;">{{ $lang['hello'][$langId] }}</p>
        <p>{{ $lang['title'][$langId] }}</p>
        <a type="button" href="{{$url}}" class="btn btn-primary" style="padding:5px 10px; border-radius:4px;text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500;">{{ $lang['link'][$langId] }}</a>
        <p>{{ $lang['copy'][$langId] }}
            <span style="color:red; font-size:10px;">{{$url}}</span>, {{ $lang['paste'][$langId] }}</p>
        <br/><br/> {{ $lang['book'][$langId] }}
        <p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
        <p style="font-size:15px;">{{ $lang['notes'][$langId] }}</p> <br/>
        <p style="margin-bottom:30px;">Thank you,<br/>
            @if(isset($domain)) {{ $domain }}@elseif($hostName == 'Usitour') www.usitour.com @else www.usitrip.com @endif</p>
        <small style="color:#787878">17870 Castleton St. Suite 358-388</small>
        <br/>
        <small style="color:#787878">City of Industry, CA 91748</small>
        <br/>
        <small style="color:#787878">{{ $lang['phone'][$langId] }}</small>
        <br/>
        <small style="color:#787878">{{ $lang['email'][$langId] }}: {{ $email }}</small>
        <br/>
        <p style="color:#787878; font-size:8px;">{{ $lang['notice'][$langId] }}</p>
    </div>
</div>