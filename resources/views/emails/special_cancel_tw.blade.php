<div style="margin: 0 auto; width:40%; min-width:320px;  font-family:Roboto, Arial, Lucida Grande, Microsoft Yahei, Hiragino Sans GB, Hiragino Sans GB W3, SimSun, STHeiti;">
	<div>
		<div style="text-align: left; margin-bottom:25px" >
			<img style="margin-top:50px; margin-bottom:30px;" src="{{$img}}" />
			<div style="padding-top:10px;padding-bottom:10px;border-radius:0px; text-decoration:none; background: #f1791e !important; border-color: #f1791e; color: #fff !important; margin-left:0px; font-size:15px; font-weight:500; margin-bottom:30px; text-align:center">
				您飯店預定,經過商務部的處理,已經成功取消(全部或部分)
			</div>
		</div>
		<p style="font-size:16px;">
			{{$partnerName}} 您好,
		</p>
		<p style="font-size:15px;">
			您的飯店預定 (訂單號: {{$bookingId}}) 已經取消成功!
		</p>
		<p style="border-top: 1px solid lightgrey; font-size:15px;"></p>
		<p style="margin-bottom:15px; font-size:15px;">
			飯店信息
		</p>
		<p style="font-size:15px;">
			飯店名稱： {{$hotelName}}
		</p>
		<p style="font-size:15px;">
			Reference#： {{$orderRef}}
		</p>
		<p style="font-size:15px;">
			訂單金額： {{$totalPrice}}
		</p>
		<p style="font-size:15px;">
			支付方式： {{$paymentType}}
		</p>
		<p style="margin-top:10px; border-top: 1px solid lightgrey"></p>
		<p style="font-size:15px;">此郵件僅用作取消預訂的確認,不可將此作為退款憑證等其他任何用途.如有其它問題,也請隨時聯繫我們</p>
		<br/>
		@include('emails.footer-tw')
	</div>
</div>