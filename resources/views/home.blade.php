@extends('layouts.master')

{{--define php var--}}
<?php
$domain = $is_usitour ?  'Usitour' : 'Usitrip';
?>
{{--/define php var--}}

{{--single page title--}}
@if($is_aa)
    @section('title', '酒店预订，最便宜的北美酒店查询预订平台美亚酒店')
@else
    @section('title', trans('tdk.title_home',['domain' => $domain]))
@endif

{{--single page metas--}}

@section('metas')
    <meta name="Keywords" content="{{trans('tdk.keywords_home')}}"/>
    <meta name="description" content="{{trans('tdk.description_home',['domain' => $domain])}}">
@endsection
{{--/single page metas--}}

@section('gtag_events')
    @if($should_track && !$is_aa)
        @php
            $subscribe_con_label = $is_usitour ? 'AW-825564842/iqv3CIKmgoYBEKq91IkD' : 'AW-825564842/BTHxCKb47noQqr3UiQM';
        @endphp
        <!-- Event snippet for 领券 conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
        <script>
            function gtag_report_conversion_subscribe(url) {
                let callback = function () {
                    if (typeof(url) !== 'undefined') {
                        window.location = url;
                    }
                };
                gtag('event', 'conversion', {
                    'send_to': '{{ $subscribe_con_label }}',
                    'value': 1.0,
                    'currency': 'USD',
                    'event_callback': callback
                });
                return false;
            }
        </script>
    @endif
@endsection

{{--single page styles--}}
@section('css')
    <link rel="canonical" href="{{ config('app.url') }}">
    {{--webpack css--}}
    <link rel="stylesheet" href="{{ asset('assets/css/pc/plugins.home.css') }}">
@endsection
{{--/single page styles--}}

{{--single page main content--}}
@section('content')
    @if(!$is_b && !$is_aa)
        @include('components.nav-bar')
    @endif
    {{-- main search begin --}}
    @include('includes.home.home-banner')
    {{-- main earch end --}}

    {{--advantages bar：eg. icon+美国A级认证--}}
    @include('includes.home.advantages')
    @if(!$is_aa)
    {{--affiliate banner--}}
    <div style="margin:0 auto; width:1200px;"><a href="/affiliate/register"><img src="/img/general/affiliate/affiliate_banner_{{$language}}.png"></a></div>
    {{--affiliate banner--}}
   
    {{--subscribe box--}}
    <div class="subscribe-wrapper">
        <form action="#" id="subscribeBox" class="subscribe-box">
            <h2 class="subscribe-title">
                @lang('home.subLabel1') <span class="color-red">@lang('home.subLabel2')!</span>
            </h2>
            <div class="subscribe-input-wrapper">
                <input type="email" name="subscribeEmail" class="subscribe-input" id="subscribeEmail" placeholder="@lang('home.subEmailHint')" required>
                <button type="submit" class="subscribe-btn loadingBtnTop" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">@lang('home.subscribe')</button>
            </div>
            <div class="subscribe-messages">
                <div class="message-1 message">@lang('subscribe.subscribe_send_mem_again')</div>
                <div class="message-2 message">@lang('subscribe.subscribe_send_mem')</div>
            </div>
        </form>
    </div>
    @endif
    {{-- ad1 今日特惠 --}}
    @include('includes.home.recommend-ad')

    {{--ad2 热门目的地--}}
    @include('includes.home.recommend-cities')

    {{--ad3 酒店精品推荐--}}
    @include('includes.home.recommend-hotels')

    {{--底部我们的合作商--}}
    {{--@if	($language === 1)--}}
    {{--<div class="Cons">--}}
        {{--<div class="partners_icons" style="background-position:-14px -186px; width: 182px;height: 99px;"></div>--}}
        {{--<div class="partners_icons" style="background-position: -227px -203px; width: 225px; height: 69px;"></div>--}}
        {{--<div class="partners_icons" style="background-position: -479px -211px; width: 239px; height: 62px;"></div>--}}
        {{--<div class="partners_icons" style="background-position: -752px -191px;width: 227px;height: 107px;"></div>--}}
        {{--<div class="partners_icons last_one" style="background-position: -1006px -206px; width: 197px; height: 70px;"></div>--}}
    {{--</div>--}}
    {{--@endif--}}
@endsection
{{--/single page main content--}}

{{--single page scripts--}}
@section('scripts')
    <script>
        var lang_error_email = "{{trans('subscribe.errorEmail')}}";
    </script>
    {{--webpack js--}}
    <script src="{{ asset('assets/js/pc/plugins.home.js?v=1111') }}"></script>

@endsection
