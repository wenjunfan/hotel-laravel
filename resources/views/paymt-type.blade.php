@extends( !$is_b  ?  'layouts.master' : 'layouts.master-b' )

@section('title', 'Extra Pay')

@section('css')
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<link href="{{ asset('css/plugins/sweetalert/csweetalert.css') }}" rel="stylesheet">
	<link href="{{ asset('css/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">
	<style>

   		.fa-star{
			color: #ffc101;
		}

		.no-border {
			border: 0 !important;
		}

		.font-normal {
			font-weight: normal !important;
		}

		@media (min-width: 1400px) {
			.all-wrapper {
				margin: auto;
				max-width: 1240px; }
		}

		@media (max-width: 1400px) {
			.all-wrapper {
				margin: auto;
				max-width: 980px !important;
			}
		}
	</style>
@endsection

@section('content')
	<div class="all-wrapper" style="margin-top:35px;">
		<div class="row">
			<!--左侧 start-->
			<div class="col-md-7" style="padding-right:55px;">
				<div class="ibox">
					<div class="ibox-noborder">
						<!--<form class="form-horizontal" role="form" id="book-form" >-->
						<form class="form-horizontal" role="form" method="post" id="book-form" action="">
						{!! csrf_field() !!}
						<!--住客信息 start-->
							<div>
					 <span>
						<!--付款信息start-->
						<div class="form-group">
						   <h3 style="font-weight:400; font-size:18px; border-bottom:1px solid lightgrey; padding-bottom:12px;">@lang('paymt-type.soc')</h3>
						   <div style="padding-top:15px;">
							  <br/><br/>
							  <div class="row">
								 <label class="col-sm-12" style="margin-left:15px;font-size:14px;">
									<large style="float:right; margin-top:-15px;" class="text-right text-danger"><small style="font-size:15px; color:#484848;">@lang('paymt-type.totalcharge')：</small><span style="font-size:30px; font-weight:500 !important;">USD</span>&nbsp;<span class="totalPrice" style="font-size:30px; font-weight:500 !important;">{{$amount}}</span></large>
								 </label>
							  </div>
						   </div>
						</div>
					 </span>
							</div>
							<!--付款信息end-->
							<!--付款方式start-->
							<div class="form-group">
								<h3 style="font-weight:400; font-size:18px; border-bottom:1px solid lightgrey; padding-bottom:12px;">@lang('paymt-type.pymt')</h3>
								<div class="row" style="padding-left:15px; padding-top:20px;padding-right:15px;">
									<input type="radio" name="imgsel" value="cc" checked="checked"/>&nbsp;&nbsp;
                                    <label style="margin-right:35px; font-size:14px;">
                                        <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>&nbsp;&nbsp;@lang('paymt-type.cc')
                                    </label>
								
									<button type="button" class="btn btn-primary pull-right ladda-button ladda-button-book" data-style="expand-right" onclick="bookRooms();" margin-right:-15px; style="width:180px; font-size:16px;">
                                        @lang('paymt-type.paynow')
                                    </button>
								</div>
								<div class="form-group pull-right" style="padding-top:20px; text-align: justify;
						-moz-text-align-last: left; /* Code for Firefox */
						text-align-last: left;">
									<span class="col-sm-12 text-danger"></span>
								</div>
							</div>
							<!--付款方式end-->
						</form>
					</div>
				</div>
			</div>
			<!--左侧 end-->
			<style>
				.img-responsive-display {
					width: 100%;
					max-height: 300px;
				}
			</style>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
	<script src="{{ asset('js/plugins/ladda/spin.min.js') }}"></script>
	<script src="{{ asset('js/plugins/ladda/ladda.min.js') }}"></script>
	<script src="{{ asset('js/plugins/ladda/ladda.jquery.min.js') }}"></script>
	<script src="{{ asset('js/plugins/validate/jquery.validate.min.js') }}"></script>
	<script type="text/javascript">
		function bookRooms() {
			var temp = 'po';
			var radio = document.getElementsByName("imgsel");
			for (i = 0; i < radio.length; i++) {
				if (radio[i].checked) {
					temp = radio[i].value;
				}
			}


			if (temp == 'po') {
				//	alert(0);
			}
			if (temp == 'cc') {
				//	alert(1);
			}
			if (temp == 'al') {
				//	alert(2);
			}
			var ggg = $('#book-form');
			//	 $('#book-form').append('<input type="hidden" name="tempType"  value="' + temp + '"/>');
			ggg.append('<input type="hidden" name="tempType"  value="' + temp + '"/>');
			//	alert(document.getElementsByName("tempType").value);
			//	swal("System is under maintenance. Please call（US） 626-768-3706 \n （中） 025-6668-0241 for reservation");

			var r = $("#book-form").valid();

			if (!r) {
				swal("{{ trans('paymt-type.plzcomplete') }}");
				return;
			}

			var l = $('.ladda-button-book').ladda();
			l.ladda('start');
			//$("#book-form").submit();
			var url = "{{url('/business/extra-pay/pay/')}}";
			//$.post(url, $("#book-form").serialize(), function(data) {
			$.post(url, ggg.serialize(), function (data) {
				var l = $('.ladda-button-book').ladda();
				//l.ladda('stop');

				if (data.success) {
					self.location = data.voucherUrl;
				} else {
					if (data.voucherUrl) {
						self.location = data.voucherUrl;
					} else {
						l.ladda('stop');
						swal("{{ trans('paymt-type.failed') }}", data.message, "error");
					}
				}
			});
		}
	</script>
@endsection
