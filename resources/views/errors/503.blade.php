@extends($is_b ? 'layouts.master-b':'layouts.master')

<?php
$isMobile = \Agent::isMobile();
// for usitour, language will always be 1 ('en' for locale)
if ($is_usitour && session('language', 0) == 0) {
    session()->set('language', 1);
    App::setLocale('en');
}elseif(isset($language)){
    session()->set('language', $language);
   if($language == 2){
       $langStr ='tw';
   }elseif($language == 1){
       $langStr ='en';
   }else{
       $langStr ='cn';
   }
    App::setLocale($langStr);
}
?>

@section('title',trans('503.title'))

@section('css')
    <style>
        body {
            background: #fff;
        }
        #master-main {
            text-align: center;
        }
        .content {
            text-align: center;
            display: inline-block;
            padding: 7rem 0;
            margin: 0 auto;
            width: 100vw;
        }
        .title-image {
            display: inline-block;
            background: url('/img/spriteImgs/503_icon.png') no-repeat 16.038% 3.357%;
            background-size: 352px 384px;
            width: 299px;
            height: 176px;
        }
        .text {
            font-size: 18px;
            max-width: 690px;
            color: #636363;
            text-align: left;
            margin: 0 auto;
            width: 90vw;
        }
        @media screen and (min-width: 1000px) {
            .title-image {
                background: url('/img/spriteImgs/503_icon.png') no-repeat -17px -386px;
                width: 671px;
                height: 376px;
            }
            .text {
                font-size: 24px;
                margin-top: 60px;
            }
        }
    </style>
@endsection

@section('content')
    <div class="content">
        <div class="title-image"></div>
        <div class="text">
            @lang('503.server_down')<br><br>@lang('503.try_later')
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(function () {
            // $('#changeLang').css('opacity', 0);
            // $('#page_header2 .account').css('right', '15px');
        });
    </script>
@endsection