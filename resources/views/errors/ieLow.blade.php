<!DOCTYPE html>
<html lang="{{ $language === 0 ? 'zh-Hans' : $language === 2 ? 'zh-Hant': 'en' }}" xmlns="//www.w3.org/1999/xhtml" >
<head>
    <meta charset="UTF-8">
    <title>{{$language === 1 ? 'Please update your browser' : '请升级您的浏览器'}} - @if($is_b) 117book @elseif($is_usitour) Usitour @else Usitrip @endif</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0,minimal-ui" name="viewport" />
    <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
    <meta content="IE=9" http-equiv="X-UA-Compatible">
    <link href=" @if($is_b) {{asset('117book.ico')}} @elseif($is_usitour&& $language !== 2) {{asset('faviconen.ico')}} @else {{asset('favicon.ico')}} @endif" rel="shortcut icon">
    <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
</head>
<style>
    td{
        width:300px;
        text-align:center;
    }
    .ie-outdate-text{
        height: 90px;
        width: 900px;
        margin-left: auto;
        margin-right: auto;
        line-height:40px;
        font-size: 20px;
        color: #333333;
        letter-spacing: 0;
        text-align: center;
        margin-top: 31px;
        font-weight: 400;
    }
    .browser_name {
        text-align: center;
        margin-top: 16px;
        margin-bottom: 16px;
        font-weight: 600;
    }
    .btn-info {
        cursor: pointer;
        border:0;
        width: 100px;
        height: 36px;
        background: #2681FF;
        border-radius: 4px;
        color: #fff;
        font-size: 14px;
        text-align: center;
        line-height: 36px;
    }
    .brower_icon {
        width: 60px;
        height: 60px;
        background-size: 100%;
        display: block;
    }
    .btn-recommend{
        height: 24px;
        padding:2px 10px;
        background: #FFEEDC;
        border-radius: 3px;
        color: #FF7500;
        font-size: 12px;
        line-height: 24px;
        border: 0;
        text-align: center;
        width: 150px;
    }
    .text-center {
        margin-left: auto;
        margin-right: auto;
    }
    .browser_recommend_list{
        height:85px;
    }
    .usitour-icon{
        text-align:center;
        margin-left: 51px;
        height: 61px;
        margin-top: 8px;
        background-image: url(/img/general/icon/usitour_new_icon.png);
        background-repeat: no-repeat;
        background-position: -665px -64px;
    }
    .top-logo{
        height:145px;
    }
</style>
<body>
@if(\Agent::isMobile())
    <style>
        tr{
            line-height:30px;
        }
        .mobile-logo{
            height:25vh;
        }
        .mobile-reco-browser{
            height:30vh;
        }
    </style>
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="font-size:18px; color:#1a1a1a; border-collapse: separate; mso-table-lspace: 0; mso-table-rspace: 0; background-color: #ffffff; width: 100%;" width="100%" bgcolor="#ffffff">
        <tbody>
        <tr class="mobile-logo">
            <td colspan="3">
                @if($is_usitour)
                    <div class="usitour-icon"></div>
                @else
                    <img src="@if($is_b) img/landing/Logo-EN.png @else /img/general/logo/logo-usitrip.png @endif" alt="logo"/>
                @endif
            </td>
        </tr>
        <tr >
            <td colspan="3">
                @if($language === 0)
                    亲，您的浏览器版本过低，为了您可以使用我们网站的更多功能，请升级您的浏览器，
                    如果您在更新后仍然无法访问，請在Internet Explorer设置中禁用兼容性视图。
                @else
                    It looks like your browser is out of date! In order to use all
                    the great features on our website, please update your browser first. If you are still unable to access
                    the site normally after updating, please disable Compatibility View in your Internet Explorer settings.
                @endif
            </td>
        </tr>
        <tr class="mobile-reco-browser">
            <td >
                <img src="/img/general/icon/chrome@2x.png" alt="chrome" class="brower_icon text-center">
                <p class="browser_name text-center">{{$language === 1 ? 'Chrome' : 'Chrome浏览器'}}</p>
                <a href="@if($language === 1)//www.google.com/chrome @else //www.google.cn/intl/zh-CN/chrome/ @endif" onclick="downloadChrome()">
                    <button class="btn btn-info">{{$language === 1 ? 'Download' : '下载'}}</button>
                </a>
            </td>
            <td>
                <img src="/img/general/icon/ie9@2x.png" alt="Internet explorer" class="brower_icon text-center">
                <p class="browser_name text-center">{{$language === 1 ? 'IE' : '升级IE浏览器'}}</p>
                <a href="@if($language === 1)//www.microsoft.com/en-us/download/internet-explorer-11-for-windows-7-details.aspx @else //windows.microsoft.com/zh-cn/internet-explorer/download-ie @endif"
                   onclick="downloadIE11()">
                    <button class="btn btn-info">{{$language === 1 ? 'Download' : '下载'}}</button>
                </a>
            </td>
            <td>
                <img src="/img/general/icon/firefox@2x.png" alt="firefox" class="brower_icon text-center">
                <p class="browser_name text-center">{{$language === 1 ? 'Firefox' : '火狐浏览器'}}</p>
                <a href="@if($language === 1)//www.mozilla.org @else //www.firefox.com.cn @endif" onclick="downloadFirefox()">
                    <button class="btn btn-info">{{$language === 1 ? 'Download' : '下载'}}</button>
                </a>
            </td>
        </tr>
        </tbody>
    </table>
@else

            <table border="0" cellpadding="0" cellspacing="0" class="body" style="font-size:18px; color:#1a1a1a; border-collapse: separate; mso-table-lspace: 0; mso-table-rspace: 0; background-color: #ffffff; width: 100%;" width="100%" bgcolor="#ffffff">
                <tbody>
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="920px;" style="overflow:hidden; border-collapse: collapse; background-color:white; margin-top:20px; margin-bottom:20px;">
                            <tbody>
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="900px;" style="border-collapse: collapse; background-color:white; margin-top:20px; margin-bottom:20px; margin-left:20px; margin-right:20px;">
                                        <tbody>
                                        <tr class="top-logo">
                                            <td></td>
                                            <td>
                                                @if($is_usitour)
                                                    <div class="usitour-icon">
                                                    </div>
                                                @else
                                                    <img src="@if($is_b) img/landing/Logo-EN.png @else /img/general/logo/logo-usitrip.png @endif" alt="logo"/>
                                                @endif
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr class="ie-outdate-text">
                                            <td colspan="3">
                                                @if($language === 0)
                                                    亲，您的浏览器版本过低，为了您可以使用我们网站的更多功能，请升级您的浏览器，
                                                    如果您在更新后仍然无法访问，請在Internet Explorer设置中禁用兼容性视图。
                                                @else
                                                    It looks like your browser is out of date! In order to use all
                                                    the great features on our website, please update your browser first. If you are still unable to access
                                                    the site normally after updating, please disable Compatibility View in your Internet Explorer settings.
                                                @endif
                                            </td>
                                        </tr>
                                        <tr class="browser_recommend_list">
                                            <td class="browser_recommend">
                                                <label class="btn-recommend">{{$language === 1 ? 'Recommended' : '推荐浏览器'}}</label>
                                            </td>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                    <img src="/img/general/icon/chrome@2x.png" alt="chrome" class="brower_icon text-center">
                                                    <p class="browser_name text-center">{{$language === 1 ? 'Chrome' : 'Chrome浏览器'}}</p>
                                                    <a href="@if($language === 1)//www.google.com/chrome @else //www.google.cn/intl/zh-CN/chrome/ @endif" onclick="downloadChrome()">
                                                        <button class="btn btn-info">{{$language === 1 ? 'Download' : '下载'}}</button>
                                                    </a>
                                            </td>
                                            <td>
                                                <img src="/img/general/icon/ie9@2x.png" alt="Internet explorer" class="brower_icon text-center">
                                                    <p class="browser_name text-center">{{$language === 1 ? 'Internet explorer' : '升级IE浏览器'}}</p>
                                                        <a href="@if($language === 1)//www.microsoft.com/en-us/download/internet-explorer-11-for-windows-7-details.aspx @else //windows.microsoft.com/zh-cn/internet-explorer/download-ie @endif"
                                                            onclick="downloadIE11()">
                                                        <button class="btn btn-info">{{$language === 1 ? 'Download' : '下载'}}</button>
                                                    </a>
                                            </td>
                                            <td>
                                               <img src="/img/general/icon/firefox@2x.png" alt="firefox" class="brower_icon text-center">
                                                <p class="browser_name text-center">{{$language === 1 ? 'Firefox' : '火狐浏览器'}}</p>
                                                <a href="@if($language === 1)//www.mozilla.org @else //www.firefox.com.cn @endif" onclick="downloadFirefox()">
                                                    <button class="btn btn-info">{{$language === 1 ? 'Download' : '下载'}}</button>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            @endif
</body>
</html>
