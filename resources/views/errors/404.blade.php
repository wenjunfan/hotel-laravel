<?php
// for usitour, language will always be 1 ('en' for locale)
if ($is_usitour) {
    session()->set('language', 1);
    App::setLocale('en');
}
?>
<!DOCTYPE html>
<html lang="{{ $language === 0 ? 'zh-Hans' : $language === 2 ? 'zh-Hant': 'en' }}">
{{--该页面应像503页面一样做成responsive模式--}}
@if(Agent::isMobile())
    <head>
        <meta charset="UTF-8">
        <title>404-{{ config('app.name') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0"/>
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        {{--<link href=" @if($is_b) {{asset('117book.ico')}} @else {{asset('favicon.ico')}} @endif" rel="shortcut icon">--}}
        <link href="@if($is_b){{asset('117book.ico')}}@elseif($is_usitour&& $language !== 2){{asset('faviconen.ico')}}@else{{asset('favicon.ico')}}@endif" rel="shortcut icon">
        {{--for ios safari--}}
        <meta name="apple-mobile-web-app-capable" content="yes">
        {{--隐藏状态栏--}}
        <meta name="apple-mobile-web-app-status-bar-style” content=black"/>
        {{--指定的iphone中safari顶端的状态条的样式--}}
        <meta content="black" name="apple-mobile-web-app-status-bar-style"/>
        <link rel="canonical" href="{{ config('app.url') }}">
        {{--plugins styles--}}
        <link rel="stylesheet" href="{{ asset('assets/css/mobile/plugins.mobile.css') }}">
        {{--global styles--}}
        <link rel="stylesheet" href="{{ asset('assets/css/mobile/app.css') }}">
        <style type="text/css">
            .error-page {padding: .2rem .2rem;min-height: 80vh;}
            .title {font-weight: normal;font-size: .5rem;text-align: center;color: #003366;}
            .error-desc {color: #e4393c;font-size: .3rem;line-height: .4rem;}
            .error-solution {color: #666666;}
            .can-bt, .can-item {margin: 0;}
            .go-home {margin-top: 1.4rem;display: block;width: 100%;background: #229cfc;color: #fff !important;font-size: .3rem;text-align: center;line-height: .7rem;}
        </style>
    </head>
    <body>
    @include('components.site-notices')
    <div id="app" v-cloak>
        <div class="content-wrapper">
            {{--导航：非酒店关键词，功能性Ui可以使用vue 组件--}}
            <top-nav :show-lang="false"></top-nav>
            {{-- main content --}}
            <div class="error-page">
                <h1 class="title">@lang('404.title')</h1>
                <p class="error-desc">{{!empty($exception->getMessage()) ? $exception->getMessage() : trans('404.error_desc')}}</p>
                <div class="error-solution">
                    <p class="can-bt">@lang('404.error_so_title')</p>
                    <p class="can-item">@lang('404.error_so_1')</p>
                    <p class="can-item">@lang('404.error_so_2')</p>
                </div>
                <a class="go-home" href="{{ config('app.url') }}">@lang('404.back_home')</a>
            </div>
            <footer-nav></footer-nav>
            <back-top></back-top>
        </div>
    </div>

    <script src="{{ asset('assets/js/mobile/plugins.mobile.js') }}"></script>

    {{--js global vars from laravel--}}
    @include('includes.global-vars')

    <script type="text/javascript">
        new Vue({
            el: '#app'
        });
    </script>
    </body>
@else
    @if($is_b)
        {{--b端--}}
        <style type="text/css">
            html, body {height: 100%;}
            body {margin: 0;padding: 0;width: 100%;color: #B0BEC5;display: table;font-weight: 100;font-family: 'Lato';}
            .container {text-align: center;}
            .logo-bar {max-width: 1200px;width: 100%;padding-top: 10px;}
            .content {text-align: center;display: inline-block;}
            .title {font-size: 24px;margin-bottom: 40px;}
            .navbar-brand {top: 15px;}
            .copyright {margin-top: 50px;}
        </style>
        <head>
            <title>404-117book</title>
            <link href="{{asset('117book.ico')}}" rel="shortcut icon"/>
        </head>
        <body>
        <div class="container">
            <div class="logo-bar">
                <a class="navbar-brand" href="http://www.117book.com">
                    <img src="{{asset('img/landing/logo.png')}}">
                </a>
            </div>
            <div class="content">
                <div style="margin-top:50px;">
                    <img src="https://www.usitour.com/tpl/en.usitrip.com/image/404/404text.png" alt="">
                </div>
                <div class="title">{{$exception->getMessage()}}</div>
                <a href="{{ config('app.url') }}">
                    <img src="https://www.usitour.com/tpl/en.usitrip.com/image/404/404btn.png" alt="">
                </a>
                <div class="copyright">
                    <div class="row">
                        <strong style="font-size:18px;  color:#484848; font-weight:600;">Copyright &copy; 2016-<span id="currentYear"></span>&nbsp; 117book.com | All rights reserved.</strong>
                    </div>
                </div>
            </div>
        </div>
        </body>
        <script type="text/javascript" src="https://www.usitrip.com/jscss??ext/jquery/jquery-1.8.3-min.js,ext/jquery/jquery.cookies/jquery.cookie-min.js,tpl/www/js/public.js?t=20180505015120.js" charset="utf-8"></script>
        <script type="text/javascript">
            $(function () {
                //set current year
                $('#currentYear').text((new Date()).getFullYear());
            });
        </script>
    @elseif($is_usitour)
        {{--c端usitour站--}}
        <head>
            <link href="{{asset('faviconen.ico')}}" rel="shortcut icon">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
            <meta property="qc:admins" content="2715266567653142106375"/>
            <meta name="renderer" content="webkit">
            <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"/>
            <title>404-Usitour</title>
            <meta name="Description" content=""/>
            <meta name="Keywords" content=""/>
            <link rel="stylesheet" href="{{ asset('assets/css/404.css') }}">
            <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pc/plugins.master.css') }}" charset="utf-8"/>
            <style type="text/css">
                body {background: #fff;}
                .errorPage {width: 1200px;margin: 0 auto;overflow: hidden;margin-top: 145px;margin-bottom: 225px;}
                .errorBanner, .errorInfo {width: 100%;text-align: center;}
                .errorInfo {margin: 0 auto;}
                /*.title {font-size: 24px;text-align: center;color: #e4393c;}*/
                .goBack {margin-top: 30px;}
            </style>
        </head>
        <body>
        @include('components.header')
        @include('components.nav-bar')
        <div id="J_NavCrumbs" class="g-crumbs">
            <div class="Cons"></div>
        </div>
        <script type="text/javascript" src="https://www.usitour.com/jscss??ext/jquery/jquery-1.8.3-min.js,ext/jquery/jquery.cookies/jquery.cookie-min.js,tpl/www.usitour.com/js/public.js?t=20180724.js" charset="utf-8"></script>
        <script type="text/javascript">
            document.domain = "{{ request()->getHost() }}";
            var SEO_EXTENSION_SEPARATOR = '--';
            var DEBUG = 'false';

            <!--    侧边栏+在线客服    -->
            /*搜索框里添加搜索关键词*/
            var keyword = "";
            if (!keyword) {
                keyword = "";
            }
            if (!keyword) {
                keyword = "where to?";
            }
            $('.hdsearchWords').val(keyword);

            var get_login_status_href = "//post.usitour.com/ajax.php?mod=login&language_code=utf-8";
            G.getLoginStatus("//post.usitour.com/ajax.php?mod=login&language_code=utf-8");
            $('#js-cart').mouseover(function () {
                G.reShoppingCar();
            });
        </script>
        <div class="errorPage clearfix">
            <div class="errorBanner">
                <img src="https://www.usitour.com/tpl/en.usitrip.com/image/404/404text.png" alt="">
            </div>
            <div class="errorInfo">
                @if($exception->getMessage())
                    <div class="title">Detail: {{$exception->getMessage()}}</div>
                @endif
                <p class="goBack">
                    <a href="{{ config('app.url') }}">
                        <img src="https://www.usitour.com/tpl/en.usitrip.com/image/404/404btn.png" alt=""/>
                    </a>
                </p>
            </div>
        </div>
        @include('components.footer')
        <div id="js-contact-wrap-contact-wrap" class="contact-wrap js-contact-wrap" style='display:block'>
            <ul class="contact-way js-contact-way">
                <li class="online-service-hover">
                    <div class="online-service-2">
                        <div class="online-service">
                            <span>Start Chat</span>
                        </div>
                    </div>
                </li>
                <li class="phone-hover">
                    <div class="phone-content">
                        <div class="phone-service">
                            <div class="phone-detail hide" style="left:-284px; min-height:46px;">
                                <p><b>Contact Us: </b><span> 1-626-898-7658 (24/7)</span></p>
                            </div>
                        </div>
                        <span>Tel</span>
                    </div>
                </li>
                <li class="return-top-hover">
                    <a href="#" onclick="window.scrollTo(0,0);return false;">
                        <div class="return-top"></div>
                        <span>top</span></a>
                </li>
            </ul>
        </div>
        @if($language == 1)
            {{--can't async this js file as the inclued jquery version will conflicts with main site's jquery--}}
            <script type="text/javascript" src="{{ asset('assets/js/pc/header-en.js') }}"></script>
        @else
            <script type="text/javascript" src="{{ asset('assets/js/pc/header.js') }}"></script>
        @endif
        <script type="text/javascript" src="https://www.usitour.com/ext/kf/53.js" charset="utf-8"></script>
        <script type="text/javascript">
            $('.en_language').hide();
            $(function () {
                var returnLi = $('.contact-wrap .return-top-hover');
                $(window).scroll(function () {
                    if ($(window).scrollTop() > 700) {
                        returnLi.fadeIn(200);
                    } else {
                        returnLi.fadeOut(200);
                    }
                }).resize(function () {
                });

                setTimeout(function () {
                    $('#js-contact-wrap-contact-wrap').show();
                }, 50);
            });

            /*53自动弹窗
             在线客服1*/
            (function () {
                var service = {
                    init53: function () {
                        var config = {
                            /* 要点击弹出对话框的无素 */
                            el: $('.contact-wrap div.online-service-1')[0],
                            /* new 新窗口打开 popup  弹出层的形式 */
                            type: 'new'
                        };
                        window.service.get53kf(config);
                    },
                    initMeiQia: function () {
                        var config = {
                            url: '//static.meiqia.com/dist/meiqia.js?_=t',
                            el: $('.contact-wrap div.online-service-2')[0],
                            id: 105738,
                            language: 'en',
                            track_tag_callback: function () {
                                $('.contact-wrap div.online-service-2').click(function () {
                                    var first = $(location).attr('pathname').toLowerCase();
                                    first = first.split("/")[1];
                                    first = first ? first : 'home';
                                    if (typeof ga == 'function') {
                                        ga('send', 'event', 'chat', first);
                                    } else {
                                        alert('只有正式站才载入ga（analytics.js）。所以跟踪在测试站不会生效。');
                                    }
                                });
                                $('body').on('click', '#MEIQIA-BTN-HOLDER', function () {
                                    var first = $(location).attr('pathname').toLowerCase();
                                    first = first.split("/")[1];
                                    first = first ? first : 'home';
                                    if (typeof ga == 'function') {
                                        ga('send', 'event', 'chat', first);
                                    } else {
                                        alert('只有正式站才载入ga（analytics.js）。所以跟踪在测试站不会生效。');
                                    }
                                });
                            }
                        };
                        window.service.getMeiQia(config);

                    }
                };
                service.initMeiQia();
            })();

            $(function () {
                //set current year
                $('#currentYear').text((new Date()).getFullYear());
            });
        </script>
        </body>
    @elseif($is_aa)
        <head>
            <meta charset="UTF-8">
            <title>酒店预订，最便宜的北美酒店查询预订平台美亚酒店</title>
            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">

            <meta name="Keywords" content="便宜飯店，北美飯店，美國飯店，好康飯店">
            <meta name="description" content="走四方北美飯店預訂平台,為您提供美國、加拿大等北美飯店在線預訂服務,您可以實時查詢飯店房型價格、房態、品牌、設施、電話、地址、星級等信息,在線預訂北美飯店,享受更多優惠價格。">
            <meta name="author" content="走四方">
            <link href="http://hotel.supervacation.net/favicon.ico" rel="shortcut icon">

            <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pc/plugins.master.css') }}">

            <link rel="canonical" href="http://hotel.supervacation.net">
            <link rel="stylesheet" href="{{ asset('/assets/css/pc/plugins.home.css') }}">

            <style type="text/css">
                .copyright {
                    margin-top: 55px;
                    line-height: 24px;
                    text-align: center;
                }
                .copyright li {
                    display: inline-block;
                }
                .copyright a {
                    padding: 0 2px;
                    color: #666;
                }
                a, a:hover {
                    text-decoration: none;
                }
                .errorPage.clearfix {
                    text-align: center;
                    padding: 50px 0;
                }
                .notice, .hot-tip, #whyUs, .subscribe-box, .question, #affiliateBox, .why-us-block, .exchange-rate-warning, .tell24-other-phone, #MEIQIA-BTN {
                    display: none !important;
                }
                .logo a {
                    margin-left: -5px;
                    background: url('/img/partnerImgs/aaLogo.png') no-repeat;
                }
                .tell24-img {
                    background: url('/img/partnerImgs/aaPhone.png') no-repeat 5px 0;
                }
            </style>
        </head>
        <body class="  pace-done">
        <div class="pace  pace-inactive">
            <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div>
        </div>
        <div style="display:none;">
            <img src="http://hotel.supervacation.net/img/landing/wechatlogo_320x320.jpg" alt="wechat logo">
        </div>
        <div class="wired-all-wraper-for-hotel">
            <div id="topbar">
                <div class="g-wrap uifix">
                    <div class="fl login-wrap" id="login-wrap">
                        <span class="bar-welcome pngFix">欢迎来到美國亞洲旅行社</span>

                        <span class="logout-status js-logout-status"><a class="logi-btn" href="http://www.supervacation.net/index.php?mod=login&amp;language_code=big5">登录</a><a class="regi-btn" href="http://www.supervacation.net/index.php?mod=create_account&amp;language_code=big5">免费注册</a></span><span class="notice"><div id="notice_0"><span id="notice_1"><a href="https://www.usitrip.com/announce/showAnnounce/id-114.html" target="_blank">2019黄石团6折热卖，现在预订保证入住黄石木屋！</a></span></div></span>
                    </div>
                    <div class="toolbar fr uifix">
                        <div class="bar-menu">
                            <ul class="menu-list">
                                <li class="myusi" id="js-myusi">
                                    <div class="myusi-text">
                                        <a class="title" href="//www.supervacation.net/index.php?mod=account&amp;language_code=gb2312"><span>會員中心</span></a>
                                        <span class="down-arrow"><!----></span>
                                    </div>
                                    <dl class="myusi-list">
                                        <dd>
                                            <a href="//www.supervacation.net/index.php?mod=account_history&amp;language_code=gb2312" rel="nofollow">我的订单</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/index.php?mod=account_favorite&amp;language_code=gb2312" rel="nofollow">我的收藏</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/index.php?mod=account_ordersFastQuery&amp;language_code=gb2312" rel="nofollow">订单快速查询</a>
                                        </dd>
                                    </dl>
                                </li>
                                <li class="cart" id="js-cart">
                                    <div class="cart-bd">
                                        <div class="cart-text pngFix">
                                            <a class="title" href="//www.supervacation.net/index.php?mod=shopping_cart&language_code=big5" rel="nofollow"><span>购物车<strong class="total">0</strong></span></a>
                                            <span class="down-arrow"><!----></span>
                                        </div>

                                        <div class="expand-shoppingcart" style="display: none;">
                                            <dl class="cart-list" id="js-cart-list"></dl>

                                            <div class="product-total" id="js-pro-total">
                                                <span>共<em>0</em>件商品 金额总计:</span><strong class="total">$0.00</strong>
                                            </div>
                                            <div class="goshoppingcart">
                                                <a href="//www.supervacation.net/index.php?mod=shopping_cart&amp;language_code=gb2312" rel="nofollow"><span>去购物车结算</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div id="topCurrencyBox" class="bar-language bar-money">
                                        <div class="bar-language-bd">
                                            <div class="selected-language ">
                                                <span id="defCurrency">美元</span> <span class="down-arrow"><!----></span>
                                            </div>

                                            <ul class="site-language">
                                                <li class="pngFix">
                                                    <a href="javascript:void(0)" cur="usd"><span class="title"></span>美元</a>
                                                </li>
                                                <li class="pngFix">
                                                    <a href="javascript:void(0)" cur="cny"><span class="title"></span>人民币</a>
                                                </li>
                                                <li class="pngFix">
                                                    <a href="javascript:void(0)" cur="eur"><span class="title"></span>欧元</a>
                                                </li>
                                                <li class="pngFix">
                                                    <a href="javascript:void(0)" cur="gbp"><span class="title"></span>英镑</a>
                                                </li>
                                                <li class="pngFix">
                                                    <a href="javascript:void(0)" cur="aud"><span class="title"></span>澳元</a>
                                                </li>
                                                <li class="pngFix">
                                                    <a href="javascript:void(0)" cur="hkd"><span class="title"></span>港币</a>
                                                </li>
                                                <li class="pngFix">
                                                    <a href="javascript:void(0)" cur="twd"><span class="title"></span>新台币</a>
                                                </li>
                                                <li class="pngFix">
                                                    <a href="javascript:void(0)" cur="cad"><span class="title"></span>加拿大元</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <header class="border-bottom">
                <div id="" class="white-bg">
                    <div class="row">
                        <div id="head">
                            <div class="hd">
                                <div class="fl logo">
                                    <a id="top_logo_demo" href="//www.supervacation.net" title="歡迎來到美國亞洲旅行社-訂房">美亞旅行社-訂房</a>
                                </div>
                                <div class="fl">
                                    <div class="hdsearch"></div>
                                </div>

                                <div class="fr hdContact">
                                    <div class="tell24-img"></div>
                                    <p class="tell24-split-line"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <main id="master-main">
                <!-- 美亚導航欄 begin 繁體tw -->
                <div class="m-nav-wrap" id="id_nav_head">
                    <div class="g-wrap uifix" id="id_g_wrap">
                        <div class="m-nav-sort fl" id="id_nav_sort">
                            <span class="m-nav-sort-tit">旅遊頻道<i></i></span>
                            <div class="m-sort-box " id="id_sort_box">
                                <div class="nav-sort-item">
                                    <div class="sort-item-left">
                                        <div class="item-title">
                                            <div class="title-content">
                                                <a href="//www.supervacation.net/america/" target="_blank" style="" class="title ">美國</a>
                                            </div>
                                        </div>
                                        <i class="nav-right-icon"></i>
                                        <ul class="item-recommend">
                                            <li>
                                                <a href="//www.supervacation.net/west-coast/" target="_blank" class="" style="color: #F77700; ">美國西海岸</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/america/vc-1/" target="_blank" class="" style=" ">洛杉磯</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/america/vc-2/" target="_blank" class="" style=" ">舊金山</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/america/vc-9/" target="_blank" class="" style=" ">黃石公園</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/america/vc-295/" target="_blank" class="" style=" ">西雅圖</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/america/vc-588/" target="_blank" class="" style="color: #F77700; ">大峽谷</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/east-coast/" target="_blank" class="" style="color: #F77700; ">美國東海岸</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/america/vc-66/" target="_blank" class="" style=" ">紐約</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/america/vc-290/" target="_blank" class="" style=" ">邁阿密</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/america/vc-204/" target="_blank" class="" style=" ">奧蘭多</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/america/vc-65/" target="_blank" class="" style=" ">華盛頓</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/hawaii/" target="_blank" class="" style="color: #F77700; ">夏威夷</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="sort-item-right">
                                        <div>
                                            <div class="item-right-list">
                                                <dl>
                                                    <dt>
                                                        <a href="//www.supervacation.net/west-coast/" target="_blank" class="" style=" ">美國西海岸</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-1/" target="_blank" class="" style="color: #F77700; ">洛杉磯</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-3/" target="_blank" class="" style=" ">拉斯維加斯</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-2/" target="_blank" class="" style=" ">舊金山</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-295/" target="_blank" class="" style=" ">西雅圖</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-10/" target="_blank" class="" style=" ">鹽湖城</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-565/" target="_blank" class="" style=" ">聖地亞哥</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="//www.supervacation.net/east-coast/" target="_blank" class="" style=" ">美國東海岸</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-66/" target="_blank" class="" style=" ">紐約</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-71/" target="_blank" class="" style=" ">波士頓</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-65/" target="_blank" class="" style=" ">華盛頓</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-93/" target="_blank" class="" style="color: #F77700; ">尼亞加拉瀑布</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-477/" target="_blank" class="" style=" ">芝加哥</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-67/" target="_blank" class="" style=" ">費城</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="javascript:" class="" style=" cursor: default;">國家公園</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-9/" target="_blank" class="" style="color: #F77700; ">黃石公園</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-588/" target="_blank" class="" style=" ">大峽谷</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-564/" target="_blank" class="" style="color: #F77700; ">羚羊彩穴</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-32/" target="_blank" class="" style=" ">拱門國家公園</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-51/" target="_blank" class="" style=" ">優勝美地</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-34/" target="_blank" class="" style=" ">大提頓公園</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="//www.supervacation.net/hawaii/" target="_blank" class="" style=" ">夏威夷</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-162/" target="_blank" class="" style=" ">檀香山</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-2700/" target="_blank" class="" style=" ">茂宜島</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-163/" target="_blank" class="" style=" ">珍珠港</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-2675/" target="_blank" class="" style=" ">火山大島</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/hawaii/vc-2699/" target="_blank" class="" style=" ">歐胡島</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="//www.supervacation.net/alaska/" target="_blank" class="" style=" ">阿拉斯加</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-934/" target="_blank" class="" style=" ">費爾班克斯</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-400/" target="_blank" class="" style=" ">安克雷奇</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/alaska/vc-978/" target="_blank" class="" style=" ">珍娜溫泉</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/alaska/vc-2833/" target="_blank" class="" style=" ">北極圈</a>
                                                    </dd>

                                                    <dd>
                                                        <a href="//www.supervacation.net/alaska/vc-405/" target="_blank" class="" style=" ">迪納利國家公園</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="//www.supervacation.net/florida/" target="_blank" class="" style=" ">佛羅裏達</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-290/" target="_blank" class="" style=" ">邁阿密</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-204/" target="_blank" class="" style="color: #F77700; ">奧蘭多</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-286/" target="_blank" class="" style=" ">墨西哥灣</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-1633/" target="_blank" class="" style=" ">西鎖島</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/america/vc-796/" target="_blank" class="" style=" ">大沼澤國家公園</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/cruise/" target="_blank" class="" style=" ">加勒比郵輪</a>
                                                    </dd>
                                                </dl>
                                            </div>

                                            <div class="item-right-bottom">
                                                <div class="item-line-play">
                                                    <h2 class="title">
                                                        <a href="javascript:" class="" style=" cursor: default;">熱門玩法</a>
                                                    </h2>
                                                    <div class="play-content">
                                                        <a href="//www.supervacation.net/america/vc-9/" target="_blank" class="" style=" ">2019黃石7折預售 <i></i></a>
                                                        <a href="//www.supervacation.net/america/ts-479/" target="_blank" class="" style=" ">小團遊 <i></i></a>
                                                        <a href="//www.supervacation.net/america/ts-1476/" target="_blank" class="" style="color: #F77700; ">房車旅遊 <i></i></a>
                                                        <a href="//www.supervacation.net/america/ts-1620/" target="_blank" class="" style=" ">美國西南巨環世界奇觀遊 <i></i></a>
                                                        <a href="//www.supervacation.net/group_buys/" target="_blank" class="" style=" ">限時團購 <i></i></a>
                                                        <a href="//www.supervacation.net/west-coast/hd-1644/" target="_blank" class="" style=" ">西峽谷小木屋</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="nav-sort-item">
                                    <div class="sort-item-left">
                                        <div class="item-title">
                                            <div class="title-content">
                                                <a href="//www.supervacation.net/canada/" target="_blank" style="" class="title ">加拿大</a>
                                            </div>
                                            <div class="title-content">
                                                <a href="//www.supervacation.net/south-america/" target="_blank" style=" " class="title ">南美</a>
                                            </div>
                                        </div>

                                        <i class="nav-right-icon"></i>
                                        <ul class="item-recommend">
                                            <li>
                                                <a href="//www.supervacation.net/canada/vc-270/" target="_blank" class="" style="">溫哥華</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/canada/vc-478/" target="_blank" class="" style="">多倫多</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/canada/vc-365/" target="_blank" class="" style="color: #F77700;">班芙公園</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/mexico/vc-370/" target="_blank" class="" style="">墨西哥城</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/mexico/vc-371/" target="_blank" class="" style="color: #F77700;">坎昆</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/mexico/vc-637/" target="_blank" class="" style="color: #F77700;">奇琴伊察</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="sort-item-right">
                                        <div>
                                            <div class="item-right-content">
                                                <dl>
                                                    <dt>
                                                        <a href="//www.supervacation.net/western-canada/" target="_blank" class="" style=" ">加拿大西部</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-270/" target="_blank" class="" style=" ">溫哥華</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-2813/" target="_blank" class="" style="color: #F77700; ">卡爾加裏</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-296/" target="_blank" class="" style=" ">維多利亞</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-1781/" target="_blank" class="" style=" ">惠斯勒</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="//www.supervacation.net/eastern-canada/" target="_blank" class="" style=" ">加拿大東部</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-478" target="_blank" class="" style=" ">多倫多</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-519/" target="_blank" class="" style=" ">蒙特利爾</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-346/" target="_blank" class="" style=" ">渥太華</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-138" target="_blank" class="" style=" ">魁北克</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-93/" target="_blank" class="" style="color: #F77700; ">尼亞加拉瀑布</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="javascript:" class="" style=" cursor: default;">國家公園</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-365/" target="_blank" class="" style=" ">班夫國家公園</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-545/" target="_blank" class="" style="color: #F77700; ">洛基山國家公園</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-2815/" target="_blank" class="" style=" ">幽鶴國家公園</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-2338/" target="_blank" class="" style=" ">冰川國家公園</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a class="" style=" cursor: default;">極光</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-791/" target="_blank" class="" style=" ">黃刀/耶洛奈夫</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/canada/vc-8863/" target="_blank" class="" style=" ">白馬/懷特霍斯</a>
                                                    </dd>
                                                </dl>
                                                <div class="right-content-play">
                                                    <h2 class="title">
                                                        <a href="" class="" style="cursor: default;">熱門玩法</a>
                                                    </h2>
                                                    <div class="play-content">
                                                        <a href="//www.supervacation.net/canada/vc-431/" target="_blank" class="" style=" ">硫磺山泡溫泉 <i></i></a>
                                                        <a href="//www.supervacation.net/canada/ts-620/" target="_blank" class="" style=" ">觀世界最高潮汐 <i></i></a>
                                                        <a href="//www.supervacation.net/canada/ts-812" target="_blank" class="" style=" ">直升機飛躍千島湖</a>
                                                    </div>
                                                    <div class="line-play-img">
                                                        <a href="//www.supervacation.net/canada/vc-365/" target="_blank">
                                                            <img src="//www.supervacation.net/images/banners/Home_Page_Left_Menu_400x60_22_400x60_1555494406.4645.jpg" alt="班芙國家公園">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-right-content">
                                                <dl>
                                                    <dt>
                                                        <a href="//www.supervacation.net/mexico/" target="_blank" class="" style=" ">墨西哥</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/mexico/vc-371/" target="_blank" class="" style="color: #F77700; ">坎昆</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/mexico/vc-370/" target="_blank" class="" style=" ">墨西哥城</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/mexico/vc-672/" target="_blank" class="" style=" ">梅裏達</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/mexico/vc-637/" target="_blank" class="" style="color: #F77700; ">奇琴伊察</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/mexico/vc-1002/" target="_blank" class="" style=" ">特奧蒂瓦坎</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="//www.supervacation.net/puerto-rico/" target="_blank" class="" style=" ">波多黎各</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/puerto-rico/vc-1428/" target="_blank" class="" style=" ">聖胡安</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/puerto-rico/vc-8823/" target="_blank" class="" style="color: #F77700; ">庫萊布拉島</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/puerto-rico/vc-9430/" target="_blank" class="" style=" ">弗拉門戈海灘</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/puerto-rico/vc-8825/" target="_blank" class="" style="color: #F77700; ">熒光湖</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/puerto-rico/vc-9433/" target="_blank" class="" style=" ">聖胡安老城區</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="//www.supervacation.net/cuba/" target="_blank" class="" style=" ">古巴</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/cuba/vc-8675/" target="_blank" class="" style="color: #F77700; ">哈瓦那</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/cuba/vc-8672/" target="_blank" class="" style=" ">巴拉德羅</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/cuba/vc-8673/" target="_blank" class="" style=" ">革命廣場</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/cuba/vc-8709/" target="_blank" class="" style=" ">國家藝術館</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/cuba/vc-8707/" target="_blank" class="" style=" ">哈瓦那老城區</a>
                                                    </dd>

                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="//www.supervacation.net/latin-america/" target="_blank" class="" style=" ">南美洲</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/brazil/" target="_blank" class="" style=" ">巴西</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/peru/" target="_blank" class="" style=" ">秘魯</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/argentina/" target="_blank" class="" style=" ">阿根廷</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/south-america/vc-9454/" target="_blank" class="" style=" ">智利</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/south-america/vc-8725/" target="_blank" class="" style="color: #F77700; ">馬丘比丘</a>
                                                    </dd>
                                                </dl>
                                                <div class="right-content-play">
                                                    <h2 class="title">
                                                        <a href="" class="" style=" cursor: default;">熱門玩法</a>
                                                    </h2>
                                                    <div class="play-content">
                                                        <a href="//www.supervacation.net/south-america/ts-834" target="_blank" class="" style=" ">坎昆五星全包飯店 <i></i></a>
                                                        <a href="//www.supervacation.net/south-america/fc-371_d-0-1/" target="_blank" class="" style=" ">坎昆自由行推薦</a>
                                                    </div>
                                                    <div class="line-play-img">
                                                        <a href="//www.supervacation.net/south-america/vc-371/" target="_blank">
                                                            <img src="//www.supervacation.net/images/banners/Home_Page_Left_Menu_400x60_23_400x60_15554945769529.jpg" alt="坎昆">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="nav-sort-item">
                                    <div class="sort-item-left">
                                        <div class="item-title">
                                            <div class="title-content">
                                                <a href="//www.supervacation.net/europe/" target="_blank" style="" class="title ">歐洲</a>

                                            </div>
                                            <div class="title-content">
                                                <a href="//www.supervacation.net/australia-new-zealand/" target="_blank" style="" class="title ">澳新</a>
                                            </div>
                                        </div>

                                        <i class="nav-right-icon"></i>
                                        <ul class="item-recommend">
                                            <li>
                                                <a href="//www.supervacation.net/europe/vc-416/" target="_blank" class="" style=" ">倫敦</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/europe/vc-331/" target="_blank" class="" style=" ">巴黎</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/europe/vc-419/" target="_blank" class="" style=" ">巴塞羅那</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/australia-new-zealand/vc-355/" target="_blank" class="" style=" ">悉尼</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/australia-new-zealand/vc-353/" target="_blank" class="" style=" ">墨爾本</a>
                                            </li>
                                            <li>
                                                <a href="//www.supervacation.net/australia-new-zealand/vc-2124/" target="_blank" class="" style=" ">黃金海岸</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="sort-item-right">
                                        <div>
                                            <div class="item-right-content">
                                                <dl>
                                                    <dt>
                                                        <a href="javascript:" class="" style=" cursor: default;">英國</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-416/" target="_blank" class="" style=" ">倫敦</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-481/" target="_blank" class="" style=" ">愛丁堡</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-443/" target="_blank" class="" style=" ">伯明翰</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-623/" target="_blank" class="" style="color: #F77700; ">約克</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-723/" target="_blank" class="" style=" ">巴斯</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="javascript:" class="" style=" cursor: default;">西歐</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-420/" target="_blank" class="" style="color: #F77700; ">柏林</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-487/" target="_blank" class="" style=" ">慕尼黑</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-325/" target="_blank" class="" style=" ">阿姆斯特丹</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-311/" target="_blank" class="" style="color: #F77700; ">法蘭克福</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-515/" target="_blank" class="" style=" ">盧森堡</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="javascript:" class="" style=" cursor: default;">南歐</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-331/" target="_blank" class="" style=" ">巴黎</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-419/" target="_blank" class="" style=" ">巴塞羅那</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-319/" target="_blank" class="" style="color: #F77700; ">羅馬</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-417/" target="_blank" class="" style=" ">米蘭</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-578/" target="_blank" class="" style=" ">威尼斯</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="javascript:" class="" style=" cursor: default;">北歐</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-9120/" target="_blank" class="" style=" ">冰島</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-937/" target="_blank" class="" style="color: #F77700; ">芬蘭</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-938/" target="_blank" class="" style=" ">挪威</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-936/" target="_blank" class="" style=" ">瑞典</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/europe/vc-933/" target="_blank" class="" style=" ">丹麥</a>
                                                    </dd>
                                                </dl>

                                                <div class="right-content-play">
                                                    <h2 class="title">
                                                        <a href="" class="" style=" cursor: default;">熱門玩法</a>
                                                    </h2>
                                                    <div class="play-content">
                                                        <a href="//www.supervacation.net/europe/ts-512/" target="_blank" class="" style="color: #F77700; ">英倫風情 <i></i></a>
                                                        <a href="//www.supervacation.net/europe/ts-511/" target="_blank" class="" style=" ">環遊法國八城 <i></i></a>
                                                        <a href="//www.supervacation.net/europe/ts-510/" target="_blank" class="" style=" ">熱情西班牙&amp;葡萄牙 <i></i></a>
                                                        <a href="//www.supervacation.net/europe/ts-504/" target="_blank" class="" style="color: #F77700; ">北歐冰川 <i></i></a>
                                                        <a href="//www.supervacation.net/europe/ts-509/" target="_blank" class="" style="color: #F77700; ">東歐璀璨環遊 <i></i></a>
                                                        <a href="//www.supervacation.net/europe/ts-508/" target="_blank" class="" style=" ">南法義大利 <i></i></a>
                                                        <a href="//www.supervacation.net/europe/ts-507/" target="_blank" class="" style=" ">西歐經典</a>
                                                    </div>
                                                    <div class="line-play-img">
                                                        <a href="//www.supervacation.net/europe/ts-503/" target="_blank">
                                                            <img src="//www.supervacation.net/images/banners/Home_Page_Left_Menu_400x60_31_400x60_1548928696.1944.jpg" alt="歐洲經典環線">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-right-content">
                                                <dl>
                                                    <dt>
                                                        <a href="" class="" style="cursor: default;">新南威爾士</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-356/" target="_blank" class="" style=" ">堪培拉</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-355/" target="_blank" class="" style="color: #F77700; ">悉尼</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-2200/" target="_blank" class="" style=" ">藍山</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-8915/" target="_blank" class="" style=" ">中央海岸</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="" class="" style="cursor: default;">維多利亞</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-353/" target="_blank" class="" style="color: #F77700;">墨爾本</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-2235/" target="_blank" class="" style="">大洋路</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-2249/" target="_blank" class="" style="">企鵝島</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-2229/" target="_blank" class="" style="">十二門徒</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-2137/" target="_blank" class="" style="color: #F77700;">塔斯馬尼亞</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="" class="" style="cursor: default;">昆士蘭/北領地</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-2124/" target="_blank" class="" style=" ">黃金海岸</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-361/" target="_blank" class="" style="color: #F77700;">大堡礁</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-360/" target="_blank" class="" style=" ">凱恩斯</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-358/" target="_blank" class="" style=" ">布裏斯本</a>
                                                    </dd>
                                                </dl>
                                                <dl>
                                                    <dt>
                                                        <a href="" class="" style="cursor: default;">南澳/西澳</a>
                                                    </dt>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-8577/" target="_blank" class="" style=" ">珀斯</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-8563/" target="_blank" class="" style="color: #F77700;">阿德萊德</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-8588/" target="_blank" class="" style=" ">尖峰石陣</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-8590/" target="_blank" class="" style=" ">天鵝河谷</a>
                                                    </dd>
                                                    <dd>
                                                        <a href="//www.supervacation.net/australia-new-zealand/vc-8583/" target="_blank" class="" style=" ">波浪巖</a>
                                                    </dd>
                                                </dl>
                                                <div class="right-content-play">
                                                    <h2 class="title">
                                                        <a href="" class="" style=" cursor: default;">熱門玩法</a>
                                                    </h2>
                                                    <div class="play-content">
                                                        <a href="//www.supervacation.net/australia-new-zealand/ts-1043/" target="_blank" class="" style="color: #F77700; ">《指環王》奇幻世界 <i></i></a>
                                                        <a href="//www.supervacation.net/australia-new-zealand/ts-1049/" target="_blank" class="" style=" ">紐西蘭4大國家公園 <i></i></a>
                                                        <a href="//www.supervacation.net/australia-new-zealand/ts-1576/" target="_blank" class="" style=" ">世界奇觀藍光螢火蟲洞 <i></i></a>
                                                        <a href="//www.supervacation.net/australia-new-zealand/ts-1566/" target="_blank" class="" style=" ">纜車飛躍阿凡達雨林</a>
                                                    </div>
                                                    <div class="line-play-img">
                                                        <a href="//www.supervacation.net/australia-new-zealand/ts-1574/" target="_blank">
                                                            <img src="//www.supervacation.net/images/banners/Home_Page_Left_Menu_400x60_32_400x60_1548928741.246.jpg" alt="限時限量：小企鵝大遊行">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="m-nav fl">
                            <li id="id_5">
                                <a class="nav_a" href="//www.supervacation.net/" target="_blank" checked_rule="" title="">首頁</a>
                            </li>
                            <li id="id_6" class="nav-down">
                                <a class="nav_a" href="//www.supervacation.net/america" target="_blank" checked_rule="/america" title="">當地參團<i></i><em></em></a>
                                <div class="nav-down-box">
                                    <dl>
                                        <dt>
                                            <a href="//www.supervacation.net/america/" target="_blank" style="">美國</a>
                                        </dt>
                                        <dd>
                                            <a href="//www.supervacation.net/east-coast/" target="_self" style=" ">美國東海岸</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/west-coast/" target="_self" style=" ">美國西海岸</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-9/" target="_self" style=" ">黃石國家公園</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/florida/" target="_self" style=" ">佛羅裏達</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/alaska/" target="_self" style=" ">阿拉斯加</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/hawaii/" target="_self" style=" ">夏威夷</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/south_central/" target="_self" style=" ">美國中南部</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-1/" target="_self" style=" ">洛杉磯</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-66/" target="_self" style=" ">紐約</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-2/" target="_self" style=" ">舊金山</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-3/" target="_self" style=" ">拉斯維加斯</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-588/" target="_self" style=" ">大峽谷</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-65/" target="_self" style=" ">華盛頓</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-71/" target="_self" style=" ">波士頓</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-93/" target="_self" style=" ">尼亞加拉瀑布</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-67/" target="_self" style=" ">費城</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-295/" target="_self" style=" ">西雅圖</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-290/" target="_self" style=" ">邁阿密</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-477/" target="_self" style=" ">芝加哥</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-565/" target="_self" style=" ">聖地亞哥</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-380/" target="_self" style=" ">新奧爾良</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/america/vc-2749/" target="_self" style=" ">休斯頓</a>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="//www.supervacation.net/canada/" target="_blank" style="">加拿大</a>
                                        </dt>
                                        <dd>
                                            <a href="//www.supervacation.net/canada/vc-270/" target="_self" style=" ">溫哥華</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/canada/vc-478/" target="_self" style=" ">多倫多</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/canada/vc-545/" target="_self" style=" ">落基山</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/canada/vc-365/" target="_self" style=" ">班芙國家公園</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/canada/vc-1781/" target="_self" style=" ">惠斯勒</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/canada/vc-296/" target="_self" style=" ">維多利亞</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/canada/vc-138/" target="_self" style=" ">魁北克</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/canada/vc-346/" target="_self" style=" ">渥太華</a>
                                        </dd>
                                        <dd>
                                            <a href="//www.supervacation.net/canada/vc-519/" target="_self" style=" ">蒙特利爾</a>
                                        </dd>
                                    </dl>
                                </div>
                            </li>
                            <li id="id_20" class="active">
                                <a class="nav_a" style="height:40px;" href="/" target="_self" checked_rule="/hotels/" title="">訂房</a>
                                <a style="width: 26px;height: 19px;display: inline-block;
               background-image: url(/img/general/icon/hot.png);background-repeat: no-repeat;background-position: center;position: absolute;left: 47px;top: -8px;"></a>
                            </li>

                            <li id="id_19">
                                <a class="nav_a" href="//www.supervacation.net/america/ts-479/" target="_blank" checked_rule="/america/ts-479/" title="">小團游</a>
                            </li>
                            {{--<li id="id_18"><a class="nav_a" href="//www.supervacation.net/activity/" target="_blank" checked_rule="/activity/" title="">票券當地游</a>--}}
                            {{--</li>--}}
                            <li class=" m-nav-hotel">
                                <a class="nav_a" href="//www.supervacation.net/about_us.html" target="_blank" title="">關於美亞</a>
                            </li>
                        </ul>
                        <ul class="m-nav-two fr" id="id_xmenu"></ul>
                    </div>
                </div>
                <!--导航栏 end-->
                <div class="errorPage clearfix">
                    <div class="errorBanner">
                        <img src="https://www.usitour.com/tpl/en.usitrip.com/image/404/404text.png" alt="">
                    </div>
                    <div class="errorInfo">
                        <div class="title">Detail: {{$exception->getMessage()}}</div>
                        <p class="goBack">
                            <a href="/">
                                <img src="https://www.usitour.com/tpl/en.usitrip.com/image/404/404btn.png" alt="">
                            </a>
                        </p>
                    </div>
                </div>

            </main>
            <!-- end of main content -->
            <footer id="frWrapper">
                <!--Footer Wrapper Start-->
                <div id="foot">
                    <div class="uiWrap uifix">
                        <div class="copyright">
                            <ul>
                                <li>
                                    <a href="http://www.supervacation.net/about_us.html" target="_blank" style="font-size:18px;">關於我們</a>
                                </li>
                                <li>
                                    <a href="http://www.supervacation.net/index.php?mod=order_agreement&amp;action=fromOrder&amp;onlybody=1&amp;language_code=gb2312" target="_blank" style="font-size:18px;margin-left: 15px;">訂購條例</a>
                                </li>
                            </ul>
                            <p>Copyright©1981-<span id="copyDate">2019</span> www.supervacation.net | 美國亞洲旅行社 All rights reserved.</p>
                        </div>
                    </div>
                </div>
                <!--Footer Wrapper End-->
            </footer>
        </div>
        </body>
    @else
        {{--c端usitrip站--}}
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
            <meta property="qc:admins" content="2715266567653142106375"/>
            <meta name="renderer" content="webkit">
            <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"/>
            <link href="{{asset('favicon.ico')}}" rel="shortcut icon">
            <title>404-Usitrip</title>
            <base href="https://www.usitrip.com/"/>
            <meta name="Description" content=""/>
            <meta name="Keywords" content=""/>
            <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pc/plugins.master.css') }}" charset="utf-8"/>
            <style type="text/css">
                /* header */
                .g-hd { width: 1200px; margin: auto; padding: 21px 0 21px 0;}
                .m-logo { background: url(//www.usitrip.com/tpl/www/image/logo24.png) no-repeat; width: 190px; height: 67px; float: left;}
                .m-logo h1 a {width: 190px; height: 67px; text-indent: -9999px; display: block;}
                .m-toptel { float: right; margin: 15px 12px 0 0; /* background:url(//www.usitrip.com/tpl/www/image/contact-phone.png) no-repeat;*/ width: 266px;height: 42px;position: relative;}
                .tell24-img { background: url(//www.usitrip.com/tpl/www/image/contact-phone.png) no-repeat;height: 42px;}
                .tell24-split-line {width: 100%;left: 2px;}
                .tell24-split-line:after {left: 130px;}
                .tell24-other-phone {display: none;width: 168px;padding: 7px 50px;left: 3px;font: 12px/1.5 "Microsoft Yahei", Arial, Helvetica, sans-serif;}
                .tell24-other-phone:before {left: 130px;}
                /* footer */
                .m-busili { position: relative; }
                .m-busili img { position: absolute; bottom: 20px; left: -300px; display: none; z-index: 9999}
                .errorInfo {margin: 60px 0 0 50px;}
                .errorDetail {margin-top: 30px;font-size: 14px;}
                .errorDetail dd {margin-top: 5px;}
                .title {font-size: 24px;margin-top: 30px;color: #e4393c;}
                .errorBanner, .errorInfo { float: left;}
                .goBack {margin-top: 63px;}
                .goBack .goIndex {font-size: 18px;color: #0066cc;text-decoration: underline;}
            </style>
        </head>
        <body>
        <div class="g-hd clearfix">
            <div class="m-toptel">
                <div class="tell24-img"></div>
                <p class="tell24-split-line"></p>
                <!-- 其他国家客服电话 -->
                <div class="tell24-other-phone">
                    <p><b>北美免费：</b><span>888-887-2816</span></p>
                    <p><b>美国：</b><span>1-626-898-7800</span></p>
                    <p><b>加拿大：</b><span>1-647-243-3769</span></p>
                    <p class="line"></p>
                    <p><b>中国：</b><span>025-6668-0241</span></p>
                    <p><b>香港：</b><span>852-5806-0465</span></p>
                    <p class="line"></p>
                    <p>定制包团</p>
                    <p><b>美国：</b><span>1-626-389-0706</span></p>
                    <p><b>中国：</b><span>025-6668-0241</span></p>
                </div>
            </div>
            <div class="m-logo">
                <h1>
                    <a href="{{ config('app.url') }}" title="欢迎来到走四方网">走四方网</a>
                </h1>
            </div>
        </div>
        <div class="errorPage clearfix" style="width: 1200px; margin:0 auto; ">
            <div class="errorBanner">
                <img src="tpl/www/image/404/404_03.jpg" alt="">
            </div>
            <div class="errorInfo">
                <img src="tpl/www/image/404/404_06.png" alt=""/>
                @if(!empty($exception->getMessage()))
                    <h1 class="title">{{ $exception->getMessage() }}</h1>
                @endif
                <ol class="errorDetail">
                    <dt>您可以</dt>
                    <dd>1.请检查输入的网址是否正确</dd>
                    <dd>2.致电走四方旅游网热线：25-6668-0241。</dd>
                </ol>
                <p class="goBack">
                    <a href="{{ config('app.url') }}" class="goIndex">返回首页</a>
                </p>
            </div>
        </div>
        <!--底部-->
        <div class="m-copyright-wrap">
            Copyright<i>&copy;</i>2008-{{\Carbon\Carbon::now()->year}} usitrip.com |
            <a href="https://www.usitrip.com/" target="_blank">走四方旅游网</a>
            All rights reserved. |
            <a href="http://beian.miit.gov.cn" target="_blank">粤ICP备12040635号</a> |
            <span class="license">美国营业执照<em><img data-hover-src="https://www.usitrip.com/tpl/www/image/DBAyyzz.jpg" src="https://www.usitrip.com/tpl/www/image/DBAyyzz.jpg"></em></span>
        </div>
        <script type="text/javascript" src="https://www.usitrip.com/jscss??ext/jquery/jquery-1.8.3-min.js,ext/jquery/jquery.cookies/jquery.cookie-min.js,tpl/www/js/public.js?t=20180505015120.js" charset="utf-8"></script>
        <script type="text/javascript">
            document.domain = "{{ request()->getHost() }}";
            var SEO_EXTENSION_SEPARATOR = '--';
            var DEBUG = 'false';

            /* 底部营业执照 */
            $(".m-busili").hover(function () {
                $(this).children("img").show();
            }, function () {
                $(this).children("img").hide();
            });
            <!--底部end-->

            function jQuery_Enter (event, obj, fun) {
                var key = -1;
                if (event.which == null) { key = event.keyCode; } else { key = event.which; }
                if (key == 13) { eval("obj.value=" + fun + "(obj.value)"); }
                return true;
            }
        </script>
        <!--如果是分销商则隐藏在线客服-->
        <!-- 悬浮框开始 -->
        <div id="js-contact-wrap-contact-wrap" class="contact-wrap js-contact-wrap">
            <ul class="contact-way js-contact-way">
                <li class="phone-hover">
                    <div class="phone-content">
                        <div class="phone-service">
                            <!-- 其他国家客服电话 -->
                            <div class="phone-detail hide">
                                <p><b>北美免费：</b><span>888-887-2816</span></p>
                                <p><b>美国：</b><span>1-626-898-7800</span></p>
                                <p><b>加拿大：</b><span>1-647-243-3769</span></p>
                                <p class="line"></p>
                                <p><b>中国：</b><span>025-6668-0241</span></p>
                                <p><b>香港：</b><span>852-5806-0465</span></p>
                                <p class="line"></p>
                                <p>定制包团</p>
                                <p><b>美国：</b><span>1-626-389-0706</span></p>
                                <p><b>中国：</b><span>025-6668-0241</span></p>
                            </div>
                        </div>
                        <span>电话</span>
                    </div>
                </li>
{{--                <li class="qq-hover">--}}
{{--                    <div class="qq-content">--}}
{{--                        <div class="qq-service">--}}
{{--                            <div class="qq-detail">--}}
{{--                                <ul>--}}
{{--                                    <li>--}}
{{--                                        <a onclick="window.open('//wpa.qq.com/msgrd?v=3&uin=2853759769&site=签证咨询&menu=yes','_blank');" href="javascript:void(0);"><span>签证咨询</span></a>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a onclick="window.open('//wpa.qq.com/msgrd?v=3&uin=2853759761&site=定制包团&menu=yes','_blank');" href="javascript:void(0);"><span>定制包团(中)</span></a>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a onclick="window.open('//wpa.qq.com/msgrd?v=3&uin=2355891217&site=定制包团&menu=yes','_blank');" href="javascript:void(0);"><span>定制包团(美)</span></a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <span>QQ咨询</span>--}}
{{--                    </div>--}}
{{--                </li>--}}
                <li class="qq-hover line" style="background:url(https://www.usitrip.com/tpl/www/image/contact-bg-tw.png) no-repeat">
                    <div class="qq-content">
                        <div class="qq-service">
                            <div class="qq-detail">
                                <img src="https://www.usitrip.com/tpl/www/image/line_qr_code.jpg" title="Line" alt="Line">
                            </div>
                        </div>
                        <span>Line</span>
                    </div>
                </li>
                <li class="weixin-hover">
                    <div class="weixin-content">
                        <div class="weixin-service">
                            <div class="weixin-detail">
                                <img src="{{ asset('img/tours/weixin.png') }}" title="微信" alt="微信"/>
                            </div>
                        </div>
                        <span>微信客服</span>
                    </div>
                </li>
                <li class="return-top-hover">
                    <a href="javascript:" onclick="window.scrollTo(0,0);return false;">
                        <div class="return-top"></div>
                        <span>返回顶部</span></a>
                </li>
            </ul>
            <!-- 购物车开始 -->
            <a class="shopping" href="/index.php?mod=shopping_cart&language_code=gb2312" style="display:block;color:#666666;"><span class="addOne">+1</span> <span id="JS_shoppingNum" class="shoppingNum">0</span>
                <p class="shoppingTitle">购物车</p>
            </a>
        </div>
        <script type="text/javascript" src="https://www.usitrip.com/ext/kf/53.js" charset="utf-8"></script>
        <script type="text/javascript">
            $(function () {
                var returnLi = $('.contact-wrap .return-top-hover');
                $(window).scroll(function () {
                    if ($(window).scrollTop() > 700) {
                        returnLi.fadeIn(200);
                    } else {
                        returnLi.fadeOut(200);
                    }
                }).resize(function () {
                });

                /* 触屏设备要去掉我们自定义的js-contact-wrap-contact-wrap框 */
                setTimeout(function () {
                    if (!isTouch) {
                        $('#js-contact-wrap-contact-wrap').show();
                    }
                }, 50);
            });

            <!-- 悬浮框结束 -->
            $(function () {
                var url = "//post.usitrip.com/ajax.php";
                G.isModelFix(url);
                if (G.getCookie('login_id')) {
                    var clearUrl = "//post.usitrip.com/ajax.php?language_code=gb2312";
                    $("body").append('<div><button type="button" onclick="G.removeCache(' + clearUrl + ')">清除本面缓存</button></div>');
                }
                /* 如果是子框架引用的此页面要在a标签中添加target=_parent */
                if (window != parent) {
                    jQuery('#head a[href!="#"], #head form').attr('target', "_parent");
                    jQuery('#topbar').hide(0);
                }
            });

            /*53自动弹窗
             在线客服1*/
            (function () {
                var service = {
                    initTag: function (arr) {
                        var service_obj = $('.js-contact-way'), li_obj, li;
                        $.each(arr, function (index, value) {
                            li_obj = service_obj.find('li:first');
                            li = document.createElement('li');
                            li.className = (index == arr.length - 1 ? 'online-service-hover' : 'online-service-hover-2') + ' JS-service-' + value;
                            service_obj[0].insertBefore(li, li_obj[0]);
                            li.innerHTML = '<div class="online-service-2">' +
                                '<div class="online-service">' +
                                '<span>在线客服' + (index == arr.length - 1 ? '' : arr.length - index - 1) + '</span>' +
                                '</div>' +
                                '</div>';
                            switch (value) {
                                case '1':
                                    service.init53();
                                    break;
                                case '2':
                                    service.initMeiQia();
                                    break;
                                case '3':
                                    service.initLive800();
                                    break;
                            }
                        });
                    },
                    init53: function () {
                        var config = {
                            /* 要点击弹出对话框的无素 */
                            el: $('.contact-wrap .JS-service-1')[0],
                            /* new 新窗口打开 popup  弹出层的形式 */
                            type: 'new'
                        };
                        window.service.get53kf(config);
                    },
                    initMeiQia: function () {
                        var config = {
                            el: $('.contact-wrap .JS-service-2')[0],
                            track_tag_callback: function () {
                                $('.contact-wrap .JS-service-2').click(function () {
                                    var first = $(location).attr('pathname').toLowerCase();
                                    first = first.split("/")[1];
                                    first = first ? first : 'home';
                                    if (typeof ga == 'function') {
                                        ga('send', 'event', 'chat', first);
                                    } else {
                                        alert('只有正式站才载入ga（analytics.js）。所以跟踪在测试站不会生效。');
                                    }
                                });
                                $('body').on('click', '#MEIQIA-BTN-HOLDER', function () {
                                    var first = $(location).attr('pathname').toLowerCase();
                                    first = first.split("/")[1];
                                    first = first ? first : 'home';
                                    if (typeof ga == 'function') {
                                        ga('send', 'event', 'chat', first);
                                    } else {
                                        alert('只有正式站才载入ga（analytics.js）。所以跟踪在测试站不会生效。');
                                    }
                                });
                            }
                        };
                        var local_host = window.location.host;
                        if (local_host.indexOf("hotel.usitrip.com") > -1) {
                            /*下面参数用来配置指定客服 */
                            config.fallback = 1;
                            config.token = '922c24eb45cc9a1f71272b6d63951a1e,98ced06ca64b68c54760092e64730347,03b1d2b76498e93f2d707cb0287b7122,54b6236c5156f756990f13c5ef9a4730,a424142b479d24355ed1946b5d51305a,6e0c1e72337420bbe0eac86a80b0da18,41a355ea543a7efb12c0de9c9a3b6aeb,a63905ad1cb5d936e297fa7b1b40270c,65f5d705ef59752d5e186715a6d850a9,2d3d6a5c7de424430498a6e98d132f56,40c6ad501747b0007076c1be946d4e1a,576c223952056a516c8c1ccdd857510a,2858ae422e6ab947d00ca94e8b12789d,89a0c8d79fa4d8b2fc0443371801b948';
                            config.group_token = ''
                        }
                        window.service.getMeiQia(config);
                    },
                    initLive800: function () {
                        var config = {
                            el: $('.contact-wrap .JS-service-3')[0]
                        };
                        window.service.getLive800(config);
                    }
                };
                service.initMeiQia();
            })();
        </script>
        </body>
    @endif
@endif
</html>
