@extends('layouts.mobile')

@section('title')
    @lang('home.page_title')
@endsection

{{--  sigle page description and keywords   --}}
@section('metas')
    <meta name="description" content="@lang('metas.description')">
    <meta name="keywords" content="@lang('metas.keywords')">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/mobile/voucher.css') }}">
    <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
@endsection

{{--  sigle page main content   --}}
@section('content')

    <div class="voucher">
        {{--1: header--}}
        {{--2: status card: confirmed, on request or cancelled--}}
        <div @if ($status == 'ON REQUEST') class="onRequest-card" @elseif ($status == 'CONFIRMED') class="confirmed-card" @elseif ($status == 'CANCELLED') class="cancelled-card" @endif>
            <p class="center font20">
                @lang('voucher.referenceNumber')
                <br/>
                <span class="voucher-text-bold font36">
            @if(count(explode(",",$orderPReference)) > 1 && explode(",",$orderPReference)[0] == ' ')
                        {{explode(",",$orderPReference)[1]}}
                    @else
                        {{explode(",",$orderPReference)[0]}}
                    @endif
        </span>
                <br/>
                @lang('voucher.bookStatus')：
                <span class="voucher-text-bold">
            @if ($status == 'ON REQUEST')
                        @lang('voucher.onRequest')
                    @elseif ($status == 'CONFIRMED')
                        @lang('voucher.confirmed')
                    @elseif ($status == 'CANCELLED')
                        @lang('voucher.cancelled')
                    @endif
        </span>
            </p>
        </div>
        <?php $index = 1 ?>
        @foreach ($rooms as $room)
            {{--Notes: do not for loop for each rooms--}}
            <?php if($index == count($rooms))
            {
            ?>
            {{--3 Hotel information--}}
            <div class="hotel-info-card">
                {{--3.1 Hotel Name and star rating--}}
                <p class="hotel-name">
                    {{$hotelName}}
                    <span v-if="langStr === 'cn'">{{isset($hotelNameZh)? $hotelNameZh: ''}}</span>
                    @for ($i = 0; $i < $rating; $i++)
                        <i class="fa fa-star red-star"></i>
                    @endfor
                </p>
                {{--3.2 Hotel Address and Phone--}}
                <div class="hotel-address-and-phone">
                    <p class="padding-bottom-10">
                        <i class="fa fa-map-marker-alt" style="font-size:10px;"></i>
                        {{ucwords(strtolower(str_replace(" ,","",$addr)))}}
                        <span v-if="langStr === 'cn'">{{$desName_zh}}</span>
                    </p>
                    {{--Notes: check if hotelPhone exist--}}
                    @if(isset($hotelPhone) && $hotelPhone != '')
                        <p>
                            <i class="fa fa-phone" style="font-size:10px;"></i>
                            {{$hotelPhone}}
                        </p>
                    @endif
                </div>
                {{--3.3 checkin checkout date card--}}
                {{--2.3. 入住退房card begin--}}
                <div class="flex-container">
                    <div>
                        <p class="font18 padding-top-bottom">
                            @lang('voucher.checkIn')<br/>
                            @if($is_usitour) {{ substr($checkinDate, -5) }}-{{ substr($checkinDate, 0, 4) }}  @else {{substr($checkinDate, -10)}} @endif
                            @{{ dayNameListObj[checkinDayName][langStr] }} </p>
                    </div>
                    <div class="border-left"><p class="font18 padding-top-bottom">
                            @lang('voucher.checkOut')<br/>
                            @if($is_usitour) {{ substr($checkoutDate, -5) }}-{{ substr($checkoutDate, 0, 4) }}     @else {{substr($checkoutDate, -10)}} @endif
                            @{{dayNameListObj[checkoutDayName][langStr] }} </p>
                    </div>
                </div>
                {{--3.4 room Info: room count , day count and cancellation policy--}}
                <div class="room-info">
                    <p class="voucher-text-bold">
                        @if( $room_number == '')
                            1
                        @else
                            {{$room_number}}
                        @endif
                        @lang('voucher.room'):&nbsp;{{ucfirst(strtolower($room['name']))}}
                    </p>
                    <p><span class="voucher-text-bold">@lang('voucher.numOfNight'):</span>&nbsp;{{round($numDays)}} @lang('voucher.night')</p>
                    <p><span class="voucher-text-bold">@lang('voucher.boardType'):</span>&nbsp;{{ucwords(strtolower($room['boardName']))}} <span v-if="langStr === 'cn'">{{isset($room['formatBoard'])? '(' .$room['formatBoard'] .')': ''}}</span></p>
                    <p><span class="voucher-text-bold">@lang('voucher.cancellationPolicy'):</span>

                        <?php if(isset($room['cancelPolicies'])){ ?>
                        @foreach ($room['cancelPolicies'] as $cancel)
                            <?php
                            if (isset($cancel['end']) && strtotime($cancel['end']) > strtotime(date('Y-m-d')) && $cancel['amount'] == 0) {
                                echo '{{freeBefore[langStr]}} ' . substr(str_replace("T", " ", $cancel['end']), 0, 19);
                                if ($status == 'CONFIRMED' && $is_usitour && $partnerId == config('app.partner_id')) {
                                    echo '<span id="cancel_$orderReference"><br/><br/><button style="border:none; color:#fff; background-color:#e4393c" onclick="cancelOrder(\''.$orderReference.'\',\'PO\')">Cancel Order</button></span>';
                           }
                            } elseif (isset($cancel['from']) && strtotime($cancel['from']) > strtotime(date('Y-m-d')) && $cancel['amount'] == 0) {
                                echo '{{freeBefore[langStr]}} ' . substr(str_replace("T", " ", $cancel['from']), 0, 19);
                                if ($status == 'CONFIRMED' && $is_usitour && $partnerId == config('app.partner_id')) {
                                    echo '<span id="cancel_$orderReference"><br/><br/><button style="border:none; color:#fff; background-color:#e4393c" onclick="cancelOrder(\''.$orderReference.'\',\'PO\')">Cancel Order</button></span>';
                            }
                            } else {
                                echo "{{nonRef[langStr]}}";
                            }
                            ?>
                        @endforeach
                        <?php }?>
                    </p>
                </div>
                {{--3.5 Amount paid and wiating for pay, not for 117book--}}
                @if($partnerId == config('app.partner_id'))
                    <div class="room-amount">
                        @if(isset($coupon_amount))
                            <p><span class="voucher-text-bold">@lang('voucher.paidAmount'):</span><strike class="red-star">&nbsp;{{$currencyCode . ' ' .$totalPrice}}</strike>&nbsp;&nbsp;&nbsp;<span>{{$currencyCode . ' ' .$coupon_amount}}</span></p>
                            <p class="voucher-color-blue">Coupon code: {{$coupon_code}}</p>
                        @else
                            <p><span class="voucher-text-bold">@lang('voucher.paidAmount'):</span>&nbsp;{{$currencyCode . ' ' .$totalPrice}}</p>
                        @endif
                        @if(isset($mandatory_fees) && $mandatory_fees != '')
                            <p class="voucher-color-blue"><span class="voucher-text-bold">@lang('voucher.dueHotel') :</span>&nbsp;
                                {{$currencyCode . ' ' . $mandatory_fees}}
                            </p>
                        @endif
                    </div>
                @endif
                {{--3.6 guests--}}
                <div class="guest-name-section">
                        @for ($p = 0; $p < count($rooms); $p++)
                            {{--Note: for each room converting number to chinese characters--}}
                            <?php
                            $roomCount = array("{{langVoucher.one}}", "{{langVoucher.two}}", "{{langVoucher.three}}", "{{langVoucher.four}}", "{{langVoucher.five}}", "{{langVoucher.six}}", "{{langVoucher.seven}}", "{{langVoucher.eight}}");
                            ?>
                            <p class="voucher-text-bold">
                                <span>@lang('voucher.the'){{$roomCount[$p]}}</span>
                                @lang('voucher.guestRoom')：{{$room['adult']}} @lang('voucher.adult')，{{$room['children']}} @lang('voucher.child')</p>
                            @for ($pp = 0; $pp < count($room['paxes']);$pp++)
                                {{$room['paxes'][$pp]['lastName']}}, {{$room['paxes'][$pp]['firstName']}}<br/>
                            @endfor
                        @endfor
                </div>
            </div>
            {{--4 Hotel Remarks--}}
            {{--Notes: check if remark is exists--}}
            @if(isset($room['comments']))
                <div class="comments-card">
                    <p class="comments-card-head">
                        @lang('voucher.remarks'): </p>
                    <p class="comments-card-content">
                        @if($room['comments'] != '')
                            {!! $room['comments'] !!}
                        @else
                            N/A
                        @endif
                    </p>
                </div>
            @endif
            <?php };
            $index += 1;
            ?>
        @endforeach
        {{--5 * Notes to front desk--}}
        <div class="note-to-front-desk-card">
            <p class="note-to-front-desk-card-head">
                * @lang('voucher.noteFrontDesk') </p>
            <p class="note-to-front-desk-card-content">
                @if(!$is_aa)
                    @lang('voucher.note1')
                @endif
                <br>
                <br>
                @lang('voucher.note2') </p>
        </div>
        {{--6 Emergency contact--}}
        <div class="regular-card">
            <p class="regular-card-head">@lang('voucher.emergencyContact') </p>
            <div  v-if="!isAA">
                <p class="regular-card-contact"><i class="fa fa-envelope font20"></i> &nbsp;@if($is_b) bd@117book.com @elseif($is_usitour) service@usitour.com @else service@usitrip.com @endif</p>
                <p class="regular-card-contact"><i class="fa fa-phone font20"></i> &nbsp;@if($is_b) 626-522-2906 @elseif($is_usitour) 626-898-7658  @else 626-898-7800 @endif</p>
                <p class="regular-card-contact"><i class="fa fa-phone font20"></i> &nbsp; 626-434-5267</p>
                <p class="regular-card-contact"><i class="fab fa-weixin font20"></i> &nbsp;usitripalex</p>
                <p class="regular-card-contact"><i class="fab fa-qq font20"></i> &nbsp;2355652773</p>
            </div>
            <div v-else>
                <p class="regular-card-contact"><i class="fa fa-phone font20"></i>626-571-2988</p>
            </div>

        </div>
        {{--7 Voucher information--}}
        <div class="regular-card">
            <p class="regular-card-head">
                @lang('voucher.voucherInformation') </p>
            <div class="regular-card-voucher-info">
                <p class="voucher-text-bold">@lang('voucher.thankYou')</p>
                <p>
                    @lang('voucher.thankYouText') </p><br/>
                <p class="voucher-text-bold">@lang('voucher.yourVoucher')</p>
                <p>
                    @lang('voucher.yourVoucherText') </p><br/>
                <p class="voucher-text-bold">@lang('voucher.delayedArrival') </p>
                <p>
                    @lang('voucher.delayedArrivalText') </p><br/>
                <p class="voucher-text-bold">@lang('voucher.pleaseNote')</p>
                <p>
                    @lang('voucher.pleaseNoteText') </p>
                <div  v-if="!isAA">
                    @if($partnerId == config('app.partner_id'))
                        <br/>
                        <p class="voucher-text-bold">@lang('voucher.chargeFee')</p>
                        <p>
                            @lang('voucher.extraChargeText') </p>
                    @endif
                    @if(!$is_b)
                        <br/>
                        <p class="voucher-text-bold">@lang('voucher.howToCancel')</p>
                        <p>
                            @lang('voucher.howToCancelText') </p>
                    @endif
                </div>
                <br/>
                <p class="voucher-text-bold voucher-color-blue">
                    @lang('voucher.wish') </p>
            </div>
        </div>
        {{--8 Voucher bottom with booking id and company information--}}
        <div class="voucher-bottom"  v-if="!isAA">
            <p class="voucher-text-bold font20" >
                @if($partnerId == config('app.partner_id'))
                    @if($is_usitour)
                            <strong>www.usitour.com</strong><br>
                    @else
                        <strong>www.usitrip.com</strong><br>
                    @endif
                @else
                    <strong>www.117book.com</strong><br>
                @endif
            </p>
            <p class="voucher-text-bold font14">
                17870 Castleton St #388, City of Industry, CA 91748 </p><br>
            <p class="voucher-text-bold font18">
                @lang('voucher.bookingId'): {{$id}}
                <br>
                @lang('voucher.bookingDate'): {{$created_at}}
            </p>
        </div>
        <div class="voucher-bottom"  v-else>
            <p class="voucher-text-bold font20" >
               <strong>www.supervacation.net</strong><br>
            </p>
            <p class="voucher-text-bold font14">
                117 E Garvey Ave., Monterey Park, CA 91755 U.S.A</p><br>
            <p class="voucher-text-bold font18">
                @lang('voucher.bookingId'): {{$id}}
                <br>
                @lang('voucher.bookingDate'): {{$created_at}}
            </p>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var langVoucher = {!! json_encode(trans('voucher')) !!};

        var checkin = '<?= $checkinDate ?>'
        var checkout = '<?= $checkoutDate ?>'
        var roomCount = '<?= isset($room_number) ? $room_number : 1?>'

        $(function () {
            if ({{$is_usitour}}) {
                // 全局替换usitrip为usitour
                $("body").children().each(function () {
                    $(this).html($(this).html().replace("Usitrip", "Usitour"));
                });
            }
            var options = JSON.parse('<?= $options ?>');
            if (options) {
                var anaData = {
                    'id': options.id,
                    'name': options.hotelName,
                    'roomName': options.roomName,
                    'country': options.country,
                    'city': options.city,
                    'affiliate': options.source,
                    'price': options.netPrice,
                    'tax': options.tax,
                    'roomCount': options.roomCount,
                    'dayCount': options.dayCount,
                    'brand': options.brand
                };
                onBookComplete(anaData);
            }
        })

        // below functions are for cancel button
        function cancelOrder (id) {
            swal({
                title: "Are you sure you want to cancel this order?",
                text: "Please check the cancellation policy carefully before you cancel this order",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Yes",
                confirmButtonColor: "#007FF8",
                confirmButtonText: "No",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (!isConfirm) {
                    click = 1;
                    FullscreenSpinner.create();
                    $.post("{{url('/order/cancel')}}", {
                        '_token': $('meta[name="csrf_token"]').attr('content'),
                        'id': id
                    }, function (data) {
                        if (data.hasOwnProperty('options')) {
                            var options = data.options;
                            onCancelComplete(options);
                        }
                        FullscreenSpinner.destroy();
                        if (data.success) {
                            swal({
                                title: "You have cancelled this order successfully",
                                text: data.message,
                                type: "success"
                            }, function () {
                                location.reload();
                            });
                        } else if (data.tyle == "SESSION_TIMEOUT") {
                            swal({
                                title: "please refresh this page",
                                confirmButtonText: "OK",
                                closeOnConfirm: true
                            }, function () {
                                location.reload();
                            });
                        } else {
                            swal("Cancellation failed, please contact customer service", "", "error");
                        }
                    });
                } else {

                }
            });
        }

        var FullscreenSpinner = function () {
            return {
                create: function () {
                    $("body").append('<div class="fullscreen-spinner" style="text-align:center"><div class="sk-bounce1"></div> <div class="sk-bounce2"></div><div class="sk-bounce3"></div> </div>');

                    resizeSpinner($(window).height());
                },

                destroy: function () {
                    $(".fullscreen-spinner").remove();
                }
            };
        }();

        function resizeSpinner (height) {
            var topOffs = (height - 32) / 2;
            $(".fullscreen-spinner").css("padding-top", topOffs + "px");
        }

        $(window).bind("resize", function () {
            resizeSpinner($(this).height());
        });
        {{--这里可以定义本页需要的php变量转js变量--}}

    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    {{--delete later--}}
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    {{-- 源文件： resources/assets/js/mobile/voucher.js --}}
    <script src="{{ asset('assets/js/mobile/voucher.js') }}"></script>
@endsection

