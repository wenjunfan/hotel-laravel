@extends('layouts.mobile')

<?php
$companyName = $is_aa ? 'America Asia' : 'Unitedstars International Ltd';
if ($is_b) {
    $year20 = '20'; //两个paypal要求的数据year不同，b端的要求2018 。。。
} else {
    $year20 = '';  //两个paypal要求的数据year不同，c端的要求18 。。。
}
?>

@section('title',  'Credit Card Payment')

@section('css')
    <link href="{{ asset('assets/css/mobile/plugins.paypal.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div style="width:100vw; padding:3vw; @if($is_usitour) background-color:#f8f6fc; @else background-color:#dfeaf4; @endif">
        <div class="ibox float-e-margins hidden-print">
            {!! csrf_field() !!}
            <div style="padding-top:18px; color:#484848; font-size:20px;">
                <i class="fa fa-credit-card cardBigBlue"></i><h1 class="ccTitle">{{trans('paypal.title')}}</h1><br>
                <i class="fab fa-paypal" style="padding: 3px; color: #4c8ecb;"></i><span class="grey" style="font-size: 12px;"> {{trans('paypal.ccsecure')}}</span><br>
                <i class="fa fa-check" style="padding: 3px;"></i><span class="grey" style="font-size: 12px;"> {{trans('paypal.pisecure')}}</span>
            </div>
            {{--右侧 start--}}
            <div style="line-height: 30px; margin-top:15px;">
                <div style=" background-color:#fff; padding:0 10px;  margin-bottom:15px;">
                    <div class="caption">
                        <p style="font-size:18px; font-weight:500; padding-top:15px; line-height:20px;">
                             <span id="hotel-name">
                                {{$hotelInfo['name']}} @if($language == 0 && $hotelInfo['name_zh'] != "")({{$hotelInfo['name_zh']}})@endif
                             </span>
                        </p>
                        <p style="padding:5px 0; font-size:16px; font-weight:500; color:yellow">
                            <span id="hotel-rating">{{$hotelInfo->rating}}</span>
                        </p>
                    </div>
                    <div class="caption" style="">
                        <p style="font-size:14px;">{{ucwords(strtolower(str_replace(',',' ',$hotelInfo->address))). ', '. ucwords(strtolower($hotelInfo->city)). ', ' . $hotelInfo->zipcode}}</p>
                    </div>
                    <div class="caption" style="">
                         <span style="font-size:14px;">{{trans('book.checkin')}}:
                         </span>
                        <span style="float:right; font-size:14px;" class="control-label checkin"></span>
                    </div>
                    <div class="caption" style="">
                         <span style="font-size:14px;">{{trans('book.checkout')}}:
                         </span>
                        <span style="float:right; font-size:14px;" class="control-label checkout"></span>
                    </div>
                    <!--取消政策start-->
                    <div class="border-bottom" style="border-bottom: 1px solid #eee !important; padding-bottom:15px;">
                        <p style="font-weight:400; font-size:14px;">{{trans('book.canc')}}:</p>
                        <div id="hotel-cancel" class="text-blue" style="font-size:14px">
                            @if($canOrNot)
                                {{trans('book.freeb')}} {{substr(str_replace("T", " ",$canOrNot), 0, 19)}} {{trans('book.ltime')}}
                            @else
                                {{trans('book.non-ref')}}
                            @endif
                        </div>
                    </div>
                    <!--取消政策end-->
                </div>
                <!--价格明细start-->
                <div style=" background-color:#fff">
                    <div style="padding: 15px 10px 0 10px;">
                        <h3 style="font-weight:400; font-size:18px; padding-bottom:12px;">
                            {{trans('book.soc')}}</h3>
                        <p style="font-size:14px;">{{trans('book.rooms')}}:
                            <span style="font-size:16px; font-weight:500; color:#484848; float:right">{{$roomDetails['roomCount'] or 1}}</span>
                        </p>
                        <p style="font-size:14px;">{{trans('book.nights')}}:
                            <span style="font-size:16px; font-weight:500; color:#484848; float:right">{{$roomDetails['dayCount'] or 1}}</span>
                        </p>
                        <p style="font-size:14px;">{{trans('book.roomcost')}}:
                            <span style="font-size:16px; font-weight:500; color:#484848; float:right">{{$roomDetails['currency'] == 'USD'? '$': $roomDetails['currency']}}
                                <span class="roomPrice">
                                     {{$roomDetails['beforeTax']/($roomDetails['dayCount']?$roomDetails['dayCount']:1)/($roomDetails['roomCount']?$roomDetails['roomCount']:1)}}
                                 </span>
                            </span>
                        </p>
                        <p style="font-size: 14px;">{{trans('book.tax')}}:
                            <span style="font-size:16px; font-weight:500; float:right">
                                {{$roomDetails['currency'] == 'USD'? '$': $roomDetails['currency']}}<span id="roomTax">&nbsp;{{$roomDetails['tax']}}</span>
                            </span>
                        </p>

                        @if(isset($coupon))
                            <div class="applied-coupon-block bottom-line-divider">
                                <p class="color-green">@lang('book.coupon_applied_title')</p>
                                <p class="coupon-amount color-green"><span>@lang('book.coupon_applied_desc')</span><span id="couponAmount" class="pull-right">-${{ $coupon->amount }}</span></p>
                            </div>
                        @endif

                        @if($cleanFee !== 0)
                            <p style="border-bottom:1px solid darkgrey; padding-bottom:15px;font-size: 14px;">{{trans('book.clean_fee')}}:
                                <span style="font-size:16px; font-weight:500; float:right">$ {{$cleanFee}}</span>
                            </p>
                            <br/>
                        @endif
                        <label style="font-size:18px; color:#484848; padding-bottom:15px;">{{trans('book.totalcharge')}}: </label>
                        <span style="float:right;" class="text-right text-danger">
                             <span style="font-weight:500">   {{$roomDetails['currency'] == 'USD'? '$': $roomDetails['currency']}}</span>&nbsp;
                             <span class="totalPrice" style="font-size:30px; font-weight:500">{{$totalPrice}}</span>
                         </span>
                    </div>
                    @if((isset($roomDetails['mandatoryFees']) && str_replace('.0', '',$roomDetails['mandatoryFees']) != 0) || (isset($roomDetails['resortFee']) && str_replace('.0', '',$roomDetails['resortFee']) != 0) || (isset($tempInfo->mandatory_fee) && str_replace('.0', '',$tempInfo->mandatory_fee) != 0))
                        <div style="margin:0 10px; padding:1px 0;">
                            <label style="font-size:15px; color:grey;">{{trans('book.totalchargeHotel')}} ({{trans('book.resortFee')}})</label>
                            <span style="float:right;" class="text-right">
                                 @if(isset($roomDetails['mandatoryFees']) != null)
                                    <span style="font-size:16px; font-weight:500;  color:grey;">{{$roomDetails['currency'] == 'USD'? '$': $roomDetails['currency']}}
                                        <span class="roomPrice">{{str_replace('.0', '',$roomDetails['mandatoryFees'])}}</span>
                                 </span>
                                @elseif( isset($roomDetails['resortFee']) != null)
                                    <span class="hotelCharge">{{$roomDetails['resortFee']['currency']}}
                                        <span class="roomPrice">{{str_replace('.0', '', $roomDetails['resortFee']['amount'])}}</span>
                                 </span>
                                @else
                                    <span class="hotelCharge">{{$tempInfo->currency}}
                                        <span class="roomPrice">{{str_replace('.0', '',$tempInfo->mandatory_fee)}}</span>
                                </span>
                                @endif
                            </span>
                        </div>
                    @endif
                </div>
                {{--<!--价格明细end-->--}}
                {{--右侧 end--}}
            </div>
            <form class="form-horizontal" style="margin-top:20px; margin-bottom:30px; padding:10px; background-color:#fff;" role="form" id="creditform">
                {!! csrf_field() !!}
                <input type="hidden" name="Reorderid" id="Reorderid" value="{{$Reorderid}}">
                <input type="hidden" id="language" name="language" value="{{$language}}">
                <p class="subHead">{{trans('paypal.cardInfo')}}</p><br/>
                {{--信用卡在线付款--}}
                <div class="form-group">
                    <label class="col-sm-2 leftTitle" for="cardType">{{trans('paypal.ct')}}<i class="mustFill">*</i></label>
                    <div class="col-sm-9 form-inline">
                        <img style="height:40px;" src="/img/payment/all-major-credit-cards.png"/>
                        <input type="hidden" id="cardType" name="cardType" value=""/>
                    </div>
                </div>
                {{--姓名--}}
                <div class="form-group">
                    <label class="col-sm-2 leftTitle" for="firstName">{{trans('paypal.cardholder')}}<i class="mustFill">*</i></label>
                    <div class="col-sm-9 form-inline">
                        <input type="text" class="form-control" id="cc_fn" name="firstName" value="" placeholder="{{trans('book.fname')}}" v-model="cardData.firstName"/>
                        <input type="text" class="form-control" id="cc_ln" name="lastName" value="" placeholder="{{trans('book.lname')}}" v-model="cardData.lastName"/>
                    </div>
                </div>
                {{--卡号--}}
                <div class="form-group">
                    <label class="col-sm-2 leftTitle" for="number">
                        {{trans('refunds.cn')}}<i class="mustFill">*</i>
                    </label>
                    <div class="col-sm-6 form-inline">
                        <input type="text" id="number" class="form-control" name="number" value="" placeholder="{{trans('paypal.cn')}}" v-model="cardData.number"/>
                    </div>
                </div>
                {{--有效日期--}}
                <div class="form-group">
                    <label class="col-sm-2 leftTitle" for="expirationDate">{{trans('paypal.ed')}}<i class="mustFill">*</i></label>
                    <div class="row">
                        <div class="col-sm-6 pull-left">
                            <select id="month" name="month" class="form-control" v-model="cardData.month">
                                <option value="01">
                                    {{trans('paypal.jan')}}</option>
                                <option value="02">
                                    {{trans('paypal.feb')}}</option>
                                <option value="03">
                                    {{trans('paypal.mar')}}</option>
                                <option value="04">
                                    {{trans('paypal.apr')}}</option>
                                <option value="05">
                                    {{trans('paypal.may')}}</option>
                                <option value="06">
                                    {{trans('paypal.jun')}}</option>
                                <option value="07">
                                    {{trans('paypal.jul')}}</option>
                                <option value="08">
                                    {{trans('paypal.aug')}}</option>
                                <option value="09">
                                    {{trans('paypal.sep')}}</option>
                                <option value="10">
                                    {{trans('paypal.oct')}}</option>
                                <option value="11">
                                    {{trans('paypal.nov')}}</option>
                                <option value="12">
                                    {{trans('paypal.dec')}}</option>

                            </select>
                        </div>
                        <div class="col-sm-6 pull-left">
                            <select id="year" name="year" class="form-control" v-model="cardData.year">
                                <option value="{{$year20.'20'}}">
                                    2020{{trans('paypal.year1')}}</option>
                                <option value="{{$year20.'21'}}">
                                    2021{{trans('paypal.year1')}}</option>
                                <option value="{{$year20.'22'}}">
                                    2022{{trans('paypal.year1')}}</option>
                                <option value="{{$year20.'23'}}">
                                    2023{{trans('paypal.year1')}}</option>
                                <option value="{{$year20.'24'}}">
                                    2024{{trans('paypal.year1')}}</option>
                                <option value="{{$year20.'25'}}">
                                    2025{{trans('paypal.year1')}}</option>
                                <option value="{{$year20.'26'}}">
                                    2026{{trans('paypal.year1')}}</option>
                                <option value="{{$year20.'27'}}">
                                    2027{{trans('paypal.year1')}}</option>
                                <option value="{{$year20.'28'}}">
                                    2028{{trans('paypal.year1')}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                {{--CVS验证码--}}
                <div class="form-group">
                    <label class="col-sm-2 leftTitle" for="CVC">{{trans('paypal.cvc')}}<i class="mustFill">*</i></label>
                    <div class="col-sm-9 form-inline">
                        <input type="text" id="cvc" class="form-control" name="CVC" value="" placeholder="CVC" v-model="cardData.cvc"/>
                    </div>
                </div>

                {{--手机号--}}
                <div class="form-group NeedVerify">
                    <label class="col-sm-2 leftTitle" for="phone">{{trans('paypal.phoneValid')}}<i class="mustFill">*</i></label>
                    <div id="validatePhoneSection" class="col-sm-9 form-inline validatePhone">
                        <select id="countryCodePhone" name="countryCodePhone" class="form-control" style="margin-bottom: 10px;">
                            <option value="+1" selected>{{trans('search.USA')}}+1
                            </option>
                            <option value="+1">{{trans('search.Canada')}}+1
                            </option>
                            <option value="+86">{{trans('search.CN')}}+86
                            </option>
                            <option value="+886">{{$language == '1'? 'TaiWan CN' : '中国台湾'}}+886
                            </option>
                            <option value="+852">{{$language == '1'? 'HongKong CN' : '中国香港'}}+852
                            </option>
                            <option value="+853">{{$language == '1'? 'Macau CN' : '中国澳门'}}+853
                            </option>
                            <option value="+44">{{$language == '1'? 'United Kingdom' : '英国'}}+44
                            </option>
                            <option value="+49">{{$language == '1'? 'Germany' : '德国'}}+49
                            </option>
                            <option value="+33">{{$language == '1'? 'France' : '法国'}}+33
                            </option>
                            <option value=" ">{{trans('search.other')}} </option>
                        </select>
                        <span class="other"></span>
                        <input type="text" id="phone" class="form-control" name="phone" value='' placeholder="{{trans('order.cphone')}}" v-model="cardData.phone"/>
                        <label class="btn verifingBtn">{{trans('paypal.sendCode')}}</label>
                        <div class="appendValidateSpace"><input id="validatePhoneCode" name="validatePhoneCode" class="form-control" placeholder="{{trans('paypal.enterVrfCode')}}">
                            <label disabled id='verifyCodeBtn' class="btn validateCode">{{trans('paypal.vrfCode')}}</label>
                        </div>
                    </div>
                </div>
                <br/>
                <p class="subHead">{{trans('paypal.addrInfo')}}</p><br/>
                {{--国家--}}
                <div class="form-group">
                    <label class="col-sm-2 leftTitle" for="countryCode">{{trans('paypal.country')}}<i class="mustFill">*</i></label>
                    <div class="col-sm-9 form-inline">
                        <select id="countryCode" name="countryCode" class="form-control" v-model="cardData.countryCode">
                            <option value="US" selected>{{trans('search.USA')}}</option>
                            <option value="CH">{{trans('search.CN')}}</option>
                            <option value="CA">{{trans('search.Canada')}}</option>
                            <option value="other">{{trans('search.other')}}</option>
                        </select>
                    </div>
                </div>
                {{--地址--}}
                <div class="form-group">
                    <label class="col-sm-2 leftTitle" for="street">{{trans('paypal.add')}}<i class="mustFill">*</i></label>

                    <div class="col-sm-9 form-inline">
                        <input type="text" class="form-control" id="cc_street" name="street" value="" placeholder="{{trans('paypal.st')}}" v-model="cardData.street"/>
                    </div>
                </div>
                {{--地址2--}}
                <div class="form-group">
                    <label class="col-sm-2 leftTitle" for="street2">{{trans('paypal.add2')}}</label>
                    <div class="col-sm-9 form-inline">
                        <input type="text" class="form-control" id="cc_street2" name="street2" value="" placeholder="{{trans('paypal.apt')}}" v-model="cardData.street2"/>
                    </div>
                </div>
                {{--城市--}}
                <div class="form-group">
                    <label class="col-sm-2 leftTitle" for="city">{{trans('paypal.city')}}<i class="mustFill">*</i></label>
                    <div class="col-sm-6 form-inline">
                        <input type="text" class="form-control" id="cc_city" name="city" value="" v-model="cardData.city"/>
                    </div>
                </div>
                {{--州/省--}}
                <div class="form-group">
                    <label class="col-sm-2 leftTitle" for="state">{{trans('paypal.county')}}<i class="mustFill">*</i></label>

                    <div class="col-sm-6 form-inline">
                        <input type="text" class="form-control" id="cc_state" name="state" value="" v-model="cardData.state"/>
                    </div>
                </div>
                {{--邮编--}}
                <div class="form-group">
                    <label class="col-sm-2 leftTitle" for="zip">{{trans('paypal.zip')}}<i class="mustFill">*</i></label>
                    <div class="col-sm-6 form-inline">
                        <input type="text" class="form-control" id="cc_zip" name="zip" value="" v-model="cardData.zip"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 leftTitle">
                        <div class="NeedVerify">
                            <span id="ccAuth">
                                <input type="checkbox" id="cb" name="cb" checked="checked" @click="calc" class="pull-left" style="margin-right:10px;padding-bottom:20px;text-align: justify;
                    -moz-text-align-last: left; /* Code for Firefox */
                    text-align-last: left;">
                            </span>
                            <strong>
                                {{trans('paypal.read')}}
                                <a data-toggle="modal" data-target="#modalAuthorization" @click="showForm = true">{{trans('paypal.ccAuth')}}</a>
                                {{trans('paypal.confirmTrans')}}
                            </strong>
                        </div>
                        <button type="button" @click="submitPayment" id="bt" name="bt" class="ladda-button ladda-button-book payment-btn">@lang('paypal.pmt')</button>
                        <div style="text-align: center; margin-top: 5px;"><i class="fab fa-paypal"></i> Secure payment by Paypal</div>
                    </div>
                </div>
            </form>

            <!--注意事项start-->
            <div style="background-color:#fff; padding:20px 10px;">
                <div class="card" style="background-color:#fff;" @click="showComment = !showComment">
                    <div class="card-title" style="font-weight:400; font-size:18px;">{{trans('book.fineprint')}}<i class="fa fa-angle-down pull-right"></i></div>
                    <div id="hotel-comment" style="font-size:14px; padding-top:10px; text-align: justify;
-moz-text-align-last: left; /* Code for Firefox */
text-align-last: left;" v-show="showComment">
                        {!! isset($roomDetails['comments'][0]['comments'] ) && $roomDetails['comments'][0]['comments'] != '' ? $roomDetails['comments'][0]['comments']: 'None' !!}
                    </div>
                </div>
            </div>
            <!--注意事项end-->

            {{--prevent user from click outside to close--}}
            <mu-dialog :open="processing" @close="processing = true" dialog-class="submit-loading-modal">
                <img src="/img/general/loading-spinner.gif" width="100%" alt="loading spinner"/>
                <h4 id="ifMobileHide">
                    @if(0 === $language) 正在为您跳转... @else  Please wait patiently... @endif
                </h4>
            </mu-dialog>
            {{--左侧 end--}}

            {{--授权书modal--}}
            <mu-dialog dialog-class="auth-dialog" title-class="auth-dialog-title" body-class="auth-dialog-body" :open="showForm"
                    :scrollable="true" title="Credit Card Charge Authorization Form" @close="showForm = false">
                {{--授权书内容 MODAL--}}
                <div class="modal-term-body">
                    <h3>The reason for credit card charging:</h3>
                    <p>Main Guest: {{$guest}}, Hotel Name: {{$hotelName}}, Checkin Date to Checkout Date: @{{
                        cardData.checkInOut }}</p>
                    <hr>
                    <h3>The charging amount: {{$price}}</h3>
                    <p><strong>Name on Card: </strong> @{{ cardData.lastName }}, @{{ cardData.firstName }}</p>
                    <p><strong>Card Number: </strong> @{{ cardData.number }}
                    <p><strong>Expiration Date:</strong> @{{ cardData.month }}/ @{{ cardData.year }}</p>
                    <p><strong>Security Code:</strong> @{{ cardData.cvc }} </p>
                    <p><strong>Cardholder Phone #: </strong> @{{ cardData.phone }}</p>
                    <p><strong>Credit Card Billing Address: </strong> @{{ cardData.street }} @{{
                        cardData.street2 }}
                    </p>
                    <p><strong>City/State/Zip/Country: </strong> @{{ cardData.city }}, @{{ cardData.state }},
                        @{{
                        cardData.zip }}, @{{ cardData.countryCode }}</p>
                    <hr>
                    <p>
                        I authorize {{$companyName}} to charge the credit card indicated in this
                        authorization form according to the terms outlined above. This payment authorization is
                        for
                        the goods/services described above, for the amount indicated above only, and is valid
                        for
                        one time use only. I certify that I am an authorized user of this credit card and that I
                        will not dispute the payment with my credit card company; so long as the transaction
                        corresponds to the terms indicated in this form. I also read the  {{$companyName}}'s purchasing agreement on
                        @if($is_usitour)
                            @if($language == 2)
                                <a href="https://www.usitrip.com/tw/order_agreement.html" target="_blank">www.usitrip.com/tw/</a>
                            @else
                                <a href="https://www.usitour.com/order_agreement.html" target="_blank">www.usitour.com/</a>
                            @endif
                        @elseif($is_aa)
                            <a href="//www.supervacation.net/order_agreement.html" target="_blank">www.supervacation.net//</a>
                        @else
                            <a href="https://www.usitrip.com/order_agreement.html" target="_blank">www.usitrip.com/</a>
                        @endif
                        and agree on all the terms and conditions. </p>
                    <hr>
                    <h3>Signature of Card Holder: @{{ cardData.lastName }}, @{{ cardData.firstName }}</h3>
                    <h3>Date: <?php echo date("n/j/Y"); ?></h3>

                    <hr>
                    <p class="text-center">
                        <button class="btn btn-default" @click="printForm">
                            <i class="fa fa-print"></i>{{trans('paypal.print')}}
                        </button>
                    </p>
                </div>
                <mu-flat-button slot="actions" primary @click="showForm = false" label="{{trans('paypal.close')}}"/>
                {{--/授权书内容 MODAL--}}
            </mu-dialog>
        </div>
        {{--print authorization form--}}
        <div class="visible-print-block">
            <h3>Credit Card Charge Authorization Form</h3>
            <hr>
            <h4>The reason for credit card charging:</h4>
            <p>Main Guest: {{$guest}}, Hotel Name: {{$hotelName}}, Checkin Date to Checkout Date: @{{
                cardData.checkInOut }}</p>
            <hr>
            <h3>The charging amount:{{$price}}</h3>
            <p><strong>Card Type: </strong> @{{ cardData.cardType }}</p>
            <p><strong>Name on Card: </strong> @{{ cardData.lastName }}, @{{ cardData.firstName }}</p>
            <p><strong>Card Number: </strong> @{{ cardData.number }}
            <p><strong>Expiration Date:</strong> @{{ cardData.month }}/ @{{ cardData.year }}</p>
            <p><strong>Security Code:</strong> @{{ cardData.cvc }} </p>
            <p><strong>Cardholder Phone #: </strong> @{{ cardData.phone }}</p>
            <p><strong>Credit Card Billing Address: </strong> @{{ cardData.street }} @{{ cardData.street2 }} </p>
            <p><strong>City/State/Zip/Country: </strong> @{{ cardData.city }}, @{{ cardData.state }}, @{{cardData.zip}}, @{{ cardData.countryCode }}</p>
            <hr>
            <p>
                I authorize {{$companyName}}to charge the credit card indicated in this authorization
                form
                according to the terms outlined above. This payment authorization is for the goods/services
                described
                above, for the amount indicated above only, and is valid for one time use only. I certify that I am
                an
                authorized user of this credit card and that I will not dispute the payment with my credit card
                company;
                so long as the transaction corresponds to the terms indicated in this form. I also read the
                's purchasing agreement on
                {{--todo lang--}}
                @if($is_usitour)
                    <a href="https://www.usitour.com/order_agreement.html" target="_blank">//www.usitour.com/</a>
                @elseif($is_aa)
                    <a href="//www.supervacation.net/order_agreement.html" target="_blank">//www.supervacation.net</a>
                @else
                    <a href="https://www.usitrip.com/order_agreement.html" target="_blank">//www.usitrip.com/</a>
                @endif
                and agree on all the terms and conditions. </p>
            <hr>
            <h3>Signature of Card Holder: @{{ cardData.lastName }}, @{{ cardData.firstName }}</h3>
            <h3>Date: <?php echo date("n/j/Y"); ?></h3>
        </div>
        {{--print authorization form--}}
    </div>
@endsection

@section('scripts')
    {{--这里可以定义本页需要的php变量转js变量--}}
    <script>
      var source = '<?= isset($_COOKIE['source']) ? $_COOKIE['source'] : ''; ?>';
      var hotelPath = '{{ $hotelPath }}';
      var hotelRating = "{{ $hotelInfo->rating }}";
      var hotelCategory = "{{ $hotelInfo->category }}";
      var host = '<?= isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '' ?>';
      var Recorder_id = '{{$Reorderid}}'; // for function checkValidatePhone
      var phoneCountryCode = '{{$phoneCountryCode}}';
      var phoneNum = '{{$phone}}';
      {{--vue调用下面两个变量，不要写到jquery里面--}}
      var searchInfo = $.parseJSON({!! json_encode($tempInfo->search_info) !!});
      var checkIn = isUsitour ? searchInfo.checkin.substr(5, 10)+'-'+searchInfo.checkin.substr(0, 4) : searchInfo.checkin ;
      var checkOut = isUsitour ? searchInfo.checkout.substr(5, 10)+'-'+searchInfo.checkout.substr(0, 4) :searchInfo.checkout;
      {{--start language--}}
      var langPaypal = {!! json_encode(trans('paypal')) !!};
      var lang_changeCard = "{{ trans('paypal.changecard') }}";
      var lang_phoneNotEnter = "{{ trans('paypal.phone_not_enter') }}";
      var lang_codeNotEnter = "{{ trans('paypal.code_not_enter') }}";
      var lang_sendCode = "{{ trans('paypal.sendCode') }}";
      var lang_vrfCode = "{{ trans('paypal.vrfCode') }}";
      var lang_valid = "{{ trans('paypal.valid') }}";
      var lang_headTitle = "{{trans('paypal.headtitle')}}";
      var lang_tooManyTimes = "{{trans('paypal.tooManyTimes')}}";
      var lang_phoneRecheck = "{{trans('paypal.phonerecheck')}}";
      var lang_sa = "{{trans('paypal.sa')}}";
      var lang_doNotRefresh = "{{trans('paypal.do_not_refresh')}}";
      var lang_phoneNotValid = "{{trans('paypal.phonenotvalid')}}";
      var lang_cardInDan = "{{trans('paypal.cardInDan')}}";
      var lang_pleaseUploadFile = "{{trans('paypal.pleaseUploadFile')}}";
      var lang_soldout = "{{trans('book.soldout')}}";
      var lang_limit = "{{trans('book.limit')}}";
      var lang_timeout = "{{trans('book.timeout')}}";
      var lang_auth = "{{trans('book.auth')}}";
      var lang_duplicate = "{{trans('book.duplicate')}}";
      var lang_cnetwork = "{{trans('book.cnetwork')}}";
      var lang_confirmDu = "{{trans('book.confirmDu')}}";
      var lang_confirm = "{{trans('book.confirm')}}";
      var lang_cancel = "{{trans('book.cancel')}}";
      var lang_failed = "{{trans('book.failed')}}";
      var lang_cfailed = "{{trans('book.cfailed')}}";
      var lang_backtoSearch = "{{trans('search.backtoSearch')}}";
      var lang_priceChange = "{{trans('search.priceChange')}}";
        {{-- end language --}}
    </script>
    {{--webpack js--}}
    <script src="{{ asset('assets/js/mobile/plugins.paypal.js') }}"></script>
@endsection
