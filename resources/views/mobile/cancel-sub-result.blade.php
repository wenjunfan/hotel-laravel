@extends('layouts.mobile')

@section('title', 'Cancel Subscribe')

@section('css')
    <style>
        .unsub-block {
            margin-top: 250px;
            display: flex;
            justify-content: center;
            font-size: 18px;
        }
        .unsub-success {
            border: 1px solid #a8aaac;
            color: #636363;
            width: 300px;
            display: flex;
            flex-direction: column;
            align-items: center;
            padding: 18px;
            background-color: #ffffff;
        }
        .unsub-success img {
            margin-right: 20px;
        }
        .unsub-fail {
            color: #e4393c;
            border: 1px solid #a8aaac;
            padding: 18px;
            background-color: #ffffff;
            display: flex;
            flex-direction: column;
            align-items: center;
        }
    </style>
@endsection

@section('content')
    <div class="unsub-block">
        @if($success === true)
            <div v-if="langStr === 'cn'">
                <div class="unsub-success">
                    <span>
                        <img src="{{ asset('/img/general/subscribe/subscribe.png') }}" alt="取消成功">
                    </span>
                    <span>您已经取消订阅走四方邮件服务，如需获得更多酒店优惠信息，请<a href="https://www.usitrip.com">登录走四方旅游网。</a></span>
                </div>
            </div>
            <div v-else>
                <div class="unsub-success">
                    <span>
                        <img src="{{ asset('/img/general/subscribe/subscribe.png') }}" alt="取消成功">
                    </span>
                    <span>You have successfully unsubscribed from Usitour email service. Please visit <a href="https://hotel.usitour.com">https://hotel.usitour.com</a> for special promotions.</span>
                </div>
            </div>
        @else
            <div v-if="langStr === 'cn'">
                <div class="unsub-fail">
                    <span>
                        <img src="{{ asset('/img/general/subscribe/warning.png') }}" alt="取消成功">
                    </span>
                    <span>取消订阅失败</span>
                </div>
            </div>
            <div v-else>
                <div class="unsub-fail">
                    <span>
                        <img src="{{ asset('/img/general/subscribe/warning.png') }}" alt="取消成功">
                    </span>
                    <span>Fail to unsubscribe</span>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/js/mobile/cancelSubResult.js') }}"></script>
@endsection