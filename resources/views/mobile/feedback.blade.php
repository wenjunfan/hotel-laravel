@extends('layouts.mobile')

@section('title', ($is_usitour ? 'Usitour Hotel' : 'Usitrip Hotel') . ' Feedback')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/mobile/feedback.css') }}">
@endsection

@section('content')

    <?php
    $selections = [
        0 => [
            '贵',
            '跟其他网站差不多',
            '便宜',
            '特别便宜'
        ],
        1 => [
            'Expensive',
            'Fair',
            'Cheap',
            'Excellent'
        ]
    ];

    $suggestions = [
        0 => [
            'A. 酒店查找流程',
            'B. 付款流程',
            'C. 个人信息输入流程',
            'D. 网站打开速度',
            'E. 界面美观',
            'F. 以上都不是'
        ],
        1 => [
            'A. Ease with which I found my hotel',
            'B. Ease with which I paid for my hotel',
            'C. Ease with which I entered my personal details',
            'D. Ease with which I loaded the website',
            'E. Website looking',
            'F. None of above.'
        ]
    ];
    ?>

    <div class="feedback">
        <h1 class="feedback-title" v-if="!submitted">@lang('feedback.title')</h1>
        <h1 class="feedback-message" v-else>@lang('feedback.message', ['site' => $is_usitour ? 'Usitour' : 'Usitrip'])</h1>
        <form id="feedbackForm" class="feedback-details" v-if="!submitted">
            <input type="hidden" name="feedback" value="{{ $feedback }}">
            <p class="feedback-details-desc">@lang('feedback.desc_mobile')</p>
            <div class="questions-list">
                <div class="question question-1">
                    <h2>@lang('feedback.question1')</h2>
                    <div class="selection-wrapper row">
                        @foreach($selections[$language] as $index => $selection)
                            <div class="selection selection-{{ $index }} @if($language === 0) selection-chinese @endif col-xs-3">
                                <input type="radio" id="price{{ $index }}" class="selection-checkbox" name="priceSelection" value="{{ $selection }}" v-model="aboutPrice">
                                <label for="price{{ $index }}">
                                    <i class="icon icon-{{ $index }}"></i>
                                    <p>{{ $selection }}</p>
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="question question-2">
                    <h2>@lang('feedback.question2')</h2>
                    <div class="suggestion-wrapper">
                        @foreach($suggestions[$language] as $index => $suggestion)
                            <div class="suggestion suggestion-{{ $index }}">
                                <input type="checkbox" id="suggestion{{ $index }}" class="suggestion-checkbox" name="suggestion" value="{{ substr($suggestion, 3) }}" v-model="suggestions">
                                <label for="suggestion{{ $index }}" class="suggestion-label">{{ $suggestion }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="question question-3">
                    <h2>@lang('feedback.question3')</h2>
                    <div>
                        <textarea name="otherNotes" id="otherNotes" class="notes-area" v-model="otherNotes"></textarea>
                    </div>
                </div>
            </div>
            <div class="submit-wrapper">
                <button type="button" class="btn btn-submit" @click="submitForm()" :disabled="!(aboutPrice && suggestions.length > 0)">@lang('feedback.submit')</button>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/js/feedback.js') }}"></script>
@endsection