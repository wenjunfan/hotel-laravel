@extends('layouts.mobile')

@section('title', 'Upload File')

@section('css')
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('assets/css/mobile/plugins.file-upload.css') }}">
@endsection

@section('content')
    <div class="form-group">
        <div style="width:95vw; padding-left:5vw;">
            <div id="uploadValidFile" class="upload-wrapper">
                <p class="upload_file_title">@lang('paypal.secure')</p>
                <form enctype="multipart/form-data" id="imageForm" method="post">
                    <p class="upload_file_tutorial">@lang('paypal.thanksChoosingUs')</p>
                    <p class="file-upload-explain">
                        @lang('paypal.file_upload_explain')
                        @if($language == 1)<br/><br/>@endif
                        <strong>@lang('paypal.file_upload_explain_2')</strong>
                        @lang('paypal.file_upload_explain_3')
                    </p>
                    <input type="file" name="photo" id="photo" accept="image/png, image/jpeg, application/pdf"/>
                    <p class="text-right">
                        <mu-raised-button class="btn-upload" @click="upload">@lang('paypal.upload')</mu-raised-button>
                    </p>
                    <span class="demo" @click="openModal">@lang('paypal.demo')</span><br>
                </form>
                <div class="ssl"><i class="ssl-logo"></i>
                    <div class="ssl-text">@lang('paypal.secured') <br/>@lang('paypal.ssl')</div>
                </div>
            </div>
        </div>
    </div>

    <mu-dialog :open="showModal" title="@lang('paypal.demo')" @close="showModal = false">
        @if ($language == 0)
            <img class="img-responsive" src="/img/general/fraud/DemoCn.jpg">
        @else
            <img class="img-responsive" src="/img/general/fraud/DemoEn.jpg">
        @endif
        <mu-flat-button slot="actions" primary @click="showModal = false" label="OK"></mu-flat-button>
    </mu-dialog>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        new Vue({
            'el': '#app',
            data: {
                showModal: false,
            },
            mounted: function () {
                $('#hotelHead').text("{{trans('paypal.usitrip-hotel-reserve')}}");
            },
            methods: {
                openModal: function() {
                    this.showModal = true;
                },
                upload: function() {
                    if ($('#photo').val() !== '') {
                        var photo = $('#photo').prop('files')[0];
                        var formData = new FormData();

                        formData.append('photo', photo, photo.name);
                        formData.append('_token', $('meta[name="csrf_token"]').attr('content'));
                        formData.append('Reorderid', '{{$Reorderid}}');

                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', "{{url('user-file/upload')}}", true);
                        xhr.send(formData);

                        xhr.onreadystatechange = function () {
                            if (xhr.readyState === 4 && xhr.status == 200) {
                                var response = JSON.parse(xhr.response);
                                if (response.success) {
                                    $('#uploadValidFile').
                                        html('<br/><div><h3 class="text-center">Uploaded Image Preview</h3><img class="img-responsive" class="smallPic col-lg-12" id="user_file" src="' + response['url'] + '" alt="user profile"><br/><a id="ok" class="btn btn-info btn-lg pull-right btn-block">NEXT</a></div>');

                                    $('#uploadValidFile').addClass('valid');
                                    $('#fileUrl').val(response['url']);
                                    $('.loadingBtn').button('reset');
                                    $('#ok').on('click', function () {
                                        swal({
                                            title: "{{trans('paypal.safe')}}",
                                            type: "success",
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "{{trans('search.backtoSearch')}}",
                                        }, function () {
                                            window.location.href = "{{url('/search')}}";
                                        });
                                    });
                                } else if (response.errorMessage) {
                                    swal(response.errorMessage);
                                }
                            } else {
                                swal('Something went wrong');
                            }
                        }
                    } else {
                        swal({
                            title: "{{trans('paypal.pickImg')}}",
                            type: "warning",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "OK",
                        });
                    }
                }
            }
        })
    </script>
@endsection