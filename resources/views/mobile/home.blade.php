@extends('layouts.mobile')
<?php
$domain = $is_usitour ?  'Usitour' : ($is_aa ? 'America Asia' : 'Usitrip');
?>
{{--  sigle page title   --}}
@if	($language === 0)
    @section('title', '酒店预订，最便宜的北美酒店查询预订平台-' . $domain . '走四方酒店')
@elseif	($language === 2)
    @section('title', '飯店預定,最便宜的北美酒店查詢預定平台-' . $domain . '走四方飯店')
@else
    @section('title', 'Hotels: Cheap & Discount Hotel Deals, Hotel Booking | ' . $domain)
@endif
{{--single page metas--}}
@section('metas')
    @if	($language  === 0)
        <meta name="Keywords" content="便宜酒店，北美酒店，美国酒店，特价酒店"/>
        <meta name="description" content="走四方北美酒店预订平台,为您提供美国、加拿大等北美酒店在线预订服务,您可以实时查询酒店房型价格、房态、品牌、设施、电话、地址、星级等信息,在线预订北美酒店,享受更多优惠价格。">
    @else
        <meta name="Keywords" content="hotels, motel, cheap hotels, booking hotel"/>
        <meta name="description" content="{{$domain . '\'s Hotel provides 350,000 hotels worldwide with great value. Book now for instant savings with our 100% price guarantee policy.'}}">
    @endif
@endsection

@section('gtag_events')
    @if(config('app.enable_tracking') && !$is_b && !$is_aa)
        @php
            $subscribe_con_label = $is_usitour ? 'AW-825564842/iqv3CIKmgoYBEKq91IkD' : 'AW-825564842/BTHxCKb47noQqr3UiQM';
        @endphp
        <!-- Event snippet for 领券 conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
        <script>
            function gtag_report_conversion_subscribe(url) {
                var callback = function () {
                    if (typeof(url) !== 'undefined') {
                        window.location = url;
                    }
                };
                gtag('event', 'conversion', {
                    'send_to': '{{ $subscribe_con_label }}',
                    'value': 1.0,
                    'currency': 'USD',
                    'event_callback': callback
                });
                return false;
            }
        </script>
    @endif
@endsection

@section('css')
    <link rel="canonical" href="{{ config('app.url') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/mobile/home.css') }}">
@endsection

{{--  sigle page main content   --}}
@section('content')
    <div class="home">
        <div v-if="isUsitour">
            <icon-buttons></icon-buttons>
        </div>

        {{--search params display block--}}
        <form id="search-form" action="#" :class="{'usitour-searchWrapper': isUsitour}">
            <input type="hidden" name="scenicId" v-model="destination.scenicId">
            <input type="hidden" name="desName" v-model="destination.name">
            <input type="hidden" name="scenicName" v-model="destination.scenicName">
            <input type="hidden" name="checkin" v-model="searchData.checkInDate">
            <input type="hidden" name="checkout" v-model="searchData.checkOutDate">
            <input type="hidden" name="roomCount" v-model="searchData.roomCount">

            {{--title--}}
            <h1 class="title-1 title-home">
                <span v-if="isUsitour">Search Hotels</span><span v-else>@{{langHome.hotel}}</span>
            </h1>
           
            {{--destination--}}
            <home-input v-on:openpopup="openPopup" :title="langHome.hintTextGo" :hinttext="langHome.hintTextGo" :inputicon="inputIconMap"></home-input>

            {{--search popup--}}
            <search-popup :destination-name="destination.name" :destination-id="destination.id" :scenic-name="destination.scenicName" :scenic-id="destination.scenicId" v-on:set-close="setPopupClose" v-on:set-des="setDestination" :is-pop-open="searchPopup"></search-popup>

            {{-- check-in & check-out --}}
            <date-picker v-on:changedates="changeDate" :checkin="checkInDate" :checkout="checkOutDate"></date-picker>

            {{--room, adult，child--}}
            <home-selection v-on:changeselections="changeSelections" :roomnum="searchData.roomCount" :adultnum="searchData.adult" :childnum="searchData.child" :child-ages="childAges"></home-selection>

            <mu-raised-button :label="langHome.labelSearch" :class="['submit-btn', isUsitour ? 'usitour' : '']" primary @click="submitSearch"></mu-raised-button>
        </form>

        <div class="divider-home" v-if="!isUsitour"></div>

        @if(!$is_aa)
        {{--why us section--}}
        <div class="why-us-section" v-if="!is117book">
            <p :class="['why-us-title', isUsitour ? 'text-uppercase' : '']">@{{ langHome.whyUsTitle }}</p>
            <div :class="['why-us-block', isUsitour ? 'usitour' : '']">
                <mu-row class="why-us-block-row1" gutter>
                    <mu-col width="10" tablet="10" desktop="10">
                        <i :class="[isUsitour ? 'price-guaranteed why-us-icon-usitour' : 'usitrip_mobile_all_img check-icon-red why-us-icon']"></i>
                    </mu-col>
                    <mu-col width="40" tablet="40" desktop="40">
                        <span class="why-us-text">@{{ langHome.why11 }} @{{ langHome.why12 }}</span>
                    </mu-col>
                    <mu-col width="10" tablet="10" desktop="10">
                        <i :class="[isUsitour ? 'hot-line why-us-icon-usitour' : 'usitrip_mobile_all_img check-icon-red why-us-icon']"></i>
                    </mu-col>
                    <mu-col width="40" tablet="40" desktop="40">
                        <span class="why-us-text">@{{ langHome.why21 }} @{{ langHome.why22 }}</span>
                    </mu-col>
                </mu-row>
                <mu-row gutter>
                    <mu-col width="10" tablet="10" desktop="10">
                        <i :class="[isUsitour ? 'secured why-us-icon-usitour' : 'usitrip_mobile_all_img check-icon-red why-us-icon']"></i>
                    </mu-col>
                    <mu-col width="40" tablet="40" desktop="40">
                        <span class="why-us-text">@{{ langHome.why31 }} @{{ langHome.why32 }}</span>
                    </mu-col>
                    <mu-col width="10" tablet="10" desktop="10">
                        <i :class="[isUsitour ? 'instant why-us-icon-usitour' : 'usitrip_mobile_all_img check-icon-red why-us-icon']"></i>
                    </mu-col>
                    <mu-col width="40" tablet="40" desktop="40">
                        <span class="why-us-text">@{{ langHome.why41 }} @{{ langHome.why42 }}</span>
                    </mu-col>
                </mu-row>
            </div>
        </div>
        {{--affiliate banner--}}
        @if(!$is_b && !$is_aa)
                <div style="margin-bottom:18px; width:100%;"><a href="/affiliate/register"><img src="/img/general/affiliate/affiliate-m-{{$language}}.jpg"></a></div>
        @endif
        {{--affiliate banner--}}

        {{--hot sales banner--}}
        <p :class="['sales-today-title', isUsitour ? 'text-uppercase' : '']" v-if="!is117book">@{{ langHome.salesToday }}</p>

        <home-scenic-pic v-on:search-hotels="showDiscountHotels" v-if="!is117book"></home-scenic-pic>

        {{--subscribe--}}
        <div class="subscribe" v-if="!is117book">
            <mu-flat-button class="subscribe-title" @click="showSubDetail = !showSubDetail">
                <span :class="{'usitour-text-dark': isUsitour}">@lang('home.subLabel1')</span>
                <span class="color-red">&nbsp;@lang('home.subLabel2')</span><span>!</span>
            </mu-flat-button>

            <form class="subscribe-inputs" accept-charset="UTF-8" id="leftCouponBox" method="POST" v-if="!subscribeMsg">
                {!! csrf_field() !!}
                <mu-text-field :hint-text="'@lang('home.subEmailHint')'" class="email-wrapper" id="v" name="subscribeEmail" :hint-text-class="['email-hint', {'usitour': isUsitour}]" v-model="subscriberEmail"></mu-text-field>
                <div class="subscribe-button">
                    <button id="subscribeBtn" @click="subscribe" :class="{'usitour-bg-blue': isUsitour}" type="button">
                        @lang('home.subscribe')
                    </button>
                </div>
            </form>
            <p :class="['subscribe-notes', isUsitour ? 'usitour-text-dark' : '']">
                <span v-if="subscribeMsg">@{{ subscribeMsg }}</span>
                <span v-else-if="isUsitour">@lang('list.subNotes')</span>
                <span v-else>@lang('home.subNotes')</span>
            </p>
        </div>

        {{--partner--}}
        <section v-if="!is117book">
            <p :class="['partner-title', isUsitour ? 'text-uppercase' : '']">@{{ langHome.partner }}</p>
            <div class="partner-block usitour" v-if="isUsitour">
                <mu-row class="partner-block-row1">
                    <mu-col width="50" tablet="25" desktop="25">
                        <i src="#" alt="Expedia" class="partner-expedia partner-logo"></i>
                    </mu-col>
                    <mu-col width="50" tablet="25" desktop="25">
                        <i src="#" alt="Expedia" class="partner-priceline partner-logo"></i>
                    </mu-col>
                    <mu-col width="50" tablet="25" desktop="25">
                        <i src="#" alt="Expedia" class="partner-visitusa partner-logo"></i>
                    </mu-col>
                    <mu-col width="50" tablet="25" desktop="25">
                        <i src="#" alt="Expedia" class="partner-la partner-logo"></i>
                    </mu-col>
                </mu-row>
            </div>
            <div class="partner-block" v-else>
                <mu-row gutter>
                    <mu-col width="50" tablet="25" desktop="25">
                        <img src="/img/general/mobile/logo1.jpg" class="partner-box partner-ex">
                    </mu-col>
                    <mu-col width="50" tablet="25" desktop="25">
                        <img src="/img/general/mobile/logo2.jpg" class="partner-box partner-pl">
                    </mu-col>
                    <mu-col width="50" tablet="25" desktop="25">
                        <img src="/img/general/mobile/logo3.jpg" class="partner-box partner-usa">
                    </mu-col>
                    <mu-col width="50" tablet="25" desktop="25">
                        <img src="/img/general/mobile/logo4.jpg" class="partner-box partner-los">
                    </mu-col>
                </mu-row>
            </div>
        </section>
        @endif
        {{--city banners--}}
        <p class="rec-city-title">
            <span class="text-uppercase" v-if="isUsitour">Popular Destinations</span>
            <span v-else>@{{ langHome.recCity }}</span>
        </p>
        <home-pic v-on:search-hotels="showDiscountHotels"></home-pic>
    </div>
@endsection

@section('scripts')
    @include('includes.popup-cities')

    <script>
        var langHome = {!! json_encode(trans('home')) !!};

       {{--这里可以定义本页需要的php变量转js变量--}}
        var sessionKey = '{{ uniqid() }}';
    </script>
    {{-- 源文件： resources/assets/js/mobile/home.js --}}
    <script src="{{ asset('assets/js/mobile/home.js') }}"></script>
@endsection
