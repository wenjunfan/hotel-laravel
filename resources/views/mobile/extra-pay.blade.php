{{--fraud用户补差价页面 mobile--}}
@extends('layouts.mobile')

@section('title', 'Credit Card')

<?php
$Suspicious = \App\Model\SuspiciousOrder::where('Reorderid', $Reorderid)->first();
$searchedInfo = json_decode($Suspicious->search_info, true);
$porder_tem = DB::table('porder_tem')->where('Reorderid', $Reorderid)->first();
$orgPrice = $Suspicious->currency . ' ' . $Suspicious->amount;
$hotelName = $porder_tem->goods_name;
$canOrNot = $roomDetails['rateClass'];
$cancelDate = isset($roomDetails['cancelDate']) ? substr(str_replace("T", " ", $roomDetails['cancelDate']), 0, 19) : '';
$totalPrice = $roomDetails['netPrice'];
$beforeTax = floor($totalPrice * .88 / $roomDetails['dayCount'] / $roomDetails['roomCount']);
$tax = $totalPrice - ($beforeTax * $roomDetails['dayCount'] * $roomDetails['roomCount']);
$leftPrice = $totalPrice - $Suspicious->amount;
?>

@section('css')
    <style>
        label {
            font-size: 18px;
        }
        .caption label {
            font-size: 14px;
        }
        .text-danger {
            color: #e4393c;
        }
        .error {
            color: #e4393c;
        }
        .form-group {
            margin-bottom: 20px;
        }
        .mu-text-field {
            width: 100%;
        }
        .card-form {
            margin: 0 15px;
            padding: 0 15px;
        }
        .card-input {
            font-size: 18px;
            border: 1px solid #ccc;
            border-radius: 4px;
            background-color: #fff;
            padding: 0 12px;
            width: 100%;
        }
        .card-input-addon {
            font-size: 18px;
        }
        .card-input-notes {
            font-size: 13px;
            margin-top: 5px;
            color: #8a6d3b;
        }
        .declineBtn {
            margin-top: 15px;
        }
        .price-detail label {
            font-size: 14px;
        }
    </style>
@endsection

@section('content')
    <div style="color: #484848;">
        <h2 style="text-align: center;">
            @lang('paypal.title')
        </h2>
        <mu-row>
            <mu-col width="100" tablet="50" desktop="50">
                <form role="form" id="creditform" class="card-form">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label class="control-label" for="number">
                            @lang('refunds.cn')
                        </label>
                        {{--卡号--}}
                        <div>
                            <div class="input-group">
                                <mu-flexbox>
                                    <mu-flexbox-item grow="11">
                                        <input type="text" class="card-input" name="number" placeholder="@lang('paypal.cn')"
                                                minlength="15" maxlength="16" v-model="cardData.number" required/>
                                    </mu-flexbox-item>
                                    <mu-flexbox-item>
                                        <span class="card-input-addon"><i class="fa fa-credit-card"></i></span>
                                    </mu-flexbox-item>
                                </mu-flexbox>
                            </div>
                            <div class="input-group">
                                <label class="f-13 text-warning text-i">
                                    @if($language == 0)
                                        请使用此订单使用的信用卡进行支付
                                    @elseif($language == 2)
                                        請使用此訂單使用的信用卡進行支付
                                    @else
                                        Please use previous card:
                                    @endif
                                    （******{{substr($ccNumber,4,4)}}）
                                </label>
                            </div>
                        </div>
                    </div>
                    {{--信用卡在线付款--}}
                    <div class="form-group">
                        <label class="control-label" for="cardType">@lang('paypal.ct')</label>
                        <div>
                            <img style="height:40px;width: 100%;max-width: 289px;" src="/img/payment/all-major-credit-cards.png"/>
                            <input type="hidden" id="cardType" name="cardType" value=""/>
                        </div>
                    </div>
                    {{--有效日期--}}
                    <div class="form-group">
                        <label class="control-label" for="expirationDate">@lang('paypal.ed')</label>
                        <mu-row>
                            <mu-col width="50" tablet="50" desktop="50">
                                <mu-select-field v-model="cardData.month" hint-text="{{ trans('paypal.month') }}" class="card-input">
                                    <mu-menu-item v-for="n in 13" :key="n" :value="n < 10 ? ('0' + n) : n.toString()"
                                            :title="n + (langStr === 'cn' ? '月' : '')"></mu-menu-item>
                                </mu-select-field>
                            </mu-col>
                            <mu-col width="50" tablet="50" desktop="50">
                                <mu-select-field v-model="cardData.year" hint-text="{{ trans('paypal.year') }}" class="card-input">
                                    <mu-menu-item v-for="n in 9" :key="n" :value="(17+n).toString()"
                                            :title="'20' + (17+n) + (langStr === 'cn' ? '年' : '')"></mu-menu-item>
                                </mu-select-field>
                            </mu-col>
                        </mu-row>
                    </div>
                    {{--CVS验证码--}}
                    <div class="form-group">
                        <label class="control-label" for="CVC">@lang('paypal.cvc')</label>
                        <div>
                            <input type="text" class="card-input" name="CVC" placeholder="CVC" minlength="3" maxlength="4"
                                    v-model="cardData.cvc" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <mu-raised-button class="mu-raised-button-full" label="{{ trans('paypal.pmt') }}"
                                @click="submitPayment" primary></mu-raised-button>
                        <mu-raised-button class="declineBtn mu-raised-button-full" label="{{ trans('paypal.requestDecline') }}"
                                @click="fraudEmail"></mu-raised-button>
                    </div>
                </form>
            </mu-col>

            <mu-col width="100" tablet="50" desktop="50">
                <div style="border: 1px solid #e7eaec; margin: 0 15px 2rem;padding: 0 15px 1em;">
                    <div class="caption">
                        <h3 style="font-size:18px; font-weight:500;">
                            <span id="hotel-name">{{$hotelName}}</span>
                        </h3>
                    </div>
                    <div class="caption">
                        <label>
                            @lang('book.checkin'):
                        </label>
                        <label style="float:right;" class="control-label">{{$searchedInfo['checkin']}}</label>
                    </div>
                    <div class="caption">
                        <label>
                            @lang('book.checkout'):
                        </label>
                        <label style="float:right;" class="control-label">{{$searchedInfo['checkout']}}</label>
                    </div>

                    <!--取消政策start-->
                    <div class="border-bottom" style="border-bottom: 1px solid lightgrey !important; padding-bottom:15px;">
                        <label>
                                <span style="font-size:14px;">
                                    @lang('book.canc'):
                                </span>
                        </label>
                        <div id="hotel-cancel" style="color:#0065b8; font-size:14px">
                            @if($canOrNot == '可取消' || $cancelDate)
                                @lang('book.freeb') {{$cancelDate}} @lang('book.ltime')
                            @else
                                @lang('book.non-ref')
                            @endif
                        </div>
                    </div>
                    <!--取消政策end-->

                    <!--价格明细start-->
                    <div class="price-detail" style="margin-right:5px;">
                        <h3 style="font-weight:400; font-size:18px;">
                            @lang('book.soc')
                        </h3>
                        <mu-row>
                            <mu-col width="50" tablet="50" desktop="50">
                                <label style="font-size:14px;">
                                    @lang('book.rooms'):
                                    <span style="font-size:16px; font-weight:500; color:#484848;">{{$roomDetails['roomCount']}}</span>
                                </label>
                            </mu-col>
                            <mu-col width="50" tablet="50" desktop="50">
                                <label style="font-size:14px;">
                                    @lang('book.nights'):
                                    <span style="font-size:16px; font-weight:500; color:#484848;">{{$roomDetails['dayCount']}}</span>
                                </label>
                            </mu-col>
                        </mu-row>
                        <mu-row>
                            <mu-col width="50" tablet="50" desktop="50">
                                <label style=" font-size:14px;">
                                    @lang('book.roomcost'):
                                    <span style="font-size:16px; font-weight:500; color:#484848;">{{$roomDetails['currency']}}
                                        <span class="roomPrice">{{$beforeTax}}</span>
                                    </span>
                                </label>
                            </mu-col>
                            <mu-col width="50" tablet="50" desktop="50">
                                <label style="font-size:14px;">
                                    @lang('book.tax'):
                                    <span style="font-size:16px; font-weight:500; color:#484848;">
                                        {{$roomDetails['currency']}}
                                        <span id="roomTax">&nbsp;{{$tax}}</span>
                                    </span>
                                </label>
                            </mu-col>
                        </mu-row>
                        <label style="font-size:15px; color:#484848;">
                            @lang('book.totalcharge'):
                        </label>
                        <span style="float:right;" class="text-right text-danger">
                            <span style="font-weight:500">{{$roomDetails['currency']}}</span>&nbsp;
                            <span class="totalPrice" style="font-weight:500">{{$totalPrice}}</span>
                        </span>
                        <br/>
                        <label style="font-size:15px; color:#484848;">
                            @lang('orders.TotalPaid'):
                        </label>
                        <span style="float:right;" class="pull-right">
                            <span class="compare" style="font-weight:500">({{$orgPrice}})</span>
                        </span>
                    </div>
                    <div style="border-top: 1px solid lightgrey;margin-top: 15px;padding-top: 15px;">
                        <label style="font-size:15px; color:#484848;">
                            @lang('paypal.leftToPay'):
                        </label>
                        <span style="float:right; margin-top:-15px; margin-right:10px;" class="text-right">
                            <span class="compare" style="font-size:30px; font-weight:500">
                                {{$roomDetails['currency'] . ' ' .$leftPrice}}
                            </span>
                        </span>
                    </div>
                    <!--价格明细end-->
                </div>
            </mu-col>
        </mu-row>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var app = new Vue({
            el: '#app',
            data: {
                cardData: {
                    number: '',
                    // cardType: 'Visa',
                    month: '01',
                    year: '20',
                    cvc: '',
                    phone: '',
                    firstName: '',
                    lastName: '',
                    street: '',
                    street2: '',
                    city: '',
                    state: '',
                    zip: '',
                    countryCode: 'US'
                }
            },
            mounted: function () {
                $("#creditform").validate();
            },
            methods: {
                fraudEmail: function () {
                    $.post("{{url('/potential-order/decline/' . $Reorderid)}}", {
                        '_token': $("input[name='_token']").val(),
                    }, function (data) {
                        if (data.success) {
                            swal({
                                title: "{{trans('paypal.declineok')}}",
                                type: "success",
                                confirmButtonColor: "#dd6b55",
                                confirmButtonText: "OK",
                            }, function () {
                                window.location.href = "{{url('/search')}}";
                            });
                        } else {
                            swal({
                                title: "{{trans('paypal.declinefailed')}}",
                                type: "warning",
                                confirmButtonColor: "#dd6b55",
                                confirmButtonText: "OK",
                            }, function () {
                                window.location.href = self.location;
                            });
                        }
                    });
                },
                submitPayment: function () {
                    let self = this;
                    let form = $('#creditform');
                    if (form.valid()) {
                        $.post('{{url('/paypal/extra-pay/submit/' . $Reorderid)}}', form.serialize(), function (data) {
                            self.checkOrderStatus(data)
                        });
                    }
                },
                checkOrderStatus: function (data) {
                    if (data.voucherUrl) {
                        window.location.replace(data.voucherUrl);
                    } else if (data === '') {
                        var message;
                        if (language === 0) {
                            message = '您已成功预订，我们会稍后发送您的预订成功邮件';
                        } else {
                            message = 'Thanks! Your hotel reservation is confirmed, and your confirmation Email is on the way';
                        }
                        swal({
                            title: message,
                            type: 'success',
                            confirmButtonColor: '#dd6b55',
                            allowOutsideClick: false,
                            confirmButtonText: 'OK',
                        }, function () {
                            window.location.href = '/';
                        });
                    } else if (data.errorId === "CC15006") {
                        swal({
                            title: data.errorMessage,
                            type: "warning",
                            confirmButtonColor: "#dd6b55",
                            confirmButtonText: "OK",
                        });
                    } else if (data["errorId"] === "CC15005") {
                        swal({
                            title: "{{trans('book.auth')}}",
                            type: "warning",
                            text: data.message,
                            confirmButtonColor: "#dd6b55",
                            confirmButtonText: "OK",
                        }, function () {
                            window.location.href = self.location;
                        });
                    } else if (data['errorId'] === 'ER1415') { // for duplicate order check
                        swal({
                            title: "Maybe duplicate order, do you still want to continue?",
                            type: 'warning',
                            confirmButtonText: "{{trans('book.confirm')}}",
                            showCancelButton: true,
                            cancelButtonText: "{{trans('book.cancel')}}",
                        }, function (confirmed) {
                            if (confirmed) {
                                $.post('/duplicate/proceed', {
                                    '_token': $('meta[name="csrf_token"]').attr('content'),
                                    'action': 'continue',
                                    'Reorderid': data.Reorderid,
                                    'vendor': 'cc',
                                    'p_id': data.p_id,
                                }, function (data) {
                                    checkOrderStatus(data);
                                });
                            } else {
                                $.post('/duplicate/proceed', {
                                    '_token': $('meta[name="csrf_token"]').attr('content'),
                                    'action': 'cancel',
                                    'Reorderid': data.Reorderid,
                                    'vendor': 'cc',
                                    'p_id': data.p_id,
                                }, function (data) {
                                    if (!data.success) {
                                        data['message'] = '{{trans('book.failed')}}';
                                    }

                                    swal({
                                        title: data.message,
                                        type: 'warning',
                                        confirmButtonText: "OK",
                                    }, function () {
                                        window.location.href = '/';
                                    });
                                });
                            }
                        });
                    } else if (data.errorId === "ER1401" || data.errorId === "ER1301") {
                        swal({
                            title: "{{trans('book.soldout')}}",
                            type: "warning",
                            confirmButtonColor: "#dd6b55",
                            confirmButtonText: "{{trans('search.backtoSearch')}}",
                        }, function () {
                            window.location.href = "{{url('/search')}}";
                        });
                    } else if (data["errorId"] === "ER1411" || data["errorId"] === "ER1402") {
                        swal({
                            title: "{{trans('search.priceChange')}}",
                            type: "warning",
                            confirmButtonColor: "#dd6b55",
                            confirmButtonText: "{{trans('search.backtoSearch')}}",
                        }, function () {
                            window.location.href = "{{url('/search')}}";
                        });
                    } else if (data["errorId"] === "IT1001") {
                        swal({
                            title: "{{trans('book.cnetwork')}}",
                            type: "warning",
                            text: data.message,
                            confirmButtonColor: "#dd6b55",
                            confirmButtonText: "{{trans('search.backtoSearch')}}",
                        }, function () {
                            window.location.href = "{{url('/search')}}";
                        });
                    } else if (data.errorId) {
                        swal({
                            title: data.errorId,
                            type: "warning",
                            confirmButtonColor: "#dd6b55",
                            confirmButtonText: "{{trans('search.backtoSearch')}}",
                        }, function () {
                            window.location.href = "{{url('/search')}}";
                        });
                    } else {
                        swal({
                            title: "Try again",
                            type: "warning",
                            confirmButtonColor: "#dd6b55",
                            confirmButtonText: "OK",
                        }, function () {
                            window.location.href = self.location;
                        });
                    }
                },
            }
        });
    </script>
@endsection