@extends('layouts.mobile')
<?php
$hotel_name = isset($hotel_name) ? $hotel_name : '';
$hotelNameZh = isset($hotel->name_zh) ? $hotel->name_zh : $hotel->name;
$domain = $is_usitour ?  'Usitour' : $is_aa ? 'America Asia' : 'Usitrip';
?>
{{--  sigle page title   --}}
@if	(0 === (int)$language)
    @section('title', '' . $hotelNameZh . '预订,' . $hotelNameZh . '特价,折扣,优惠-' . $domain . '走四方旅游网')
@elseif	(2 === (int)$language)
    @section('title', '' .  $hotel->name. '預定,' .  $hotel->name . '特價,折扣,優惠-' . $domain . '走四方旅旅遊網')
@else
    @section('title', '' . $hotel->name . ' - Hotel Deals & Photos -  ' . $domain)
@endif

{{--  sigle page description and keywords   --}}
@section('metas')
    @if	($language  === 0)
        <meta name="Keywords" content="{{$hotelNameZh}}预订,{{$hotelNameZh}}特价,{{$hotelNameZh}}折扣" >
        <meta name="description" content="走四方国际酒店预订平台,为您提供{{$hotelNameZh}}在线预订服务,您可以实时查询{{$hotelNameZh }}的房间价格、房态、设施、电话、地址、星级等信息,选择最优惠、最让您满意的房型。">
    @else
        <meta name="Keywords" content="{{$hotel->name}} reserve,{{$hotel->name}} special deals,{{$hotel->name}} discount"/>
        <meta name="description" content="{{$domain}}'s Hotel provides the best room deals in {{$hotel->name}}.Book now for instant savings in {{$hotel->name}} with our 100% price guarantee policy.">
    @endif
@endsection

@section('gtag_events')
    @if(config('app.enable_tracking') && !$is_b && !$is_aa)
        <!-- Event snippet for 酒店预订 conversion pageIn your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
        @php
            $book_con_label = $is_usitour ? 'AW-825564842/NF5XCO2d3oUBEKq91IkD' : 'AW-825564842/k0XkCNfbxXoQqr3UiQM';
        @endphp
        <script>
            function gtag_report_conversion_book(url) {
                var callback = function () {
                    if (typeof(url) != 'undefined') {
                        window.location = url;
                    }
                };
                gtag('event', 'conversion', {
                    'send_to': '{{ $book_con_label }}',
                    'value': 1.0,
                    'currency': 'USD',
                    'transaction_id': '',
                    'event_callback': callback
                });
                return false;
            }
        </script>

        {{--adwords remarketing tag--}}
        <script>
            var searchInfo = {!! json_encode($searchInfo) !!};

            var today = new Date();
            todayStr = today.getFullYear() + '-' + today.getMonth() + 1 + '-' + today.getDate();
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            tomorrowStr = tomorrow.getFullYear() + '-' + tomorrow.getMonth() + 1 + '-' + tomorrow.getDate();
            var checkin = searchInfo.checkin ? searchInfo.checkin : todayStr;
            var checkout = searchInfo.checkout ? searchInfo.checkout : tomorrowStr;

            gtag('event', 'page_view', {
                'send_to': 'AW-825564842',
                'hrental_enddate': checkout,
                'hrental_id': '{{$hotel->hotelId}}',
                'hrental_startdate': checkin,
                'hrental_totalvalue': {{ $hotel->originalPrice or 0 }}
            });
        </script>
    @endif
@endsection

{{--  sigle page style   --}}
@section('css')
    <link rel="canonical" href="{{ config('app.url') }}/hotel/{{ $hotel->hotelId }}.html">
    <link rel="stylesheet" href="{{ asset('assets/css/mobile/hotel.css') }}">
@endsection

{{--  sigle page main content   --}}
@section('content')
    {{--hotel pics--}}
    <hotel-images :images="imgUrlArray" :hotel="hotel" :hide="hidePics" v-if="!hidePics" @closepics="closePicsPop">
    </hotel-images>

    {{--hotel detail--}}
    <div class="hotelDetail">
        <mu-row gutter>
            {{--back button and hotel name--}}
            <mu-col width="10" tablet="10" desktop="10" class="icon-goback">
                <i class="fa fa-chevron-left left-arrow" onclick="window.goBackIfHistory();return false;"></i>
            </mu-col>
            <mu-col width="90" tablet="90" desktop="90" class="hotelDetail-title">
                <h1>{{$hotel->name}}<span v-if="langStr ==='cn' && hotel.name_zh">(@{{hotel.name_zh}})</span></h1>
                <p class="hotelDetail-rating"><i v-for="n in hotel.rating" class="fa fa-star"></i></p>
            </mu-col>
        </mu-row>

        <mu-content-block class="hotelDetail-extra-wrapper">
            <mu-row gutter class="hotelDetail-extra">
                <mu-col width="60" class="guest-rating">
                    <span class="guest-rating-line1">
                        @if($hotel->guestRating)
                            @if($hotel->guestRating > 4.7)
                                @lang('hotel.exceptional')
                            @elseif($hotel->guestRating >= 4.4)
                                @lang('hotel.wonderful')
                            @elseif($hotel->guestRating >= 4.0)
                                @lang('hotel.excellent')
                            @else
                                @lang('hotel.very_good')
                            @endif
                        @else
                            @lang('hotel.expedia_rate')
                        @endif
                        <span class="guest-rating-score">
                            <span v-if="hotel.guestRating">@{{hotel.guestRating}}</span>
                            <span v-else>N/A</span>
                        </span>
                    </span>@if(empty($hotel->guestRating))<br>@endif
                    <span class="guest-rating-line2" v-if="hotel.guestReviewCount">
                        <a @click="goGuestReview">(@{{ hotel.guestReviewCount }} @{{langHotel.review }})</a>
                    </span>
                </mu-col>
                <mu-col width="40" class="hotelDetail-price" v-show="hotel.minPrice > 0">
                    <p class="hotelDetail-price-title" v-show="hotel.minPrice > 0"> @{{ langHotel.pricePerNit }}</p>
                    <p class="hotelDetail-price-detail">
                        <del class="origin-price">
                            <span v-if="hotel.currency=='USD'">$</span>
                            <span v-if="hotel.currency!='USD'">@{{hotel.currency}}</span>
                            @{{hotel.originalPrice}}
                        </del>
                        <span class="min-price">
                            <span v-if="hotel.currency=='USD'">$</span>
                            <span v-if="hotel.currency!='USD'">@{{hotel.currency}}</span>
                            @{{hotel.minPrice}}
                        </span>
                    </p>
                </mu-col>
            </mu-row>
        </mu-content-block>
    </div>
    {{--/hotel detail--}}

    @if(in_array($hotel->hotelId, $countdown_hotel))
        <div v-if="isUsitour" style="background-image:url('/img/general/countdown/cancun-countdown-mobile.png');height:258px;" class="hotel-main-img">
            <button class="count-down-btn">
                Sales End In:  <span id="countdown"></span>
            </button>
        </div>
        <hr v-if="isUsitour" class="mu-divider m-t-20 m-b-20">
    @endif

    @php
        if($language == 2){
            if($is_aa){
                  $thumbnail = '/img/error/aa_img_error.gif';
            }else{
                $thumbnail = '/img/error/tw_img_error.png';
            }
        }else{
         if($is_usitour){
                $thumbnail = '/img/error/usitour_img_error.png';
            }else if($is_b){
                 $number = rand(1, 10);
                 $thumbnail = '/img/error/DefaultImage_'.$number.'.jpg';
             }else{
                $thumbnail = '/img/error/hotel_img_error.jpg';
            }
        }
    @endphp
    {{--hotel main pic--}}
    <div class="hotel-images">
        <div class="icon-more-pic" @click="hidePics=false" v-if="imgUrlArray.length > 0">
            <i class="fa fa-image"></i> More
        </div>
        <div class="hotel-main-img">
            <img src="{{ $hotel->thumbnail }}" onerror="this.src = '{{ $thumbnail }}'" alt="{{ $hotel->name }}" width="100%" height="100%">
        </div>
    </div>

    {{--popular facility--}}
    @if(isset($hotel['hotel_facilities']) && isset($hotel['hotel_facilities']['popular']))
        <div class="facilities-banner-wrapper">
            <h4 class="facilities-banner-title">@lang('hotel.popular_facility')</h4>
            <div class="facilities-banner facility-small">
                @foreach($hotel['hotel_facilities']['popular'] as $index => $facility)
                    <div class="facilities-banner-item facility-item">
                        <i class="facility-icon facility-icon-{{ $facility['facilityCode'] }}"></i>
                        <span class="facility-text">{{ $facility['facilityName'] }}</span>
                    </div>
                    @break($index === 3)
                @endforeach
                <div class="facilities-banner-item facility-item more-facility-anchor">
                    <i class="facility-icon facility-icon-more"></i>
                    <span class="facility-text">More</span>
                </div>
            </div>
        </div>
    @endif

    {{--hotel map--}}
    <div id="mapAnchor" class="hotel-map-anchor" @click="toggleMap">
        <img src="#" alt="hotel map">
        <div class="map-viewer">@{{ langHotel.showMap }}</div>
    </div>

    <mu-content-block class="hotel-address">
        <i class="fa fa-map-marker-alt"></i> {{ ucwords(strtolower($hotel->address))}}
    </mu-content-block>

    <div id="mapWrapper" class="map-wrapper">
        <div id="mapBanner" class="map-banner" @click="toggleMap">
            <i class="fa fa-angle-left"></i>
            <div>
                <div class="map-banner-desName">
                    @{{ hotel.name }}
                    <span v-if="langStr ===  'cn' && hotel.name_zh">
                        (@{{ hotel.name_zh }})
                    </span>
                </div>
                <div> @{{ formattedCheckin }} - @{{ formattedCheckout }}</div>
            </div>
        </div>
        <div id="infowindow" class="infowindow" v-if="showInfowindow && infoHotel">
            <info-hotel :hotel="infoHotel" :session-key="infoHotel.sessionKey" :search-data="searchData"
                    :destination-city="hotel.city">
            </info-hotel>
        </div>
        <div id="map" class="map"></div>
    </div>
    {{--/hotel map--}}

    {{--hotel description--}}
    <div class="hotel-description" v-if="hotel.description || (hotel.description_zh && langStr === 'cn')">
        <p class="clamp">
            <span class="real-desc">
                <span v-if="hotel.description_zh && langStr === 'cn'">@{{ hotel.description_zh }}<br/></span>
                <span v-if="hotel.description">@{{ hotel.description }}</span>
            </span>
        </p>
        <mu-flat-button @click="toggleClamp" class="toggle-button" >
            <span v-if="viewButton">@{{ viewButton }}</span>
            <span v-else>@{{ langHotel.viewMore }}</span>
        </mu-flat-button>
    </div>
    {{--/hotel description--}}

    {{--new search --}}
    <mu-sub-header class="hotel-search-header">@{{ langHotel.chooseRoom }}</mu-sub-header>

    <form action="#" id="search-form" role="form" enctype="multipart/form-data">
        <input type="hidden" name="hotelId" id="hotelId" value="{{ $hotel->hotelId }}">
        <input type="hidden" name="destinationId" value="{{ $hotel->desId }}">
        <input type="hidden" name="sessionKey" :value="sessionKey">
        <input type="hidden" name="dayCount" value="{{ $dayCount }}">
        <input type="hidden" name="checkin" v-model="searchData.checkin">
        <input type="hidden" name="checkout" v-model="searchData.checkout">
        <input type="hidden" name="passedPartnerId" value="{{isset($passedpartnerId)? $passedpartnerId : ''}}">

        <mu-content-block>
            <date-picker v-on:changedates="changeDate" :checkin="searchDataTemp.checkin" :checkout="searchDataTemp.checkout">
            </date-picker>
            {{--房间数，成人，儿童--}}
            <home-selection v-on:changeselections="changeSelections" :roomnum="searchDataTemp.roomCount"
                    :adultnum="searchDataTemp.adultCount1" :childnum="searchDataTemp.childCount1" :child-ages="childAges"></home-selection>
            <mu-raised-button :label="langHotel.update" class="submit-btn" primary @click="submitSearch"></mu-raised-button>
        </mu-content-block>
    </form>

    <mu-divider class="m-t-20 m-b-20"></mu-divider>

    {{--loading rooms--}}
    <div v-show="!showRooms">
        <div class="sk-spinner sk-spinner-three-bounce" style="height: 100px; padding-top: 30px;">
            <div class="sk-bounce1"></div>
            <div class="sk-bounce2"></div>
            <div class="sk-bounce3"></div>
        </div>
    </div>

    {{--hotel room loop--}}
    <div class="hotel-rooms" v-show="showRooms">
        <div v-if="roomsLess.length > 0">
            <room-item v-for="(room,index) in (showMoreRoom? roomsLess : rooms)" :key="room.reference" :room="room"
                    :day-count="searchData.dayCount" :facilities="facilities" :hotel-id="hotel.hotelId" :hotel-name="hotel.name"
                    :room-num="searchData.roomCount" :session-key="sessionKey" :reorderid="Reorderid" :is-ad="isAd" :is-api="isApi"
                    :params="searchParams" :see-provider="seeProvider" :img-path="extraImgPath"
                    v-on:show-dialog="toggleRoomDialog">
            </room-item>
            <div class="room-btn-wrapper">
                <mu-raised-button :label="showMoreRoom ? langHotel.viewMore : langHotel.viewLess" primary
                        v-if="rooms.length > roomLimit" @click="showMoreRoom = !showMoreRoom">
                </mu-raised-button>
            </div>
        </div>
        <div v-else>
            @{{ langHotel.noResults }}
        </div>
    </div>
    {{--/hotel room loop--}}

    {{--regist box--}}
    @if(!$is_b)
        <div id="affiliateBox" style="display:inline-block !important"  v-if="!isAA">
            <a href="/affiliate/register"><img :src="'/img/general/affiliate/affiliate-m-sm-'+langStr+'.jpg'"></a>
        </div>
    @endif

    {{--nearby info--}}
    <hotel-info :description="langStr === 'cn' && hotel.description_zh ? hotel.description_zh : hotel.description"
            :facilities="facilities" :facilities-new="hotel.hotel_facilities" :policies="hotelPolicy" :area-info="hotel.area"
            :hotel-reviews="hotel.reviews" :hotel-restaurants="hotel.restaurants" :facility-open="hotelFacilityOpen"
                v-on:toggle-facility="toggleHotelFacility" :review-open="hotelReviewOpen"
                v-on:toggle-review="toggleHotelReview">
    </hotel-info>

    {{--Recommended Hotel--}}
    <div class="detail-main-title">@{{ langHotel.nearBy }}</div>
    <div class="rec-hotels" v-if="recHotels.length > 0">
        <hotel-item v-for="hotel in recHotels" :key="hotel.hotelId" :hotel="hotel" :session-key="recSessionKey"
                :search-data="searchData">
        </hotel-item>
    </div>
    {{--loading rec hotels--}}
    <div v-show="!showRecHotels">
        <div class="sk-spinner sk-spinner-three-bounce" style="height: 100px; padding-top: 30px;">
            <div class="sk-bounce1"></div>
            <div class="sk-bounce2"></div>
            <div class="sk-bounce3"></div>
        </div>
    </div>

    <room-dialog :room="dialogRoom" :dialog-open="dialogOpen" :search-data="searchData" :img-path="extraImgPath"
            v-on:toggle-dialog="toggleRoomDialog" v-on:open-search="toggleSearchEditor">
    </room-dialog>

    <search-editor :editor-open="editorOpen" :search-data="searchDataTemp" v-on:toggle-dialog="toggleSearchEditor"
            v-on:change-date="changeDate" v-on:change-selections="changeSelections" v-on:submit-search="submitSearch">
    </search-editor>
@endsection

{{--  sigle page script   --}}
@section('scripts')
    <script>
        var page_lang = "{{$language}}"; // number
        var langHotel = {!! json_encode(trans('hotel')) !!};

        // 这里可以定义本页需要的php变量转js变量
        var hotel = {!! $hotel !!};
        //parse html entities this way
        var txt = document.createElement("textarea");
        txt.innerHTML = JSON.stringify(hotel);
        hotel = JSON.parse(txt.value);
        var imgUrlArray = {!! json_encode($imgUrlArray) !!};
        var sessionKey = '{{ $sessionKey }}';
        var facilities = {!! json_encode($facilities) !!};
        var searchInfo = {!! json_encode($searchInfo) !!};
        var childAges = {!! json_encode($childAges) !!};
        var hasCachedHotel = {{ $hasCachedHotel }};
        var countdown_hotel = {!! json_encode($countdown_hotel) !!};
        var roomCount = '{{ $roomCount }}';
        var dayCount = '{{ $dayCount }}';
        var Reorderid = '{{ $Reorderid or '' }}';
        var userEmail = '{{ Auth::check() ? \Auth::user()->email : '' }}';
        var isSeeProvider = '{{ isset($seePr) ? $seePr :false }}';
        // to control initmap
        window.mapState = {
            initMap: false
        };
        function initMap() {
            window.mapState.initMap = true;
        }

        var mapKey = '{{ config('app.google_maps_api_key') }}';
        var extraImgPath = '{{ config('constants.image_host') . 'Extra_Image/' }}';

    </script>

    @php
        $lang_code = 'en';
        if($language === 0) {
           $lang_code = 'zh-CN';
        } elseif($language === 2) {
            $lang_code = 'zh-TW';
        }
    @endphp
    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_maps_api_key') }}&callback=initMap&language={{$lang_code}}" async defer></script>
    <script src="{{ asset('assets/js/mobile/hotel.js') }}"></script>
@endsection
