@extends('layouts.mobile')
<?php
// 中英文title for TDK
$countCn = count(explode('(', $destinationName));
$domain = $is_usitour ?  'Usitour' : $is_aa ? 'America Asia' : 'Usitrip';
if ($countCn > 1) {
    //有中文名称附加在后面
    $destinationCity = trim(explode(',', explode('(', $destinationName)[1])[0], ")");
} else {
    $destinationCity = explode(',', $destinationName)[0];
}
// end - 中英文title for TDK
?>
{{--  sigle page title   --}}
@if	($language === 0)
    @section('title', '' . $destinationCity . '酒店预订,' . $destinationCity . '特价酒店,折扣酒店在线预订- '.$domain.'走四方旅游网')
@elseif	($language === 2)
    @section('title', '' . $destinationCity . '飯店預定,' . $destinationCity . '特價飯店,折扣飯店在線預定- '.$domain.'走四方旅遊網')
@else
    @section('title', 'The 20 Best Cheap Hotels in ' . $destinationCity . ' - ' . $domain)
@endif
{{--  sigle page description and keywords   --}}
@section('metas')
    @if($language == 0)
        {{--中文--}}
        <meta name="Keywords" content="{{$destinationCity}}酒店预订,{{$destinationCity}}特价酒店预订,{{$destinationCity}}折扣酒店预订">
        <meta name="description" content="走四方国际酒店预订平台,为您提供{{$destinationCity}}酒店在线预订且可享受超值优惠。查询符合您预算并提供最低价格保证的{{$destinationCity}}酒店。">
    @else
        {{--英文--}}
        <meta name="Keywords" content="{{$destinationCity}}Hotel,{{$destinationCity}} Cheap Hotel,{{$destinationCity}} Hotel Reservation.">
        <meta name="description" content="{{$domain}}s' Hotel provides the best hotels deals in {{$destinationCity}}. Book now for instant savings in {{$destinationCity}} with our 100% price guarantee policy.">
    @endif
@endsection

@section('gtag_events')
    @if(config('app.enable_tracking') && !$is_b && !$is_aa)
        @php
            $subscribe_con_label = $is_usitour ? 'AW-825564842/iqv3CIKmgoYBEKq91IkD' : 'AW-825564842/BTHxCKb47noQqr3UiQM';
        @endphp
        <!-- Event snippet for 领券 conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
        <script>
            function gtag_report_conversion_subscribe(url) {
                var callback = function () {
                    if (typeof(url) != 'undefined') {
                        window.location = url;
                    }
                };
                gtag('event', 'conversion', {
                    'send_to': '{{ $subscribe_con_label }}',
                    'value': 1.0,
                    'currency': 'USD',
                    'event_callback': callback
                });
                return false;
            }
        </script>

        {{--adwords remarketing tag--}}
        <script>
            var today = new Date();
            todayStr = today.getFullYear() + '-' + today.getMonth() + 1 + '-' + today.getDate();
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            tomorrowStr = tomorrow.getFullYear() + '-' + tomorrow.getMonth() + 1 + '-' + tomorrow.getDate();
            var checkin = '{{$checkin}}' ? '{{$checkin}}' : todayStr;
            var checkout = '{{$checkout}}' ? '{{$checkout}}' : tomorrowStr;

            gtag('event', 'page_view', {
                'send_to': 'AW-825564842',
                'hrental_enddate': checkout,
                'hrental_id': '{{$destinationId}}',
                'hrental_startdate': checkin,
                'hrental_totalvalue': 0
            });
        </script>
    @endif
@endsection

{{--  sigle page style   --}}
@section('css')
    <link rel="canonical" href="{{ config('app.url') }}/list/{{ $destinationId }}.html">
    <link rel="stylesheet" href="{{ asset('assets/css/mobile/list.css') }}">
@endsection

{{--  sigle page main content   --}}
@section('content')
    <div class="subscribe" v-if="!is117book">
        <mu-flat-button class="subscribe-title" @click="showSubDetail = !showSubDetail" v-if="!isAA">
            <span :class="{'usitour': isUsitour}">@lang('list.subLabel1') <span class="color-red">@lang('list.subLabel2')</span> !</span>
            <i class="fa fa-chevron-down"></i>
        </mu-flat-button>
        <transition name="fade">
            <div v-if="showSubDetail">
                <form class="subscribe-inputs" accept-charset="UTF-8" id="leftCouponBox" method="POST" v-if="!subscribeMsg">
                    {!! csrf_field() !!}
                    <mu-text-field :hint-text="'@lang('home.subEmailHint')'" id="subscribeEmail" class="email-wrapper" name="subscribeEmail" :hint-text-class="['email-hint', {'usitour': isUsitour}]" v-model="subscriberEmail"></mu-text-field>
                    <div class="subscribe-button">
                        <button id="subscribeBtn" @click="subscribe" type="button" :class="{'usitour-bg-blue':isUsitour}">
                            @lang('list.subscribe')
                        </button>
                    </div>
                </form>
                <p :class="['subscribe-notes', {'usitour': isUsitour}]">
                    <span :class="{'usitour': isUsitour}" v-if="subscribeMsg">@{{ subscribeMsg }}</span>
                    <span :class="{'usitour': isUsitour}" v-else-if="isUsitour">@lang('list.subNotes')</span>
                    <span :class="{'usitour': isUsitour}" v-else>@lang('list.subNotes')</span>
                </p>
            </div>
        </transition>
    </div>
    <div class="list">
        <div class="temp-params" v-show="!showSearch" @click="showSearch = !showSearch">
            <div class="mu-text-field temp-params-1">
                <i class="mu-icon fa fa-map-marker-alt"></i>
                <div class="mu-text-field-content">
                    <input type="text" name="destination" class="mu-text-field-input input-des-popup" v-model="destination.scenicName ? destination.scenicName : destination.name">
                </div>
            </div>
            <mu-row gutter class="temp-params-2">
                <mu-col width="40" tablet="40" desktop="40">
                    <div :class="['temp-params-input', isUsitour ? 'usitour' : '']">
                        <i class="fa fa-calendar-alt"></i>
                        <span>@{{ searchData.checkin.substring(5) }}</span>
                        <span>@{{ langList.to }}</span>
                        <span>@{{ searchData.checkout.substring(5) }}</span>
                    </div>
                </mu-col>
                <mu-col width="60" tablet="60" desktop="60">
                    <div :class="['temp-params-input', isUsitour ? 'usitour' : '']" style="padding: 10px 3px;">
                        <i class="fa fa-bed"></i>
                        <span>@{{ searchData.roomCount }} @{{ langList.room }}, </span>
                        <span>@{{ searchData.adultCount1 }} @{{ langList.adult }}, </span>
                        <span>@{{ searchData.childCount1 }} @{{ langList.child }}</span>
                    </div>
                </mu-col>
            </mu-row>
            <mu-raised-button :label="langList.updateSearch" class="submit-btn" primary v-show="!showSearch"></mu-raised-button>
        </div>

        <form id="search-form" action="#" :class="{'usitour-searchWrapper': isUsitour}" v-show="showSearch">
            <input type="hidden" name="desId" v-model="destination.id">
            <input type="hidden" name="scenicId" v-model="destination.scenicId">
            <input type="hidden" name="desName" v-model="destination.name">
            <input type="hidden" name="scenicName" v-model="destination.scenicName">
            <input type="hidden" name="checkin" v-model="searchData.checkin">
            <input type="hidden" name="checkout" v-model="searchData.checkout">
            <input type="hidden" name="dayCount" v-model="searchData.dayCount">

            {{--search input--}}
            <div class="list-autocomplete">
                {{--destination--}}
                <home-input v-on:openpopup="openPopup" :title="langList.hintTextGo" :hinttext="langList.hintTextGo" :inputicon="inputIconMap"></home-input>
                <search-popup :destination-name="destinationTemp.name" :destination-id="destinationTemp.id" :scenic-name="destinationTemp.scenicName" :scenic-id="destinationTemp.scenicId" v-on:set-close="setPopupClose" v-on:set-des="setDestination" :is-pop-open="searchPopup"></search-popup>
            </div>

            {{-- check-in & check-out --}}
            <date-picker v-on:changedates="changeDate" :checkin="searchDataTemp.checkin" :checkout="searchDataTemp.checkout"></date-picker>

            {{--房间数，成人，儿童--}}
            <home-selection v-on:changeselections="changeSelections" :roomnum="searchDataTemp.roomCount"
                    :adultnum="searchDataTemp.adultCount1" :childnum="searchDataTemp.childCount1" :child-ages="childAges"></home-selection>

            <mu-raised-button :label="langList.search" class="submit-btn" primary @click="submitSearch"></mu-raised-button>
        </form>

        <hr class="liner">

        <h1 class="filter-buttons-label" v-if="isUsitour">
            {{$destinationCity}} @{{ labelTotalHotels }} properties
        </h1>
        {{--order and filter--}}
        <mu-row gutter id="filterBlock" :class="['filter-buttons', isUsitour ? 'usitour' : 'usitrip']">
            <mu-col id="searchParams" width="100" tablet="100" desktop="100" v-if="stickyTop">
                <div :class="['search-params', isUsitour ? 'usitour' : 'usitrip']" @click="showSearchBlock">
                    <div v-if="isUsitour">
                        <i class="fa fa-search"></i>
                    </div>
                    <div class="search-params-text">
                        <div class="search-params-item">
                            <i class="fa fa-map-marker-alt" v-if="!isUsitour"></i>
                            <span :class="['search-params-desName', isUsitour ? '' : 'usitrip']">@{{ destination.scenicName ? destination.scenicName : destination.name }}</span>
                        </div>
                        <div class="search-params-item">
                            <div :class="['search-params-dates', isUsitour ? '' : 'usitrip']">
                                <span>@{{ searchData.checkin.substring(5) }}</span>
                                <span>@{{ langList.to }}</span>
                                <span>@{{ searchData.checkout.substring(5) }}</span>
                            </div>
                            <div :class="['search-params-pax', isUsitour ? '' : 'usitrip']">
                                <span>@{{ searchData.roomCount }} @{{ langList.room }}, </span>
                                <span>@{{ searchData.adultCount1 }} @{{ langList.adult }}, </span>
                                <span>@{{ searchData.childCount1 }} @{{ langList.child }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </mu-col>
            {{--filter--}}
            <mu-col id="filterBtn" class="button-filter button" :width="hotelMap ? '33' : '50'" :tablet="hotelMap ? '33' : '50'" :desktop="hotelMap ? '33' : '50'">
                <mu-raised-button class="btn-block" primary :disabled="disableFilter" @click="setFilterPopupStatus(true)">
                    <i class="fa fa-filter" v-if="isUsitour"></i>
                    <span>@{{ langList.labelFilter }} <span v-if="!isUsitour">@{{ labelTotalHotels }}</span></span>
                </mu-raised-button>
            </mu-col>
            {{--sort--}}
            <mu-col id="sortBtn" class="button-sort button" :width="hotelMap ? '33' : '50'" :tablet="hotelMap ? '33' : '50'" :desktop="hotelMap ? '33' : '50'">
                <mu-raised-button class="order-raised-button btn-block order-label" @click="setOrderPopupStatus(true)" primary>
                    <span>
                        <i :class="sortIcon[filterData.sortBy]" v-if="filterData.orderBy !== 'discount' && !isUsitour"></i>
                        <i class="fa fa-sort" v-if="isUsitour"></i>
                        @{{ langList.orderLabels[filterData.orderBy] }}
                    </span>
                </mu-raised-button>
            </mu-col>
            {{--map--}}
            <mu-col class="button-map button" width="33" tablet="33" desktop="33" v-if="hotelMap">
                <mu-raised-button class="btn-block" @click="toggleMap" primary>
                    <i class="fa fa-map" v-if="isUsitour"></i>
                    <span>@{{ langList.labelMap }}</span>
                </mu-raised-button>
            </mu-col>
        </mu-row>
        {{--/ order and filter--}}

        <hr class="liner liner-margin" v-if="!isUsitour">

        <div :class="['hotels-block', isUsitour ? 'usitour' : 'usitrip']">
            {{--single hotel searched--}}
            <div v-if="isHotelSearch && showSearchedHotel">
                {{--  hotel item  --}}
                <hotel-item class="card" :hotel="searchedHotel" :session-key="searchedHotelSessionKey" :destination-city="destination.city"
                        :search-data="searchData">
                </hotel-item>

                {{--  message content  --}}
                <div class='hotels-block-message' role='alert'>
                    <span v-if="isHotelSearch">
                    <div v-if="searchedHotel.minPrice > 0"
                         class="container-more-option">@{{langList.moreOption}}
                    </div>
                    <span v-if="(!searchedHotel.minPrice || searchedHotel.minPrice === 0) && recommendHotels.length > 0">@{{langList.soldOutContent}}</span>
                    </span>
                </div>
            </div>

            <div class="sk-spinner sk-spinner-three-bounce" v-if="isLoading">
                <div class="sk-bounce1"></div>
                <div class="sk-bounce2"></div>
                <div class="sk-bounce3"></div>
            </div>

            <div class="temp-hotels-block" v-if="showTempHotels">
                <transition-group name="list-item" class="list__ul">
                    <hotel-item class="card list__item" v-for="hotel in loadingTempHotels" :key="hotel.hotelId"
                            :hotel="hotel" session-key="tempHotel" :destination-city="destination.city" :search-data="searchData"
                            :is-temp="true">
                    </hotel-item>
                </transition-group>
            </div>

            {{--regist box--}}
            @if(!$is_b)
                <div id="affiliateBox"  v-if="!isAA">
                    <a href="/affiliate/register">
                        <img :src="'/img/general/affiliate/affiliate-m-sm-'+langStr+'.jpg'">
                    </a>
                </div>
            @endif

            {{--recommend hotels--}}
            <div v-if="showRecHotelsBlock">
                <div v-if="recommendHotels.length > 0">
                    <hotel-item class="card" v-for="(hotel, index) in recommendHotels" :key="hotel.hotelId"
                            :hotel="hotel" :session-key="recHotelSessionKey" :destination-city="destination.city" :search-data="searchData"
                            :scenic="selectedScenic" :order-by="filterData.orderBy">
                    </hotel-item>
                </div>
                <div v-else class="text-no-data">
                    @{{ langList.noRecHotels }}
                </div>
            </div>
        </div>

        {{--pagination--}}
        <div v-if="showPages">
            <mu-pagination :current="currentPage"
                           :total="totalHotels"
                           :default-page-size="numLimit"
                           @page-change="goPage">
            </mu-pagination>
        </div>

        {{--filter popup--}}
        <filter-popup v-on:setfilterpopup="setFilterPopupStatus"
                      :is-pop-open="filterPopup"
                      :keyword="filterData.keyword"
                      :rating="filterData.rating"
                      :price="filterData.price"
                      :scenic-ids="filterData.scenicIds"
                      :scenic-names="filterData.scenicNames"
                      :chain-names="filterData.chainNames"
                      :districts="districts"
                      :airports="airports"
                      :zones="zones"
                      :locations="locations"
                      :universities="universities"
                      :hotel-chains="hotelChains"
                      @apply-filters="applyFilters">
        </filter-popup>

        {{--order popup--}}
        <order-popup v-on:setpopup="setOrderPopupStatus"
                     :is-pop-open="orderPopup"
                     :sort="filterData.sortBy"
                     :order-by="filterData.orderBy"
                     :order-labels="langList.orderLabels"
                     @set-order="setOrder">
        </order-popup>

    </div>

    <div id="mapWrapper" class="map-wrapper">
        <div id="mapBanner" class="map-banner" @click="toggleMap">
            <i class="fa fa-angle-left"></i>
            <div>
                <div class="map-banner-desName"> @{{ destination.name }} </div>
                <div> @{{ formattedCheckin }} - @{{ formattedCheckout }}</div>
            </div>
        </div>
        <div id="infowindow" class="infowindow" v-if="showInfowindow && infoHotel">
            <info-hotel :hotel="infoHotel" :session-key="searchedHotelSessionKey" :search-data="searchData"
                    :destination-city="destination.city">
            </info-hotel>
        </div>
        <div id="map" class="map"></div>
    </div>
@endsection

{{--  sigle page script   --}}
@section('scripts')
    @include('includes.popup-cities')

    <script type="text/javascript">
        var langList = {!! json_encode(trans('list')) !!};

                {{--这里可以定义本页需要的php变量转js变量(HotelController@mobileList)--}}
        var langFilter = {
                    @foreach (trans('filter') as $index => $value)
                    '{{ $index }}': '{{ $value }}',
                    @endforeach
          };

        var destinationName = "{{ $destinationName }}";
        var destinationCity = "{{ $destinationCity }}";
        var destinationId = '{{ $destinationId }}';
        var checkIn = '{{ $checkin }}';
        var checkOut = '{{ $checkout }}';
        var labelFilterText = langFilter.filter;
        var roomCount = '{{ $roomCount }}';
        var dayCount = '{{ $dayCount }}';
        var adultCount = '{{ $adultCount1 }}';
        var childCount = '{{ $childCount1 }}';
        var childAges = {!! json_encode($childAges) !!};
        if (childCount > 0 && !childAges) {
            childAges = [];
            for(var i = 0; i < childCount; i++) {
                childAges.push(12);
            }
        }
        var scenicName = '{{ $scenicName }}';
        var scenicId = '{{ $scenicId }}';

        var showAllDiscount = '{{ $showAllDiscount }}';
        var orderBy = '{{ $orderBy }}';
        var sortBy = '{{ $sort }}';
        var key = '{{ $key }}';
        var currentPage = parseInt('{{ $pagePos }}');

        var rating = '{{ $rating }}';
        rating = rating.length > 0 ? rating.split(',') : [];
        var price = "{{ $price }}";
        price = price.length > 0 ? price.split(',') : [];
        // selected scenic for share
        var scenicAreaArr = '{{ $scenicArea }}';
        scenicAreaArr = scenicAreaArr.length > 0 ? scenicAreaArr.split(',') : [];
        if (scenicId) {
            scenicAreaArr.push(scenicId);
        }
        var chainNames = '{{ $chainNames }}';
        chainNames = chainNames.length > 0 ? chainNames.split(',') : [];

        window.mapState = {
            initMap: false
        };
        function initMap() {
            window.mapState.initMap = true;
        }
    </script>
    {{-- 源文件： resources/assets/js/mobile/pagename.js --}}
    <script src="{{ asset('assets/js/mobile/list.js') }}"></script>
    @if($language == 0)
        <script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_maps_api_key') }}&callback=initMap&language=zh-CN" async defer></script>
    @elseif($language == 2)
        <script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_maps_api_key') }}&callback=initMap&language=zh-TW" async defer></script>
    @else
        <script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_maps_api_key') }}&callback=initMap&language=en" async defer></script>
    @endif
@endsection
