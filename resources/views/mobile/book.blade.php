@extends('layouts.mobile')

{{--  sigle page title   --}}
@section('title')
    @if	($language === 0)
        {{ $hotel->name . '预订_' .  $hotel->name . '_价格_房型_设施_环境_地址查询-走四方旅游网' }}
    @elseif($language == 2)
        {{ $hotel->name . '預定_' . $hotel->name. '_價格_房型_設施_環境_地址查詢-走四方旅遊網' }}
    @else
        {{ $hotel->name . ' Booking_' .  $hotel->name . ' Price_Room Type_Facilities_Environment_location Searching' }}
    @endif
@endsection

{{--  sigle page description and keywords   --}}
@section('metas')
    @if	($language === 0)
        <meta name="Keywords" content={{ $hotel->name}}预订，{{ $hotel->name}}价格，{{ $hotel->name}}酒店地址"/>
        <meta name="description" content="{{ $hotel->name}}预订_{{ $hotel->name}}价格_房型_设施_环境_地址查询">
    @else
        <meta name="Keywords" content="{{ $hotel->name}} Booking, {{ $hotel->name}} Price, {{ $hotel->name}} Address"/>
        <meta name="description" content="{{ $hotel->name}} Booking_{{ $hotel->name}} Price_Room Type_Facilities_Environment_location Search ">
    @endif
@endsection

@section('css')
    <link rel="canonical" href="{{ config('app.url') }}/book/{{ $hotel->hotelId }}">
    {{--源文件： resources/assets/sass/mobile/book.scss --}}
    <link rel="stylesheet" href="{{ asset('assets/css/mobile/book.css') }}">
@endsection

{{--  sigle page main content   --}}
@section('content')
    {{--1. lock price card begin--}}
    <book-lock-price></book-lock-price>

    {{--2. hotel book detail card begin--}}
    <mu-card class="card">
        <div style="background-color:transparent;">
            <div>
                <p id="browserNotice"></p>
                {{--2.1 hotel img and hotel name with star rating card begin--}}
                <div v-if="hotel.thumbnail" :style="{ 'background-image': 'url(https://s3-us-west-1.amazonaws.com/usitrip/Hotel_Image/' + hotel.thumbnail + ')' }"
                     class="book-hotel-img"></div>
                <h1 class="hotelName padding-sides">
                    <span id="hotel-name">
                       @{{hotel.name}}<span v-if="langStr ==='cn' && hotel.name_zh"> @{{hotel.name_zh}}</span>
                        <span class="hotel-rating">
                            <i v-for="n in hotel.rating" class="fa fa-star hotel-star"></i>
                        </span>
                    </span>
                </h1>

                {{--2.2. address card begin--}}
                <div class="location-on padding-sides">
                    <i class="fa fa-map-marker-alt"></i>
                    <span id="hotel-address">
                        {{ucwords(strtolower(str_replace(',',' ',$hotel->address))). ', '. ucwords(strtolower($hotel->city)). ', ' . $hotel->zipcode}}
                </span>
                </div>

                {{--2.3. 入住退房card begin--}}
                <div class="flex-container">
                    <div class="text-blue">
                        <p class="font14 padding-top-bottom">@{{langBook.checkin}}<br/>
                            @if($is_usitour) {{ substr($checkin, -5) }}-{{ substr($checkin, 0, 4) }}     @else {{substr($checkin, -10)}} @endif
                            &nbsp; @{{ dayNameListObj[checkinDayName][langStr] }}
                        </p>
                    </div>
                    <div class="border-left text-blue">
                        <p class="font14 padding-top-bottom">
                        @{{langBook.checkout}}<br/>
                        @if($is_usitour) {{ substr($checkout, -5) }}-{{ substr($checkout, 0, 4) }}     @else {{substr($checkout, -10)}} @endif
                        @{{dayNameListObj[checkoutDayName][langStr] }}</p>
                    </div>
                </div>
            </div>
            {{--2.4. 房间信息 card begin--}}
            {{--2.4.1: room count , day count and cancellation policy--}}
            <div class="room-info">
                <p class="font14"><span
                            class="text-bold">@{{roomNum}} @{{langBook.guestroom}}:{{ucwords(strtolower($room['room']['name']))}}</span>
                </p>
                <p class="font14">
                    <span class="text-bold">@{{langBook.duration}}:</span>
                    @{{dayCount}} @{{langBook.night}}
                </p>
                <p class="font14"><span class="text-bold">@{{langBook.cancellationPolicy}}:</span> <span id="hotel-cancel"></span></p>
                {{--2.4.2: payment detail: paxes and payment rate before tax single night total taxes. and resort fee if any--}}
                <h4 class="font14 text-bold text-blue"> @{{langBook.priceDetail}}：</h4>
                <p class="text-bold" v-for="n in Number(roomCount)">
                    @{{langBook.the}}@{{roomCountListObj[n][langStr]}}@{{langBook.guestroom}}：{{$room['occupancy']['adults']}}
                    @{{langBook.adult}}，{{$room['occupancy']['children']}}@{{langBook.child}}
                </p>
                <p class="font14">
                    @{{langBook.ratePerRN}}
                    <span class="roomPrice pull-right">
                     @{{ currency }} @{{singleRoomNightPriceB4Tax}}
                    </span>
                </p>
                <p class="font14">
                    @{{langBook.totalTax}}
                    <span class="pull-right"> @{{ currency }} @{{ room.tax }}</span>
                </p>
                @if($room['room']['cleanFee'] !== 0)
                    <p class="font14 hidden" id="cleanFee">
                        @{{langBook.clean_fee}}
                        <span class="pull-right">USD <span class="clean-fee"></span></span>
                    </p>
                @endif
            </div>

            {{--已使用coupon--}}
            <div class="applied-coupon-block">
                <p class="color-green">
                    <span>@lang('book.coupon_applied_title'): @lang('book.coupon_applied_desc')</span>
                    <span id="couponAmount" class="pull-right"></span>
                </p>
                <p class="remove-btn-wrapper">
                    <button class="remove-btn" @click="removeCoupon()">@lang('book.coupon_remove')</button>
                </p>
            </div>

            <div class="due-today text-blue">
                @{{langBook.dueToday}}<span class="pull-right"> {{$room['room']['currency']}}
                        <span id="netPrice"> {{$room['room']['netPrice']}}</span>
                </span>
            </div>
            @if($room['room']['mandatoryFees'] != null)
                <p class="hotel-due">@{{langBook.dueHotel}} (@{{langBook.resortFee}})<span
                            class="pull-right">{{$room['room']['currency'] .' '.$room['room']['mandatoryFees']}}</span>
                </p>
            @elseif($room['room']['resortFee'] != null && $room['room']['resortFee']['amount'] != 0)
                <p class="hotel-due">@{{langBook.dueHotel}} @{{langBook.resort}}<span
                            class="pull-right">{{$room['room']['resortFee']['currency'] .' '.$room['room']['resortFee']['amount']}}</span>
                </p>
            @endif
        </div>
        {{--预订信息--}}
    </mu-card>

    @if(!$is_b)
        <div class="coupon-block mu-card card">
            <div class="coupon-title">@lang('book.coupon_title')</div>
            <mu-row gutter>
                <mu-col width="70" tablet="70" desktop="70">
                    <input type="text" :class="['coupon-input', invalidCoupon ? 'error' : '']" id="codeInput"
                           placeholder="@lang('book.coupon_placeholder')" v-model="couponCode">
                    <div v-if="invalidCoupon" class="error-message">
                        <span v-if="couponErrorMsg">@{{ couponErrorMsg }}</span>
                        <span v-else>@lang('book.coupon_error_msg')</span>
                    </div>
                </mu-col>
                <mu-col width="30" tablet="30" desktop="30">
                    <button class="coupon-apply" @click="applyCoupon()">@lang('book.coupon_btn')</button>
                </mu-col>
            </mu-row>
        </div>
    @endif

    {{--3. booking detail with promotion slogan--}}
    <div class="book-now-title">@{{langBook.bookInfo}}
        <p class="text-blue font14">@{{langBook.goodChoice}}</p>
    </div>

    {{--4.input guest info card begin--}}
    <form class="form-horizontal" role="form" method="post" id="book-form">
        {!! csrf_field() !!}
        <input type="hidden" name="source">
        <input type="hidden" name="bookIndex" value="{{$bookIndex}}">
        <input type="hidden" name="sessionKey" value="{{$sessionKey}}">
        <input type="hidden" name="displayId" value="{{$displayId or ''}}">
        <input type="hidden" name="booker_local_time" id="booker_local_time">
        <input type="hidden" name="total_mandatory_fees" id="total_mandatory_fees">
        <input type="hidden" name="coupon_code" id="couponCode" value="">

        <p id="browserNotice"></p>
        <!--住客信息 start-->
        <div v-for="n in Number(roomCount)">
            <mu-card class="card">
                <input type="hidden" :name="'roomRef' + n" v-model="roomRef">
                <input type="hidden" :name="'roomn'+ n" value="{{$rooms[0]['room']['name']}}">

                <div class="card-title">
                    <p>
                        <span class="text-bold">@{{langBook.room}} @{{n}}:</span>
                        {{$rooms[0]['occupancy']['adults']}}
                        @{{langBook.adult}}, {{$rooms[0]['occupancy']['children']}}
                        @{{langBook.child}}<br/>
                        <span>{{strtolower($rooms[0]['room']['name']. ', ' . $rooms[0]['room']['boardName'])}}</span>
                    </p>
                </div>

                <div class="padding-sides">
                    <label class="text-bold">
                        @{{langBook.mainGuest}}
                        <span class="text-red">*</span>
                    </label>
                    <p>
                        <input class="book-text-field" :placeholder="firstName[langStr]" :id="'guest' + n + '_a1_fn'"
                               :name="'guest' + n + '_a1_fn'" value="{{$booker['customers_firstname'] or ''}}"/>
                    </p>
                    <p>
                        <input class="book-text-field" :placeholder="lastName[langStr]" :id="'guest' + n + '_a1_ln'"
                               :name="'guest' + n + '_a1_ln'" value="{{$booker['customers_lastname'] or ''}}"/>
                    </p>
                </div>
            </mu-card>
        </div>

        {{--5. warning for user input same guest name for all rooms--}}
        <div class="warning-guest-name-for-all">
            <i class="fa fa-info-circle" aria-hidden="true"></i>@{{langBook.tip}}
        </div>

        {{--6. special requests--}}
        <mu-card class="card" v-if="is117book">
            <div>
                <div class="card-title font16 text-bold text-black" @click="showSpecialRequests = !showSpecialRequests">
                    <p>@{{langBook.specialRequest}}?<i class="fa fa-angle-down pull-right"></i></p>
                </div>

                <div class="padding-sides text-black" v-show="showSpecialRequests">
                    <p>‌@{{langBook.specialRequestNote}}</p>
                    <input type="checkbox" name="specialrequest[]" value="I'd like two beds" tabindex="-1">
                    @{{langBook.srOne}}
                    <br/>
                    <input type="checkbox" name="specialrequest[]" value="I'd like one (large) bed"
                           tabindex="-1"> @{{langBook.srTwo}}
                    <br/>
                    <input type="checkbox" name="specialrequest[]" value="I'd like a quiet room" tabindex="-1">
                    @{{langBook.srThree}}
                    <br/>
                    <input type="checkbox" name="specialrequest[]" value="I'd like a room on ground floor"
                           tabindex="-1"> @{{langBook.srFour}}
                    <br/>
                    <input type="checkbox" name="specialrequest[]" value="I'd like a room on high floor"
                           tabindex="-1"> @{{langBook.srFive}}
                    <br/>
                    <input type="checkbox" name="specialrequest[]" value="I'd like a room with view"
                           tabindex="-1"> @{{langBook.srSix}}
                    <br/>
                    <br/>
                </div>
            </div>
        </mu-card>

        {{--7. booker informations card begin 联系人信息--}}
        <mu-card class="card text-black">
            <div class="card-title font16 text-bold">
                <p>@{{langBook.contactInfo}}</p>
            </div>
            <div class="padding-sides">
                <p class="text-bold">@{{langBook.contactName}}<span class="text-red"> *</span></p>
                <p><input class="book-text-field" :placeholder="firstName[langStr]" id="booker_fn" name="booker_fn"
                          value="{{$booker['customers_firstname'] or ''}}"/></p>
                <p><input class="book-text-field" name="booker_ln" id="booker_ln" :placeholder="lastName[langStr]"
                          value="{{$booker['customers_lastname'] or ''}}"></p>
                <p class="text-bold">@{{langBook.contactPhone}}<span class="text-red"> *</span></p>
                <mu-row gutter>
                    <mu-col width="35" tablet="35" desktop="35">
                        <select name="phone_country" id="phone_country" class="book-text-field" required>
                            <option value="+1" selected>
                                @{{ langBook.usa }}+1
                            </option>
                            <option value="+1">
                                @{{ langBook.canada }}+1
                            </option>
                            <option value="+86">
                                @{{ langBook.cn }}+86
                            </option>
                            <option value="+852">
                                @{{ langBook.hongkong }}+852
                            </option>
                            <option value="+886">
                                @{{ langBook.taiwan }}+886
                            </option>
                            <option value="+853">
                                @{{ langBook.macau }}+853
                            </option>
                            <option value="+44">
                                @{{ langBook.uk }}+44
                            </option>
                            <option value="+49">
                                @{{ langBook.germany }}+49
                            </option>
                            <option value="+33">
                                @{{ langBook.france }}+33
                            </option>
                            <option value=" ">
                                @{{ langBook.other }}
                            </option>
                        </select>
                        <span class="other"></span>
                    </mu-col>
                    <mu-col width="65" tablet="65" desktop="65">
                        <input class="book-text-field" name="booker_phone" id="booker_phone"
                               value="{{$booker['customers_mobile_phone'] or ''}}">
                    </mu-col>
                </mu-row>
                <p class="text-bold">@{{langBook.contactEmail}}<span class="text-red"> *</span></p>
                <input class="book-text-field" id="booker_email" name="booker_email" @blur="saveEmailInfo"
                       value="{{$booker['customers_email_address'] or ''}}">
            </div>
            <br/>
        </mu-card>

        {{--8. 付款方式 card begin--}}
        {{--117book CN user has no CC, usitrip is US, usitour only show cc--}}
        <div class="padding-transparent-sides text-bold text-black font16" id="payMethodBlock">
            {{--config('app.2b_test_partner_id') 是给b端用户看的不下单的账号--}}
            @if(!$is_b || $is_b && Auth::user()->paymentType == 'Prepay')
                <p>@{{langBook.pmtMethod}}</p>
                @if(Auth::check() && Auth::user()->id == config('app.2b_test_partner_id'))
                    <p id="bt" style="color:red; text-align:center">请注册成为真实用户后下单,支持支付宝，微信，paypal和信用卡，postpay支付</p>
                @else
                    <p class="alipay-warning" style="display:none">@{{langBook.alipayComeBack}}</p>
                    <div class="padding-transparent-sides">
                        @if(config('app.enable_h5_we') && !$is_usitour)
                            <label for="wechatpay" class="paymentLabel" id="wechatpayWrapper">
                                <input class="pmt-radio-size" id="wechatpay" type="radio" name="imgsel" value="we" checked="checked" required/>&nbsp;&nbsp;
                                <img class="pmt-img-size" src="{{url('/img/payment/wechat.png')}}">&nbsp;&nbsp;
                                <span class="font16">@{{langBook.wechat}}</span>
                            </label>
                        @endif
                        {{--Warning！！！！不能把这个信用卡的权限开通给中文用户（大团的是英文客户），因为商务部对中国用户直接开通，不去查看证件之类的，防止fraud，必须关闭这个信用卡权限   09142018  Ann--}}
                        @if($is_b && Auth::user()->Country_Code === 'US' || !$is_b )
                           {{--<label for="ppBtn" class="paymentLabel" id="ppBtnPaymentLabel">
                                <input class="pmt-radio-size" id="ppBtn" type="radio" name="imgsel" value="pb" required disabled/>&nbsp;&nbsp;
                                <img style="height:24px;" src="/img/payment/PayPal.png"/>
                                <!--span class="font16">@{{langBook.pp_btn}}</span-->
                            </label>--}}
                            @if(config('app.enable_cc')|| $is_b)
                                <label for="ccpay" id="ccPaymentLabel" class="paymentLabel">
                                    <input class="pmt-radio-size" id="ccpay" type="radio" name="imgsel" value="cc" @if($is_usitour) checked="checked" @endif required/>
                                    &nbsp;&nbsp;
                                    <img style="height:25px;" src="/img/payment/all-major-credit-cards.png"/>
                                    {{--&nbsp;&nbsp; &nbsp;&nbsp;<span class="font16">@{{langBook.cc}}</span>--}}
                                </label>
                            @endif
                        @endif
                        <label for="alipay" class="paymentLabel" id="alipayWrapper" v-if="!isUsitour">
                            <input class="pmt-radio-size" id="alipay" type="radio" name="imgsel" value="al" @if(!config('app.enable_h5_we')) checked="checked"
                                   @endif  required/>&nbsp;&nbsp;
                            <img class="pmt-img-size" src="{{url('/img/payment/alipay.png')}}">&nbsp;&nbsp;<span class="font16">@{{langBook.alipay}}</span>
                        </label>
                        <input type="hidden" name="hotelId" value="{{$hotelId}}">
                    </div>
                    <div>
                <span id="paymentAuth">
                    <input type="checkbox" id="cb" name="cb" @click="calc()" checked="checked"/>
                </span>
                        <span class="font12">@{{langBook.pmtInfoConfirm}}</span>
                    </div>
                    {{--submit button 立即付款--}}
                    <button type="button" class="book-pay-button ladda-button ladda-button-book btn-ready"
                            data-loading-text="<i class='fa fa-spinner fa-spin '></i> ......" data-style="expand-left" id="bt"
                            name="bt" @click="bookRooms">@{{langBook.payNow}}
                    </button>
                    <p class="exchange-rate-warning font12" style="display:none"><i class="fa fa-info-circle"></i> &nbsp;
                        <span v-if="isAA">美亚</span><span v-else-if="isUsitour">Usitour</span><span v-else-if="is117book">117book</span><span v-else>{{$language === 1 ? 'Usitrip' : '走四方'}}</span> @{{langBook.exchangeRateWaiver}}</p>
                @endif
            @elseif($is_b && Auth::user()->paymentType == 'Postpay')
                {{--postpay用户付款--}}
                <button type="button" class="book-pay-button ladda-button ladda-button-book btn-ready"
                        data-loading-text="<i class='fa fa-spinner fa-spin '></i> ......" data-style="expand-left" id="bt"
                        name="bt" @click="bookRooms()">@{{langBook.payNow}}
                </button>
                {{--payment aggrement--}}
                <div>
            <span id="paymentAuth">
                <input type="checkbox" id="cb" name="cb" @click="calc()" checked="checked"/>
            </span>
                    <span class="font12">@{{langBook.pmtInfoConfirm}}
            </span>
                </div>
            @endif
            {{--paypal btn direct checkout--}}
            <div id="paypal-button" style="display:none"></div>
            {{--payment aggrement--}}
        </div>
        {{--9. 预订须知--}}
        <mu-card class="card">
            <div class="card-title" @click="showComment = !showComment">
                <p class="text-bold">@{{langBook.finePrint}}<i class="fa fa-angle-down pull-right"></i></p>
            </div>
            <div class="padding-sides text-black" id="hotel-comment" v-show="showComment">
                <p>@{{roomComments}}</p>
            </div>
        </mu-card>
        <!--注意事项end-->
    </form>

    <div style="margin-top:35px; padding:0 20px 0 40px;">
        <div class="row">
            <!--左侧 start-->
            <div style="margin-right:20px;">
                <div class="ibox">
                    <div class="ibox-noborder">
                    </div>
                </div>
            </div>
            <!--左侧 end-->
            {{--手机端：安卓chrome使用citcon支付时不会打开回调url，而是返回该book页，需检查用户是否支付完成--}}
            <modal v-if="showModal" confirm-label="确认" cancel-label="取消" @cancel-action="cancelBook" @confirm-action="confirmBook">
                <div slot="header">
                    <h3 class="placeOrder-header">
                        @{{ modalContent }}
                    </h3>
                </div>
            </modal>
            <!-- toggle loading modal-->
            <processing v-if="startLoading"></processing>
        </div>
    </div>

@endsection

{{--  sigle page script   --}}
@section('scripts')
    <script>
      var langBook = {!! json_encode(trans('book')) !!};
      {{--这里可以定义本页需要的php变量转js变量--}}
      var hotel =  {!! json_encode($hotel) !!};
      var rooms = {!! json_encode($rooms) !!};
      var room = {!! json_encode(isset($room['room']) ? $room['room'] : []) !!};
      var dayCount = '{{ isset($room['room']['dayCount']) ? $room['room']['dayCount'] : 0 }}';
      var currency = '{{ isset($room['room']['currency']) ? $room['room']['currency'] : null }}';
      var comments = {!! json_encode(isset($room['room']['comments']) ? $room['room']['comments'] : []) !!};
      var roomComments = '{{ isset($roomDetails['comments'][0]) ? $roomDetails['comments'][0]['comments'] : 'None' }}';
      var status = '{{ isset($room['room']['status']) ? $room['room']['status'] : null }}';
      var roomCount = '{{ isset($room['room']['roomCount']) ? $room['room']['roomCount'] : 0 }}';
      var netPrice = '{{ isset($room['room']['netPrice']) ? $room['room']['netPrice'] + $room['room']['cleanFee'] : null }}';
      var cleanFee = '{{ isset($room['room']['cleanFee']) ? $room['room']['cleanFee'] : 0 }}';
      var singleRoomNightPrice = '{{ ceil($room['room']['netPrice'] * 100 / $room['room']['dayCount'] / $room['room']['roomCount']) / 100.0 }}';
      var singleRoomNightPriceB4Tax = '{{ isset($singleRoomNightPriceB4Tax) ? $singleRoomNightPriceB4Tax : '' }}';
      var singleRoomNightTax = '{{ isset($singleRoomNightTax) ? $singleRoomNightTax : '' }}';
      var beforeTax = '{{ $room['room']['beforeTax'] + $room['room']['cleanFee']}}';
      var tax = '{{ $room['room']['tax'] }}';
      var hotelAddress = '{{ ucwords(strtolower(str_replace(",", " ", $hotel->address))) . ", " . ucwords(strtolower($hotel->city)) . ", " . $hotel->zipcode }}';

      var guests = {!! json_encode($guests) !!};
      var booker = {!! json_encode($booker) !!};

      var sessionKey = '{{ $sessionKey }}';
      var source = '{{ isset($_COOKIE['source']) ? $_COOKIE['source'] : '' }}';

      var checkin = '{{ $checkin }}';
      var checkout = '{{ $checkout }}';
      var hotelId = '{{ $hotelId }}';
      var hotelName = '{{ $hotel['name'] }}';
      var roomRef = '{{$bookIndex }}';
      var displayId = '{{ $displayId }}';

      var hotelPath = '{{ $hotelPath }}';
      var ipLocation = '{{ $ipLocation }}';
      var paypalEnv = '{{ config('app.env') === 'production' ? 'production' : 'sandbox'}}';
      var app_pp_btn = "{{config('app.paypal_button')}}";
      var rooms_key = '{{ $rooms_key }}';
    </script>
    {{--paypalBtn的js,需要先加载--}}
    {{--<script src="https://www.paypalobjects.com/api/checkout.min.js"></script>--}}
    {{--源文件： resources/assets/js/mobile/book.js --}}
    <script src="{{ asset('assets/js/mobile/book.js') }}"></script>
@endsection
