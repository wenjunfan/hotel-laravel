@extends('layouts.mobile')

@section('title', 'Hotel Deals List')

@section('css')
    <link rel="stylesheet" href="{{ asset('/assets/css/mobile/deal.css') }}">
@endsection

@section('content')
    <div id="dealsApp">
        <div class="top-search-bar">
            <form id="search-form" action="#" class="container">
                <input type="hidden" name="checkin" :value="searchData.checkin">
                <input type="hidden" name="checkout" :value="searchData.checkout">

                <div class="display-block" v-show="!isEditing" @click="isEditing = !isEditing">
                    <div :class="['display-line-1', destination.name === 'where to go' ? 'default' : '']">@{{ destination.name }}</div>
                    <div class="display-line-2">
                        <span>@{{ searchData.checkin }} - @{{ searchData.checkout }}, </span>
                        <span>
                            @{{ searchData.roomCount }}
                            <span v-if="langStr === 'cn'">
                                房间
                            </span>
                            <span v-else>
                                <span v-if="searchData.roomCount > 1">Rooms</span><span v-else>Room</span>
                            </span>,
                            @{{ searchData.adultCount1 }}
                            <span v-if="langStr === 'cn'">
                                成人
                            </span>
                            <span v-else>
                                <span v-if="searchData.adultCount1 > 1">Adults</span><span v-else>Adult</span>
                            </span>,
                            @{{ searchData.childCount1 }}
                            <span v-if="langStr === 'cn'">
                                小孩
                            </span>
                            <span v-else>
                                <span v-if="searchData.childCount1 > 1">Children</span><span v-else>Child</span>
                            </span>
                        </span>
                    </div>
                </div>
                <mu-row gutter class="search-block" v-show="isEditing">
                    <mu-col width="100" tablet="33" class="destination-name">
                        <label for="desName">@lang('deal.search_destination')</label>
                        <input type="hidden" name="desId" v-model="destination.id">
                        <auto-complete
                                :destination-name="destination.name"
                                :destination-id="destination.id"
                                :scenic-name="destination.scenicName"
                                :scenic-id="destination.scenicId"
                                :input-placeholder="langStr === 'cn' ? '请输入目的地' : 'Where to go'"
                                :input-class="['param-data', 'destination-input']"
                                @update-destination="updateDestination">
                        </auto-complete>
                    </mu-col>
                    <mu-col width="50" tablet="33" class="checkin-date">
                        <label for="checkin">@lang('deal.search_checkin')</label>
                        <mu-date-picker name="checkin" :input-class="'param-data'" :auto-ok="true" :min-date="checkinMin" v-model="searchData.checkin"></mu-date-picker>
                    </mu-col>
                    <mu-col width="50" tablet="33" class="checkout-date">
                        <label for="checkout">@lang('deal.search_checkout')</label>
                        <mu-date-picker name="checkout" :input-class="'param-data'" :auto-ok="true" :min-date="checkoutMin" v-model="searchData.checkout"></mu-date-picker>
                    </mu-col>
                    <mu-col width="100" tablet="33" class="rooms-wrapper">
                        <label for="selects">@lang('deal.search_rooms')</label>
                        <div class="rooms-display">
                            <div @click="editRooms = !editRooms">
                                @{{ searchData.roomCount }}
                                <span v-if="langStr === 'cn'">
                                    房间
                                </span>
                                <span v-else>
                                    <span v-if="searchData.roomCount > 1">Rooms</span><span v-else>Room</span>
                                </span>,
                                @{{ searchData.adultCount1 }}
                                <span v-if="langStr === 'cn'">
                                    成人
                                </span>
                                <span v-else>
                                    <span v-if="searchData.adultCount1 > 1">Adults</span><span v-else>Adult</span>
                                </span>,
                                @{{ searchData.childCount1 }}
                                <span v-if="langStr === 'cn'">
                                    小孩
                                </span>
                                <span v-else>
                                    <span v-if="searchData.childCount1 > 1">Children</span><span v-else>Child</span>
                                </span>
                            </div>
                            <div class="rooms-dropdown" v-show="editRooms">
                                <div>
                                    <label for="roomCount">@lang('deal.search_room_count'):</label>
                                    <button type="button" class="control-minus" @click="searchData.roomCount--;return false;" :disabled="searchData.roomCount === 1">-</button>
                                    <input type="text" name="roomCount" placeholder="room number" v-model="searchData.roomCount">
                                    <button type="button" class="control-plus" @click="searchData.roomCount++;return false;" :disabled="searchData.roomCount === 8">+</button>
                                </div>
                                <div>
                                    <label for="adultCount1">@lang('deal.search_adult_count'):</label>
                                    <button type="button" class="control-minus" @click="searchData.adultCount1--;return false;" :disabled="searchData.adultCount1 === 1">-</button>
                                    <input type="text" name="adultCount1" placeholder="adult number" v-model="searchData.adultCount1">
                                    <button type="button" class="control-plus" @click="searchData.adultCount1++;return false;" :disabled="searchData.adultCount1 === 4">+</button>
                                </div>
                                <div>
                                    <label for="childCount1">@lang('deal.search_child_count'):</label>
                                    <button type="button" class="control-minus" @click="searchData.childCount1--;return false;" :disabled="searchData.childCount1 === 0">-</button>
                                    <input type="text" name="childCount1" placeholder="child number" v-model="searchData.childCount1">
                                    <button type="button" class="control-plus" @click="searchData.childCount1++;return false;" :disabled="searchData.childCount1 === 4">+</button>
                                </div>
                                <div class="child-age-wrapper" id="childAgeWrapper" v-show="searchData.childCount1 > 0">
                                    <label class="number-label child-age-label">@lang('home.childAge')</label>
                                    <div id="childAgeContent">
                                        <select name="childAge[]" class="child-age-select" v-for="(childAge, index) in childAges"
                                                @input="changeAge(childAge, index, $event)" :value="parseInt(childAge)">
                                            <option value="0"><1 @lang('home.yearsOld')</option>
                                            <option v-for="n in 17" :value="n">@{{ n }} @lang('home.yearsOld')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </mu-col>
                    <mu-col width="70" tablet="50" class="search-btn-wrapper">
                        <button type="button" class="search-btn" @click="submitSearch()">
                            @lang('deal.search_submit')
                        </button>
                    </mu-col>
                    <mu-col width="30" tablet="17" class="cancel-btn-wrapper">
                        <button type="button" class="cancel-btn" @click="isEditing = !isEditing">
                            @lang('deal.search_cancel')
                        </button>
                    </mu-col>
                </mu-row>
            </form>
        </div>

        @if($coupon->hide_image !== 1)
            <div class="coupon-banner">
                <div class="coupon-banner-bg"
                        style="background-image: url('{{ 'https://s3-us-west-1.amazonaws.com/usitrip/' . (config('app.env') === 'local' ? 'coupon_img_test/' : 'coupon_img/') . 'coupon_'.$coupon->id.'_mobile.png?2018' }}')">
                </div>
                <div class="coupon-banner-text">
                    <h1 class="coupon-title">@if($language == 0) {{ $coupon->title_zh }} @else {{ $coupon->title }} @endif</h1>
                    <p class="coupon-name">@if($language == 0) {{ $coupon->name_zh }} @else {{ $coupon->name }} @endif</p>
                    <p class="coupon-code">
                        @if($language == 0)
                            使用折扣码 <span>{{ $coupon->code }}</span>
                        @else
                        With coupon code <span>{{ $coupon->code }}</span>
                        @endif
                    </p>
                    <p class="coupon-details">
                        @if($language == 0) 点击查看 <a href="">详情</a> @else Click to <a href="">view details</a> @endif
                    </p>
                </div>
            </div>
        @endif

        <div class="bottom-wrapper">
            @if(count($hotelGroups) > 0)
                @php
                    $params = 'checkin=' . $checkin . '&checkout=' . $checkout . '&roomCount=1&adultCount1=2&childCount1=0';
                @endphp
                <div class="hotels-wrapper">
                    @if(count($hotelGroups) > 1)
                        <div class="destination-block">
                            <h2>@lang('deal.popular_destination')</h2>
                            <div :class="['destination-wrapper', stickyTop ? 'sticky' : '']">
                                @foreach($hotelGroups as $hotelGroup)
                                    <a href="#" data-target="#{{ str_replace(' ', '-', $hotelGroup['destination']['city']) }}" class="destination-toggler">{{ $hotelGroup['destination']['city'] }}</a>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @foreach($hotelGroups as $hotelGroup)
                        <div id="{{ str_replace(' ', '-', $hotelGroup['destination']['city']) }}" class="hotel-group row">
                            <div class="hotel-group-header col-xs-12 col-md-12">
                                <h2>{{ $hotelGroup['destination']['city'] }}</h2>
                            </div>

                            @foreach($hotelGroup['hotels'] as $index => $hotel)
                                <div class="hotel-item col-xs-12 col-md-4" id="hotelItem{{ $hotel['hotelId'] }}">
                                    <img src="{{ config('constants.image_host') . (stripos($hotel['thumbnail'], '_Image') === false ? 'Hotel_Image/' : '') . $hotel['thumbnail'] }}" alt="{{ $hotel['name'] }}">
                                    <span id="discountLabel{{ $hotel['hotelId'] }}" class="discount-label"></span>
                                    <div class="hotel-item-details">
                                        <h3 class="hotel-name">{{ $hotel['name'] }} @if($language === 0 && !empty($hotel['name_zh']))({{ $hotel['name_zh'] }})@endif</h3>
                                        <div class="hotel-rating-wrapper">
                                            <i class="hotel-rating hotel-rating-{{ $hotel['rating'] }}"></i>
                                        </div>
                                        <div class="hotel-price">
                                            ...
                                        </div>
                                        <a class="book-btn" target="_blank" href="{{ url('/hotel') . '/' . $hotel['hotelId'] . '.html?' . $params }}">@lang('deal.book_now')</a>
                                    </div>
                                </div>
                            @endforeach

                            <a href="{{ url('/list') . '/D' . $hotelGroup['destination']['desId'] . '.html' }}" class="view-more-btn">@lang('deal.more_hotels')</a>
                        </div>
                    @endforeach
                </div>
            @endif

            <mu-dialog :dialog-class="'policy-modal'" :open="policyModalOpen">
                <div class="modal-header text-center">
                    <h2>@lang('deal.policy_title')</h2>
                    <i class="modal-close" @click="policyModalOpen = false"></i>
                </div>
                <div class="modal-body">
                    @lang('deal.policy_content', ['start' => $coupon->checkin_start_date, 'end' => $coupon->checkin_end_date])
                </div>
            </mu-dialog>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var coupon = {!! $coupon !!},
          couponId = coupon.id,
          appUrl = '{{ config('app.url') }}',
            checkin = '{{ $checkin }}',
            checkout = '{{ $checkout }}';
    </script>

    <script type="text/javascript" src="{{ url('/assets/js/mobile/deal.js') }}"></script>
@endsection