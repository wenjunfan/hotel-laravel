{{--init content data--}}
<?php
$data = [
    [
        'title'   => '确认接纳和服务简介:',
        'content' => '<p>1.1 走四方专业旅游网（即http://www.usitrip.com）的所有权和经营管理权归美国联星旅游有限公司所有。会员一经完成走四方专业旅游网注册并加入销售联盟计划，经走四方营销联盟确认后，则视为会员完全同意本协议的全部内容及相关使用流程，成为走四方专业旅游网的会员并使用相关合作服务。</p>
                <p>1.2 走四方专业旅游网是面向个人和各商业网站、具有ICP以及ICP备案等相关证明的网站推出的网络营销业务合作计划。各合作网站所有者或者个人申请成为联盟会员并经过确认后，即可将走四方提供的有关业务宣传资料及链接放到各自拥有的合作网站上，最终会员通过此宣传资料及链接进入并购买走四方专业旅游网的产品，我方会按约定的比例和费率共同分享因此带来的收益。</p>'
    ],
    [
        'title'   => '会员权利:',
        'content' => '<p>2.1 走四方授予销售联盟会员一种可撤销的、非专属的、通用的权利，重新开发及传播走四方所提供的用于走四方代理机构"代理区"的名称、标口、商标、服务标记、 商业外观以及专有技术，以便达到在此协议期内，将销售联盟会员网站与走四方合作网站相链接之唯一目的。任何由销售联盟使用走四方合作网站的名称、标口、商 标、服务标记以及商业外观所带来的商业信誉仅保证走四方合作网站的利益，并不会给销售联盟带来任何所有权、产权以及信誉。</p>
                <p>2.2 会员仅可使用本计划提供的或由走四方发布的或在计划范围以内的商标、服务标记、商业外观、广告、内容链接、优惠券代码等（"创作"）。除上文所述之可容许 的方式外，任何将此创作用于其他网站、其他邮件或其他资源，用任何方式操作或源自其他任何方式的手段使用本创作的行为都是不容许的。</p>'
    ],
    [
        'title'   => '会员义务:',
        'content' => '<p>作为走四方营销联盟的会员，会员声明与保证：</p>
                <p>3.1 其及授权代表可以全权订立本协议并完全履行其与协议项下之义务。</p>
                <p>3.2 其拥有的合作网站已经获得了政府有关部门的所有许可和批准，有权进行网站的运行和经营。网站的经营严格遵守相关法律法规，网站所进行的市场开拓、广告宣传及相关经营活动合法。</p>
                <p>3.3 其自行配备上网及会员网站运营的所需设备，自行负担其上网和设备维护及进行此合作的电话费、信息费等有关费用。</p>
                <p>3.4 其对所提交的网站具有完全的所有权、使用权、决策权等相应权利，对其所有用来合作的网站自始至终负完全责任，其将努力推广其网站，发展网站流量，拓展最终会员，并自行负担其运营网站、发展流量、拓展会员的相关费用。</p>
                <p>3.5 其如实填写走四方营销联盟的会员登记表，提供真实、准确、反映现状和完整的会员资料包括个人信息和网站资料。</p>
                <p>3.6 其妥善保存并迅速地更新会员资料包括个人信息和网站资料，确保它们真实、准确、及时和完整。走四方营销联盟不承担对上述资料的真实性的审查责任。如果会员 所提供的会员资料失实、不准确或不完整，或走四方营销联盟有合理理由怀疑此会员资料失实、不准确、不及时或不完整，走四方营销联盟有权暂停或取消该会员作 为走四方营销联盟的会员资格，并且因此引起的法律纠纷由会员自行承担。</p>
                <p>3.7 会员不得以欺诈、胁迫等不正当手段损害走四方销售联盟的利益及声誉。严禁会员通过不合法的手段赚取佣金，一旦发现，走四方销售联盟有权立即取消合作，不再支付会员帐号上尚余的佣金，并有权追回已经支付的全部佣金，四方销售联盟保留进一步追索责任的权利；同时，该会员必须承担因此给走四方带来的所有损失。</p>'
    ],

    [
        'title'   => '佣金以及结算和支付:',
        'content' => '<p>4.1 走四方提供产品（产品在该计划中包括酒店提供的相关服务；产品不包括旅行团及门票服务）并支付规定的佣金以换取特定的广告服务以促成销售，走四方会及时支付佣金（需累计佣金达到$50），佣金支付时间为, 客人入住后每月10日前结算到销售联盟商的银行账户中， 会员不得要求获取拥金之外的其他报酬.</p>
                <p>4.2 "有效链接"是从销售联盟会员站点到走四方合作网站的一个链接。它采用一个专门的URL或其他由走四方提供的URL，当此链接是顾客在进行产品买卖会话时 连接到走四方合作网站的最后一个链接时，即为有效链接。"会话"是指(a) 30天内的一段时间，从顾客最初通过销售联盟会员网站与走四方合作网站相联系开始或此协议失效或终止； (b)顾客在30天内从不止一个会员网站与走四方合作网站相链接，那么从顾客最初通过联属网路行销网站与走四方的合作网站相连接开始到顾客通过非联属网路 行销网站回到走四方的合作网站或此协议失效或终止。  </p>
                <p>4.3 链接是否有效以及佣金是否可付将由走四方自主决定，并最终对走四方与会员产生约束，产品价格将由走四方自主决定。</p>
                <p>4.4 在销售联盟会员带来销售之前, 走四方可能会或者不会预先通知（a）任何产品的变动 (包括价格及旅游行程)、中断或终止，或 (b) 解除、变更、或修改任何走四方提交的产品图示或横幅广告。会员同意立即执行走四方关于解除、变更或修改任何走四方提交的、用于销售联盟的图示或广告的要求。 </p>
                <p>4.5 如果客户通过会员有效链接到走四方合作网站上购买，但其后此客户取消了该产品，或者因未能与我们达成一致和由于信用卡诈骗而导致信用卡付款失败，会员将无法通过该笔交易获取佣金。如果佣金已支付，会员应同意退还已获取的佣金。</p>
                <p>4.6 会员不能通过自己到走四方合作网站上购买获取拥金，解释权归走四方专业旅游网站所有。</p>
                <p>4.7推广链与优惠码佣金获得说明<br>
                    4.7.1 推广链佣金：<br>
                    客户点击您的“推广链”进入走四方网站的同时，其电脑Cookie中将保存您的“推广链”信息。客户在Cookie生成之日起30天内购买走四方产品，都会调用Cookie中您的“推广链”信息。订单成功后，您将会获得相应佣金。<br>
                    备注说明：<br>
                    A.如果客户点击多个不同“推广链”时，电脑中的Cookie信息将显示为最后一个推广者的链接，即佣金由最后一个“推广链”的拥有者获得。<br>
                    B.如果在Cookies生成的30天内客人主动清除Cookies，当客户再下单时，系统将无法调用订单的“推广链”信息，您也将无法获得佣金。<br>
                </p>'
    ],
    [
        'title'   => '我方平台义务:',
        'content' => '<p>5.1 有义务保证自身系统的正常运转，负责后台的开发、维护以及专区相关下载网页的设计及维护。</p>
                <p>5.2 提供的联盟广告是法律允许范围内的广告；</p>
                <p>5.3 提供一定的旅游产品和公司资料（包括但不限于一定数量的文字和图片）；</p>
                <p>5.4 保障提供的联盟广告内容所述的产品的内容健康、合法、真实；</p>
                <p>5.5 按协议约定及时将佣金以支票和paypal形式支付给会员；</p>
                <p>5.6 有权随时就联盟会员网站服务推广情况进行检查，包括但不限于：登录会员网站、电话咨询、会员访问等。</p>'
    ],
    [
        'title'   => '账号安全:',
        'content' => '<p>6.1 会员一旦注册成功，将得到一个密码和帐号。会员应对帐号和密码安全负全部责任。同时，每个会员都要对因使用会员的密码帐户或服务而进行的全部活动和事件负全责，因此所衍生的任何损失或损害，走四方营销联盟无法也不承担任何责任。</p>
                <p>6.2 会员不应将其帐号、密码转让或出借予他人使用。会员若发现有任何非法使用自己的密码和帐号或发生其他任何安全问题或存在安全漏洞的情况，应立即通知走四方营销联盟。</p>
                <p>6.3 会员没能保证其帐号及其密码的安全而遭受的任何损失，走四方营销联盟不承担任何法律责任。因黑客行为等非走四方营销联盟能控制和预见的因素导致帐号、密码遭他人非法使用，走四方营销联盟也均不承担任何法律责任。</p>
                <p>6.4 会员不得盗用其他会员帐号、密码，一旦发现，走四方营销联盟将冻结该会员的账号，并协助被盗用了账号的会员向该会员追究其法律责任，同时走四方营销联盟将保留进一步追究该会员法律责任的权利。</p>'
    ],
    [
        'title'   => '会员隐私制度:',
        'content' => '<p>7.1 走四方营销联盟保证不对外公开或向第三方提供会员注册资料及会员在使用网络服务时存储在走四方营销联盟的非公开内容，但下列情况除外：</p>
                <p>(a) 事先获得会员的明确授权；</p>
                <p>(b) 根据有关的法律法规要求；</p>
                <p>(c) 按照相关政府主管部门的要求；</p>
                <p>(d) 为维护社会公众的利益；</p>
                <p>(e) 为维护走四方营销联盟的合法权益。</p>
                <p>7.2 走四方营销联盟可能会与第三方合作向会员提供相关的网络服务，在此情况下，如该第三方同意承担与走四方营销联盟同等的保护会员隐私的责任，则走四方营销联盟可将会员的注册资料等提供给该第三方。
                </p><p>7.3 在不透露单个会员隐私资料的前提下，走四方营销联盟有权对整个会员数据库进行分析并对会员数据库进行商业上的利用。</p>'
    ],
    [
        'title'   => '中断或终止服务:',
        'content' => '<p>如发生下列任何一种情形，走四方有权随时中断或终止向会员提供本协定项下的网路服务，而无需对会员或任何第三方承担任何责任。 </p>
                <p>-- 会员向走四方提供的个人资料不真实。 </p>
                <p>-- 会员违反本协定的规则或不履行其所承担的义务。 </p>
                <p>除此之外，会员可随时根据需要通知走四方终止向该会员提供服务，会员服务终止后，会员使用服务的权利同时终止。自会员服务终止之时起，我方不再对该会员承担任何责任或义务。此协定终止或失效后，会员须立即停止使用走四方的名称、标口、商标、服务标记、商业外观、专有技术及其它创作。</p>'
    ],
    [
        'title'   => '免责声明:',
        'content' => ' <p>-- 会员提供的资讯仅代表其个人的立场与观点，与本网站的立场和观点无关。 </p>
                <p>-- 本网站对会员或第三方的以下损失概不承担任何责任： </p>
                <p>(a) 会员提供的资讯由于不正确、不完整、不及时而导致的会员自身的损失； </p>
                <p>(b) 会员因错误理解和使用、复制或传播本网站上的内容而造成的损失、损害或会员 提供的资讯涉及侵犯他人的版权、署名权等纠纷，纯属其个人行为，与本网站无关。 </p>
                <p>(c) 会员通过本网站而获得的链接或通过链接获得的产品、服务或资讯内容上的缺陷 （如：内容的不完整、不及时等）与本网站概不相关，因本网站仅提供链接的管道，而 不负责对该网站或网页的维护。</p>
                <p>(d) 因互联网本身的原因而引起的问题（如：网站服务导致的执行失败、错误、电脑病毒或电脑的硬体问题等而导致的问题）与本网站无关。 </p>
                <p>-- 本网站上提供的资讯仅供会员参考。若会员或流览者要使用，须进一步的调查与核实，否则引起的一切损失，本网站概不负责。 </p>
                <p>-- 本网站上的部分内容（如：图片）来源于互联网，因互联网上的作品权利人身份不便确认，故可能有部分内容涉及版权问题。若经核实涉及版权问题，本网站将即时予以删除或更正。 </p>
                <p>-- 本网站有权在本网站内使用会员在本网站上发表的资讯（包括但不限于图片、游记等）。但会员在本网站上发表的资讯若其他流览者要转载，需经过本网站和该会员的许可，否则视为侵权。</p>
                <p>因会员违反本法律声明而引发的任何一切索赔、损害等等，本网站概不负责。 </p>'
    ],
    [
        'title'   => '申诉:',
        'content' => '<p>10.1 双方都需要向另一方表示，对方有权讨论此协议，并充分享有授予任何承认的执照的权利。 </p>
                <p>10.2 会员表示∶<br>
                    （a）网站内容不<br>
                    &nbsp;&nbsp;&nbsp;&nbsp;（i）侵犯任何第三方的版权、专利、商标、商业秘密或其他专有权利，公开或保密的权利；<br>
                    &nbsp;&nbsp;&nbsp;&nbsp;（ii）违反任何适用的法律、法规、条例或规章；<br>
                    &nbsp;&nbsp;&nbsp;&nbsp;（iii）含有诽谤或诽谤材料；<br>
                    &nbsp;&nbsp;&nbsp;&nbsp;（iv）含有猥亵，色情或淫秽材料；<br>
                    &nbsp;&nbsp;&nbsp;&nbsp;（v）违反任何关于不公平竞争、反歧视或虚假广告的法律;<br>
                    &nbsp;&nbsp;&nbsp;&nbsp;（vi）宣扬暴力或含仇恨的演讲；<br>
                    或<br>
                    &nbsp;&nbsp;&nbsp;&nbsp;（vii）含病毒、特洛伊木马病 毒、蠕虫、时间炸弹，或其他类似的有害或破坏编程的程式；<br>
                    及<br>
                    （b）会员享有并遵守与联邦及州法律和法规相一致的隐私政策，这是突出显示在其网站上的。</p>
                <p>10.3 除上述陈述，任何一方都未向另一方作出任何表述或担保，其中包括，但不仅限于，任何关于商品的内在保证或为达某种特殊目的所采取的手段或策略。</p>'
    ],
    [
        'title'   => '违约责任:',
        'content' => '<p>11.1 会员违反本协议规定，走四方网络销售联盟有权视情节分别给予警告、停止合作、暂停发放会员的佣金、扣罚部分或全部佣金等不同处理，造成走四方网络销售联盟损失的，会员还需承担赔偿责任。会员的违约行为严重的，走四方网络销售联盟保留进一步追究其法律责任的权利。</p>
                <p>11.2 对于走四方网络销售联盟及其职员、关联人士、授权人和代表任何一方可能须承担的任何损失、索偿、法律行动、损害、责任和开支，只要有关损失是因或基于会员违反本协议或相关法律的索偿而产生，或因会员或会员的代理或关联人士（以参与者的身份行事）所作出的行为或疏忽而造成，会员应就此向上述人士作出赔偿并使他们免受损害。</p>
                <p>若会员因违反本协议或者中华人民共和国的法律法规而给走四方网络销售联盟或者任何第三方造成损失的，会员应负责赔偿。</p>
                <p>11.3 如因走四方网络销售联盟过错原因造成会员的损失，应给予会员相应的补偿，补偿金额不得超过当月应当支付给会员的佣金。</p>
                <p>11.4 会员作为参与者进行活动所造成的任何间接的、附带的、特殊的、结果性的或惩戒性的损害（包括但不限于利润亏损、商誉、使用服务、数据的损害或其它无形损失（即使走四方网络销售联盟已获告知产生有关损害的可能性），走四方网络销售联盟概无须负任何责任。会员或会员的代理或关联人士违反本协议或相关法律，或因会员或会员的代理人或者关联人士自身原因造成的任何损害，概与走四方网络销售联盟无关，由会员自行负责。</p>'
    ],
    [
        'title'   => '法律:',
        'content' => '<p>12.1 此协议已制定并应按照加州法律理解和执行。任何因本协议引起的纠纷均应在洛杉矶联邦或州法院提出诉讼。如果您需要发送正式的函件，请挂号邮至∶17870 Castleton St. Suite 358-388, City of Industry CA 91748 (358为客服中心)</p>
                <p>12.2 本协议相关内容可根据国家政策、法律法规、移动运营商的策略调整而进行相应调整。本协议（以不时修订的版本为准）构成各方就本文内容而达成的完整协议，并取代各方之间就有关内容而在较早前达成的全部书面或口头协议。走四方对任何违反或未遵守本条款给予的豁免，不得视为对任何先前或其后的违反或未遵守本条款的豁免。本文所用的条款标题仅为方便起见而供参考之用，并不具备任何法律含义。</p>
                <p>12.3 如本协议中的任何条款无论因何种原因完全或部分无效或不具有执行力，本协议的其余条款仍应有效并且有约束力。</p>'
    ]
];
$data_en = [
    [
        'title'   => 'Confirmation of acceptance and service introduction:',
        'content' => '<p>1.1 The ownership and management of the Usitour Professional Travel Network (http://www.usitour.com) is owned by UnitedStars International Ltd Once the member has completed the registration of the Usitour Professional Travel Network and joined the sales alliance plan, after confirming by the Usitour Marketing Alliance, it is deemed that the member fully agrees with the entire content of the agreement and the related use process, and becomes a member of the Usitour Professional Travel Network and uses relevant Cooperative service.</p>
                <p>1.2 The Usitour Professional Travel Network is an online marketing business cooperation plan for individuals and various commercial websites, websites with relevant certificates such as ICP and ICP filing. After the owners or individuals of each cooperation website apply to become members of the alliance and have confirmed, they can put the relevant business promotion materials and links provided by the Usitour on their respective cooperation websites. Finally, the members enter and purchase through the promotional materials and links. For the products of UnitedStars International Ltd Professional Travel Network, we will share the benefits brought by the agreed proportions and rates.</p>'
    ],
    [
        'title'   => 'Member rights:',
        'content' => '<p>2.1 Unitedstars International Ltd to grant the Sales Alliance members a revocable, non-exclusive, universal right to re-develop and disseminate the names, labels, trademarks, and service marks provided by the Usitour for the "agent area" of the Usitour Agency. , trade dress and know-how, in order to achieve the sole purpose of linking the sales affiliate website to the four-party cooperation website during the term of this agreement. Any business reputation brought by the sales alliance using the name, logo, trademark, service mark and commercial appearance of the Usitour website only guarantees the interests of the Usitour partner website and does not bring any ownership or property rights to the sales alliance.</p>
                <p>2.2 Members may only use trademarks, service marks, trade dress, advertisements, content links, coupon codes, etc. ("creation") provided by the Program or issued by the Usitour or within the scope of the Program. Except for the permissible methods described above, any use of this creation for any other website, other mail or other resources, in any way or by any other means is not permitted.</p>'
    ],
    [
        'title'   => 'Member obligations:',
        'content' => '<p>As a member of the Usitour Marketing Alliance, member declarations and warranties:</p>
                <p>3.1 It and its authorized representative may enter into this Agreement at its sole discretion and fully perform its obligations under the Agreement.</p>
                <p>3.2 The partner website owned by the company has obtained all the licenses and approvals from relevant government departments and has the right to operate and operate the website. The operation of the website strictly abides by relevant laws and regulations, and the market development, advertising and related business activities carried out by the website are legal.</p>
                <p>3.3 It is equipped with the necessary equipment for online operation and member website operation, and bears the expenses related to the Internet access and equipment maintenance and the telephone and information fees for this cooperation.</p>
                <p>3.4 It has full rights to the submitted website, such as ownership, use rights, decision-making rights, etc., and it is fully responsible for all the websites used for cooperation. It will strive to promote its website, develop website traffic, expand the final membership, and Self-pay for its operating website, development of traffic, and expansion of membership fees.</p>
                <p>3.5 It fills in the membership registration form of the Usitour Marketing Alliance and provides true, accurate, current status and complete membership information including personal information and website information.</p>
                <p>3.6 It keeps and promptly updates member information, including personal information and website information, to ensure that they are true, accurate, timely and complete. The Usitour Marketing Alliance does not assume responsibility for reviewing the authenticity of the above information. If the membership information provided by the member is inaccurate, inaccurate or incomplete, or if the Usitour Marketing Alliance has reasonable grounds to suspect that the member\'s information is inaccurate, inaccurate, untimely or incomplete, the Usitour Marketing Alliance has the right to suspend or cancel the member. Going to the membership of the Usitour Marketing Alliance, and the resulting legal disputes are borne by the members themselves.</p>
                <p>3.7 Members shall not damage the interests and reputation of the Usitour Sales Alliance by fraud or coercion. It is strictly forbidden for members to earn commissions through illegal means. Once found, the Usitour Sales Alliance has the right to cancel the cooperation immediately, no longer pay the remaining commission on the member account, and has the right to recover all the commissions already paid. The Usitour Sales Alliance reserves The right to further recourse is also borne; at the same time, the member must bear all the losses incurred by the four parties.</p>'
    ],

    [
        'title'   => 'Commission and settlement and payment:',
        'content' => '<p>4.1 Unitedstars International Ltd to provide the products (the products include related services provided by the hotel in the plan; the products do not include tour groups and ticket services) and pay the prescribed commissions in exchange for specific advertising services to facilitate sales, and the four parties will pay commissions in time (required) The accumulated commission reaches $50), the commission payment time is, and the customer settles in the bank account of the sales affiliate before the 10th of each month after the check-in. The member may not ask for other rewards other than the bonus.</p>
                <p>4.2 "Effective Link" is a link from the sales affiliate site to the four-party partner site. It uses a special URL or other URL provided by the Usitour, which is a valid link when the customer connects to the last link to the Usitour website when they are in a product buying and selling session. "Session" means (a) a period of time within 30 days from the initial contact of the Customer through the Sales Alliance Member Site to the Usitour Partner Website or the expiration or termination of the Agreement; (b) Customer has more than one Member Site within 30 days By linking to the Usitour website, the customer initially connects to the partner website through the affiliate marketing website and then the customer returns to the partner website through the non-affiliated network marketing website or the agreement expires or terminates.</p>
                <p>4.3 Whether the link is valid and whether the commission is payable will be decided by the Usitour, and finally the restrictions will be imposed on the Usitour and the member. The price of the product will be determined by the Usitour.</p>
                <p>4.4 Before the sales affiliates bring sales, the Usitour may or may not notify (a) any product changes (including price and travel itinerary), interruption or termination, or (b) dismiss, change, or modify any of the products. Product icon or banner advertisement submitted by Usitour. Members agree to immediately execute the requirements of the Usitour regarding the removal, alteration or modification of any icon or advertisement submitted by the Usitour for the sale of the Alliance. </p>
                <p>4.5 If the customer purchases through the member\'s effective link to the four-party cooperation website, but the customer cancels the product afterwards, or fails to reach an agreement with us and the credit card payment fails due to credit card fraud, the member will not be able to pass the transaction. Get a commission. If the commission has been paid, the member should agree to refund the commission that has been paid.</p>
                <p>4.6 Members can\'t buy and get the money by going to the Usitour cooperation website, and the right to interpret belongs to the Usitour professional travel website.</p>
                <p>4.7 Promotion Chain and Coupon Code Commission Instructions<br>
                    4.7.1 Promotion chain commission:<br>
                    When the customer clicks on your "promotional chain" to enter the Usitour website, your "promotional chain" information will be saved in the computer cookie. By purchasing the Usitour product within 30 days of the date the cookie is generated, the customer will call your "Promotion Chain" information in the cookie. Once the order is successful, you will receive a commission.<br>
                   instruction manual:<br>
                   A. If the customer clicks on multiple different "promotional chains", the cookie information in the computer will be displayed as the link of the last promoter, ie the commission is obtained by the owner of the last "promotion chain".<br>
                   B. If the customer actively clears the cookie within 30 days of the cookie generation, when the customer places the order again, the system will not be able to call the “promotion chain” information of the order, and you will not be able to get the commission.<br>
                </p>'
    ],
    [
        'title'   => 'Our platform obligations:',
        'content' => '<p>5.1 Obligation to ensure the normal operation of its own system, responsible for the development and maintenance of the background and the design and maintenance of the relevant download page.</p>
                <p>5.2 The provided affiliate advertisements are advertisements within the scope permitted by law;</p>
                <p>5.3 Provide certain tourism products and company information (including but not limited to a certain number of texts and pictures);</p>
                <p>5.4 Ensure that the content of the products described in the affiliated advertising content is healthy, legal and authentic;</p>
                <p>5.5 Pay the commission to the member in the form of cheque and paypal in time according to the agreement;</p>
                <p>5.6 Have the right to check the promotion of the affiliate website services at any time, including but not limited to: login to member websites, telephone consultation, member visits, etc.</p>'
    ],
    [
        'title'   => 'Account security:',
        'content' => '<p>6.1 Once the member has successfully registered, he will get a password and account number. Members are solely responsible for account and password security. At the same time, each member is fully responsible for all activities and events that are carried out due to the use of the member\'s password account or service. Therefore, any loss or damage caused by the four parties cannot and does not assume any responsibility.</p>
                <p>6.2 Members should not transfer or lend their account number or password to others. Members should immediately notify the Usitour Marketing Alliance if they find any illegal use of their own passwords and accounts or any other security issues or security breaches.</p>
                <p>6.3 If the member fails to guarantee the security of his account and its password, the Usitour Marketing Alliance will not bear any legal responsibility. Due to hacking behavior and other factors that can be controlled and foreseen by the Usitour Marketing Alliance, the account and password are illegally used by others, and the Usitour Marketing Alliance does not assume any legal responsibility.</p>
                <p>6.4 Members shall not steal other member accounts and passwords. Once found, the Usitour Marketing Alliance will freeze the member\'s account and assist the member who has stolen the account to pursue the legal liability of the member. At the same time, the Usitour Marketing Alliance will retain further investigation. The right to legal liability of members.</p>'
    ],
    [
        'title'   => 'Member privacy system:',
        'content' => '<p>7.1 The Usitour Marketing Alliance guarantees that the membership registration information will not be made public or provided to third parties and that the members will store the non-public content of the Usitour Marketing Alliance when using the web service, except in the following cases:</p>
                <p>(a) Obtain prior authorization from the member;</p>
                <p>(b) in accordance with relevant laws and regulations;</p>
                <p>(c) in accordance with the requirements of the relevant government authorities;</p>
                <p>(d) for the benefit of the public;</p>
                <p>(e) To safeguard the legitimate rights and interests of the Usitour Marketing Alliance.</p>
                <p>7.2 The Usitour Marketing Alliance may cooperate with third parties to provide relevant network services to the members. In this case, if the third party agrees to assume the same responsibility as protecting the member\'s privacy with the Usitour Marketing Alliance, then the Usitour Marketing Alliance may The member\'s registration information, etc. are provided to the third party.
                </p><p>7.3 Under the premise of not revealing the individual member\'s privacy information, the Usitour Marketing Alliance has the right to analyze the entire member database and make commercial use of the member database.</p>'
    ],
    [
        'title'   => 'Interrupt or terminate the service:',
        'content' => '<p>In the event of any of the following circumstances, the Usitour reserves the right to discontinue or terminate the provision of the Internet Services under this Agreement to the Member at any time without any liability to the Member or any third party.</p>
                <p>-- The personal information provided by members to the Usitour is not true. </p>
                <p>-- Members violate the rules of this Agreement or fail to fulfill their obligations. </p>
                <p>In addition, the member may terminate the service to the member at any time upon request, and the member\'s right to use the service shall be terminated at the same time. Since the termination of the Member Services, we no longer assume any responsibility or obligation to the Member. Upon termination or expiration of this Agreement, Members shall immediately cease to use the names, logos, trademarks, service marks, trade dress, proprietary technology and other creations of the Usitour.</p>'
    ],
    [
        'title'   => 'Disclaimer:',
        'content' => ' <p>-- The information provided by the members represents only their personal position and opinions, and has nothing to do with the position and opinions of this website.</p>
                <p>-- This website does not assume any responsibility for the following losses of members or third parties: </p>
                <p>(a) the loss of the member\'s own information due to incorrect, incomplete or untimely information provided by the member;</p>
                <p>(b) The loss, damage or information provided by the member due to misunderstanding and use, copy or transmission of the contents of this website involves infringement of the copyrights, authorships and other disputes of others. It is purely personal and has nothing to do with this website. . </p>
                <p>(c) The link obtained by the member through this website or the defect in the product, service or information content obtained through the link (such as: incomplete content, untimely, etc.) is not relevant to this website, as this website only provides links. The pipeline is not responsible for the maintenance of the site or webpage.</p>
                <p>(d) Problems caused by the Internet itself (such as problems caused by website service failures, errors, computer viruses or computer hardware problems) are not related to this website. </p>
                <p>-- The information provided on this website is for member reference only. If a member or a visitor wants to use it, further investigation and verification is required. Otherwise, this website is not responsible for any losses. </p>
                <p>-- Some of the content (such as pictures) on this website comes from the Internet. Because the identity of the right holder of the work on the Internet is inconvenient to confirm, there may be some content related to copyright issues. If verified to involve copyright issues, this website will be deleted or corrected immediately. </p>
                <p>-- This website has the right to use the information published by members on this website (including but not limited to pictures, travel notes, etc.) on this website. However, if the information published by members on this website is to be reprinted by other visitors, it must be approved by this website and the member, otherwise it will be considered infringement.</p>
                <p>This website is not responsible for any claims, damages, etc. caused by members\' violation of this legal notice. </p>'
    ],
    [
        'title'   => 'Appeal:',
        'content' => '<p>10.1 Both parties need to indicate to the other party that the other party has the right to discuss the agreement and fully enjoy the right to grant any recognized license.</p>
                <p>10.2 Member states:<br>
                     &nbsp;&nbsp;&nbsp;&nbsp;(a) The content of the website is not<br>
                     &nbsp;&nbsp;&nbsp;&nbsp;(i) the right to infringe the copyright, patent, trademark, trade secret or other proprietary right of any third party, whether it is public or confidential;<br>
                     &nbsp;&nbsp;&nbsp;&nbsp;(ii) violates any applicable laws, rules, regulations or rules;<br>
                     &nbsp;&nbsp;&nbsp;&nbsp;(iii) containing bismuth or antimony materials;<br>
                     &nbsp;&nbsp;&nbsp;&nbsp;(iv) contains sputum, pornography or obscenity material;<br>
                    &nbsp;&nbsp;&nbsp;&nbsp;(v) violate any law on unfair competition, anti-discrimination or false advertising;<br>
                       &nbsp;&nbsp;&nbsp;&nbsp;(vi) a speech that promotes violence or hate;<br>
                    or<br>
                    &nbsp;&nbsp;&nbsp;&nbsp;    (vii) a program containing viruses, Trojan horse viruses, worms, time bombs, or other similar harmful or destructive programming;<br>
                    and<br>
                    (b) Members enjoy and comply with privacy policies consistent with federal and state laws and regulations, which are highlighted on their website.</p>
                <p>10.3 Except as set forth above, neither party makes any representations or warranties to the other party, including, but not limited to, any intrinsic warranties for the goods or the means or strategies adopted for a particular purpose.
</p>'
    ],
    [
        'title'   => 'Liability for breach of contract:',
        'content' => '<p>11.1 Members violate the provisions of this agreement, and the Usitour Network Sales Alliance has the right to separately give warnings, stop cooperation, suspend the issuance of members\' commissions, and deduct some or all of the commissions according to the circumstances, resulting in the loss of the Usitour Network Sales Alliance. Responsibility for compensation. If the member\'s breach of contract is serious, the Usitour Network Sales Alliance reserves the right to further pursue its legal responsibility.</p>
                <p>11.2 any loss, claim, legal action, damage, liability and expense that may be incurred by any party to the Usitour Network Sales Alliance and its employees, affiliates, licensors and representatives, as long as the loss is due to or based on a member’s breach of this Agreement or Members shall compensate the above-mentioned persons for damages and damages caused by the actions of the relevant laws or the acts or omissions of the members or members of the agents or related persons (acting as the participants).</p>
                <p>If a member causes damage to the Usitour Network Sales Alliance or any third party due to violation of this Agreement or the laws and regulations of the People\'s Republic of China, the Member shall be liable for compensation.</p>
                <p>11.3 If the member\'s loss is caused by the fault of the Usitour Network Sales Alliance, the member shall be compensated accordingly, and the compensation amount shall not exceed the commission that should be paid to the member in the current month.</p>
                <p>11.4 Any indirect, incidental, special, consequential or exemplary damages (including but not limited to loss of profits, goodwill, use of services, damage to data or other intangible damages) caused by the activities of the Member as a participant ( Even if the Usitour Network Sales Alliance has been informed of the possibility of damage, there is no liability for the Usitour Network Sales Alliance. Members or affiliates or affiliates are in breach of this Agreement or related laws, or are acting as members or members. Any damage caused by the person or related person\'s own reasons is irrelevant to the UnitedStars International Ltd Network Sales Alliance, and the member is responsible for it.</p>'
    ],
    [
        'title'   => 'Legal:',
        'content' => '<p>12.1 This Agreement has been developed and understood and implemented in accordance with the laws of the State of California. Any dispute arising out of this Agreement shall be filed in a federal or state court in Los Angeles. If you need to send a formal letter, please log in to: 17870 Castleton St. Suite 358-388, City of Industry CA 91748 (358 for Customer Service Center)</p>
                <p>12.2 The relevant content of this agreement can be adjusted according to national policies, laws and regulations, and mobile operator\'s strategic adjustments. This Agreement (which is subject to change from time to time) constitutes the entire agreement between the parties with respect to the content herein and supersedes all written or oral agreements between the parties at the earlier date. Any waiver by the Quartet against any breach or non-compliance with this clause shall not be deemed a waiver of any prior or subsequent violation or non-compliance with this clause. The headings of the articles used herein are for convenience only and do not have any legal meaning.</p>
                <p>12.3 If any provision of this Agreement is wholly or partially invalid or unenforceable for any reason, the remaining provisions of this Agreement shall remain valid and binding.</p>'
    ]
];
$domain = $is_usitour ? 'Usitour' : 'Usitrip';
$infos = $language == 0 ? $data:$data_en;
?>

@extends('layouts.admin')

@if	($language == 0)
    @section('title', $domain. '酒店预订平台 - 人人赚分销联盟条例')
@elseif	($language == 2)
    @section('title', $domain. '飯店預定平台 - 人人賺分銷聯盟條例')
@else
    @section('title', 'Affiliate Partnership Policies | ' . $domain)
@endif

@section('css')
    <style>
        .main-title{
            margin-left: 60px;
            margin-bottom: 30px;
        }
        .timeline-item .date {
            /*width: 280px;*/
        }
        .content-title{
            margin-bottom: 20px;
        }
        .timeline-item .content{
            padding-bottom: 18px;
        }
        .content-index{
            font-size: 24px;
            font-weight: bold;
            width: 50px !important;
        }
    </style>
@endsection

@section('content')
    <div class="row animated fadeInRight">
        <h1 class="main-title">{{$language == 0? '欢迎使用走四方分销联盟' : 'Welcome to UnitedStars International Ltd Hotel affiliate partnership'}}</h1>

        <div class="ibox-content inspinia-timeline">
            <h2 class="main-title">{{$language == 0 ? '联盟条例' : 'Affiliate Policies'}}</h2>
            @foreach($infos as $key=>$item)
                <div class="timeline-item">
                    <div class="row">
                        <div class="col-xs-5 col-md-5 date">
                            <i class="content-index">{{$key + 1}}</i>
                        </div>
                        <div class="col-xs-7 col-md-7 content no-top-border">
                            <h3 class="content-title"><strong>{{$item['title']}}</strong></h3>
                            <p>{!! $item['content'] !!}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

