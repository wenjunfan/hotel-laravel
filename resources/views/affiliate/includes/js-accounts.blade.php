<script type="text/javascript">
    jQuery(function ($) {
        $('#from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('.dataTables-accounts').DataTable({
            "order": [[0, "desc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: '分销账户列表'},
                {extend: 'pdf', title: '分销账户列表'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ]
        });
    });

    function changeAffiliateCommission(id, ratio) {
        swal({
              title: "请填写分销佣金大小",
              text: "请填写0~0.15范围的小数（即0%至15%）",
              type: "input",
              showCancelButton: true,
              cancelButtonText: "取消",
              confirmButtonText: "确定",
              closeOnConfirm: false,
              animation: "slide-from-top",
              inputPlaceholder: ratio
          },
          function (inputValue) {
              if (inputValue === false) {
                  return false;
              }

              if (inputValue === "" || isNaN(inputValue)) {
                  swal.showInputError("请填写一个小数");
                  return false;
              }

              var f = parseFloat(inputValue);
              if (f < 0 || f > 0.15) {
                  swal.showInputError("请填写0~0.15范围的小数（0.03即是3%）");
                  return false;
              }
              $.ajax({
                  method: 'PUT',
                  headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                  url: "{{url('/affiliate/accounts/')}}/" + id,
                  data: {'ratio': f},
                  success: function (data) {
                      if (data.success) {
                          swal({
                              title: "更改成功",
                              text: "",
                              confirmButtonText: "确定",
                              type: "success"
                          }, function () {
                              self.location = '{{url('/affiliate/accounts')}}';
                          });
                      }
                      else {
                          swal(data.message);
                      }
                  },
                  error: function (xhr, e) {
                      swal(xhr.status + " " + e);
                  },
              });
          });
    }


    function search () {
      var from = $('#from').val();
    var to = $('#to').val();
    var keywords = $("#keywords").val();
    self.location = "{{url('/affiliate/accounts')}}"
        + '?from' + from
        + '&to=' + to
        + '&keywords=' + keywords;
    }

</script>