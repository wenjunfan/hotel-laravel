<script type="text/javascript">
    jQuery(function ($) {
        if('{{Auth::user()->id}}' === '10672'){ //只有技术账号可以打印列表
            $('.dataTables-potentialOrderLists').DataTable({
                "order": [[3, "desc"]],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {
                        extend: 'excel',
                        title: '潜在订单'
                    },
                    {
                        extend: 'pdf',
                        title: '潜在订单'
                    },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ]
            });
        }else{
            $('.dataTables-potentialOrderLists').DataTable({
                'order': [[3, 'desc']],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: []
            });
        }
    });

    function formatDate(date) {
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oct",
            "Nov", "Dec"
        ];
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
        var hour = date.getHours();
        var minute = date.getMinutes();
        var second = date.getUTCSeconds();
        return day + '-' + monthNames[monthIndex] + '-' + year +' '+ hour + ':' +minute + ':' + second + ' ';
    }

    function addNewNote(id) {
        var newCommend = document.getElementById('new-commend-' + id).value;
        FullscreenSpinner.create();
        var authUser = '{{Auth::user()->id}}';
        if (authUser == 10150) {
            sales_no = 628;
        } else {
                @if(isset($_COOKIE['a']))
            var salesA = '{{$_COOKIE['a']}}';
            sales_no = salesA;
            @else
              sales_no = '{{Auth::user()->id}}';
            @endif
        }

        var newCommend = formatDate(new Date()) + newCommend + '  (' + sales_no + ')';

        $.post("{{url('/potential-order/comment')}}", {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'id': id,
            'commend': newCommend,
        }, function (data) {
            FullscreenSpinner.destroy();
            if (data.success) {
                $('#old-commend-' + id).text(newCommend + $('#old-commend-' + id).text());
                swal("更新成功", data.message, "success");
                location.reload();
                document.getElementById('commend' + id).readOnly = true;
                $("#roEditBtn" + id).toggleClass("hidden");
                $("#roSaveBtn" + id).toggleClass("hidden");
            } else {
                swal("更新失败", data.message, "error");
            }
        });
    }

    function isFollowedOrder(id) {
        var checkbox = document.getElementById("isFollowedOrder" + id);
            @if(isset($_COOKIE['a']))
        var sales = '{{$_COOKIE['a']}}';
        rep = sales;
        @else
          rep = '{{Auth::user()->id}}';
        @endif
        if (checkbox.checked == true) {
            FullscreenSpinner.create();
            $.post("{{url('/potential-order/followup')}}", {
                '_token': $('meta[name="csrf_token"]').attr('content'),
                'id': id,
                'rep':rep,
                'isFollow': 1
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    swal("更新成功", data.message, "success");
                    location.reload();
                } else {
                    swal("更新失败", data.message, "error");
                    checkbox.checked = false;
                }
            });
        } else {
            checkbox.checked = true;
            var click = 0;
            swal({
                title: "确认取消吗？",
                text: "",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "返回",
                confirmButtonColor: "#007FF8",
                confirmButtonText: "确定",
                closeOnConfirm: false
            }, function () {
                click = 1;
                FullscreenSpinner.create();
                $.post("{{url('/potential-order/followup')}}", {
                    '_token': $('meta[name="csrf_token"]').attr('content'),
                    'id': id,
                    'rep':"no one yet",
                    'isFollow': 0
                }, function (data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal("更新成功", data.message, "success");
                        location.reload();
                        checkbox.checked = false;
                    } else {
                        swal("更新失败", data.message, "error");
                    }
                });
            });
        }
    }

    function ableAddOrderId(id) {
        $("#roEditBtn" + id).toggleClass("hidden");
        $("#roSaveBtn" + id).toggleClass("hidden");
        document.getElementById('linked_order_id_' + id).readOnly = false;
    }
    function addLinkedOrderId(id) {
        var linked_order_id = document.getElementById('linked_order_id_' + id).value;
        var sales_no = '{{isset($_COOKIE['a']) ? $_COOKIE['a']: Auth::user()->id}}';
        FullscreenSpinner.create();
        $.post("{{url('/potential-order/link')}}", {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'id': id,
            'linked_order_id': linked_order_id,
            'sales_no' : sales_no,
        }, function (data) {
            FullscreenSpinner.destroy();
            if (data.success) {
                swal("更新成功", data.message, "success");
                document.getElementById('linked_order_id_' + id).readOnly = true;
                $("#roEditBtn" + id).toggleClass("hidden");
                $("#roSaveBtn" + id).toggleClass("hidden");
            }
            else {
                swal("更新失败", data.message, "error");
            }
        });
    }
    function isDoneOrder(id) {
        var checkbox = document.getElementById("isDoneOrder" + id);
        if (checkbox.checked == true) {
            FullscreenSpinner.create();
            $.post("{{url('/potential-order/complete')}}", {
                '_token': $('meta[name="csrf_token"]').attr('content'),
                'id': id,
                'isDone': 1
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    swal("更新成功", data.message, "success");
                    location.reload();
                } else {
                    swal("更新失败", data.message, "error");
                    checkbox.checked = false;
                }
            });
        } else {
            checkbox.checked = true;

            var click = 0;
            swal({
                title: "确认取消吗？",
                text: "",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "返回",
                closeOnConfirm: false,
                confirmButtonColor: "#007FF8",
                confirmButtonText: "确定",
            }, function () {
                click = 1;

                FullscreenSpinner.create();
                $.post("{{url('/potential-order/complete')}}", {
                    '_token': $('meta[name="csrf_token"]').attr('content'),
                    'id': id,
                    'isDone': 0
                }, function (data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal("更新成功", data.message, "success");
                        location.reload();
                        checkbox.checked = false;
                    } else {
                        swal("更新失败", data.message, "error");
                    }
                });
            });
        }
    }

    {{--搜索赋值给url进行get检索--}}
    function search () {
        var isFollow = $('#isFollow').val();
        var isDone = $('#isDone').val();
        var email = $('#email').val();
        var fromUsitour = $('#fromUsitour').val();
        self.location = "{{url('/affiliate/potential-lists')}}"
          + '?isFollow=' + isFollow
          + '&isDone=' + isDone
          + '&email=' + email
          + '&fromUsitour=' + fromUsitour
        ;
    }
</script>