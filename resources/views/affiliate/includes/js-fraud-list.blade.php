<script type="text/javascript">
    {{--这里是同时使用datatable and footable的范例。--}}
    $(document).ready(function () {
        $('.dataTables-admin-orders').DataTable({
            'order': [[0, 'desc']],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {
                    extend: 'excel',
                    title: '黑白名单列表'
                },
                {
                    extend: 'pdf',
                    title: '黑白名单列表'
                },
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg')
                        $(win.document.body).css('font-size', '10px')
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit')
                    }
                }
            ],
            "fnDrawCallback": function () {
                // $('.dataTables-admin-orders').footable();
                {{--stop loading--}}
                $('.table-data').children('.ibox-content').removeClass('sk-loading');
            }
        });

    });

    //get done order id
    function showOrderId (id) {
        $.ajax({
            type: 'GET',
            url: '/fraud-order/order-id/' + id,
            dataType: 'json',
            success: function (orderid) {
                if (orderid) {
                    $("#seeorderId_" + id).html('<p class="text-success">订单号：' + orderid + '</p>');
                }
            }
        });
    }

    //get more info new 20180710
    function showMoreInfoModal (id) {
        $('.table-more-info').hide();
        $('.fraud-loader').show();
        $('#fraudMoreInfoModal').modal('show');
        $('#fraudMoreInfoModalLabel').text('可疑订单号： ' + id);
        var card_holder = $('#card-holder-' + id).text();
        var guest_name = $('#guest-name-' + id).text();

        // get fraud detail info
        $.ajax({
            type: 'GET',
            url: '/fraud-order/info/' + id,
            dataType: 'json',
            success: function (data) {
                // check mobile type
                if (data.phone_num_type == 'mobile') {
                    data.phone_num_type = '<span class="text-success">' + data.phone_num_type + '</span>';
                } else if (data.phone_num_type != '' && data.phone_num_type != null) {
                    data.phone_num_type = '<span class="text-danger">' + data.phone_num_type + '</span>';
                }
                // error message
                err_message = '';
                if (data.error_list && data.error_list.length > 0) {
                    errors = JSON.parse(data.error_list);
                    for (i = 0; i < errors.length; i++) {
                        err_message += (i + 1) + ': ' + errors[i].errDesc + "<br>";
                    }
                }
                $('.fraud-loader').hide();
                $('.table-more-info').fadeIn();
                $('#td-names').text('持卡人：' + card_holder + '， ' + '入住人：' + guest_name);
                $('#td-hotel-name').text(data.hotelName);
                $('#td-check-in').text(data.checkin + ' / ' + data.checkout);
                $('#td-days').text(data.daybetween);
                $('#td-data-1').text(data.days + '天, ' + data.rooms + '房 , ' + data.people + "人");
                $('#td-address').text(data.address);
                $('#td-data-2').html('<span id="ipCountry_' + id + '"></span>');
                $('#td-data-3').text(data.street + ', ' + data.cityState + ', ' + data.cardCountry + ', ' + data.zipCode);
                $('#td-phone').html(data.phoneCountry + data.phoneCity + ' (' + data.phone_num_type + ') ');
                $('#td-email').html(data.userEmail);
                $('#td-user-lang').text(data.userLang);
                $('#td-order-created').text(data.localTime);
                $('#td-danger-level').html('<span class="font-bold">可疑信息: </span><br>' + err_message);
                $('#td-fail-count').text(data.cc_error_count);
                $('#td-new-user').text(data.isNewUser);
                $('#ipCountry_' + id).text(data.ipLocation);
                $("#phone_type_" + id).html(data.phone_num_type);
            }
        });
    }

    //修改控制fraud的变量
    function changeConfig (id) {
        var value;
        switch (id) {
            case 1 :
                value = document.getElementById('1').value;
                break;
            case 10 :
                value = document.getElementById('10').value;
                break;
            case 11 :
                value = document.getElementById('11').value;
                break;
            case 12 :
                value = document.getElementById('12').value;
                break;
            case 13 :
                value = document.getElementById('13').value;
                break;
            case 14 :
                value = document.getElementById('14').value;
                break;
        }

        $.post("{{url('/affiliate/config/update')}}", {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'id': id,
            'value': value,
        }, function (data) {
            FullscreenSpinner.destroy();
            if (data.success) {
                swal("更新成功", data.message, "success");
                location.reload();
            } else {
                swal("更新失败", data.message, "error");
            }
        });
    }

    function formatDate (date) {
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "June", "July",
            "Aug", "Sept", "Oct",
            "Nov", "Dec"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
        var hour = date.getHours();
        var minute = date.getMinutes();
        return day + '-' + monthNames[monthIndex] + '-' + year + ' ' + hour + ':' + minute + ' ';
    }

    function sendEmailAgain (id) {
        $('.loadingA' + id).button('loading');
        $.post("{{url('/affiliate/fraud-order/email/resend')}}/" + id, {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'id': id,
        }, function (data) {
            $('.loadingA' + id).button('reset');
            if (data.success) {
                swal("邮件发送成功", data.message, "success");
                location.reload();
            } else {
                swal({
                    title: "邮件发送失败",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "ok",
                });
                return false;
            }
        });
    }

    function isFraud (id) {
        swal({
                title: "确认要将此卡设为永不可下单的黑名单吗？",
                text: "",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "取消",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确认",
                closeOnConfirm: true
            },
            function () {
                $('.loadingBtn' + id).button('loading');
                $.post("{{url('/fraud-order/blacklist/add')}}/" + id, {
                    '_token': $('meta[name="csrf_token"]').attr('content'),
                    'id': id,
                    'isFraud': 1,
                }, function (data) {
                    $('.loadingBtn' + id).button('reset');
                    if (data.success) {
                        swal("更新成功", data.message, "success");
                        location.reload();
                    } else {
                        swal("更新失败", data.message, "error");
                    }
                });
            });
    }

    function notFraud (id) {
        swal({
                title: "确认要将此卡恢复成白名单下单吗？",
                text: "",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "取消",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确认",
                closeOnConfirm: true
            },
            function () {
                $('.loadingBtn2' + id).button('loading');
                $.post("{{url('/affiliate/fraud-order/blacklist/remove')}}/" + id, {
                    '_token': $('meta[name="csrf_token"]').attr('content'),
                    'id': id,
                    'isFraud': 0,
                }, function (data) {
                    $('.loadingBtn2' + id).button('reset');
                    if (data.success) {
                        swal("更新成功", data.message, "success");
                        location.reload();
                    } else {
                        swal("更新失败", data.message, "error");
                    }
                });
            });
    }

    function addNewNote (id) {
        var newCommend = document.getElementById('new-commend-' + id).value;
        FullscreenSpinner.create();
        var authUser = '{{Auth::user()->id}}';
        if (authUser == 10150) {
            sales_no = 628;
        } else {
                    @if(isset($_COOKIE['a']))
            var salesA = '{{$_COOKIE['a']}}';
            sales_no = salesA;
            @else
                sales_no = '{{Auth::user()->id}}';
            @endif
        }

        var newCommend = newCommend + '  (' + sales_no + ')';

        $.post("{{url('/fraud-order/comment')}}", {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'id': id,
            'commend': newCommend,
        }, function (data) {
            FullscreenSpinner.destroy();
            if (data.success) {
                $('#old-commend-' + id).text(newCommend + $('#old-commend-' + id).text());
                swal("更新成功", data.message, "success");
                location.reload();
                document.getElementById('commend' + id).readOnly = true;
                $("#roEditBtn" + id).toggleClass("hidden");
                $("#roSaveBtn" + id).toggleClass("hidden");
            } else {
                swal("更新失败", data.message, "error");
            }
        });
    }

    function SendSms () {
        swal({
                title: "请输入需要额外发送短信确认的订单号",
                text: "短信发送要收费，请不要重复发送，仅发送给需要的客户",
                type: "input",
                showCancelButton: true,
                cancelButtonText: "取消",
                confirmButtonText: "确定",
                closeOnConfirm: false,
                animation: "slide-from-top",
            },
            function (inputValue) {
                if (inputValue === false) {
                    return false;
                }
                if (inputValue.trim() === "" || isNaN(inputValue)) {
                    swal.showInputError("请填写订单号");
                    return false;
                }
                FullscreenSpinner.create();
                var id = inputValue.replace(' ', '');
                $.post("{{url('/affiliate/sms-code/resend')}}/" + id, {
                    '_token': $('meta[name="csrf_token"]').attr('content'),
                    'id': id,
                }, function (data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal("短信发送成功", data.message, "success");
                    } else {
                        swal("短信发送失败", data.message, "error");
                    }
                });
            });
    }
</script>