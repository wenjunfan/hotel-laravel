<script type="text/javascript">
    jQuery(function ($) {
        $('#orderStatus').val("{{$select_value}}")
        $('#source').val("{{$source}}")
        {{--TODO: 日历的UX优化，比如to的日期要在from日期的第二天--}}
        $('#from').datepicker({
            todayBtn: 'linked',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('#to').datepicker({
            todayBtn: 'linked',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('#cfrom').datepicker({
            todayBtn: 'linked',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('#cto').datepicker({
            todayBtn: 'linked',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('#affiliate-ratio').html($('#partner-affiliate-ratio').html());
    });

    {{--这里是同时使用datatable and footable的范例。--}}
    $(document).ready(function () {
        $('.dataTables-admin-orders').DataTable({
            'order': [[0, 'desc']],
            // "paging":   false, // 避免与footable冲突
            // "ordering": false, // 避免与footable冲突
            // "info": false, // 避免与footable冲突
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {
                    extend: 'excel',
                    title: 'Order Records'
                },
                {
                    extend: 'pdf',
                    title: 'Order Records'
                },
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg')
                        $(win.document.body).css('font-size', '10px')
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit')
                    }
                }
            ],
            "fnDrawCallback": function () {
                // $('.dataTables-admin-orders').footable();
            }
        });

        // $('.dataTables-admin-orders').footable({
        //     "paging": {
        //         "enabled": true,
        //         "limit": 3
        //     },
        // });

    });

    {{--搜索赋值给url进行get检索--}}
    function search () {
        var source = $('#source').val();
        var from = $('#from').val();
        var to = $('#to').val();
        var cfrom = $('#cfrom').val();
        var cto = $('#cto').val();
        var sn = $('#sales_no').val();
        var orderStatus = $('#orderStatus').val();
        var url = "{{url('/affiliate/orders')}}"
            + '/' + source
            + '?from=' + from
            + '&to=' + to
            + '&cfrom=' + cfrom
            + '&cto=' + cto
            + '&orderStatus=' + orderStatus;
        if (sn !== '') {
            url = +'&sales_no=' + sn
        }
        self.location = url;
    }

    function emailAccountant (amount) {
        $('.commission-btn').html('Requesting...').prop('disabled', true);
        $.post("{{url('/affiliate/orders/cashing')}}/" + amount, {
            '_token': $('input[name="_token"]').val(),
            'amount': amount,
        }, function (data) {
            if (!data.success) {
                if (data.message) {
                    data['message'] = '{{trans('orders.cashing_request_false_amount')}}';
                    swal({
                        title: '',
                        text: data.message,
                        type: 'warning',
                        confirmButtonText: 'OK'
                    }, function () {
                        //function after failed?
                        window.location.href = self.location;
                    });
                } else {
                    data['message'] = '{{trans('orders.cashing_request_false')}}';
                    swal({
                        title: '',
                        text: data.message,
                        type: 'warning',
                        confirmButtonText: 'OK'
                    }, function () {
                        //function after failed?
                        window.location.href = self.location;
                    });
                }
            } else {
                data['message'] = '{{trans('orders.cashing_request_true')}}';
                swal({
                    title: '',
                    text: data.message,
                    type: 'success',
                    confirmButtonText: 'OK'
                }, function () {
                    //function after success?
                    window.location.href = self.location;
                });
            }
        });
    }
</script>