<script type="text/javascript">
    jQuery(function ($) {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
    });

    function changeAvatar(avatar) {
        $.ajax({
            method: "POST",
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
            url: "{{url('/avatar')}}",
            data: {'avatar': avatar},
            success: function (data) {
                if (data.status === "success") {
                    swal({
                        title: "Updated",
                        text: "",
                        type: 'success'
                    }, function () {
                      self.location = "{{ url('/affiliate/welcome') }}";
                    });
                }
                else {
                    swal(data.message);
                }
            },
            error: function (xhr, e) {
                swal(xhr.status + " " + e);
            },
        });
    }

    @if (session('message'))
    swal({
      title: '{{trans('welcome.thanks')}}',
      text: "",
      type: 'success'
    }, function () {
      self.location = "{{ url('/affiliate/profile') }}";
    });
    @endif

</script>