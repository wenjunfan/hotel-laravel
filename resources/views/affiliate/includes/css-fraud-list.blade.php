
<style>
    .danger-a-bg, tr.gradeX.danger-a-bg:hover{
        background-color:mediumpurple;
    }
    .danger-b-bg, tr.gradeX.danger-b-bg:hover{
        background-color:darkgrey;
    }
    .danger-c-bg, tr.gradeX.danger-c-bg:hover{
        background-color:lightskyblue;
    }
    .danger-d-bg, tr.gradeX.danger-d-bg:hover {
        background-color:darkorange;
    }
    .fraud-bg, tr.gradeX.fraud-bg:hover{
        background-color: black;
        color: white;
    }

    body{
        font-size: 16px;
    }
    .modal-dialog-for-more-info{
        width: 960px;
    }
    .f12{
        font-size: 12px;
    }
    .modal-dialog-fraud {
        width: 1000px;
        margin: 30px auto;
    }
    .loader{
        height:50px;
        text-align:center;
    }
    .modal-body-black-list{
        height:100px;
        padding: 15px 15px 0 15px;
    }
    .modal-body-config{
        height: 300px;
        background-color: #fff;
        padding: 15px;
    }
    .modal-body-blk-list-more-info {
        height: 282px;
        background-color: #fff;
        padding: 15px;
    }
    .close{
        color: #484848 !important;
    }
    .smlpic{
        width:20px;
        height:20px;
    }
    .text-yellow{
        color:orange;
    }
    .extendmodalboday{
        height:220px;
    }
    .middleSpace{
        width:30px !important;
    }
    .configtable{
        line-height:55px;
    }
    .modal-content-fraud{
        height:360px;
    }
    .text-amount{
        color: red;
        font-weight: bold;
    }
    .icon-showorder{
        margin-bottom: 6px;
    }
    .dataTables-admin-orders td{
        padding: 10px !important;
    }
    .table-config{
        border-bottom: #ccc 1px solid;
    }

    .table-config td{
        padding: 5px !important;
    }
    .form-group{
        width: 160px;
    }
    .form-group input{
        width: 66px !important;
    }
    .form-inline td{
        text-align: right;
    }
    .form-inline .form-group{
        text-align: left;
    }
    .fraud-loader{
        text-align: center;
    }
    .table-more-info{
        display: none;
    }
    .note-content{
        margin-left: 35px;
        padding-top:135px;
        font-weight:bold;
        font-size:16px;
    }
    .img-responsive{
        margin: 0 auto;
    }
    @media only screen
    and (max-device-width: 680px) {
        body{
            font-size: 12px;
        }
        .modal-dialog{
            width: auto;
        }
        .modal-dialog-fraud {
            width: auto;
            margin: auto auto;
        }
        .ibox-content{
            padding: 0;
        }
        .table-fraud{
            padding: 0;
        }
        .note-content{
            margin-left: 5px;
            padding-top:5px;
            font-weight:bold;
            font-size:16px;
        }
        .table-more-info td{
            padding: 5px 5px 5px 0 !important;
        }
        .html5buttons{
            display: none;
        }
        .dataTables_length{
            display: none;
        }

    }

</style>