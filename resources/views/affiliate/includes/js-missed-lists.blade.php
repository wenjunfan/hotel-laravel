<script type="text/javascript">
    jQuery(function ($) {
        $('.dataTables-missedOrderLists').DataTable({
            "order": [[0, "desc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {
                    extend: 'excel',
                    title: '可疑微信收款'
                },
                {
                    extend: 'pdf',
                    title: '可疑微信收款'
                },
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ]
        });
    });

    function formatDate(date) {
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "June", "July",
            "Aug", "Sept", "Oct",
            "Nov", "Dec"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
        var hour = date.getHours();
        var minute = date.getMinutes();
        return day + '-' + monthNames[monthIndex] + '-' + year + ' '+ hour + ':' +minute + ' ';
    }

    function ableAddNewAmount(id) {
        $("#roEditBtn" + id).toggleClass("hidden");
        $("#roSaveBtn" + id).toggleClass("hidden");
        document.getElementById('new_amount_confirmed_' + id).readOnly = false;
    }

    function confirmNewAmount(id) {
        var new_amount_confirmed = document.getElementById('new_amount_confirmed_' + id).value;
        var new_commend = formatDate(new Date())+ '财务确认过到账，现在新的客人已付款'+ new_amount_confirmed + '  ({{Auth::user()->id}})\n' + document.getElementById('commend' + id).value; //财务确认过更改价格了
        FullscreenSpinner.create();
        $.post("{{url('/ccpay/amount/update')}}", {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'id': id,
            'new_amount_confirmed': new_amount_confirmed,
            'new_commend' : new_commend,
        }, function (data) {
            FullscreenSpinner.destroy();
            if (data.success) {
                swal("更新成功", data.message, "success");
                document.getElementById('linked_order_id_' + id).readOnly = true;
                $("#roEditBtn" + id).toggleClass("hidden");
                $("#roSaveBtn" + id).toggleClass("hidden");
            }
            else {
                swal("更新失败", data.message, "error");
            }
        });
    }

    function checkPmyStatus(id, type) {
        var Reorderid = $('#ref_' + id).html();
        var laddaBtn = $('#citcon_' + id).ladda();
        laddaBtn.ladda('start');
        $.get('/ccpay/status/'+ Reorderid, function (response) {
            if (response.data) {
                $('#order_process_' + id).html(response.data);
                if (response.data === 'charge') {
                    $('#order_process_' + id).siblings('.order-continue').removeClass('hide');
                }
            }
            laddaBtn.ladda('stop');
        });
    }

    function addNewNote(id) {
        var newCommend = document.getElementById('new-commend-' + id).value;
        FullscreenSpinner.create();
        var authUser = '{{Auth::user()->id}}';
        if (authUser == 10150) {
            sales_no = 628;
        } else {
                @if(isset($_COOKIE['a']))
            var salesA = '{{$_COOKIE['a']}}';
            sales_no = salesA;
            @else
              sales_no = '{{Auth::user()->id}}';
            @endif
        }

        var newCommend = formatDate(new Date()) + newCommend + '  (' + sales_no + ')';

        $.post("{{url('/missed-order/comment')}}", {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'id': id,
            'commend': newCommend,
        }, function (data) {
            FullscreenSpinner.destroy();
            if (data.success) {
                $('#old-commend-' + id).text(newCommend + $('#old-commend-' + id).text());
                swal("更新成功", data.message, "success");
                //location.reload();
                document.getElementById('commend' + id).readOnly = true;
                $("#roEditBtn" + id).toggleClass("hidden");
                $("#roSaveBtn" + id).toggleClass("hidden");
            } else {
                swal("更新失败", data.message, "error");
            }
        });
    }
</script>