{{--黑白名单管理 fraud order list TODO: 优化 moreInfo弹出样式; 备注MODAL--}}
<?php
$is_admin_n_accounting = (Auth::user()->admin == 2 || Auth::user()->admin == 1);
?>
@extends('layouts.admin')
@section('title', '黑白名单管理')
@section('css')
	<link href="{{ asset('assets/css/admin/plugins.orders.css') }}" rel="stylesheet">
    {{--single page style--}}
    @include('affiliate.includes.css-fraud-list')
@endsection

@section('content')
	<div class="row animated fadeInRight">

		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-noborder" style="margin-top:20px;">
					<h2>黑白名单管理</h2>
				</div>
				<div class="ibox-content" style="border: none;">
                    <div class=" col-lg-8">
                        <table class="table table-config">
                            <tr>
                                <td style="text-align: right">
                                    每笔订单可预订金额上限：
                                </td>
                                <td>
                                    {{ $configs[0]->value }} USD
                                </td>
                                <td style="text-align: right">
                                    预定时间距离免费取消日期：
                                </td>
                                <td>
                                    {{ $configs[5]->value }} 天
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    每笔订单触发fraud检查金额上限：
                                </td>
                                <td>
                                    {{ $configs[6]->value }} USD
                                </td>
                                <td style="text-align: right">
                                    每笔每房晚触发fraud检查金额上限：
                                </td>
                                <td>
                                    {{$configs[7]->value}} USD
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    每笔订单触发fraud检查入住房间数上限：
                                </td>
                                <td>
                                    {{$configs[8]->value}} 间
                                </td>
                                <td style="text-align: right">
                                    每笔每房晚触发fraud检查入住晚数上限：
                                </td>
                                <td>
                                    {{$configs[9]->value}} 晚
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-lg-4">
                        @if(Auth::user()->id == 10672 || Auth::user()->id == 10655 || Auth::user()->admin == 1)
                            <a class="btn btn-danger" data-toggle="modal" data-target="#configModal">更改可控变量</a>
                        @endif
						@if(!$is_aa)
						<br/>
							<button class="btn btn-info" onclick="SendSms()">发送确认短信</button>
						@endif
                    </div>
					<form method="post" action="{{URL::to('/affiliate/config/update')}}">
						<div class="modal fade" id="configModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document" style="padding-top:70px">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span></button>
										<h4>修改可控变量：</h4>
									</div>
									<div class="modal-body-config extendmodalboday">
										<table class="col-lg-12 configtable">
                                            <tr class="form-inline">
												<td>
													<h4>每笔订单可预订金额上限(USD)：</h4>
												</td>
												<td class="form-group">
													<input class="form-control" id="1" name="topVal" value="{{ $configs[0]->value }}">
													<button onclick="changeConfig(1)" class="btn btn-info">更新</button>
												</td>
												<td class="middleSpace"></td>
												<td>
													<h4>预定时间距离免费取消日期(天)：</h4>
												</td>
                                                <td class="form-group">
													<input class="form-control" id="10" name="daysbf" value="{{ $configs[5]->value }}">
													<button onclick="changeConfig(10)" class="btn btn-info">更新
													</button>
												</td>
											</tr>
                                            <tr class="form-inline">
												<td>
													<h4>每笔订单触发fraud检查金额上限(USD)：</h4>
												</td>
                                                <td class="form-group">
													<input class="form-control" id="11" name="totalAmt" value="{{ $configs[6]->value }}">
													<button onclick="changeConfig(11)" class="btn btn-info">更新
													</button>
												</td>
												<td class="middleSpace"></td>
												<td>
													<h4>每笔每房晚触发fraud检查金额上限(USD)：</h4>
												</td>
                                                <td class="form-group">
													<input class="form-control" id="12" name="perAmt" value="{{$configs[7]->value}}">
													<button onclick="changeConfig(12)" class="btn btn-info">更新
													</button>
												</td>
											</tr>
											<tr class="form-inline">
												<td>
													<h4>每笔订单触发fraud检查入住房间数上限(间)：</h4>
												</td>
                                                <td class="form-group">
													<input class="form-control" id="13" name="roomlmt" value="{{$configs[8]->value}}">
													<button onclick="changeConfig(13)" class="btn btn-info">更新
													</button>
												</td>
												<td class="middleSpace"></td>
												<td>
													<h4>每笔每房晚触发fraud检查入住晚数上限(晚)：</h4>
												</td>
                                                <td class="form-group">
													<input class="form-control" id="14" name="nightlmt" value="{{$configs[9]->value}}">
													<button onclick="changeConfig(14)" class="btn btn-info">更新
													</button>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="note-content">
			<p class="text-danger">如果发现背景标注颜色是需要高度重视的危险信号 @if(!$is_aa)，且<a style="text-decoration: underline !important" target="_blank" href="//www.beenverified.com/f/login" class="tooltip-test" title="www.beenverified.com/f/login">验证用户信息的网站</a>无法找到信息： 网站用户名为gangyu1982@gmail.com    密码为usitrip2019$F @endif</p>
			1:<span class="danger-a-bg">非美国加拿大信用卡;</span> 2:<span class="danger-b-bg">手机号码为虚拟号;</span> 3:<span class="danger-c-bg"> 不是美国和加拿大手机; </span> 4:<span class="danger-d-bg">用户语言为中文然而入住人或持卡人是外国人</span>
			<p>@if(!$is_aa)以上类型留给余总检查是否移除危险还是确定黑名单并退款 @endif</p>
		</div>
		<div class="col-lg-12 col-md-12 table-fraud">
			{{--<div class="ibox float-e-margins">--}}
			<div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-bordered table-hover dataTables-orderLists dataTables-admin-orders" data-limit-navigation="5">
							<thead>
								<tr>
									<th>ID</th>
                                    {{--2--}}
									<th>操作 <br>(下单/拒单)</th>
									{{--2.5--}}
									<th>日期</th>
                                    {{--3--}}
									<th><span class="text-success">订购</span>/<span class="text-info">持卡人</span><br/>不重名入住人</th>
                                    {{--4--}}
									<th><span class="text-success">订购手机号</span><br><span class="text-info">确认手机号</span></th>
                                    {{--5--}}
									<th>金额 <br>卡号 (有效期)</th>
                                    {{--6--}}
									<th>网站来源</th>
                                    {{--7--}}
									<th>白名单</th>
                                    {{--8--}}
									<th>黑名单</th>
                                    {{--9--}}
									<th>上传资料(如客戶沒有上传销售需代传下单)</th>
                                    {{--10--}}
									<th>备注</th>
								</tr>
							</thead>
							<tbody>
								@foreach($orderLists as $order)
                                    <?php
                                    $bookerDetail = json_decode($order->booker, true);
                                    $cardDetail = json_decode($order->cc_info, true);
                                    $paxes = isset(json_decode($order->rooms, true)[0]['paxes']) ? json_decode($order->rooms, true)[0]['paxes'] : '';
                                    ?>
									<tr class="gradeX @if($order->danger_sign == 1) danger-a-bg @elseif($order->danger_sign == 2) danger-b-bg @elseif($order->danger_sign == 3) danger-c-bg @elseif($order->danger_sign == 4) danger-d-bg @elseif($order->isFraud == 1) fraud-bg @endif">
                                        {{--1 id--}}
										<td style="text-align: center;">
											<a onclick='showMoreInfoModal("{{$order->id}}")' data-toggle="modal" data-target="#fraudModal-{{$order->id}}" data-whatever="@if(isset($order->id)){{$order->id}}@endif">{{$order->id}}
                                                <br><i class="fa fa-2x fa-info-circle"></i></a>
										</td>
                                        {{--2 操作--}}
										<td>
											@if($order->isDone == 0 && $order->isLocked == 0 && $order->isDeclined == 0 && $order->isFraud == 0)
                                                <span class="text-danger">new!</span>
                                                <br>
                                                <a class="btn btn-info" target="_blank" href="{{$order->reorder_url}}{{$CookieValue !== '' ? '?a='.$CookieValue : ''}}">下/拒单链接</a>
											@elseif($order->isDone == 0 && $order->isLocked == 1 && $order->isDeclined == 0)
												<span class="text-yellow">{{$order->sales_no}}处理中..</span>
											@else
												{{$order->sales_no}}已处理
											@endif
										</td>
										{{--2.5 生成日期--}}
										<td>
											{{$order->created_at}}
										</td>
                                        {{--3 订购人 持卡人--}}
										<td>
											<span class="text-success" id="booker-name-{{$order->id}}">{{$bookerDetail['firstname'] .' ' .$bookerDetail['lastname']}}</span><br/>
											<span id="card-holder-{{$order->id}}" class="text-info">{{$cardDetail['firstName'] .' ' .$cardDetail['lastName']}}</span><br/>
											<span id="guest-name-{{$order->id}}">
												@if(isset($paxes[0]['name']))
													{{$paxes[0]['name'].' '. $paxes[0]['surname']}}
                                                    <?php
                                                    $main_pax = $paxes[0];
                                                    foreach($paxes as $pax){
                                                        $each = ', '.$pax['name']. ' '. $pax['surname'];
                                                        if($each == (', '. $main_pax['name']. ' '. $main_pax['surname']))
                                                        {
                                                            $each = '';
                                                        }
                                                        echo "$each";
                                                    }
                                                    ?>
												@endif
											</span>
										</td>
                                        {{--4 手机号--}}
										<td style="text-align: right; padding-right:16px;">
                                            {{--订购手机号--}}
											<p class="text-success">
											@if(isset($bookerDetail['countryCodePhone']))
												@if(isset($bookerDetail['validPhoneNumber']))
													{{--改版后，记录所有的电话号码--}}
													{{$bookerDetail['phone_country'] .' ' .$bookerDetail['validPhoneNumber']}}
												@else
													{{$bookerDetail['countryCodePhone'] .' ' .$bookerDetail['phone']}}
												@endif
											@else
												{{$bookerDetail['phone']}}
											@endif
											</p>
                                            {{--短信确认手机号--}}
											<p class="text-info">
												@if(isset($cardDetail['countryCodePhone']))
													{{$cardDetail['countryCodePhone'] .' ' .$cardDetail['phone']}} 
												@endif
											</p>
                                            {{--phone type--}}
                                            <p id="phone_type_{{$order->id}}">@if(isset($order->phone_num_type)){{$order->phone_num_type}} @endif</p>
										</td>
                                        {{--5 卡号 有效期 订单金额 --}}
										<td>
                                            <strong @if($order->amount >= $configs[6]->value)class="text-amount" @endif>
                                                {{$order->amount. ' ' . $order->currency}}
                                            </strong><br>
                                            {{$cardDetail['number']}} ({{$cardDetail['month']. '/' .$cardDetail['year']}})
										</td>
                                        {{--6.网站来源--}}
                                        <td>
                                            @if(!empty( $order->language) )
                                                @if( $order->language == 0 )
                                                    中文
                                                @elseif($order->language == 2)
                                                    繁体
                                                @endif
                                                <br>
                                            @endif
                                            @if(!empty($order->http_host) )
                                                <span @if(strpos($order->http_host, 'usitour')!== false) class="text-info" @endif>
                                                    {{ $order->http_host }}
                                                </span>
                                            @else
                                                N/A
                                            @endif

                                        </td>
                                        {{--7. 白名单--}}
										<td style="text-align: center">
                                            @if($order->isDone == 1)
                                                <p class="icon-showorder">
                                                <a onclick="showOrderId({{$order->id}})">
                                                    <img class='smlpic' src='/img/general/icon/valid.png' class="tooltip-test" title="点击查看订单号">
                                                </a>
                                                <span id="seeorderId_{{$order->id}}"></span>
                                                </p>
                                            @endif
										</td>
                                        {{--8 黑名单--}}
										<td style="text-align: center">
											@if($order->isFraud != 1)
												<button id="isFraud" data-loading-text="更新中<i class='fa fa-spinner fa-spin'></i>" class="btn btn-sm btn-danger loadingBtn{{$order->id}}" onclick="isFraud({{$order->id}})">
													确认黑名单并退款
												</button>
												@if($order->danger_sign != 0)
													{{--现在只要是有danger_sign就是黑名单的标志，但是必须confirmed isFraud才是真正的黑名单并且退款--}}
													<button id="notFraud" data-loading-text="更新中<i class='fa fa-spinner fa-spin'></i>" class="btn btn-sm btn-info loadingBtn2{{$order->id}}" onclick="notFraud({{$order->id}})">
														移除危险名单
													</button>
												@endif
											@else
												<img class='smlpic' src='/img/general/icon/notvalid.png'>黑名单
											@endif
                                            @if($order->isDeclined == 1 && $order->isFraud != 1)
                                                <P class="text-danger">
                                                <img class='smlpic' src='/img/general/icon/notvalid.png'>拒单
                                                </P>
                                            @endif
										</td>
                                        {{--9 上传资料--}}
										<td>
											@if(($order->isDone == 0 && $order->isLocked == 0 && $order->isDeclined == 0 && $order->isFraud == 0) || ((Auth::user()->id == 10672 || Auth::user()->id == 10655 || Auth::user()->admin == 2)))
												@if($order->file_url != '')
													@php
														$fileUrl = $order->file_url;
														$fileArray = explode(',', $fileUrl);
													@endphp
													@if(count($fileArray) > 1)
														@for($x= 0; $x< count($fileArray); $x++)
															<a class="label btn" href="{{$fileArray[$x]}}" target="_blank">查看图片{{$x+1}}</a>
														@endfor
													@else
														<a class="label btn" href="{{$order->file_url}}" target="_blank">查看图片</a>
													@endif
													<br/>
												@endif
											@endif
											@if(($order->isDone == 0 && $order->isLocked == 0 && $order->isDeclined == 0 && $order->isFraud == 0))
												<a data-loading-text="邮件发送中 <i class='fa fa-spinner fa-spin'></i>" class="loadingA{{$order->id}} text-danger" onclick="sendEmailAgain({{$order->id}})">发送客户上传证件链接</a>
											@endif
												<br/><a href="/file-upload/{{$order->Reorderid}}" target="_blank">帮客户上传图片</a>
										</td>
                                        {{--10 备注--}}
										<td>
											<a href="#" style="text-decoration:none; color:black;" class="tooltip-test" title="{{$order->commend}}">
											<textarea readonly id="commend{{$order->id}}" rows="3" cols="30" maxlength="1000" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($order->commend)){{$order->commend}}@endif</textarea></a>
											<button name="roEdit" class="label btn" type="button" id="roEditBtn{{$order->id}}" data-toggle="modal" data-target="#myModal-{{$order->id}}" data-whatever="@if(isset($order->id)){{$order->id}}@endif">
											<i class="fa fa-edit"></i>
											</button>
											<form action="{{URL::to('/affiliate/fraud-list')}}">
                                                <!--Modal start-->
												<div class="modal fade" id="myModal-{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
													<div class="modal-dialog" role="document" style="padding-top:70px">
														<div class="modal-content">
															<div class="modal-header">
																<span class="pull-left" style="color:#484848;">备注({{$order->id}})：</span>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body-black-list">
																<textarea autofocus id="new-commend-{{$order->id}}" name="new-commend-{{$order->id}}" rows="4" cols="80" maxlength="1000" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;"></textarea>
																<button name="roSave" id="roSaveBtn{{$order->id}}" class="label btn" style="margin-top:2px;" onclick="addNewNote({{$order->id}})">
																	保存更新
																</button>
															</div>
															<div class="modal-footer" style="height:300px;background-color:#fff;text-align:left">
																<textarea readonly id="old-commend-{{$order->id}}" name="old-commend-{{$order->id}}" rows="10" cols="91" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto;line-height: 250%; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($order->commend)){{$order->commend}}@endif</textarea>
															</div>
														</div>
													</div>
												</div>
                                                <!--Modal end-->
											</form>
										</td>
									</tr>
								@endforeach

							</tbody>

                            <tfoot>
                            <tr>
                                <td colspan="@if($is_admin_n_accounting) 8 @else 7 @endif">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    {{--more info modal - new 20180614 May--}}
    <div class="modal fade" id="fraudMoreInfoModal" tabindex="-1" role="dialog" aria-labelledby="fraudMoreInfoModalLabel">
        <div class="modal-dialog modal-dialog-for-more-info" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="fraudMoreInfoModalLabel">订单号</h3>
                </div>
                <div class="modal-body">
                    {{--loading--}}
                    <div class="fraud-loader"><img src="/img/general/fraud/horizontal_loader.gif" class="img-responsive"></div>
                    {{--ajax info--}}
                    <table class="table table-more-info">
						<tr>
							<td class="text-center font-bold" colspan="2" id="td-names"></td>
						</tr>
                        <tr>
                            <td class="text-right font-bold" style="width: 40%;">酒店: </td>
                            <td id="td-hotel-name"></td>
                        </tr>
						<tr>
							<td></td>
							<td id="td-address"></td>
						</tr>
                        <tr>
                            <td class="text-right font-bold">入住/退房: </td>
                            <td><span id="td-check-in"></span>&nbsp;&nbsp;&nbsp;(<span id="td-data-1"></span>)</td>
                        </tr>
						<tr>
							<td class="text-right font-bold">当地下单时间: </td>
							<td><span id="td-order-created"></span></td>
						</tr>
						<tr>
							<td class="text-right font-bold">下单距入住天: </td>
							<td id="td-days"></td>
						</tr>
                        <tr>
                            <td class="text-right font-bold">IP信息: </td>
                            <td id="td-data-2"></td>
                        </tr>
						<tr>
							<td class="text-right font-bold">电话信息</td>
							<td id="td-phone"></td>
						</tr>
						<tr>
							<td class="text-right font-bold">预订人邮箱</td>
							<td id="td-email"></td>
						</tr>
                        <tr>
                            <td class="text-right font-bold">信用卡信息 </td>
                            <td id="td-data-3"></td>
                        </tr>
                        <tr>
                            <td class="text-right font-bold">语言 </td>
                            <td id="td-user-lang"></td>
                        </tr>
                        <tr>
                            <td class="text-right font-bold">卡Auth失败次数 </td>
                            <td id="td-fail-count"></td>
                        </tr>
                        <tr>
                            <td class="text-right font-bold">是否新用户 </td>
                            <td id="td-new-user"></td>
                        </tr>
                        <tr>
                            <td id="td-danger-level" colspan="2"></td>
                        </tr>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--/more info modal--}}
@endsection
@section('scripts')
	<script src="{{ asset('assets/js/admin/plugins.orders.js') }}"></script>
    {{--single page script--}}
    @include('affiliate.includes.js-fraud-list')

@endsection
