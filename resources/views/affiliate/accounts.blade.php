@extends('layouts.admin')

@section('title', 'affiliate-accounts')

@section('css')
	<link href="{{ asset('assets/css/admin/plugins.orders.css') }}" rel="stylesheet">
	<style>
		textarea{
			border-style: none;
			border-color: transparent;
			background-color:transparent;
			resize: none;
			box-shadow: none;
			overflow: auto;
			outline: none;
			-webkit-box-shadow: none;
			-moz-box-shadow: none;
			width: 100%;
		}
		.smlpic {
			width: 20px;
		}
	</style>
@endsection

@section('content')
	<div class="row animated fadeInRight" id="affiliateAccount" v-cloak>
		<div class="col-lg-4 col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-noborder" style="margin-top:20px;">
					<strong>账户查询</strong>
				</div>
				<div>
					<div class="ibox-content">
						<div class="form-horizontal">
							{!! csrf_field() !!}
							<div class="form-group">
								<label class="col-sm-3 control-label" for="from">创建日期</label>
								<div class="col-sm-5">
									<div class="input-group date">
										<input id="from" placeholder="年-月-日" class="form-control" name="from" v-model="from" />
										<span class="input-group-addon" style="padding-right:0px;"><i class="fa fa-long-arrow-alt-right"></i></span>
									</div>
								</div>
								<div class="col-sm-4 " style="padding-left:0px;">
									<div class="input-group date btn-block" class="padding-left:0px; padding-right:0px; margin-left;-15px;">
										<input id="to" placeholder="年-月-日" class="form-control" name="to" v-model="to" />
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">用户名</label>
								<div class="col-sm-9">
									<div class="input-group date btn-block">
										<input id="keywords" placeholder="用户名" class="form-control" name="keywords" v-model="keywords" />
									</div>
								</div>
							</div>

							<div class="form-group" style="margin-bottom:-20px;">
								<div class="col-sm-6">
									<button class="btn btn-primary btn-block search-btn" type="button" @click="search()">
										<i class="fa fa-search"></i> 搜索
									</button>
								</div>
								<div class="col-sm-6">
									<a class="btn btn-warning btn-block" type="reset" href="/affiliate/accounts">重置</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12 col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<p style="color: #f1791e"> 权限：财务、超级和市场人员可改账户分销佣金，激活账户，其余更改请去117book的账户管理操作</P>
				</div>
				<div>
					<div class="ibox-content">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover dataTables-accounts">
								<thead>
								<tr>
									<th>ID</th>
									<th>用户名</th>
									<th>邮箱/电话/支票地址</th>
									<th>联系人姓名</th>
									<th>分销订单金额(总/未结)</th>
									<th>创建日期(申请/开通)</th>
									<th>分销比例</th>
									<th>激活状态</th>
									<th style="width: 100px;">用户进程备注</th>
								</tr>
								</thead>
								<tbody>
									<tr class="gradeX" v-for="(user, index) in users" :key="index">
										<td>
											@{{ user.partner_id }}
											<p style="color:#f1791e" v-if="user.partner">
												<span v-if="user.partner.host_name === 'Usitour'">Usitour</span>
												<span v-else>走四方</span>
											</p>
											<p style="color:#23c6c8" v-if="user.partner">
												<span v-if="user.partner.language === 1">En</span>
                                                <span v-else>中</span>
											</p>
										</td>
										<td>
											@{{ user.distributor_name }}
										</td>
										<td>
											<span v-if="user.partner &&  user.partner.email">@{{ user.partner.email }}</span>
											<br/>
											<span v-if="user.partner &&  user.partner.mobile_phone">@{{ user.partner.mobile_phone }}</span>
											<br/>
											<span v-if="user.partner &&  user.partner.company_address">@{{ user.partner.company_address }}</span>
										</td>
										<td>
											<span v-if="user.partner &&  user.partner.contact_person">@{{ user.partner.contact_person }}</span>
										</td>
										<td>
											@{{ user.total_contributed}}/@{{ user.total_contributed - user.total_cashed}}
										</td>
										<td>
											<span v-if="user.partner &&  user.partner.created_at">@{{ user.partner.created_at }}</span>/
											<span v-if="user.created_at">@{{ user.created_at}}</span>
										<td>
											<p v-if="user.partner" :id="user.partner_id +'_rate'">@{{ user.partner.affiliateRate }}</p>
											<button @click="changeAffiliateCommission(user.partner_id, user.partner.affiliateRate)" class="btn btn-info btn-xs" type="button">
												更改分销佣金
											</button>
										</td>
										<td>
											<button v-if="user.partner && user.partner.admin && user.partner.admin === 4" @click="active(user.partner_id,4)" class="btn btn-success btn-xs" type="button">解除分销账户</button>
											<button v-if="user.partner && user.partner.admin && user.partner.admin === 5" @click="active(user.partner_id,5)" class="btn btn-danger btn-xs" type="button">激活分销账户</button>
											<br/>
											<div v-if="user.partner && user.partner.admin && user.partner.admin !== 0 && user.partner.admin !== 5" >
												<img class="smlpic" src="/img/general/icon/valid.png" :title="user.distributor_code">
												<span style="color:blue" v-if="user.partner.admin ===4">分销</span>
												<span style="color:red" v-if="user.partner.admin ===1">超级</span>
												<span style="color:lawngreen" v-if="user.partner.admin ===2">财务</span>
												<span style="color:deepskyblue" v-if="user.partner.admin ===3">OP</span>
											</div>
										</td>
										<td>
                                            <div style="height:60px;text-align:left;  overflow-y: auto"  ><span :id="'remark-' + user.partner_id" :name="'remark' + user.partner_id" v-if="user.partner" v-html="user.partner.remark"> </span></div>

											<i v-if="isAdmin === 1 || isAdmin === 3" class="fa fa-2x fa-edit pull-left text-success" @click="showAddCommentModal(user, index)" > </i>
										</td>
									</tr>
								</tbody>
							</table>
							{{-- start add comment Modal--}}
							<div class="modal fade" id="addCommentModal" tabindex="-1" role="dialog" aria-labelledby="AddCommentModalTitle" data-backdrop="static">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="addCommentModalTitle">Modal title</h4>
										</div>
										<div class="modal-body">
											<div id="old_remark"></div>
											<hr>
											<div id="add-area"><textarea name="" id="" cols="90" rows="6" v-model="addComment" style="resize: none;"></textarea></div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>  <button class="btn btn-primary" @click="addNewNote">提交</button>
										</div>
									</div>
								</div>
							</div>
							{{--end add comment Modal--}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')
	<script>
	  {{--get params from url vars and pass to vue data--}}
      var users = {!! $users !!};
      var userId = "{!! \Auth::id() !!}";
      var isAdmin = {!! \Auth::user()->admin !!};
      var urlParams = window.getUrlVars();
      var from = urlParams.from === undefined ? "{!! $from !!}" : decodeURI(urlParams.from);
      var to = urlParams.to === undefined ? "{!! $to !!}" : decodeURI(urlParams.to);
      var keywords = urlParams.keywords === undefined ? "" : decodeURI(urlParams.keywords);
	</script>
	<script src="{{ asset('assets/js/admin/plugins.orders.js') }}"></script>
	<script src="{{ asset('assets/js/affiliate/plugins.accounts.js') }}"></script>
@endsection
