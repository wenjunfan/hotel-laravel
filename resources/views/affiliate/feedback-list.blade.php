@extends('layouts.admin')

@section('title', config('app.name') . ' Feedback List')

@section('css')
    <style>
        td,th {
            padding: 8px;
        }
        .notes ul {
            list-style-type: circle;
            margin-bottom: 20px;
            padding-left: 15px;
        }
    </style>
@endsection

@section('content')
    <div class="notes">
        <ul>
            <li>feedback: 用户在邮件中点击的评分</li>
            <li>priceSelection: 用户在feedback页面选择的评分</li>
            <li>suggestion: 多选项</li>
            <li>otherNotes: 用户的个人评价</li>
        </ul>
    </div>

    <table id="feedbackList" border="1" width="100%">
        <thead>
        <tr>
            <th>用户邮箱</th>
            <th>反馈内容</th>
            <th>反馈时间</th>
        </tr>
        </thead>

        <tbody>
        @if(count($feedback) > 0)
            @foreach($feedback as $item)
                <tr>
                    <td>{{ $item->user_email }}</td>
                    <td>
                        @php
                            $details = json_decode($item->details, true);
                        @endphp

                        @if(count($details) > 0)
                            @foreach($details as $key => $detail)
                                <p>{{ $key }}: {{ $detail }}</p>
                            @endforeach
                        @endif
                    </td>
                    <td>{{ $item->created_at }}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#feedbackList').DataTable();
        });
    </script>
@endsection