<?php
$is_admin_n_accounting = (Auth::user()->admin == 2 || Auth::user()->admin == 1);
$marketing = in_array(Auth::user()->email, config('constants.marketing'), true);
?>

@extends('layouts.admin')
@if	($language == 0)
    @section('title', '人人赚分销联盟-分销商订单管理')
@elseif	($language == 2)
    @section('title', '人人賺分銷聯盟-分銷商訂單管理')
@else
    @section('title', 'Affiliate Partnership Order Management')
@endif

@section('css')
    <link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/footable/footable.core.css') }}" rel="stylesheet">
    {{--single page style--}}
    <style>
        .shorter {
            width: 35px !important;
        }

        @if(Auth::user()->admin==4)
		.salesShoworNot {
            display: none;
        }
        @endif

        .wrapper-content{
            padding-bottom: 10px;
        }
        @media (max-width: 667px) {
            .col-sm-7 {
                margin-bottom: 57px;
            }
        }
        .box-info{
            display:none;
        }
        .commission-btn:hover{
            color:#fff !important;
            background-color: #e76c0e;
            border-color: #e76c0e;
        }
        .redirect_pc{
            color: red;
            font-size: 18px;
            border: 1px solid red;
            padding: 5vw;
            margin-bottom: 43vh;
            margin-top: 57vw;
        }
    </style>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        {{--filter:2355891206@qq.com之前是dunhill的账户，后用作市场部doris的账户，因为这个合作商解除合作了--}}
        <div class="col-lg-12" style="margin-bottom:40px;">
            <div class="ibox float-e-margins">
                @if($isMobile)
                    <div class="redirect_pc">
                        {{trans('orders.redirect_pc')}}
                    </div>
                @else
                <div class="ibox-noborder" style="margin-top:20px;">
                    <strong>{{trans('orders.title')}}  {{ Auth::user()->company_name!= '' ? '('. Auth::user()->company_name .')': '' }}</strong>
                    @foreach($distributors as $distributor)
                        @if(Auth::user()->id == $distributor->partner_id && !$marketing)
                            <br/>{{trans('orders.ratio')}} :
                            <span id="partner-affiliate-ratio">{{$distributor->partner->affiliateRate }}</span>
                            <style>
                                .box-info {
                                    border: 1px solid #e1e1e1;
                                    padding: 20px 12px 50px 12px;
                                    box-sizing: border-box;
                                    color:#484848;
                                    display:block !important;
                                }
                                .box-info h2{
                                    font-size:16px;
                                    padding-bottom: 10px;
                                }
                                .box-info h3{
                                    font-size:12px;
                                    font-weight: normal;
                                }
                                .line {
                                    height: 1px;
                                    background: #e1e1e1;
                                    margin: 15px 0;
                                }
                                .break-down span:not(:nth-child(2)){
                                    font-size:12px;
                                    line-height: 2;
                                }
                                .break-down span:last-child{
                                    float:right;
                                }
                                .commission-btn{
                                    background-color: #f5762e;
                                    float: right;
                                    border: none;
                                    color: #fff;
                                    font-size: 12px;
                                    margin-top: 10px;
                                }
                                .orange{
                                    color:#f5762e;
                                }
                                .break-down {
                                    border-bottom: 1px dashed #e1e1e1;
                                }
                            </style>
                        @endif
                    @endforeach
                </div>
                <div>
                    <div class="ibox-content">
                        <div class="col-sm-7">
                            {{--filter form start--}}
                            <form class="form-horizontal" role="form" id="search-form" name="search-form">
                                {!! csrf_field() !!}
                                <div class="form-group">

                                    <div class="col-md-6">
                                    <label class="control-label" for="id">{{trans('orders.affiliate')}}</label>
                                    <div>
                                        <select id="source" name="source" class="form-control" value="{{$source}}">
                                            @if($is_admin_n_accounting  || $marketing)
                                                <option value="All117BookAffiliatePartner">全部分销商:</option>
                                            @endif
                                            @foreach($distributors as $distributor)
                                                @if($is_admin_n_accounting  || $marketing)
                                                    <option value="{{$distributor->distributor_code}}">{{$distributor->distributor_name}}</option>
                                                @elseif(Auth::user()->id == $distributor->partner_id && !$marketing)
                                                    <option value="{{$distributor->distributor_code}}">{{$distributor->distributor_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    </div>

                                    <div class="col-md-5">
                                    <label class="pull-left control-label">@lang('orders.BillingStatus')</label>
                                    <div>
                                        <select class="form-control" id="orderStatus" name="orderStatus" value="{{$select_value}}">
                                            <option value="4">{{trans('orders.All')}}</option>
                                            <option value="1">{{$language == 0 ? "已结算" : "Cleared"}}</option>
                                            <option value="2">{{$language == 0 ? "待结算" : "Pending"}}</option>
                                            <option value="3">{{$language == 0 ? "未产生佣金" : "N/A"}}</option>
                                        </select>
                                    </div>
                                    </div>
                                    @if($is_admin_n_accounting || $marketing)
                                        <div class="salesShoworNot col-md-1">
                                            <label class="control-label">销售号</label>
                                            <div>
                                                <input class="form-control" type="text" id="sales_no" name="sales_no" value="{{$sales_no}}"/>
                                            </div>
                                        </div>
                                    @else
                                        <input  type="hidden" id="sales_no" name="sales_no" value=""/>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                    <label class="control-label" for="from">@lang('orders.BookingDate')</label>
                                    <div>
                                        <div class="input-group date">
                                            <input id="from" name="from" type="text" value="{{substr($from, 0, 10)}}" class="form-control"
                                                   placeholder="YYYY-MM-DD"><span class="input-group-addon"><i class="fa fa-long-arrow-alt-right"></i></span>

                                            <input id="to" name="to" type="text" class="form-control" value="{{substr($to, 0, 10)}}"
                                                   placeholder="YYYY-MM-DD">
                                        </div>
                                    </div></div>
                                    <div class="col-md-6">
                                    <label class="control-label" for="cfrom">@lang('orders.CheckInDate')</label>

                                    <div>
                                        <div class="input-group date">
                                            <input id="cfrom" name="cfrom" type="text" value="{{$cfrom}}"
                                                   class="form-control" placeholder="YYYY-MM-DD">
                                            <span class="input-group-addon"><i class="fa fa-long-arrow-alt-right"></i></span>
                                            <input id="cto" name="cto" type="text" class="form-control" value="{{$cto}}"
                                                   placeholder="YYYY-MM-DD">
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-sm-offset-8">
                                    <button class="btn btn-primary btn-block" type="button" onclick="search()">
                                        <i class="fa fa-search"></i>@lang('orders.Search')
                                    </button>
                                </div>
                                <div class="form-group " style="margin-bottom:-20px;">
                                    <!--div class="col-sm-3">
                                        <button class="btn btn-warning btn-block" type="reset">重置</button>
                                    </div-->
                                </div>
                            </form>
                            {{--filter form end--}}
                        </div>
                        <div class="col-sm-5">
                            <div class="box-info">
                                <h2>{{$language == 0 ? "佣金兑现说明：" : "How to cashing your commission:"}}</h2>
                                <h3>@if($is_aa)
                                        每月返还已入住的订单总金额(刨除每笔手续费4%和酒店的税),佣金比例为15%;
                                    @else
                                        @if($language == 0 )所得佣金可以在个人账户累计,只有当佣金累计到$50以后,您可以选择支票或Paypal提取您的佣金. @else The commission you earned can be accumulated. After it’s up to $50, you can choose to withdraw by check or withdraw by Paypal @endif
                                    @endif</h3>
                                <div class="line"></div>
                                <div class="break-down">
                                    <span>{{$language == 0 ? "佣金百分比" : "Commission Ratio"}}</span><span class="dashes"></span><span class="orange" id="affiliate-ratio"> </span>
                                </div>
                                <div class="break-down">
                                    <span>{{$language == 0 ? "可结算销售总额(不含税金)" : "Billable sales total(Excluding tax)"}}</span><span class="dashes"></span><span class="orange">${{$sum_unpaid}}</span>
                                </div>
                                <div class="break-down">
                                    <span>{{$language == 0 ? "可获得佣金(不含税金)" : "total available commission(Excluding tax)"}}</span><span class="dashes"></span><span class="grey">${{$able_cash_out_amount}}   (+ ${{$sum_able_cash-$able_cash_out_amount . trans('orders.cashing_not_accrued')}} ) </span>
                                </div>
                                <button class="btn commission-btn" @if(!$able_cash_out) disabled @else onclick="emailAccountant({{$able_cash_out_amount}})"  @endif >{{$language == 0 ? "佣金兑现" : "Commission withdraw"}}</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        @if(!$isMobile)
        {{--table start--}}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins table-data">
                    <div class="ibox-title">
                        <h5>@lang('orders.CheckOrder'): {{$count}}</h5>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-stripped toggle-arrow-tiny  dataTables-admin-orders" data-limit-navigation="5">
                            <thead>
                            <tr>
                                <th data-toggle="true" data-sorted="true" data-direction="desc">@lang('orders.BookingID')</th>
                                @if($is_admin_n_accounting || $marketing)
                                    <th>追踪码</th>
                                    <th>广告方位/标题</th>
                                @endif
                                <th>@lang('orders.Name')</th>
                                <th>@lang('orders.BookingDate')</th>
                                <th>@lang('orders.NetPrice')</th>
                                @if($is_aa)
                                <th>可结算佣金</th>
                                @endif
                                <th>@lang('orders.BilledOrNot')</th>
                                @if($is_admin_n_accounting)
                                    <th>用户要求/备注</th>
                                @endif
                                <th>@lang('orders.Action')</th>
                                {{--detail content--}}
                                <th data-hide="all">@lang('orders.HotelName')</th>
                                <th data-hide="all">@lang('orders.checkInOut')</th>
                                <th data-hide="all">@lang('orders.CancellationPolicy')</th>
                                {{--detail content end--}}
                            </tr>
                            </thead>
                            <tbody>
                            {{--loop start--}}
                            @foreach($orders as $order)
                                {{--check 取消政策: 如果取消日期在今天之前，则显示不可取消--}}
                                <?php
                                $cancelPolicies = json_decode($order->rooms[0]['cancelPolicies']);
                                if( count($cancelPolicies) > 0 ){
                                    $cancelDate = $cancelPolicies[0]->end;
                                }
                                if(\Carbon\Carbon::now() > $cancelDate){
                                    $cancelDate = 'Non-Refundable';
                                } else {
                                    $cancelDate = 'Hotel Local Time: '. isset($cancelDate) ? substr(str_replace("T", " ", $cancelDate), 0, 19) : '';
                                }
                                ?>
                                <tr>
                                    {{--订单号--}}
                                    <td>
                                        {{$order->id}} &nbsp;
                                        {{--订单状态--}}
                                        @if($order->id == 1153)
                                            <span class="label label-warning">Cancelled With Fees</span>
                                        @else
                                            <span class="label {{$order->status == 'CONFIRMED' ? 'label-info' : ($order->status == 'CANCELLED' ? 'btn' : ($order->status == 'ON REQUEST' ? 'label-success' : 'label-danger'))}}">
                                                @if ($order->status == 'CONFIRMED')
                                                    {{$language == 0 ? "已确认" : "Confirmed"}}
                                                @elseif ($order->status == 'CANCELLED')
                                                    @if($order->payment_status == 'PENDING')
                                                        {{$language == 0 ? "已取消退款中" : "Refund Pending"}}
                                                    @elseif($order->payment_status == 'REFUND')
                                                        @if($order->payment_type == 'PO')
                                                            {{$language == 0 ? "已取消" : "Cancelled"}}
                                                        @else
                                                            {{$language == 0 ? "已取消已退款" : "Refunded"}}
                                                        @endif
                                                    @else
                                                        @if($order->payment_type == 'PO')
                                                            {{$language == 0 ? "已取消" : "Cancelled"}}
                                                        @else
                                                            {{$language == 0 ? "已取消已退款" : "Refunded"}}
                                                        @endif
                                                    @endif
                                                @elseif ($order->status == 'ON REQUEST')
                                                    {{$language == 0 ? "处理中" : "On Request"}}
                                                @else
                                                    {{$language == 0 ? "失败" : "Failed"}}
                                                @endif
                                            </span>
                                        @endif
                                        @if ($order->status == 'ON REQUEST')
                                        @elseif(($order->status == 'CANCELLED') && ($order->payment_status == 'PENDING'))
                                        @endif
                                    </td>
                                    {{--追踪码--}}
                                    @if($is_admin_n_accounting || $marketing)
                                        <td>
                                            @if(isset($order->source) && $order->source != '')
                                                <label class="label label-warning" class="tooltip-test" title="{{$order->source}}">
                                                    @foreach($distributors as $distributor)
                                                        @if($order->source == $distributor->distributor_code)
                                                        {{$distributor->distributor_name}}
                                                        @endif
                                                    @endforeach

                                                     @if(in_array($order->source, config('constants.adSources'), true))
                                                            {{$order->source}}
                                                    @endif
                                                </label>
                                            @endif
                                            @if(isset($order->sales_no) && $order->sales_no != 0)
                                                <br><b style='color:hotpink;'>{{$language == 0 ? "销售工号" : "Sales #"}}: {{$order->sales_no}}</b>
                                            @endif
                                        </td>
                                        {{--广告方位--}}
                                        <td>@if(isset($order->source_pos) && $order->source_pos != '') {{$order->source_pos}} @else <span class="text-muted">N/A</span> @endif</td>
                                    @endif
                                    {{--预订人--}}
                                    <td>{{$order->bookerLastName . ' ' . $order->bookerFirstName}}</td>
                                    {{--下单日期--}}
                                    <td>{{$order->created_at}}</td>
                                    <td>
                                        @if(isset($order->orderTem->coupon_code) && $order->orderTem->coupon_code !== '')
                                            {{$order->currencyCode . ' ' .$order->orderTem->amount}}
                                            <br/><span class="text-danger">({{$order->orderTem->coupon_code}})</span>
                                        @else
                                            @if ($order->payment_type == 'ALIPAY')
                                                @if ($order->infact_cost_cn == '' || null == $order->infact_cost_cn)
                                                    {{$order->currencyCode . ' ' . $order->totalPrice}}
                                                @else
                                                    {{'CNY' . ' ' . $order->infact_cost_cn}}
                                                @endif
                                            @else
                                                {{$order->currencyCode . ' ' . $order->totalPrice}}
                                            @endif
                                        @endif
                                    </td>
                                    {{--aa: 佣金金额也要显示出来--}}
                                    @if($is_aa)
                                    <td>
                                        {{round($order->totalPrice * .1215,2)}}
                                    </td>
                                    @endif
                                    {{--是否结算--}}
                                    <td>
                                        @if ($order->isAffiliated == 1 && $order->status == 'CONFIRMED')
                                            <span class="label label-info">{{$language == 0 ? "已结算" : "Cleared"}}</span>
                                        @elseif ($order->isAffiliated ==0 && $order->status == 'CONFIRMED')
                                            <span class="label label-danger">{{$language == 0 ? "待结算" : "Pending"}}</span>
                                        @elseif ($order->status <> 'CONFIRMED')
                                            <span class="label label-success">{{$language == 0 ? "未产生佣金" : "N/A"}}</span>
                                        @endif
                                    </td>

                                    {{--用户要求/备注--}}
                                    @if($is_admin_n_accounting)
                                        <td>
                                            @if(isset($order->remark) && $order->remark !='')<p style="color:red;">备注：</p>{{$order->remark}}@endif
                                            @if(isset($order->special_request) && $order->special_request !=''&& $order->special_request != null)
                                                <p style="color:red;">特别要求：</p>
                                                <p style="text-decoration:underline">{{$order->special_request}}</p>
                                            @endif</td>
                                    @endif
                                    {{--操作--}}
                                    <td>
                                        <a target="_blank" href="{{url('/order/' . $order->orderReference)}}"class="btn btn-sm btn-default">{{trans('orders.OrderInfo')}}</a>
                                        @if ($order->status == 'CONFIRMED' &&  $is_admin_n_accounting)
                                            <a target="_blank" href="{{url('voucher/' . $order->code)}}" class="btn btn-sm btn-default">酒店凭证</a>
                                        @endif
                                    </td>
                                    {{--酒店名称--}}
                                    <td>{{$order->hotelName }} @if($language === 0){{$order->hotelName_zh}}@endif</td>
                                    {{--入住/退房--}}
                                    <td>{{$order->checkinDate}}
                                        / {{$order->checkoutDate}}</td>
                                    {{--取消政策--}}
                                    <td>
                                        {{$cancelDate}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="@if($is_admin_n_accounting) 9 @else 6 @endif">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{--table end--}}
        @endif
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/footable/footable.all.min.js') }}"></script>
    {{--single page script--}}
    @include('affiliate.includes.js-orders')
@endsection
