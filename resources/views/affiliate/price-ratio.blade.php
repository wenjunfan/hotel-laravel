@extends('layouts.admin')

@section('title', '酒店调价Ratio')

@section('css')
    <link href="{{ asset('assets/css/plugins.price-ratio.css') }}?v=111" rel="stylesheet">
    {{--single page style--}}
    <style>
        #page-wrapper {
            background: #f3f3f4;
        }
        .wrapper-content {
            padding: 0;
        }
        .page-heading {
            margin-bottom: 20px;
        }
        .h1, .h2, .h3, h1, h2, h3 {
            margin-top: 20px;
            margin-bottom: 20px;
            font-weight: 100;
        }
        h2 {
            font-size: 24px;
        }
        .ibox {
            margin: 0;
        }
        .white-bg {
            background: white;
        }
        .grey-bg {
            background-color: #f3f3f4;
        }
        .input-group {
            display: flex;
        }
        .btn {
            border-radius: 0;
        }
        .input-radio {
            width: 60px;
            text-align: center;
        }
        .div-ratio-all .table {
            margin-bottom: 0;
        }
        .chosen-container-multi .chosen-choices {
            border-radius: 0;
        }
        .ibox-content.sk-loading:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .ibox-content.sk-loading > .sk-spinner {
            display: block;
            position: absolute;
            top: 40%;
            left: 0;
            right: 0;
            z-index: 2000;
        }
        .ibox-content > .sk-spinner {
            display: none;
        }
        .chosen-select-id, .chosen-select-name {
            width: 350px;
        }
        .input-ids .bootstrap-tagsinput {
            width: 450px;
        }
        .bootstrap-tagsinput input {
            width: 100px;
        }
        .sweet-alert button.cancel {
            display: inline-block !important;
        }
    </style>
@endsection

@section('content')
    <div id="ratioApp">
        {{--header --}}
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>酒店调价 Ratio</h2>
            </div>
        </div>
        {{--search--}}
        <div class="row">
            {{--<div class="col-lg-6">--}}
            {{--<div class="ibox">--}}
            {{--<div class="ibox-title">--}}
            {{--<h5>按酒店名称搜索多个酒店 (EBooking酒店列表)</h5>--}}
            {{--</div>--}}
            {{--<div class="ibox-content">--}}
            {{--<div class="input-group">--}}
            {{--<select data-placeholder="Choose a name..." class="chosen-select-name" multiple tabindex="4">--}}
            {{--<option value="">Select</option>--}}
            {{--@foreach($hotelSearchList as $hotel)--}}
            {{--<option value="{{$hotel->hotelId}}">{{$hotel->name}}</option>--}}
            {{--@endforeach--}}
            {{--</select>--}}
            {{--<button type="button" class="btn btn-default" @click="getPriceRatioHotels('name')"><i class="fa fa-search"></i>搜索</button>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            <div class="col-lg-6">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>按酒店id搜索多个酒店 (输入数字后回车即可添加新id)</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="input-group input-ids">
                            <input class="tagsinput form-control" type="text" :value="ids"/>
                            <button type="button" class="btn btn-default" @click="getPriceRatioHotels('id')"><i class="fa fa-search"></i> 搜索</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <hr>
        {{--table start--}}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>酒店搜索结果列表</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li>
                                    <a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li>
                                    <a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content sk-loading">
                        {{--start loading--}}
                        @include('components.loading')
                        {{--end loading--}}
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="col-lg-1">
                                        <input type="checkbox" class="i-checks icheck-all" name="input[]">
                                        全选
                                    </th>
                                    <th>酒店id</th>
                                    <th>酒店名称</th>
                                    <th>酒店地址</th>
                                    <th class="col-lg-1">Pirce Ratio</th>
                                    <th class="col-lg-1">C端 Ratio</th>
                                    <th class="col-lg-1">Ctrip Ratio</th>
                                    <th class="col-lg-1">Ali Ratio</th>
                                    <th class="col-lg-1">Temp Ratio</th>
                                    <th class="col-lg-1">操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="hotel in hotelList">
                                    <td>
                                        <input type="checkbox" class="i-checks" name="input[]" checked v-icheck="" :id="hotel.hotelId">
                                    </td>
                                    <td>@{{ hotel.hotelId }}</td>
                                    <td>@{{ hotel.name }}</td>
                                    <td>@{{ hotel.address }}</td>
                                    <td>
                                        <input type="text" class="form-control input-sm input-radio" v-model="hotel.priceRatio">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm input-radio" v-model="hotel.priceRatioToC">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm input-radio" v-model="hotel.priceRatioToCtrip">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm input-radio" v-model="hotel.priceRatioToAli">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm input-radio" v-model="hotel.priceRatioTemp">
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-primary" @click="updateHotelsPriceRatio(hotel)">保存</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        {{--批量修改--}}
                        <div class="row">
                            <div class="col-sm-5 text-danger">
                                <strong>说明: </strong><br>- 单个修改酒店ratio, 请在单行记录中调整，并点击单行中最后的保存按钮保存<br>- 如果批量修改多个酒店的ratio(统一值) ，请点击表格上面的"保存所有"保存
                            </div>
                            <div class="col-sm-7 div-ratio-all">
                                <strong>批量修改酒店Ratio</strong>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Pirce Ratio</th>
                                        <th>C端 Ratio</th>
                                        <th>Ctrip Ratio</th>
                                        <th>Ali Ratio</th>
                                        <th>Temp Ratio</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <input type="text" class="form-control input-sm input-radio" v-model="allItemRatio.priceRatio">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm input-radio" v-model="allItemRatio.priceRatioToC">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm input-radio" v-model="allItemRatio.priceRatioToCtrip">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm input-radio" v-model="allItemRatio.priceRatioToAli">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm input-radio" v-model="allItemRatio.priceRatioTemp">
                                        </td>
                                        <td>
                                            <button class="btn btn-primary" @click="updateHotelsPriceRatio(allItemRatio)">保存所有选中酒店Ratio</button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/js/plugins.price-ratio.js') }}?v=111"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    {{--single page script--}}
    <script>

        Vue.directive('icheck', {
            inserted: function (el, b, vnode) {
                jQuery(el).iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%', // optional
                });
            },
        });

        var app = new Vue({
            el: '#ratioApp',
            data: {
                message: 'Hello Vue!',
                checkAll: true,
                hotelList: [],
                ids: [],
                ids_name: [],
                allItemRatio: {
                    priceRatio: '0.1',
                    priceRatioToC: '0.2',
                    priceRatioToCtrip: '0.3',
                    priceRatioTemp: '0.5',
                    priceRatioToAli: '0.4',
                },
            },
            methods: {

                getPriceRatioHotels (type) {
                    $('.ibox-content').addClass('sk-loading');
                    var self = this;
                    var get_ids = type === 'name' ? self.ids_name : self.ids;
                    axios.get('/affiliate/get-price-ratio-hotels', {
                        params: {
                            ids: get_ids,
                        },
                    }).then(function (response) {
                        self.hotelList = response.data.data;
                        $('.ibox-content').removeClass('sk-loading');
                    }).catch(function (error) {
                        $('.ibox-content').removeClass('sk-loading');
                    }).then(function () {
                        // always executed
                    });
                },

                updateHotelsPriceRatio (item) {
                    var node_loading = $('.ibox-content');
                    var self = this;
                    var update_ids = [];
                    update_ids = item.hotelId === undefined ? self.ids : [item.hotelId];

                    if (update_ids.length === 0) {
                        alert('没有选中任何酒店！');

                        return;
                    }
                    // alert first
                    swal({
                        title: "Are you sure to update Ratio?",
                        text: update_ids,
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }, function (willDelete) {
                        if (willDelete) {
                            // TODO: 提交前检查check和uncheck items, 更新ids, 目前是全选状态，uncheck无效
                            node_loading.addClass('sk-loading');
                            axios.post('/affiliate/update-hotels-price-ratio', {
                                ids: update_ids,
                                data_ratio: item,
                            }).then(function (response) {
                                if (response.data.success) {
                                    swal({
                                        title: 'update success!!',
                                        text: '',
                                        type: 'success',
                                    });

                                    if (item.hotelId === undefined) {
                                        self.hotelList = self.hotelList.map(hotelItem => {
                                            hotelItem.priceRatio = item.priceRatio;
                                            hotelItem.priceRatioToC = item.priceRatioToC;
                                            hotelItem.priceRatioToCtrip = item.priceRatioToCtrip;
                                            hotelItem.priceRatioTemp = item.priceRatioTemp;
                                            hotelItem.priceRatioToAli = item.priceRatioToAli;

                                            return hotelItem;
                                        });
                                    }
                                } else {
                                    alert('update failed!');
                                }
                                node_loading.removeClass('sk-loading');
                            }).catch(function (error) {
                                node_loading.removeClass('sk-loading');
                            }).then(function () {
                                // always executed
                            });

                        } else {
                            swal("Your imaginary file is safe!");
                        }
                    });

                },
            },

            mounted: function () {
                var self = this;
                // remove loading
                window.addEventListener('load', function (event) {
                    $('.ibox-content').removeClass('sk-loading');
                });
                // 选中的hotels names
                $('.chosen-select-name').chosen({width: '100%'}).change(function (e, params) {
                        self.ids_name = $('.chosen-select-name').chosen().val();
                    },
                );

                jQuery('input').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%', // optional
                });

                // 全选方法
                var node_check_all = jQuery('.icheck-all');
                node_check_all.on('ifChecked', function (e) {
                    $('.i-checks').iCheck('check');
                });
                node_check_all.on('ifUnchecked', function (e) {
                    $('.i-checks').iCheck('uncheck');
                });

                // 点击酒店的 icheck
                var node_check_item = jQuery('.i-checks');

                //根据id 搜索： 多选tags
                var tagesInput = $('.tagsinput');
                tagesInput.tagsinput({
                    tagClass: 'label label-primary',
                });
                tagesInput.on('itemAdded', function (event) {
                    self.ids = tagesInput.tagsinput('items');
                });
                tagesInput.on('itemRemoved', function (event) {
                    self.ids = tagesInput.tagsinput('items');
                });
            },

        });
    </script>
@endsection
