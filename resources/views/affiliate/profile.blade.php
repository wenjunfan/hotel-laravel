<?php
function avatarSelected($profileName, $name)
{
	return $profileName == $name ? 'circle-border' : '';
}
function profileValue($name)
{
	return old($name) == '' ? Auth::user()->$name : old($name);
}
?>

@extends('layouts.admin')

@section('title', 'Profile')

@section('css')
	<link href="{{ asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
	<link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
	<style>
		@media (min-width: 1920px) {
			.all-wrapper {
				margin: auto;
				max-width: 1800px; }
		}
	</style>
@endsection

@section('content')
	<div class="row all-wrapper animated fadeInRight">
		<div class="col-lg-4 col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<strong>{{trans('profile.Title')}}</strong>
				</div>
				<div class="ibox-content">
					<table>
						<tr>
							<td width="60" height="60" align="right" valign="top">{{trans('profile.first')}}
								：
							</td>
							<td valign="top">{{trans('profile.firsti')}}</td>
						</tr>

						<tr>
							<td width="60" height="60" align="right" valign="top">{{trans('profile.second')}}
								：
							</td>
							<td valign="top">{{trans('profile.secondi')}}</td>
						</tr>

						<tr>
							<td width="60" height="60" align="right" valign="top">{{trans('profile.third')}}
								：
							</td>
							<td valign="top">{{trans('profile.thirdi')}}</td>
						</tr>
						<tr>
							<td width="80" height="0" align="right"></td>
							<td>{{trans('profile.hotlineb')}} {{trans('profile.hotlineAlex')}}</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					{{trans('profile.password')}}
				</div>
				<div>
					<div class="ibox-content">
						<form class="form-horizontal" role="form" method="POST" action="{{ url('affiliate/resetPassword') }}">
							{!! csrf_field() !!}
							<button type="submit" class="btn btn-primary btn-block">
								<i class="fa fa-envelope"></i> {{trans('profile.passwordb')}}
							</button>
						</form>
					</div>
				</div>
			</div>

		</div>
		<div class="col-lg-8 col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<strong>{{trans('profile.company')}}  </strong>
				</div>
				<div>
					<div class="ibox-content">
						<form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/affiliate/profile/update') }}">
							{!! csrf_field() !!}
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">{{trans('profile.username')}}
									<span class="text-danger">&nbsp;*</span></label>

								<div class="col-sm-9 col-md-4">
									<input type="text" name="name" value="{{ profileValue('name') }}" class="form-control">

									@if ($errors->has('email'))
										<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">{{trans('profile.email')}}
									<span class="text-danger">&nbsp;*</span></label>
								<div class="col-sm-9 col-md-6">
									<input type="text" name="email" value="{{ profileValue('email') }}" class="form-control" readonly>
									@if ($errors->has('email'))
										<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('contact_person') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">{{trans('profile.fullName')}}
									<span class="text-danger">&nbsp;*</span></label>
								<div class="col-sm-9 col-md-4">
									<input type="text" name="contact_person" value="{{ profileValue('contact_person') }}" class="form-control">

									@if ($errors->has('contact_person'))
										<span class="help-block">
										<strong>{{ $errors->first('contact_person') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
								<label class="col-sm-3 control-label">{{trans('profile.cell')}}
									<span class="text-danger">&nbsp;*</span></label>
								<div class="col-sm-9 col-md-4">
									<input type="text" name="mobile_phone" value="{{ profileValue('mobile_phone') }}" class="form-control">

									@if ($errors->has('mobile_phone'))
										<span class="help-block">
										<strong>{{ $errors->first('mobile_phone') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('company_address') ? 'has-error' : '' }}">
								<label class="col-sm-3 control-label">{{trans('profile.checkAddress')}}<span class="text-danger">&nbsp;*</span>
								<span class="text-danger">&nbsp;</span></label>
								<div class="col-sm-9 col-md-6">
								<input type="text" name="company_address" value="{{ profileValue('company_address') }}" class="form-control">
								@if ($errors->has('company_address'))
								<span class="help-block">
								<strong>{{ $errors->first('company_address') }}</strong>
								</span>
								@endif
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-6 col-sm-offset-3">
									<button class="btn btn-primary" type="submit">
										<i class="fa fa-save"></i>{{trans('profile.save')}}
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
    {{--single page scripts--}}
    @include('affiliate.includes.js-profile')
@endsection
