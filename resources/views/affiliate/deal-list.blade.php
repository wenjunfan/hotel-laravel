@extends('layouts.admin')

@section('title', '折扣列表')

@section('css')
    <link rel="stylesheet" href="{{ url('/assets/css/admin/plugins.deal-list.css') }}">
@endsection

@section('content')
    <div id="deals_list_app">
        <div class="controller-wrapper text-right">
            <button type="button" class="btn btn-primary btn-toggle-disabled" @click="toggleDisabled"><span>显示</span>Disabled</button>
            <button type="button" class="btn btn-info" @click="openNewCouponModal">新增coupon</button>
        </div>

        <table id="coupons_list" border="1" width="100%">
            <thead>
            <tr>
                <th>Coupon Id</th>
                <th>Coupon Code</th>
                <th>使用次数（生成潜在订单时计算）</th>
                <th>使用酒店历史</th>
                <th>操作</th>
            </tr>
            </thead>

            <tbody>
            @foreach($coupons as $coupon)
                <tr class="coupon-item @if($coupon->enabled === 0) coupon-item-disabled hidden @endif">
                    <td>{{ $coupon->id }} @if($coupon->enabled ===0)<span class="badge">disabled</span>@endif</td>
                    <td>{{ $coupon->code }}</td>
                    <td>{{ count($coupon->couponHistory) }}</td>
                    <td>
                        @if(isset($coupon->couponHistory))
                            @foreach($coupon->couponHistory as $index => $history)
                                @if($index !== 0), @endif{{ isset($history->hotel) ? $history->hotel->name : $history->hotel_id }}
                            @endforeach
                        @endif
                    </td>
                    <td>
                        <a href="{{ url('/deal') . '/' . $coupon->code }}" target="_blank" class="btn btn-info">前台查看</a>
                        <button class="btn btn-primary" @click="openUpdateCouponModal('{{ $coupon->id }}')">更新</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="modal fade" id="couponModal">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="text-center">
                            <span v-if="isUpdateCoupon">Update Coupon</span>
                            <span v-else>Add New Coupon</span>
                        </h3>
                    </div>

                    <div class="modal-body" v-if="coupon">
                        <form action="#" method="post" id="coupon_form">
                            {!! csrf_field() !!}
                            <h4 v-if="!isUpdateCoupon"><span class="text-danger">*: 图片只支持png格式</span></h4><br>
                            <div class="row">
                                <div class="col-xs-3 text-right">
                                    <label for="coupon_code">是否隐藏顶部图片:</label>
                                </div>
                                <div class="col-xs-5">
                                    <select name="coupon_image_display" id="coupon_image_display" v-model="coupon.hide_image">
                                        <option :value="1">是</option>
                                        <option :value="0">否</option>
                                    </select>
                                </div>
                                <div class="col-xs-3">
                                    <button type="button" class="btn btn-danger pull-right btn-disable" @click="disableCoupon" v-show="isUpdateCoupon">Disable Coupon</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 text-right">
                                    <label for="coupon_code"><span class="text-danger">*</span>Code:</label>
                                </div>
                                <div class="col-xs-9">
                                    <input type="text" id="coupon_code" name="coupon_code" required v-model="coupon.code">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 text-right">
                                    <label for="coupon_amount"><span class="text-danger">*</span>Amount:</label>
                                </div>
                                <div class="col-xs-9">
                                    <input type="number" min="0" id="coupon_amount" name="coupon_amount" required v-model="coupon.amount">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 text-right">
                                    标题
                                </div>
                                <div class="col-xs-4">
                                    <label for="coupon_title">
                                        英文
                                    </label>
                                    <textarea id="coupon_title" name="text" cols="45" rows="3" v-model="coupon.title"></textarea>
                                </div>
                                <div class="col-xs-4">
                                    <label for="coupon_title_zh">
                                        中文
                                    </label>
                                    <textarea id="coupon_title_zh" name="text" cols="45" rows="3" v-model="coupon.title_zh"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 text-right">
                                    名称
                                </div>
                                <div class="col-xs-4">
                                    <label for="coupon_name">
                                        英文
                                    </label>
                                    <textarea id="coupon_name" name="text" cols="45" rows="3" v-model="coupon.name"></textarea>
                                </div>
                                <div class="col-xs-4">
                                    <label for="coupon_name_zh">
                                        中文
                                    </label>
                                    <textarea id="coupon_name_zh" name="text" cols="45" rows="3" v-model="coupon.name_zh"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 text-right">
                                    <label for="start_at">使用日期:</label>
                                </div>
                                <div class="col-xs-9">
                                    <input type="text" id="start_at" name="start_at" class="datepicker" v-model="coupon.start_at" autocomplete="off"> -
                                    <input type="text" id="expires_at" name="expires_at" class="datepicker" v-model="coupon.expires_at" autocomplete="off">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 text-right">
                                    <label for="expires_at">checkin日期:</label>
                                </div>
                                <div class="col-xs-9">
                                    <input type="text" id="checkin_start_date" name="checkin_start_date" class="datepicker" v-model="coupon.checkin_start_date" autocomplete="off"> -
                                    <input type="text" id="checkin_end_date" name="checkin_end_date" class="datepicker" v-model="coupon.checkin_end_date" autocomplete="off">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 text-right">
                                    <label for="hotelIds">
                                        适用酒店Id<br><span class="text-danger">（用英文逗号分开，无空格）</span>:
                                    </label>
                                </div>
                                <div class="col-xs-9">
                                    <textarea name="hotelIds" id="hotelIds" cols="90" rows="10" v-model="coupon.hotelIds"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 text-right">
                                    <label for="img_pc"><span class="text-danger" v-if="imageRequired">*</span>pc背景:</label>
                                </div>
                                <div class="col-xs-9">
                                    <input type="file" accept="image/png,image/jpeg" id="img_pc" name="img_pc" :required="imageRequired" @change="processFile($event, 'img_pc')">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 text-right">
                                    <label for="img_mobile"><span class="text-danger" v-if="imageRequired">*</span>mobile背景:</label>
                                </div>
                                <div class="col-xs-9">
                                    <input type="file" accept="image/png,image/jpeg" id="img_mobile" name="img_mobile" :required="imageRequired" @change="processFile($event, 'img_mobile')">
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" id="submit_btn" @click="updateCoupon" v-if="isUpdateCoupon">Submit</button>
                        <button type="submit" class="btn btn-primary" id="submit_btn" @click="createCoupon" v-else>Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var appUrl = '{{ config('app.url') }}';
        var coupons = {!! json_encode($coupons->toArray()) !!};
    </script>
    <script src="{{ url('/assets/js/admin/plugins.deal-list.js') }}" type="text/javascript"></script>
@endsection