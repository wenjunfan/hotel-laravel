@extends('layouts.admin')

@section('title', '潜在订单')

@section('css')
	<link href="{{ asset('assets/css/admin/plugins.orders.css') }}" rel="stylesheet">
	<link href="{{ asset('css/hotel/potential-list.css?080918a') }}" rel="stylesheet">
@endsection

@section('content')
	<div class="row animated fadeInRight">
		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-noborder potential-title-box">
					<h4>潜在订单</h4>
					<br>
                    <form class="form-horizontal" role="form" id="search-form" name="search-form">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label class="col-sm-1 control-label" for="id">网站来源</label>
                            <div class="col-sm-1">
                                <select class="form-control" id="fromUsitour" name="fromUsitour" value="{{$fromUsitour}}">
                                    <option class="form-control" @if($fromUsitour == '') selected @endif value="">
                                        全部
                                    </option>
									<option class="form-control"  @if($fromUsitour === '0') selected @endif  value="0">
										Usitrip
									</option>
                                    <option class="form-control" @if($fromUsitour === '1') selected @endif value="1">
										Usitour
                                    </option>
                                </select>
                            </div>
							<div class="form-group">
								<label class="col-sm-1 control-label" for="id">是否跟进</label>
								<div class="col-sm-1">
									<select class="form-control" id="isFollow" name="isFollow" value="{{$isFollow}}">
										<option class="form-control" @if($isFollow == '') selected @endif value="">
											全部
										</option>
										<option class="form-control" @if($isFollow == '0') selected @endif value="0">
											否
										</option>
										<option class="form-control"  @if($isFollow == '1') selected @endif  value="1">
											是
										</option>
									</select>
								</div>
							<label class="col-sm-1 control-label" for="id">是否成单</label>
							<div class="col-sm-1">
								<select id="isDone" name="isDone" class="form-control"  value="{{$isDone}}">
									<option class="form-control" @if($isDone == '') selected @endif value="">
										全部
									</option>
									<option class="form-control" @if($isDone == '0') selected @endif value="0">
										否
									</option>
									<option class="form-control" @if($isDone == '1') selected @endif  value="1">
										是
									</option>
								</select>
							</div>
                            <label class="col-sm-1 pull-left control-label">用户邮箱</label>
                            <div class="col-sm-2">
                                <input id="email" name="email" type="text" class="form-control" value="{{$email}}" placeholder="用户邮箱">
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-primary btn-block" type="button" onclick="search()">
                                    <i class="fa fa-search"></i> 查询
                                </button>
                            </div>
                        </div>
                        </div>
                    </form>
                    各地当前时间：
					<button class="btn btn-default">中国北京标准时间：{{\Carbon\Carbon::now()->timezone('Asia/Hong_Kong')}}</button>
					<button class="btn btn-default">美国西部PDT时间：{{\Carbon\Carbon::now()->timezone('America/Los_Angeles')}}</button>
					<button class="btn btn-default">美国山区MDT时间：{{\Carbon\Carbon::now()->timezone('America/Denver')}}</button>
					<button class="btn btn-default">美国中部CDT时间：{{\Carbon\Carbon::now()->timezone('America/Chicago')}}</button>
					<button class="btn btn-default">美国东部EDT时间：{{\Carbon\Carbon::now()->timezone('America/New_York')}}</button>
					<br/><br/>
					<h2>客服使用该页流程</h2>
					<br/>
					<div>
						1: 点击跟进按钮，系统自动记录跟进的工号。<span class="text-info">(浅蓝色背景的是usitour的客户，或者8/9号以前，姓氏长度大于5的用户)</span>
						<br/>
						2: 看到有new标签的潜在订单后，复制客人的姓名或者电话，去订单管理页面filter一下，检查客人是否已经在别的时间下单（同一个酒店和入住退房日期）成功。
						<br/>
						3: 如果客人下单成功，直接回到本页，将成功的订单号写在订单号备注里，并且点击成单。
						<br/>
                        4: 如果客人下单没有成功，回到本页，用复制的客人的邮箱在本页filter一下，找到所有相关的酒店信息，然后电话客人咨询。成功帮助客人下单后重复步骤3.
						<br/>
						5：在所有客人姓名的潜在订单后写上咨询结果，系统自动记录您的工号在备注栏。谢谢。
					</div>
					<p class="text-danger">请注意：
						该列表只显示最近的300个来自C端的尝试下单20分钟后未成单的数据，且不含括技术使用公司ip下的测试单,请注意客人的当地下单时间。谢谢
					</p>
					@if(Auth::user()->admin == 1|| Auth::user()->admin == 2)<p>财务请注意：只有在工号为大红色的时候（说明成单在跟进之后，且订单为确认状态。第一个工号是跟进，第二个是成单，订单号的酒店凭证上方是下酒店的工号）才是需要结算的</p> @endif
				</div>
			</div>
		</div>
		<div class="col-lg-12 col-md-12">
			<div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover dataTables-potentialOrderLists">
							<thead>
							<tr>
								<th>ID</th>
								<th>酒店名/人数/房数</th>
								<th>入住/退房</th>
								<th>生成潜在订单的时间<br/>(<span class="text-success">server</span>/<span class="text-info">当地</span>/<span class="text-danger">距离现在</span>)</th>
								<th>姓名/电话/Email/语言</th>
								<th>金额</th>
								<th>跟进</th>
								<th>成单</th>
								<th>订单号</th>
								<th>备注</th>
								<th>工号</th>
							</tr>
							</thead>
							<tbody>
							@foreach($potentialLists as $potentialList)
                                <?php
                                $bookerDetail = json_decode($potentialList->booker, true);
                                $searchDetail = json_decode($potentialList->search_info, true);
                                ?>
								@if($potentialList->created_at > 0)
									<tr class="gradeX @if((isset($bookerDetail['http_host']) && strrpos($bookerDetail['http_host'], 'usitour') !== false )|| (strlen($bookerDetail['lastname']) >=6 && $potentialList->created_at < "2018-08-09 14:36:09")) from-usitour-bg @endif">
										<td>
											{{$potentialList->id}}
											@if($potentialList->isFollow == 0 && $potentialList->commend == '')
												<button class="btn-new btn-danger">New!</button>
											@endif
											@if(isset($potentialList->is_pc))
												<p class="potential-device">{{$potentialList->is_pc == 2? 'mobi' : 'pc'}}</p>
											@endif
										</td>
										<td>
											{{$potentialList->goods_name}}
											<p class="text-info">{{$searchDetail['occupancies'][0]['rooms'].'房 ('.$searchDetail['occupancies'][0]['adults'] .'成人，' . $searchDetail['occupancies'][0]['adults'] .'儿童)'}}</p>
										</td>
										<td>
											<p>{{$searchDetail['checkin']}}<br/>{{$searchDetail['checkout']}}</p>
										</td>
										<td>
											<p>{{$potentialList->created_at}}</p>
											<p class="text-info">
												@if(isset($potentialList->booker_local_time))
                                                    <?php
                                                    $monthStr = substr($potentialList->booker_local_time,4, 3);
                                                    $monthArray = array("01"=>"Jan", "02"=>"Feb", "03"=>"Mar","04"=>"Apr","05"=>"May","06"=>"Jun",
                                                        "07"=>"Jul","08"=>"Aug","09"=>"Sep", "10"=>"Oct", "11"=>"Nov", "12"=>"Dec");
                                                    $monthNum = array_search($monthStr,$monthArray);
                                                    ?>
													{{substr($potentialList->booker_local_time,11, 4). '-'.$monthNum.'-'.substr($potentialList->booker_local_time,8, 2).' '.substr($potentialList->booker_local_time,16, 8)}}<br/>
													{{substr($potentialList->booker_local_time,24)}}
												@endif
											</p>
											<p class="text-danger">距离现在：
												{{str_replace('before','',\Carbon\Carbon::parse($potentialList->created_at)->diffForHumans(\Carbon\Carbon::now()))}}
											</p>
										</td>
										<td>
											<p>{{$bookerDetail['firstname'] .' ' .$bookerDetail['lastname']}}</p>
											<p class="text-info">{{$bookerDetail['phone']}}</p>
											<p class="text-success">{{$bookerDetail['email']}}</p>
											<p>@if(isset($bookerDetail['language'])){{$bookerDetail['language'] == 1 ? 'English' : '中文'}}@endif</p>
										</td>
										<td>
											{{$potentialList->currencyCode. ' ' .$potentialList->amount }}
											<br/>
											{{'(RMB ' .$potentialList->cny. ')'}}
										</td>
										<td>
											<label class="switch">
												@if ($potentialList->isFollow == 1)
													<img class="smlpic" src="/img/general/icon/valid.png"><br/>
													{{$potentialList->followed_at}}
												@else
													<input type="checkbox" id="isFollowedOrder{{$potentialList->id}}" name="isFollowedOrder{{$potentialList->id}}" onclick="isFollowedOrder({{$potentialList->id}});">
													<div class="slider round"></div>
												@endif
											</label>
										</td>
										<td>
											<label class="switch">
												@if ($potentialList->isDone == 1)
													.<input checked="checked" type="checkbox" id="isDoneOrder{{$potentialList->id}}" name="isDoneOrder{{$potentialList->id}}" onclick="isDoneOrder({{$potentialList->id}});">
												@else
													<input type="checkbox" id="isDoneOrder{{$potentialList->id}}" name="isDoneOrder{{$potentialList->id}}" onclick="isDoneOrder({{$potentialList->id}});">
												@endif
												<div class="slider round"></div>
											</label>
										</td>
										<td>
											@if(isset($potentialList->linked_order_id) && $potentialList->linked_order_id != '')
												<span class="text-danger">{{$potentialList->linked_order_id}}</span><br/>
												{{isset($potentialList->linkedorderid->created_at)? $potentialList->linkedorderid->created_at : 'N/A'}}
												<span style="color:red">{{isset($potentialList->linkedorderid->sales_no)? $potentialList->linkedorderid->sales_no : 'N/A'}}</span> <br/>
												<a target="_blank" href="{{isset($potentialList->linkedorderid->code) ? url('voucher/'.$potentialList->linkedorderid->code) : ''}}">酒店凭证</a>
											@else
												<textarea readonly id="linked_order_id_{{$potentialList->id}}" name="linked_order_id_{{$potentialList->id}}" rows="1" cols="4" maxlength="4" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;"></textarea>
												<button name="roEdit" class="label btn" id="roEditBtn{{$potentialList->id}}" onclick="ableAddOrderId({{$potentialList->id}})">
													<i class="fa fa-edit"></i></button>
												<button name="roSave" id="roSaveBtn{{$potentialList->id}}" class="label btn hidden" style="margin-top:2px;" onclick="addLinkedOrderId({{$potentialList->id}})">
													保存更新
												</button>
											@endif
										</td>
										<td>
											<a href="#" class="tooltip-test" title="{{$potentialList->commend}}">
												<textarea readonly id="commend{{$potentialList->id}}" rows="3" cols="10" maxlength="1000" class="potential-commend-area">@if(isset($potentialList->commend)){{$potentialList->commend}}@endif</textarea></a>
											<button name="roEdit" class="label btn" type="button" id="roEditBtn{{$potentialList->id}}" data-toggle="modal" data-target="#myModal-{{$potentialList->id}}" data-whatever="@if(isset($potentialList->id)){{$potentialList->id}}@endif">
												<i class="fa fa-edit"></i>
											</button>
											<!--Modal start-->
											<form action="{{URL::to('/affiliate/potential-lists')}}">
												<div class="modal fade" id="myModal-{{$potentialList->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
													<div class="modal-dialog" role="document" style="padding-top:70px">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
																<h4>备注({{$potentialList->id}})：</h4>
															</div>
															<div class="modal-body potential-order-commend">
																<textarea autofocus id="new-commend-{{$potentialList->id}}" name="new-commend-{{$potentialList->id}}" rows="4" cols="80" maxlength="1000"  class="potential-commend-area potential-commend-input"></textarea>
																<button name="roSave" id="roSaveBtn{{$potentialList->id}}" class="label btn" onclick="addNewNote({{$potentialList->id}})">
																	保存更新
																</button>
															</div>
															<div class="modal-footer" style="text-align:left">
																<textarea readonly id="old-commend-{{$potentialList->id}}" name="old-commend-{{$potentialList->id}}" rows="10" cols="91" class="potential-old-commend-box potential-commend-area">@if(isset($potentialList->commend)){{$potentialList->commend}}@endif</textarea>
															</div>
														</div>
													</div>
												</div>
											</form>
										</td>
										<td>
											<span @if(isset($potentialList->linkedorderid->created_at) && isset($potentialList->followed_at) && $potentialList->linkedorderid->created_at > $potentialList->followed_at && $potentialList->linkedorderid->status == 'CONFIRMED')class="text-danger"@endif >
											{{$potentialList->rep}} @if(isset($potentialList->linked_order_id)) , {{$potentialList->sales_no}} @endif
											</span>
										</td>
									</tr>
								@endif
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('/assets/js/admin/plugins.orders.js?09122018')}}"></script>
    {{--single page scripts--}}
    @include('affiliate.includes.js-potential-lists')
@endsection
