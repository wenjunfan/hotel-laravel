<?php
$optionName = isset($optionName) ? $optionName : 'Welcome';
$domain =  $is_b? "117book" : $is_usitour? 'Usitour' : 'Usitrip';
use App\Model\Distributor;
$distributors = Distributor::where('partner_id', '<>', '')->get();
?>
@extends('layouts.admin')

@section('title', 'Welcome')

@section('css')
    <link href="{{ asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/admin/welcome-affiliate.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row all-wrapper animated fadeInRight">
        <img class="img-responsive login-intro-img">
        <div class="content-box">
            <h1>
                {{trans('welcome.welcome')}}
            </h1>

            <h2>{{trans('welcome.code_below')}}</h2>
            @foreach($distributors as $distributor)
                @if(Auth::user()->id == $distributor->partner_id)
                <span class="link">{{$distributor->distributor_code}}</span>
                @endif
            @endforeach

            <h2>{{trans('welcome.link_below')}}</h2>
            @foreach($distributors as $distributor)
                @if(Auth::user()->id == $distributor->partner_id)
                    <a class="link" target="_blank" href="https://hotel.{{$domain}}.com?source={{$distributor->distributor_code}}">https://hotel.{{$domain}}.com?source={{$distributor->distributor_code}}</a>
                @endif
            @endforeach
            <br/>
            <h4><i class="money"></i>{{trans('welcome.title1')}}</h4>
            <div class="flow-one"></div>
            <p>{{trans('welcome.text1')}}</p>

            <h4><i class="percentage"></i>{{trans('welcome.title2')}}</h4>
            <div class="flow-two"></div>
            <p>{{trans('welcome.text2')}}</p>

            <h4><i class="cash"></i>{{trans('welcome.title3')}}</h4>
            <div class="flow-three"></div>
            <p>{{trans('welcome.text3')}}</p>

            <h4><i class="card"></i>{{trans('welcome.title4')}}</h4>
            <div class="flow-four"></div>
            <p>{{trans('welcome.text4')}}</p>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
@endsection
