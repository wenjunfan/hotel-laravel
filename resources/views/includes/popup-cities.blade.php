<script>
    {{--搜索城市id, 在php config 文件中定义--}}
      window.cityIds = {
        @foreach (config('city-ids') as $key => $value)
        "{{ $key }}": "{{$value}}",
        @endforeach
    };
    //TODO 以下定义，未来使用laravel locale时，可以把name重新定义
    window.hotCities = [
        {
            name: { 'cn': '纽约', 'tw': '紐約', 'en': 'New York' },
            id: cityIds['1001']
        },
        {
            name: { 'cn': '洛杉矶', 'tw': '洛杉磯', 'en': 'Los Angeles' },
            id: cityIds['1002']
        },
        {
            name: { 'cn': '拉斯维加斯', 'tw': '拉斯維加斯', 'en': 'Las Vegas' },
            id: cityIds['1004']
        },
        {
            name: { 'cn': '芝加哥', 'tw': '芝加哥', 'en': 'Chicago' },
            id: cityIds['1006']
        },
        {
            name: { 'cn': '多伦多', 'tw': '多倫多', 'en': 'Toronto' },
            id: cityIds['1013']
        },
        {
            name: { 'cn': '悉尼', 'tw': '悉尼', 'en': 'Sydney' },
            id: cityIds['3001']
        },
        {
            name: { 'cn': '伦敦', 'tw': '倫敦', 'en': 'London' },
            id: cityIds['2001']
        },
        {
            name: { 'cn': '柏林', 'tw': '柏林', 'en': 'Berlin' },
            id: cityIds['2006']
        },
        {
            name: { 'cn': '罗马', 'tw': '羅馬', 'en': 'Rome' },
            id: cityIds['2009']
        },
        {
            name: { 'cn': '威尼斯', 'tw': '威尼斯', 'en': 'Venice' },
            id: cityIds['2010']
        },
        {
            name: { 'cn': '马德里', 'tw': '馬德裏', 'en': 'Madrid' },
            id: cityIds['2013']
        },
        {
            name: { 'cn': '日内瓦', 'tw': '日内瓦', 'en': 'Geneva' },
            id: cityIds['2018']
        },
        {
            name: { 'cn': '阿姆斯特丹', 'tw': '阿姆斯特丹', 'en': 'Amsterdam' },
            id: cityIds['2015']
        },
        {
            name: { 'cn': '卢森堡', 'tw': '盧森堡', 'en': 'Luxembourg' },
            id: cityIds['2019']
        },
    ];
    window.usCities = [
        {
            name: { 'cn': '纽约', 'tw': '紐約', 'en': 'New York' },
            id: cityIds['1001']
        },
        {
            name: { 'cn': '洛杉矶', 'tw': '洛杉磯', 'en': 'Los Angeles' },
            id: cityIds['1002']
        },
        {
            name: { 'cn': '旧金山', 'tw': '舊金山', 'en': 'San Francisco' },
            id: cityIds['1003']
        },
        {
            name: { 'cn': '拉斯维加斯', 'tw': '拉斯维加斯', 'en': 'Las Vegas' },
            id: cityIds['1004']
        },
        {
            name: { 'cn': '波士顿', 'tw': '波士頓', 'en': 'Boston' },
            id: cityIds['1005']
        },
        {
            name: { 'cn': '芝加哥', 'tw': '芝加哥', 'en': 'Chicago' },
            id: cityIds['1006']
        },
        {
            name: { 'cn': '华盛顿', 'tw': '華盛頓', 'en': 'Washington' },
            id: cityIds['1007']
        },
        {
            name: { 'cn': '檀香山', 'tw': '檀香山', 'en': 'Honolulu' },
            id: cityIds['1008']
        },
        {
            name: { 'cn': '奥兰多', 'tw': '奧蘭多', 'en': 'Orlando' },
            id: cityIds['1009']
        },
        {
            name: { 'cn': '迈阿密', 'tw': '邁阿密', 'en': 'Miami' },
            id: cityIds['1010']
        },
        {
            name: { 'cn': '西雅图', 'tw': '西雅圖', 'en': 'Seattle' },
            id: cityIds['1011']
        },
        {
            name: { 'cn': '温哥华', 'tw': '溫哥華', 'en': 'Vancouver' },
            id: cityIds['1012']
        },
        {
            name: { 'cn': '多伦多', 'tw': '多倫多', 'en': 'Toronto' },
            id: cityIds['1013']
        },
        {
            name: { 'cn': '维多利亚', 'tw': '維多利亞', 'en': 'Victoria' },
            id: cityIds['1014']
        },
        {
            name: { 'cn': '蒙特利尔', 'tw': '蒙特利爾', 'en': 'Montreal' },
            id: cityIds['1015']
        },
        {
            name: { 'cn': '坎昆', 'tw': '坎昆', 'en': 'Cancun' },
            id: cityIds['1016']
        },
    ];
    window.eurCities = [
        {
            name: { 'cn': '伦敦', 'tw': '倫敦', 'en': 'London' },
            id: cityIds['2001']
        },
        {
            name: { 'cn': '爱丁堡', 'tw': '愛丁堡', 'en': 'Edinburgh' },
            id: cityIds['2002']
        },
        {
            name: { 'cn': '巴黎', 'tw': '巴黎', 'en': 'Paris' },
            id: cityIds['2003']
        },
        {
            name: { 'cn': '尼斯', 'tw': '尼斯', 'en': 'Nice' },
            id: cityIds['2004']
        },
        {
            name: { 'cn': '里昂', 'tw': '裏昂', 'en': 'Lyon' },
            id: cityIds['2005']
        },
        {
            name: { 'cn': '柏林', 'tw': '柏林', 'en': 'Berlin' },
            id: cityIds['2006']
        },
        {
            name: { 'cn': '慕尼黑', 'tw': '慕尼黑', 'en': 'Munich' },
            id: cityIds['2007']
        },
        {
            name: { 'cn': '法兰克福', 'tw': '法蘭克福', 'en': 'Frankfurt' },
            id: cityIds['2008']
        },
        {
            name: { 'cn': '罗马', 'tw': '羅馬', 'en': 'Rome' },
            id: cityIds['2009']
        },
        {
            name: { 'cn': '威尼斯', 'tw': '威尼斯', 'en': 'Venice' },
            id: cityIds['2010']
        },
        {
            name: { 'cn': '佛罗伦萨', 'tw': '佛羅倫薩', 'en': 'Florence' },
            id: cityIds['2011']
        },
        {
            name: { 'cn': '巴塞罗那', 'tw': '巴塞羅那', 'en': 'Barcelona' },
            id: cityIds['2012']
        },
        {
            name: { 'cn': '马德里', 'tw': '馬德裏', 'en': 'Madrid' },
            id: cityIds['2013']
        },
        {
            name: { 'cn': '苏黎世', 'tw': '蘇黎世', 'en': 'Zurich' },
            id: cityIds['2014']
        },
        {
            name: { 'cn': '阿姆斯特丹', 'tw': '阿姆斯特丹', 'en': 'Amsterdam' },
            id: cityIds['2015']
        },
        {
            name: { 'cn': '雅典', 'tw': '雅典', 'en': 'Athens' },
            id: cityIds['2016']
        },
        {
            name: { 'cn': '米兰', 'tw': '米蘭', 'en': 'Milan' },
            id: cityIds['2017']
        },
    ];
    window.oceCities = [
        {
            name: { 'cn': '悉尼', 'tw': '悉尼', 'en': 'Sydney' },
            id: cityIds['3001']
        },
        {
            name: { 'cn': '墨尔本', 'tw': '墨爾本', 'en': 'Melbourne' },
            id: cityIds['3002']
        },
        {
            name: { 'cn': '黄金海岸', 'tw': '黄金海岸', 'en': 'Gold Coast' },
            id: cityIds['3003']
        },
        {
            name: { 'cn': '凯恩斯', 'tw': '凯恩斯', 'en': 'Keynes' },
            id: cityIds['3004']
        },
        {
            name: { 'cn': '布里斯班', 'tw': '布裏斯班', 'en': 'Brisbane' },
            id: cityIds['3005']
        },
        {
            name: { 'cn': '阿德莱德', 'tw': '阿德萊德', 'en': 'Adelaide' },
            id: cityIds['3006']
        },
        {
            name: { 'cn': '珀斯', 'tw': '珀斯', 'en': 'Perth' },
            id: cityIds['3007']
        },
        {
            name: { 'cn': '堪培拉', 'tw': '堪培拉', 'en': 'Canberra' },
            id: cityIds['3008']
        },
        {
            name: { 'cn': '奥克兰', 'tw': '奧克蘭', 'en': 'Oakland' },
            id: cityIds['3010']
        },
        {
            name: { 'cn': '皇后镇', 'tw': '皇後鎮', 'en': 'Queenstown' },
            id: cityIds['3011']
        },
        {
            name: { 'cn': '惠灵顿', 'tw': '惠靈頓', 'en': 'Wellington' },
            id: cityIds['3012']
        },
        {
            name: { 'cn': '基督城', 'tw': '基督城', 'en': 'Christchurch' },
            id: cityIds['3013']
        },
        {
            name: { 'cn': '菲利普岛', 'tw': '菲利普島', 'en': 'Phillip Island' },
            id: cityIds['3014']
        },
        {
            name: { 'cn': '都柏林', 'tw': '都柏林', 'en': 'Dublin' },
            id: cityIds['3015']
        },
    ];
    window.asiCities = [
        {
            name: { 'cn': '新加坡', 'tw': '新加坡', 'en': 'Singapore' },
            id: cityIds['4001']
        },
        {
            name: { 'cn': '首尔', 'tw': '首爾', 'en': 'Seoul' },
            id: cityIds['4002']
        },
        {
            name: { 'cn': '济州岛', 'tw': '濟州島', 'en': 'Jeju Island' },
            id: cityIds['4003']
        },
        {
            name: { 'cn': '巴厘岛', 'tw': '巴厘島', 'en': 'Bali' },
            id: cityIds['4004']
        },
        {
            name: { 'cn': '曼谷', 'tw': '曼谷', 'en': 'Bangkok' },
            id: cityIds['4005']
        },
        {
            name: { 'cn': '普吉岛', 'tw': '普吉岛', 'en': 'Phuket' },
            id: cityIds['4006']
        },
        {
            name: { 'cn': '清迈', 'tw': '清邁', 'en': 'Chiang Mai' },
            id: cityIds['4007']
        },
        {
            name: { 'cn': '芭提亚', 'tw': '芭提亞', 'en': 'Pattaya' },
            id: cityIds['4008']
        },
        {
            name: { 'cn': '苏梅岛', 'tw': '蘇梅島', 'en': 'Sumei Island' },
            id: cityIds['4009']
        },
        {
            name: { 'cn': '吉隆坡', 'tw': '吉隆坡', 'en': 'Kuala Lumpur' },
            id: cityIds['4010']
        },
        {
            name: { 'cn': '凌家卫岛', 'tw': '淩家衛島', 'en': 'Langkawi' },
            id: cityIds['4011']
        },
        {
            name: { 'cn': '东京', 'tw': '東京', 'en': 'Tokyo' },
            id: cityIds['4012']
        },
        {
            name: { 'cn': '大阪', 'tw': '大阪', 'en': 'Osaka' },
            id: cityIds['4013']
        },
        {
            name: { 'cn': '北京', 'tw': '北京', 'en': 'Beijing' },
            id: cityIds['4014']
        },
        {
            name: { 'cn': '香港', 'tw': '香港', 'en': 'Hong Kong' },
            id: cityIds['4015']
        },
    ];
    window.afcCities = [
        {
            name: { 'cn': '开罗', 'tw': '開羅', 'en': 'Cairo' },
            id: cityIds['5001']
        },
        {
            name: { 'cn': '约翰内斯堡', 'tw': '約翰內斯堡', 'en': 'Johannesburg' },
            id: cityIds['5002']
        },
        {
            name: { 'cn': '开普敦', 'tw': '開普敦', 'en': 'Cape Town' },
            id: cityIds['5003']
        },
        {
            name: { 'cn': '卡萨布兰卡', 'tw': '卡薩布蘭卡', 'en': 'Casablanca' },
            id: cityIds['5004']
        },
        {
            name: { 'cn': '内罗毕', 'tw': '內羅畢', 'en': 'Nairobi' },
            id: cityIds['5005']
        },
        {
            name: { 'cn': '德班', 'tw': '德班', 'en': 'Durban' },
            id: cityIds['5006']
        },
        {
            name: { 'cn': '突尼斯', 'tw': '突尼斯', 'en': 'Tunisia' },
            id: cityIds['5007']
        },
        {
            name: { 'cn': '比塞大', 'tw': '比塞大', 'en': 'Bizerte' },
            id: cityIds['5008']
        },
        {
            name: { 'cn': '卢克索', 'tw': '盧克索', 'en': 'Luxor' },
            id: cityIds['5009']
        },
        {
            name: { 'cn': '卢萨卡', 'tw': '盧薩卡', 'en': 'Lusaka' },
            id: cityIds['5010']
        },
        {
            name: { 'cn': '毛里求斯', 'tw': '毛裏求斯', 'en': 'Mauritius' },
            id: cityIds['5011']
        },
        {
            name: { 'cn': '阿克拉', 'tw': '阿克拉', 'en': 'Accra' },
            id: cityIds['5012']
        },
        {
            name: { 'cn': '哈拉雷', 'tw': '哈拉雷', 'en': 'Harare' },
            id: cityIds['5013']
        },
        {
            name: { 'cn': '马拉喀什', 'tw': '馬拉喀什', 'en': 'Marrakesh' },
            id: cityIds['5014']
        },
    ];
</script>
