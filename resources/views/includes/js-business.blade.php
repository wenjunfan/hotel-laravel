<script type="text/javascript">
    jQuery(function ($) {
        $("#Lang").val({{$language}});
        //$("#Lang").val(0);
    });

    function updateLanguage(x) {
        var lang = document.getElementById(x).value;
        if (typeof(Storage) !== "undefined")
            localStorage.setItem('lang', lang);
        //update db
        $.post("{{url('/lang/update')}}", {
            '_token': $("meta[name='csrf_token']").attr('content'),
            'lang': lang,
        }, function (data) {
            if (data.success) {
                location.replace(data.url);
            }
        });
    }

    $('.pull-down').each(function () {
        var $this = $(this);
        $this.css('margin-top', $this.parent().height() - $this.height())
    });

    $(document).ready(function () {
        /* Check width on page load*/
        if ($(window).width() < 767) {
            $("body").removeClass("mini-navbar");
        }
        else {
        }
    });
    // back to top
    $("#backtotop").click(function () {
        $("body,html").animate({
            scrollTop: 0
        }, 600);
    });
    $(window).scroll(function () {
        if ($(window).scrollTop() > 150) {
            $("#backtotop").addClass("visible");
        } else {
            $("#backtotop").removeClass("visible");
        }
    });
</script>