<div class="book-head-marketing h-90">
    <div class="pull-left">
        <h1 class="two-mins">
            {{trans('book.two_mins')}}
        </h1>
        <p class="hurry">
            <i class="fa fa-exclamation-circle"></i>{{trans('book.hurry')}}
        </p>
    </div>
    <div class="pull-right">
        <p class="question"> {{trans('book.question')}}</p>
    </div>
</div>