<div>
    {{--config('app.2b_test_partner_id') 是给b端用户看的不下单的账号--}}
    @if(Auth::check() && Auth::user()->id == config('app.2b_test_partner_id'))
        <p style="color:red; text-align:center">请注册成为真实用户后下单,支持支付宝，微信，paypal和信用卡，postpay支付</p>
    @else
        {{--117book CN user has no CC, usitrip is US, usitour only show cc--}}
        @if(!$is_b || $is_b && Auth::user()->paymentType == 'Prepay')
            <p class="pmt-title">
                @if($is_usitour)
                    {{trans('book.pymt')}}
                @else
                    {{trans('book.pymts')}}
                @endif
            </p>
            <div class="line-divider"></div>
            @if(!$is_usitour)
                <label for="wechatpay" class="paymentLabel"  id="wechatPaymentLabel" >
                    <input id="wechatpay" type="radio" name="imgsel" value="we" checked="checked" required/>
                    <img class="pmy-type-icon" src="{{url('/img/payment/wechat.png')}}" >{{trans('book.we')}}
                </label>
            @endif
            @if(!$is_b || $is_b && Auth::user()->Country_Code == 'US')
                {{--  <label for="ppBtn" class="paymentLabel" id="ppBtnPaymentLabel" >
                    <input id="ppBtn" type="radio" name="imgsel" value="pb" required disabled/>
                    <img class="pmy-type-icon"src="/img/payment/PayPal.png"/>
                </label>--}}
                @if(config('app.enable_cc') || $is_b)
                    <label for="ccpay" id="ccPaymentLabel" class="paymentLabel">
                        <input id="ccpay" type="radio" name="imgsel" value="cc"  @if($is_usitour)checked="checked" @endif required />
                        <img class="pmy-type-icon" src="/img/payment/all-major-credit-cards.png"/>
                    </label>
                @endif
            @endif
            @if(!$is_usitour)
                <label for="alipay" class="paymentLabel" id="aliPaymentLabel" >
                    <input id="alipay" type="radio" name="imgsel" value="al" required />
                    <img class="pmy-type-icon" src="{{url('/img/payment/alipay.png')}}">{{trans('book.ali')}}
                </label>
            @endif
            <input type="hidden" name="hotelId" value="{{ $hotelId }}">
            <div class="form-group pmt-confirm-check">
                    <span id="paymentAuth">
                        <input type="checkbox" id="cb" name="cb" onclick="calc();" checked="checked" class="pull-left"/>
                    </span>
                <div class="pmt-confirm-text">
                    {{trans('book.warmtip')}}
                    <a data-toggle="modal" data-target="#exampleModalLong">
                        {{trans('book.warmtiplink')}}
                    </a>
                    {{trans('book.warning')}}
                </div>
            </div>
            <button type="button" class="btn btn-primary pull-left ladda-button ladda-button-book" data-style="expand-right" id="bt" name="bt" onclick="bookRooms()">
                {{trans('book.paynow')}}
            </button>
            <p class="exchange-rate-warning"><i class="fa
                fa-info-circle"></i>&nbsp;{{$is_b ? '117book' : $is_usitour ? 'Usitour' : $language === 1 ? 'Usitrip' : '走四方'}}{{trans('book.exchange_rate')}}</p>
            {{--117book has postpay as well--}}
        @elseif(Auth::user()->paymentType == 'Postpay' && $is_b)
            <div>
                <div class="form-group" style="padding-top:20px; text-align: justify;-moz-text-align-last: left; /* Code for Firefox */text-align-last: left; padding-right:35px; ">
                    <input type="checkbox" id="cb" name="cb" onclick="calc();" checked="checked" class="pull-left" style="margin-right:10px;padding-bottom:20px;text-align: justify;-moz-text-align-last: left; /* Code for Firefox */text-align-last: left;"/> <strong>{{trans('book.warmtip')}}</strong>{{trans('book.warning')}}
                </div>
                <button type="button" class="btn btn-primary pull-left btn-block ladda-button ladda-button-book" data-style="expand-right" id="bt" name="bt" onclick="bookRooms();" style="margin-right:-15px; width:180px; font-size:16px;" disabled>
                    {{trans('book.paynow')}}
                </button>
            </div>
        @endif
    @endif
    {{--paypal btn direct checkout--}}
    <div id="paypal-button" style="display:none;"></div>
</div>