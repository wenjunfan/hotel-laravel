
    <p class="contact-title">{{trans('book.contactinfo')}}</p>
    <div class="line-divider"></div>
        <div class="form-group" id="hotel-booker">
            <label class="col-sm-2 book-info-label" style="text-align:left;  font-size:14px;padding-left:15px">{{trans('book.name')}}<span class="must-fill">*</span></label>
            <div class="col-sm-3">
                <input class="form-control" type="text" placeholder="{{trans('book.fname')}}" name="booker_fn"  id="booker_fn"  value="{{$booker['customers_firstname'] or ''}}"/>
            </div>
            <div class="col-sm-3">
                <input class="form-control" type="text" name="booker_ln" placeholder="{{trans('book.lname')}}" id="booker_ln"   value="{{$booker['customers_lastname'] or ''}}">
            </div>
        </div>
        <div class="form-group" id="hotel-booker">
            <label class="col-sm-2 book-info-label">{{trans('book.phone')}}<span class="must-fill">*</span></label>
            <div class="col-sm-3">
                <div class="area-box">
                    <input name="phone_country" id="phone_country" readonly class="form-control" type="text" value="+1 " placeholder="">
                    <span class="country-with-flag"><i class="flag" style="background-position: 0px -44px;"></i><i class="fa fa-angle-down"></i></span>
                </div>
                @include('components.phone-country-list')
                <span class="other"></span>
            </div>
            <div class="col-sm-5">
                <input class="form-control" type="text" name="booker_phone" id="booker_phone" placeholder="{{trans('book.phone')}}" value="{{$booker['customers_mobile_phone'] or ''}}">
            </div>
        </div>
        <div class="form-group" id="hotel-booker">
            <label class="col-sm-2 book-info-label">{{trans('book.email')}}<span class="must-fill">*</span></label>
            <div class="col-sm-8">
                <input onfocusout="saveEmailInfo()" class="form-control" type="text" id="booker_email" name="booker_email" value="{{$booker['customers_email_address'] or ''}}" >
            </div>
        </div>
        <div class="form-group" id="hotel-booker">
            <label class="col-sm-2 book-info-label">{{trans('book.remark')}}</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" id="booker_remark" name="booker_remark" >
            </div>
        </div>
