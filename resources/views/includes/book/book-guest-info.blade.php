@for($i = 0; $i < $room['room']['roomCount']; $i++)
    <?php
    $adultNo = $room['occupancy']['adults'];
    $childNo = $room['occupancy']['children'];
    $roomNo = $room['occupancy']['rooms'];
    $totalPrice = $room['room']['netPrice'];
    ?>
    <div>
        <input type="hidden" name="roomRef<?=$i + 1?>" id="roomRef" value="<?php echo $room['room']['roomRef']; ?>">
        <input type="hidden" name="room<?=$i + 1?>" value="{{$room['room']['name']}}">
        <p class="book-room-detail">
          {{trans('book.room')}}<?=$i + 1?>: {{$adultNo . ' ' .trans('book.adult'). ', ' .$childNo. ' ' .trans('book.children').' ' .$room['room']['name']. ', ' .$room['room']['boardName']}}
        </p>
    </div>
    @for($j = 1; $j <= $adultNo; $j++)
        <?php
        if ($adultNo > 1) {
            $adultNo = 1;
        }
        ?>
        <div class="form-group" id="hotel-booker">
            <p class="main-guest-title">{{trans('book.pguest')}}</p>
            <div class="line-divider"></div>
            <label class="book-info-label col-sm-2">{{trans('book.name')}}<span class="must-fill">*</span></label>
            <div class="col-sm-3">
                <input class="form-control" type="text" placeholder="{{trans('book.fname')}}" name="guest<?=$i + 1?>_a1_fn" id="guest<?=$i + 1?>_a1_fn" @if($i == 0)value="{{$booker['customers_firstname'] or ''}}"@endif/>
            </div>
            <div class="col-sm-3">
                <input class="form-control" type="text" placeholder="{{trans('book.lname')}}" name="guest<?=$i + 1?>_a1_ln" id="guest<?=$i + 1?>_a1_ln" @if($i == 0)value="{{$booker['customers_lastname'] or ''}}"@endif/>
            </div>
        </div>
    @endfor
@endfor
<div class="form-group">
    <span class="col-sm-12 guest-info-tip"><i class="fa fa-info-circle" aria-hidden="true"></i>{{trans('book.tip')}}</span>
</div>

{{----}}
@if($is_b)
{{--向酒店request 的特殊需求--}}
<div class="form-group" style="margin-bottom:0">
    <p class="col-sm-12" style="text-align:left; color:#484848; font-size:15px; font-weight:400;">{{trans('book.specialrequest')}}</p>
    <div class="hotel-gallery-options specialrequest" style="margin-top:-15px; height:190px; margin-bottom:0px; padding-left:15px; margin-right:15px;">
        <p style="color:#767676; padding-bottom:5px; text-align: left; padding-top:10px;">{{trans('book.request')}}</p>
        <table>
            <tbody style="font-size:14px; padding-top:25px;">
            <tr style="height:40px">
                <td>
                    <input type="checkbox" name="specialrequest[]" value="I'd like two beds" tabindex=-1> {{trans('book.one')}}
                </td>
                <td>
                    <input type="checkbox" name="specialrequest[]" value="I'd like one (large) bed" tabindex=-1> {{trans('book.two')}}
                </td>
                <td>
                    <input type="checkbox" name="specialrequest[]" value="I'd like a quiet room" tabindex=-1> {{trans('book.three')}}
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" name="specialrequest[]" value="I'd like a room on ground floor" tabindex=-1> {{trans('book.four')}}
                </td>
                <td>
                    <input type="checkbox" name="specialrequest[]" value="I'd like a room on high floor" tabindex=-1> {{trans('book.five')}}
                </td>
                <td>
                    <input type="checkbox" name="specialrequest[]" value="I'd like a room with view" tabindex=-1> {{trans('book.six')}}
                </td>
            </tr>
            </tbody>
        </table>
        <br/>
        <input class="form-control" accept="[a-zA-Z]+" maxlength="1000" style="border:1px solid #e7eaec; margin-bottom:0px; text-align:left; color:#767676;" type="text" placeholder="{{trans('book.requestoptional')}}" name="specialrequest[]" tabindex=-1>
    </div>
</div>
@endif