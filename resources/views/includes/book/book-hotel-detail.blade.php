<div class="hotel-detail-box">
    <div class="hotel-detail-top border-box">
        {{--1: hotel name and star rating--}}
        <p id="hotel-name">
            {{ $hotel->name}}
            @if($language == 0 && $hotel->name_zh != "")
                ({{$hotel->name_zh}})
            @endif
        </p>
        <p id="hotel-rating">{{ $hotel->rating }}</p>
        {{--2: hotel address--}}
        <p><span class="fa fa-map-marker-alt"></span>&nbsp;
            <span id="hotel-address">
                {{ucwords(strtolower(str_replace(',',' ',$hotel->address))). ', '. ucwords(strtolower($hotel->city)). ', ' . $hotel->zipcode}}
            </span>
        </p>
        <div class="date-box">
            <p>
                <label style="font-size:14px;">{{trans('book.check_in')}}</label>
                <label style="float:right; font-size:14px;" class="control-label">{{$checkin}}</label>
            </p>
            <p>
                <label style="font-size:14px;">{{trans('book.check_out')}}</label>
                <label style="float:right; font-size:14px;" class="control-label">{{$checkout}}</label>
            </p>
        </div>
        <!--3: 取消政策start-->
        <div class="book-cancel-policy">
            <label>{{trans('book.canc')}}:</label>
            <span id="hotel-cancel">
                   <img src="/img/general/book-loading-gif.gif" style="height:55px; margin-top:-25px; margin-bottom:-25px;" />
            </span>
        </div>
        <!--取消政策end-->

        <!--价格明细 start-->
        <div class="price-break-down-box">
            <p class="hotel-rate-title">{{trans('book.soc')}}</p>
            <p class="bottom-line-divider"><span class="pull-left">{{trans('book.rooms')}}</span><span class="pull-right">{{$room['room']['roomCount']}}</span></p><br/>
            <p class="bottom-line-divider"><span class="pull-left">{{trans('book.nights')}}</span><span class="pull-right">{{$room['room']['dayCount']}}</span></p><br/>
            <p class="bottom-line-divider"><span class="pull-left">{{trans('book.roomcost')}}</span><span class="pull-right">{{$room['room']['currency'] == 'USD'? '$' : $room['room']['currency']}}<span class="roomPrice">{{ $singleRoomNightPriceB4Tax }}</span></span></p><br/>
            <p class="bottom-line-divider"><span class="pull-left">{{trans('book.tax')}}</span><span class="pull-right">{{$room['room']['currency'] == 'USD'? '$' : $room['room']['currency']}}<span id="roomTax">{{$room['room']['tax']}}</span></span></p><br/>
            <p class="bottom-line-divider hidden" id="cleanFee">
                <span class="pull-left">{{trans('book.clean_fee')}}</span>
                <span class="pull-right"><span class="clean-fee"></span>
                </span>
            </p>
            <div class="applied-coupon-block bottom-line-divider hide">
                <p class="color-green">@lang('book.coupon_applied_title')</p>
                <p class="coupon-amount color-green"><span>@lang('book.coupon_applied_desc')</span><span id="couponAmount" class="pull-right"></span></p>
                <p class="remove-btn-wrapper"><button class="remove-btn" onclick="removeCoupon()">@lang('book.coupon_remove')</button></p>
            </div>
            <p class="bottom-line-divider">
                <span class="pull-left pay-today">{{trans('book.totalcharge')}}</span>
                <span class="pull-right hotel-total-price">
                    {{$room['room']['currency'] == 'USD'? '$' : $room['room']['currency']}}
                    <span class="totalPrice"></span>
                </span>
            </p>
        </div>

        <!--价格明细 end-->
        <p class="hidden bottom-line-divider" id="HotelDue">
            <span class="pull-left">{{trans('book.totalchargeHotel')}}  ({{trans('book.resortFee')}}):</span>
            <span class="pull-right HotelDue"></span>
        </p>
    </div>

    @if(!$is_b)
    <div class="coupon-block border-box">
        <h1>@lang('book.coupon_title')</h1>
        <div>
            <div class="code-input-wrapper">
                <input type="text" class="code-input" id="codeInput" placeholder="@lang('book.coupon_placeholder')">
            </div>
            <button class="code-apply" onclick="applyCoupon()">@lang('book.coupon_btn')</button>
        </div>
        <p class="error-message">@lang('book.coupon_error_msg')</p>
    </div>
    @endif

    <!--注意事项start-->
    <div class="hotel-detail-bottom note-and-why-us-box border-box">
        <p class="note-title">{{trans('book.fineprint')}}</p>
        <div id="hotel-comment">
            {!! isset($roomDetails['comments'][0]) ? $roomDetails['comments'][0]['comments']: 'None' !!}
        </div>
        @include('includes.book.book-why-us-box')
    </div>
    <!--注意事项end-->
</div>