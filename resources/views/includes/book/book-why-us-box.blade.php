@if(!$is_b)
<div class="why-us-block">
    <p class="why-us-title">{{trans('list.whyUs')}}</p>
    <i class="usitrip_pc_all_img check-icon-red why-us-text-long-icon"></i>
    <div class="why-us-right">
        <p class="why-us-subtitle">{{trans('list.whyUs11')}}</p>
        <p class="why-us-text why-us-text-long">{{trans('list.whyUs12')}}</p>
    </div>
    <br/>
    <i class="usitrip_pc_all_img check-icon-red"></i>
    <div  class="why-us-right">
        <p class="why-us-subtitle">{{trans('list.whyUs21')}}</p>
        <p class="why-us-text">{{trans('list.whyUs22')}}</p>
    </div>
    <br/>
    <i class="usitrip_pc_all_img check-icon-red"></i>
    <div  class="why-us-right">
        <p class="why-us-subtitle">{{trans('list.whyUs31')}}</p>
        <p class="why-us-text">{{trans('list.whyUs32')}}</p>
    </div>
    <br/>
    <i class="usitrip_pc_all_img check-icon-red"></i>
    <div  class="why-us-right">
        <p class="why-us-subtitle">{{trans('list.whyUs41')}}</p>
        <p class="why-us-text">{{trans('list.whyUs42')}}</p>
    </div>
</div>
@endif