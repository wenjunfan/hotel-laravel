{{--
本文件主要定义PHP变量转的 js全局变量
global variable
Int value of language: 0-chinese; 1-Eng $language从AppServiceProvider.php中定义。
String value of language: cn-chinese, en-Eng
--}}
<script>
    var shouldTrack = Number('{{ $should_track && !$is_aa }}'); // to decide weather should track
    var isHotelApp = window.location.hostname.indexOf('{{ config('app.b_host_keyword') }}') < 0;
    var isUsitour = window.location.hostname.indexOf('{{ config('app.c_en_host_keyword') }}') > -1;
    var is117book = !isHotelApp;
    var isAA = window.location.hostname.indexOf('supervacation') > -1;
    var language = Number('{!! $language !!}');

    window.usitrip = {
        csrfToken: '{{ csrf_token() }}',
        basePath: '{{ url('/') }}/'
    };

    //globally inject vue mixin here
    Vue.mixin({
        data: function() {
            return {
                isUsitour: isUsitour,
                is117book: is117book,
                langId: language,
            }
        },
        computed: {
            langStr: function() {
                if (this.langId == 2) {
                    return 'tw';
                } else if (this.langId == 1) {
                    return 'en';
                } else {
                    return 'cn';
                }
            }
        },
    });
</script>
