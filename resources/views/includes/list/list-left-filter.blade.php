<div class="list-left-section" id="leftFiterWrapper">
    <div id="mapAnchor"></div>

    <input type="hidden" value="0" id="locationCheckedLength">
    <input type="hidden" value="0" id="airportCheckedLength">
    <input type="hidden" value="0" id="zoneCheckedLength">
    <input type="hidden" value="0" id="districtCheckedLength">
    <input type="hidden" value="0" id="chainCheckedLength">

    <div class="ibox hotel-filters-wrapper">
        <form onkeydown="if(event.keyCode==13){return false;}" class="form-horizontal" role="form" id="search-form-left" name="search-form-left">
            <div class="if-hit-stick-to-top">
                <div id="ratePrice" class="rate-price-wrapper" name="ratePrice"></div>
                <div id="scenicArea" name="scenicArea"></div>
                <div id="chainBlock" class="chainBlock"></div>
            </div>
            @if(!$is_b)
                <div id="whyUs" class="why-us-block">
                    <div class='why-us'>
                        <div id="counter" class="hidden">{{trans('list.narrowResult')}}    <i class="fa fa-chevron-down"></i></div>
                    </div>
                    <p class="why-us-title">{{trans('list.whyUs')}}</p>
                    <i class="usitrip_pc_all_img check-icon-red"></i>
                    <div class="why-us-right">
                        <p class="why-us-subtitle">{{trans('list.whyUs11')}}</p>
                        <p class="why-us-text">{{trans('list.whyUs12')}}</p>
                    </div>
                    <br/>
                    <i class="usitrip_pc_all_img check-icon-red"></i>
                    <div  class="why-us-right">
                        <p class="why-us-subtitle">{{trans('list.whyUs21')}}</p>
                        <p class="why-us-text">{{trans('list.whyUs22')}}</p>
                    </div>
                    <br/>
                    <i class="usitrip_pc_all_img check-icon-red"></i>
                    <div  class="why-us-right">
                        <p class="why-us-subtitle">{{trans('list.whyUs31')}}</p>
                        <p class="why-us-text">{{trans('list.whyUs32')}}</p>
                    </div>
                    <br/>
                    <i class="usitrip_pc_all_img check-icon-red"></i>
                    <div  class="why-us-right">
                        <p class="why-us-subtitle">{{trans('list.whyUs41')}}</p>
                        <p class="why-us-text">{{trans('list.whyUs42')}}</p>
                    </div>
                </div>
            @endif
        </form>
    </div>
    <!--搜索form end-->
</div>