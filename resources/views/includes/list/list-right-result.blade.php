<div class="list-right-section" id="searchResultWrapper">
    <div class="list-sort-bar" id="hotelSort">
        <ul class="m-nav border-topfocus col-mm-12 white-filter">
            <li class="btn list-btn-clear fadeBorderLeft">{{trans('search.SortResult')}}</li>
            <li id="button-discount" onclick="sort('discount')" class="btn col-mm-3" type="button">
                {{trans('search.Discount')}}
                <span id="icon-discount"></span>
            </li>
            <li id="button-distance" onclick="sort('distance', 'asc')" class="btn col-mm-3" type="button">
                {{trans('search.Distance')}}
                <span id="icon-distance"></span>
            </li>
            <li id="button-minPrice" class="m-nav-down btn-wider" type="button">
                {{trans('search.Price')}}
                <span id="icon-minPrice"></span><a class="arrow-down"><i></i></a>
                <div class="nav-down-box" id="open-sort-price">
                    <p class="open-sort-price-text" onclick="sort('minPrice', 'asc')">
                        {{trans('search.Price') . trans('list.asc')}}
                    </p>
                    <p class="open-sort-price-text" onclick="sort('minPrice', 'desc')">
                        {{trans('search.Price') . trans('list.desc')}}
                    </p>
                </div>
            </li>
            <li id="button-rating" class="m-nav-down btn-wider" type="button">
                {{trans('search.Rate')}}
                <span id="icon-rating"></span><a class="arrow-down"><i></i></a>
                <div class="nav-down-box" id="open-sort-rate">
                    <p class="open-sort-rate-text" onclick="sort('rating', 'asc')">
                        {{trans('search.Rate'). trans('list.asc')}}
                    </p>
                    <p class="open-sort-rate-text" onclick="sort('rating', 'desc')">
                        {{trans('search.Rate') . trans('list.desc')}}
                    </p>
                </div>
            </li>
            <li id="button-name" class="m-nav-down btn-wider" type="button">
                {{trans('search.Name')}}
                <span id="icon-name"></span><a class="arrow-down"><i></i></a>
                <div class="nav-down-box" id="open-sort-name">
                    <p class="open-sort-name-text" onclick="sort('name', 'asc')">
                        {{trans('search.Name'). trans("list.nameAsc")}}
                    </p>
                    <p class="open-sort-name-text" onclick="sort('name', 'desc')">
                        {{trans('search.Name') . trans('list.nameDesc')}}
                    </p>
                </div>
            </li>
        </ul>
    </div>

    <!--hotel result-->
    <div id="hotelResult" class="hotel-items-wrapper fadeInRight hoverimg">
        <div id="searchedHotelBlock"></div>
        <div id="recommendedHotelsBlock"></div>
        <div id='hotel_starter' style="display: none; margin-top:30px;">
            <div id='firstTmpHotel'></div>
        </div>
        <hr id="noMoreHotels" class="hr-text" data-content="No more hotels">
    </div>

    {{--hotel page--}}
    <div class="hotels-pager">
        <span id="searchPage"></span>
        <span class="pull-right" id="hotelPage"></span>
    </div>
</div>