<div id="change-search-toggled">
    <div class="top-center hidden" id="enMarginTop">
        {{--<!--目的地-->--}}
        <div class="col-sm-4" id="destinationNameWrapper">
            {{--these values' change has to reload the page--}}
            <input type="hidden" name="desId" id="desId" value="{{$destinationId or ''}}">

            <label class="label-des">@lang("list.destination")</label><br/>
            <div class="form-group extraTopPadding">
                <input onClick="selectText(this)" type="text" autocomplete="off" class="form-control search-box" name="desName" value="{{$destinationName or ''}}" id="desName" placeholder="@lang('search.DestinationInput')" tip="{{$destinationName or ''}}">
                <input type="hidden" name="scenicId" id="scenicId" value="{{$scenicId or ''}}"> <input type="hidden" name="scenicName" id="scenicName" value="{{$scenicName or ''}}">
                <input type="hidden" name="dayCount" id="dayCount" value="{{$dayCount}}">
                <input type="hidden" name="roomCount" class="roomCount" value="{{$roomCount}}">
                <input type="hidden" name="adultCount1" class="adultCount1" value="{{$adultCount1}}">
                <input type="hidden" name="childCount1" class="childCount1" value="{{ $childCount1 }}">

                @include('includes.home.destination-list')
            </div>
        </div>
        {{--<!--入住日期-->--}}
        <div class="col-sm-2 margin-left-30">
            <label class="label-checkin">@lang("list.checkin")</label> <br/>
            <div class="form-group extraTopPadding">
                <div class="input-group date">
                    <input id="checkin" name="checkin" type="text" class="form-control search-box datepicker" onchange="updateCheckin()" value="" placeholder="@lang('search.Checkin')" autocomplete="off">
                </div>
            </div>
        </div>
        {{--<!--退房日期-->--}}
        <div class="col-sm-2 margin-left-30">
            <label class="label-checkin">@lang("list.checkout")</label><br/>
            <div class="form-group extraTopPadding">
                <div class="input-group date">
                    <input id="checkout" name="checkout" type="text" class="form-control search-box" onchange="updateCheckout()" value="" placeholder="@lang('search.Checkout')" autocomplete="off">
                </div>
            </div>
        </div>
        {{--<!--房间人数-->--}}
        <div class="col-sm-3 margin-left-30 relative" id="searchform-rightBlock">
            @lang("list.roomGuest")<br/>
            <div class="extraTopPadding">
                <div class="search-box" id="room-guest-toggle">
                    <span id="tRoom">{{$roomCount}}</span>
                    <span id="tRoom_text">@lang("list.room"),</span>
                    <span id="tAdult">{{$adultCount1}}</span>
                    <span id="tAdult_text">@lang("list.adults"),</span>
                    <span id="tChild">{{$childCount1}}</span>
                    <span id="tChild_text">@lang("list.child")</span>
                </div>
            </div>
            {{--Dialog 选择房间数，成人数，儿童数量--}}
            <div class="dialog" id="searchDialog" style="/* display: none; */">
                <div class="number-spinner-container">
                    {{--new style--}}
                    {{--room count--}}
                    <div class="form-group">
                        <label class="number-label" style="width:30px; float:left">@lang('home.search_t_count_room2')</label>
                        <div class="input-group number-spinner" style="width:90px; float:left">
                            <span class="input-group-btn">
                                <a class="btn btn-subtract" id="roomCountSubtract" data-dir="dwn">-</a>
                            </span> <input type="text" disabled name="roomCount" class="form-control text-center input-number roomCount" value="{{$roomCount}}" max=8 min=1> <span class="input-group-btn">
                                <a class="btn btn-add" id="roomCountAdd" data-dir="up">+</a>
                            </span>
                        </div>
                    </div>
                    {{--adult count--}}
                    <div class="form-group">
                        <label class="number-label" style="width:30px; float:left">@lang('home.search_t_count_adult')</label>
                        <div class="input-group number-spinner" style="width:90px; float:left">
                            <span class="input-group-btn">
                                <a class="btn btn-subtract" id="adultCountSubtract" data-dir="dwn">-</a>
                            </span> <input type="text" disabled name="adultCount1" class="form-control text-center input-number adultCount1" value="{{$adultCount1}}" max=4 min=1> <span class="input-group-btn">
                                <a class="btn btn-add" id="adultCountAdd" data-dir="up">+</a>
                            </span>
                        </div>
                    </div>
                    {{--child count--}}
                    <div class="form-group">
                        <label class="number-label" style="width:30px; float:left">@lang('home.search_t_count_child')</label>
                        <div class="input-group number-spinner" style="width:90px; float:left">
                            <span class="input-group-btn">
                                <a class="btn btn-subtract" id="childCountSubtract" data-dir="dwn">-</a>
                            </span> <input type="text" disabled name="childCount1" class="form-control text-center input-number childCount1" value="{{$childCount1}}" max=4 min=0> <span class="input-group-btn">
                                <a class="btn btn-add" id="childCountAdd" data-dir="up">+</a>
                            </span>
                        </div>
                    </div>
                    <div class="form-group child-age-wrapper hide" id="childAgeWrapper">
                        <label class="number-label child-age-label">@lang('home.childAge')</label>
                        <div id="childAgeContent">
                            <select name="childAge[]" class="child-age-select">
                                @for($i=0; $i<18; $i++)
                                    <option value="{{ $i }}" @if($i === 12) selected @endif>{{ $i === 0 ? '<1' : $i }} @lang('home.yearsOld')</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="col-sm-12">
            <button type="button" id="searchButton" class="pull-left list-btn-top ladda-button ladda-button-search" data-style="expand-right" onclick="showResultPage()">@lang('list.search')</button>
            <p id="cancel-re-search" class="pull-right usitrip-pure font-18 change-search top-search-text">@lang("list.cancel")</p>
        </div>
    </div>
</div>
