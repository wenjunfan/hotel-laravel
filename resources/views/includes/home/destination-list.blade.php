<!--地址-->
@if	($language == 1)
	<style>
		.dft_title li {
			width: 60px;
			height: 30px;
			text-align: center;
			color: rgb(51, 51, 51);
			line-height: 30px;
			font-size: 14px;
			margin-right: 10px;
			float: left;
		}
	</style>
@endif
<div class="dest_wrapper">
	<div class="dft_dest" style="display:  none;">
		<ul class="dft_title">
			<li class="on">{{trans('list.webPopular')}}</li>
			<li class="">{{trans('search.Americas')}}</li>
			<li class="">{{trans('search.Europe')}}</li>
			{{--<li class="">{{trans('search.Oceania')}}</li>--}}
			{{--<li class="">{{trans('search.Asia')}}</li>--}}
		</ul>
		<div class="dft_list">
			<ul class="wq_clearfix">
				<li data-des-id="D{!! config('city-ids')['1001'] !!}" data-cn="{{trans('search.NewYork')}}">
					<a href="javascript:void(0);" title="{{trans('search.NewYork')}}">{{trans('search.NewYork')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1002'] !!}" data-cn="{{trans('search.LosAngeles')}}">
					<a href="javascript:void(0);" title="{{trans('search.LosAngeles')}}">{{trans('search.LosAngeles')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1006'] !!}" data-cn="{{trans('search.Chicago')}}">
					<a href="javascript:void(0);" title="{{trans('search.Chicago')}}">{{trans('search.Chicago')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1013'] !!}" data-cn="{{trans('search.Toronto')}}">
					<a href="javascript:void(0);" title="{{trans('search.Toronto')}}">{{trans('search.Toronto')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3001'] !!}" data-cn="{{trans('search.Sydney')}}">
					<a href="javascript:void(0);" title="{{trans('search.Sydney')}}">{{trans('search.Sydney')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2001'] !!}" data-cn="{{trans('search.London')}}">
					<a href="javascript:void(0);" title="{{trans('search.London')}}">{{trans('search.London')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2006'] !!}" data-cn="{{trans('search.Berlin')}}">
					<a href="javascript:void(0);" title="{{trans('search.Berlin')}}">{{trans('search.Berlin')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2009'] !!}" data-cn="{{trans('search.Rome')}}">
					<a href="javascript:void(0);" title="{{trans('search.Rome')}}">{{trans('search.Rome')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2010'] !!}" data-cn="{{trans('search.Venice')}}">
					<a href="javascript:void(0);" title="{{trans('search.Venice')}}">{{trans('search.Venice')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2013'] !!}" data-cn="{{trans('search.Madrid')}}">
					<a href="javascript:void(0);" title="{{trans('search.Madrid')}}">{{trans('search.Madrid')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2018'] !!}" data-cn="{{trans('search.Geneva')}}">
					<a href="javascript:void(0);" title="{{trans('search.Geneva')}}">{{trans('search.Geneva')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1004'] !!}" data-cn="{{trans('search.LasVegas')}}">
					<a href="javascript:void(0);" title="{{trans('search.LasVegas')}}">{{trans('search.LasVegas')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2015'] !!}" data-cn="{{trans('search.Amsterdam')}}">
					<a href="javascript:void(0);" title="{{trans('search.Amsterdam')}}">{{trans('search.Amsterdam')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2019'] !!}" data-cn="{{trans('search.Luxembourg')}}">
					<a href="javascript:void(0);" title="{{trans('search.Luxembourg')}}">{{trans('search.Luxembourg')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1016'] !!}" data-cn="{{trans('search.Cancun')}}">
					<a href="javascript:void(0);" title="{{trans('search.Cancun')}}">{{trans('search.Cancun')}}</a>
				</li>
			</ul>
			<ul class="wq_clearfix" style="display:none;">
				<li data-des-id="D{!! config('city-ids')['1001'] !!}" data-cn="{{trans('search.NewYork')}}">
					<a href="javascript:void(0);" title="{{trans('search.NewYork')}}">{{trans('search.NewYork')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1002'] !!}" data-cn="{{trans('search.LosAngeles')}}">
					<a href="javascript:void(0);" title="{{trans('search.LosAngeles')}}">{{trans('search.LosAngeles')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1003'] !!}" data-cn="{{trans('search.SanFrancisco')}}">
					<a href="javascript:void(0);" title="{{trans('search.SanFrancisco')}}">{{trans('search.SanFrancisco')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1004'] !!}" data-cn="{{trans('search.LasVegas')}}">
					<a href="javascript:void(0);" title="{{trans('search.LasVegas')}}">{{trans('search.LasVegas')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1005'] !!}" data-cn="{{trans('search.Boston')}}">
					<a href="javascript:void(0);" title="{{trans('search.Boston')}}">{{trans('search.Boston')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1006'] !!}" data-cn="{{trans('search.Chicago')}}">
					<a href="javascript:void(0);" title="{{trans('search.Chicago')}}">{{trans('search.Chicago')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1007'] !!}" data-cn="{{trans('search.Washington')}}">
					<a href="javascript:void(0);" title="{{trans('search.Washington')}}">{{trans('search.Washington')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1008'] !!}" data-cn="{{trans('search.Honolulu')}}">
					<a href="javascript:void(0);" title="{{trans('search.Honolulu')}}">{{trans('search.Honolulu')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1009'] !!}" data-cn="{{trans('search.Orlando')}}">
					<a href="javascript:void(0);" title="{{trans('search.Orlando')}}">{{trans('search.Orlando')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1010'] !!}" data-cn="{{trans('search.Miami')}}">
					<a href="javascript:void(0);" title="{{trans('search.Miami')}}">{{trans('search.Miami')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1011'] !!}" data-cn="{{trans('search.Seattle')}}">
					<a href="javascript:void(0);" title="{{trans('search.Seattle')}}">{{trans('search.Seattle')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1012'] !!}" data-cn="{{trans('search.Vancouver')}}">
					<a href="javascript:void(0);" title="{{trans('search.Vancouver')}}">{{trans('search.Vancouver')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1013'] !!}" data-cn="{{trans('search.Toronto')}}">
					<a href="javascript:void(0);" title="{{trans('search.Toronto')}}">{{trans('search.Toronto')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1016'] !!}" data-cn="{{trans('search.Cancun')}}">
					<a href="javascript:void(0);" title="{{trans('search.Cancun')}}">{{trans('search.Cancun')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['1015'] !!}" data-cn="{{trans('search.Montreal')}}">
					<a href="javascript:void(0);" title="{{trans('search.Montreal')}}">{{trans('search.Montreal')}}</a>
				</li>
			</ul>
			<ul class="wq_clearfix" style="display:none;">
				<li data-des-id="D{!! config('city-ids')['2001'] !!}" data-cn="{{trans('search.London')}}">
					<a href="javascript:void(0);" title="{{trans('search.London')}}">{{trans('search.London')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2002'] !!}" data-cn="{{trans('search.Edinburgh')}}">
					<a href="javascript:void(0);" title="{{trans('search.Edinburgh')}}">{{trans('search.Edinburgh')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2003'] !!}" data-cn="{{trans('search.Paris')}}">
					<a href="javascript:void(0);" title="{{trans('search.Paris')}}">{{trans('search.Paris')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2004'] !!}" data-cn="{{trans('search.Nice')}}">
					<a href="javascript:void(0);" title="{{trans('search.Nice')}}">{{trans('search.Nice')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2005'] !!}" data-cn="{{trans('search.Lyon')}}">
					<a href="javascript:void(0);" title="{{trans('search.Lyon')}}">{{trans('search.Lyon')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2006'] !!}" data-cn="{{trans('search.Berlin')}}">
					<a href="javascript:void(0);" title="{{trans('search.Berlin')}}">{{trans('search.Berlin')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2007'] !!}" data-cn="{{trans('search.Munich')}}">
					<a href="javascript:void(0);" title="{{trans('search.Munich')}}">{{trans('search.Munich')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2008'] !!}" data-cn="{{trans('search.Frankfurt')}}">
					<a href="javascript:void(0);" title="{{trans('search.Frankfurt')}}">{{trans('search.Frankfurt')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2009'] !!}" data-cn="{{trans('search.Rome')}}">
					<a href="javascript:void(0);" title="{{trans('search.Rome')}}">{{trans('search.Rome')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2010'] !!}" data-cn="{{trans('search.Venice')}}">
					<a href="javascript:void(0);" title="{{trans('search.Venice')}}">{{trans('search.Venice')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2011'] !!}" data-cn="{{trans('search.Florence')}}">
					<a href="javascript:void(0);" title="{{trans('search.Florence')}}">{{trans('search.Florence')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2012'] !!}" data-cn="{{trans('search.Barcelona')}}">
					<a href="javascript:void(0);" title="{{trans('search.Barcelona')}}">{{trans('search.Barcelona')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2013'] !!}" data-cn="{{trans('search.Madrid')}}">
					<a href="javascript:void(0);" title="{{trans('search.Madrid')}}">{{trans('search.Madrid')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2014'] !!}" data-cn="{{trans('search.Zurich')}}">
					<a href="javascript:void(0);" title="{{trans('search.Zurich')}}">{{trans('search.Zurich')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2015'] !!}" data-cn="{{trans('search.Amsterdam')}}">
					<a href="javascript:void(0);" title="{{trans('search.Amsterdam')}}">{{trans('search.Amsterdam')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2016'] !!}" data-cn="{{trans('search.Athens')}}">
					<a href="javascript:void(0);" title="{{trans('search.Athens')}}">{{trans('search.Athens')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['2017'] !!}" data-cn="{{trans('search.Milan')}}">
					<a href="javascript:void(0);" title="{{trans('search.Milan')}}">{{trans('search.Milan')}}</a>
				</li>
			</ul>
			<ul class="wq_clearfix" style="display:none;">
				<li data-des-id="D{!! config('city-ids')['3001'] !!}" data-cn="{{trans('search.Sydney')}}">
					<a href="javascript:void(0);" title="{{trans('search.Sydney')}}">{{trans('search.Sydney')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3002'] !!}" data-cn="{{trans('search.Melbourne')}}">
					<a href="javascript:void(0);" title="{{trans('search.Melbourne')}}">{{trans('search.Melbourne')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3003'] !!}" data-cn="{{trans('search.GoldCoast')}}">
					<a href="javascript:void(0);" title="{{trans('search.GoldCoast')}}">{{trans('search.GoldCoast')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3004'] !!}" data-cn="{{trans('search.Keynes')}}">
					<a href="javascript:void(0);" title="{{trans('search.Keynes')}}">{{trans('search.Keynes')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3005'] !!}" data-cn="{{trans('search.Brisbane')}}">
					<a href="javascript:void(0);" title="{{trans('search.Brisbane')}}">{{trans('search.Brisbane')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3006'] !!}" data-cn="{{trans('search.Adelaide')}}">
					<a href="javascript:void(0);" title="{{trans('search.Adelaide')}}">{{trans('search.Adelaide')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3007'] !!}" data-cn="{{trans('search.Perth')}}">
					<a href="javascript:void(0);" title="{{trans('search.Perth')}}">{{trans('search.Perth')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3008'] !!}" data-cn="{{trans('search.Canberra')}}">
					<a href="javascript:void(0);" title="{{trans('search.Canberra')}}">{{trans('search.Canberra')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3009'] !!}" data-cn="{{trans('search.GreatBarrierReef')}}">
					<a href="javascript:void(0);" title="{{trans('search.GreatBarrierReef')}}">{{trans('search.GreatBarrierReef')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3010'] !!}" data-cn="{{trans('search.Oakland')}}">
					<a href="javascript:void(0);" title="{{trans('search.Oakland')}}">{{trans('search.Oakland')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3011'] !!}" data-cn="{{trans('search.Queenstown')}}">
					<a href="javascript:void(0);" title="{{trans('search.Queenstown')}}">{{trans('search.Queenstown')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3012'] !!}" data-cn="{{trans('search.Wellington')}}">
					<a href="javascript:void(0);" title="{{trans('search.Wellington')}}">{{trans('search.Wellington')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3013'] !!}" data-cn="{{trans('search.Christchurch')}}">
					<a href="javascript:void(0);" title="{{trans('search.Christchurch')}}">{{trans('search.Christchurch')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3014'] !!}" data-cn="{{trans('search.PhillipIsland')}}">
					<a href="javascript:void(0);" title="{{trans('search.PhillipIsland')}}">{{trans('search.PhillipIsland')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['3015'] !!}" data-cn="{{trans('search.Dublin')}}">
					<a href="javascript:void(0);" title="{{trans('search.Dublin')}}">{{trans('search.Dublin')}}</a>
				</li>
			</ul>
			<ul class="wq_clearfix" style="display:none;">
				<li data-des-id="D{!! config('city-ids')['4001'] !!}" data-cn="{{trans('search.Singapore')}}">
					<a href="javascript:void(0);" title="{{trans('search.Singapore')}}">{{trans('search.Singapore')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4002'] !!}" data-cn="{{trans('search.Seoul')}}">
					<a href="javascript:void(0);" title="{{trans('search.Seoul')}}">{{trans('search.Seoul')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4003'] !!}" data-cn="{{trans('search.JejuIsland')}}">
					<a href="javascript:void(0);" title="{{trans('search.JejuIsland')}}">{{trans('search.JejuIsland')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4004'] !!}" data-cn="{{trans('search.Bali')}}">
					<a href="javascript:void(0);" title="{{trans('search.Bali')}}">{{trans('search.Bali')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4005'] !!}" data-cn="{{trans('search.Bangkok')}}">
					<a href="javascript:void(0);" title="{{trans('search.Bangkok')}}">{{trans('search.Bangkok')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4006'] !!}" data-cn="{{trans('search.Phuket')}}">
					<a href="javascript:void(0);" title="{{trans('search.Phuket')}}">{{trans('search.Phuket')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4007'] !!}" data-cn="{{trans('search.ChiangMai')}}">
					<a href="javascript:void(0);" title="{{trans('search.ChiangMai')}}">{{trans('search.ChiangMai')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4008'] !!}" data-cn="{{trans('search.Pattaya')}}">
					<a href="javascript:void(0);" title="{{trans('search.Pattaya')}}">{{trans('search.Pattaya')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4009'] !!}" data-cn="{{trans('search.SuMei')}}">
					<a href="javascript:void(0);" title="{{trans('search.SuMei')}}">{{trans('search.SuMei')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4010'] !!}" data-cn="{{trans('search.KualaLumpur')}}">
					<a href="javascript:void(0);" title="{{trans('search.KualaLumpur')}}">{{trans('search.KualaLumpur')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4011'] !!}" data-cn="{{trans('search.Langkawi')}}">
					<a href="javascript:void(0);" title="{{trans('search.Langkawi')}}">{{trans('search.Langkawi')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4012'] !!}" data-cn="{{trans('search.Tokyo')}}">
					<a href="javascript:void(0);" title="{{trans('search.Tokyo')}}">{{trans('search.Tokyo')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4013'] !!}" data-cn="{{trans('search.Osaka')}}">
					<a href="javascript:void(0);" title="{{trans('search.Osaka')}}">{{trans('search.Osaka')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4014'] !!}" data-cn="{{trans('search.Beijing')}}">
					<a href="javascript:void(0);" title="{{trans('search.Beijing')}}">{{trans('search.Beijing')}}</a>
				</li>
				<li data-des-id="D{!! config('city-ids')['4015'] !!}" data-cn="{{trans('search.HongKong')}}">
					<a href="javascript:void(0);" title="{{trans('search.HongKong')}}">{{trans('search.HongKong')}}</a>
				</li>
			</ul>
		</div>
		<p name="tip" class="dest_tip">{{trans('list.InputSearch')}}</p>
	</div>
	<div class="dest_result" style="display: none;">
		<ul name="location-list" class="dest_list_ul"></ul>
		<p name="msg" class="dest_msg">{{trans('list.noMatch')}}</p>
	</div>
</div>
