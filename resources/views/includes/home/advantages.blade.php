{{--分销域名判断--}}
<?php
if($is_aa){
    $usitrip = '//www.supervacation.net';
}else{
    $usitrip = '//www.usitrip.com';
}
?>

<div class="h-super-wrap relative" id="h-super-wrap-id">
    @if($language == 0) <a href="{{$usitrip}}/web_action/Hotel/" target="_blank" class="h-super-wrap-cover"> @endif
        <ul class="h-super-box">
            <li>
                <span class="hotel-icon h-super-icon1"></span>
                <div class="super-txt">
                    <p>@lang('home.adv_1_1')</p><em>@lang('home.adv_1_2')</em>
                </div>
            </li>
            <li>
                <span class="hotel-icon h-super-icon2"></span>
                <div class="super-txt">
                    <p>@lang('home.adv_2_1')</p><em>@lang('home.adv_2_2')</em></div>
            </li>
            <li>
                <span class="hotel-icon h-super-icon3"></span>
                <div class="super-txt">
                    <p>@lang('home.adv_3_1')</p><em>@lang('home.adv_3_2')</em>
                </div>
            </li>
            <li>
                <span class="hotel-icon h-super-icon4"></span>
                <div class="super-txt">
                    <p>@lang('home.adv_4_1')</p><em>@lang('home.adv_4_2')</em>
                </div>
            </li>
            <li>
                <span class="hotel-icon h-super-icon5"></span>
                <div class="super-txt">
                    <p>@lang('home.adv_5_1')</p><em>@lang('home.adv_5_2')</em>
                </div>
            </li>
            <li style="margin-right:0;">
                <span class="hotel-icon h-super-icon6"></span>
                <div class="super-txt">
                    <p>@lang('home.adv_6_1')</p><em>@lang('home.adv_6_2')</em>
                </div>
            </li>
        </ul>
    @if($language == 0) </a> @endif
</div>


 

