{{--首页 热门目的地推荐--}}

{{--title--}}
<div class="ad_title clear m-t-100">
    @lang('home.adv2_title')
</div>

<div class="h-recom-wrap">
    {{--热门城市 导航 （按大洲）--}}
    <div class="h-recom-nav clear">
            <ul class="h-recom-tab js-remen fl @if($language==1) h-recom-tab-en @endif ">
                <li><a href="javascript:void(0);" class="active">@lang('home.Americas')</a></li>
                <li><a href="javascript:void(0);">@lang('home.Europe')</a></li>
                <li><a href="javascript:void(0);">@lang('home.Oceania')</a></li>
                <li><a href="javascript:void(0);">@lang('home.Asia')</a></li>
            </ul>
    </div>
    {{--1.美洲 OK--}}
    <div class="city-recom-wrap clear">
        {{--第一排 左边的--}}
        <div class="city-recom-right m-b-12 fl">
            {{--纽约--}}
            <div class="h-recom-cont-box div-city-lg mt100 mb18">
                <a target="_self" href="{{ url('/list/D46858.html') }}" class="lg @if($language != 0) en @endif america-newyork america">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>纽约</p>
                            <span><em>New York</em></span>
                        @else
                            <p>New York</p>

                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
        {{--第一排 右边的--}}
        <div class="city-recom-left m-b-12 fr">
            {{--洛杉矶--}}
            <div class="h-recom-cont-box div-city-small mt120 m-b-12">
                <a target="_self" href="{{ url('/list/D46852.html') }}" class="@if($language != 0) en @endif america-la america">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>洛杉矶</p>
                            <span><em>Los Angeles</em></span>
                        @else
                            <p>Los Angeles</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
            {{--旧金山--}}
            <div class="h-recom-cont-box div-city-small mt100">
                <a target="_self" href="{{ url('/list/D46867.html') }}" class="@if($language != 0) en @endif america-sf america">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>旧金山</p>
                            <span><em>San Francisco</em></span>
                        @else
                            <p>San Francisco</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
        {{--第二排 左边的--}}
        <div class="city-recom-left fl">
            {{--坎昆--}}
            <div class="h-recom-cont-box div-city-small m-b-12 mt100">
                <a target="_self" href="{{ url('/list/D60176.html') }}" class="@if($language != 0) en @endif america-cancun america">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>坎昆</p>
                            <span><em>Cancun</em></span>
                        @else
                            <p>Cancun</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
            {{--芝加哥--}}
            <div class="h-recom-cont-box div-city-small mt100">
                <a target="_self" href="{{ url('/list/D46840.html') }}" class="@if($language != 0) en @endif america-chicago america">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>芝加哥</p>
                            <span><em>Chicago</em></span>
                        @else
                            <p>Chicago</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
        {{--第二排 右边的--}}
        <div class="city-recom-right fr">
            {{--拉斯维加斯--}}
            <div class="h-recom-cont-box div-city-lg mt100 mb18">
                <a target="_self" href="{{ url('/list/D46851.html') }}" class="lg @if($language != 0) en @endif america-las america">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>拉斯维加斯</p>
                            <span><em>Las Vegas</em></span>
                        @else
                            <p>Las Vegas</p>

                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    {{--2.欧洲 OK--}}
    <div class="city-recom-wrap clear" style="display:none;">
        {{--第一排 左边的--}}
        <div class="city-recom-right m-b-12 fl">
            {{--伦敦--}}
            <div class="h-recom-cont-box div-city-lg mt100 mb18">
                <a target="_self" href="{{ url('/list/D72211.html') }}" class="lg @if($language != 0) en @endif europe-london europe">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>伦敦</p>
                            <span><em>London</em></span>
                        @else
                            <p>London</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
        {{--第一排 右边的--}}
        <div class="city-recom-left m-b-12 fr">
            {{--雅典--}}
            <div class="h-recom-cont-box div-city-small mt120 m-b-12">
                <a target="_self" href="{{ url('/list/D95395.html') }}" class="@if($language != 0) en @endif europe-athen europe">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>雅典</p>
                            <span><em>Athen</em></span>
                        @else
                            <p>Athen</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
            {{--罗马--}}
            <div class="h-recom-cont-box div-city-small mt100">
                <a target="_self" href="{{ url('/list/D138862.html') }}" class="@if($language != 0) en @endif europe-rome europe">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>罗马</p>
                            <span><em>Rome</em></span>
                        @else
                            <p>Rome</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
        {{--第二排 左边的--}}
        <div class="city-recom-left fl">
            {{--米兰--}}
            <div class="h-recom-cont-box div-city-small m-b-12 mt100">
                <a target="_self" href="{{ url('/list/D137584.html') }}" class="@if($language != 0) en @endif europe-milan europe">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>米兰</p>
                            <span><em>Milan</em></span>
                        @else
                            <p>Milan</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
            {{--法兰克福 德国--}}
            <div class="h-recom-cont-box div-city-small mt100">
                <a target="_self" href="{{ url('/list/D119430.html') }}" class="@if($language != 0) en @endif europe-frankurt europe">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>法兰克福</p>
                            <span><em>Frankfurt</em></span>
                        @else
                            <p>Frankfurt</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
        {{--第二排 右边的--}}
        <div class="city-recom-right fr">
            {{--巴黎--}}
            <div class="h-recom-cont-box div-city-lg mt100 mb18">
                <a target="_self" href="{{ url('/list/D77580.html') }}" class="lg @if($language != 0) en @endif europe-paris europe">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>巴黎</p>
                            <span><em>Paris</em></span>
                        @else
                            <p>Paris</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    {{--3.大洋洲 oceania OK--}}
    <div class="city-recom-wrap clear" style="display:none;">
        {{--第一排 左边的--}}
        <div class="city-recom-right m-b-12 fl">
            {{--悉尼--}}
            <div class="h-recom-cont-box div-city-lg mt100 mb18">
                <a target="_self" href="{{ url('/list/D67498.html') }}" class="lg @if($language != 0) en @endif oceania-sydney oceania">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>悉尼</p>
                            <span><em>Sydney</em></span>
                        @else
                            <p>Sydney</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
        {{--第一排 右边的--}}
        <div class="city-recom-left m-b-12 fr">
            {{--奥克兰--}}
            <div class="h-recom-cont-box div-city-small mt120 m-b-12">
                <a target="_self" href="{{ url('/list/D96331.html') }}" class="@if($language != 0) en @endif oceania-auckland oceania">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>奥克兰</p>
                            <span><em>Auckland</em></span>
                        @else
                            <p>Auckland</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
            {{--黄金海岸--}}
            <div class="h-recom-cont-box div-city-small mt100">
                <a target="_self" href="{{ url('/list/D67573.html') }}" class="@if($language != 0) en @endif oceania-goldcoast oceania">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>黄金海岸</p>
                            <span><em>Gold Coast</em></span>
                        @else
                            <p>Gold Coast</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
        {{--第二排 左边的--}}
        <div class="city-recom-left fl">
            {{--布里斯班--}}
            <div class="h-recom-cont-box div-city-small m-b-12 mt100">
                <a target="_self" href="{{ url('/list/D67570.html') }}" class="@if($language != 0) en @endif oceania-brisbane oceania">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>布里斯班</p>
                            <span><em>Brisbane</em></span>
                        @else
                            <p>Brisbane</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
            {{--凯恩斯--}}
            <div class="h-recom-cont-box div-city-small mt100">
                <a target="_self" href="{{ url('/list/D67033.html') }}" class="@if($language != 0) en @endif oceania-cairns oceania">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>凯恩斯</p>
                            <span><em>Cairns</em></span>
                        @else
                            <p>Cairns</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
        {{--第二排 右边的--}}
        <div class="city-recom-right fr">
            {{--墨尔本--}}
            <div class="h-recom-cont-box div-city-lg mt100 mb18">
                <a target="_self" href="{{ url('/list/D67497.html') }}" class="lg @if($language != 0) en @endif oceania-melbourne oceania">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>墨尔本</p>
                            <span><em>Melbourne</em></span>
                        @else
                            <p>Melbourne</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    {{--4.亚洲 asia--}}
    <div class="city-recom-wrap clear" style="display:none;">
        {{--第一排 左边的--}}
        <div class="city-recom-right m-b-12 fl">
            {{--新加坡--}}
            <div class="h-recom-cont-box div-city-lg mt100 mb18">
                <a target="_self" href="{{ url('/list/D95045.html') }}" class="lg @if($language != 0) en @endif asia-singapore asia">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>新加坡</p>
                            <span><em>Singapore</em></span>
                        @else
                            <p>Singapore</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
        {{--第一排 右边的--}}
        <div class="city-recom-left m-b-12 fr">
            {{--曼谷--}}
            <div class="h-recom-cont-box div-city-small mt120 m-b-12">
                <a target="_self" href="{{ url('/list/D105378.html') }}" class="@if($language != 0) en @endif asia-bangkok asia">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>曼谷</p>
                            <span><em>Bangkok</em></span>
                        @else
                            <p>Bangkok</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
            {{--普吉岛--}}
            <div class="h-recom-cont-box div-city-small mt100">
                <a target="_self" href="{{ url('/list/D105421.html') }}" class="@if($language != 0) en @endif asia-phuket asia">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>普吉岛</p>
                            <span><em>Phuket</em></span>
                        @else
                            <p>Phuket</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
        {{--第二排 左边的--}}
        <div class="city-recom-left fl">
            {{--香港--}}
            <div class="h-recom-cont-box div-city-small m-b-12 mt100">
                <a target="_self" href="{{ url('/list/D94435.html') }}" class="@if($language != 0) en @endif asia-hongkong asia">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>香港</p>
                            <span><em>Hong Kong</em></span>
                        @else
                            <p>Hong Kong</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
            {{--东京--}}
            <div class="h-recom-cont-box div-city-small mt100">
                <a target="_self" href="{{ url('/list/D103569.html') }}" class="@if($language != 0) en @endif asia-tokyo asia">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>东京</p>
                            <span><em>Tokyo</em></span>
                        @else
                            <p>Tokyo</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
        {{--第二排 右边的--}}
        <div class="city-recom-right fr">
            {{--首尔--}}
            <div class="h-recom-cont-box div-city-lg mt100 mb18">
                <a target="_self" href="{{ url('/list/D147072.html') }}" class="lg @if($language != 0) en @endif asia-seoul asia">
                    <div class="h-recom-cont-box-shade"></div>
                    <div class="h-recom-cont-box-tit">
                        @if($language == 0)
                            <p>首尔</p>
                            <span><em>Seoul</em></span>
                        @else
                            <p>Seoul</p>
                        @endif
                        <div class="h-go-look">@lang('home.adv3_btn')</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
