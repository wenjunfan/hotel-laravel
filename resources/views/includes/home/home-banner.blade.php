<?php
if($is_aa){
    $usitrip = '//www.supervacation.net';
}else{
    $usitrip = '//www.usitrip.com';
}
?>

{{--home.blade.php--}}
<div class="h-banner">
    <div class="h-banner-content">
        <form class="h-search-form relative" id="mainSearchForm" onkeydown="if(event.keyCode===13){return false;}">
            {{--title: 酒店预订--}}
            <h1 class="h-search-title clear">
                <span class="h-search-title-icon"><i class="fa fa-search"></i></span> <span class="h-search-title-text">@lang('home.HotelBooking')</span>
            </h1>
            {{--inputs--}}
            <ul class="h-filt-bd-list row">
                <li class="col-md-12 col-lg-12 search-t-ad">
                    <h2 class="search-t-ad-pad">@lang('home.search_t_ad')</h2>
                </li>
                {{--目的地--}}
                <li class="h-filt-bd-seek-box col-md-12 col-lg-12">
                    <input type="hidden" name="desId" id="mainDestinationId" class="destinationId" value="{{$desId or ''}}">
                    <input type="hidden" name="scenicId" id="mainScenicId" value="">
                    <input type="hidden" name="roomCount" id="topRoomCount" value="1">
                    <input type="hidden" name="adultCount1" id="topAdultCount1" value="2">
                    <input type="hidden" name="childCount1" id="topChildCount1" value="0">

                    <label class="label-des">@lang('home.search_t_des')</label>
                    <input type="text" name="desName" id="mainDestinationName" class="dest-index destinationName" value="{{$desName or ''}}" autocomplete="off" onClick="selectText(this)" placeholder="@lang('home.search_t_des_all')" tip="">

                    <input type="hidden" name="scenicName" id="mainScenicName" value="">
                    <input id="inputDestinationId" value="{{$desId}}" class="hide">

                    {{--热门城市下拉框--}}
                    @include('includes.home.destination-list')
                </li>
                {{--入住日期--}}
                <li class="pull-left search-box-fix-width">
                    <label class="label-checkin">@lang('home.checkin')</label>
                    <input id="mainCheckin" name="checkin" type="text" class="date date_in checkin" placeholder="checkin date" onchange="updateCheckin('main')" readonly="" value="">
                </li>
                {{--退房日期--}}
                <li class="pull-left search-box-fix-width">
                    <label class="label-checkout">@lang('home.checkout')</label>
                    <input id="mainCheckout" name="checkout" type="text" class="date date_out checkout" placeholder="checkout date" onchange="updateCheckout('main')" readonly="" value="">
                </li>
                {{--房间人数--}}
                <li class="pull-left search-box-fix-width">
                    <label>@lang('home.room_people')</label>
                    <div class="person_select_wrapper">
                        <div class="person_desc @if($language==1) person_desc_en @endif " id="searchDesc" style="pointer-events:all">
                            <span id="tRoom" style="pointer-events:none">1</span>
                            <span id="tRoom_text" style="pointer-events:none">@lang('home.roomNum'),</span> <span id="tAdult" style="pointer-events:none">2</span>
                            <span id="tAdult_text" style="pointer-events:none">@lang('home.adult'),</span> <span id="tChild" style="pointer-events:none">0</span>
                            <span id="tChild_text" style="pointer-events:none">@lang('home.children')</span>
                        </div>
                        {{--Dialog 选择房间数，成人数，儿童数量--}}
                        <div class="dialog" id="searchDialog" style="display: none;">
                            <div class="search-dialog-div">
                                {{--new style--}}
                                {{--room count--}}
                                <div class="form-group">
                                    <label class="control-label col-md-8 number-label">@lang('home.search_t_count_room2')</label>
                                    <div class="input-group number-spinner col-md-4">
                                        <span class="input-group-btn">
                                            <a class="btn btn-subtract disabled-btn" disabled id="roomCountSubtract" data-dir="dwn">-</a>
                                        </span> <input type="text" disabled id="mainRoomCount" name="roomCountInp" class="form-control text-center input-number" value="1" max=8 min=1> <span class="input-group-btn">
                                            <a class="btn btn-add" id="roomCountAdd" data-dir="up">+</a>
                                        </span>
                                    </div>
                                </div>
                                {{--adult count--}}
                                <div class="form-group">
                                    <label class="control-label col-md-8 number-label">@lang('home.search_t_count_adult')</label>
                                    <div class="input-group number-spinner col-md-4">
                                        <span class="input-group-btn ">
                                            <a class="btn btn-subtract" id="adultCountSubtract" data-dir="dwn">-</a>
                                        </span> <input type="text" disabled id="mainAdultCount1" name="adultCountInp" class="form-control text-center input-number" value="2" max=4 min=1> <span class="input-group-btn">
                                            <a class="btn btn-add" id="adultCountAdd" data-dir="up">+</a>
                                        </span>
                                    </div>
                                </div>
                                {{--child count--}}
                                <div class="form-group">
                                    <label class="control-label col-md-8 number-label">@lang('home.search_t_count_child')</label>
                                    <div class="input-group number-spinner col-md-4">
                                        <span class="input-group-btn">
                                            <a class="btn btn-subtract disabled-btn" disabled id="childCountSubtract" data-dir="dwn">-</a>
                                        </span>
                                        <input type="text" disabled id="mainChildCount1" name="childCountInp" class="form-control text-center input-number" value="0" max=4 min=0>
                                        <span class="input-group-btn">
                                            <a class="btn btn-add" id="childCountAdd" data-dir="up">+</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group child-age-wrapper hide" id="childAgeWrapper">
                                    <label class="control-label number-label child-age-label">@lang('home.childAge')</label>
                                    <div id="childAgeContent">
                                        <select name="childAge[]" class="child-age-select">
                                            @for($i=0; $i<18; $i++)
                                                <option value="{{ $i }}" @if($i === 12) selected @endif>{{ $i === 0 ? '<1' : $i }} @lang('home.yearsOld')</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                {{--提交BUTTON--}}
                <li class="pull-left search-box-fix-width">
                    <button type="button" id="mainSearchButton" class="h-filt-bd-button pull-right" onclick="showResultPage('main')">@lang('home.search_t_search')</button>
                </li>
            </ul>
        </form>

        {{--走四方合作伙伴--}}
        <div class="@if($language == 0) cursor-partner @endif h-banner-partner relative">
            <div class="h-banner-partner-title col-sm-12">
                @if($is_aa)
                    美亞的合作夥伴
                @else
                    @lang('home.h_right_t_partner')
                @endif
            </div>
            <a class="h-banner-partner-item1 col-sm-6" @if($language == 0)href="{{$usitrip}}/web_action/Hotel/" @endif target="_blank"></a>
            <a class="h-banner-partner-item2 col-sm-6" @if($language == 0)href="{{$usitrip}}/web_action/Hotel/" @endif target="_blank"></a>
            <a class="h-banner-partner-item3 col-sm-6" @if($language == 0)href="{{$usitrip}}/web_action/Hotel/" @endif target="_blank"></a>
            <a class="h-banner-partner-item4 col-sm-6" @if($language == 0)href="{{$usitrip}}/web_action/Hotel/" @endif target="_blank"></a>
            @if($language == 0)<a class="h-banner-partner-cover" href="{{$usitrip}}/web_action/Hotel/" target="_blank"></a>@endif
        </div>
    </div>
</div>