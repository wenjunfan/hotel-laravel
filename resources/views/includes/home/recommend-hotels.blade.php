{{--首页 酒店推荐--}}
<?php
$imgErrorUrl = '//www.usitrip.com/tpl/www/image/2015/hotels/img_error.jpg';
$cfg['RecHotels'][0] = [
    'v1' => "威尼斯人度假赌场酒店",
    'v2' => "卢克索赌场酒店",
    'v3' => "米高梅赌场酒店",
    'v5' => "马戏团赌场酒店",
];
$cfg['RecHotels'][1] = [
    'v1' => "The Venetian Resort Hotel Casino",
    'v2' => "Luxor Hotel and Casino",
    'v3' => "MGM Grand",
    'v5' => "Circus Circus Hotel and Casino",
];
$cfg['RecHotels'][2] = [
    'v1' => "威尼斯人度假賭場飯店",
    'v2' => "盧克索賭場飯店",
    'v3' => "米高梅賭場飯店",
    'v5' => "馬戲團賭場飯店",
];
$hotels = [
    //0 拉斯维加斯
    [
        // 拉斯维加斯 1个大图酒店
        'lgHotelSearchName' => 'Stratosphere Casino, Hotel & Tower, Nevada, US',
        'lgHotelId'         => '1065834',
        'lgImgUrl'          => '/img/home/las_vegas/Stratosphere-Casino.jpg',
        'price'             => [
            'cur_usd' => '$23',
            'cur_cny' => "￥153",
            'cur_eur' => "€19",
            'cur_gbp' => "£17",
            'cur_aud' => "AUD29",
            'cur_hkd' => "HKD179",
            'cur_twd' => "NT$693",
            'cur_cad' => "CAD28",
        ],
        // 拉斯维加斯 4个小图酒店 OK
        'hotelItems'        => [
            // 0 威尼斯人度假赌场酒店  The Venetian Resort Hotel Casino
            [
                'hotelSearchName' => 'The Venetian Resort Hotel Casino',
                'hotelName'       => $cfg['RecHotels'][$language]['v1'],
                'hotelId'         => '1048997',
                'imgUrl'          => '/img/home/las_vegas/The-Venetian-Resort-Hotel-Casino_label.jpg',
                'hotelLevel'      => 5,
                'price'           => [
                    'cur_usd' => '$161',
                    'cur_cny' => "￥1,075",
                    'cur_eur' => "€136",
                    'cur_gbp' => "£119",
                    'cur_aud' => "AUD205",
                    'cur_hkd' => "HKD1,257",
                    'cur_twd' => "NT$4,855",
                    'cur_cad' => "CAD200",
                ],
            ],
            // 1 卢克索赌场酒店  The Venetian Resort Hotel Casino
            [
                'hotelSearchName' => 'Luxor Hotel and Casino',
                'hotelName'       => $cfg['RecHotels'][$language]['v2'],
                'hotelId'         => '1057143',
                'imgUrl'          => '/img/home/las_vegas/Luxor-Hotel-and-Casino_label.jpg',
                'hotelLevel'      => 4,
                'price'           => [
                    'cur_usd' => '$36',
                    'cur_cny' => "￥240",
                    'cur_eur' => "€30",
                    'cur_gbp' => "£26",
                    'cur_aud' => "AUD45",
                    'cur_hkd' => "HKD281",
                    'cur_twd' => "NT$1,085",
                    'cur_cad' => "CAD44",
                ],
            ],
            // 2 米高梅赌场酒店  The Venetian Resort Hotel Casino
            [
                'hotelSearchName' => 'MGM Grand',
                'hotelName'       => $cfg['RecHotels'][$language]['v3'],
                'hotelId'         => '1055618',
                'imgUrl'          => '/img/home/las_vegas/MGM-Grand_label.jpg',
                'hotelLevel'      => 4,
                'price'           => [
                    'cur_usd' => '$94',
                    'cur_cny' => "￥627",
                    'cur_eur' => "€79",
                    'cur_gbp' => "£70",
                    'cur_aud' => "AUD119",
                    'cur_hkd' => "HKD734",
                    'cur_twd' => "NT$2,834",
                    'cur_cad' => "CAD116",
                ],
            ],
            // 3 马戏团赌场酒店  Circus Circus Hotel and Casino
            [
                'hotelSearchName' => 'The Venetian Resort Hotel Casino',
                'hotelName'       => $cfg['RecHotels'][$language]['v5'],
                'hotelId'         => '1062640',
                'imgUrl'          => '/img/home/las_vegas/Circus-Circus-Hotel-and-Casino_label.jpg',
                'hotelLevel'      => 3,
                'price'           => [
                    'cur_usd' => '$24',
                    'cur_cny' => "￥160",
                    'cur_eur' => "€20",
                    'cur_gbp' => "£17",
                    'cur_aud' => "AUD30",
                    'cur_hkd' => "HKD187",
                    'cur_twd' => "NT$723",
                    'cur_cad' => "CAD29",
                ],
            ],
        ]
    ],
]
?>
<div class="ad_title clear">
    @lang('home.adv3_title')
</div>

<div class="h-recom-wrap">
    {{--推荐酒店 导航 （按城市）--}}
    <div class="h-recom-nav clear ">
        <div class="relative">
            <ul class="h-recom-tab h-recom-nav-hotel js-tuijian fl @if($language==1) h-recom-nav-hotel-en @endif" style="margin-top:2px;">
                <li>
                    <a href="javascript:void(0);" class="active">
                        @lang('search.LasVegas')
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">@lang('search.LosAngeles')</a>
                </li>
                <li>
                    <a href="javascript:void(0);">@lang('search.SanFrancisco')</a>
                </li>
                <li>
                    <a href="javascript:void(0);">@lang('search.NewYork')</a>
                </li>
                <li>
                    <a href="javascript:void(0);">@lang('search.Honolulu')</a>
                </li>
                <li>
                    <a href="javascript:void(0);">@lang('search.Miami')</a>
                </li>
            </ul>
        </div>
    </div>
    {{--start hotel pics and links--}}

    {{--start 切换城市模块--}}
    {{--1.拉斯维加斯--}}
    <div class="hotel-recom-wrap relative" id="hotel-recom-wrap_1" style="display: block;">
        {{--更多链接--}}
        <a class="h-recom-more @if($language == 1) h-recom-more-en @endif" target="_self" href="{{ url('/list') . '/D' . config('city-ids')['1004'] . '.html' }}">@lang('search.more')</a>
        {{--拉斯维加斯 左边大图--}}
        <div class="hotel-recom-left fl">
            <a class="homepage-rec-hotel relative lg lasvegas-1 lasvegas" target="_blank" href="{{ url('/hotel') . '/' . $hotels[0]['lgHotelId'] . '.html' }}">
                <div class="hotel-recom-left-cont">
                    <p>@lang('cfg.RecHotels.dd')</p>
                    <span>
                        <em cur_usd="{!! $hotels[0]['price']['cur_usd'] !!}" cur_cny="{!! $hotels[0]['price']['cur_cny'] !!}" cur_eur="{!! $hotels[0]['price']['cur_eur'] !!}" cur_gbp="{!! $hotels[0]['price']['cur_gbp'] !!}" cur_aud="{!! $hotels[0]['price']['cur_aud'] !!}" cur_hkd="{!! $hotels[0]['price']['cur_hkd'] !!}" cur_twd="{!! $hotels[0]['price']['cur_twd'] !!}" cur_cad="{!! $hotels[0]['price']['cur_cad'] !!}">{!! $hotels[0]['price']['cur_usd'] !!}</em>
                        <i class="money-m">@lang('search.up')</i>
                    </span>
                </div>
            </a>
        </div>
        {{--/左边大图--}}
        {{--右边四个小图酒店--}}
        <ul class="hotel-recom-right fr">
            {{--拉斯维加斯 hotel loop--}}
            @for ($i = 0; $i < 4; $i++)
                <li class="hotel-recom-item">
                    <a class="homepage-rec-hotel relative lasvegas-{{$i+2}} lasvegas" target="_blank" href="{{ url('/hotel') . '/' . $hotels[0]['hotelItems'][$i]['hotelId'] . '.html' }}">
                        <div class="hotel-recom-right-img"></div>
                        <div class="hotel-recom-item-content">
                            <div class="item-name">{!! $hotels[0]['hotelItems'][$i]['hotelName'] !!}</div>
                            <p class="clear">
                                <i class="hotel-ico hotel-recom-ico{!! $hotels[0]['hotelItems'][$i]['hotelLevel']!!}"></i>
                            </p>
                            <span>
                            <em cur_usd="{!! $hotels[0]['hotelItems'][$i]['price']['cur_usd'] !!}" cur_cny="{!! $hotels[0]['hotelItems'][$i]['price']['cur_cny'] !!}" cur_eur="{!! $hotels[0]['hotelItems'][$i]['price']['cur_eur'] !!}" cur_gbp="{!! $hotels[0]['hotelItems'][$i]['price']['cur_gbp'] !!}" cur_aud="{!! $hotels[0]['hotelItems'][$i]['price']['cur_aud'] !!}" cur_hkd="{!! $hotels[0]['hotelItems'][$i]['price']['cur_hkd'] !!}" cur_twd="{!! $hotels[0]['hotelItems'][$i]['price']['cur_twd'] !!}" cur_cad="{!! $hotels[0]['hotelItems'][$i]['price']['cur_cad'] !!}">
                                {!! $hotels[0]['hotelItems'][$i]['price']['cur_usd'] !!}</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                        </div>
                    </a>
                </li>
            @endfor
        </ul>
    </div>

    {{--2.洛杉矶--}}
    <div class="hotel-recom-wrap relative" id="hotel-recom-wrap_2" style="display: none;">
        {{--更多链接--}}
        <a class="h-recom-more @if($language == 1) h-recom-more-en @endif" target="_self" href="{{ url('/list') . '/D' . config('city-ids')['1002'] . '.html' }}">@lang('search.more')</a>
        {{--洛杉矶 左边大图 阿纳海姆希尔顿酒店--}}
        <div class="hotel-recom-left fl">
            <a class="homepage-rec-hotel relative lg losangeles-1 losangeles" target="_blank" href="{{ url('/hotel/1048189.html') }}">
                <div class="hotel-recom-left-cont">
                    <p>@if($language == 0) 阿纳海姆希尔顿酒店 @elseif($language == 2) 阿納海姆希爾頓飯店 @else Hilton Anaheim @endif</p>
                    <span>
                            <em cur_usd="$95" cur_cny="￥634" cur_eur="€80" cur_gbp="£70" cur_aud="AUD121" cur_hkd="HKD742" cur_twd="NT$2,864" cur_cad="CAD118">$95</em>
                        <i class="money-m">@lang('search.up')</i>
                    </span>
                </div>
            </a>
        </div>
        {{--/左边大图--}}
        {{--洛杉矶右边4个小图--}}
        <ul class="hotel-recom-right fr">
            {{--洛杉矶 hotel1--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative losangeles-2 losangeles" target="_blank" href="{{ url('/hotel/1053581.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.l1')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico3"></i>
                        </p>
                        <span>
                            <em cur_usd="$140" cur_cny="￥935" cur_eur="€118" cur_gbp="£104" cur_aud="AUD178" cur_hkd="HKD1,093" cur_twd="NT$4,221" cur_cad="CAD174">$140</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            {{--洛杉矶 hotel2--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative losangeles-3 losangeles" target="_blank" href="{{ url('/hotel/1048189.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.l6')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico4"></i>
                        </p>
                        <span>
                            <em cur_usd="$144" cur_cny="￥961" cur_eur="€122" cur_gbp="£107" cur_aud="AUD183" cur_hkd="HKD1,124" cur_twd="NT$4,342" cur_cad="CAD179">$144</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            {{--洛杉矶 hotel3--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative losangeles-4 losangeles" target="_blank" href="{{ url('/hotel/1063212.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.l3')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico4"></i>
                        </p>
                        <span>
                            <em cur_usd="$212" cur_cny="￥1,416" cur_eur="€179" cur_gbp="£157" cur_aud="AUD270" cur_hkd="HKD1,656" cur_twd="NT$6,393" cur_cad="CAD263">$212</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            {{--洛杉矶 hotel4--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative losangeles-5 losangeles" target="_blank" href="{{ url('/hotel/1050590.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.l4')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico4"></i>
                        </p>
                        <span>
                            <em cur_usd="$162" cur_cny="￥1,082" cur_eur="€137" cur_gbp="£120" cur_aud="AUD206" cur_hkd="HKD1,265" cur_twd="NT$4,885" cur_cad="CAD201">$162</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
        </ul>
    </div>

    {{--3.旧金山--}}
    <div class="hotel-recom-wrap relative" id="hotel-recom-wrap_3" style="display:none">
        {{--更多链接--}}
        <a class="h-recom-more @if($language == 1) h-recom-more-en @endif" target="_self" href="{{ url('/list') . '/D' . config('city-ids')['1003'] . '.html' }}">@lang('search.more')</a>
        {{--左边大图--}}
        <div class="hotel-recom-left fl">
            <a class="homepage-rec-hotel relative lg sanf-1 sanf" target="_blank" href="{{ url('/hotel/1051585.html') }}">
                <div class="hotel-recom-left-cont">
                    <p>@if($language == 0) 旧金山港湾索菲特飯店 @elseif($language == 2) 舊金山港灣索菲特飯店 @else Sofitel San Francisco
                        Bay @endif</p>
                    <span>
                        <em cur_usd="$144" cur_cny="￥961" cur_eur="€122" cur_gbp="£107" cur_aud="AUD183" cur_hkd="HKD1,124" cur_twd="NT$4,342" cur_cad="CAD179">$144</em>
                        <i class="money-m">@lang('search.up')</i>
                    </span>
                </div>
            </a>
        </div>
        {{--/左边大图--}}
        <ul class="hotel-recom-right fr">
            {{--旧金山 hotel1--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative sanf-2 sanf" target="_blank" href="{{ url('/hotel/1077010.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.s5')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico5"></i>
                        </p>
                        <span>
                            <em cur_usd="$136" cur_cny="￥908" cur_eur="€115" cur_gbp="£101" cur_aud="AUD173" cur_hkd="HKD1,062" cur_twd="NT$4,101" cur_cad="CAD169">
                                $136</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            {{--旧金山 hotel2--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative sanf-3 sanf" target="_blank" href="{{ url('/hotel/1060316.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.s2')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico3"></i>
                        </p>
                        <span>
                            <em cur_usd="$127" cur_cny="￥848" cur_eur="€107" cur_gbp="£94" cur_aud="AUD161" cur_hkd="HKD992" cur_twd="NT$3,829" cur_cad="CAD157">$127</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            {{--旧金山 hotel3--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative sanf-4 sanf" target="_blank" href="{{ url('/hotel/1064567.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.s3')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico2"></i>
                        </p>
                        <span>
                            <em cur_usd="$102" cur_cny="￥681" cur_eur="€86" cur_gbp="£75" cur_aud="AUD129" cur_hkd="HKD796" cur_twd="NT$3,075" cur_cad="CAD126">$102</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            {{--旧金山 hotel4--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative sanf-5 sanf" target="_blank" href="{{ url('/hotel/1063654.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.s4')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico4"></i>
                        </p>
                        <span>
                            <em cur_usd="$158" cur_cny="￥1,055" cur_eur="€134" cur_gbp="£117" cur_aud="AUD201" cur_hkd="HKD1,234" cur_twd="NT$4,764" cur_cad="CAD196">$158</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
        </ul>
    </div>

    {{--4.纽约--}}
    <div class="hotel-recom-wrap relative" id="hotel-recom-wrap_4" style="display:none">
        {{--更多链接--}}
        <a class="h-recom-more @if($language == 1) h-recom-more-en @endif" target="_self" href="{{ url('/list') . '/D' . config('city-ids')['1001'] . '.html' }}">@lang('search.more')</a>
        {{--纽约 左边大图--}}
        <div class="hotel-recom-left fl">
            <a class="homepage-rec-hotel relative lg newyork-1 newyork" target="_blank" href="{{ url('/hotel/1121090.html') }}">
                <div class="hotel-recom-left-cont">
                    <p>@if($language == 0) 纽波特威斯汀酒店 @elseif($language == 2) 紐波特威斯汀飯店 @else Westin Jersey City Newport @endif</p>
                    <span>
                        <em cur_usd="$169" cur_cny="￥1,128" cur_eur="€143" cur_gbp="£125" cur_aud="AUD215" cur_hkd="HKD1,320" cur_twd="NT$5,096" cur_cad="CAD210">$169</em>
                        <i class="money-m">@lang('search.up')</i>
                    </span>
                </div>
            </a>
        </div>
        {{--/左边大图--}}
        <ul class="hotel-recom-right fr">
            {{--纽约 hotel1--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative newyork-2 newyork" target="_blank" href="{{ url('/hotel/1087103.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.n1')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico4"></i>
                        </p>
                        <span>
                            <em cur_usd="$397" cur_cny="￥2,651" cur_eur="€336" cur_gbp="£295" cur_aud="AUD505" cur_hkd="HKD3,101" cur_twd="NT$11,971" cur_cad="CAD493">$397</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            {{--纽约 hotel2--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative newyork-3 newyork" target="_blank" href="{{ url('/hotel/1186252.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.n5')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico4"></i>
                        </p>
                        <span>
                            <em cur_usd="$174" cur_cny="￥1,162" cur_eur="€147" cur_gbp="£129" cur_aud="AUD221" cur_hkd="HKD1,359" cur_twd="NT$5,247" cur_cad="CAD216">$174</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            {{--纽约 hotel3--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative newyork-4 newyork" target="_blank" href="{{ url('/hotel/1144184.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.n3')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico4"></i>
                        </p>
                        <span>
                            <em cur_usd="$266" cur_cny="￥1,776" cur_eur="€225" cur_gbp="£198" cur_aud="AUD338" cur_hkd="HKD2,077" cur_twd="NT$8,021" cur_cad="CAD330">$266</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            {{--纽约 hotel4--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative newyork-5 newyork" target="_blank" href="{{ url('/hotel/1171557.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.n4')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico3"></i>
                        </p>
                        <span>
                            <em cur_usd="$249" cur_cny="￥1,663" cur_eur="€211" cur_gbp="£185" cur_aud="AUD317" cur_hkd="HKD1,945" cur_twd="NT$7,508" cur_cad="CAD309">$249</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
        </ul>
    </div>

    {{--5.檀香山--}}
    <div class="hotel-recom-wrap relative" id="hotel-recom-wrap_5" style="display:none">
        {{--更多链接--}}
        <a class="h-recom-more @if($language == 1) h-recom-more-en @endif" target="_self" href="{{ url('/list') . '/D40273.html' }}">@lang('search.more')</a>
        {{--檀香山 左边大图--}}
        <div class="hotel-recom-left fl">
            <a class="homepage-rec-hotel relative lg honolulu-1 honolulu" target="_blank" href="{{ url('/hotel/1056341.html') }}">
                <div class="hotel-recom-left-cont">
                    <p>@if($language == 0) 威基基海滩希尔顿夏威夷乡村酒店 @elseif($language == 2) 威基基海灘希爾頓夏威夷鄉村飯店 @else Hilton Hawaiian Village Waikiki Beach Resort @endif</p>
                    <span>
                        <em cur_usd="$347" cur_cny="￥2,317" cur_eur="€294" cur_gbp="£258" cur_aud="AUD442" cur_hkd="HKD2,710" cur_twd="NT$10,464" cur_cad="CAD431">$347</em>
                        <i class="money-m">@lang('search.up')</i>
                    </span>
                </div>
            </a>
        </div>
        {{--/左边大图--}}
        <ul class="hotel-recom-right fr">
            {{--檀香山 hotel1--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative honolulu-2 honolulu" target="_blank" href="{{ url('/hotel/1052757.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.h1')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico2"></i>
                        </p>
                        <span>
                            <em cur_usd="$214" cur_cny="￥1,429" cur_eur="€181" cur_gbp="£159" cur_aud="AUD272" cur_hkd="HKD1,671" cur_twd="NT$6,453" cur_cad="CAD266">$214</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            {{--檀香山 hotel2--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative honolulu-3 honolulu" target="_blank" href="{{ url('/hotel/1059902.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.h4')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico4"></i>
                        </p>
                        <span>
                            <em cur_usd="$358" cur_cny="￥2,391" cur_eur="€303" cur_gbp="£266" cur_aud="AUD456" cur_hkd="HKD2,796" cur_twd="NT$10,795" cur_cad="CAD445">
                                $358</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            {{--檀香山 hotel3--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative honolulu-4 honolulu" target="_blank" href="{{ url('/hotel/1048340.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.h3')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico4"></i>
                        </p>
                        <span>
                            <em cur_usd="$418" cur_cny="￥2,792" cur_eur="€354" cur_gbp="£311" cur_aud="AUD532" cur_hkd="HKD3,265" cur_twd="NT$12,605" cur_cad="CAD519">$418</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            {{--檀香山 hotel4--}}
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative honolulu-5 honolulu" target="_blank" href="{{ url('/hotel/1230913.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.h6')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico5"></i>
                        </p>
                        <span>
                            <em cur_usd="$510" cur_cny="￥3,231" cur_eur="€405" cur_gbp="£369" cur_aud="AUD498" cur_hkd="HKD3,848" cur_twd="NT$11,292" cur_cad="CAD491">$510</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
        </ul>
    </div>

    {{--6.迈阿密--}}
    <div class="hotel-recom-wrap relative" id="hotel-recom-wrap_6" style="display:none">
        {{--更多链接--}}
        <a class="h-recom-more @if($language == 1) h-recom-more-en @endif" target="_self" href="{{ url('/list') . '/D' . config('city-ids')['1010'] . '.html' }}">@lang('search.more')</a>
        {{--迈阿密 左边大图--}}
        <div class="hotel-recom-left fl">
            <a class="homepage-rec-hotel relative lg miami-1 miami" target="_blank" href="{{ url('/hotel/1061399.html') }}">
                <div class="hotel-recom-left-cont">
                    <p>@if($language == 0) 迈阿密-海里亚智选假日酒店及套房 @elseif($language == 2) 邁阿密-海裏亞智選假日飯店及套房 @else Holiday Inn Express &amp; Suites
                        Miami-Hialeah @endif</p>
                    <span>
                        <em cur_usd="$121" cur_cny="￥808" cur_eur="€102" cur_gbp="£90" cur_aud="AUD154" cur_hkd="HKD945" cur_twd="NT$3,648" cur_cad="CAD150">$121</em>
                        <i class="money-m">@lang('search.up')</i>
                    </span>
                </div>
            </a>
        </div>
        {{--/左边大图--}}
        <ul class="hotel-recom-right fr">
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative miami-2 miami" target="_blank" href="{{ url('/hotel/1186847.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.m1')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico4"></i>
                        </p>
                        <span>
                            <em cur_usd="$275" cur_cny="￥1,837" cur_eur="€233" cur_gbp="£204" cur_aud="AUD350" cur_hkd="HKD2,148" cur_twd="NT$8,292" cur_cad="CAD341">$275</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative miami-3 miami" target="_blank" href="{{ url('/hotel/1065994.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.m2')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico3"></i>
                        </p>
                        <span>
                            <em cur_usd="$125" cur_cny="￥835" cur_eur="€106" cur_gbp="£93" cur_aud="AUD159" cur_hkd="HKD976" cur_twd="NT$3,769" cur_cad="CAD155">$125</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative miami-4 miami" target="_blank" href="{{ url('/hotel/1056130.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.m3')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico3"></i>
                        </p>
                        <span>
                            <em cur_usd="$142" cur_cny="￥948" cur_eur="€120" cur_gbp="£105" cur_aud="AUD180" cur_hkd="HKD1,109" cur_twd="NT$4,282" cur_cad="CAD176">$142</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
            <li class="hotel-recom-item">
                <a class="homepage-rec-hotel relative miami-5 miami" target="_blank" href="{{ url('/hotel/1117451.html') }}">
                    <div class="hotel-recom-right-img"></div>
                    <div class="hotel-recom-item-content">
                        <div class="item-name">@lang('cfg.RecHotels.m4')</div>
                        <p class="clear">
                            <i class="hotel-ico hotel-recom-ico3"></i>
                        </p>
                        <span>
                            <em cur_usd="$139" cur_cny="￥928" cur_eur="€117" cur_gbp="£103" cur_aud="AUD177" cur_hkd="HKD1,085" cur_twd="NT$4,191" cur_cad="CAD172">$139</em>
                            <i class="money-m">@lang('search.up')</i>
                        </span>
                    </div>
                </a>
            </li>
        </ul>
    </div>
    {{--/首页 酒店推荐--}}
</div>
