{{--导航栏--}}
<?php
$add_tw =  $language == 2 ? '?lang=tw' : '';
if($is_aa){
    $usitrip = '//www.supervacation.net';
}else{
    $usitrip = $language == 2 ? 'https://www.usitrip.com/tw' : 'https://www.usitrip.com';
}
?>
{{--首页 今日特惠home ad--}}
{{--title--}}
<div class="ad_title clear">
   @lang('home.adv1_title')
</div>
{{--sliders--}}
<div class="ad-slider">
    {{--item 1--}}
    @if($language == 0)
    <div class="ad-slider-item" style="display: none;" onclick="window.open('{{$usitrip}}/web_action/Hotel/','_blank');">
        {{--image--}}
        <div class="ad-slider-item-pic1">
        </div>
        {{--title--}}
        <div class="ad-slider-item-content" onclick="">
            <p class="ad-slider-item-title">@lang('home.slider_1_title')</p>
            <a href="{{$usitrip}}/web_action/Hotel/" target="_blank">@lang('home.view_detail')</a>
        </div>
    </div>
    @endif
    {{--item 2--}}
    <div class="ad-slider-item" style="display: none;" onclick="window.open('/list/D60176.html{{ $add_tw }}','_blank');">
        {{--image--}}
        <div class="@if($language == 0) ad-slider-item-pic2 @else ad-slider-item-pic2-en  @endif">
        </div>
        {{--title--}}
        <div class="ad-slider-item-content">
            <p class="ad-slider-item-title">@lang('home.slider_2_title')</p>
            <a href="/list/D60176.html{{ $add_tw }}" target="_blank">
                @lang('home.view_detail')</a>
        </div>
    </div>

    {{--item 3--}}
    <div class="ad-slider-item" style="display: none;"  onclick="window.open('/scenic/11536.html{{ $add_tw }}','_blank');">
        {{--image--}}
        <div class="@if($language == 0) ad-slider-item-pic3 @else ad-slider-item-pic3-en  @endif">
        </div>
        {{--title--}}
        <div class="ad-slider-item-content">
            <p class="ad-slider-item-title">@lang('home.slider_3_title')</p>
            <a href="/scenic/11536.html{{ $add_tw }}" target="_blank">@lang('home.view_detail')</a>
        </div>
    </div>
    {{--item 4--}}
    <div class="ad-slider-item" style="display: none;"  onclick="window.open('/scenic/11536.html{{ $add_tw }}','_blank');">
        {{--image--}}
        <div class="@if($language == 0) ad-slider-item-pic4 @else ad-slider-item-pic4-en  @endif">
        </div>
        {{--title--}}
        <div class="ad-slider-item-content">
            <p class="ad-slider-item-title">@lang('home.slider_4_title')</p>
            <a href="/scenic/33834.html{{ $add_tw }}" target="_blank">@lang('home.view_detail')</a>
        </div>
    </div>
</div>

{{--原限时抢购的效果--}}
{{--<div id="posterGrid"></div>--}}
{{--styles--}}
@if($language == 1)
    <style>
    .slick-dots li.slick-active button:before{
        opacity: 0;
    }
    </style>
@endif