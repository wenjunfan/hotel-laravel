<?php

$r1 = [];
$r2 = [];
$r3 = [];
$r4 = [];
$r5 = [];
$r6 = [];
$r7 = [];
$r8 = [];
$r9 = [];
$r10 = [];

foreach ($facilities as $facility) {
    switch ($facility['type_zh']) {
        case '网络连接':
            $r1[] = $facility;
            break;
        case '房间设备':
            $r2[] = $facility;
            break;
        case '酒店服务':
            $r3[] = $facility;
            break;
        case '餐饮服务':
            $r4[] = $facility;
            break;
        case '餐饮设施':
            $r5[] = $facility;
            break;
        case '健康美容':
            $r6[] = $facility;
            break;
        case '娱乐项目':
            $r7[] = $facility;
            break;
        case '商务服务':
            $r8[] = $facility;
            break;
        case '交通情况':
            $r9[] = $facility;
            break;
        case '其他设施':
            $r10[] = $facility;
            break;
    }
}

$result = [
    '网络连接' => $r1,
    '房间设备' => $r2,
    '酒店服务' => $r3,
    '餐饮服务' => $r4,
    '餐饮设施' => $r5,
    '健康美容' => $r6,
    '娱乐项目' => $r7,
    '商务服务' => $r8,
    '交通情况' => $r9,
    '其他设施' => $r10,
];
?>

<div class="hotel-facility-wrapper">
    <!--酒店服务与设施 start-->
    <div id="hotel-detail-facilities" class="hotel-section-wrapper">
    @if(count($r1) + count($r2) + count($r3) + count($r4) + count($r5) + count($r6) + count($r7) + count($r8) + count($r9) + count($r10) > 0)
        <div class="ibox whattheyoffers hotel-services-box">
            <div class="ibox hotel-section-title info-num">
                <h4><span>{{trans('hotel.wto')}}</span></h4>
                <div class="ibox hotel-dynamic-policy-box">
                    <div class="ibox-noborder facility-item  hotel-services-detail" id="hotelservices">
                        @if(count($result['网络连接']) > 0)
                            <ul>
                                @for($i = 0; $i < count($result['网络连接']); $i++)
                                    <li class='hotel-service-special-li' id='internet'>
                                        <?php $r = $result['网络连接'][$i];?>
                                        @if($r['fee'] == 1)
                                            <span class='hotel-service-special-with-fee'>
                                        @endif
                                                {{$language == 0 ? $r['title_zh'] == '' ? $r['title'] : $r['title_zh'] : $r['title']}}
                                                {{$r['val'] == 0 ? '' : '：' . $r['val']}}
                                                {{$i == count($result['网络连接']) - 1 ? ' ' : ' '}}
                                                @if($r['fee'] == 1)
                                            </span>
                                        @endif
                                    </li>
                                @endfor
                            </ul>
                        @endif

                        @if(count($result['房间设备']) > 0)
                            <ul>
                                @for($i = 0; $i < count($result['房间设备']); $i++)
                                    <li class='hotel-service-special-li' id='room'>
                                        <?php $r = $result['房间设备'][$i];?>
                                        @if($r['fee'] == 1)
                                            <span class='hotel-service-special-with-fee'>
                                                                @endif
                                                {{$language == 0 ? $r['title_zh'] == '' ? $r['title'] : $r['title_zh'] : $r['title']}}
                                                {{$r['val'] == 0 ? '' : '：' . $r['val']}}
                                                {{$i == count($result['房间设备']) - 1 ? '' : ' '}}
                                            </span>
                                    </li>
                                @endfor
                            </ul>
                        @endif

                        @if(count($result['酒店服务']) > 0)
                            <ul>
                                @for($i = 0; $i < count($result['酒店服务']); $i++)
                                    <li class='hotel-service-special-li' id='hotel'>
                                        <?php $r = $result['酒店服务'][$i];?>
                                        @if($r['fee'] == 1)
                                            <span class='hotel-service-special-with-fee'>
                                                                @endif
                                                {{$language  == 0? $r['title_zh'] == '' ? $r['title'] : $r['title_zh'] : $r['title']}}
                                                {{$r['val'] == 0 ? '' : '：' . $r['val']}}
                                                {{$i == count($result['酒店服务']) - 1 ? '' : ' '}}
                                            </span>
                                    </li>
                                @endfor
                            </ul>
                        @endif

                        @if(count($result['餐饮服务']) > 0)
                            <ul>
                                @for($i = 0; $i < count($result['餐饮服务']); $i++)
                                    <li class='hotel-service-special-li' id='foodService'>
                                        <?php $r = $result['餐饮服务'][$i];?>
                                        @if($r['fee'] == 1)
                                            <span class='hotel-service-special-with-fee'>
                                                                @endif
                                                {{$language  == 0? $r['title_zh'] == '' ? $r['title'] : $r['title_zh'] : $r['title']}}
                                                {{$r['val'] == 0 ? '' : '：' . $r['val']}}
                                                {{$i == count($result['餐饮服务']) - 1 ? '' : ' '}}
                                            </span>
                                    </li>
                                @endfor
                            </ul>
                        @endif

                        @if(count($result['餐饮设施']) > 0)
                            <ul>
                                @for($i = 0; $i < count($result['餐饮设施']); $i++)
                                    <li class='hotel-service-special-li' id='foodFacility'>
                                        <?php $r = $result['餐饮设施'][$i];?>
                                        @if($r['fee'] == 1)
                                            <span class='hotel-service-special-with-fee'>
                                                                @endif
                                                {{$language  == 0? $r['title_zh'] == '' ? $r['title'] : $r['title_zh'] : $r['title']}}
                                                {{$r['val'] == 0 ? '' : '：' . $r['val']}}
                                                {{$i == count($result['餐饮设施']) - 1 ? '' : ' '}}
                                        </span>
                                    </li>
                                @endfor
                            </ul>
                        @endif

                        @if(count($result['健康美容']) > 0)
                            <ul>
                                @for($i = 0; $i < count($result['健康美容']); $i++)
                                    <li class='hotel-service-special-with-li' id='beauty'>
                                        <?php $r = $result['健康美容'][$i];?>
                                        @if($r['fee'] == 1)
                                            <span class='hotel-service-special-with-fee'>
                                                                @endif
                                                {{$language  == 0? $r['title_zh'] == '' ? $r['title'] : $r['title_zh'] : $r['title']}}
                                                {{$r['val'] == 0 ? '' : '：' . $r['val']}}
                                                {{$i == count($result['健康美容']) - 1 ? '' : ' '}}
                                           </span>
                                    </li>
                                @endfor
                            </ul>
                        @endif

                        @if(count($result['娱乐项目']) > 0)
                            <ul>
                                @for($i = 0; $i < count($result['娱乐项目']); $i++)
                                    <li class='hotel-service-special-with-li' id='entertainment'>
                                        <?php $r = $result['娱乐项目'][$i];?>
                                        @if($r['fee'] == 1)
                                            <span class='hotel-service-special-with-fee'>
                                                                @endif
                                                {{$language  == 0? $r['title_zh'] == '' ? $r['title'] : $r['title_zh'] : $r['title']}}
                                                {{$r['val'] == 0 ? '' : '：' . $r['val']}}
                                                {{$i == count($result['娱乐项目']) - 1 ? '' : ' '}}
                                        </span>
                                    </li>
                                @endfor
                            </ul>
                        @endif

                        @if(count($result['商务服务']) > 0)
                            <ul>
                                @for($i = 0; $i < count($result['商务服务']); $i++)
                                    <li class='hotel-service-special-with-li' id='business'>
                                        <?php $r = $result['商务服务'][$i];?>
                                        @if($r['fee'] == 1)
                                            <span class='hotel-service-special-with-fee'>
                                                                @endif
                                                {{$language == 0 ? $r['title_zh'] == '' ? $r['title'] : $r['title_zh'] : $r['title']}}
                                                {{$r['val'] == 0 ? '' : '：' . $r['val']}}
                                                {{$i == count($result['商务服务']) - 1 ? '' : ' '}}
                                            </span>
                                    </li>
                                @endfor
                            </ul>
                        @endif

                        @if(count($result['交通情况']) > 0)
                            <ul>
                                @for($i = 0; $i < count($result['交通情况']); $i++)
                                    <li class='hotel-service-special-with-li' id='traffic'>
                                        <?php $r = $result['交通情况'][$i];?>
                                        @if($r['fee'] == 1)
                                            <span class='hotel-service-special-with-fee'>
                                                                @endif
                                                {{$language  == 0? $r['title_zh'] == '' ? $r['title'] : $r['title_zh'] : $r['title']}}
                                                {{$r['val'] == 0 ? '' : '：' . $r['val']}}
                                                {{$i == count($result['交通情况']) - 1 ? '' : ' '}}
                                            </span>
                                    </li>
                                @endfor
                            </ul>
                        @endif

                        @if(count($result['其他设施']) > 0)
                            <ul>
                                @for($i = 0; $i < count($result['其他设施']); $i++)
                                    <li class='hotel-service-special-with-li' id='others'>
                                        <?php $r = $result['其他设施'][$i];?>
                                        @if($r['fee'] == 1)
                                            <span class='hotel-service-special-with-fee'>
                                                                @endif
                                                {{$language == 0 ? $r['title_zh'] == '' ? $r['title'] : $r['title_zh'] : $r['title']}}
                                                {{$r['val'] == 0 ? '' : '：' . $r['val']}}
                                                {{$i == count($result['其他设施']) - 1 ? '' : ' '}}
                                            </span>
                                    </li>
                                @endfor
                            </ul>
                        @endif
                    </div>
                    <p class="hotel-facility-note"></p>
                </div>
            </div>
        </div>
    @endif
    </div>
    <!--酒店服务与设施 end-->
</div>