<style>
    .smallError{
        color:#a94442; font-size:12px; font-weight:400;
    }
    .has-account{
        float: left;
        height: 30px;
        line-height: 30px;
    }
    .padding-bottom-30{
        padding-bottom:30px;
    }
</style>
<div class="register-modal modal fade" id="registerModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="room-modal-close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <div>
                    <p class="login-text text-center font36">@lang('hotel.register')</p>
                    <form class="m-t" role="form" autocomplete="off" id="registerForm">
                        {!! csrf_field() !!}
                        <input name="link" id="RegisterBookLink" type="hidden" value="">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="@lang('hotel.userName')" autofocus required>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('hotel.userEmail')" required>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" name="password" placeholder="@lang('hotel.password')" onfocus="document.getElementById('passwordRule').style.visibility = document.getElementById('passwordRule').style.visibility === 'hidden' ? 'visible' : 'hidden';" autocomplete="new-password" required>
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" name="password_confirmation" placeholder="@lang('hotel.confirmPassword')" autocomplete="new-password" required>
                            <span class="help-block" style="visibility:hidden;" id="passwordRule">
                                 <strong class="smallError" >@lang('hotel.passwordRule')</strong>
                            <br>
                            </span>
                        </div>
                        <input type="checkbox" id="cb" onclick="document.getElementById('bt').disabled = !this.checked;" class="pull-left">
                        <span>@lang('hotel.agree1')
                            <a href="http://www.117book.com/tiaokuan0.html" target="_blank">@lang('hotel.termOfUse')</a>@lang('hotel.agree2')
                            <a href="http://www.117book.com/tiaokuan.html" target="_blank">@lang('hotel.privacyPolicy')</a>
                        </span>
                        <br><br>
                        <button type="button" id="bt" disabled class="btn btn-primary btn-block" onclick="logOutAndRegister()">
                            @lang('hotel.registerToBook')
                        </button>
                        <div class="padding-bottom-30">
                            <p class="has-account text-center">
                                <span>Already has an account?</span>
                            </p>
                            <a class="btn btn-sm btn-warning  pull-right" href="/logout">Login</a>
                        </div>
                    </form>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
  function logOutAndRegister(){
    var isValid = true;
    var email = $('input[name="email"]').val();
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");

    $('input').filter('[required]:visible').each(function() {
      $(this).val($(this).val().trim());
      if ($(this).val() === '' ){
        $(this).focus();
        isValid = false;
      }
    });

    if($('input[name="name"]').val().trim() === ''){
      swal(langHotel.userNameRequired);
    }else if(isValid === false){
      swal(langHotel.pleaseCheckInput);
    }else if(atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
      swal(langHotel.notValidEmail);
    }else if($('input[name="password_confirmation"]').val() !== $('input[name="password"]').val()){
      swal(langHotel.passwordNotMatch);
    }else {
      FullscreenSpinner.create();
      $.post("{{url('/logoutAndRegister')}}", $("#registerForm").serialize(), function (data) {
        if (data.success) {
          FullscreenSpinner.destroy();
          swal({
            title: langHotel.registerSuccess,
            text:  langHotel.okToBook,
            type: "success"
          }, function () {
            location.reload();
          });
        } else {
          FullscreenSpinner.destroy();
          swal(langHotel.registerFailed, data.message, "error");
          $('.close').click();
        }
      });
    }
  }

</script>
