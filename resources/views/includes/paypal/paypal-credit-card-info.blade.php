{{--信用卡在线付款--}}
<div class="form-group">
    <label class="col-sm-2 leftTitle" for="cardType">{{trans('paypal.ct')}}</label>
    <div class="col-sm-9 form-inline">
        <img style="height:30px;" src="/img/payment/all-major-credit-cards.png"/>
        <input type="hidden" id="cardType" name="cardType" class="form-control" value=""/>
    </div>
</div>
{{--姓名--}}
<div class="form-group">
    <label class="col-sm-2 leftTitle" for="firstName">{{trans('paypal.cardholder')}}<i class="mustFill">*</i></label>
    <div class="col-sm-9 form-inline">
        <input type="text" class="form-control" id="cc_fn" name="firstName" value="" placeholder="{{trans('book.fname')}}" v-model="cardData.firstName"/>
        <input type="text" class="form-control" id="cc_ln" name="lastName" value="" placeholder="{{trans('book.lname')}}" v-model="cardData.lastName"/>
    </div>
</div>
{{--卡号--}}
<div class="form-group">
    <label class="col-sm-2 leftTitle" for="number">
        {{trans('refunds.cn')}}<i class="mustFill">*</i>
    </label>
    <div class="col-sm-9 form-inline">
        <input type="text" id="number" class="form-control" name="number" value="" v-model="cardData.number"/>
    </div>
</div>
{{--有效日期--}}
<div class="form-group">
    <label class="col-sm-2 leftTitle" for="expirationDate">{{trans('paypal.ed')}}<i class="mustFill">*</i></label>

    <div class="col-sm-9 form-inline">
        <select id="month" name="month" class="form-control" v-model="cardData.month">
            <option value="01">
                {{trans('paypal.jan')}}</option>
            <option value="02">
                {{trans('paypal.feb')}}</option>
            <option value="03">
                {{trans('paypal.mar')}}</option>
            <option value="04">
                {{trans('paypal.apr')}}</option>
            <option value="05">
                {{trans('paypal.may')}}</option>
            <option value="06">
                {{trans('paypal.jun')}}</option>
            <option value="07">
                {{trans('paypal.jul')}}</option>
            <option value="08">
                {{trans('paypal.aug')}}</option>
            <option value="09">
                {{trans('paypal.sep')}}</option>
            <option value="10">
                {{trans('paypal.oct')}}</option>
            <option value="11">
                {{trans('paypal.nov')}}</option>
            <option value="12">
                {{trans('paypal.dec')}}</option>
        </select>
        <select id="year" name="year" class="form-control" v-model="cardData.year">
            <option value="{{$year20.'20'}}">
                2020{{trans('paypal.year1')}}</option>
            <option value="{{$year20.'21'}}">
                2021{{trans('paypal.year1')}}</option>
            <option value="{{$year20.'22'}}">
                2022{{trans('paypal.year1')}}</option>
            <option value="{{$year20.'23'}}">
                2023{{trans('paypal.year1')}}</option>
            <option value="{{$year20.'24'}}">
                2024{{trans('paypal.year1')}}</option>
            <option value="{{$year20.'25'}}">
                2025{{trans('paypal.year1')}}</option>
            <option value="{{$year20.'26'}}">
                2026{{trans('paypal.year1')}}</option>
            <option value="{{$year20.'27'}}">
                2027{{trans('paypal.year1')}}</option>
            <option value="{{$year20.'28'}}">
                2028{{trans('paypal.year1')}}</option>
        </select>
    </div>
</div>
{{--CVS验证码--}}
<div class="form-group">
    <label class="col-sm-2 leftTitle" for="CVC">{{trans('paypal.cvc')}}<i class="mustFill">*</i></label>
    <div class="col-sm-9 form-inline">
        <input type="text" id="cvc" class="form-control" name="CVC" value="" placeholder="CVC" v-model="cardData.cvc"/>
    </div>
</div>
{{--手机号验证--}}
<div class="form-group NeedVerify" style="display: none;">
    <label class="col-sm-2 leftTitle" for="phone">{{trans('paypal.phoneValid')}}<i class="mustFill">*</i></label>
    <div id="validatePhoneSection" class="col-sm-9 form-inline validatePhone">
        <div class="area-box">
            <input name="countryCodePhone" id="countryCodePhone" readonly class="form-control" type="text" value="{{$phoneCountryCode}} " placeholder="">
            <span class="country-with-flag"><i class="flag" style="background-position: 0px -44px;"></i><i class="fa fa-angle-down"></i></span>
        </div>
        @include('components.phone-country-list')
        <span class="other"></span>
        <input type="text" id="phone" class="form-control" name="phone" value="{{$phone}}" placeholder="{{trans('order.cphone')}}" v-model="cardData.phone"/>
        <label class="btn verifingBtn">{{trans('paypal.sendCode')}}</label>
        <div class="appendValidateSpace"><input id="validatePhoneCode" name="validatePhoneCode" class="form-control" placeholder="{{trans('paypal.enterVrfCode')}}">
            <label disabled id='verifyCodeBtn' class="btn validateCode">{{trans('paypal.vrfCode')}}</label>
        </div>
    </div>
</div>

<br/><p class="subHead">{{trans('paypal.addrInfo')}}</p>
<br/>
{{--国家--}}
<div class="form-group">
    <label class="col-sm-2 leftTitle" for="countryCode">{{trans('paypal.country')}}<i class="mustFill">*</i></label>
    <div class="col-sm-9 form-inline">
        <select id="countryCode" name="countryCode" class="form-control" v-model="cardData.countryCode">
            <option value="US" selected>{{trans('search.USA')}}</option>
            <option value="CH">{{trans('search.CN')}}</option>
            <option value="CA">{{trans('search.Canada')}}</option>
            <option value="other">{{trans('search.other')}}</option>
        </select>
    </div>
</div>