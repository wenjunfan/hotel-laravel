<?php
$companyName = $is_aa ? 'America Asia' : 'Unitedstars International Ltd';
$language = session('language',0);
?>
{{--授权书内容 MODAL--}}
<div class="modal fade" id="modalAuthorization" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLongTitle">Credit Card Charge Authorization Form
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h2>
            </div>
            <div class="modal-term-body">
                <h3>The reason for credit card charging:</h3>
                <p>Main Guest: {{$guest}}, Hotel Name: {{$hotelName}}, Checkin Date to Checkout Date: @{{
                    cardData.checkInOut }}</p>
                <hr>
                <h3>The charging amount: {{$price}}</h3>
                <p><strong>Name on Card: </strong> @{{ cardData.lastName }}, @{{ cardData.firstName }}</p>
                <p><strong>Card Number: </strong> @{{ cardData.number }}
                <p><strong>Expiration Date:</strong> @{{ cardData.month }}/ @{{ cardData.year }}</p>
                <p><strong>Security Code:</strong> @{{ cardData.cvc }} </p>
                <p><strong>Cardholder Phone #: </strong> @{{ cardData.phone }}</p>
                <p><strong>Credit Card Billing Address: </strong> @{{ cardData.street }} @{{
                    cardData.street2 }}
                </p>
                <p><strong>City/State/Zip/Country: </strong> @{{ cardData.city }}, @{{ cardData.state }},
                    @{{
                    cardData.zip }}, @{{ cardData.countryCode }}</p>
                <hr>
                <p>
                    I authorize {{$companyName}} to charge the credit card indicated in this
                    authorization form according to the terms outlined above. This payment authorization is
                    for
                    the goods/services described above, for the amount indicated above only, and is valid
                    for
                    one time use only. I certify that I am an authorized user of this credit card and that I
                    will not dispute the payment with my credit card company; so long as the transaction
                    corresponds to the terms indicated in this form. I also read the  {{$companyName}}'s purchasing agreement on
                    @if($language == 2)
                        @if($is_aa)
                            <a href="//www.supervacation.net/order_agreement.html" target="_blank">//www.supervacation.net/</a>
                        @else
                            <a href="https://www.usitrip.com/tw/order_agreement.html" target="_blank">//www.usitrip.com/tw/</a>
                        @endif
                    @else
                        @if($language == 1)
                            <a href="https://www.usitour.com/order_agreement.html" target="_blank">//www.usitour.com/</a>
                        @else
                            <a href="https://www.usitrip.com/order_agreement.html" target="_blank">//www.usitrip.com/</a>
                        @endif
                    @endif
                    and agree on all the terms and conditions. </p>
                <hr>
                <h3>Signature of Card Holder: @{{ cardData.lastName }}, @{{ cardData.firstName }}</h3>
                <h3>Date: <?php echo date("n/j/Y"); ?></h3>

                <hr>
                <p class="text-center">
                    <button class="btn btn-default" onclick="window.print();">
                        <i class="fa fa-print"></i>{{trans('paypal.print')}}
                    </button>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger"
                        data-dismiss="modal">{{trans('paypal.close')}}</button>
            </div>
        </div>
    </div>
</div>
{{--/授权书内容 MODAL--}}
{{--print authorization form--}}
<div class="visible-print-block">
    <h2>Credit Card Charge Authorization Form</h2>
    <hr>
    <h3>The reason for credit card charging:</h3>
    <p>Main Guest: {{$guest}}, Hotel Name: {{$hotelName}}, Checkin Date to Checkout Date: @{{
        cardData.checkInOut }}</p>
    <hr>
    <h3>The charging amount:{{$price}}</h3>
    <p><strong>Card Type: </strong> @{{ cardData.cardType }}</p>
    <p><strong>Name on Card: </strong> @{{ cardData.lastName }}, @{{ cardData.firstName }}</p>
    <p><strong>Card Number: </strong> @{{ cardData.number }}
    <p><strong>Expiration Date:</strong> @{{ cardData.month }}/ @{{ cardData.year }}</p>
    <p><strong>Security Code:</strong> @{{ cardData.cvc }} </p>
    <p><strong>Cardholder Phone #: </strong> @{{ cardData.phone }}</p>
    <p><strong>Credit Card Billing Address: </strong> @{{ cardData.street }} @{{ cardData.street2 }} </p>
    <p><strong>City/State/Zip/Country: </strong> @{{ cardData.city }}, @{{ cardData.state }}, @{{cardData.zip}}, @{{ cardData.countryCode }}</p>
    <hr>
    <p>
        I authorize {{$companyName}}to charge the credit card indicated in this authorization
        form
        according to the terms outlined above. This payment authorization is for the goods/services
        described
        above, for the amount indicated above only, and is valid for one time use only. I certify that I am
        an
        authorized user of this credit card and that I will not dispute the payment with my credit card
        company;
        so long as the transaction corresponds to the terms indicated in this form. I also read the
       's purchasing agreement on
        @if($language == 2)
            @if($is_aa)
                <a href="//www.supervacation.net/order_agreement.html" target="_blank">//www.supervacation.net/</a>
            @else
                <a href="https://www.usitrip.com/tw/order_agreement.html" target="_blank">//www.usitrip.com/tw/</a>
            @endif
        @elseif($language ==1)
                <a href="https://www.usitour.com/order_agreement.html" target="_blank">//www.usitour.com/</a>
        @else
                <a href="https://www.usitrip.com/order_agreement.html" target="_blank">//www.usitrip.com/</a>
        @endif
        and agree on all the terms and conditions. </p>
    <hr>
    <h3>Signature of Card Holder: @{{ cardData.lastName }}, @{{ cardData.firstName }}</h3>
    <h3>Date: <?php echo date("n/j/Y"); ?></h3>
</div>
{{--print authorization form--}}