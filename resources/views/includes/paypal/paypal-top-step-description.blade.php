<div class="book-step-one">
        <i class="usitrip_pc_all_img red-circle-icon pull-left"></i><div class="red-line pull-left"></div><i class="usitrip_pc_all_img red-circle-icon pull-left"></i><div class="grey-line pull-left"></div><i class="usitrip_pc_all_img grey-check-icon pull-left"></i>
    </div>
    <p class="book-explain-step">
        <span class="step-now">{{trans('book.step_one')}}</span> <span class="step-current">{{trans('book.step_two')}}</span><span class="step-final">{{trans('book.step_three')}}</span>
    </p>
    <div class="cc-online-guarantee">
        <i class="fa fa-credit-card big-card-blue"></i><span class="ccTitle">{{trans('paypal.title')}}</span>
        <i class="fa fa-check"></i><span class="smaller-en">{{trans('paypal.ccsecure')}}</span>
        <i class="fa fa-check"></i><span class="no-extra-fee">{{trans('paypal.pisecure')}}</span>
</div>

