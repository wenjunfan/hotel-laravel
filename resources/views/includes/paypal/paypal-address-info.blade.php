{{--地址--}}
<div class="form-group">
    <label class="col-sm-2 leftTitle" for="street">{{trans('paypal.add')}}<i class="mustFill">*</i></label>

    <div class="col-sm-9 form-inline">
        <input type="text" class="form-control" id="cc_street" name="street" value="" placeholder="{{trans('paypal.st')}}" v-model="cardData.street"/>
    </div>
</div>{{--地址2--}}
<div class="form-group">
    <label class="col-sm-2 leftTitle" for="street2">{{trans('paypal.add2')}}</label>
    <div class="col-sm-9 form-inline">
        <input type="text" class="form-control" id="cc_street2" name="street2" value="" placeholder="{{trans('paypal.apt')}}" v-model="cardData.street2"/>
    </div>
</div>{{--城市--}}
<div class="form-group">
    <label class="col-sm-2 leftTitle" for="city">{{trans('paypal.city')}}<i class="mustFill">*</i></label>
    <div class="col-sm-6 form-inline">
        <input type="text" class="form-control" id="cc_city" name="city" value="" v-model="cardData.city"/>
    </div>
</div>{{--州/省--}}
<div class="form-group">
    <label class="col-sm-2 leftTitle" for="state">{{trans('paypal.county')}}<i class="mustFill">*</i></label>

    <div class="col-sm-6 form-inline">
        <input type="text" class="form-control" id="cc_state" name="state" value="" v-model="cardData.state"/>
    </div>
</div>{{--邮编--}}
<div class="form-group">
    <label class="col-sm-2 leftTitle" for="zip">{{trans('paypal.zip')}}<i class="mustFill">*</i></label>
    <div class="col-sm-6 form-inline">
        <input type="text" class="form-control" id="cc_zip" name="zip" value="" v-model="cardData.zip"/>
    </div>
</div>
<div class="form-group">
    <div class="col-md-12 leftTitle">
        <div class="NeedVerify">
            <span id="ccAuth"><input type="checkbox" id="cb" name="cb" checked="checked" onclick="calc()" class="pull-left authorize-text"></span>
            <strong>{{trans('paypal.read')}}
                <a data-toggle="modal" data-target="#modalAuthorization">
                    {{trans('paypal.ccAuth')}}
                </a>
            </strong>
            {{trans('paypal.confirmTrans')}}
        </div>
        <div class="line-divider"></div>
        <button id="bt" name="bt" type="button" class="btn btn-primary ladda-button ladda-button-book payment-btn" data-style="expand-right" onclick="submitPayment(this);">{{trans('paypal.pmt')}}</button>
    </div>
</div>