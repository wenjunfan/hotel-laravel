<div class="hotel-detail-box">
    <div class="hotel-detail-top">
        <p id="hotel-name">
            {{$hotelInfo['name']}}
            @if($language == 0 && $hotelInfo['name_zh'] != "")
                ({{$hotelInfo['name_zh']}})
            @endif
        </p>
        <p id="hotel-rating">{{$hotelInfo->rating}}</p>
        <p><span class="fa fa-map-marker-alt"></span>&nbsp;
            <span id="hotel-address">{{ucwords(strtolower(str_replace(',',' ',$hotelInfo->address))). ', '. ucwords(strtolower($hotelInfo->city)). ', ' . $hotelInfo->zipcode}}</span>
        </P>
        <div class="date-box">
            <p>
                <label>{{trans('book.check_in')}}</label>
                <label style="float:right;" class="control-label checkin"></label>
            </p>
            <p>
                <label>{{trans('book.check_out')}}</label>
                <label style="float:right;" class="control-label checkout"></label>
            </p>
        </div>
        <!--取消政策start-->
        <div class="book-cancel-policy">
            <label>{{trans('book.canc')}}:</label>
            <span id="hotel-cancel">
                @if($canOrNot)
                    {{trans('book.freeb')}} {{substr(str_replace("T", " ", $canOrNot), 0, 19)}} {{trans('book.ltime')}}
                @else
                    {{trans('book.non-ref')}}
                @endif
            </span>
        </div>
        <!--取消政策end-->
        <!--价格明细start-->
        <div class="price-break-down-box">
            <p class="hotel-rate-title">{{trans('book.soc')}}</p>
            <p class="bottom-line-divider"><span class="pull-left">{{trans('book.rooms')}}</span><span class="pull-right">{{$roomDetails['roomCount'] or 1}}</span></p><br/>
            <p class="bottom-line-divider"><span class="pull-left">{{trans('book.nights')}}</span><span class="pull-right">{{$roomDetails['dayCount'] or 1}}</span></p><br/>
            <p class="bottom-line-divider"><span class="pull-left">{{trans('book.roomcost')}}</span><span class="pull-right">{{$roomDetails['currency'] == 'USD'? '$' : $roomDetails['currency']}}<span class="roomPrice">{{$roomDetails['beforeTax']/($roomDetails['dayCount']?$roomDetails['dayCount']:1)/($roomDetails['roomCount']?$roomDetails['roomCount']:1)}}</span></span></p><br/>
            @if(isset($coupon))
            <div class="applied-coupon-block bottom-line-divider">
                <p class="color-green">@lang('book.coupon_applied_title')</p>
                <p class="coupon-amount color-green"><span>@lang('book.coupon_applied_desc')</span><span id="couponAmount" class="pull-right">-${{ $coupon->amount }}</span></p>
            </div>
            @endif
            <p class="bottom-line-divider"><span class="pull-left">{{trans('book.tax')}}</span><span class="pull-right">{{$roomDetails['currency'] == 'USD'? '$' : $roomDetails['currency']}}<span id="roomTax">{{$roomDetails['tax']}}</span></span></p><br/>
            @if($cleanFee !== 0)
                <p class="bottom-line-divider" id="cleanFee">
                    <span class="pull-left">{{trans('book.clean_fee')}}</span>
                    <span class="pull-right"><span class="clean-fee">${{$cleanFee}}</span>
                </span>
                </p>
                <br/>
            @endif
            <p class="bottom-line-divider"><span class="pull-left pay-today">{{trans('book.totalcharge')}}</span><span class="pull-right hotel-total-price">{{$roomDetails['currency'] == 'USD'? '$' : $roomDetails['currency']}}<span class="totalPrice">{{$totalPrice}}</span></span></p>
        </div>

        @if(isset($tempInfo->mandatory_fee) && $tempInfo->mandatory_fee != '' && $tempInfo->mandatory_fee != 0)
            {{--统一使用searchRoom的mandatory_fee的值，如果没有，记录的是checkRoom的--}}
            <p class="bottom-line-divider" id="HotelDue">
                <span class="pull-left">{{trans('book.totalchargeHotel')}}  ({{trans('book.resortFee')}}):</span>
                <span class="pull-right HotelDue">
                        <span class="hotelCharge">{{$roomDetails['currency'] == 'USD'? '$' : $roomDetails['currency']}}
                            <span class="roomPrice">{{$tempInfo->mandatory_fee}}</span>
                        </span>
                </span>
            </p>
        @endif
    <!--价格明细end-->
    </div>
    <!--注意事项start-->
    <div class="hotel-detail-bottom note-and-why-us-box">
        <p class="note-title">{{trans('book.fineprint')}}</p>
        <div id="hotel-comment">{!! isset($roomDetails['comments'][0]['comments'] ) && $roomDetails['comments'][0]['comments'] != '' ? $roomDetails['comments'][0]['comments']: 'None' !!}</div>
        @include('includes.book.book-why-us-box')
    </div>
    <!--注意事项end-->
    {{--右侧 end--}}
</div>