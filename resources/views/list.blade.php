@extends( !$is_b  ?  'layouts.master' : 'layouts.master-b' )
<?php
$domain = $is_usitour ? 'Usitour' : $is_aa ? 'America Asia' :'Usitrip';
//有中文名称附加在后面
$titleDestinationName = (count(explode('(', $destinationName)) > 1) ? trim(explode(',', explode('(', $destinationName)[1])[0], ")") : $titleDestinationName = explode(',', $destinationName)[0];
?>

@section('title', trans('tdk.title_list', ['name' => $titleDestinationName, 'domain' => $domain]))

@section('metas')
<meta name="Keywords" content="{{ trans('tdk.title_list', ['name' => $titleDestinationName]) }}">
<meta name="description" content="{{ trans('tdk.description_list', ['name' => $titleDestinationName, 'domain' => $domain]) }}">
@endsection

@section('gtag_events')
    @if(config('app.enable_tracking') && !$is_b && !$is_aa)
        @php
            $subscribe_con_label = $is_usitour ? 'AW-825564842/iqv3CIKmgoYBEKq91IkD' : 'AW-825564842/BTHxCKb47noQqr3UiQM';
        @endphp
        <!-- Event snippet for 领券 conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
        <script>
            function gtag_report_conversion_subscribe(url) {
                var callback = function() {
                    if (typeof(url) != 'undefined') {
                        window.location = url;
                    }
                };
                gtag('event', 'conversion', {
                    'send_to': '{{ $subscribe_con_label }}',
                    'value': 1.0,
                    'currency': 'USD',
                    'event_callback': callback,
                });
                return false;
            }
        </script>

        {{--adwords remarketing tag--}}
        <script>
            var today = new Date();
            todayStr = today.getFullYear() + '-' + today.getMonth() + 1 + '-' + today.getDate();
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            tomorrowStr = tomorrow.getFullYear() + '-' + tomorrow.getMonth() + 1 + '-' + tomorrow.getDate();
            var checkin = window.localStorage ? window.localStorage.getItem('checkin') : todayStr;
            var checkout = window.localStorage ? window.localStorage.getItem('checkout') : tomorrowStr;

            gtag('event', 'page_view', {
                'send_to': 'AW-825564842',
                'hrental_enddate': checkout,
                'hrental_id': '{{$destinationId}}',
                'hrental_startdate': checkin,
                'hrental_totalvalue': 0,
            });
        </script>
    @endif
@endsection

@section('css')
    <link rel="canonical" href="{{ config('app.url') }}/list/{{ $destinationId }}.html">
    {{--webpack css--}}
    <link href="{{ asset('assets/css/pc/plugins.list.css') }}" rel="stylesheet" >

    {{--customized css: autocomplete.css is been packed above--}}
    <link href="{{ asset('assets/css/pc/list.css') }}?030419" rel="stylesheet">

    @if($language == 1)
        <style>
            #open-sort-price {
                width: 207px;

            }
            #open-sort-rate {
                width: 204px;
            }
            #open-sort-name {
                width: 170px;
            }
        </style>
    @endif
@endsection

@section('content')
    @if(!$is_b && !$is_aa)
        @include('components.nav-bar')
    @endif
    {{--top search info show--}}
    <div class="change-search-toggle" id="list-search-info-toggle">
        <div class="top-center">
            <div class="pull-left usitrip-bright"><i class="fa fa-search top-search-icon" aria-hidden="true"></i></div>
            <div class="pull-left">
                <span id="displayDestination" class="font-14">@lang('list.destination')</span><br/><span class="change-search list-long-text">{{$destinationName}}</span>
            </div>
            <div id="displayDates" class="pull-left change-search-subtitle">
                @lang('list.dates')<br/><span class="change-search">
				<?php
                    $dayofWeek = array(trans('list.7'), trans('list.1'),
                        trans('list.2'), trans('list.3'),
                        trans('list.4'),trans('list.5'),trans('list.6'));
                    $checkinWoD = date('w', strtotime($checkin));
                    $checkoutWoD = date('w', strtotime($checkout));
                    ?>
                    {{$checkin}} {{$dayofWeek[$checkinWoD]}} - {{$checkout}} {{$dayofWeek[$checkoutWoD]}}</span>
            </div>
            <div id="displayRoomCount" class="pull-left change-search-subtitle">
                @lang('list.guestRoom')<br/><span class="change-search">{{$roomCount}}</span>
            </div>
            <div id="updateSearchBtn" class="pull-right change-search-subtitle top-search-text">
                <br/><span class="usitrip-pure font-18 change-search">@lang('list.changeSearches')</span>
            </div>
        </div>
    </div>


    {{--form start--}}
    <form class="form-horizontal" role="form" id="search-form" name="search-form" onkeydown="if(event.keyCode==13){return false;}">

        {{--顶部搜索栏start--}}
        @include('includes.list.list-top-search')

        <div class="all-wrapper">
            <div class="ad-question-call">
                <h1 class="left-top-search-result">
                    <span class="list-long-text">{{$destinationName}}</span>: <span id="totalHotels"> ... </span>@lang("list.properties")
                </h1>
                <span class="question">@lang("list.question")</span>
            </div>

            <div class="form-all-list">
                <!--左侧搜索栏 start-->
            @include('includes.list.list-left-filter')
            <!--左侧搜索栏 end-->

                {{--右侧显示结果 start--}}
                @include('includes.list.list-right-result')
                {{--右侧显示结果 end--}}
            </div>
        </div>
    </form>

    <div id="mapWrapper" class="map-wrapper">
        <div class="map-banner">
            <div class="map-banner-col1" onclick="backToList()">
                <i class="fa fa-angle-left"></i> <span id="mapBannerDesName" class="map-banner-desName"></span>
            </div>
            <button onclick="backToList()">@lang('list.close_map') </button>
        </div>
        <div id="infowindow" class="infowindow"></div>
        <div id="hotelsMap" class="hotels-map"></div>
    </div>

    @if(!$is_b && !$is_aa)
        {{--affilaite register box--}}
        <div id="affiliateBox">
            <a href="/affiliate/register"><img title="affiliate register link" src="/img/general/affiliate/affiliate_banner_sm_{{$language}}.png" width="880px" height="87px"></a>
        </div>

        {{--subscribe box--}}
        <form action="#" class="subscribe-box" id="listCouponBox">
            <h2 class="subscribe-title">
                @lang('list.subLabel1') <span class="color-red">@lang('list.subLabel2')!</span>
            </h2>
            <div class="subscribe-input-wrapper">
                <input type="email" name="subscribeEmail" class="subscribe-input" id="subscribeEmail" placeholder="@lang('list.subEmailHint')" required>
                <button type="submit" class="subscribe-btn loadingBtnList" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">@lang('list.subscribe')</button>
            </div>
            <div class="subscribe-messages">
                <div class="message-1 message">@lang('subscribe.subscribe_send_mem_again')</div>
                <div class="message-2 message">@lang('subscribe.subscribe_send_mem')</div>
            </div>
        </form>
    @endif
@endsection

@section('scripts')
    <script>
        var page_lang = "{{$language}}"; // number
        var is_usitour = '{{$is_usitour}}';
        var destinationName = '{{ $destinationName }}';
        var destinationId = '{{ $destinationId }}';
        var destinationCity = "{{ $destinationCity }}";
        var scenicId = '{{ $scenicId }}';
        var scenicName = '{{ $scenicName or '' }}';
        var checkin = '{{ $checkin }}';
        var checkout = '{{ $checkout }}';
        var roomCount = parseInt('{{ $roomCount }}');
        var dayCount = parseInt('{{ $dayCount }}');
        var adultCount1 = "{{ $adultCount1  }}";
        var childCount1 = '{{ $childCount1 }}';
        var childAges = {!! json_encode($childAges) !!};
        if (childCount1 > 0 && !childAges) {
            childAges = [];
            for(var i = 0; i < childCount1; i++) {
                childAges.push(12);
            }
        }
        var app_map_key = "{{ config('app.google_maps_api_key') }}";
        var cityName = "{{explode(',',$destinationName)[0]}}"; //todo

        // filters
        var page = {!! $pagePos !!};
        var showAllDiscount = '{{$showAllDiscount or false}}';
        var orderBy = '{{ $orderBy }}';
        var sortMethod = '{{ $sort }}';
        var key = '{{ $key }}';
        var currentPage = parseInt('{{ $pagePos }}');
        var rating = '{{ $rating }}';
        rating = rating.length > 0 ? rating.split(',') : [];
        var price = "{{ $price }}";
        price = price.length > 0 ? price.split(',') : [];
        var scenicAreaArr = '{{ $scenicArea }}';
        scenicAreaArr = scenicAreaArr.length > 0 ? scenicAreaArr.split(',') : [];
        var chainNames = '{{ $chainNames }}';
        chainNames = chainNames.length > 0 ? chainNames.split(',') : []; // dynamic

        var no_lowest_html = '<div class="no-lowprice"><i class="fa fa-check usitrip-hover-tag"></i>{{trans("list.no_cc_fee")}}<br/><i class="fa fa-check usitrip-hover-tag"></i>{{trans("list.hotel_confirm_instant")}}</div>';
        var lowest_html = '<div class="hover-lowprice">' +
        '<span class="lowest-tooltip">{{trans("list.low_price_1")}}<br>{{trans("list.low_price_2")}}<br>{{trans("list.low_price_3")}}<br>{{trans("list.low_price_4")}}</span>' +
        '<i class="fa fa-check usitrip-hover-tag"></i>{{trans("list.price_guarantee")}}<br/><i class="fa fa-check usitrip-hover-tag"></i>{{trans("list.no_cc_fee")}}<br/><i class="fa fa-check usitrip-hover-tag"></i>{{trans("list.hotel_confirm_instant")}}</div>';

        {{--start language--}}
        var lang_checkin_valid = "{{trans('search.checkinvalid')}}";
        var lang_checkout_valid = "{{trans('search.checkoutvalid')}}";
        var lang_how_to_active = "{{trans('search.howtoActivate')}}";
        var lang_format = "{{trans('search.format')}}";
        var lang_group_alert = "{{trans('search.groupalert')}}";
        var lang_limit = "{{trans('search.limit')}}";
        var lang_login_expired = "{{trans('search.loginExpired')}}";
        var lang_login = "{{trans('search.login')}}";
        var lang_more_option = "{{trans('search.moreoption')}}";
        var lang_next = "{{trans('search.Next')}}";
        var lang_no_destination_inp = "{{trans('search.noDestinationInput')}}";
        var lang_not_active = "{{trans('search.notActivate')}}";
        var lang_no_result = "{{trans('search.noResult')}}";
        var lang_previous = "{{trans('search.Previous')}}";
        var lang_PricePerDay = "{{trans('search.PricePerDay')}}";
        var lang_rating = "{{trans('search.Rating')}}";
        var lang_reset = "{{trans('search.Reset')}}";
        var lang_see_others = "{{trans('search.seeothers')}}";
        var lang_sout = "{{trans('search.sout')}}";

        var langList = {!! json_encode(trans('list')) !!};
        {{-- end language --}}

    </script>
    {{--webpack js--}}
    {{--customized js: autocomplete.js customized and jquery original one are all been packed above, hot city customized also packed above--}}
    <script src="{{ asset('assets/js/pc/plugins.list.js') }}"></script>
    {{--google maps api--}}
    @if($language == 0)
        <script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_maps_api_key') }}&callback=initMap&language=zh-CN" async defer></script>
    @elseif($language == 2)
        <script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_maps_api_key') }}&callback=initMap&language=zh-TW" async defer></script>
    @else
       <script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_maps_api_key') }}&callback=initMap&language=en" async defer></script>
    @endif
@endsection