@extends('layouts.master')

@section('css')
    <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <style>
        #placing_order {
            padding-top: 20vh;
        }
        #placing_order-buttons {
            padding: 20px 0 20px;
        }
    </style>
@endsection

@section('content')
    <div style="text-align: center; width:99vw; height:99vh;">
        <div id="loadingBlock">
            <div id="loadingSpinner" class="hide">
                <div class="sk-spinner sk-spinner-three-bounce" style="height: 300px;padding-top: 120px;">
                    <div class="sk-bounce1"></div>
                    <div class="sk-bounce2"></div>
                    <div class="sk-bounce3"></div>
                </div>
            </div>
        </div>
    </div>

    <div id="placing_order" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h2>
                        {{ trans('book.paymentCheck') }}
                    </h2>

                    <div id="placing_order-buttons">
                        <button type="button" class="btn btn-success btn-block" onclick="checkPayment()">
                            {{ trans('book.confirm') }}
                        </button>
                        {{--<button type="button" class="btn btn-secondary pull-right" onclick="checkPayment(true)">--}}
                            {{--{{ trans('book.cancel') }}--}}
                        {{--</button>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    {{--这里可以定义本页需要的php变量转js变量--}}
    <script>
    var reference = '{{$reference}}';
    var source = '<?= isset($_COOKIE['source']) ? $_COOKIE['source'] : ''; ?>';
    var vendorType = '{{$vendor}}';
    {{--language start--}}
    var lang_warning = "{{trans('continue-order.warning')}}";
    var lang_failed = "{{trans('book.failed')}}";
    var lang_refunded = "{{trans('book.refunded')}}";
        {{--language end--}}
    </script>
    {{--等待压缩js--}}
    <script>
        var checkRequest = null;
        $(function () {
            if (window.localStorage) {
                localStorage.removeItem('reference');
                localStorage.removeItem('temp');
            }
            swal('',lang_warning,'', "warning");
            if (vendorType === 'failed') {
              swal({
                title:lang_failed,
                text: lang_refunded,
                type: 'warning',
                confirmButtonText: "OK",
              }, function () {
                window.location.href = '/';
              });
            } else {
                var $modal = $('#placing_order').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $modal.modal('show');
            }
        });

        function checkPayment () {
            $('#placing_order').modal('hide');
            $('#loadingSpinner').removeClass('hide');
            var getUrl = "/ccpay/inquire";
            $.get(getUrl, {
                'reference': reference
            }, function (inquireData) {
                var hotelPath = '/hotel/'+ inquireData.options.hotelId +'.html?'+inquireData.options.params;
                if (inquireData.type === 'charge' && inquireData.status === 'success') {
                    if (inquireData.hasOwnProperty('options') && inquireData.options.length > 0) {
                        var hotelData = {
                            'id': inquireData.options.hotelId,
                            'name': inquireData.options.hotelName
                        };
                        onPaymentComplete(hotelData);
                    }
                    swal("{{trans('book.placeOrder')}}","", "success");

                    $.post('{{url("/ccpay/order")}}', {
                        '_token': $('meta[name="csrf_token"]').attr('content'),
                        'Reorderid': reference,
                        'vendor': vendorType
                    }, function (data) {
                        checkOrderStatus(data);
                    });
                } else if (inquireData.type === 'refund' && inquireData.status === 'refunded') {
                  swal({
                    title:"{{trans('book.failed'). ', ' .trans('book.refunded')}}",
                    type: 'warning',
                    confirmButtonText: "OK",
                  }, function () {
                    window.location.href = hotelPath;
                  });
                } else {
                  swal({
                    title:"{{trans('book.paymentFailed')}}",
                    type: 'warning',
                    confirmButtonText: "OK",
                  }, function () {
                    window.location.href = hotelPath;
                  });
                }
            });
        }

        function checkOrderStatus (data) {
            if (data.success && data.voucherUrl) {
                window.location.replace(data.voucherUrl);
            } else if (data.message === 'wait') {
                var timesCalled = 0;
                checkRequest = setInterval(function () {
                    if (timesCalled === 6) {
                        clearInterval(checkRequest);
                    }
                    $.getJSON("{{url('/ccpay/order/check')}}", {
                        'reference': reference
                    }, function (data) {
                        if (data.success) {
                            window.location.href = data.voucherUrl;
                        } else {
                          swal("","{{trans('continue-order.waiting_order_status')}}", "warning");
                        }
                    });
                    timesCalled++;
                }, 5000);
            } else if (data['errorId'] === 'ER1401' || data['errorId'] === 'ER1301' || data['errorId'] === 'ER1501') {
              swal({
                title:"{{trans('book.soldout'). ', ' .trans('book.refunded')}}",
                type: 'warning',
                confirmButtonText: "OK",
              }, function () {
                window.location.href = '/';
              });
            } else if (data['errorId'] === 'ER1413') {
              swal({
                title:"{{trans('book.soldout'). ', ' .trans('book.refunded')}}",
                type: 'warning',
                confirmButtonText: "OK",
              }, function () {
                window.location.href = '/';
              });
            } else if (data['errorId'] === 'ER1411' || data['errorId'] === 'ER1402' || data['errorId'] === 'ER1302') {
              swal({
                title:"{{trans('book.pchange'). ', ' .trans('book.refunded')}}",
                type: 'warning',
                confirmButtonText: "OK",
              }, function () {
                window.location.href = '/';
              });
            } else if (data['errorId'] === 'ER1414') {
              swal({
                title: '{{trans('book.duplicate')}}'.replace('xxx', data['errorMessage']),
                type: 'warning',
                confirmButtonText: "OK",
              }, function () {
                window.location.href = '/';
              });
            } else if (data['errorId'] === 'ER1004') {
              swal({
                title:"{{trans('book.timeout'). ', ' .trans('book.refunded')}}",
                type: 'warning',
                confirmButtonText: "OK",
              }, function () {
                window.location.href = '/';
              });
            } else if (data['errorId'] === 'IT1001') {
              swal({
                title:"{{trans('book.cnetwork')}}",
                type: 'warning',
                confirmButtonText: "OK",
              }, function () {
                window.location.href = '/';
              });
            } else if (data === '') {
              var message;
              if( language === 0){
                message = '您已成功预订，我们会稍后发送您的预订成功邮件';
              }else{
                message = 'Thanks! Your hotel reservation is confirmed, and your confirmation Email is on the way';
              }
              swal("",message, "success");
            }else if (data['errorId'] === 'IT1002') {
              swal({
                title:"{{trans('book.paymentFailed')}}",
                type: 'warning',
                confirmButtonText: "OK",
              }, function () {
                window.location.href = '/';
              });
            } else if (data['errorId'] === 'ER1415') {
                swal({
                    title: "{{trans('book.confirmDu')}}",
                    type: 'warning',
                    confirmButtonText: "{{trans('book.confirm')}}",
                    showCancelButton: true,
                    cancelButtonText: "{{ trans('book.cancel') }}",
                }, function (confirmed) {
                    if (confirmed) {
                        $.post('/duplicate/proceed', {
                            '_token': $('meta[name="csrf_token"]').attr('content'),
                            'action': 'continue',
                            'Reorderid': reference,
                            'vendor': vendorType,
                        }, function (data) {
                            checkOrderStatus(data);
                        });
                    } else {
                        $.post('/duplicate/proceed', {
                            '_token': $('meta[name="csrf_token"]').attr('content'),
                            'action': 'cancel',
                            'Reorderid': reference,
                            'vendor': vendorType
                        }, function (data) {
                            if (!data.success) {
                                data['message'] = '{{ trans('book.failed') }}';
                            }
                            swal({
                                title: data.message,
                                type: 'warning',
                                confirmButtonText: "OK",
                            }, function () {
                                window.location.href = '/';
                            });
                        });
                    }
                });
            } else {
              swal({
                title:"{{trans('book.failed'). ', ' .trans('book.refunded')}}",
                type: 'warning',
                confirmButtonText: "OK",
              }, function () {
                window.location.href = '/';
              });
            }
        }

    </script>
@endsection