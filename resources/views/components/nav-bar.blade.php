{{--c端导航栏--}}
<?php
    $tourDomain = $language == 2 ? 'https://www.usitrip.com/tw' : ($language == 1 ? 'https://www.usitour.com' : 'https://www.usitrip.com')
?>
@if($language == 0)
    <!-- 导航栏 begin 簡體 -->
    <div class="m-nav-wrap" id="id_nav_head">
        <div class="g-wrap uifix" id="id_g_wrap">
            <div class="m-nav-sort fl" id="id_nav_sort">
                <span class="m-nav-sort-tit">热门目的地<i></i></span>
                <div class="m-sort-box " id="id_sort_box">
                    <div class="nav-sort-item">
                        <div class="sort-item-left">
                            <div class="item-title">
                                <div class="title-content">
                                    <a href="{{$tourDomain}}/america/" target="_blank" style="" class="title ">美国</a>
                                    <a href="{{$tourDomain}}/america/rz-2100" target="_blank" style=" " class="hot-tip ">独家全程加州一号公路</a>
                                </div>
                            </div>
                            <i class="nav-right-icon"></i>
                            <ul class="item-recommend">
                                <li><a href="{{$tourDomain}}/west-coast/" target="_blank" class="" style="color: #F77700; " >美国西海岸</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-1/" target="_blank" class="" style=" " >洛杉矶</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-2/" target="_blank" class="" style=" " >旧金山</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-9/" target="_blank" class="" style=" " >黄石公园</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-295/" target="_blank" class="" style=" " >西雅图</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-588/" target="_blank" class="" style="color: #F77700; " >大峡谷</a></li>
                                <li><a href="{{$tourDomain}}/east-coast/" target="_blank" class="" style="color: #F77700; " >美国东海岸</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-66/" target="_blank" class="" style=" " >纽约</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-290/" target="_blank" class="" style=" " >迈阿密</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-204/" target="_blank" class="" style=" " >奥兰多</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-65/" target="_blank" class="" style=" " >华盛顿</a></li>
                                <li><a href="{{$tourDomain}}/hawaii/" target="_blank" class="" style="color: #F77700; " >夏威夷</a></li>
                            </ul>
                        </div>
                        <div class="sort-item-right">
                            <div>
                                <div class="item-right-list">
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/west-coast/" target="_blank" class="" style=" ">美国西海岸</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/america/vc-1/" target="_blank" class="" style="color: #F77700; ">洛杉矶</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-3/" target="_blank" class="" style=" ">拉斯维加斯</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-2/" target="_blank" class="" style=" ">旧金山</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-295/" target="_blank" class="" style=" ">西雅图</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-10/" target="_blank" class="" style=" ">盐湖城</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-565/" target="_blank" class="" style=" ">圣地亚哥</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/east-coast/" target="_blank" class="" style=" ">美国东海岸</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/america/vc-66/" target="_blank" class="" style=" ">纽约</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-71/" target="_blank" class="" style=" ">波士顿</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-65/" target="_blank" class="" style=" ">华盛顿</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-93/" target="_blank" class="" style="color: #F77700; ">尼亚加拉瀑布</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-477/" target="_blank" class="" style=" ">芝加哥</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-67/" target="_blank" class="" style=" ">费城</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="javascript:;" class="" style=" cursor: default;">国家公园</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/america/vc-9/" target="_blank" class="" style="color: #F77700; ">黄石公园</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-588/" target="_blank" class="" style=" ">大峡谷</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-564/" target="_blank" class="" style="color: #F77700; ">羚羊彩穴</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-32/" target="_blank" class="" style=" ">拱门国家公园</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-51/" target="_blank" class="" style=" ">优胜美地</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-34/" target="_blank" class="" style=" ">大提顿公园</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/hawaii/" target="_blank" class="" style=" ">夏威夷</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/america/vc-162/" target="_blank" class="" style=" ">檀香山</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-2700/" target="_blank" class="" style=" ">茂宜岛</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-163/" target="_blank" class="" style=" ">珍珠港</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-2675/" target="_blank" class="" style=" ">火山大岛</a></dd>
                                        <dd><a href="{{$tourDomain}}/hawaii/vc-2699/" target="_blank" class="" style=" ">欧胡岛</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/alaska/" target="_blank" class="" style=" ">阿拉斯加</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/america/vc-934/" target="_blank" class="" style=" ">费尔班克斯</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-400/" target="_blank" class="" style=" ">安克雷奇</a></dd>
                                        <dd><a href="{{$tourDomain}}/alaska/vc-978/" target="_blank" class="" style=" ">珍娜温泉</a></dd>
                                        <dd><a href="{{$tourDomain}}/alaska/vc-2833/" target="_blank" class="" style=" ">北极圈</a></dd>
                                        <dd><a href="{{$tourDomain}}/alaska/vc-405/" target="_blank" class="" style=" ">迪纳利国家公园</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/florida/" target="_blank" class="" style=" ">佛罗里达</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/america/vc-290/" target="_blank" class="" style=" ">迈阿密</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-204/" target="_blank" class="" style="color: #F77700; ">奥兰多</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-286/" target="_blank" class="" style=" ">墨西哥湾</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-1633/" target="_blank" class="" style=" ">西锁岛</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-796/" target="_blank" class="" style=" ">大沼泽国家公园</a></dd>
                                        <dd><a href="{{$tourDomain}}/cruise/" target="_blank" class="" style=" ">加勒比邮轮</a></dd>
                                    </dl>
                                </div>

                                <div class="item-right-bottom">
                                    <div class="item-line-play">
                                        <h2 class="title">
                                            <a href="javascript:;" class="" style=" cursor: default;">热门玩法</a>
                                        </h2>
                                        <div class="play-content">
                                            <a href="{{$tourDomain}}/america/rz-2100/" target="_blank" class="" style="color: #F77700; ">走四方独家全程加州一号公路<i></i></a>
                                            <a href="{{$tourDomain}}/america/hd-2134/" target="_blank" class="" style="color: #F77700; ">冬季黄石 <i></i></a>
                                            <a href="{{$tourDomain}}/america/hd-2140/" target="_blank" class="" style="color: #F77700; ">东南峡谷,羚羊谷,四大国家公园 <i></i></a>
                                            <a href="{{$tourDomain}}/alaska/ts-2018/" target="_blank" class="" style="color: #F77700; ">Alaska星球小屋  <i></i></a>
                                            <a href="{{$tourDomain}}/america/ts-479/" target="_blank" class="" style=" ">小众团 <i></i></a>
                                            <a href="{{$tourDomain}}/america/ts-1476/" target="_blank" class="" style="color: #F77700; ">房车旅游 <i></i></a>
                                            <a href="{{$tourDomain}}/america/ts-1908/" target="_blank" class="" style=" ">美东名校巡礼 <i></i></a>
                                            <a href="{{$tourDomain}}/group_buys/" target="_blank" class="" style=" ">限时团购 <i></i></a>
                                        </div>
                                    </div>
                                    <ul class="icon-nav">
                                        <li>
                                            <a href="{{$tourDomain}}/ticket/" target="_blank">
                                                <i class="sdc-r-ico3"></i>
                                                <p style="">门票</p>
                                            </a></li>
                                        <li><a href="{{$tourDomain}}/cruise/" target="_blank"> <i class="sdc-r-ico11"></i>
                                                <p style=" ">邮轮</p>
                                            </a></li>
                                        <li><a href="{{$tourDomain}}/baoche/" target="_blank"> <i class="sdc-r-ico4"></i>
                                                <p style="">包车</p>
                                            </a></li>
                                        <li><a href="{{$tourDomain}}/transfer/" target="_blank"> <i class="sdc-r-ico5"></i>
                                                <p style="">接送机</p>
                                            </a></li>
                                        <li><a href="{{$tourDomain}}/tours-departing-from-china/" target="_blank">
                                                <i class="sdc-r-ico1"></i>
                                                <p style="">中国出发</p>
                                            </a></li>
                                        <li><a href="/" target="_blank">
                                                <i class="sdc-r-ico6"></i>
                                                <p style="">酒店</p>
                                            </a></li>
                                        <li><a href="{{$tourDomain}}/visa/" target="_blank"> <i class="sdc-r-ico8"></i>
                                                <p style="">签证</p>
                                            </a></li>
                                        <li><a href="{{$tourDomain}}/wifi/" target="_blank"> <i class="sdc-r-ico10"></i>
                                                <p style="">电话卡/wifi</p>
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sort-item">
                        <div class="sort-item-left">
                            <div class="item-title">
                                <div class="title-content">
                                    <a href="{{$tourDomain}}/canada/" target="_blank" style="" class="title ">加拿大</a>
                                    <a href="{{$tourDomain}}/web_action/Winter_canada/" target="_blank" style=" " class="hot-tip ">冬季“加”年华</a>
                                </div>
                                <div class="title-content">
                                    <a href="{{$tourDomain}}/south-america/" target="_blank" style=" " class="title ">南美</a>
                                </div>
                            </div>

                            <i class="nav-right-icon"></i>
                            <ul class="item-recommend">
                                <li><a href="{{$tourDomain}}/canada/vc-270/" target="_blank" class="" style="">温哥华</a></li>
                                <li><a href="{{$tourDomain}}/canada/vc-478/" target="_blank" class="" style="">多伦多</a></li>
                                <li>
                                    <a href="{{$tourDomain}}/canada/vc-365/" target="_blank" class="" style="color: #F77700;">班芙公园</a>
                                </li>
                                <li><a href="{{$tourDomain}}/mexico/vc-370/" target="_blank" class="" style="">墨西哥城</a>
                                </li>
                                <li>
                                    <a href="{{$tourDomain}}/mexico/vc-371/" target="_blank" class="" style="color: #F77700;">坎昆</a>
                                </li>
                                <li>
                                    <a href="{{$tourDomain}}/mexico/vc-637/" target="_blank" class="" style="color: #F77700;">奇琴伊察</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sort-item-right">
                            <div>
                                <div class="item-right-content">
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/western-canada/" target="_blank" class="" style=" ">加拿大西部</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/canada/vc-270/" target="_blank" class="" style=" ">温哥华</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-2813/" target="_blank" class="" style="color: #F77700; ">卡尔加里</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-296/" target="_blank" class="" style=" ">维多利亚</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-1781/" target="_blank" class="" style=" ">惠斯勒</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/eastern-canada/" target="_blank" class="" style=" ">加拿大东部</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/canada/vc-478" target="_blank" class="" style=" ">多伦多</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-519/" target="_blank" class="" style=" ">蒙特利尔</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-346/" target="_blank" class="" style=" ">渥太华</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-138" target="_blank" class="" style=" ">魁北克</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-93/" target="_blank" class="" style="color: #F77700; ">尼亚加拉瀑布</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="javascript:;" class="" style=" cursor: default;">国家公园</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/canada/vc-365/" target="_blank" class="" style=" ">班夫国家公园</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-545/" target="_blank" class="" style="color: #F77700; ">洛基山国家公园</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-2815/" target="_blank" class="" style=" ">幽鹤国家公园</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-2338/" target="_blank" class="" style=" ">冰川国家公园</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a class="" style=" cursor: default;">极光</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/canada/vc-791/" target="_blank"  class="" style=" ">黄刀/耶洛奈夫</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-8863/" target="_blank"  class="" style=" ">白马/怀特霍斯</a></dd>
                                    </dl>
                                    <div class="right-content-play">
                                        <h2 class="title">
                                            <a href="" class="" style="cursor: default;">热门玩法</a>
                                        </h2>
                                        <div class="play-content">
                                            <a href="{{$tourDomain}}/canada/hd-2138/" target="_blank" style="color: #F77700; ">冬季班芙 <i></i></a>
                                            <a href="{{$tourDomain}}/canada/vc-791/" target="_blank" style="color: #F77700; ">黄刀镇追极光 <i></i></a>
                                            <a href="{{$tourDomain}}/canada/vc-431/" target="_blank" class="" style=" ">硫磺山泡温泉 <i></i></a>
                                            <a href="{{$tourDomain}}/canada/ts-812" target="_blank" class="" style=" ">直升机飞跃千岛湖 </a>
                                        </div>
                                        <div class="line-play-img">
                                            <a href="{{$tourDomain}}/canada/vc-791/" target="_blank">
                                                <img src="{{$tourDomain}}/images/banners/2019/11/Home_Page_Left_Menu_400x60_22_400x60_20191106195510.jpg" alt="全球最佳观测极光之城-黄刀镇">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-right-content">
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/mexico/" target="_blank" class="" style=" ">墨西哥</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/mexico/vc-371/" target="_blank" class="" style="color: #F77700; ">坎昆</a></dd>
                                        <dd><a href="{{$tourDomain}}/mexico/vc-370/" target="_blank" class="" style=" ">墨西哥城</a></dd>
                                        <dd><a href="{{$tourDomain}}/mexico/vc-672/" target="_blank" class="" style=" ">梅里达</a></dd>
                                        <dd><a href="{{$tourDomain}}/mexico/vc-637/" target="_blank" class="" style="color: #F77700; ">奇琴伊察</a></dd>
                                        <dd><a href="{{$tourDomain}}/mexico/vc-1002/" target="_blank" class="" style=" ">特奥蒂瓦坎</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/puerto-rico/" target="_blank" class="" style=" ">波多黎各</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/puerto-rico/vc-1428/" target="_blank" class="" style=" ">圣胡安</a></dd>
                                        <dd><a href="{{$tourDomain}}/puerto-rico/vc-8823/" target="_blank" class="" style="color: #F77700; ">库莱布拉岛</a></dd>
                                        <dd><a href="{{$tourDomain}}/puerto-rico/vc-9430/" target="_blank" class="" style=" ">弗拉门戈海滩</a></dd>
                                        <dd><a href="{{$tourDomain}}/puerto-rico/vc-8825/" target="_blank" class="" style="color: #F77700; ">荧光湖</a></dd>
                                        <dd><a href="{{$tourDomain}}/puerto-rico/vc-9433/" target="_blank" class="" style=" ">圣胡安老城区</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/cuba/" target="_blank" class="" style=" ">古巴</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/cuba/vc-8675/" target="_blank" class="" style="color: #F77700; ">哈瓦那</a></dd>
                                        <dd><a href="{{$tourDomain}}/cuba/vc-8672/" target="_blank" class="" style=" ">巴拉德罗</a></dd>
                                        <dd><a href="{{$tourDomain}}/cuba/vc-8673/" target="_blank" class="" style=" ">革命广场</a></dd>
                                        <dd><a href="{{$tourDomain}}/cuba/vc-8709/" target="_blank" class="" style=" ">国家艺术馆</a></dd>
                                        <dd><a href="{{$tourDomain}}/cuba/vc-8707/" target="_blank" class="" style=" ">哈瓦那老城区</a></dd>

                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/latin-america/" target="_blank" class="" style=" ">南美洲</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/brazil/" target="_blank" class="" style=" ">巴西</a></dd>
                                        <dd><a href="{{$tourDomain}}/argentina/" target="_blank" class="" style=" ">阿根廷</a></dd>
                                        <dd><a href="{{$tourDomain}}/south-america/vc-9454/" target="_blank" class="" style=" ">智利</a></dd>
                                        <dd><a href="{{$tourDomain}}/south-america/vc-8725/" target="_blank" class="" style="color: #F77700; ">马丘比丘</a></dd>
                                        <dd><a href="{{$tourDomain}}/bolivia/" target="_blank" class="" style="color: #F77700; ">玻利维亚</a></dd>
                                    </dl>
                                    <div class="right-content-play">
                                        <h2 class="title">
                                            <a href="" class="" style=" cursor: default;">热门玩法</a>
                                        </h2>
                                        <div class="play-content">
                                            <a href="{{$tourDomain}}/south-america/ts-1914/" target="_blank" class="" style=" ">夏季海岛风情游 <i></i></a>
                                            <a href="{{$tourDomain}}/south-america/fc-371_d-0-1/" target="_blank" class="" style=" ">坎昆自由行推荐 </a>
                                        </div>
                                        <div class="line-play-img">
                                            <a href="{{$tourDomain}}/south-america/vc-371/" target="_blank">
                                                <img src="{{$tourDomain}}/images/banners/Home_Page_Left_Menu_400x60_23_400x60_15554945769529.jpg" alt="坎昆">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sort-item">
                        <div class="sort-item-left">
                            <div class="item-title">
                                <div class="title-content">
                                    <a href="{{$tourDomain}}/europe/" target="_blank" style="" class="title ">欧洲</a>

                                </div>
                                <div class="title-content">
                                    <a href="{{$tourDomain}}/australia-new-zealand/" target="_blank" style="" class="title ">澳新</a>
                                    <a href="{{$tourDomain}}/web_action/aunz/" target="_blank" style=" " class="hot-tip ">反季游8折起</a>
                                    <div class="title-content">
                                        <a href="{{$tourDomain}}/africa/" class="title ">非洲</a>
                                    </div>
                                </div>
                            </div>

                            <i class="nav-right-icon"></i>
                            <ul class="item-recommend">
                                <li><a href="{{$tourDomain}}/europe/vc-416/" target="_blank" class="" style=" " >伦敦</a></li>
                                <li><a href="{{$tourDomain}}/europe/vc-331/" target="_blank" class="" style=" " >巴黎</a></li>
                                <li><a href="{{$tourDomain}}/europe/vc-419/" target="_blank" class="" style=" " >巴塞罗那</a></li>
                                <li><a href="{{$tourDomain}}/australia-new-zealand/vc-355/" target="_blank" class="" style=" " >悉尼</a></li>
                                <li><a href="{{$tourDomain}}/australia-new-zealand/vc-353/" target="_blank" class="" style=" " >墨尔本</a></li>
                                <li><a href="{{$tourDomain}}/australia-new-zealand/vc-2124/" target="_blank" class="" style=" " >黄金海岸</a></li>
                            </ul>
                        </div>
                        <div class="sort-item-right">
                            <div>
                                <div class="item-right-content">
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/europe/vc-416/" class="" style=" cursor: default;">英国</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/europe/vc-416/" target="_blank" class="" style=" ">伦敦</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-481/" target="_blank" class="" style=" ">爱丁堡</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-443/" target="_blank" class="" style=" ">伯明翰</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-623/" target="_blank" class="" style="color: #F77700; ">约克</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-723/" target="_blank" class="" style=" ">巴斯</a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="{{$tourDomain}}/western-europe/" class="" style=" cursor: default;">西欧</a></dt>
                                        <dd><a href="{{$tourDomain}}/europe/vc-420/" target="_blank" class="" style="color: #F77700; ">柏林</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-487/" target="_blank" class="" style=" ">慕尼黑</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-325/" target="_blank" class="" style=" ">阿姆斯特丹</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-311/" target="_blank" class="" style="color: #F77700; ">法兰克福</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-515/" target="_blank" class="" style=" ">卢森堡</a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="{{$tourDomain}}/southern-europe/" class="" style=" cursor: default;">南欧</a></dt>
                                        <dd><a href="{{$tourDomain}}/europe/vc-331/" target="_blank" class="" style=" ">巴黎</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-419/" target="_blank" class="" style=" ">巴塞罗那</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-319/" target="_blank" class="" style="color: #F77700; ">罗马</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-417/" target="_blank" class="" style=" ">米兰</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-578/" target="_blank" class="" style=" ">威尼斯</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/northern-europe/" class="" style=" cursor: default;">北欧</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/europe/vc-9120/" target="_blank" class="" style=" ">冰岛</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-937/" target="_blank" class="" style="color: #F77700; ">芬兰</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-938/" target="_blank" class="" style=" ">挪威</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-936/" target="_blank" class="" style=" ">瑞典</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-933/" target="_blank" class="" style=" ">丹麦</a></dd>
                                    </dl>

                                    <div class="right-content-play">
                                        <h2 class="title">
                                            <a href="" class="" style=" cursor: default;">热门玩法</a>
                                        </h2>
                                        <div class="play-content">
                                            <a href="{{$tourDomain}}/europe/ts-512/" target="_blank" class="" style="color: #F77700; ">英伦风情 <i></i></a>
                                            <a href="{{$tourDomain}}/europe/ts-511/" target="_blank" class="" style=" ">环游法国八城 <i></i></a>
                                            <a href="{{$tourDomain}}/europe/ts-510/" target="_blank" class="" style=" ">热情西班牙&葡萄牙 <i></i></a>
                                            <a href="{{$tourDomain}}/europe/ts-504/" target="_blank" class="" style="color: #F77700; ">北欧冰川 <i></i></a>
                                            <a href="{{$tourDomain}}/europe/ts-509/" target="_blank" class="" style="color: #F77700; ">东欧璀璨环游 <i></i></a>
                                            <a href="{{$tourDomain}}/europe/ts-508/" target="_blank" class="" style=" ">南法意大利 <i></i></a>
                                            <a href="{{$tourDomain}}/europe/ts-507/" target="_blank" class="" style=" ">西欧经典 </a>
                                        </div>
                                        <div class="line-play-img">
                                            <a href="{{$tourDomain}}/europe/ts-503/" target="_blank">
                                                <img src="{{$tourDomain}}/images/banners/Home_Page_Left_Menu_400x60_31_400x60_15489286961944.jpg" alt="欧洲经典环线">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-right-content">
                                    <dl>
                                        <dt>
                                            <a href="" class="" style="cursor: default;">新南威尔士</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-356/" target="_blank" class="" style=" ">堪培拉</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-355/" target="_blank" class="" style="color: #F77700; ">悉尼</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-2200/" target="_blank" class="" style=" ">蓝山</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-8915/" target="_blank" class="" style=" ">中央海岸</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="" class="" style="cursor: default;">维多利亚</a>
                                        </dt>
                                        <dd>
                                            <a href="{{$tourDomain}}/australia-new-zealand/vc-353/" target="_blank" class="" style="color: #F77700;">墨尔本</a>
                                        </dd>
                                        <dd>
                                            <a href="{{$tourDomain}}/australia-new-zealand/vc-2235/" target="_blank" class="" style="">大洋路</a>
                                        </dd>
                                        <dd>
                                            <a href="{{$tourDomain}}/australia-new-zealand/vc-2249/" target="_blank" class="" style="">企鹅岛</a>
                                        </dd>
                                        <dd>
                                            <a href="{{$tourDomain}}/australia-new-zealand/vc-2229/" target="_blank" class="" style="">十二门徒</a>
                                        </dd>
                                        <dd>
                                            <a href="{{$tourDomain}}/australia-new-zealand/vc-2137/" target="_blank" class="" style="color: #F77700;">塔斯马尼亚</a>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="" class="" style="cursor: default;">昆士兰/北领地</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-2124/" target="_blank" class="" style=" ">黄金海岸</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-361/" target="_blank" class="" style="color: #F77700;">大堡礁</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-360/" target="_blank" class="" style=" ">凯恩斯</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-358/" target="_blank" class="" style=" ">布里斯本</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="" class="" style="cursor: default;">南澳/西澳</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-8577/" target="_blank" class="" style=" ">珀斯</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-8563/" target="_blank" class="" style="color: #F77700;">阿德莱德</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-8588/" target="_blank" class="" style=" ">尖峰石阵</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-8590/" target="_blank" class="" style=" ">天鹅河谷</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-8583/" target="_blank" class="" style=" ">波浪岩</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/morocco/">摩洛哥</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/morocco/vc-10841/" target="_blank" style="color: #F77700; ">马拉喀什</a></dd>
                                        <dd><a href="{{$tourDomain}}/morocco/vc-10839/" target="_blank" style="color: #F77700; ">卡萨布兰卡</a></dd>
                                    </dl>
                                <dl>
                                            <dt>
                                                                                                <a href="javascript:;" style=" cursor: default;">热门玩法</a>
                                                                                            </dt>
                                                                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/ts-1043/" style="color: #F77700; ">《指环王》奇幻世界</a></dd>
                                                                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/ts-1576/">世界奇观蓝光萤火虫洞</a></dd>
                                                                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/ts-1566/">缆车飞跃阿凡达雨林</a></dd>
                                                                                    </dl>
                                    <div class="right-content-play">
                                       
                                        <div class="line-play-img">
                                            <a href="{{$tourDomain}}/australia-new-zealand/ts-1574/" target="_blank">
                                                <img src="{{$tourDomain}}/images/banners/Home_Page_Left_Menu_400x60_32_400x60_1548928741246.jpg" alt="限时限量：小企鹅大游行">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="m-nav fl">
                <li id="id_5"><a class="nav_a" href="{{$tourDomain}}/" target="_blank" checked_rule="" title="">首页</a></li>
                <li id="id_6" class="nav-down">
                    <a class="nav_a" href="{{$tourDomain}}/america" target="_blank" checked_rule="/america/,/east-coast/" title="">当地参团<i></i><em></em></a>
                    <div class="nav-down-box">
                        <dl>
                            <dt>
                                <a href="{{$tourDomain}}/america/" target="_blank" style="">美国</a>
                            </dt>
                            <dd><a href="{{$tourDomain}}/east-coast/" target="_self" style=" ">美国东海岸</a></dd>
                            <dd><a href="{{$tourDomain}}/west-coast/" target="_self" style=" ">美国西海岸</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-9/" target="_self" style=" ">黄石国家公园</a></dd>
                            <dd><a href="{{$tourDomain}}/florida/" target="_self" style=" ">佛罗里达</a></dd>
                            <dd><a href="{{$tourDomain}}/alaska/" target="_self" style=" ">阿拉斯加</a></dd>
                            <dd><a href="{{$tourDomain}}/hawaii/" target="_self" style=" ">夏威夷</a></dd>
                            <dd><a href="{{$tourDomain}}/south_central/" target="_self" style=" ">美国中南部</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-1/" target="_self" style=" ">洛杉矶</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-66/" target="_self" style=" ">纽约</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-2/" target="_self" style=" ">旧金山</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-3/" target="_self" style=" ">拉斯维加斯</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-588/" target="_self" style=" ">大峡谷</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-65/" target="_self" style=" ">华盛顿</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-71/" target="_self" style=" ">波士顿</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-93/" target="_self" style=" ">尼亚加拉瀑布</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-67/" target="_self" style=" ">费城</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-295/" target="_self" style=" ">西雅图</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-290/" target="_self" style=" ">迈阿密</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-477/" target="_self" style=" ">芝加哥</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-565/" target="_self" style=" ">圣地亚哥</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-380/" target="_self" style=" ">新奥尔良</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-2749/" target="_self" style=" ">休斯顿</a></dd>
                        </dl>
                        <dl>
                            <dt>
                                <a href="{{$tourDomain}}/canada/" target="_blank" style="">加拿大</a>
                            </dt>
                            <dd><a href="{{$tourDomain}}/canada/vc-270/" target="_self" style=" ">温哥华</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-478/" target="_self" style=" ">多伦多</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-545/" target="_self" style=" ">落基山</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-365/" target="_self" style=" ">班芙国家公园</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-1781/" target="_self" style=" ">惠斯勒</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-296/" target="_self" style=" ">维多利亚</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-138/" target="_self" style=" ">魁北克</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-346/" target="_self" style=" ">渥太华</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-519/" target="_self" style=" ">蒙特利尔</a></dd>
                        </dl>
                    </div></li>
                    <li id="id_15"  class="nav-down  "  >
                        <a class="nav_a" href="{{$tourDomain}}/private_group/" target="_self" checked_rule="/private_group/" title="" >定制游<i></i><em></em></a>
                        <div class="nav-down-box">
                            <dl>
                                <dt><a href="{{$tourDomain}}/private_group/list/" target="_self" style=" ">主题定制</a></dt>
                                <dd><a href="{{$tourDomain}}/web_action/familyfun/" target="_self" style="color: #F77700; ">亲子游世界</a></dd>
                                <dd><a href="{{$tourDomain}}/private_group/zt-14/" target="_self" style=" ">婚拍蜜月</a></dd>
                                <dd><a href="{{$tourDomain}}/private_group/638469.html" target="_self" style=" ">激情NBA</a></dd>
                                <dd><a href="{{$tourDomain}}/index.php?mod=advanced_search_result&type_id=Dingzhi&w=%B8%DF%B6%FB%B7%F2&search_in_description=1&v=1" target="_self" style=" ">高尔夫之旅</a></dd>
                                <dd><a href="{{$tourDomain}}/private_group/636655.html" target="_self" style=" ">电影之旅</a></dd>
                            </dl>
                            <dl>
                                <dt><a href="{{$tourDomain}}/private_group/list/" target="_self" style=" ">热门行程</a></dt>
                                <dd><a href="{{$tourDomain}}/private_group/280406.html" target="_self" style=" ">自驾一号公路</a></dd>
                                <dd><a href="{{$tourDomain}}/private_group/638471.html" target="_self" style=" ">巡礼八大名校</a></dd>
                                <dd><a href="{{$tourDomain}}/private_group/638467.html" target="_blank" style=" ">美国夏令营</a></dd>
                                <dd><a href="{{$tourDomain}}/private_group/143923.html" target="_self" style=" ">深度独享黄石</a></dd>
                                <dd><a href="{{$tourDomain}}/private_group/638005.html" target="_self" style=" ">日本赏樱</a></dd>
                            </dl>
                            <dl>
                                <dt><a href="{{$tourDomain}}/baoche/" target="_self" style=" ">包车1日游</a></dt>
                                <dd><a href="{{$tourDomain}}/baoche/279897.html" target="_self" style=" ">洛城名校之旅</a></dd>
                                <dd><a href="{{$tourDomain}}/baoche/279994.html" target="_self" style=" ">纽约购物</a></dd>
                                <dd><a href="{{$tourDomain}}/baoche/280090.html" target="_self" style=" ">锡安国家公园</a></dd>
                                <dd><a href="{{$tourDomain}}/baoche/279968.html" target="_self" style=" ">尼亚加拉瀑布</a></dd>
                                <dd><a href="{{$tourDomain}}/baoche/279967.html" target="_self" style=" ">首都地标</a></dd>
                                <dd><a href="{{$tourDomain}}/baoche/279918.html" target="_self" style=" ">卡特里娜岛</a></dd>
                                <dd><a href="{{$tourDomain}}/baoche/279958.html" target="_self" style="color: #F77700; ">羚羊谷&马蹄湾</a></dd>
                                <dd><a href="{{$tourDomain}}/baoche/279950.html" target="_self" style=" ">纳帕酒乡</a></dd>
                                <dd><a href="{{$tourDomain}}/baoche/279973.html" target="_self" style=" ">听涛山庄</a></dd>
                                <dd><a href="{{$tourDomain}}/baoche/279974.html" target="_self" style=" ">自由广场</a></dd>
                            </dl>
                            <dl>
                                <dt><a href="{{$tourDomain}}/private_group/route/" target="_self" style=" ">精品方案</a></dt>
                                <dd><a href="{{$tourDomain}}/private_group/route/3.html" target="_self" style=" ">美西摄影</a></dd>
                                <dd><a href="{{$tourDomain}}/private_group/route/2.html" target="_self" style=" ">黄石深度</a></dd>
                                <dd><a href="{{$tourDomain}}/private_group/route/6.html" target="_self" style=" ">美东名城</a></dd>
                                <dd><a href="{{$tourDomain}}/private_group/route/7.html" target="_self" style=" ">极光猎艳</a></dd>
                                <dd><a href="{{$tourDomain}}/private_group/route/1.html" target="_self" style=" ">美南阳光</a></dd>
                            </dl>
                        </div>
                    </li>
                    <li id="id_19"><a class="nav_a"
                                      href="{{$tourDomain}}/america/ts-479/" target="_blank"
                                      checked_rule="/america/ts-479/" title=""> 品质小团</a></li>
                    <li id="id_20" class="active">
                            <a class="nav_a" style="height:40px;"
                               href="/" target="_self"
                               checked_rule="/hotels/" title="">酒店<a style="width: 26px;height: 19px;display: inline-block;background-image: url(/img/general/icon/hot.png);
                           background-repeat: no-repeat;background-position: center;position: absolute;left: 47px;top: -8px;"></a></a>
                    </li>
                <li id="id_280">
                    <a class="nav_a" href="{{$tourDomain}}/america/rz-2100/" target="_blank" checked_rule="/america/rz-2100/" title="">走四方独家专线</a>
                </li>
                <li id="id_281">
                    <a class="nav_a" href="{{$tourDomain}}/coupons/" target="_self" checked_rule="/coupons/" title="" >折扣券</a>
                </li>
                <li id="id_21" class="nav-down">
                    <a class="nav_a" href="javascript:" target="_blank" checked_rule="zuche.usitrip.com/,insurance.usitrip.com/" title="" >更多<i></i><em></em></a>
                    <div class="nav-down-box">
                        <dl>
                            <dt><a href="{{$tourDomain}}/activity/" target="_blank" >当地玩乐</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/tui-guang/" target="_self" style=" ">优惠专区</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/jiebantongyou/type-1/" target="_self" style=" ">结伴同游</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/visa/" target="_self" >签证</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="//zuche.usitrip.com" target="_self" style=" ">租车</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/place/USA/" target="_self" style=" ">目的地</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/news/" target="_self" style=" ">攻略</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/ask/" target="_self" style=" ">问答</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/insurance_detail.html" target="_self" style=" ">保险</a></dt>
                        </dl>
                    </div>
                </li>
            </ul>
            <ul class="m-nav-two fr" id="id_xmenu"></ul>
        </div>
    </div>
@elseif($language == 1)
    {{--英文--}}
    <div id="en_nav">
        <div class="Cons">
            <ul>
                <li class="en_list_group en_nav_hot" id="id_437" >
                    <a href="https://hotel.usitour.com" target="_self" checked_rule="" title="">Hotel</a>
                    <i class="hot_ico"></i>
                    <!--二级目录-->
                </li>
                <li class="en_list_group" id="id_592">
                    <a href="{{ $tourDomain }}" target="_blank" checked_rule="" title="">Tour Packages</a>
                    <!--二级目录-->
                    <div id="did_592" class="list" style="display: none;">
                        <div class="child-list-row">
                            <div class="child-list-col1">
                                <a href="{{ $tourDomain }}/west-coast/" target="_blank" title="" class="child-list-title">
                                    US West Tours
                                </a>
                            </div>
                            <div class="child-list-col2">
                                <a href="{{ $tourDomain }}/west-coast/vc-1/" target="_blank" style=" ">Los Angeles</a>
                                <a href="{{ $tourDomain }}/west-coast/vc-2/" target="_blank" style=" ">San Francisco</a>
                                <a href="{{ $tourDomain }}/west-coast/vc-3/" target="_blank" style=" ">Las Vegas</a>
                                <a href="{{ $tourDomain }}/west-coast/vc-295/" target="_blank" style=" ">Seattle</a>
                                <a href="{{ $tourDomain }}/america/vc-9/" target="_blank" style="color: #F77700 !important; ">Yellowstone</a>
                                <a href="{{ $tourDomain }}/america/vc-8/" target="_blank" style=" ">Grand Canyon</a>
                                <a href="{{ $tourDomain }}/west-coast/vc-564/" target="_blank" style="color: #F77700 !important; ">Antelope Canyon</a>
                                <a href="{{ $tourDomain }}/west-coast/vc-51/" target="_blank" style=" ">Yosemite</a>
                            </div>
                        </div>
                        <div class="child-list-row">
                            <div class="child-list-col1">
                                <a href="{{ $tourDomain }}/east-coast/" target="_blank" title="" class="child-list-title">
                                    US East Tours
                                </a>
                            </div>
                            <div class="child-list-col2">
                                <a href="{{ $tourDomain }}/america/vc-66/" target="_blank" style=" ">New York</a>
                                <a href="{{ $tourDomain }}/america/vc-93/" target="_blank" style="color: #F77700 !important; ">Niagara Falls</a>
                                <a href="{{ $tourDomain }}/america/vc-65/" target="_blank" style=" ">Washington D.C.</a>
                                <a href="{{ $tourDomain }}/america/vc-71/" target="_blank" style=" ">Boston</a>
                                <a href="{{ $tourDomain }}/america/vc-477/" target="_blank" style=" ">Chicago</a>
                                <a href="{{ $tourDomain }}/america/vc-67/" target="_blank" style=" ">Philadelphia</a>
                                <a href="{{ $tourDomain }}/america/vc-204/" target="_blank" style=" ">Orlando</a>
                                <a href="{{ $tourDomain }}/america/vc-290/" target="_blank" style=" ">Miami</a>
                            </div>
                        </div>
                        <div class="child-list-row">
                            <div class="child-list-col1">
                                <a href="{{ $tourDomain }}/canada/" target="_blank" title="" class="child-list-title">
                                    Canada Tours
                                </a>
                            </div>
                            <div class="child-list-col2">
                                <a href="{{ $tourDomain }}/canada/vc-270/" target="_blank" style=" ">Vancouver</a>
                                <a href="{{ $tourDomain }}/canada/vc-478/" target="_blank" style="color: #F77700 !important; ">Toronto</a>
                                <a href="{{ $tourDomain }}/canada/vc-365/" target="_blank" style=" ">Banff N.P.</a>
                                <a href="{{ $tourDomain }}/canada/vc-519/" target="_blank" style=" ">Montreal</a>
                                <a href="{{ $tourDomain }}/canada/vc-138/" target="_blank" style=" ">Québec</a>
                                <a href="{{ $tourDomain }}/canada/vc-2813/" target="_blank" style=" ">Calgary</a>
                            </div>
                        </div>
                        <div class="child-list-row">
                            <div class="child-list-col1">
                                <a href="{{ $tourDomain }}/alaska/" target="_blank" title="" class="child-list-title">
                                    Alaska Tours
                                </a>
                            </div>
                            <div class="child-list-col2">
                                <a href="{{ $tourDomain }}/alaska/vc-400/" target="_blank" style=" ">Anchorage</a>
                                <a href="{{ $tourDomain }}/alaska/vc-405/" target="_blank" style=" ">Denali N.P.</a>
                                <a href="{{ $tourDomain }}/alaska/vc-1871/" target="_blank" style="color: #F77700 !important; ">Northern Lights</a>
                                <a href="{{ $tourDomain }}/alaska/vc-934/" target="_blank" style=" ">Fairbanks</a>
                                <a href="{{ $tourDomain }}/alaska/vc-2044/" target="_blank" style=" ">Whittier</a>
                                <a href="{{ $tourDomain }}/alaska/fc-10241/" target="_blank" style=" ">Seward</a>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="en_list_group" id="id_313">
                    <a href="{{ $tourDomain }}/group_buys/" target="_self" checked_rule="/group_buys/" title="">Tour Deals</a>
                    <!--二级目录-->
                </li>
                <li class="en_list_group" id="id_658">
                    <a href="https://blog.usitour.com" target="_self" checked_rule="" title="">Travel Blog</a>
                    <!--二级目录-->
                </li>

                <li class="usitour-phone-number"><i class="phone_logo"></i><span>+1 626-898-7658</span></li>

            </ul>
        </div>
    </div>
@else
    <!-- 導航欄 begin 繁體tw -->
    <div class="m-nav-wrap" id="id_nav_head">
        <div class="g-wrap uifix" id="id_g_wrap">
            <div class="m-nav-sort fl" id="id_nav_sort">
                <span class="m-nav-sort-tit">旅遊頻道<i></i></span>
                <div class="m-sort-box " id="id_sort_box">
                    <div class="nav-sort-item">
                        <div class="sort-item-left">
                            <div class="item-title">
                                <div class="title-content">
                                    <a href="{{$tourDomain}}/america/" target="_blank" style="" class="title ">美國</a>
                                    <a href="{{$tourDomain}}/america/vc-9/" target="_blank" style=" " class="hot-tip ">黃石木屋7折起</a>
                                </div>
                            </div>
                            <i class="nav-right-icon"></i>
                            <ul class="item-recommend">
                                <li><a href="{{$tourDomain}}/west-coast/" target="_blank" class="" style="color: #F77700; " >美國西海岸</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-1/" target="_blank" class="" style=" " >洛杉磯</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-2/" target="_blank" class="" style=" " >舊金山</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-9/" target="_blank" class="" style=" " >黃石公園</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-295/" target="_blank" class="" style=" " >西雅圖</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-588/" target="_blank" class="" style="color: #F77700; " >大峡谷</a></li>
                                <li><a href="{{$tourDomain}}/east-coast/" target="_blank" class="" style="color: #F77700; " >美國東海岸</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-66/" target="_blank" class="" style=" " >紐約</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-290/" target="_blank" class="" style=" " >邁阿密</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-204/" target="_blank" class="" style=" " >奧蘭多</a></li>
                                <li><a href="{{$tourDomain}}/america/vc-65/" target="_blank" class="" style=" " >華盛頓</a></li>
                                <li><a href="{{$tourDomain}}/hawaii/" target="_blank" class="" style="color: #F77700; " >夏威夷</a></li>
                            </ul>
                        </div>
                        <div class="sort-item-right">
                            <div>
                                <div class="item-right-list">
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/west-coast/" target="_blank" class="" style=" ">美國西海岸</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/america/vc-1/" target="_blank" class="" style="color: #F77700; ">洛杉磯</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-3/" target="_blank" class="" style=" ">拉斯維加斯</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-2/" target="_blank" class="" style=" ">舊金山</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-295/" target="_blank" class="" style=" ">西雅圖</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-10/" target="_blank" class="" style=" ">鹽湖城</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-565/" target="_blank" class="" style=" ">聖地亞哥</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/east-coast/" target="_blank" class="" style=" ">美國東海岸</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/america/vc-66/" target="_blank" class="" style=" ">紐約</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-71/" target="_blank" class="" style=" ">波士頓</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-65/" target="_blank" class="" style=" ">華盛頓</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-93/" target="_blank" class="" style="color: #F77700; ">尼亞加拉瀑布</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-477/" target="_blank" class="" style=" ">芝加哥</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-67/" target="_blank" class="" style=" ">費城</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="javascript:;" class="" style=" cursor: default;">國家公園</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/america/vc-9/" target="_blank" class="" style="color: #F77700; ">黃石公園</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-588/" target="_blank" class="" style=" ">大峽谷</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-564/" target="_blank" class="" style="color: #F77700; ">羚羊峽谷</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-32/" target="_blank" class="" style=" ">拱門國家公園</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-51/" target="_blank" class="" style=" ">優勝美地</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-34/" target="_blank" class="" style=" ">大提頓公園</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/hawaii/" target="_blank" class="" style=" ">夏威夷</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/america/vc-162/" target="_blank" class="" style=" ">檀香山</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-2700/" target="_blank" class="" style=" ">茂宜島</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-163/" target="_blank" class="" style=" ">珍珠港</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-2675/" target="_blank" class="" style=" ">火山大島</a></dd>
                                        <dd><a href="{{$tourDomain}}/hawaii/vc-2699/" target="_blank" class="" style=" ">歐胡島</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/alaska/" target="_blank" class="" style=" ">阿拉斯加</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/america/vc-934/" target="_blank" class="" style=" ">費爾班克斯</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-400/" target="_blank" class="" style=" ">安克雷奇</a></dd>
                                        <dd><a href="{{$tourDomain}}/alaska/vc-978/" target="_blank" class="" style=" ">珍娜溫泉</a></dd>
                                        <dd><a href="{{$tourDomain}}/alaska/vc-2833/" target="_blank" class="" style=" ">北極圈</a></dd>
                                        <dd><a href="{{$tourDomain}}/alaska/vc-405/" target="_blank" class="" style=" ">迪納利國家公園</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/florida/" target="_blank" class="" style=" ">佛羅裏達</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/america/vc-290/" target="_blank" class="" style=" ">邁阿密</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-204/" target="_blank" class="" style="color: #F77700; ">奧蘭多</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-286/" target="_blank" class="" style=" ">墨西哥灣</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-1633/" target="_blank" class="" style=" ">西鎖島</a></dd>
                                        <dd><a href="{{$tourDomain}}/america/vc-796/" target="_blank" class="" style=" ">大沼澤國家公園</a></dd>
                                        <dd><a href="{{$tourDomain}}/cruise/" target="_blank" class="" style=" ">加勒比郵輪</a></dd>
                                    </dl>
                                </div>

                                <div class="item-right-bottom">
                                    <div class="item-line-play">
                                        <h2 class="title">
                                            <a href="javascript:;" class="" style=" cursor: default;">熱門玩法</a>
                                        </h2>
                                        <div class="play-content">
                                            <a href="{{$tourDomain}}/america/rz-2100/" target="_blank" style="color: #F77700; ">走四方獨家全程加州一號公路 <i></i></a>
                                            <a href="{{$tourDomain}}/america/hd-2134" target="_blank" style="color: #F77700; ">冬季黃石 <i></i></a>
                                            <a href="{{$tourDomain}}/america/hd-2140" target="_blank" style="color: #F77700; ">東南峽谷,羚羊谷,四大國家公園 <i></i></a>
                                            <a href="{{$tourDomain}}/alaska/ts-2018/" target="_blank" style="color: #F77700; ">Alaska星球小屋 <i></i></a>
                                            <a href="{{$tourDomain}}/america/ts-479/" target="_blank" class="" style=" ">小團遊<i></i></a>
                                            <a href="{{$tourDomain}}/america/ts-1476/" target="_blank" class="" style="color: #F77700; ">露營車旅遊 <i></i></a>
                                            <a href="{{$tourDomain}}/america/ts-1908/" target="_blank" class="" style=" ">美東名校巡禮 <i></i></a>
                                            <a href="{{$tourDomain}}/group_buys/" target="_blank" class="" style=" ">限時團購 <i></i></a>
                                        </div>
                                    </div>
                                    <ul class="icon-nav">
                                        <li>
                                            <a href="{{$tourDomain}}/ticket/" target="_blank">
                                                <i class="sdc-r-ico3"></i>
                                                <p style="">門票</p>
                                            </a></li>
                                        <li><a href="{{$tourDomain}}/cruise/" target="_blank"> <i class="sdc-r-ico11"></i>
                                                <p style=" ">郵輪</p>
                                            </a></li>
                                        <li><a href="{{$tourDomain}}/baoche/" target="_blank"> <i class="sdc-r-ico4"></i>
                                                <p style="">包車</p>
                                            </a></li>
                                        <li><a href="{{$tourDomain}}/transfer/" target="_blank"> <i class="sdc-r-ico5"></i>
                                                <p style="">接送機</p>
                                            </a></li>
                                        <li><a href="{{$tourDomain}}/tours-departing-from-china/" target="_blank">
                                                <i class="sdc-r-ico1"></i>
                                                <p style="">中國出發</p>
                                            </a></li>
                                        <li><a href="/" target="_blank">
                                                <i class="sdc-r-ico6"></i>
                                                <p style="">飯店</p>
                                            </a></li>
                                        <li><a href="{{$tourDomain}}/visa/" target="_blank"> <i class="sdc-r-ico8"></i>
                                                <p style="">簽證</p>
                                            </a></li>
                                        <li><a href="{{$tourDomain}}/wifi/" target="_blank"> <i class="sdc-r-ico10"></i>
                                                <p style="">電話卡/wifi</p>
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sort-item">
                        <div class="sort-item-left">
                            <div class="item-title">
                                <div class="title-content">
                                    <a href="{{$tourDomain}}/canada/" target="_blank" style="" class="title ">加拿大</a>
                                    <a href="https://www.usitrip.com/web_action/Winter_canada/tw" target="_blank" style=" " class="hot-tip ">冬季“加”年華</a>
                                </div>
                                <div class="title-content">
                                    <a href="{{$tourDomain}}/south-america/" target="_blank" style=" " class="title ">南美</a>
                                </div>
                            </div>

                            <i class="nav-right-icon"></i>
                            <ul class="item-recommend">
                                <li><a href="{{$tourDomain}}/canada/vc-270/" target="_blank" class="" style="">溫哥華</a></li>
                                <li><a href="{{$tourDomain}}/canada/vc-478/" target="_blank" class="" style="">多倫多</a></li>
                                <li>
                                    <a href="{{$tourDomain}}/canada/vc-365/" target="_blank" class="" style="color: #F77700;">班芙公園</a>
                                </li>
                                <li><a href="{{$tourDomain}}/mexico/vc-370/" target="_blank" class="" style="">墨西哥城</a>
                                </li>
                                <li>
                                    <a href="{{$tourDomain}}/mexico/vc-371/" target="_blank" class="" style="color: #F77700;">坎昆</a>
                                </li>
                                <li>
                                    <a href="{{$tourDomain}}/peru/" target="_blank" class="" style="color: #F77700;">祕魯</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sort-item-right">
                            <div>
                                <div class="item-right-content">
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/western-canada/" target="_blank" class="" style=" ">加拿大西部</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/canada/vc-270/" target="_blank" class="" style=" ">溫哥華</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-2813/" target="_blank" class="" style="color: #F77700; ">卡爾加裏</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-296/" target="_blank" class="" style=" ">維多利亞</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-1781/" target="_blank" class="" style=" ">惠斯勒</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/eastern-canada/" target="_blank" class="" style=" ">加拿大東部</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/canada/vc-478" target="_blank" class="" style=" ">多倫多</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-519/" target="_blank" class="" style=" ">蒙特利爾</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-346/" target="_blank" class="" style=" ">渥太華</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-138" target="_blank" class="" style=" ">魁北克</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-93/" target="_blank" class="" style="color: #F77700; ">尼加拉瀑布</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="javascript:;" class="" style=" cursor: default;">國家公園</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/canada/vc-365/" target="_blank" class="" style=" ">班夫國家公園</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-545/" target="_blank" class="" style="color: #F77700; ">洛基山國家公園</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-2815/" target="_blank" class="" style=" ">幽鶴國家公園</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-2338/" target="_blank" class="" style=" ">冰川國家公園</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a class="" style=" cursor: default;">極光</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/canada/vc-791/" target="_blank"  class="" style=" ">黃刀/耶洛奈夫</a></dd>
                                        <dd><a href="{{$tourDomain}}/canada/vc-8863/" target="_blank"  class="" style=" ">白馬/懷特霍斯</a></dd>
                                    </dl>
                                    <div class="right-content-play">
                                        <h2 class="title">
                                            <a href="" class="" style="cursor: default;">熱門玩法</a>
                                        </h2>
                                        <div class="play-content">
                                            <a href="{{$tourDomain}}/canada/hd-2138/" target="_blank" style="color: #F77700; ">冬季班芙 <i></i></a>
                                            <a href="{{$tourDomain}}/canada/vc-791/" target="_blank" style="color: #F77700; ">黃刀鎮追極光 <i></i></a>
                                            <a href="{{$tourDomain}}/canada/vc-431/" target="_blank" >硫磺山泡溫泉 <i></i></a>
                                            <a href="{{$tourDomain}}/canada/ts-812" target="_blank" class="" style=" ">直升機飛躍千島湖 </a>
                                        </div>
                                        <div class="line-play-img">
                                            <a href="{{$tourDomain}}/canada/vc-365/" target="_blank">
                                                <img src="//qiniu.usitrip.com/images/banners/Home_Page_Left_Menu_400x60_22_400x60_15554944064645.jpg" alt="班芙國家公園">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-right-content">
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/mexico/" target="_blank" class="" style=" ">墨西哥</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/mexico/vc-371/" target="_blank" class="" style="color: #F77700; ">坎昆</a></dd>
                                        <dd><a href="{{$tourDomain}}/mexico/vc-370/" target="_blank" class="" style=" ">墨西哥城</a></dd>
                                        <dd><a href="{{$tourDomain}}/mexico/vc-672/" target="_blank" class="" style=" ">梅裏達</a></dd>
                                        <dd><a href="{{$tourDomain}}/mexico/vc-637/" target="_blank" class="" style="color: #F77700; ">奇琴伊察</a></dd>
                                        <dd><a href="{{$tourDomain}}/mexico/vc-1002/" target="_blank" class="" style=" ">特奧蒂瓦坎</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/puerto-rico/" target="_blank" class="" style=" ">波多黎各</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/puerto-rico/vc-1428/" target="_blank" class="" style=" ">聖胡安</a></dd>
                                        <dd><a href="{{$tourDomain}}/puerto-rico/vc-8823/" target="_blank" class="" style="color: #F77700; ">庫萊布拉島</a></dd>
                                        <dd><a href="{{$tourDomain}}/puerto-rico/vc-9430/" target="_blank" class="" style=" ">弗拉門戈海灘</a></dd>
                                        <dd><a href="{{$tourDomain}}/puerto-rico/vc-8825/" target="_blank" class="" style="color: #F77700; ">熒光湖</a></dd>
                                        <dd><a href="{{$tourDomain}}/puerto-rico/vc-9433/" target="_blank" class="" style=" ">聖胡安老城區</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/cuba/" target="_blank" class="" style=" ">古巴</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/cuba/vc-8675/" target="_blank" class="" style="color: #F77700; ">哈瓦那</a></dd>
                                        <dd><a href="{{$tourDomain}}/cuba/vc-8672/" target="_blank" class="" style=" ">巴拉德羅</a></dd>
                                        <dd><a href="{{$tourDomain}}/cuba/vc-8673/" target="_blank" class="" style=" ">革命廣場</a></dd>
                                        <dd><a href="{{$tourDomain}}/cuba/vc-8709/" target="_blank" class="" style=" ">國家藝術館</a></dd>
                                        <dd><a href="{{$tourDomain}}/cuba/vc-8707/" target="_blank" class="" style=" ">哈瓦那老城區</a></dd>

                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/latin-america/" target="_blank" class="" style=" ">南美洲</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/brazil/" target="_blank" class="" style=" ">巴西</a></dd>
                                        <dd><a href="{{$tourDomain}}/argentina/" target="_blank" class="" style=" ">阿根廷</a></dd>
                                        <dd><a href="{{$tourDomain}}/south-america/vc-9454/" target="_blank" class="" style=" ">智利</a></dd>
                                        <dd><a href="{{$tourDomain}}/south-america/vc-8725/" target="_blank" class="" style="color: #F77700; ">馬丘比丘</a></dd>
                                    </dl>
                                    <div class="right-content-play">
                                        <h2 class="title">
                                            <a href="" class="" style=" cursor: default;">熱門玩法</a>
                                        </h2>
                                        <div class="play-content">
                                            <a href="{{$tourDomain}}/south-america/ts-1914" target="_blank" class="" style=" ">夏季海島風情遊 <i></i></a>
                                            <a href="{{$tourDomain}}/south-america/fc-371_d-0-1/" target="_blank" class="" style=" ">坎昆票券當地遊推薦 </a>
                                        </div>
                                        <div class="line-play-img">
                                            <a href="{{$tourDomain}}/south-america/vc-371/" target="_blank">
                                                <img src="//qiniu.usitrip.com/images/banners/Home_Page_Left_Menu_400x60_23_400x60_15554945769529.jpg" alt="坎昆">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sort-item">
                        <div class="sort-item-left">
                            <div class="item-title">
                                <div class="title-content">
                                    <a href="{{$tourDomain}}/europe/" target="_blank" style="" class="title ">歐洲</a>

                                </div>
                                <div class="title-content">
                                    <a href="{{$tourDomain}}/australia-new-zealand/" target="_blank" style="" class="title ">紐澳</a>
                                    <a href="https://www.usitrip.com/web_action/aunz/" target="_blank" style=" " class="hot-tip ">反季遊8折起</a>
                                </div>
                            </div>

                            <i class="nav-right-icon"></i>
                            <ul class="item-recommend">
                                <li><a href="{{$tourDomain}}/europe/vc-416/" target="_blank" class="" style=" " >倫敦</a></li>
                                <li><a href="{{$tourDomain}}/europe/vc-331/" target="_blank" class="" style=" " >巴黎</a></li>
                                <li><a href="{{$tourDomain}}/europe/vc-419/" target="_blank" class="" style=" " >巴塞羅那</a></li>
                                <li><a href="{{$tourDomain}}/australia-new-zealand/vc-355/" target="_blank" class="" style=" " >悉尼</a></li>
                                <li><a href="{{$tourDomain}}/australia-new-zealand/vc-353/" target="_blank" class="" style=" " >墨爾本</a></li>
                                <li><a href="{{$tourDomain}}/australia-new-zealand/vc-2124/" target="_blank" class="" style=" " >黃金海岸</a></li>
                            </ul>
                        </div>
                        <div class="sort-item-right">
                            <div>
                                <div class="item-right-content">
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/europe/vc-416/" class="" style=" cursor: default;">英國</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/europe/vc-416/" target="_blank" class="" style=" ">倫敦</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-481/" target="_blank" class="" style=" ">愛丁堡</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-443/" target="_blank" class="" style=" ">伯明翰</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-623/" target="_blank" class="" style="color: #F77700; ">約克</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-723/" target="_blank" class="" style=" ">巴斯</a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="{{$tourDomain}}/western-europe/" class="" style=" cursor: default;">西歐</a></dt>
                                        <dd><a href="{{$tourDomain}}/europe/vc-420/" target="_blank" class="" style="color: #F77700; ">柏林</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-487/" target="_blank" class="" style=" ">慕尼黑</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-325/" target="_blank" class="" style=" ">阿姆斯特丹</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-311/" target="_blank" class="" style="color: #F77700; ">法蘭克福</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-515/" target="_blank" class="" style=" ">盧森堡</a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="{{$tourDomain}}/southern-europe/" class="" style=" cursor: default;">南歐</a></dt>
                                        <dd><a href="{{$tourDomain}}/europe/vc-331/" target="_blank" class="" style=" ">巴黎</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-419/" target="_blank" class="" style=" ">巴塞羅那</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-319/" target="_blank" class="" style="color: #F77700; ">羅馬</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-417/" target="_blank" class="" style=" ">米蘭</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-578/" target="_blank" class="" style=" ">威尼斯</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="{{$tourDomain}}/northern-europe/" class="" style=" cursor: default;">北歐</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/europe/vc-9120/" target="_blank" class="" style=" ">冰島</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-937/" target="_blank" class="" style="color: #F77700; ">芬蘭</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-938/" target="_blank" class="" style=" ">挪威</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-936/" target="_blank" class="" style=" ">瑞典</a></dd>
                                        <dd><a href="{{$tourDomain}}/europe/vc-933/" target="_blank" class="" style=" ">丹麥</a></dd>
                                    </dl>

                                    <div class="right-content-play">
                                        <h2 class="title">
                                            <a href="" class="" style=" cursor: default;">熱門玩法</a>
                                        </h2>
                                        <div class="play-content">
                                            <a href="{{$tourDomain}}/europe/ts-512/" target="_blank" class="" style="color: #F77700; ">英倫風情 <i></i></a>
                                            <a href="{{$tourDomain}}/europe/ts-511/" target="_blank" class="" style=" ">環遊法國八城 <i></i></a>
                                            <a href="{{$tourDomain}}/europe/ts-510/" target="_blank" class="" style=" ">熱情西班牙&葡萄牙 <i></i></a>
                                            <a href="{{$tourDomain}}/europe/ts-504/" target="_blank" class="" style="color: #F77700; ">北歐冰川 <i></i></a>
                                            <a href="{{$tourDomain}}/europe/ts-509/" target="_blank" class="" style="color: #F77700; ">東歐璀璨環遊 <i></i></a>
                                            <a href="{{$tourDomain}}/europe/ts-508/" target="_blank" class="" style=" ">南法義大利 <i></i></a>
                                            <a href="{{$tourDomain}}/europe/ts-507/" target="_blank" class="" style=" ">西歐經典 </a>
                                        </div>
                                        <div class="line-play-img">
                                            <a href="{{$tourDomain}}/europe/ts-503/" target="_blank">
                                                <img src="//qiniu.usitrip.com/images/banners/Home_Page_Left_Menu_400x60_31_400x60_15489286961944.jpg" alt="歐洲經典環線">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-right-content">
                                    <dl>
                                        <dt>
                                            <a href="" class="" style="cursor: default;">新南威爾士</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-356/" target="_blank" class="" style=" ">堪培拉</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-355/" target="_blank" class="" style="color: #F77700; ">悉尼</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-2200/" target="_blank" class="" style=" ">藍山</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-8915/" target="_blank" class="" style=" ">中央海岸</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="" class="" style="cursor: default;">維多利亞</a>
                                        </dt>
                                        <dd>
                                            <a href="{{$tourDomain}}/australia-new-zealand/vc-353/" target="_blank" class="" style="color: #F77700;">墨爾本</a>
                                        </dd>
                                        <dd>
                                            <a href="{{$tourDomain}}/australia-new-zealand/vc-2235/" target="_blank" class="" style="">大洋路</a>
                                        </dd>
                                        <dd>
                                            <a href="{{$tourDomain}}/australia-new-zealand/vc-2249/" target="_blank" class="" style="">企鵝島</a>
                                        </dd>
                                        <dd>
                                            <a href="{{$tourDomain}}/australia-new-zealand/vc-2229/" target="_blank" class="" style="">十二門徒</a>
                                        </dd>
                                        <dd>
                                            <a href="{{$tourDomain}}/australia-new-zealand/vc-2137/" target="_blank" class="" style="color: #F77700;">塔斯馬尼亞</a>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="" class="" style="cursor: default;">昆士蘭/北領地</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-2124/" target="_blank" class="" style=" ">黃金海岸</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-361/" target="_blank" class="" style="color: #F77700;">大堡礁</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-360/" target="_blank" class="" style=" ">凱恩斯</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-358/" target="_blank" class="" style=" ">布裏斯本</a></dd>
                                    </dl>
                                    <dl>
                                        <dt>
                                            <a href="" class="" style="cursor: default;">南澳/西澳</a>
                                        </dt>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-8577/" target="_blank" class="" style=" ">珀斯</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-8563/" target="_blank" class="" style="color: #F77700;">阿德萊德</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-8588/" target="_blank" class="" style=" ">尖峰石陣</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-8590/" target="_blank" class="" style=" ">天鵝河谷</a></dd>
                                        <dd><a href="{{$tourDomain}}/australia-new-zealand/vc-8583/" target="_blank" class="" style=" ">波浪巖</a></dd>
                                    </dl>
                                    <div class="right-content-play">
                                        <h2 class="title">
                                            <a href="" class="" style=" cursor: default;">熱門玩法</a>
                                        </h2>
                                        <div class="play-content">
                                            <a href="{{$tourDomain}}/australia-new-zealand/ts-1043/" target="_blank" class="" style="color: #F77700; ">《指環王》奇幻世界 <i></i></a>
                                            <a href="{{$tourDomain}}/australia-new-zealand/ts-1049/" target="_blank" class="" style=" ">紐西蘭4大國家公園 <i></i></a>
                                            <a href="{{$tourDomain}}/australia-new-zealand/ts-1576/" target="_blank" class="" style=" ">世界奇觀藍光螢火蟲洞 <i></i></a>
                                            <a href="{{$tourDomain}}/australia-new-zealand/ts-1566/" target="_blank" class="" style=" ">纜車飛躍阿凡達雨林 </a>
                                        </div>
                                        <div class="line-play-img">
                                            <a href="{{$tourDomain}}/australia-new-zealand/ts-1574/" target="_blank">
                                                <img src="//qiniu.usitrip.com/images/banners/Home_Page_Left_Menu_400x60_32_400x60_1548928741246.jpg" alt="限時限量：小企鵝大遊行">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="m-nav fl">
                <li id="id_5">
                    <a class="nav_a" href="{{$tourDomain}}/" target="_blank" checked_rule="" title="">首頁</a>
                </li>
                <li id="id_6" class="nav-down">
                    <a class="nav_a" href="{{$tourDomain}}/america" target="_blank" checked_rule="/america/,/east-coast/" title="">當地參團<i></i><em></em></a>
                    <div class="nav-down-box">
                        <dl>
                            <dt>
                                <a href="{{$tourDomain}}/america/" target="_blank" style="">美國</a>
                            </dt>
                            <dd><a href="{{$tourDomain}}/east-coast/" target="_self" style=" ">美國東海岸</a></dd>
                            <dd><a href="{{$tourDomain}}/west-coast/" target="_self" style=" ">美國西海岸</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-9/" target="_self" style=" ">黃石國家公園</a></dd>
                            <dd><a href="{{$tourDomain}}/florida/" target="_self" style=" ">佛羅裏達</a></dd>
                            <dd><a href="{{$tourDomain}}/alaska/" target="_self" style=" ">阿拉斯加</a></dd>
                            <dd><a href="{{$tourDomain}}/hawaii/" target="_self" style=" ">夏威夷</a></dd>
                            <dd><a href="{{$tourDomain}}/south_central/" target="_self" style=" ">美國中南部</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-1/" target="_self" style=" ">洛杉磯</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-66/" target="_self" style=" ">紐約</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-2/" target="_self" style=" ">舊金山</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-3/" target="_self" style=" ">拉斯維加斯</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-588/" target="_self" style=" ">大峽谷</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-65/" target="_self" style=" ">華盛頓</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-71/" target="_self" style=" ">波士頓</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-93/" target="_self" style=" ">尼加拉瀑布</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-67/" target="_self" style=" ">費城</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-295/" target="_self" style=" ">西雅圖</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-290/" target="_self" style=" ">邁阿密</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-477/" target="_self" style=" ">芝加哥</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-565/" target="_self" style=" ">聖地亞哥</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-380/" target="_self" style=" ">新奧爾良</a></dd>
                            <dd><a href="{{$tourDomain}}/america/vc-2749/" target="_self" style=" ">休斯頓</a></dd>
                        </dl>
                        <dl>
                            <dt>
                                <a href="{{$tourDomain}}/canada/" target="_blank" style="">加拿大</a>
                            </dt>
                            <dd><a href="{{$tourDomain}}/canada/vc-270/" target="_self" style=" ">溫哥華</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-478/" target="_self" style=" ">多倫多</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-545/" target="_self" style=" ">落基山</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-365/" target="_self" style=" ">班芙國家公園</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-1781/" target="_self" style=" ">惠斯勒</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-296/" target="_self" style=" ">維多利亞</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-138/" target="_self" style=" ">魁北克</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-346/" target="_self" style=" ">渥太華</a></dd>
                            <dd><a href="{{$tourDomain}}/canada/vc-519/" target="_self" style=" ">蒙特利爾</a></dd>
                        </dl>
                    </div></li>
                <li id="id_15"  class="nav-down  "  >
                    <a class="nav_a" href="{{$tourDomain}}/private_group/" target="_self" checked_rule="/private_group/"
                       title="" >客制遊<i></i><em></em></a>
                    <div class="nav-down-box">
                        <dl>
                            <dt><a href="{{$tourDomain}}/private_group/list/" target="_self" style=" ">主題客制</a></dt>
                            <dd><a href="https://www.usitrip.com/web_action/familyfun/tw" target="_self" style="color: #F77700; ">親子遊世界</a></dd>
                            <dd><a href="{{$tourDomain}}/private_group/zt-14/" target="_self" style=" ">婚拍蜜月</a></dd>
                            <dd><a href="{{$tourDomain}}/private_group/638469.html" target="_self" style=" ">激情NBA</a></dd>
                            <dd><a href="{{$tourDomain}}/index.php?mod=advanced_search_result&type_id=Dingzhi&w=%B8%DF%B6%FB%B7%F2&search_in_description=1&v=1" target="_self" style=" ">高爾夫之旅</a></dd>
                            <dd><a href="{{$tourDomain}}/private_group/636655.html" target="_self" style=" ">電影之旅</a></dd>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/private_group/list/" target="_self" style=" ">熱門行程</a></dt>
                            <dd><a href="{{$tourDomain}}/private_group/280406.html" target="_self" style=" ">自駕一號公路</a></dd>
                            <dd><a href="{{$tourDomain}}/private_group/638471.html" target="_self" style=" ">巡禮八大名校</a></dd>
                            <dd><a href="{{$tourDomain}}/private_group/638467.html" target="_blank" style=" ">美國夏令營</a></dd>
                            <dd><a href="{{$tourDomain}}/private_group/143923.html" target="_self" style=" ">深度獨享黃石</a></dd>
                            <dd><a href="{{$tourDomain}}/private_group/638005.html" target="_self" style=" ">日本賞櫻</a></dd>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/baoche/" target="_self" style=" ">包車1日遊</a></dt>
                            <dd><a href="{{$tourDomain}}/baoche/279897.html" target="_self" style=" ">洛城名校之旅</a></dd>
                            <dd><a href="{{$tourDomain}}/baoche/279994.html" target="_self" style=" ">紐約購物</a></dd>
                            <dd><a href="{{$tourDomain}}/baoche/280090.html" target="_self" style=" ">錫安國家公園</a></dd>
                            <dd><a href="{{$tourDomain}}/baoche/279968.html" target="_self" style=" ">尼加拉瀑布</a></dd>
                            <dd><a href="{{$tourDomain}}/baoche/279967.html" target="_self" style=" ">首都地標</a></dd>
                            <dd><a href="{{$tourDomain}}/baoche/279918.html" target="_self" style=" ">卡特裏娜島</a></dd>
                            <dd><a href="{{$tourDomain}}/baoche/279958.html" target="_self" style="color: #F77700; ">羚羊谷&馬蹄灣</a></dd>
                            <dd><a href="{{$tourDomain}}/baoche/279950.html" target="_self" style=" ">納帕酒鄉</a></dd>
                            <dd><a href="{{$tourDomain}}/baoche/279973.html" target="_self" style=" ">聽濤山莊</a></dd>
                            <dd><a href="{{$tourDomain}}/baoche/279974.html" target="_self" style=" ">自由廣場</a></dd>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/private_group/route/" target="_self" style=" ">精品方案</a></dt>
                            <dd><a href="{{$tourDomain}}/private_group/route/3.html" target="_self" style=" ">美西攝影</a></dd>
                            <dd><a href="{{$tourDomain}}/private_group/route/2.html" target="_self" style=" ">黃石深度</a></dd>
                            <dd><a href="{{$tourDomain}}/private_group/route/6.html" target="_self" style=" ">美東名城</a></dd>
                            <dd><a href="{{$tourDomain}}/private_group/route/7.html" target="_self" style=" ">極光獵艷</a></dd>
                            <dd><a href="{{$tourDomain}}/private_group/route/1.html" target="_self" style=" ">美南陽光</a></dd>
                        </dl>
                    </div>
                </li>
                <li id="id_19"><a class="nav_a"
                                  href="{{$tourDomain}}/xiaozhong/ts-479/" target="_blank"
                                  checked_rule="/xiaozhong/ts-479/" title="">品質小團</a>
                </li>
                <li id="id_20" class="active">
                    <a class="nav_a" style="height:40px;"
                       href="/" target="_self"
                       checked_rule="/hotels/" title="">訂房<a style="width: 26px;height: 19px;display: inline-block;
                   background-image: url(/img/general/icon/hot.png);background-repeat: no-repeat;background-position: center;position: absolute;left: 47px;top: -8px;"></a></a></li>
                <li id="id_280">
                    <a class="nav_a" href="{{$tourDomain}}/america/rz-2100/" target="_blank" checked_rule="/america/rz-2100/" title="">走四方獨家專線</a>
                </li>
                <li id="id_281">
                    <a class="nav_a" href="{{$tourDomain}}/coupons/" target="_self" checked_rule="/coupons/" title="" >折扣券</a>
                </li>
                <li id="id_21" class="nav-down">
                    <a class="nav_a" href="javascript:" target="_blank" checked_rule="zuche.usitrip.com/,insurance.usitrip.com/" title="" >更多<i></i><em></em></a>
                    <div class="nav-down-box">
                        <dl>
                            <dt><a href="{{$tourDomain}}/activity/" target="_blank" >當地玩樂</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/tui-guang/" target="_self" style=" ">優惠專區</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/jiebantongyou/type-1/" target="_self" style=" ">結伴同遊</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/visa/" target="_self" >簽證</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="//zuche.usitrip.com" target="_self" style=" ">租車</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/place/USA/" target="_self" style=" ">目的地</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/news/" target="_self" style=" ">攻略</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/ask/" target="_self" style=" ">問答</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="{{$tourDomain}}/insurance_detail.html" target="_self" style=" ">保險</a></dt>
                        </dl>
                    </div>
                </li>
            </ul>
            <ul class="m-nav-two fr" id="id_xmenu"></ul>
        </div>
    </div>
@endif
<!--导航栏 end-->