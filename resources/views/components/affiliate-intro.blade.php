<?php
$domain = $is_usitour ? 'Usitour' : 'Usitrip';

if ($language === 0) {
    $text_intro_1 = nl2br($domain . "销售联盟是全球最知名、最专业的境外旅游行业网络营销推广合作平台，免费加盟无门槛，模式灵活有惊喜，佣金丰厚、结算便捷、支付准时！是广大站长、行业公司、论坛SNS、博客微博、旅游爱好者、以及任意个人最佳的合作伙伴、最优的赚钱选择！ \n\n" . $domain . "旅游网销售联盟作为最优质的互联网赚钱联盟体系，运作经验丰富，拥有着行业内最多样的合作模式、最高额的佣金分成、最优秀的用户体验、以及最活跃的合作氛围！信息反馈及时清晰，业绩记录准确公正，佣金支付更无拖欠，品牌保障，口碑优秀！走四方旅游网现诚邀天下英雄，强强联合，共创双赢！");
    $text_intro_2 = nl2br("加入联盟的条件非常简单只要您的朋友或家人有着订酒店的需求，您就可以随时申请加入！\n\n 加入后，您更是可以随时随地通过网络、电话、邮件、短信、直接面对面等任意形式向您的访客、朋友、家人等进行推荐。在您的家人朋友享受到折扣优惠的同时，您同样也轻松赚取了高额的佣金！每单佣金高达10%哟！制度详情请注册后参照邮件内容。");
} else {
    $text_intro_1 = "Join " . $domain . " Affiliate Program and take part in " . $domain . "’s  online success. You
    can earn a commission fee for online purchases made by " . $domain . " customers that come to our site from yours link.";
    $text_intro_2 = nl2br("Joining is Easy & Free!\n\nIn just minutes you will be ready to receive sales commission. Join now! \nEarn commission fees on all purchases. \nIndustry-Leading Tools, Flexible Linking Arrangements, Product Feeds, and Best Practices. \n" . $domain . " prides itself on quality service and effective affiliate management. \nSo why wait? It's easy to join, easy to setup, and most importantly, it works. Join the " . $domain . " Affiliate Program now and earn an up to 10% commission.");
}
?>

<div class="col-md-7 login-intro">
    <img class="img-responsive login-intro-img"
         @if($language == 0) src="/img/general/affiliate/affiliate_600.jpg"
         @else src="/img/general/affiliate/affiliate_600_en.jpg" @endif>
    <h3>{{$language == 0? '联盟介绍' : 'Affiliate marketing'}} </h3>
    <p> {!! $text_intro_1 !!}</p>
    <h3>{{$language == 0 ? '加盟条件':'Why Join '.$domain.' Affiliate Program?'}} </h3>
    <p>{!! $text_intro_2 !!}</p>
</div>
