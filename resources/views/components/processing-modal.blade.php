{{--Modal start--}}
<div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="text-align:center; margin-top:35vh;">
        <img src="/img/general/loading-spinner.gif" class="icon" alt="loading spinner"/>
        <h4 id="ifMobileHide">
            @if(0 === $language) 正在为您跳转... @else  Please wait patiently... @endif
        </h4>
    </div>
    <button id="closeProcessingModal" class="close" style="float: none;" data-dismiss="modal" aria-hidden="true"></button>
</div>
<button id="openProcessingModal" class="hide" aria-hidden="true" data-toggle="modal" data-target="#processing-modal"></button>
{{--modal end--}}