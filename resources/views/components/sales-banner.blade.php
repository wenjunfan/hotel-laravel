@if(isset($_COOKIE['a']) && !empty($_COOKIE['a']))
	<div class="salesBanner-wrapper @if(url()->current() == config('app.url')) hidden @endif" id="salesBanner">
		<div class="container">
			<div class="row">
				<div class="col-sm-2 card">工号: {{$_COOKIE['a']}}</div>
				<label for="#" class="col-sm-1 card">链接: </label>
				<input type="text" class="col-sm-5 card" id="shareLink" value="{{ request()->fullUrl() . '&no=' . $_COOKIE['a'] }}">
				<div class="col-sm-2">
					<button type="button" class="btn btn-primary" onclick="copyUrl()">复制</button>
				</div>
			</div>
		</div>
	</div>
@endif

<script type="text/javascript">
	function copyUrl() {
		var copyEle = $('#shareLink');
		copyEle.select();
		document.execCommand('Copy');
		alert('Copied!')
	}
</script>