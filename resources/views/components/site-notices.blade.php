<?php

$internal_ips = ['69.75.227.206', '76.79.234.181'];
$ip = request()->getClientIp();
$ip_long = ip2long($ip);
?>

@if((in_array($ip, $internal_ips) || $ip_long < ip2long('192.168.255.255') && $ip_long > ip2long('192.168.0.0')) && config('app.show_site_notes') && (session('hidenotes', 0) !== 1))
    <div class="siteNotes-wrapper" id="siteNotes">
        <a href="#" onclick="closeNotice()">
            <i class="fa fa-times icon-close"></i>
        </a>
        <p>
            @if(config('app.env') === 'local')
                当前环境是测试环境，数据有一定失真；下单时微信和支付宝收一美分，信用卡收一美元，取消测试单以获取退款
                @else
                当前环境是正式环境，下单将真实成单，请谨慎操作，后果自负
            @endif
        </p>
    </div>

    <script>
        function closeNotice () {
            document.getElementById('siteNotes').style.display = 'none';
            if (typeof(Storage) !== 'undefined') {
                sessionStorage.setItem('hidenotes', 1);
            }
        }
    </script>
@endif