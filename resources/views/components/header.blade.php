{{--c端顶部状态栏--}}
<?php
$tourDomain = '//www.usitrip.com';
$tourName = '走四方';
$welcome = '走四方美国旅游网！';
$member = '我的走四方';
if($language == 1){
    $welcome = '';
    $tourDomain = '//www.usitour.com';
} elseif($is_aa) {
    $tourDomain = '//www.supervacation.net';
    $tourName = '美亞';
    $welcome = '美亞旅遊';
    $member = '會員中心';
} elseif ($language == 2) {
    $welcome = '走四方美國旅遊網！';
    $tourDomain = '//www.usitrip.com/tw';
}
?>

@if($language == 1)
	<div id="en_topbar">
		<div class="Cons" >
			<a id="top_logo_usitour" href="{{ config('app.url') }}">
				@if($is_usitour)
					<div class="fl usitour-logo-new" alt="logo"></div>
				@else
					<div class="fl usitour-logo-new-cn" alt="logo"></div>
				@endif
			</a>
			<ul style="list-style-type:none !important;" class="en_menu-list">
				<li class="rotate180_box en_myusi">
					<div class="myusi-text ml10_r10">
						<a class="title color_fff" href="#" onclick="return false;">
							<span>My Account</span>&nbsp;&nbsp;<i class="fa fa-angle-down rotate180" aria-hidden="true"></i>
						</a>
					</div>
					<dl id="js_myusi_list" class="myusi-list" style="width: 190px; display: none;">
						<dd><a href="https://post.usitour.com/index.php?mod=login&language_code=utf-8" rel="nofollow" class="js_myusi_color">Sign In</a></dd>
						<dd><a id="signUpBtn" href="https://post.usitour.com/index.php?mod=create_account&language_code=utf-8" rel="nofollow">Create an Account</a></dd>
					</dl>
				</li>

				<li class="rotate180_box en_help">
					<div class="help-text ml10_r10">
						<a class="title color_fff" href="javascript:">Help&nbsp;&nbsp;<i class="fa fa-angle-down rotate180" aria-hidden="true"></i></a>
					</div>
					<dl class="help-list" style="display: none;">
						<dd><a href="https://www.usitour.com/faq_question.html">FAQs</a></dd>
						<dd><a href="https://www.usitour.com/contact_us.html">Contact Us</a></dd>
						<dd><a>1-626-898-7658&nbsp;(24/7)</a></dd>
					</dl>
				</li>

				<li class="rotate180_box en_language">
					<div class="selected-language pngFix en ml10_r10">
						<a class="title color_fff">English&nbsp;&nbsp;<i class="fa fa-angle-down rotate180" aria-hidden="true"></i></a>
					</div>
					<dl class="site-language" style="display: none;">
						@if(!$is_usitour)
							<dd class="pngFix zh"><a onclick="updateLanguage(0)">简体中文</a></dd>
						@endif
						<dd class="pngFix tw"><a onclick="updateLanguage(2)">繁體中文</a></dd>
					</dl>
				</li>
				<li class="rotate180_box en_money js_currencies"></li>
			</ul>
		</div>
	</div>
@else
	<div id="topbar">
		<div class="g-wrap uifix">
			<div class="fl login-wrap" id="login-wrap">
				<span class="bar-welcome pngFix">@lang('master.home_hi'){{$welcome}}</span>
				<span class="notice">
					<div id="notice_0">
						<span id="notice_1">
							<a href="{{$tourDomain}}/announce/showAnnounce/id-93.html" target="_blank">花YOUNG跨年，8折燃爆2018</a>
							<a href="{{$tourDomain}}/announce/showAnnounce/id-91.html" target="_blank">立减$88，极光列车赴阿拉斯加之约</a>
						</span>
						<span>
							<a href="{{$tourDomain}}/announce/showAnnounce/id-93.html" target="_blank">花YOUNG跨年，8折燃爆2018</a>
							<a href="{{$tourDomain}}/announce/showAnnounce/id-91.html" target="_blank">立减$88，极光列车赴阿拉斯加之约</a>
						</span>
					</div>
				</span>
			</div>
			<div class="toolbar fr uifix">
				<div class="bar-menu">
					<ul class="menu-list">
						<li class="myusi" id="js-myusi">
							<div class="myusi-text">
								<a class="title" href="{{$tourDomain}}/index.php?mod=account&language_code=gb2312"><span>{{$member}}</span></a>
								<span class="down-arrow"><!----></span>
							</div>
							<dl class="myusi-list">
								<dd>
									<a href="{{$tourDomain}}/index.php?mod=account_history&language_code=gb2312" rel="nofollow">@lang('master.my_order')</a>
								</dd>
								<dd>
									<a href="{{$tourDomain}}/index.php?mod=account_favorite&language_code=gb2312" rel="nofollow">我的收藏</a>
								</dd>
								<dd>
									<a href="{{$tourDomain}}/index.php?mod=account_ordersFastQuery&language_code=gb2312" rel="nofollow">@lang('master.order_look_up')</a>
								</dd>
							</dl>
						</li>
						<li class="cart" id="js-cart">
							<div class="cart-bd">
								<div class="cart-text pngFix">
									<a class="title" href="{{$tourDomain}}/index.php?mod=shopping_cart&language_code=gb2312" rel="nofollow"><span>@lang('master.shopping_cart')<strong class="total">0</strong></span></a>
									<span class="down-arrow"><!----></span>
								</div>

								<div class="expand-shoppingcart">
									<dl class="cart-list" id="js-cart-list"></dl>

									<div class="product-total" id="js-pro-total">
										<span>共<em>0</em>件商品 @lang('master.total_amount'):</span><strong class="total">$0.00</strong>
									</div>
									<div class="goshoppingcart">
										<a href="{{$tourDomain}}/index.php?mod=shopping_cart&language_code=gb2312" rel="nofollow"><span>@lang('master.check_out')</span></a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div id="topCurrencyBox" class="bar-language bar-money">
								<div class="bar-language-bd">
									<div class="selected-language ">
										<span id="defCurrency">@lang('master.USD')</span> <span class="down-arrow"><!----></span>
									</div>

									<ul class="site-language">
										<li class="pngFix">
											<a href="javascript:void(0)" cur="usd"><span class="title"></span>@lang('master.USD')</a></li>
										<li class="pngFix">
											<a href="javascript:void(0)" cur="cny"><span class="title"></span>@lang('master.CNY')</a></li>
										<li class="pngFix">
											<a href="javascript:void(0)" cur="eur"><span class="title"></span>@lang('master.EUR')</a></li>
										<li class="pngFix">
											<a href="javascript:void(0)" cur="gbp"><span class="title"></span>@lang('master.GBP')</a></li>
										<li class="pngFix">
											<a href="javascript:void(0)" cur="aud"><span class="title"></span>@lang('master.AUD')</a></li>
										<li class="pngFix">
											<a href="javascript:void(0)" cur="hkd"><span class="title"></span>@lang('master.HKD')</a></li>
										<li class="pngFix">
											<a href="javascript:void(0)" cur="twd"><span class="title"></span>@lang('master.TWD')</a></li>
										<li class="pngFix">
											<a href="javascript:void(0)" cur="cad"><span class="title"></span>@lang('master.CAD')</a></li>
										<li class="pngFix">
											<a href="javascript:void(0)" cur="nzd"><span class="title"></span>@lang('master.NZD')</a></li>
									</ul>
								</div>
							</div>
						</li>
						@if(!$is_aa)
						<li>
							<div class="bar-language js-bar-language">
								<div class="bar-language-bd">
									@if($language == 2)
										<div class="selected-language tw pngFix">
											<a class="title"><span>繁體中文</span></a>
											<span class="down-arrow"></span>
										</div>
									@else
										<div class="selected-language zh pngFix">
											<a class="title"><span>简体中文</span></a>
											<span class="down-arrow"></span>
										</div>
									@endif

									<ul class="site-language js-site-language">
										<li class="en pngFix">
											<a onclick="updateLanguage(1)"><span class="title">English</span></a>
										</li>
										@if($language == 2)
											@if(!$is_usitour)
												<li class="zh pngFix">
													<a onclick="updateLanguage(0)"><span class="title">简体中文</span></a>
												</li>
											@endif
										@else
											<li class="tw pngFix">
												<a onclick="updateLanguage(2)"><span class="title">繁體中文</span></a>
											</li>
										@endif
									</ul>
								</div>
							</div>
						</li>
						<li class="weixin phone-site pngFix"><p>
								<a href="#"><span class="title">
								</span> <span class="split">|</span> </a>
							</p>

							<div class="weixin-bg-wrap">
								<p class="arrow pngFix"><!----></p>
								<div class="bg shoujibg">
									<p class="desc">m.usitrip.com</p>
								</div>
							</div>
						</li>
						<li class="weixin pngFix"><p><a href="#"><span class="title"></span><span class="split">|</span></a>
							</p>

							<div class="weixin-bg-wrap">
								<p class="arrow pngFix"><!----></p>

								<div class="bg">
									<p class="desc">加微信 享优惠</p>
								</div>
							</div>
						</li>
						<li class="help pngFix">
							<a href="{{$tourDomain}}/faq_question.html"><span class="title"></span></a></li>
							@else
							<style>
								.notice,.hot-tip, #whyUs, .subscribe-box, .question, #affiliateBox ,.why-us-block, .exchange-rate-warning,.tell24-other-phone, #MEIQIA-BTN {
									display: none !important;
								}
								.logo a{
									margin-left: -5px;
									background: url(/img/partnerImgs/aaLogo.png) no-repeat;
								}
								.tell24-img {
									background: url(/img/partnerImgs/aaPhone.png) no-repeat 5px 0;
								}
							</style>
						@endif
					</ul>
				</div>
			</div>
		</div>
	</div>
	{{--只有中文站才有的顶部logo栏--}}
	<header class="border-bottom">
		<div id="" class="white-bg">
			<div class="row">
				<div id="head">
					<div class="hd">
						<div class="fl logo">
							@if($is_aa)
								<a id="top_logo_demo" href="{{$tourDomain}}" title="歡迎來到美國亞洲旅行社-訂房">{{$tourName}}旅行社-訂房</a>
							@elseif($language == 2)
								<strong>
									<a id="top_logo_usitrip" class="top_logo_usitrip_tw" href="{{ url('/') }}" title="歡迎來到走四方美國旅遊網">
										{{$tourName}}旅遊網-訂房
										<div class="hidden-cont"><img src="https://www.usitrip.com/tpl/www/image/2015/about/honour-01.jpg" alt="走四方最高信用評級的證書"></div>
									</a>
								</strong>
							@else
								<a id="top_logo_usitrip" href="{{ url('/') }}" title="欢迎来到走四方美国旅游网-酒店">{{$tourName}}美国旅游网-酒店</a>
							@endif
						</div>
						<div class="fl">
							<div class="hdsearch"></div>
						</div>

						@if($language === 2 && !$is_aa)
							<div class="fr hdContact">
								<div class="tell24-img tell24-img-tw"></div>
								<p class="tell24-split-line"></p>
								<!-- 其他国家客服电话 -->
								<div class="tell24-other-phone">
									<p><b>北美免費：</b><span>888-887-2816</span></p>
									<p><b>美國：</b><span>1-626-898-7800</span></p>
									<p><b>加拿大：</b><span>1-647-243-3769</span></p>
									<p><b>香港：</b><span>852-5806-0465</span></p>
									<p><b>台灣：</b><span>886-2-5594-1486</span></p>
									<p class="line24"></p>
									<p>客製包團</p>
									<p><b>美國：</b><span>1-626-389-0706</span></p>
								</div>
							</div>
						@else
							<div class="fr hdContact">
								<div class="tell24-img"></div>
								<p class="tell24-split-line"></p>
								<!-- 其他国家客服电话 -->
								<div class="tell24-other-phone">
									<p><b>北美免费：</b><span>888-887-2816</span></p>
									<p><b>美国：</b><span>1-626-898-7800</span></p>
									<p><b>加拿大：</b><span>1-647-243-3769</span></p>
									<p class="line24"></p>
									<p><b>中国：</b><span>025-6668-0241</span></p>
									<p><b>香港：</b><span>852-5806-0465</span></p>
									<p class="line24"></p>
									<p>定制包团</p>
									<p><b>美国：</b><span>1-626-389-0706</span></p>
									<p><b>中国：</b><span>025-6668-0241</span></p>
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</header>
@endif