<style>
    .footer-nav {
        border-top: 1px solid #a7a9ab;
        padding: 30px 24px;
        text-align: center;
    }
    .footer-nav .row {
        font-size: 14px;
        margin-bottom: 13px;
    }
    .footer-nav .row a {
        color: #2681ff !important;
    }
    .footer-copywrite {
        font-size: 12px;
        color: #2681ff;
    }
</style>
<div class="footer-nav">
    <div class="row">
        <div class="col-xs-4">
            <a href="@if($language === 1) https://m.usitour.com/about_us.html @elseif($language === 2) https://m.usitrip.com/tw/about_us.html @else https://m.usitrip.com/links.html @endif">@lang('footer.about_us')</a>
        </div>
        <div class="col-xs-4">
            <a href="@if($language === 1) https://m.usitour.com/contact_us.html @elseif($language === 2) https://m.usitrip.com/tw/contact_us.html @else https://m.usitrip.com/contact_us.html @endif">@lang('footer.contact_us')</a>
        </div>
        <div class="col-xs-4">
            <a href="@if($language === 1) https://m.usitour.com/feedback.html @elseif($language === 2) https://m.usitrip.com/tw/feedback.html @else https://m.usitrip.com/feedback.html @endif">@lang('footer.feedbacks')</a>
        </div>
    </div>
    <div class="footer-copywrite">
        Copyright &copy; 2008-{{\Carbon\Carbon::now()->year}}
        @if($is_usitour)
            <span>Usitour.com</span>
        @elseif($is_b)
            <span>117book.com</span>
        @else
            <span>Usitrip.com</span>
        @endif | All Rights Reserved
    </div>
</div>