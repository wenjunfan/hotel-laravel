<nav class="navbar-default navbar-static-side" style="position: fixed;height: 100%" role="navigation">
    <div class="sidebar-collapse" style="height: inherit">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span><img alt="image" style="margin-left:35px; background:#ffffff;" class="img-circle" src="{{asset('img/business/nav-logo.png')}}" alt="nav logo" /></span>
                </div>
                <div class="logo-element">
                    @if($language !== 1)
                        @lang('master.navlogo')
                    @else
                        <img src="@lang('master.navlogo')" style="width:26px !important;" alt="nav logo" />
                    @endif
                </div>
            </li>
            @if(Auth::user()->admin == 1)
                <li class="landing_link">
                    <a href="{{ url('/admin/orders') }}" titel="USER"><i class="fa fa-user-friends"></i>
                        <span class="nav-label">@lang('master.supper_admin')</span>
                        <span class="label label-warning pull-right">USER</span></a>
                </li>
            @endif
            @if(Auth::user()->admin == 2)
                <li class="landing_link">
                    <a @if(Auth::user()->id == 11173 || Auth::user()->id == 11178)
                         href="{{ url('/admin/order/list?partnerId=11087') }}"
                         @elseif(Auth::user()->id== 10057|| Auth::user()->id== 10575|| Auth::user()->id== 11379)
                         href="{{ url('/admin/order/list') }}"
                         @else
                         href="{{ url('/admin/order/list') }}"
                         @endif titel="USER"><i class="fa fa-user-friends"></i>
                        <span class="nav-label">@lang('master.finacial_admin')</span>
                        <span class="label label-warning pull-right">USER</span>
                    </a>
                </li>
            @endif
            @if(Auth::user()->admin == 3)
                <li class="landing_link">
                    <a href="{{ url('/admin/orders') }}" titel="USER"><i class="fa fa-user-friends"></i>
                        <span class="nav-label">@lang('master.channel_admin')</span>
                        <span class="label label-warning pull-right">USER</span></a>
                </li>
            @endif
            @if(Auth::user()->admin == 1 || Auth::user()->admin == 2)
                <li class="{{ optionActive($optionName, 'dashboard') }}">
                    <a href="{{ url('/dashboard') }}" title="@lang('master.dash_board')"><i class="fa fa-chart-bar"></i>
                        <span class="nav-label">@lang('master.dash_board')</span></a>
                </li>
            @endif
            @if(Auth::user()->active == 1 || Auth::user()->active == 2)
                <li class="{{ optionActive($optionName, 'search') }}">
                    <a href="{{ url('/search') }}" title="@lang('master.search')"><i class="fa fa-hotel"></i>
                        <span class="nav-label">@lang('master.search')</span></a>
                </li>
                @if($language != 1)
                    <li class="{{ optionActive($optionName, 'tours') }}">
                        <a href="{{ url('/tours') }}" title="@lang('master.tours')">
                            <i class="fa fa-umbrella-beach" title="@lang('master.tours')"></i>
                            <span class="nav-label">@lang('master.tours')</span>
                        </a>
                    </li>
                @endif
            @endif
            @if(Auth::user()->id != config('app.2b_ad_partner_id') && Auth::user()->id != config('app.2b_test_partner_id'))
                @if(Auth::user()->active == 1)
                    <li class="{{ optionActive($optionName, 'hotel orders') }}">
                        <a href="{{ url('/orders') }}" title="@lang('master.hotel_order')"><i class="fa fa-shopping-cart"></i>
                            <span class="nav-label">@lang('master.hotel_order')</span></a>
                    </li>
                @endif
                @if(Auth::user()->active == 1 && $language != 1)
                    <li class="{{ optionActive($optionName, 'tour orders') }}">
                        <a href="{{ url('/orders/tours') }}" title="@lang('master.tour_order')"><i class="fa fa-shopping-basket"></i>
                            <span class="nav-label">@lang('master.tour_order')</span></a>
                    </li>
                @endif
                @if(Auth::user()->active == 1)
                    <li class="{{ optionActive($optionName, 'refunds') }}">
                        <a href="{{ url('/refunds') }}" title="@lang('master.bcard')"><i class="fa fa-credit-card"></i>
                            <span class="nav-label">@lang('master.bcard')</span></a>
                    </li>
                @endif
                @if(Auth::user()->active == 1)
                    <!--li class="{{ optionActive($optionName, 'alipayRefunds') }}">
                        <a href="{{ url('/alipayRefunds') }}" title="@lang('master.alipay')"><i class="fa fa-paypal"></i>
                            <span class="nav-label">@lang('master.alipay')</span></a>
                    </li-->
                @endif
                <li class="{{ optionActive($optionName, 'profile') }}">
                    <a href="{{ url('/profile') }}" title="@lang('master.account')"><i class="fa fa-user"></i>
                        <span class="nav-label">@lang('master.account')</span></a>
                </li>
            @endif
            <li class="{{ optionActive($optionName, 'guide') }}">
                <a href="{{ url('/guide') }}" title="@lang('master.guide')"><i class="fa fa-book"></i>
                    <span class="nav-label">@lang('master.guide')</span></a>
            </li>
            <li class="{{ optionActive($optionName, 'copyright') }}" style="position:absolute; bottom:0;background-color:#2f4050;width: 100%;">
                <a>
                    <i class="fa fa-copyright" title="@lang('master.copyright')"></i>
                    <span class="nav-label">@lang('master.copyright')</span>
                </a>
            </li>
        </ul>
    </div>
</nav>