<?php
// 1-admin 2-accounting
$is_admin_n_accounting = (Auth::user()->admin == 2 || Auth::user()->admin == 1);
$marketing = in_array(Auth::user()->email, config('constants.marketing'), true);
// add active class to nav
function optionActive($optionName, $name)
{
    return (isset($optionName) && $optionName == $name) ? 'active' : '';
}
//pass Distributor
use App\Model\Distributor;
$distributors = Distributor::where('partner_id', '<>', '')->get();
?>
<nav class="navbar-default navbar-static-side" style="position: fixed;" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                {{--C端LOGO信息--}}
                @if(!$is_b)
                    <div class="dropdown profile-element">
                        <div><img class="img-circle" src="{{ asset('img/general/logo/logo-usitrip-avatar.png')}}" alt="img circle" /></div>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="m-t-xs"><strong class="font-bold">{{ Auth::user()->name }}</strong></span>
                            <span class="text-muted text-xs">{{ Auth::user()->company_name }}</span>
                        </span>
                        </a>
                    </div>
                @else
                    {{--B端LOGO信息--}}
                    <div class="dropdown profile-element">
                        {{--if want to open avatar later, close for now--}}
                        <div><img class="img-circle" src="{{ asset('img/general/a1.jpg') }}" alt="img circle"/></div>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="m-t-xs"><strong class="font-bold">{{ Auth::user()->name }}</strong></span>
                            <span class="text-muted text-xs">{{ Auth::user()->company_name }}<b class="caret"></b></span>
                        </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="{{ url('profile') }}">账户信息</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ url('logout') }}">安全退出</a></li>
                        </ul>
                    </div>
                @endif
                <div class="logo-element">
                    @if(!$is_b)
                        {{$domain}}
                    @else
                        要趣订
                    @endif
                </div>
            </li>
            <li class="landing_link">
                <a href="{{ url('/search') }}" title="Admin">
                    <i class="fa fa-user"></i>
                    <span class="nav-label">{{$language == 0 ? '切换至用户模式' : 'Hotel Search'}}</span>
                    <span class="label label-warning pull-right">{{$language == 0 ? '联盟' : 'Affiliate'}}</span>
                </a>
            </li>

            {{--C端导航 start --}}
            @if(!$is_b)
                @if(Auth::user()->id == 11180)
                @else
                    <li class="{{ optionActive($optionName,'dashboard')}}">
                        <a href="{{ url('affiliate/dashboard') }}" title="{{$language == 0 ? '联盟条例' : 'Affiliate Policies'}}">
                            <i class="fa fa-comment"></i>
                            <span class="nav-label">{{$language == 0 ? '联盟条例' : 'Affiliate Policies'}}</span>
                        </a>
                    </li>
                    <li class="{{ optionActive($optionName,'profile')}}">
                        <a href="{{ url('affiliate/profile') }}" title="{{$language == 0 ? '账户信息' : 'Account Info'}}">
                            <i class="fa fa-user"></i>
                            <span class="nav-label">{{$language == 0 ? '账户信息' : 'Account Info'}}</span>
                        </a>
                    </li>
                @endif
               @if(Auth::user()->admin  !== 5)
                <li class="{{ optionActive($optionName, 'orders') }}">
                    @if(!$is_b)
                        @if($is_admin_n_accounting || $marketing )
                            <a href="{{ url('affiliate/orders/All117BookAffiliatePartner') }}" title="{{$language == 0 ? '订单管理' : 'Order Management'}}">
                                <i class="fa fa-shopping-cart"></i>
                                <span class="nav-label">{{$language == 0 ? '订单管理' : 'Order Management'}}</span>
                            </a>
                        @endif

                        @foreach($distributors as $distributor)
                            @if(Auth::user()->id == $distributor->partner_id && !$marketing)
                                <a href="{{ url('affiliate/orders/'. $distributor->distributor_code) }}" title="{{$language == 0 ? '订单管理' : 'Order Management'}}">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span class="nav-label">{{$language == 0 ? '订单管理' : 'Order Management'}}</span>
                                </a>
                            @endif
                        @endforeach
                    @endif
                </li>
                @endif
                @if(Auth::user()->admin  === 4 && Auth::user()->affiliateRate !== 0 && !$marketing)
                <li class="{{ optionActive($optionName, 'welcome') }}">
                        <a href="{{ url('affiliate/welcome/') }}" title="{{$language == 0 ? '欢迎页' : 'Welcome'}}">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">{{$language == 0 ? '欢迎页' : 'Welcome'}}</span>
                        </a>
                </li>
                @endif
                @if($is_admin_n_accounting || $marketing )
                    <li class="{{ optionActive($optionName, 'affiliate-accounts') }}">
                        <a href="{{ url('/affiliate/accounts') }}" title="账户管理"><i class="fa fa-user-friends"></i>
                            <span class="nav-label">账户管理</span></a>
                    </li>
                @endif
                @if($is_admin_n_accounting || Auth::user()->id == 11180 || $marketing)
                    <li class="{{ optionActive($optionName, 'fraud-list') }}">
                        <a href="{{ url('/affiliate/fraud-list') }}" title="黑白名单管理"><i class="fa fa-lock"></i>
                            <span class="nav-label">黑白名单 (Fraud)</span></a>
                    </li>
                    <li class="{{ optionActive($optionName, 'missed-lists') }}">
                        <a href="{{ url('/affiliate/missed-lists') }}" title="未完订单"><i class="fa fa-exclamation-triangle"></i>
                            <span class="nav-label">未完订单</span></a>
                    </li>
                    <li class="{{ optionActive($optionName, 'potential-lists') }}">
                        <a href="{{ url('/affiliate/potential-lists') }}" title="潜在订单"><i class="fa fa-heart"></i>
                            <span class="nav-label">潜在订单</span>
                        </a>
                    </li>
                @elseif(Auth::user()->id == 11676)
                    <li class="{{ optionActive($optionName, 'fraud-list') }}">
                        <a href="{{ url('/affiliate/fraud-list') }}" title="可疑订单管理"><i class="fa fa-lock"></i>
                            <span class="nav-label">黑白名单 (Fraud)</span></a>
                    </li>
                    <li class="{{ optionActive($optionName, 'missed-lists') }}">
                        <a href="{{ url('/affiliate/missed-lists') }}" title="微信未完订单"><i class="fa fa-exclamation-triangle"></i>
                            <span class="nav-label">未完订单</span></a>
                    </li>
                @endif
                {{--酒店调价 市场部--}}
                @if($is_admin_n_accounting || $marketing )
                    <li class="{{ optionActive($optionName, 'price-ratio') }}">
                        <a href="{{ url('/affiliate/price-ratio') }}" title="酒店调价"><i class="fa fa-dollar-sign"></i>
                            <span class="nav-label">酒店调价</span></a>
                    </li>
                @endif
            @endif
            {{--C端导航 end --}}

            {{--B端 导航 start--}}
            @if($is_b)
                @if($is_admin_n_accounting)
                    <li class="{{ optionActive($optionName, 'dashboard') }}">
                        <a href="{{ url('/admin/dashboard') }}" title="业务总览"><i class="fa fa-chart-bar"></i>
                            <span class="nav-label">业务总览</span></a>
                    </li>
                @endif
                {{--B端 订单列表--}}
                <li class="{{ optionActive($optionName, 'orders') }}">
                <a href="{{ url('/admin/orders') }}" title="订单管理">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="nav-label">订单管理</span>
                </a>
                </li>
                @if($is_admin_n_accounting)
                <li class="{{ optionActive($optionName, 'checkOrders') }}">
                    @if(Auth::user()->id == 11173 || Auth::user()->id == 11178 )
                        <a href="{{ url('/admin/order/list?partnerId=11087') }}" title="到账记录">
                        <i class="fa fa-list-alt"></i> <span class="nav-label">C端到账记录</span></a>
                    @else
                         <a href="{{ url('/admin/order/list') }}" title="到账记录">
                         <i class="fa fa-list-alt"></i> <span class="nav-label">到账记录</span></a>
                    @endif
                </li>
                @endif
                <li class="{{ optionActive($optionName, 'statements') }}">
                    <a href="{{ url('/admin/statements/all') }}" title="账户流水"><i class="fa fa-money-bill-alt"></i>
                        <span class="nav-label">账户流水</span></a>
                </li>
                <li class="{{ optionActive($optionName, 'payments') }}">
                    <a href="{{ url('/admin/payments/all') }}" title="扣款记录"><i class="fa fa-search-dollar"></i>
                        <span class="nav-label">扣款记录</span></a>
                </li>
                <li class="{{ optionActive($optionName, 'refunds') }}">
                    <a href="{{ url('/admin/refunds/all') }}" title="信用卡退款记录"><i class="fa fa-credit-card"></i>
                        <span class="nav-label">信用卡退款记录</span></a>
                </li>
                {{--<li class="{{ optionActive($optionName, 'alipayRefunds') }}">--}}
                    {{--<a href="{{ url('/admin/alipayRefunds/all') }}" title="支付宝退款记"><i class="fa fa-paypal"></i>--}}
                        {{--<span class="nav-label">支付宝退款记录</span></a>--}}
                {{--</li>--}}
                <li class="{{ optionActive($optionName, 'accounts') }}">
                    <a href="{{ url('/admin/accounts') }}" title="账户管理"><i class="fa fa-user-friends"></i>
                        <span class="nav-label">账户管理</span></a>
                </li>
                <li class="{{ optionActive($optionName, 'feedbacks') }}">
                    <a href="{{ url('/admin/feedbacks') }}" title="用户问题"><i class="fa fa-comment"></i>
                        <span class="nav-label">用户问题</span></a>
                </li>
                <li class="{{ optionActive($optionName, 'groupRequests') }}">
                    <a href="{{ url('/admin/groupRequests') }}" title="团房请求"><i class="fa fa-heartbeat"></i><span
                            class="nav-label">团房请求</span></a>
                </li>
                <li class="{{ optionActive($optionName, 'missed-lists') }}">
                    <a href="{{ url('/admin/missed-lists') }}" title="未完订单"><i class="fa fa-exclamation-triangle"></i>
                        <span class="nav-label">未完订单</span></a>
                </li>
                <li class="{{ optionActive($optionName, 'potential-lists') }}">
                    <a href="{{ url('/admin/potential-lists') }}" title="潜在订单"><i class="fa fa-heart"></i>
                        <span class="nav-label">潜在订单</span>
                    </a>
                </li>
            @endif
            {{--B端 导航 end--}}
            {{--市场和admin可以看coupon和feedback--}}
            @if($is_admin_n_accounting || $marketing)
                <li class="{{ optionActive($optionName, 'deal-list') }}">
                    <a href="{{ url('/affiliate/deal-list') }}" title="折扣列表">
                        <i class="fa fa-file"></i>
                        <span class="nav-label">折扣列表</span>
                    </a>
                </li>
                <li class="{{ optionActive($optionName, 'feedback-list') }}">
                    <a href="{{ url('/affiliate/feedback-list') }}" title="用户反馈">
                        <i class="fa fa-envelope"></i>
                        <span class="nav-label">用户反馈</span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</nav>

