{{--group request modal--}}
<style>
    #noRobots{
        margin:10px auto;
        width: fit-content;
    }
    #buttonsmall{
        font-size:15px;
        font-weight:500;
        margin-top:0
    }
</style>
<div class="modal fade" id="myModalHorizontal" tabprindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="grouprequest">
        {{ csrf_field() }}
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div>
                            <button style="float:right; padding-right:15px;" type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&#x2715;</span> <span class="sr-only">Close</span>
                            </button>
                            <br/> <br/>
                            <h2 class="modal-title text-center" id="myModalLabel">{{trans('group-request.group_title')}}</h2><br>
                            <p class="modal-title col-sm-12" id="myModalLabel" style="font-size:13px; color:#767676; padding-bottom:20px;">{{trans('group-request.group_description')}} </p>
                            <div class="item-required" style="padding:0 15px;">
                                <input type="text" style="margin-bottom:10px;" class="form-control" id="destination_one" name="destination_one"
                                       placeholder="{{trans('group-request.group_input_city')}}" required/>
                            </div>
                            <div class="item-required input-group" style="padding:0 15px 10px 15px;">
                                <input style="padding-left:12px;" class="form-control datepicker" id="checkin_one" name="checkin_one" onchange="gRupdateCheckin()"
                                       placeholder="{{trans('group-request.group_input_checkin')}}" required
                                       autocomplete="off"/>
                                <span class="input-group-addon">
									<i class="fa fa-long-arrow-alt-right"></i>
								</span>
                                <input style="padding-left:12px;" class="form-control datepicker" id="checkout_one"  onchange="gRupdateCheckout()" name="checkout_one"
                                       placeholder="{{trans('group-request.group_input_checkout')}}" required
                                       autocomplete="off"/>
                            </div>
                            <label for="hidemorerequest" id="changetxt" onclick="changeText()" style="margin-left:15px; padding-bottom:5px;">+ {{trans('group-request.group_add_another')}}</label>
                            <input type="checkbox" class="checked" style="position:absolute; display:none;" id="hidemorerequest">
                            <div class="anotherdestination">
                                <div style="padding:0 15px;">
                                    <input type="text" style="margin-bottom:10px;" class="form-control" id="destination_two" name="destination_two" placeholder="{{trans('group-request.group_input_backup_city')}}"/>
                                </div>
                                <div class="input-group date" style="padding:0 15px 20px 15px;">
                                    <input style="padding-left:12px;" class="form-control datepicker" id="checkin_two" name="checkin_two" placeholder="{{trans('group-request.group_input_checkin')}}"
                                           autocomplete="off"/>
                                    <span class="input-group-addon">
										<i class="fa fa-long-arrow-alt-right"></i>
									</span>
                                    <input style="padding-left:12px;" class="form-control datepicker" id="checkout_two" name="checkout_two" placeholder="{{trans('group-request.group_input_checkout')}}"
                                           autocomplete="off"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div style="padding:0 15px;">
                            <input type="text" style="margin-bottom:10px;" class="form-control" id="groupnumber" name="groupnumber" placeholder="{{trans('group-request.group_input_group_code')}}"/>
                        </div>
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-6  item-required">
                                <input type="number" min="9" class="form-control" id="roomnumber" name="roomnumber" placeholder="{{trans('group-request.group_input_rpn')}}" required/>
                            </div>
                            <div class="col-sm-6  item-required" style="padding-bottom:10px;">
                                <label for="rate"></label>
                                <select id="rate" name="rate" class=" form-control  search_input" required  style="border: 1px solid #e7eaec; padding-left:12px;
                                padding-right:10px;">
                                    <option value="" disabled selected hidden>{{trans('group-request.group_input_star')}}</option>
                                    <option value="1-3">1-3 {{trans('group-request.stars')}}</option>
                                    <option value="2-3">2-3 {{trans('group-request.stars')}}</option>
                                    <option value="2-4">2-4 {{trans('group-request.stars')}}</option>
                                    <option value="3-4">3-4 {{trans('group-request.stars')}}</option>
                                    <option value="3-5">3-5 {{trans('group-request.stars')}}</option>
                                    <option value="4-5">4-5 {{trans('group-request.stars')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 item-required">
                            <input type="text" class="form-control" id="budget" name="budget" placeholder="{{trans('group-request.group_input_budget')}}" required/>
                            <br/>
                        </div>
                    </div>
                    <div>
                        <p style="color:#484848; font-weight:500; font-size:14px; padding-left:15px;">{{trans('group-request.group_contact')}} </p>
                    </div>
                    <div>
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-6 item-required">
                                <input type="text" class="form-control" id="contactname" name="contactname" placeholder="{{trans('group-request.group_input_name')}}" required/>
                            </div>
                            <div class="col-sm-6" style="padding-bottom:10px;">
                                <input type="text" class="form-control" id="companyname" name="companyname" placeholder="{{trans('group-request.group_input_company')}}"/>
                            </div>
                            <br/>
                        </div>
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-6  item-required">
                                <input type="email" class="form-control" id="email" name="email" placeholder="{{trans('group-request.group_input_email')}}" required/>
                            </div>
                            <div class="col-sm-6  item-required" style="padding-bottom:10px;">
                                <input type="tel" class="form-control" id="phone" name="phone" placeholder="{{trans('group-request.group_input_phone')}}" required/>
                            </div>
                            <br/>
                        </div>
                        <div class="col-sm-12">
                            <textarea type="text" style="resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;" rows="3"
                                      cols="80" class="form-control" id="remark" name="remark" placeholder="{{trans('group-request.group_input_additional')}}"></textarea>
                        </div>
                        <div class="col-sm-12">
                        <div id="noRobots" class="g-recaptcha" data-sitekey="{{config('app.recaptcha_site_key')}}" data-callback="correctCaptcha"></div>
                        <button id="buttonsmall" type="button" class="btn btn-primary submitgrouprequest btn-block" disabled onclick="createGroupRequest();">
                            {{trans('group-request.modal_btn_submit')}}</button>
                        </div>
                        <p style="text-align:center; padding-top:10px; padding-bottom:20px;">{{trans('group-request.btn_submit_check')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script src="https://recaptcha.net/recaptcha/api.js"></script>
<script>
    var correctCaptcha = function(response) {
        if(response.length == 0){
            swal("Are you a robot?","Please verify at first, thanks", "error");
        }else{
            $('#buttonsmall').attr('disabled', false);
        }
    };
  function createGroupRequest() {
    var isValid = true;
    if($('#rate').val() == null){
      isValid = false;
    }
    $('input').filter('[required]:visible').each(function() {
      $(this).val($(this).val().trim());
      if ($(this).val() === '' ){
        $(this).focus();
        isValid = false;
      }
    });

    if(isValid === false){
      swal('{{trans('group-request.please_check_input')}}')
    }else{
      FullscreenSpinner.create();
      $.get("{{url('/group-room-request/create')}}", $("#grouprequest").serialize(), function (data) {
        if (data.success) {
          FullscreenSpinner.destroy();
          swal("Sent Success", data.message, "success");
          $('.close').click();
        } else {
          FullscreenSpinner.destroy();
          swal("Sent Failed", data.message, "error");
          $('.close').click();
        }
      });
    }
  }

  function groupRequestSetTime() {
    var today = new Date();
    // next two weeks
    today = new Date(today.getTime() + (12 * 24 * 60 * 60 * 1000));
    var tomorrow = new Date(today.getTime() + (13 * 24 * 60 * 60 * 1000));
    td = today.getFullYear() + "-" + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + "-" + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    tm = tomorrow.getFullYear() + "-" + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + "-" + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());

    $("#checkin_one").datepicker('setDate', td);
    $('#checkout_one').datepicker('setDate', tm);
  }

  function gRupdateCheckin() {
    var checkin = $("#checkin_one").val();
    var today = new Date();
    var today = today.getFullYear() + "-" + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + "-" + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    var checkin1 = checkin.replace(/-/g, '');
    var today1 = today.replace(/-/g, '');

    if (checkin1 < today1) {
      $("#checkin_one").datepicker('setDate', today);
    }
    var checkin = $("#checkin_one").val();
    var checkout = $("#checkout_one").val();
    checkin1 = checkin.replace(/-/g, '');
    checkout1 = checkout.replace(/-/g, '');

    if (parseInt(checkin1) >= parseInt(checkout1)) {
      checkin = checkin.split('-');
      var checkin = new Date(checkin[0], (checkin[1] - 1), checkin[2]);
      var n = 1;
      var checkout = new Date(checkin - 0 + n * 86400000);
      var checkout = checkout.getFullYear() + "-" + (checkout.getMonth() + 1 > 9 ? checkout.getMonth() + 1 : '0' + (checkout.getMonth() + 1)) + "-" + (checkout.getDate() < 10 ? '0' + checkout.getDate() : checkout.getDate());
      $("#checkout_one").datepicker('setDate', checkout);
    }

    $("#checkout_one").datepicker("setStartDate", checkin);

    var a1 = parseInt(checkin1);
    var a2 = parseInt(prchecktemp);

    if (a1 == a2 && prindex != 1 && prindex != 2) {
      $("#checkout_one").datepicker('show');
    }
    prchecktemp = checkin1;
    prindex++;
  }

  function gRupdateCheckout() {
    var checkout = $("#checkout_one").val();
    var checkin = $("#checkin_one").val();
    var today = new Date();
    var today = today.getFullYear() + "-" + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + "-" + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    var checkout1 = checkout.replace(/-/g, '');
    var today1 = today.replace(/-/g, '');
    var checkin1 = checkin.replace(/-/g, '');
    if (checkout1 <= checkin1) {
      var dcheckin = new Date(checkin);
      var tomorrow = new Date(dcheckin.getTime() + 2 * (24 * 60 * 60 * 1000));
      var tomorrow1 = tomorrow.getFullYear() + "-" + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + "-" + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());
      //alert(tomorrow1);
      $("#checkout_one").datepicker('setDate', tomorrow1);
    }
    var checkin = $("#checkin_one").val();
    var checkout = $("#checkout_one").val();
    var today = new Date();
    var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));

    today1 = today.getFullYear() + "" + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + "" + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    tomorrow1 = tomorrow.getFullYear() + "" + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + "" + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());
    checkin1 = checkin.replace(/-/g, '');
    checkout1 = checkout.replace(/-/g, '');

    if (parseInt(checkin1) >= parseInt(checkout1)) {
      if (parseInt(checkout1) === parseInt(today1)) {
        swal({
          title: "checkout date must beyond today",
          type: "warning"
        }, function () {
          setTimeout(function () {$('#checkin').focus();}, 1);
        });

        td = today.getFullYear() + "-" + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + "-" + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
        tm = tomorrow.getFullYear() + "-" + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + "-" + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());

        $("#checkin_one").datepicker('setDate', td);
        $("#checkout_one").datepicker('setDate', tm);
      } else {
      }
    }
  }

  function changeText() {
    var lang_close = "{{trans('group-request.group_add_close')}}";
    var lang_add_another = "{{trans('group-request.group_add_another')}}";
    var add = document.getElementById('changetxt');
    if (add.innerText === lang_close) {
      add.innerText = '+ ' + lang_add_another;
    }else {
      add.innerText = lang_close
    }
  }

</script>