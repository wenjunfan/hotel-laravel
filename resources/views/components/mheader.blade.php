<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
<style>
    html, body {
        height: 100%;
    }
    body {
        color: #4c8ecb;
        font-family: 'Lato', Helvetica, Arial, "Microsoft Yahei", "微软雅黑", STXihei, "华文细黑", sans-serif;
        position: relative;
        margin: 0;
    }
    .nav-logo-tw {
        display: block;
        margin: 0 auto;
        height: 4.69rem;
        width: 11.76rem;
        background-image: url(/img/general/icon/hotel_eng_img.png);
        background-repeat: no-repeat;
        background-position: 63% 69.379%;
        background-size: 39.16rem 16.02rem;
    }
    #master-main {
        min-height: 77.8vh;
    }
    .flex-container {
        display: flex;
        align-items: center;
        justify-content: space-between;
        height: 100%;
    }
    #hotelHead {
        flex-grow: 3;
        text-align: center;
        padding-top: 6px;
        font-size: 1.5em;
        height: 36px;
    }
    .nav-header {
        margin-bottom: 0;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        background: white !important;
        height: 50px;
    }
    .border-bottom {
        border-bottom: 1px solid #dfeaf4 !important;
    }
    .nav-header-right {
        flex-grow: 1;
    }
    .nav-header-left {
        flex-grow: 1;
        text-align: left;
        padding-left: 10px;
    }
    .nav-header-lang {
        display: inline-block;
    }
    .account {
        flex-grow: 1;
        right: 10px;
        top: 12px;
        position: absolute;
        z-index: 99;
    }
    .logo-usitour {
        margin-top: 5px;
        width: 100px;
        height: 30px;
    }
    .logo-usitrip {
        margin-top: 5px;
        width: 55px;
        height: 30px;
    }
    /*overwrite default meiqia btn style*/
    #meiqia-container {
        right: 10px !important;
        top: auto !important;
        bottom: 10px !important;
        background: url('/img/general/icon/chat_icon.png') no-repeat 0 0 !important;
        background-size: 59px auto !important;
        animation-name: fade;
        animation-duration: 2s;
        animation-iteration-count: infinite;
        z-index: 999999 !important;
    }
    @keyframes fade {
        0% {
            opacity: 0.5;
        }
        50% {
            opacity: 1;
        }
        100% {
            opacity: 0.5;
        }
    }
</style>
<header class="border-bottom">
    <nav id="page_header2" class="navbar navbar-static-top white-bg nav-header" role="navigation">
        <div class="flex-container">
            <div class="nav-header-left">
                @if($is_aa)
                    {{--hotel homepage--}}
                    @if(( url()->current() == url('/'))  )
                        <a href="//.usitour.com"> <img src="{{ asset('img/partnerImgs/aaLogo.png') }}" alt="America Asia" class="logo-usitour"> </a>
                    @else
                        <a onclick="window.location.href='/';return false;"> <img src="{{ asset('img/partnerImgs/aaLogo.png') }}" alt="America Asia" class="logo-usitrip"> </a>
                    @endif
                @elseif($is_usitour && $language !== 2)
                    {{--hotel homepage--}}
                    @if(( url()->current() == url('/'))  )
                        <a href="//m.usitour.com"> <img src="{{ asset('img/general/logo/logo-usitour.png') }}" alt="Usitour" class="logo-usitour"> </a>
                    @else
                        <a onclick="window.location.href='/';return false;"> <img src="{{ asset('img/general/logo/logo-usitour.png') }}" alt="Usitour" class="logo-usitour"> </a>
                    @endif
                @elseif($language === 2)
                    {{--hotel homepage--}}
                    @if(( url()->current() == url('/'))  )
                        <a href="//m.usitrip.com/tw"> <i class="nav-logo-tw"></i> </a>
                    @else
                        <a onclick="window.location.href='/';return false;"> <i class="nav-logo-tw"></i> </a>
                    @endif
                @else
                    {{--hotel homepage--}}
                    @if(( url()->current() == url('/')) )
                        <a href="//m.usitrip.com"> <img src="{{ asset('img/general/logo/logo-usitrip-text.png') }}" alt="走四方" class="logo-usitrip"> </a>
                    @else
                        <a onclick="window.location.href='/';return false;"> <img src="{{ asset('img/general/logo/logo-usitrip-text.png') }}" alt="走四方" class="logo-usitrip"> </a>
                    @endif
                @endif
            </div>

            {{--title--}}
            <div id="hotelHead" onclick="window.location.href='/';return false;">
                @lang('master.header')
            </div>

            <div class="nav-header-right">
                {{--account--}}
                <div class="account">
                    <a href="@if($is_usitour) //m.usitour.com/index.php?mod=login&language_code=utf-8 @elseif($is_b) /profile @else //m.usitrip.com @endif">
                        <i class="fa fa-user" style="color:#0a6aa1"></i>
                    </a>
                </div>

                {{--language--}}
                @if(strpos(request()->getUri(), 'continue') === false)
                    <div class="nav-header-lang" id="changeLang">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="langDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                @if($language == 2)
                                    繁體
                                @elseif($language ==1)
                                    English
                                @else
                                    简体中文
                                @endif
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="langDropdown" style="min-width: 60px;right: 0;">
                                <li class="@if($language == 0) hide @endif">
                                    <a href="#" onclick="updateLanguage(0)">简体中文</a>
                                </li>
                                <li class="@if($language == 1) hide @endif">
                                    <a href="#" onclick="updateLanguage(1)">English</a>
                                </li>
                                <li class="@if($language == 2) hide @endif">
                                    <a href="#" onclick="updateLanguage(2)">繁體中文</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </nav>
</header>