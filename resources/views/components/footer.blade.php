{{--c端footer--}}
<?php
if(isset($_SERVER['HTTP_HOST']) && isset($_SERVER['REQUEST_URI'])){
    $currentUrl = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}else{
    $currentUrl = 'https://hotel.usitrip.com';
}
$usitrip = 'https://www.usitrip.com';
$usitriptw = "https://www.usitrip.com/tw";
?>
@if($is_aa)
    <style>
        .copyright {
            margin-top: 55px;
            line-height: 24px;
            text-align: center;
        }
        .copyright li {
            display: inline-block;
        }
        .copyright a {
            padding: 0 2px;
            color: #666;
        }
        a, a:hover {
            text-decoration: none;
        }
    </style>
    <footer id="frWrapper">
        <!--Footer Wrapper Start-->
        <div id="foot">
            <div class="uiWrap uifix">
                <div class="copyright">
                    <ul>
                        <li><a href="http://www.supervacation.net/about_us.html" target="_blank" style="font-size:18px;">關於我們</a></li>
                        <li><a href="http://www.supervacation.net/index.php?mod=order_agreement&action=fromOrder&onlybody=1&language_code=gb2312" target="_blank" style="font-size:18px;margin-left: 15px;">訂購條例</a></li>
                    </ul>
                    <p>Copyright©1981-<span id="copyDate">2019</span> www.supervacation.net | 美國亞洲旅行社 All rights reserved.</p>
            </div>
            </div>
        </div>
        <!--Footer Wrapper End-->
    </footer>

    @else
    @if($language == 0)
    <footer class="m-footer">
        <div class="m-spec-wrap">
            <div class="g-wrap" style="overflow:hidden;">
                <ul class="m-spec-box clearfix">
                    <li>
                        <i class="m-spec-ico00"></i>
                        <p>
                            <strong>美国当地华人旅行社</strong> <span>出游有保障</span>
                        </p>
                    </li>
                    <li>
                        <i class="m-spec-ico01"></i>
                        <p>
                            <strong>美国BBB认证</strong> <span>最高商誉评级A+</span>
                        </p>
                    </li>
                    <li>
                        <i class="m-spec-ico02"></i>
                        <p>
                            <strong>价格公开透明</strong> <span>低价保证</span>
                        </p>
                    </li>
                    <li>
                        <i class="m-spec-ico03"></i>
                        <p>
                            <strong>品质线路 华人首选</strong> <span>10年服务超数百万客人</span>
                        </p>
                    </li>
                    <li>
                        <i class="m-spec-ico04"></i>
                        <p>
                            <strong>国际SSL认证</strong> <span>支付无忧</span>
                        </p>
                    </li>
                    <li>
                        <i class="m-spec-ico05"></i>
                        <p>
                            <strong>免费签证邀请函</strong> <span>98%成签率</span>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-site-help-wrap">
            <div class="g-wrap">
                <dl class="m-site-help">
                    <dt>关于我们</dt>
                    <dd>
                        <a href="{{$usitrip}}/about_us.html" rel="nofollow" target="_blank">走四方简介</a>
                    </dd>
                    <dd>
                        <a href="{{$usitrip}}/milestones.html" rel="nofollow" target="_blank">发展历程</a>
                    </dd>
                    <dd><a href="{{$usitrip}}/teams.html" target="_blank" rel="nofollow">专业团队</a>
                    </dd>
                    <dd>
                        <a href="{{$usitrip}}/our_businesses.html" target="_blank" rel="nofollow">我们的业务</a>
                    </dd>
                    <dd>
                        <a href="{{$usitrip}}/recruitment.html" target="_blank" rel="nofollow">加入我们</a>
                    </dd>
                    <dd>
                        <a href="{{$usitrip}}/contact_us.html" target="_blank" rel="nofollow">联系我们</a>
                    </dd>
                </dl>
                <dl class="m-site-help">
                    <dt>服务条款</dt>
                    <dd>
                        <a href="{{$usitrip}}/order_agreement.html" target="_blank" rel="nofollow">用户协议</a>
                    </dd>
                    <dd>
                        <a href="{{$usitrip}}/privacy_policy.html" target="_blank" rel="nofollow">隐私和版权声明</a>
                    </dd>
                    <!--<dd><a href="#" target="_blank">免责条款</a></dd>-->
                </dl>
                <dl class="m-site-help">
                    <dt>订购指南</dt>
                    <dd>
                        <a href="{{$usitrip}}/payment.html" target="_blank" rel="nofollow">支付方式</a>
                    </dd>
                    <dd>
                        <a href="{{$usitrip}}/order_process.html" target="_blank" rel="nofollow">订购流程</a>
                    </dd>
                    <dd>
                        <a href="{{$usitrip}}/change_plan.html" target="_blank" rel="nofollow">变更取消</a>
                    </dd>
                    <dd>
                        <a href="{{$usitrip}}/refund_instructions.html" target="_blank" rel="nofollow">退款说明</a>
                    </dd>
                </dl>
                <dl class="m-site-help">
                    <dt>帮助中心</dt>
                    <dd>
                        <a href="{{$usitrip}}/faq_question.html" target="_blank" rel="nofollow">常见问题</a>
                    </dd>
                    <dd><a href="{{$usitrip}}/points.html" target="_blank" rel="nofollow">会员积分</a>
                    </dd>
                    <dd>
                        <a href="{{$usitrip}}/tour_america_need.html" target="_blank" rel="nofollow">旅美须知</a>
                    </dd>
                    <dd>
                        <a href="{{$usitrip}}/travel_insurance.html" target="_blank" rel="nofollow">旅游保险</a>
                    </dd>
                    <dd><a href="{{$usitrip}}/sitemap.html" target="_blank">网站地图</a></dd>
                </dl>
                <dl class="m-site-help">
                    <dt>走四方合作</dt>
                    <dd>
                        <a href="/affiliate" target="_blank" rel="nofollow">网站联盟</a>
                    </dd>
                    <dd>
                        <a href="{{$usitrip}}/sponsored_universities.html" target="_blank" rel="nofollow">校园合作</a>
                    </dd>
                    <dd>
                        <a href="{{$usitrip}}/business_cooperation.html" target="_blank" rel="nofollow">商务合作</a>
                    </dd>
                    <dd><a href="{{$usitrip}}/links.html" target="_blank">友情链接</a></dd>
                </dl>
                <div class="m-two-code">
                    <p class="m-two-code-tit"><span>关注走四方<i></i></span></p>
                    <div class="m-two-code-img">
                        <a href="#" onclick="return false;" target="_blank"><img src="{{ asset('img/tours/weibo.png') }}" alt=""></a>
                        <a href="#" onclick="return false;" target="_blank"><img src="{{ asset('img/tours/weixin.png') }}" alt=""></a>
                    </div>
                </div>
            </div>

            <div class="m-auth-wrap g-wrap">
                <div class="m-auth-tab" style="margin-right:200px;">
                    <ul style="margin:50px auto 30px;">
                        <li class="js-foot-tab" index="0">企业认证</li>
                        <li class="tab-line"></li>
                        <li class="js-foot-tab active" index="1">合作伙伴</li>
                        <li class="tab-line"></li>
                        <li class="js-foot-tab" index="2">友情链接</li>
                        <li class="tab-line"></li>
                        <li class="js-foot-tab" index="3">热门目的地</li>
                    </ul>
                </div>
                <div class="m-auth-box" style="height: 51px;">
                    <div class="m-auth-box-ov" style="top: -51px;">
                        <div class="m-auth-box-i">
                            <ul class="m-auth-box-a">
                                <li>
                                    <a class="m-auth-ico01" href="//www.bbb.org/sanjose/business-reviews/travel-agencies-and-bureaus/unitedstars-international-ltd-in-monterey-park-ca-1024242#bbbseal" title="美国BBB认证最高商誉评级暨美国旅游网站" rel="nofollow" target="_blank"></a>
                                </li>
                                <li>
                                    <a class="m-auth-ico02" href="{{$usitrip}}/web_action/there_html/" rel="nofollow" target="_blank" title=" 我们拥有合法的美国加州旅行社执照，注册号 CST#: 2110393-40 可在美国加州检查院官方网站进行核实."></a>
                                </li>
                                <li>
                                    <a class="m-auth-ico03" href="{{$usitrip}}/web_action/there_html/" target="_blank"></a>
                                </li>
                                <li>
                                    <a class="m-auth-ico04" href="https://search.szfw.org/cert/l/CX20140704008388008811" rel="nofollow" target="_blank"></a>
                                </li>
                            </ul>
                        </div>
                        <div class="m-auth-box-i">
                            <div class="m-coope-tab-wrap" id="js-foot-slide">
                                <a href="#" class="m-coope-bnt-l" style="display: none;"><</a>
                                <div class="m-coope-tab-ov">
                                    <ul class="m-coope-tab-list clearfix" style="margin-left: 110.5px;">
                                        <li class="m-coop-ico01"><a target="_blank"></a></li>
                                        <li class="m-coop-ico02"><a target="_blank"></a></li>
                                        <li class="m-coop-ico03"><a target="_blank"></a></li>
                                        <li class="m-coop-ico04"><a target="_blank"></a></li>
                                        <li class="m-coop-ico05"><a target="_blank"></a></li>
                                        <li class="m-coop-ico06"><a target="_blank"></a></li>
                                        <li class="m-coop-ico07"><a target="_blank"></a></li>
                                    </ul>
                                </div>
                                <a href="#" onclick="return false;" class="m-coope-bnt-r" style="display: none;">></a>
                            </div>
                        </div>
                        <div class="m-auth-box-i">
                            <ul class="m-coope-link">

                                    <li><a href="{{$usitrip}}" target="_blank">美国旅游</a></li>
                                    <li><a href="http://www.ctsscs.com/" target="_blank">成都旅行社</a></li>
                                    <li><a href="http://cn.toursforfun.com" target="_blank">途风海外旅游网</a></li>
                                    <li><a href="http://www.iutour.cn/" target="_blank">北京青年旅行社</a></li>
                                    <li><a href="http://www.yjldp.com/" target="_blank">长岛渔家乐</a></li>
                                    <li><a href="http://www.joytrav.com/" target="_blank">美国旅游</a></li>
                                    <li><a href="http://www.bus365.com/" target="_blank">长途汽车票</a></li>
                                    <li><a href="http://www.citscq.com" target="_blank">重庆中国国际旅行社</a></li>
                                    <li><a href="http://www.myvacation.cn/" target="_blank">小众旅游</a></li>
                                    <li><a href="http://www.99ly.com.cn/" target="_blank">北京旅行社</a></li>
                                    <li><a href="http://www.fireflytrip.com/" target="_blank">重庆旅行社</a></li>
                                    <li><a href="http://www.023yts.com/" target="_blank">重庆旅行社</a></li>
                                    <li><a href="http://www.shanshuijiaozuo.cn/" target="_blank">云台山旅游攻略</a></li>
                                    <li><a href="http://www.aiketour.com/" target="_blank">成都旅游</a></li>
                                    <li><a href="http://www.cits.cn" target="_blank">国旅在线</a></li>
                                    <li><a href="https://www.huoche.net/lvyou/" target="_blank">火车旅游</a></li>
                                    <li><a href="https://www.ailvyou.cn/" target="_blank">爱侣游网</a></li>
                                    <li><a href="http://www.ptotour.com/" target="_blank">香港澳门五日游</a></li>
                                    <li><a href="https://www.tibetcn.com/" target="_blank">西藏旅游</a></li>
                                    <li><a href="http://www.byts.com.cn/" target="_blank">北京旅游</a></li>
                                    <li><a href="http://www.jjxxk.com/" target="_blank">九江旅游</a></li>
                                    <li><a href="https://www.cscyts.com.cn/" target="_blank">长沙旅行社</a></li>
                                    <li><a href="http://www.qianggen.net/" target="_blank">墙根网</a></li>
                                    <li><a href="http://www.xblyw.com/" target="_blank">西北旅游网</a></li>
                                    <li><a href="http://www.97616.net/" target="_blank">长沙旅行社</a></li>
                                    <li><a href="http://www.7jiaqi.com/" target="_blank">马尔代夫</a></li>
                                    <li><a href="http://www.0411hd.com" target="_blank">大连旅游</a></li>
                                    <li><a href="http://www.thyoo.com" target="_blank">广州康辉旅行社</a></li>
                                    <li><a href="https://www.xscits.com/" target="_blank">深圳国旅</a></li>
                                    <li><a href="http://guide.itrip.com/" target="_blank">澳大利亚旅游攻略</a></li>
                            </ul>
                        </div>
                        <div class="m-auth-box-i">
                            <ul class="m-coope-link">
                                <li><a href="{{$usitrip}}/america/" target="_blank">美国旅游</a></li>
                                <li><a href="{{$usitrip}}/canada/" target="_blank">加拿大旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-1/" target="_blank">洛杉矶旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-2/" target="_blank">旧金山旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-9/" target="_blank">黄石公园旅游</a></li>
                                <li><a href="{{$usitrip}}/east-coast/" target="_blank">美东旅游</a></li>
                                <li><a href="{{$usitrip}}/west-coast/" target="_blank">美西旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-66/" target="_blank">纽约旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-3/" target="_blank">拉斯维加斯</a></li>
                                <li><a href="{{$usitrip}}/america/vc-65/" target="_blank">华盛顿旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-477/" target="_blank">芝加哥旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-67/" target="_blank">费城旅游</a></li>
                                <li><a href="{{$usitrip}}/america/florida/" target="_blank">佛罗里达旅游</a></li>
                                <li><a href="{{$usitrip}}/alaska/" target="_blank">阿拉斯加旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-290/" target="_blank">迈阿密旅游</a></li>
                                <li><a href="{{$usitrip}}/hawaii/" target="_blank">夏威夷旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-71/" target="_blank">波士顿旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-295/" target="_blank">西雅图旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-162/" target="_blank">檀香山旅游</a></li>
                                <li><a href="{{$usitrip}}/vc-565/" target="_blank">圣地亚哥旅游</a></li>
                                <li><a href="{{$usitrip}}/canada/vc-270/" target="_blank">温哥华旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-204/" target="_blank">奥兰多旅游</a></li>
                                <li><a href="{{$usitrip}}/canada/vc-478/" target="_blank">多伦多旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-93/" target="_blank">尼亚加拉瀑布旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-588/" target="_blank">大峡谷旅游</a></li>
                                <li><a href="{{$usitrip}}/america/vc-34/" target="_blank">大提顿国家公园旅游</a></li>
                                <li><a href="{{$usitrip}}/canada/vc-365/" target="_blank">班芙国家公园旅游</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="m-copyright-wrap">
                    <p>
                        Copyright<i>©</i>2008-<span id="currentYear"></span> usitrip.com | 走四方旅游网 All rights reserved |
                        <a href="http://beian.miit.gov.cn" target="_blank">粤ICP备12040635号</a>|
                        <span class="license">美国营业执照<em><img src="{{$usitrip}}/tpl/www/image/DBAyyzz.jpg"></em></span>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <div id="js-contact-wrap-contact-wrap" class="contact-wrap js-contact-wrap">
        <ul class="contact-way js-contact-way">
            <li class="online-service-hover ">
                <div class="online-service-1">
                    <div class="online-service">
                        <span>在线客服</span>
                        <!--<b></b>-->
                    </div>
                </div>
            </li>
            <li class="online-service-hover-2">
                <div class="online-service-2">
                    <div class="online-service">
                        <span>在线客服2</span>
                        <!--<b></b>-->
                    </div>
                </div>
            </li>
            <li class="phone-hover">
                <div class="phone-content">
                    <div class="phone-service">
                        <div class="phone-detail hidephone" style="height: auto !important;">
                            <p><b>北美免费：</b><span>888-887-2816</span></p>
                            <p><b>美国：</b><span>1-626-898-7800</span></p>
                            <p><b>加拿大：</b><span>1-647-243-3769</span></p>
                            <p class="lineThin"></p>
                            <p><b>中国：</b><span>025-6668-0241</span></p>
                            <p><b>香港：</b><span>852-5806-0465</span></p>
                            <p class="lineThin"></p>
                            <p>定制包团</p>
                            <p><b>美国：</b><span>1-626-389-0706</span></p>
                            <p><b>中国：</b><span>025-6668-0241</span></p>

                        </div>
                    </div>
                    <span>电话</span>
                </div>
            </li>
{{--            <li class="qq-hover">--}}
{{--                <div class="qq-content">--}}
{{--                    <div class="qq-service">--}}
{{--                        <div class="qq-detail">--}}
{{--                            <ul>--}}
{{--                                <!-- <li>--}}
{{--                                  <a target='_blank' href='//webchat.b.qq.com/webchat.htm?sid=218808P8z8p8x8x8p8K8P' rel="nofollow">--}}
{{--                                    <img src="/image/qq-online.png" title="旅游咨询" alt="旅游咨询"/>--}}
{{--                                    <span>旅游咨询</span>--}}
{{--                                  </a>--}}
{{--                                </li>--}}
{{--                                 -->--}}
{{--                                <li>--}}
{{--                                    <a onclick="window.open('//wpa.qq.com/msgrd?v=3&uin=2853759769&site=签证咨询&menu=yes','_blank');" href="javascript:void(0);">--}}
{{--                                        <!-- <img src="/image/qq-online.png" title="签证咨询" alt="签证咨询"/> -->--}}
{{--                                        <span>签证咨询</span> </a>--}}
{{--                                </li>--}}

{{--                                <!--<li>//根据需求去掉该咨询项  2017.7.19--}}
{{--                                  <a onclick="window.open('//wpa.qq.com/msgrd?v=3&uin=2853759765&site=酒店咨询&menu=yes','_blank');" href="javascript:void(0);">--}}
{{--                                    <span>酒店咨询</span>--}}
{{--                                  </a>--}}
{{--                                </li>-->--}}

{{--                                <li>--}}
{{--                                    <a onclick="window.open('//wpa.qq.com/msgrd?v=3&uin=2853759761&site=定制包团&menu=yes','_blank');" href="javascript:void(0);">--}}
{{--                                        <!-- <img src="/image/qq-online.png" title="私人定制" alt="私人定制"/>  -->--}}
{{--                                        <span>定制包团(中)</span> </a>--}}
{{--                                </li>--}}

{{--                                <li>--}}
{{--                                    <a onclick="window.open('//wpa.qq.com/msgrd?v=3&uin=2355891217&site=定制包团&menu=yes','_blank');" href="javascript:void(0);">--}}
{{--                                        <!-- <img src="/image/qq-online.png" title="私人定制" alt="私人定制"/>  -->--}}
{{--                                        <span>定制包团(美)</span> </a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <span>QQ咨询</span>--}}
{{--                </div>--}}
{{--            </li>--}}
            <li class="qq-hover line" style="background:url({{$usitrip}}/tpl/www/image/contact-bg-tw.png) no-repeat">
                <div class="qq-content">
                    <div class="qq-service">
                        <div class="qq-detail">
                            <img src="{{$usitrip}}/tpl/www/image/line_qr_code.jpg" title="Line" alt="Line">
                        </div>
                    </div>
                    <span>Line</span>
                </div>
            </li>
            <li class="weixin-hover">
                <div class="weixin-content">
                    <div class="weixin-service">
                        <div class="weixin-detail">
                            <img src="{{$usitrip}}/tpl/www/image/weixin.png" title="微信" alt="微信"/>
                        </div>
                    </div>
                    <span>微信客服</span>
                </div>
            </li>
            <li class="qr-hover">
                <div class="qr-content">
                    <div class="qr-service">
                        <div class="qr-detail">
                            <p>扫码分享当前页</p>
                            <img onerror="this.src='/img/home/qrcode.png'" width="200" height="200" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{$currentUrl}}&choe=UTF-8" title="扫码分享当前页" alt="扫码分享当前页"/>
                        </div>
                    </div>
                    <span>扫码分享</span>
                </div>
            </li>
            <li class="return-top-hover">
                <a href="javascript:" onclick="window.scrollTo(0,0);return false;">
                    <div class="return-top"></div>
                    <span>返回顶部</span></a>
            </li>
        </ul>

        <!-- 购物车开始 -->
        <a class="shopping" href="//post.usitrip.com/index.php?mod=shopping_cart&language_code=gb2312" style="display:block;color:#666666;"> <span class="addOne">+1</span> <span id="JS_shoppingNum" class="shoppingNum">0</span>
            <p class="shoppingTitle">购物车</p>
        </a>
    </div>
@elseif($language == 1)
        {{--en footer--}}
    <div class="en_m_footer">
        <div class="Cons">
            <div class="en_m_help">
                <div class="en_list_group txt_hover">
                    <ul>
                        <li><i class="usitour_icon"></i></li>
                        <li>17870 Castleton St #388,
                            City of Industry, CA 91748</li>
                        <li>
                            <dl>
                                <dd class="fl"><a href="//www.facebook.com/usitour" target="_blank"><span class="follow_us_icons facebook_icon" alt=""></span></a></dd>
                                <dd class="fl"><a href="//twitter.com/OfficialUsitour" target="_blank"><span class="follow_us_icons twitter_icon" alt=""></span></a></dd>
                                <dd class="fl"><a href="//www.instagram.com/usitour_official/" target="_blank"><span class="follow_us_icons instagram_icon"alt=""></span></a></dd>
                                <dd class="fl"><a href="//www.pinterest.com/UsiTour/" target="_blank"><span class="follow_us_icons pinterest_icon" alt=""></span></a></dd>
                            </dl>
                        </li>
                    </ul>
                </div>
                <div class="en_list_group txt_hover">
                    <ul>
                        <li>Your Usitour</li>
                        <!--    暂时隐藏我的账户页面  -->
                        <li><a href="https://www.usitour.com/about_us.html" target="_blank">About Us</a></li>
                        <li><a href="https://www.usitour.com/milestones.html" target="_blank">Milestones</a></li>
                        <li><a href="https://www.usitour.com/privacy_policy.html" target="_blank">Privacy Policy</a></li>
                        <li><a href="https://www.usitour.com/order_agreement.html" target="_blank">Order Agreement</a></li>
                        <li><a href="https://www.usitour.com/faq_question.html" target="_blank">Help/FAQs</a></li>
                        <li><a href="https://www.usitour.com/contact_us.html" target="_blank">Contact Us</a></li>
                        <li><a href="/affiliate" target="_blank">Affiliate Login</a></li>
                    </ul>
                </div>
                <div class="en_list_group txt_hover">
                    <ul>
                        <li>Top Destinations</li>
                        <!--    暂时隐藏我的账户页面  -->
                        <li><a href="https://www.usitour.com/america/vc-93/" target="_blank">Niagara Falls</a></li>
                        <li><a href="https://www.usitour.com/america/vc-66/" target="_blank">New York</a></li>
                        <li><a href="https://www.usitour.com/america/vc-9/" target="_blank">Yellowstone National Park</a></li>
                        <li><a href="https://www.usitour.com/west-coast/vc-3/" target="_blank">Las Vegas</a></li>
                        <li><a href="https://www.usitour.com/west-coast/vc-1/" target="_blank">Los Angeles</a></li>
                        <li><a href="https://www.usitour.com/west-coast/vc-2/" target="_blank">San Francisco</a></li>
                        <li><a href="https://www.usitour.com/canada/vc-478/" target="_blank">Toronto</a></li>
                        <li><a href="https://www.usitour.com/alaska/vc-934/" target="_blank">Fairbanks</a></li>
                    </ul>
                </div>
                <div class="en_list_group txt_hover">
                    <ul>
                        <li>Top Tours &amp; Activities</li>
                        <!--    暂时隐藏我的账户页面  -->
                        <li><a href="https://www.usitour.com/west-coast/" target="_blank">US West Coast Tours</a></li>
                        <li><a href="https://www.usitour.com/east-coast/" target="_blank">US East Coast Tours</a></li>
                        <li><a href="https://www.usitour.com/canada/" target="_blank">Canada Tours</a></li>
                        <li><a href="https://www.usitour.com/hawaii/" target="_blank">Hawaii Tours</a></li>
                        <li><a href="https://www.usitour.com/florida/" target="_blank">Florida Tours</a></li>
                        <li><a href="https://www.usitour.com/alaska/" target="_blank">Alaska Tours</a></li>
                        <li><a href="https://www.usitour.com/Mexico/" target="_blank">Mexico Tours</a></li>
                        <li><a href="https://www.usitour.com/Europe/" target="_blank">Europe Tours</a></li>
                    </ul>
                </div>
                <div class="en_list_group other_sites">
                    <ul>
                        <li>More Sites</li>
                        <li><a href="http:{{$usitrip}}/" target="_blank">简体中文</a></li>
                        <li><a href="http:{{$usitriptw}}/" target="_blank">繁体中文</a></li>
                    </ul>
                </div>
                <div class="en_list_group connect_us_ico">
                    <ul>
                        <li>Payment Methods</li>
                        <li>
                            <i class="payment_icons"></i>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="partners">
            <i class="partner_icon"></i>
        </div>
        <div class="en_m-copyright-wrap">
            <p>
                Copyright<i> &copy; </i>2008-<span id="currentYear"></span> || All Rights Reserved. ||
                <a href="http://beian.miit.gov.cn" target="_blank">粤ICP备12040635号</a>
            </p>
        </div>
    </div>
    <div id="js-contact-wrap-contact-wrap" class="contact-wrap js-contact-wrap" style="display:block">
        <ul class="contact-way js-contact-way">
            <li class="online-service-hover">
                <div class="online-service-2">
                    <div class="online-service">
                        <span>Start Chat</span>
                    </div>
                </div>
            </li>
            <li class="phone-hover">
                <div class="phone-content">
                    <div class="phone-service">
                        <div class="phone-detail ">
                            <p><b>Contact Us: </b><span> 1-626-898-7658 (24/7)</span></p>
                        </div>
                    </div>
                    <span>Tel</span>
                </div>
            </li>
            <li class="return-top-hover">
                <a href="javascript:" onclick="window.scrollTo(0,0);return false;">
                    <div class="return-top"></div>
                    <span>top</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="share-btn-group">
        <ul class="social_icon_ul">
            <li class="cp_social_facebook">
                <a href="http://www.facebook.com/sharer.php?u={{$currentUrl}}" class="cp_social_share " onclick="window.open(this.href,'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false">
                    <i class="social_icon social_icon_facebook"></i>
                </a>
            </li>
            {{--<li class="cp_social_google">--}}
            {{--<a href="https://plus.google.com/share?url={{$currentUrl}}" class="cp_social_share " onclick="window.open(this.href,'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false">--}}
            {{--<i class="social_icon social_icon_google"></i>--}}
            {{--</a>--}}
            {{--</li>--}}
            <li class="cp_social_twitter">
                <a href="https://twitter.com/share?url={{$currentUrl}}" class="cp_social_share " onclick="window.open(this.href,'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false">
                    <i class="social_icon social_icon_twitter"></i>
                </a>
            </li>
            <li class="cp_social_blogger"><a href="https://www.blogger.com/blog_this.pyra?t&amp;u={{$currentUrl}}" class="cp_social_share " onclick="window.open(this.href,'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false">
                    <i class="social_icon social_icon_blogger"></i>
                </a>
            </li>
            <li class="cp_social_pinterest"><a href="https://pinterest.com/pin/create/link/?url={{$currentUrl}}" class="cp_social_share " onclick="window.open(this.href,'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false">
                    <i class="social_icon social_icon_pinterest"></i>
                </a>
            </li>
        </ul>
    </div>
@else
    {{--tw footer--}}
    <footer class="m-footer">
        <div class="m-spec-wrap">
            <div class="g-wrap" style="overflow:hidden;">
                <ul class="m-spec-box clearfix">
                    <li>
                        <i class="m-spec-ico00"></i>
                        <p>
                            <strong>美國當地華人旅行社</strong> <span>出遊有保障</span>
                        </p>
                    </li>
                    <li>
                        <i class="m-spec-ico01"></i>
                        <p>
                            <strong>美國BBB認證</strong> <span>最高商譽評級A+</span>
                        </p>
                    </li>
                    <li>
                        <i class="m-spec-ico02"></i>
                        <p>
                            <strong>價格公開透明</strong> <span>低價保證</span>
                        </p>
                    </li>
                    <li>
                        <i class="m-spec-ico03"></i>
                        <p>
                            <strong>品質線路 華人首選</strong> <span>10年服務超數百萬客人</span>
                        </p>
                    </li>
                    <li>
                        <i class="m-spec-ico04"></i>
                        <p>
                            <strong>國際SSL認證</strong> <span>支付無憂</span>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-site-help-wrap">
            <div class="g-wrap">
                <dl class="m-site-help">
                    <dt>關於我們</dt>
                    <dd>
                        <a href="{{$usitriptw}}/about_us.html" rel="nofollow" target="_blank">走四方簡介</a>
                    </dd>
                    <dd>
                        <a href="{{$usitriptw}}/milestones.html" rel="nofollow" target="_blank">發展歷程</a>
                    </dd>
                    <dd><a href="{{$usitriptw}}/teams.html" target="_blank" rel="nofollow">專業團隊</a>
                    </dd>
                    <dd>
                        <a href="{{$usitriptw}}/our_businesses.html" target="_blank" rel="nofollow">我們的業務</a>
                    </dd>
                    <dd>
                        <a href="{{$usitriptw}}/recruitment.html" target="_blank" rel="nofollow">加入我們</a>
                    </dd>
                    <dd>
                        <a href="{{$usitriptw}}/contact_us.html" target="_blank" rel="nofollow">聯系我們</a>
                    </dd>
                </dl>
                <dl class="m-site-help">
                    <dt>服務條款</dt>
                    <dd>
                        <a href="{{$usitriptw}}/order_agreement.html" target="_blank" rel="nofollow">旅遊契約書</a>
                    </dd>
                    <dd>
                        <a href="{{$usitriptw}}/privacy_policy.html" target="_blank" rel="nofollow">隱私和版權聲明</a>
                    </dd>
                    <!--<dd><a href="#" target="_blank">免責條款</a></dd>-->
                </dl>
                <dl class="m-site-help">
                    <dt>訂購指南</dt>
                    <dd>
                        <a href="{{$usitriptw}}/payment.html" target="_blank" rel="nofollow">支付方式</a>
                    </dd>
                    <dd>
                        <a href="{{$usitriptw}}/order_process.html" target="_blank" rel="nofollow">訂購流程</a>
                    </dd>
                    <dd>
                        <a href="{{$usitriptw}}/change_plan.html" target="_blank" rel="nofollow">變更取消</a>
                    </dd>
                    <dd>
                        <a href="{{$usitriptw}}/refund_instructions.html" target="_blank" rel="nofollow">退款說明</a>
                    </dd>
                </dl>
                <dl class="m-site-help">
                    <dt>幫助中心</dt>
                    <dd>
                        <a href="{{$usitriptw}}/faq_question.html" target="_blank" rel="nofollow">常見問題</a>
                    </dd>
                    <dd><a href="{{$usitriptw}}/points.html" target="_blank" rel="nofollow">會員積分</a>
                    </dd>
                    <dd>
                        <a href="{{$usitriptw}}/tour_america_need.html" target="_blank" rel="nofollow">旅美須知</a>
                    </dd>
                    <dd>
                        <a href="{{$usitriptw}}/travel_insurance.html" target="_blank" rel="nofollow">旅遊保險</a>
                    </dd>
                    <dd><a href="{{$usitriptw}}/sitemap.html" target="_blank">網站地圖</a></dd>
                </dl>
                <dl class="m-site-help">
                    <dt>走四方合作</dt>
                    <dd>
                        <a href="/affiliate?lang=tw" target="_blank" rel="nofollow">網站聯盟</a>
                    </dd>
                    <dd>
                        <a href="{{$usitriptw}}/sponsored_universities.html" target="_blank" rel="nofollow">校園合作</a>
                    </dd>
                    <dd>
                        <a href="{{$usitriptw}}/business_cooperation.html" target="_blank" rel="nofollow">商務合作</a>
                    </dd>
                    <dd><a href="{{$usitriptw}}/links.html" target="_blank">友情鏈接</a></dd>
                </dl>
                <div class="m-two-code">
                    <p class="m-two-code-tit"><span>關註走四方<i></i></span></p>
                    <div class="m-two-code-img">
                        <a target="_blank" href="https://www.instagram.com/usitour_official/"><img src="{{$usitrip}}/image/Instagram.png" alt="Instagram"><span>Instagram</span></a>
                        <a target="_blank" href="https://www.facebook.com/usitripofficial/"><img src="{{$usitrip}}/image/Facebook.png" alt="Facebook"><span>Facebook</span></a>
                        <a target="_blank" href="http://nav.cx/iakRO6J"><img src="{{$usitrip}}/image/line.png" alt="Line"><span>Line</span></a>
                    </div>
                </div>
            </div>

            <div class="m-auth-wrap g-wrap">
                <div class="m-auth-tab" style="margin-right:200px;">
                    <ul style="margin:50px auto 30px;">
                        <li class="js-foot-tab" index="0">企業認證</li>
                        <li class="tab-line"></li>
                        <li class="js-foot-tab active" index="1">合作夥伴</li>
                        <li class="tab-line"></li>
                        <li class="js-foot-tab" index="2">友情鏈接</li>
                        <li class="tab-line"></li>
                        <li class="js-foot-tab" index="3">熱門目的地</li>
                    </ul>
                </div>
                <div class="m-auth-box" style="height: 51px;">
                    <div class="m-auth-box-ov" style="top: -51px;">
                        <div class="m-auth-box-i">
                            <ul class="m-auth-box-a">
                                <li>
                                    <a class="m-auth-ico01" href="//www.bbb.org/sanjose/business-reviews/travel-agencies-and-bureaus/unitedstars-international-ltd-in-monterey-park-ca-1024242#bbbseal" title="美國BBB認證最高商譽評級暨美國旅遊網站" rel="nofollow" target="_blank"></a>
                                </li>
                                <li>
                                    <a class="m-auth-ico02" href="{{$usitriptw}}/web_action/there_html/" rel="nofollow" target="_blank" title=" 我們擁有合法的美國加州旅行社執照，註冊號 CST#: 2110393-40 可在美國加州檢查院官方網站進行核實."></a>
                                </li>
                                <li>
                                    <a class="m-auth-ico03" href="{{$usitriptw}}/web_action/there_html/" target="_blank"></a>
                                </li>
                                <li>
                                    <a class="m-auth-ico04" href="https://search.szfw.org/cert/l/CX20140704008388008811" rel="nofollow" target="_blank"></a>
                                </li>
                            </ul>
                        </div>
                        <div class="m-auth-box-i">
                            <div class="m-coope-tab-wrap" id="js-foot-slide">
                                <a href="#" class="m-coope-bnt-l" style="display: none;"></a>
                                <div class="m-coope-tab-ov">
                                    <ul class="m-coope-tab-list clearfix" style="margin-left: 110.5px;">
                                        <li class="m-coop-ico01"><a target="_blank"></a></li>
                                        <li class="m-coop-ico02"><a target="_blank"></a></li>
                                        <li class="m-coop-ico03"><a target="_blank"></a></li>
                                        <li class="m-coop-ico04"><a target="_blank"></a></li>
                                        <li class="m-coop-ico05"><a target="_blank"></a></li>
                                        <li class="m-coop-ico06"><a target="_blank"></a></li>
                                        <li class="m-coop-ico07"><a target="_blank"></a></li>
                                    </ul>
                                </div>
                                <a href="#" onclick="return false;" class="m-coope-bnt-r" style="display: none;">></a>
                            </div>
                        </div>
                        <div class="m-auth-box-i">
                            <ul class="m-coope-link">
                                {{--<li><a href="//www.95zhushu.com/" target="_blank">民宿預訂</a>--}}
                            </ul>
                        </div>
                        <div class="m-auth-box-i">
                            <ul class="m-coope-link">
                                <ul class="m-coope-link">
                                    <li><a href="{{$usitriptw}}/america/" target="_blank">美國旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/canada/" target="_blank">加拿大旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-1/" target="_blank">洛杉磯旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-2/" target="_blank">舊金山旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-9/" target="_blank">黃石公園旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/east-coast/" target="_blank">美東旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/west-coast/" target="_blank">美西旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-66/" target="_blank">紐約旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-3/" target="_blank">拉斯維加斯</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-65/" target="_blank">華盛頓旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-477/" target="_blank">芝加哥旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-67/" target="_blank">費城旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/florida/" target="_blank">佛羅里達旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/alaska/" target="_blank">阿拉斯加旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-290/" target="_blank">邁阿密旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/hawaii/" target="_blank">夏威夷旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-71/" target="_blank">波士頓旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-295/" target="_blank">西雅圖旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-162/" target="_blank">檀香山旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/vc-565/" target="_blank">聖地亞哥旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/canada/vc-270/" target="_blank">溫哥華旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-204/" target="_blank">奧蘭多旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/canada/vc-478/" target="_blank">多倫多旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-93/" target="_blank">尼加拉瀑布旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-588/" target="_blank">大峽谷旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/america/vc-34/" target="_blank">大提頓國家公園旅遊</a></li>
                                    <li><a href="{{$usitriptw}}/canada/vc-365/" target="_blank">班芙國家公園旅遊</a></li>
                                </ul>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="m-copyright-wrap">
                    <p>
                        Copyright<i>©</i>2008-<span id="currentYear"></span>  @if(!$is_usitour) usitrip.com @else usitour.com @endif| 走四方旅遊網 All rights reserved |
                        <a href="http://beian.miit.gov.cn" target="_blank"> 粤ICP备12040635号 |
                        </a><span class="license"> 美國營業執照<em><img src="{{$usitrip}}/tpl/www/image/DBAyyzz.jpg"></em></span>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <div id="js-contact-wrap-contact-wrap" class="contact-wrap js-contact-wrap">
        <ul class="contact-way js-contact-way">
            <li class="online-service-hover ">
                <div class="online-service-1">
                    <div class="online-service">
                        <span>線上客服</span>
                        <!--<b></b>-->
                    </div>
                </div>
            </li>
            <li class="online-service-hover-2">
                <div class="online-service-2">
                    <div class="online-service">
                        <span>線上客服2</span>
                        <!--<b></b>-->
                    </div>
                </div>
            </li>
            <li class="phone-hover">
                <div class="phone-content">
                    <div class="phone-service">
                        <div class="phone-detail hidephone" style="height: auto !important;">
                            <p><b>北美免費：</b><span>888-887-2816</span></p>
                            <p><b>美國：</b><span>1-626-898-7800</span></p>
                            <p><b>加拿大：</b><span>1-647-243-3769</span></p>
                            <p class="lineThin"></p>
                            <p><b>香港：</b><span>852-5806-0465</span></p>
                            <p><b>台灣：</b><span>886-2-5594-1486</span></p>
                            <p class="lineThin"></p>
                            <p>客製包團</p>
                            <p><b>美國：</b><span>1-626-389-0706</span></p>
                        </div>
                    </div>
                    <span>電話</span>
                </div>
            </li>
            <li class="qq-hover line" style="background:url({{$usitrip}}/tpl/www/image/contact-bg-tw.png) no-repeat">
                <div class="qq-content">
                    <div class="qq-service">
                        <div class="qq-detail">
                            <img src="{{$usitrip}}/tpl/www/image/line_qr_code.jpg" title="Line" alt="Line">
                        </div>
                    </div>
                    <span>Line</span>
                </div>
            </li>
            <li class="weixin-hover">
                <div class="weixin-content">
                    <div class="weixin-service">
                        <div class="weixin-detail">
                            <img src="{{$usitrip}}/tpl/www/image/weixin.png" title="微信" alt="微信"/>
                        </div>
                    </div>
                    <span>微信客服</span>
                </div>
            </li>
            <li class="qr-hover">
                <div class="qr-content">
                    <div class="qr-service">
                        <div class="qr-detail">
                            <p>掃碼分享當前頁</p>
                            <img onerror="this.src='/img/home/qrcode.png'" width="200" height="200" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{$currentUrl}}&choe=UTF-8" title="掃碼分享當前頁" alt="掃碼分享當前頁"/>
                        </div>
                    </div>
                    <span>掃碼分享</span>
                </div>
            </li>
            <li class="return-top-hover">
                <a href="javascript:" onclick="window.scrollTo(0,0);return false;">
                    <div class="return-top"></div>
                    <span>返回頂部</span></a>
            </li>
        </ul>
        <!-- 購物車開始 -->
        <a class="shopping" href="https://post.usitrip.com/index.php?mod=shopping_cart&language_code=big5" style="display:block;color:#666666;"> <span class="addOne">+1</span> <span id="JS_shoppingNum" class="shoppingNum">0</span>
            <p class="shoppingTitle">購物車</p>
        </a>
    </div>
@endif
    @endif
