<!DOCTYPE html>
<html lang="{{ $language === 0 ? 'zh-Hans' : $language === 2 ? 'zh-Hant': 'en' }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <title>酒店更新页面: {{ $hotel->name or '' }}</title>
    <link rel="stylesheet" href="{{ url('/assets/css/pc/all.hotel-update.css') }}">
    <link href="@if($is_b) {{asset('117book.ico')}} @elseif($is_usitour&& $language !== 2) {{asset('faviconen.ico')}} @else {{asset('favicon.ico')}} @endif" rel="shortcut icon">
</head>

<body>
    <div id="hotelUpdateApp" class="hotel-update-app-wrapper">
        @if ($hotel)
            <div class="header" id="infoController">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <span class="text-danger" v-if="globalSave">点击保存后当前页面的所有更新都将显示在前台</span>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="{{ url('/hotel') . '/' . $hotel->hotelId . '.html' }}" target="_blank" class="btn btn-default">前台查看</a>
                            <button type="button" class="btn btn-primary" @click="doSaveForm" v-if="globalSave">保存当前页面</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="scroll-top">
                <button type="button" class="btn btn-info" @click="scrollTop">
                    <i></i>
                    <span>Top</span>
                </button>
            </div>
            <div class="container">
                <h2>
                    酒店名称: {{ $hotel->name }}
                    @if ($hotel->name_zh)
                    ({{ $hotel->name_zh }})
                    @endif
                </h2>
                <nav class="hotel-info-toggler">
                    <ul class="nav nav-tabs">
                        <li role="presentation" id="hotelDetail"><a href="#" @click="openTab('hotelDetail', true, $event)">主要信息</a></li>
                        {{--由于api问题暂时关闭周边信息的更新功能--}}
                        <li role="presentation" id="hotelArea"><a href="#hotelArea" @click="openTab('hotelArea', false, $event)">周边信息</a></li>
                        <li role="presentation" id="hotelFacility"><a href="#hotelFacility" @click="openTab('hotelFacility', true, $event)">服务与设施</a></li>
                        <li role="presentation" id="hotelReview"><a href="#hotelReview" @click="openTab('hotelReview', false, $event)">酒店评价</a></li>
                        <li role="presentation" id="hotelRestaurant"><a href="#hotelRestaurant" @click="openTab('hotelRestaurant', true, $event)">周边餐馆</a></li>
                        <li role="presentation" id="hotelThumbnail"><a href="#hotelThumbnail" @click="openTab('hotelThumbnail', true, $event)">酒店首图</a></li>
                        <li role="presentation" id="hotelImage"><a href="#hotelImage" @click="openTab('hotelImage', true, $event)">酒店图片</a></li>
                        <li role="presentation" id="hotelCard"><a href="#hotelCard" @click="openTab('hotelCard', true, $event)">付款方式</a></li>
                    </ul>
                </nav>
                <div class="hotel-info">
                    <form action="#" class="hotel-info-detail form-horizontal" data-attr="detail" id="hotelDetailForm">
                        <div class="form-group">
                            <label for="hotelName" class="col-sm-2 control-label">酒店名称（英文）</label>
                            <div class="col-sm-10">
                                <input type="text" class="text-input" id="hotelName" name="name" v-model="hotel.name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="hotelName" class="col-sm-2 control-label">酒店名称（中文）</label>
                            <div class="col-sm-10">
                                <input type="text" class="text-input" id="hotelName" name="name_zh" v-model="hotel.name_zh">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="hotelDesc" class="col-sm-2 control-label">酒店描述</label>
                            <div class="col-sm-10">
                                <textarea name="description" id="hotelDesc" class="hotel-desc" cols="60" rows="10" v-model="hotel.description"></textarea>
                                <textarea name="description_zh" id="hotelDescZh" class="hotel-desc" cols="60" rows="10" v-model="hotel.description_zh"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="hotelName" class="col-sm-2 control-label">酒店星级</label>
                            <div class="col-sm-10">
                                <input type="radio" id="rate1" name="rating" :value="1" v-model="hotel.rating">
                                <label for="rate1">1</label>
                                <input type="radio" id="rate2" name="rating" :value="2" v-model="hotel.rating">
                                <label for="rate2">2</label>
                                <input type="radio" id="rate3" name="rating" :value="3" v-model="hotel.rating">
                                <label for="rate3">3</label>
                                <input type="radio" id="rate4" name="rating" :value="4" v-model="hotel.rating">
                                <label for="rate4">4</label>
                                <input type="radio" id="rate5" name="rating" :value="5" v-model="hotel.rating">
                                <label for="rate5">5</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="hotelName" class="col-sm-2 control-label">酒店所在城市</label>
                            <div class="col-sm-10">
                                <input type="text" class="text-input" id="hotelName" name="city" v-model="hotel.city">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="hotelName" class="col-sm-2 control-label">酒店地址</label>
                            <div class="col-sm-10">
                                <input type="text" class="text-input" id="hotelName" name="address" v-model="hotel.address">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="hotelName" class="col-sm-2 control-label">zip code</label>
                            <div class="col-sm-10">
                                <input type="text" class="text-input" id="hotelZip" name="zipcode" v-model="hotel.zipcode">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="hotelName" class="col-sm-2 control-label">共享房型</label>
                            <div class="col-sm-10">
                                <input type="text" class="text-input" id="sharedAllotment" name="shared_allotment" v-model="hotel.shared_allotment">
                            </div>
                        </div>
                    </form>
                    <form action="#" class="hotel-info-area form-horizontal" data-attr="area" id="hotelAreaForm">
                        <div class="row" v-if="hotel.hotel_area">
                            <h2 class="hotel-block-title text-success">英文酒店周边信息</h2>
                            <button type="button" class="add-new btn btn-success" @click="addAreaGroup('en', $event)">添加分组</button>
                            <div :class="['area-group', 'collapse-parent', 'col-sm-6', area_group.pois.length > 7 ? 'collapsed' : '']" v-for="(area_group, index) in hotel.hotel_area">
                                <div class="group-actions">
                                    <button type="button" class="btn btn-sm btn-default btn-toggler" @click="toggleCollapse" v-if="area_group.pois.length > 7">Show more</button>
                                    <button type="button" class="btn btn-sm btn-info btn-add" @click="addAreaItem('en', index, $event)">添加行</button>
                                    <button type="button" class="btn btn-sm btn-danger btn-remove" @click="removeAreaGroup('en', index)">删除组</button>
                                </div>
                                <input class="area-group-title text-input" :name="'areaGroup'+ index" placeholder="请输入英文组名" v-model="area_group.sortName" required/>
                                <div class="area-item row" v-for="(item, line) in area_group.pois">
                                    <div class="col-sm-9">
                                        <input type="text" class="area-item-name text-input" :name="'areaItemName'+index+'-'+line" placeholder="请输入周边英文名称" v-model="item.name" required/>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="area-item-distance text-input" :name="'areaItemDistance'+index+'-'+line" v-model="item.distance"/>
                                        <button type="button" class="btn btn-danger btn-remove-sm" @click="removeAreaItem('en', index, line)">删除</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" v-if="hotel.hotel_area_zh">
                            <h2 class="hotel-block-title text-info">中文酒店周边信息</h2>
                            <button type="button" class="add-new btn btn-success" @click="addAreaGroup('zh', $event)">添加分组</button>
                            <div :class="['area-group', 'collapse-parent', 'col-sm-6', area_group.pois.length > 7 ? 'collapsed' : '']" v-for="(area_group, index) in hotel.hotel_area_zh">
                                <div class="group-actions">
                                    <button type="button" class="btn btn-sm btn-default btn-toggler" @click="toggleCollapse" v-if="area_group.pois.length > 7">Show more</button>
                                    <button type="button" class="btn btn-sm btn-info" @click="addAreaItem('zh', index, $event)">添加行</button>
                                    <button type="button" class="btn btn-sm btn-danger btn-remove" @click="removeAreaGroup('zh', index)">删除组</button>
                                </div>
                                <input class="area-group-title text-input" :name="'areaGroupzh'+index" placeholder="请输入中文组名" v-model="area_group.sortName" required/>
                                <div class="area-item row" v-for="(item, line) in area_group.pois">
                                    <div class="col-sm-9">
                                        <input type="text" class="area-item-name text-input" :name="'areaItemNamezh'+index+'-'+line" placeholder="请输入中文周边名称" v-model="item.name" required/>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="area-item-distance text-input" :name="'areaItemDistancezh'+index+'-'+line" v-model="item.distance"/>
                                        <button type="button" class="btn btn-danger btn-remove-sm" @click="removeAreaItem('zh', index, line)">删除</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div v-if="!hotel.hotel_area && !hotel.hotel_area_zh">
                            <h4>没有周边信息</h4>
                            <button class="btn btn-info" type="button" @click="addAttr('hotel_area', 'en')">添加英文信息</button>
                            <button class="btn btn-info" type="button" @click="addAttr('hotel_area', 'zh')">添加中文信息</button>
                        </div>
                    </form>
                    <form action="#" class="hotel-info-facility form-horizontal" data-attr="facility" id="hotelFacilityForm">
                        <div v-if="hotel.hotel_facility">
                            <div class="hotel-facility-block">
                                <h2 class="hotel-block-title">热门酒店服务与设施（英文）</h2>
                                <button type="button" class="add-new btn btn-success" @click="addFacilityPopular('en')">添加</button>
                                <div class="hotel-facility-popular row" v-if="hotel.hotel_facility_zh.popular">
                                    <div class="hotel-facility-item col-sm-3" v-for="(facility, index) in hotel.hotel_facility.popular">
                                        <div class="card">
                                            <p class="group-actions"><button type="button" class="btn btn-danger btn-remove-sm" @click="removeFacilityPopular('en', index)">删除</button></p>
                                            <p><b>设施名称：</b><input type="text" class="text-input" :name="'facilityName'+index" placeholder="请输入英文热门设施名称" v-model="facility.facilityName" required/></p>
                                            <p><b>设施代码：</b><input type="text" class="text-input" :name="'facilityCode'+index" placeholder="请输入英文热门设施代码" v-model="facility.facilityCode" required/></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hotel-facility-block">
                                <h2 class="hotel-block-title">全部酒店服务与设施（英文）</h2>
                                <button type="button" class="add-new btn btn-success" @click="addFacilityGroup('en', $event)">添加分组</button>
                                <div class="hotel-facility-all row" v-if="hotel.hotel_facility.all">
                                    <div v-for="(group, index) in hotel.hotel_facility.all" :class="['hotel-facility-group', 'collapse-parent', 'col-sm-4', group.facilities.length > 7 ? 'collapsed' : '']">
                                        <div class="card">
                                            <div class="group-actions">
                                                <button type="button" class="btn btn-sm btn-default btn-toggler" @click="toggleCollapse" v-if="group.facilities.length > 7">Show more</button>
                                                <button type="button" class="btn btn-sm btn-info" @click="addFacilityItem('en', index, $event)">添加子类</button>
                                                <button type="button" class="btn btn-sm btn-danger btn-remove" @click="removeFacilityGroup('en', index)">删除组</button>
                                            </div>
                                            <div>
                                                <b>类别名称：</b>
                                                <input type="text" class="hotel-facility-group-title text-input" :name="'facilityGroup'+index" placeholder="请输入英文类别名称" v-model="group.sortName" required/>
                                            </div>
                                            <div>
                                                <b>类别代码：</b>
                                                <input type="text" class="text-input" :name="'facilityGroupCode'+index" placeholder="请输入英文类别代码" v-model="group.code" required/>
                                            </div>
                                            <div v-for="(facility, line) in group.facilities" :title="facility" class="hotel-facility-item">
                                                <input type="text" class="text-input" placeholder="请输入英文设施名称" :name="'facilityItem'+index+'-'+line" v-model="group.facilities[line]" required/>
                                                <button type="button" class="fr btn btn-danger btn-remove-sm" @click="removeFacilityItem('en', index, line)">删除</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div v-if="hotel.hotel_facility_zh">
                            <div class="hotel-facility-block">
                                <h2 class="hotel-block-title">热门酒店服务与设施（中文）</h2>
                                <button type="button" class="add-new btn btn-success" @click="addFacilityPopular('zh')">添加</button>
                                <div class="hotel-facility-popular row" v-if="hotel.hotel_facility_zh.popular">
                                    <div class="hotel-facility-item col-sm-3" v-for="(facility, index) in hotel.hotel_facility_zh.popular">
                                        <div class="card">
                                            <p class="group-actions"><button type="button" class="btn btn-danger btn-remove-sm" @click="removeFacilityPopular('zh', index)">删除</button></p>
                                            <p><b>设施名称：</b><input type="text" class="text-input" :name="'facilityNamezh'+index" placeholder="请输入中文热门设施名称" v-model="facility.facilityName" required/></p>
                                            <p><b>设施代码：</b><input type="text" class="text-input" :name="'facilityCodezh'+index" placeholder="请输入中文热门设施代码" v-model="facility.facilityCode" required/></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hotel-facility-block">
                                <h2 class="hotel-block-title">全部酒店服务与设施（中文）</h2>
                                <button type="button" class="add-new btn btn-success" @click="addFacilityGroup('zh', $event)">添加分组</button>
                                <div class="hotel-facility-all row" v-if="hotel.hotel_facility_zh.all">
                                    <div v-for="(group, index) in hotel.hotel_facility_zh.all" :class="['hotel-facility-group', 'collapse-parent', 'col-sm-4', group.facilities.length > 7 ? 'collapsed' : '']">
                                        <div class="card">
                                            <div class="group-actions">
                                                <button type="button" class="btn btn-sm btn-default btn-toggler" @click="toggleCollapse" v-if="group.facilities.length > 7">Show more</button>
                                                <button type="button" class="btn btn-sm btn-info" @click="addFacilityItem('zh', index, $event)">添加子类</button>
                                                <button type="button" class="btn btn-sm btn-danger btn-remove" @click="removeFacilityGroup('zh', index)">删除组</button>
                                            </div>
                                            <div>
                                                <b>类别名称：</b>
                                                <input class="hotel-facility-group-title text-input" :name="'facilityGroupzh'+index" placeholder="请输入中文类别名称" v-model="group.sortName" required/>
                                            </div>
                                            <div>
                                                <b>类别代码：</b>
                                                <input class="text-input" :name="'facilityGroupCodezh'+index" placeholder="请输入中文类别代码" v-model="group.code" required/>
                                            </div>
                                            <div v-for="(facility, line) in group.facilities" :title="facility" class="hotel-facility-item">
                                                <input type="text" class="text-input" placeholder="请输入中文设施名称" :name="'facilityItemzh'+index+'-'+line" v-model="group.facilities[line]" required/>
                                                <button type="button" class="fr btn btn-danger btn-remove-sm" @click="removeFacilityItem('zh', index, line)">删除</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div v-if="!hotel.hotel_facility && !hotel.hotel_facility_zh">
                            <h4>没有酒店服务与设施信息</h4>
                            <button class="btn btn-info" type="button" @click="addAttr('hotel_facility', 'en')">添加英文信息</button>
                            <button class="btn btn-info" type="button" @click="addAttr('hotel_facility', 'zh')">添加中文信息</button>
                        </div>
                    </form>
                    <form action="#" class="hotel-info-review form-horizontal" data-attr="reviews" id="hotelReviewForm">
                        <div v-if="hotel.hotel_reviews">
                            <div class="hotel-review">
                                <h2 class="hotel-block-title">酒店评价（英文）</h2>
                                <button type="button" class="add-new btn btn-success" @click="addReview('en', $event)">添加</button>
                                <div class="row">
                                    <div v-for="(review, index) in hotel.hotel_reviews" class="hotel-review-item col-sm-3">
                                        <div class="card">
                                            <div class="group-actions">
                                                <button class="btn btn-sm btn-info" type="button" @click="updateReview('en', 'add', index)" v-if="!review.id">保存</button>
                                                <button class="btn btn-sm btn-info" type="button" @click="updateReview('en', 'update', index)" v-else>更新</button>
                                                <button type="button" class="btn btn-sm btn-danger btn-remove" @click="removeReview('en', index)">删除</button>
                                            </div>
                                            <p><b>评价人姓名:</b> <input type="text" class="text-input" :name="'reviewName'+index" v-model="review.name" required></p>
                                            <p><b>评价人国家:</b> <input type="text" class="text-input" :name="'reviewCountry'+index" v-model="review.country" required></p>
                                            <p><b>星级:</b> <input type="text" class="text-input" :name="'reviewRate'+index" v-model="review.rate" required></p>
                                            <p>
                                                <b class="review-content-label">内容:</b>
                                                <textarea class="review-content form-control" :name="'reviewContent'+index"
                                                        :id="'reviewContent' + index" cols="30" rows="10" v-model="review.content"></textarea>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div v-if="hotel.hotel_reviews_zh">
                            <div class="hotel-review-zh">
                                <h2 class="hotel-block-title">酒店评价（中文）</h2>
                                <button type="button" class="add-new btn btn-success" @click="addReview('zh', $event)">添加</button>
                                <div class="row">
                                    <div v-for="(review, index) in hotel.hotel_reviews_zh" class="hotel-review-item col-sm-3">
                                        <div class="card">
                                            <div class="group-actions">
                                                <button class="btn btn-sm btn-info" type="button" @click="updateReview('zh', 'add', index)" v-if="!review.id">保存</button>
                                                <button class="btn btn-sm btn-info" type="button" @click="updateReview('zh', 'update', index)" v-else>更新</button>
                                                <button type="button" class="btn btn-sm btn-danger btn-remove" @click="removeReview('zh', index)">删除</button>
                                            </div>
                                            <p><b>评价人姓名:</b> <input type="text" class="text-input" :name="'reviewNamezh'+index" v-model="review.name" required></p>
                                            <p><b>评价人国家:</b> <input type="text" class="text-input" :name="'reviewCountryzh'+index" v-model="review.country" required></p>
                                            <p><b>星级:</b> <input type="text" class="text-input" :name="'reviewRatezh'+index" v-model="review.rate" required></p>
                                            <p>
                                                <b class="review-content-label">内容:</b>
                                                <textarea class="review-content form-control" :name="'reviewContentzh'+index"
                                                        :id="'reviewContent' + index" cols="30" rows="10" v-model="review.content"></textarea>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div v-if="!hotel.hotel_reviews && !hotel.hotel_reviews_zh">
                            <h4>没有酒店评价</h4>
                            <button class="btn btn-info" type="button" @click="addAttr('hotel_reviews', 'en')">添加英文信息</button>
                            <button class="btn btn-info" type="button" @click="addAttr('hotel_reviews', 'zh')">添加中文信息</button>
                        </div>
                    </form>
                    <form action="#" class="hotel-info-restaurant form-horizontal" data-attr="restaurant" id="hotelRestaurantForm">
                        <div class="hotel-restaurant" v-if="hotel.hotel_restaurant && hotel.hotel_restaurant.length > 0">
                            <h2 class="hotel-block-title">周边餐馆信息（英文）</h2>
                            <button type="button" class="add-new btn btn-success" @click="addRestaurant('en', $event)">添加</button>
                            <div class="row">
                                <div v-for="(restaurant, index) in hotel.hotel_restaurant" class="hotel-restaurant-item col-sm-3">
                                    <div class="card">
                                        <div class="group-actions">
                                            <button type="button" class="btn btn-sm btn-danger btn-remove" @click="removeRestaurant('en', index)">删除</button>
                                        </div>
                                        <p><b>Name:</b> <input type="text" class="text-input" :name="'restaurantName'+index" v-model="restaurant.name" required></p>
                                        <p><b>Food:</b> <input type="text" class="text-input" :name="'restaurantFood'+index" v-model="restaurant.food" required></p>
                                        <p><b>Open for:</b> <input type="text" class="text-input" :name="'restaurantOpen'+index" v-model="restaurant.open_for" required></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hotel-restaurant-zh" v-if="hotel.hotel_restaurant_zh && hotel.hotel_restaurant_zh.length > 0">
                            <h2 class="hotel-block-title">周边餐馆信息（中文）</h2>
                            <button type="button" class="add-new btn btn-success" @click="addRestaurant('zh', $event)">添加</button>
                            <div class="row">
                                <div v-for="(restaurant, index) in hotel.hotel_restaurant_zh" class="hotel-restaurant-item col-sm-3">
                                    <div class="card">
                                        <div class="group-actions">
                                            <button type="button" class="btn btn-sm btn-danger btn-remove" @click="removeRestaurant('zh', index)">删除</button>
                                        </div>
                                        <p><b>Name:</b> <input type="text" class="text-input" :name="'restaurantNamezh'+index" v-model="restaurant.name" required></p>
                                        <p><b>Food:</b> <input type="text" class="text-input" :name="'restaurantFoodzh'+index" v-model="restaurant.food" required></p>
                                        <p><b>Open for:</b> <input type="text" class="text-input" :name="'restaurantOpenzh'+index" v-model="restaurant.open_for" required></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div v-if="!hotel.hotel_restaurant && !hotel.hotel_restaurant_zh">
                            <h4>没有周边餐馆信息</h4>
                            <button class="btn btn-info" type="button" @click="addAttr('hotel_restaurant', 'en')">添加英文信息</button>
                            <button class="btn btn-info" type="button" @click="addAttr('hotel_restaurant', 'zh')">添加中文信息</button>
                        </div>
                    </form>
                    <form action="#" class="hotel-info-thumbnail form-horizontal" data-attr="thumbnail" id="hotelThumbnailForm">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4>当前首图</h4>
                                <img src="{{ $hotel['thumbnail'] }}" alt="" class="hotel-image image-rounded">
                            </div>
                            <div class="col-sm-6">
                                <h4>已选图片</h4>
                                <img :src="imgRoot + selectedImage" alt="" class="hotel-image image-rounded" v-if="selectedImage">
                            </div>
                        </div>
                        <div v-if="hotel.hotel_images && hotel.hotel_images.length > 0">
                            <h4>从以下列表中选择一个</h4>
                            <div class="row">
                                <div class="col-sm-3" v-for="image in hotel.hotel_images">
                                    <a href="javascript:void(0)" class="hotel-image-anchor" @click="updateHotelThumb(image)">
                                        <img :src="imgRoot + (image.url ? image.url : image)" :alt="hotel.name" class="hotel-image img-rounded">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form action="#" class="hotel-info-image form-horizontal" data-attr="images" id="hotelImageForm">
                        <div class="hotel-info-image-action row">
                            <div class="col-sm-7">
                                S3中存储位置: <b><span v-if="is_new == 1">Hotel_Image</span><span v-else>Extra_Image</span></b>
                            </div>
                            <div class="col-sm-5 action-wrapper">
                                <button type="button" class="btn btn-success" @click="manageImage('add')">添加</button>
                                <button type="button" class="btn btn-danger" @click="manageImage('delete')" :disabled="toDelete.length === 0">删除</button>
                            </div>
                        </div>
                        <div class="row" v-if="hotel.hotel_images && hotel.hotel_images.length > 0">
                            <div class="col-sm-3" v-for="image in hotel.hotel_images">
                                <div class="button-wrapper">
                                    <button type="button" class="btn btn-primary" @click="manageImage('replace', image)">替换</button>
                                    <input type="checkbox" class="hotel-image-checkbox" :value="image.url ? image.url : image" v-model="toDelete">
                                </div>
                                <img :src="imgRoot + (image.url ? image.url : image)" :alt="hotel.name" class="hotel-image img-rounded">
                                <p class="hotel-info-image-title">@{{ image.url ? image.url : image }}</p>
                            </div>
                        </div>
                        <div v-else>
                            没有酒店图片
                        </div>
                    </form>
                    <form action="#" class="hotel-info-card form-horizontal" data-attr="cards" id="hotelCardForm">
                        <div class="hotel-card" v-if="hotel.hotel_cards_accepted">
                            <h2 class="hotel-block-title">接受的银行卡类型</h2>
                            <button type="button" class="add-new btn btn-success" @click="addCardType">添加</button>
                            <p class="hotel-card-type" v-if="hotel.hotel_cards_accepted.cards">
                                <span v-for="(card, index) in hotel.hotel_cards_accepted.cards">
                                    <input type="text" class="text-input" :name="'cardsAccepted'+index" placeholder="请输入卡的类型" v-model="hotel.hotel_cards_accepted.cards[index]" required>
                                    <button type="button" class="btn btn-danger btn-remove-sm" @click="removeCardType(index)">删除</button>
                                    <span v-if="index !== hotel.hotel_cards_accepted.cards.length - 1">, </span>
                                </span>
                            </p>
                            <div>
                                <h2 class="hotel-block-title">描述</h2>
                                <textarea class="form-control" name="hotel-payment-desc" placeholder="请输入英文描述" v-model="hotel.hotel_cards_accepted.description"></textarea>
                            </div>
                            <div>
                                <h2 class="hotel-block-title">描述（中文）</h2>
                                <textarea class="form-control" name="hotel-payment-desc-zh" placeholder="请输入中文描述" v-model="hotel.hotel_cards_accepted.description_zh"></textarea>
                            </div>
                        </div>
                        <div v-else>
                            <h4>没有酒店支付信息</h4>
                            <button class="btn btn-info" type="button" @click="addAttr('hotel_cards')">添加</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal fade" id="imageModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">提交图片</h4>
                            <p>仅支持jpg, png, pdf, 且文件名称不可超过{{ 79 - strlen((string)$hotel->productId) }}个字符，文件大小不超过2M，一次最多添加20张图片。</p>
                        </div>
                        <div class="modal-body">
                            <div class="sk-spinner sk-spinner-three-bounce" v-if="isLoading">
                                <div class="sk-bounce1"></div>
                                <div class="sk-bounce2"></div>
                                <div class="sk-bounce3"></div>
                            </div>
                            <label for="selectedImage" v-else>
                                <input type="file" name="hotelImage" id="selectedImage" v-if="replaceImg" accept="image/png, image/jpeg, application/pdf">
                                <input type="file" name="hotelImages" id="selectedImage" accept="image/png, image/jpeg, application/pdf" multiple v-else>
                            </label>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" @click="uploadImage">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <h2>酒店不存在</h2>
        @endif
    </div>

    <script>
        var hotel = {!! json_encode($hotel) !!};
        //parse html entities this way
        var txt = document.createElement("textarea");
        txt.innerHTML = JSON.stringify(hotel);
        hotel = JSON.parse(txt.value);
        //has rich data or not
        var is_new = {{ $is_new }};
        //get active tab
        var url_parts = window.location.href.split('#');
        var tab = url_parts.length > 1 ? url_parts.pop() : '';
    </script>
    <script src="{{ url('/assets/js/pc/all.hotel-update.js') }}"></script>
</body>

</html>