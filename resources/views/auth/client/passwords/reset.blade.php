@extends('layouts.master')

<?php
$domain = $is_usitour ? 'Usitour' : 'Usitrip';
?>

{{--single page title--}}
@if	($language === 0)
    @section('title', $domain. '酒店预订平台 - 人人赚项目密码重设')
@elseif	($language === 2)
    @section('title', $domain. '飯店預定平台 - 人人賺項目密碼重設')
@else
    @section('title', 'Hotel - Reset Password | ' . $domain)
@endif

@section('metas')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{$domain}} {{$language == 0 ? '酒店预订平台 - 重设密码':'Hotel -  Reset Password' }}">
    <meta name="author" content="{{$domain}}">
    <meta name="Keywords" content="{{$domain}} {{$language == 0 ? '酒店预订平台 - 重设密码':'Hotel - Reset Password' }}"/>
    <link href="@if($is_usitour&& $language !== 2){{asset('faviconen.ico')}}@else{{asset('favicon.ico')}}@endif" rel="shortcut icon">
    <link rel="canonical" href="{{ config('app.url') }}">
@endsection

{{--single page styles--}}
@section('css')
    <style>
        .middle-box {
            z-index: 100;
            margin: 0 auto;
            padding-top: 40px;
            margin-bottom: 70px;
            max-width: 510px;
        }
        .middle-box h1 {
            font-size: 170px;
        }
        .modal-static {
            position: fixed;
            top: 50% !important;
            left: 50% !important;
            margin-top: -100px;
            margin-left: -100px;
            overflow: visible !important;
        }
        .modal-static,
        .modal-static .modal-dialog,
        .modal-static .modal-content {
            width: 200px;
            height: 200px;
        }
        .modal-static .modal-dialog,
        .modal-static .modal-content {
            padding: 0 !important;
            margin: 0 !important;
        }
        .modal-static .modal-content .icon {
        }
        .login-text {
            text-align: center;
            font-size: 20px;
            margin-right: -5px;
            margin-top: 15px;
        }
        .login-line {
            width: 179px;
            float: left;
            text-align: right;
        }
        img.img-responsive.img-logo {
            text-align: center;
            width: 100%;
        }
        .btn-primary {
            background-color: #e76c0e;
            border-color: #e76c0e;
        }
        .btn-primary:hover {
            background-color: #e76c0e;
            border-color: #e76c0e;
        }
        .has-error .form-control, .has-error .form-control:focus {
            border-color: #ed5565;
        }
        /*iPhone 6, 7, & 8 in portrait & landscape*/
        @media (max-width: 667px) {
            .img-logo {
                margin: 10px auto;
            }
            .login-text {
                text-align: center;
                margin: 0 auto;
                width: 90vw;
            }
            .login-line {
                display: none;
            }
            #en_topbar,#en_nav,#id_nav_head {
                display: none;
            }
            img.img-responsive.img-logo {
                margin: 0 auto;
            }
            .middle-box {
                margin-bottom: 70px;
                max-width: 90vw;
            }
        }

    </style>
@endsection

@section('content')
    @if(!$is_b && !$is_aa)
        @include('components.nav-bar')
    @endif
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <a href="{{url('/')}}"><img style="height:50px; text-align:center;" src="/img/general/logo/logo-usitrip.png"/></a>
            <h3 class="login-text">{{$language === 1?  'Reset Password':'重设密码'}}</h3>
            <br/>
            <form class="m-t" role="form" method="POST" action="{{ url('affiliate/password/reset') }}">
                {!! csrf_field() !!}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder=" {{$language === 1?  'Email':'邮箱'}}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" name="password" placeholder=" {{$language === 1?  'Password':'密码'}}" onclick="showRule()" autocomplete="new-password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" name="password_confirmation" autocomplete="new-password" placeholder="{{$language === 1?  'Confirm Password':'确认密码'}}">
                    <span class="help-block" style="visibility:hidden;" id="passwordRule">
                            <strong style="color:#f1791e; font-size:12px; font-weight:300; line-height:1;">{{$language === 1 ? 'Contains 8+ characters with 2+ of the following: a-z, A-Z, 0-9, symbols.' : '八个以上字符（2个以上 : a-z, A-Z, 0-9, 特殊符号）.'}}</strong>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                    @endif
                </div>
                <button type="submit" data-toggle="modal" data-target="#processing-modal" class="btn btn-primary btn-block">
                    {{$language === 1?  'Reset':'重新设置'}}
                    </button>
            </form>
            <div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <img src="/img/general/loading-spinner.gif" class="icon" style="margin-left:-50px;"/>
                    <h4>
                        {{$language === 1?  'Processing...':'处理中...'}}
                        <button type="button" class="close" style="float: none;" data-dismiss="modal" aria-hidden="true"></button>
                    </h4>
                </div>
            </div>
            <p class="m-t">
                <small>{{$language === 1?  $domain.' - Hotel' : '走四方酒店 '}} All rights reserved &copy;  <span id="currentYear"></span></small>
            </p>
        </div>
    </div>
@endsection
{{--single page scripts--}}
@section('scripts')
    <script src="{{ asset('assets/js/plugins.generic.js') }}"></script>
    <script type="text/javascript">
      var currentYear = new Date().getFullYear();
      $('#currentYear').html(currentYear);
        @if (session('status'))
        swal({
          title: "",
          text: "{{session('status')}}",
          type: "success",
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "OK",
        }, function () {
          window.location.href = "{{url('')}}";
        });
        @endif

        function showRule () {
          var visibility = document.getElementById('passwordRule').style.visibility;
          if (visibility == "hidden")
            document.getElementById('passwordRule').style.visibility = 'visible';
          else
            document.getElementById('passwordRule').style.visibility = 'hidden';
        }
    </script>
@endsection
