@extends('layouts.master')

<?php
$domain = $is_usitour ? 'Usitour' : 'Usitrip';
?>

{{--single page title--}}
@if	($language === 0)
    @section('title', $domain. '酒店预订平台 - 人人赚项目密码重设')
@elseif	($language === 2)
    @section('title', $domain. '飯店預定平台-人人賺項目密碼重設')
@else
    @section('title', 'Hotel - Reset Password | ' . $domain)
@endif

@section('metas')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{$domain}} {{$language == 0 ? '酒店预订平台 - 重设密码':'Hotel -  Reset Password' }}">
    <meta name="author" content="{{$domain}}">
    <meta name="Keywords" content="{{$domain}} {{$language == 0 ? '酒店预订平台 - 重设密码':'Hotel - Reset Password' }}"/>
    <link href="@if($is_usitour && $language !== 2){{asset('faviconen.ico')}}@else{{asset('favicon.ico')}}@endif" rel="shortcut icon">
    <link rel="canonical" href="{{ config('app.url') }}">
@endsection

{{--single page styles--}}
@section('css')
    <style>
        .middle-box {
            z-index: 100;
            margin: 0 auto;
            padding-top: 40px;
            margin-bottom: 70px;
            max-width: 510px;
        }
        .middle-box h1 {
            font-size: 170px;
        }
        .modal-static {
            position: fixed;
            top: 50% !important;
            left: 50% !important;
            margin-top: -100px;
            margin-left: -100px;
            overflow: visible !important;
        }
        .modal-static,
        .modal-static .modal-dialog,
        .modal-static .modal-content {
            width: 200px;
            height: 200px;
        }
        .modal-static .modal-dialog,
        .modal-static .modal-content {
            padding: 0 !important;
            margin: 0 !important;
        }
        .login-text {
            float: left;
            font-size: 20px;
            margin-right: -5px;
            margin-top: 15px;
        }
        .login-line {
            width: 180px;
            float: left;
            text-align: right;
        }
        img.img-responsive.img-logo {
            text-align: center;
            width: 100%;
        }
        .btn-primary {
            background-color: #e76c0e;
            border-color: #e76c0e;
        }
        .btn-primary:hover {
            background-color: #e76c0e;
            border-color: #e76c0e;
        }
        .has-error .form-control, .has-error .form-control:focus {
            border-color: #ed5565;
        }
        /*iPhone 6, 7, & 8 in portrait & landscape*/
        @media (max-width: 667px) {
            .img-logo {
                margin: 10px auto;
            }
            .login-text {
                text-align: center;
                margin: 0 auto;
                width: 90vw;
            }
            .login-line {
                display: none;
            }
            #en_topbar, #en_nav, #id_nav_head {
                display: none;
            }
            img.img-responsive.img-logo {
                margin: 0 auto;
            }
            .middle-box {
                margin-bottom: 70px;
                max-width: 90vw;
            }
        }
    </style>
@endsection

@section('content')
    @if(!$is_b && !$is_aa)
        @include('components.nav-bar')
    @endif
    <div class="middle-box text-center animated fadeInDown">
    <div>
        {{--<a href="{{url('/')}}"><img src="@if($is_usitour) /img/general/logo/logo-usitour.png @else /img/general/logo/logo-usitrip.png @endif" class="img-responsive img-logo"/></a>--}}
        {{--<br/> <br/>--}}
        <img class="login-line" src="/img/general/line.png"/>
        <p class="login-text">{{$language === 1?  'Reset Password':'重设密码'}}</p>
        <img class="login-line" src="/img/general/line.png"/>
        <form class="m-t" role="form" method="POST" action="{{ url('affiliate/password/email') }}" style="margin-top:49px;">
            {!! csrf_field() !!}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" style="padding-bottom:20px; padding-top:40px;">

                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                @if ($errors->has('email'))
                    <span class="help-block">
<strong class="pull-left" style="color:#a94442; font-size:12px; font-weight:400;">{{$language === 1 ? 'Email does not exist' : '邮箱不存在或输入有误'}}</strong>
                    <!--strong>{{ $errors->first('email') }}</strong-->
						</span>
                @endif
                <span style="float:left; text-align:left;"><br/>
                    <medium>{{$language == 1? 'Enter the email address associated with your account, and we\'ll email you a link to reset your password.' : '请输入与您的帐号关联的邮箱地址，我们将通过电子邮件给您发送重置密码的链接。'}}</medium>
                    <br/></span>

            </div>

            <br/><br/>
            <button type="submit" data-toggle="modal" data-target="#processing-modal" class="btn btn-primary btn-block" style="background-color:#f1791e;"><i class="fa fa-envelope"></i>{{$language === 1 ? 'Send Reset Link' :'发送重置链接'}} </button>
        </form>
        <div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <img src="/img/general/loading-spinner.gif" class="icon" style="margin-left:-50px;"/>
                <h4>{{$language === 1 ? 'Processing...' :'处理中...'}}
                    <button type="button" class="close" style="float: none;" data-dismiss="modal" aria-hidden="true"></button>
                </h4>
            </div>
        </div>
    </div>
</div>
@endsection
{{--single page scripts--}}
@section('scripts')
<script src="{{ asset('assets/js/plugins.generic.js') }}"></script>
<script type="text/javascript">
    @if (session('status'))
    swal({
        title: "",
        text: "{{session('status')}}",
        type: "success",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "OK",
    }, function () {
        window.location.href = "{{url('')}}";
    });
    @endif
</script>
@endsection
