@extends('layouts.master')

<?php
$isMobile = \Agent::isMobile();
$domain = $is_usitour ? 'Usitour' : 'Usitrip';
$title = $language === 0 ? '酒店预订平台 - 人人赚项目会员注册':$language === 2 ? '飯店預定平台 -人人賺項目會員註冊':' Hotel - Affiliate Sign Up';
?>

{{--single page title--}}
@section('title', $domain. $title)

{{--single page metas--}}

@section('metas')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{$domain.$title}}">
    <meta name="author" content="{{$domain}} ">
    <meta name="Keywords" content="{{$domain.$title}}"/>
    <link href="@if($is_usitour&& $language !== 2){{asset('faviconen.ico')}}@else{{asset('favicon.ico')}}@endif" rel="shortcut icon">
    <link rel="canonical" href="{{ config('app.url') }}">
@endsection

{{--single page styles--}}
@section('css')
    {{--webpack css--}}
    <link rel="stylesheet" href="{{ asset('assets/css/pc/plugins.home.css') }}">
    <style>
        #noRobots{
            margin: 0 auto 10px;
            display: inline-flex;
        }
        iframe{
            display: inline-block;
        }
        .hasAccount{
            margin-top:10px;
        }
        .btn-login {
            color: #f1791e ! important;
            border-color: #f1791e;
            font-size: 13px;
            border-radius: 1px;
        }

        .btn-login:hover {
            background: #f1791e;
            color: #fff ! important;
            border-color: #f1791e;
            font-size: 13px;
            border-radius: 1px;
        }

        .modal-static {
            position: fixed;
            top: 50% !important;
            left: 50% !important;
            margin-top: -100px;
            margin-left: -100px;
            overflow: visible !important;
        }

        .modal-static,
        .modal-static .modal-dialog,
        .modal-static .modal-content {
            width: 200px;
            height: 200px;
        }

        .modal-static .modal-dialog,
        .modal-static .modal-content {
            padding: 0 !important;
            margin: 0 !important;
        }

        .modal-static .modal-content .icon {
        }

        .middle-box {
            padding: 15px 15px 0 15px;
            border: 8px solid #e7eaec;
            background: #fff;
        }

        .register-line-left {
            width: 50%;
            float: left;
            margin-left: -23px;
        }

        .register-line-right {
            float: left;
            margin-right: -50px;
            width: 50%;
            text-align: right;
        }

        .register-style {
            float: left;
            font-size: 20px;
            margin-top: 25px;
        }

        .register-error-tips {
            color: #a94442;
            font-size: 12px;
            font-weight: 400;
        }

        .register-password-rule {
            color: #f1791e;
            font-size: 12px;
            font-weight: 300;
            line-height: 1;
        }

        .register-button {
            border-radius: 1px;
            border: #f1791e;
            background-color: #f1791e !important;
            opacity: 1;
            width: 100% !important;
        }

        .register-login {
            float: left;
            margin-top: 5px;
            width: 100px;
        }

        .register-has-account {
            float: left;
            margin-top: 12px;
            font-size: 13px;
        }

        .icon {
            margin-left: -50px;
        }

        .stay-middle {
            width: 1200px;
            margin: 0 auto;
            padding: 30px 0;
        }

        .login-intro-img {
            margin-bottom: 30px;
            width: 700px;
        }

        .login-intro {
            margin-bottom: 30px;
        }

        .stay-middle h2 {
            margin-bottom: 16px;
            margin-top: 10px;
            font-weight: 600;
        }

        .register-agreement {
            margin-left: 10px;
            float: left;
            line-height: 21px;
        }

        /*iPhone 6, 7, & 8 in portrait & landscape*/
        @media (max-width: 667px) {
            #en_topbar, #en_nav, #id_nav_head {
                display: none;
            }

            .stay-middle {
                width: 100% !important;
            }

            .register-style {
                margin-top: 15px;
                font-size: 14px;
            }

            .register-login {
                width: 90px;
                font-size: 11px;
            }

            img.img-responsive.img-logo {
                margin: 0 auto;
            }

            .footer-text {
                padding-right: 8vw;
            }

            .register-agreement {
                /*margin-top: -17px;*/
                margin-left: 18px;
            }
        }

        /* 20190306 may*/
        .login-intro h3 {
            color: #333333;
            font-size: 18px;
            margin: 25px 0 10px 0;
        }

        form {
            margin-bottom: 30px;
        }

        .login-intro h3 {
            font-size: 18px;
            margin: 25px 0 10px 0;
            color: #333333;
        }

        .login-intro p {
            color: #666666;
        }

        .register-agreement a {
            text-decoration: underline;
        }

        .text-login {
            margin: 10px 0;
        }
        .form-reg .form-control{
            border-radius: 0;
        }
        .text-register{
            float: left;
            font-size: 20px;
            margin-top: 5px;
            margin-bottom: 10px;
        }
        /*firefox*/
        .stay-middle .col-md-5{
            z-index: 2;
        }

    </style>
@endsection
{{--/single page styles--}}

@section('content')
    @if(!$is_b && !$is_aa)
        @include('components.nav-bar')
    @endif
    <div class="stay-middle row">
        {{--PC left: intro start--}}
        @if(!$isMobile)
            @include('components.affiliate-intro')
        @endif
        {{--PC left: intro end--}}
        <div class="col-md-5">
            <div class="middle-box text-center animated fadeInDown">
                <div>
                    <p class="text-register">@lang('affiliate-register.sign_up_member')</p>
                    <form id="affiliate-register-form" class="m-t form-reg" role="form" method="POST" action="{{ url('affiliate/register') }}" autocomplete="off">
                        <input type="hidden" name="language" value="{{$language}}"/>
                        <input type="hidden" name="domain" value="{{$domain}}"/>
                        {!! csrf_field() !!}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="@lang('affiliate-register.username')" required>
                            @if ($errors->has('name'))
                                <span class="help-block"><strong class="register-error-tips">@lang('affiliate-register.username_is_required')</strong></span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('affiliate-register.email')" autocomplete="username" required>
                            @if ($errors->has('email'))
                                <span class="help-block"><strong class="register-error-tips">{{ $errors->first('email') }}</strong></span>
                            @endif

                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" name="password" placeholder="@lang('affiliate-register.password')" onfocus="document.getElementById('passwordRule').style.display = document.getElementById('passwordRule').style.display === 'none' ? 'block' : 'none';"
                                   autocomplete="new-password" required>
                            @if ($errors->has('password'))
                                <span class="help-block"><strong class="register-error-tips">{{ $errors->first('password') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" name="password_confirmation" placeholder="@lang('affiliate-register.confirm_password')"
                                   autocomplete="new-password" required>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block"><strong class="register-error-tips">{{ $errors->first('password_confirmation') }}</strong> </span>
                            @endif
                            <span class="help-block" style="display:none;" id="passwordRule"><strong class="register-password-rule">@lang('affiliate-register.password_rules')</strong></span>
                        </div>
                        <div>
                            <label class="register-agreement">
                            <input type="checkbox" id="cb" name="cb" class="pull-left">
                            @lang('affiliate-register.agree') {{$domain}} @lang('affiliate-register.s')
                                <a href="{{ url('affiliate/terms-of-use') }}" target="_blank">  @lang('affiliate-register.term_of_use')</a>  @lang('affiliate-register.and')
                                <a href="{{ url('affiliate/private-policy') }}" target="_blank">  @lang('affiliate-register.private_policy')</a>
                            </label>
                            <br><br>
                        </div>
                        <div id="noRobots" class="g-recaptcha" data-sitekey="{{config('app.recaptcha_site_key')}}" data-callback="correctCaptcha"></div>
                        <button id="bt" name="bt" disabled type="button" onclick="affiliateSignUp()" class="btn btn-primary btn-block btn-block register-button btn-lg" title="@lang('affiliate-register.human')">
                            @lang('affiliate-register.sign_up')
                        </button>
                        {{--login--}}
                        <div class="hasAccount">
                            <p class="text-login pull-left">@lang('affiliate-register.have_account')?</p>
                            <a class="btn pull-right btn-login" href="{{ url('affiliate/login') }}">@lang('affiliate-register.affiliate_user_login')</a>
                        </div>
                    </form>
                    <br/>
                </div>
            </div>
        </div>
    </div>
    {{--mobile bottom: intro start--}}
    @if($isMobile)
        @include('components.affiliate-intro')
    @endif
    {{--mobile bottom: intro end--}}
@endsection

{{--single page scripts--}}
@section('scripts')
    <script src="https://recaptcha.net/recaptcha/api.js"></script>
    <script src="{{ asset('assets/js/plugins.generic.js') }}"></script>
    <script src="{{ asset('assets/js/business/plugins.home.js') }}"></script>
    <script>
      $(document).ready(function() {
        $('#hotelHead').html('{{$domain}}');
      });
      var correctCaptcha = function(response) {
          if(response.length !== 0){
              $('#bt').attr('disabled', false);
              $('#bt').attr('title', '');
          }
      };
      function affiliateSignUp() {
          var isValid = true;
          var email = $('input[name="email"]').val();
          var atpos = email.indexOf("@");
          var dotpos = email.lastIndexOf(".");

          $('input').filter('[required]:visible').each(function () {
              $(this).val($(this).val().trim());
              if ($(this).val() === '') {
                  $(this).focus();
                  isValid = false;
              }
          });

          if ($('input[name="name"]').val().trim() === '') {
              swal(' @lang('affiliate-register.username_error')');
          } else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
              swal('@lang('affiliate-register.email_error')');
          } else if ($('input[name="password_confirmation"]').val() !== $('input[name="password"]').val()) {
              swal('@lang('affiliate-register.password_error')');
          } else if (isValid === false) {
              swal( '@lang('affiliate-register.input_error')');
          } else if (!document.getElementById('cb').checked) {
              swal('@lang('affiliate-register.must_agree')');
          } else {
              FullscreenSpinner.create();
             $('#affiliate-register-form').submit();
          }
      }

    </script>
@endsection