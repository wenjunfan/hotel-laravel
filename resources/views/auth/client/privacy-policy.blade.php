@extends('layouts.master')

<?php
$domain = $is_usitour ?  'Usitour' : 'Usitrip';
?>

@section('metas')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{$domain}}'s User Registration Privacy Policy">
    <meta name="author" content="">
    <meta name="Keywords" content="{{$domain}} International Hotel, Hotel Booking"/>
@endsection

@if	($language === 0)
    @section('title', '酒店预订，最便宜的北美酒店查询预订平台-' . $domain . '人人赚注册隐私政策')
@elseif($language === 2)
    @section('title', '飯店預定，最便宜的北美飯店查詢預定平台-' . $domain . '人人賺註冊隱私政策')
@else
    @section('title', 'Hotels: Cheap & Discount Hotel Deals, Hotel Booking | ' . $domain .' Affiliate Registration Privacy Policy')
@endif

{{--single page styles--}}
@section('css')
    <link rel="canonical" href="{{ config('app.url') }}">
    {{--webpack css--}}
    <link rel="stylesheet" href="{{ asset('assets/css/pc/plugins.home.css') }}">
    <style>
        @media (max-width: 667px) {
            #en_topbar,#en_nav,#id_nav_head {
                display: none;
            }
            .g-wrap{
                width:100%;
                padding:2vw;
            }
        }
    </style>
@endsection
{{--/single page styles--}}

@section('content')
    @if(!$is_b && !$is_aa)
        @include('components.nav-bar')
    @endif
    <div class="g-wrap">

    <br><font size="6">{{$domain}}'s Privacy Policy Introduction</font> <br>
    <br>Unitedstars International, Ltd., a California corporation d/b/a "{{$domain}}" ("{{$domain}}," "Company, “we,” “us,” “our”) respect your privacy and are committed to protecting it through our compliance with this policy.
    <br>This policy describes the types of information we may collect from you or that you may provide when you visit our online platform that provides travel products and services (the "Services"), which online platform is accessible at www.{{$domain}}.com (collectively with any other websites through which the Company makes the Services available, the “Site”) and our practices for collecting, using, maintaining, protecting and disclosing that information.
    <br>
    <br><font size="6">This policy applies to information we collect: </font>
    <br>&nbsp;On the Services.
    <br>&nbsp;In e-mail, text and other electronic messages between you and the Services.
    <br>&nbsp;Through mobile and desktop applications you download from the Services, which provide dedicated non-browser-based interaction between you and the Services.
    <br>&nbsp;When you interact with our advertising and applications on third-party websites and services, if those applications or advertising include links to this policy.
    <br>
    <br><font size="6">It does not apply to information collected by: </font>
    <br>&nbsp;us offline or through any other means, including on any other website operated by Company or any third party (including our affiliates and subsidiaries); or
    <br>&nbsp;any third party (including our affiliates and subsidiaries), including through any application or content (including advertising) that may link to or be accessible from or on the Services.
    <br>Please read this policy carefully to understand our policies and practices regarding your information and how we will treat it. If you do not agree with our policies and practices, your choice is not to use our Website. By accessing or using the Services, you agree to this privacy policy. This policy may change from time to time. Your continued use of the Services after we make changes is deemed to be acceptance of those changes, so please check the policy periodically for updates.
    <br><font size="6">Children Under the Age of 13 </font>
    <br>
    <br>Our Services are not intended for children under 18 years of age. We do not knowingly collect personal information from children under 13. If you are under 13, do not use or provide any information on the Services or on or through any of its features/register on the Services, make any purchases through the Services, use any of the interactive or public comment features of the Services or provide any information about yourself to us, including your name, address, telephone number, e-mail address or any screen name or user name you may use. If we learn we have collected or received personal information from a child under 13 without verification of parental consent, we will delete that information. If you believe we might have any information from or about a child under 13, please contact us at bd@usitrip.com
    <br>Information We Collect About You and How We Collect It
    <br><font size="6">We collect several types of information from and about users of our Services, including information: </font>
    <br>&nbsp;by which you may be personally identified, such as name, postal address, e-mail address, telephone number, or any other identifier by which you may be contacted online or offline ("personal information");
    <br>&nbsp;that is about you but individually does not identify you, such as Place of departure, destination, itinerary; and/or
    <br>&nbsp;about your internet connection, the equipment you use to access our Services and usage details.
    <br>
    <br><font size="6">We collect this information: </font>
    <br>&nbsp;Directly from you when you provide it to us.
    <br>&nbsp;Automatically as you navigate through the Site. Information collected automatically may include usage details, IP addresses and information collected through cookies, web beacons, and other tracking technologies.
    <br>&nbsp;From third parties, for example, our business partners.
    <br>
    <br><font size="6">Information You Provide to Us. The information we collect on or through our Services may include: </font>
    <br>&nbsp;Information that you provide by filling in forms on the Site. This includes information provided at the time of registering to use our Services, posting material, or requesting further services. We may also ask you for information when you enter a contest or promotion sponsored by us, and when you report a problem with our Services.
    <br>&nbsp;Records and copies of your correspondence (including e-mail addresses), if you contact us.
    <br>&nbsp;Your responses to surveys that we might ask you to complete for research purposes.
    <br>&nbsp;Details of transactions you carry out through our Services. You may be required to provide financial information before listing or booking adventure travel destinations, activities, products or services through our Site.
    <br>&nbsp;Your search queries on the Site.
    <br>
    <br>You also may provide information to be published or displayed (hereinafter, "posted") on public areas of the Site, or transmitted to other users of the Site or third parties (collectively, "User Contributions"). Your User Contributions are posted on and transmitted to others at your own risk. Although [we limit access to certain pages/you may set certain privacy settings for such information by logging into your account profile], please be aware that no security measures are perfect or impenetrable. Additionally, we cannot control the actions of other users of the Site with whom you may choose to share your User Contributions. Therefore, we cannot and do not guarantee that your User Contributions will not be viewed by unauthorized persons.
    <br>Information We Collect Through Automatic Data Collection Technologies. As you navigate through and interact with our Services, we may use automatic data collection technologies to collect certain information about your equipment, browsing actions and patterns, including:
    <br>&nbsp;Details of your visits to our Site, including traffic data, location data, logs and other communication data and the resources that you access and use on the Site.
    <br>&nbsp;Information about your computer and internet connection, including your IP address, operating system and browser type.
    <br>
    <br>&nbsp;The information we collect automatically may include personal information. It helps us to improve our Services and to deliver a better and more personalized service, including by enabling us to:
    <br>&nbsp;Estimate our audience size and usage patterns.
    <br>&nbsp;Store information about your preferences, allowing us to customize our Website according to your individual interests.
    <br>&nbsp;Speed up your searches.
    <br>&nbsp;Recognize you when you return to our Site.
    <br>
    <br>&nbsp;The technologies we use for this automatic data collection may include:
    <br>&nbsp;Cookies (or browser cookies). A cookie is a small file placed on the hard drive of your computer. You may refuse to accept browser cookies by activating the appropriate setting on your browser. However, if you select this setting you may be unable to access certain parts of our Services. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies when you direct your browser to our Services.
    <br>&nbsp;Flash Cookies. Certain features of our Services may use local stored objects or Flash cookies to collect and store information about your preferences and navigation to, from and on our Services. Flash cookies are not managed by the same browser settings as are used for browser cookies.
    <br>&nbsp;Web Beacons. Pages of our Site and our e-mails may contain small electronic files known as web beacons (also referred to as clear gifs. pixel tags and single-pixel gifs) that permit the Company, for example, to count users who have visited those pages or opened an e-mail and for other related website statistics (for example, recording the popularity of certain website content and verifying system and server integrity).

    <br>Third-party Use of Cookies [and Other Tracking Technologies.
    <br>
    <br>Some content or applications, including advertisements, on the Services are served by third-parties, including advertisers, ad networks and servers, content providers and application providers. These third parties may use cookies, alone or in conjunction with web beacons or other tracking technologies, to collect information about you when you use our Services. The information they collect may be associated with your personal information or they may collect information, including personal information, about your online activities over time and across different websites and other online services. They
    <br>may use this information to provide you with interest-based (behavioral) advertising or other targeted content.
    <br>We do not control these third parties' tracking technologies or how they may be used. If you have any questions about an advertisement or other targeted content, you should contact the responsible provider directly.
    <br>
    <br><font size="6">How We Use Your Information </font>
    <br>
    <br><font size="3">We use information that we collect about you or that you provide to us, including any personal information: </font>
    <br>
    <br>&nbsp;To present our Services and its contents to you.
    <br>&nbsp;To provide you with information, products or services that you request from us.
    <br>&nbsp;To fulfill any other purpose for which you provide it.
    <br>&nbsp;To provide you with notices about your account.
    <br>&nbsp;To carry out our obligations and enforce our rights arising from any contracts entered into between you and us, including for billing and collection.
    <br>&nbsp;To notify you about changes to our Site or any products or services we offer or provide though our Site.
    <br>&nbsp;To allow you to participate in interactive features on our Site.
    <br>&nbsp;In any other way we may describe when you provide the information.
    <br>&nbsp;For any other purpose with your consent.
    <br>
    <br>We may also use your information to contact you about our own and third-parties' goods and services that may be of interest to you. If you do not want us to use your information in this way, please [adjust your user preferences in your account profile.
    <br>We may use the information we have collected from you to enable us to display advertisements to our advertisers' target audiences. Even though we do not disclose your personal information for these purposes without your consent, if you click on or otherwise interact with an advertisement, the advertiser may assume that you meet its target criteria.
    <br>
    <br><font size="6">Disclosure of Your Information</font>
    <br>
    <br>We may disclose aggregated information about our users, and information that does not identify any individual, without restriction.
    <br>We may disclose personal information that we collect or you provide as described in this privacy policy:
    <br>
    <br>&nbsp;To our subsidiaries and affiliates.
    <br>&nbsp;To contractors, service providers and other third parties we use to support our business, and who are bound by contractual obligations to keep personal information confidential and use it only for the purposes for which we disclose it to them.
    <br>&nbsp;To a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution or other sale or transfer of some or all of the Company's assets, whether as a going concern or as part of bankruptcy, liquidation or similar proceeding, in which personal information held by the Company about the users of our Services is among the assets transferred.
    <br>&nbsp;To third parties to market their products or services to you if you have not opted out of these disclosures.
    <br>&nbsp;To fulfill the purpose for which you provide it.
    <br>&nbsp;For any other purpose disclosed by us when you provide the information.
    <br>&nbsp;With your consent.
    <br>
    <br>&nbsp;<font size="6">We may also disclose your personal information: </font>
    <br>&nbsp;To comply with any court order, law or legal process, including to respond to any government or regulatory request.
    <br>&nbsp;To enforce or apply our Terms of Use and other agreements, including for billing and collection purposes.
    <br>&nbsp;If we believe disclosure is necessary or appropriate to protect the rights, property, or safety of the Company, our customers or others. This includes exchanging information with other companies and organizations for the purposes of fraud protection and credit risk reduction.
    <br>
    <br><font size="6">Choices About How We Use and Disclose Your Information</font>
    <br>
    <br>We strive to provide you with choices regarding the personal information you provide to us. We have created mechanisms to provide you with the following control over your information:
    <br>&nbsp;Tracking Technologies and Advertising. You can set your browser to refuse all or some browser cookies, or to alert you when cookies are being sent. To learn how you can manage your Flash cookie settings, visit the Flash player settings page on Adobe's website. If you disable or refuse cookies, please note that some parts of this site may then be inaccessible or not function properly.
    <br>&nbsp;Disclosure of Your Information for Third-Party Advertising. If you do not want us to share your personal information with unaffiliated or non-agent third parties for promotional purposes, you can opt-out by adjusting your user preferences in your account profile by checking or unchecking the relevant boxes or by sending us an e-mail stating your request to bd@usitrip.com.
    <br>&nbsp;Promotional Offers from the Company. If you do not wish to have your [e-mail address/contact information] used by the Company to promote our own or third parties' products or services, you can opt-out by adjusting your user preferences in your account profile by checking or unchecking the relevant boxes or by sending us an e-mail stating your request to bd@usitrip.com. If we have sent you a promotional e-mail, you may send us a return e-mail asking to be omitted from future e-mail distributions.
    <br>&nbsp;Targeted Advertising. If you do not want us to use information that we collect or that you provide to us to deliver advertisements according to our advertisers' target-audience preferences, you can opt-out by adjusting your user advertising preferences in your account profile by checking or un-checking the relevant boxes or by sending us an e-mail stating your request to bd@usitrip.com. For this opt-out to function, you must have your browser set to accept browser cookies.
    <br>
    <br>We do not control third parties' collection or use of your information to serve interest-based advertising. However these third parties may provide you with ways to choose not to have your information collected or used in this way.
    <br>
    <br><font size="6">Accessing and Correcting Your Information </font>
    <br>You can review and change your personal information by logging into the Site and visiting your account profile page.
    <br>
    <br>You may also send us an e-mail at bd@usitrip.com to request access to, correct or delete any personal information that you have provided to us. We cannot delete your personal information except by also deleting your user account. We may not accommodate a request to change information if we believe the change would violate any law or legal requirement or cause the information to be incorrect.
    <br>If you delete your User Contributions from the Site, copies of your User Contributions may remain viewable in cached and archived pages, or might have been copied or stored by other Site users. Proper access and use of information provided on the Site, including User Contributions, is governed by our Terms of Use.
    <br>
    <br><font size="6">Your California Privacy Rights </font>
    <br>
    <br>California Civil Code Section § 1798.83 permits users of our Services that are California residents to request certain information regarding our disclosure of personal information to third parties for their direct marketing purposes. To make such a request, please send an e-mail to [E-MAIL ADDRESS][ or write us at: [MAILING ADDRESS].
    <br>
    <br><font size="6">Data Security </font>
    <br>
    <br>The safety and security of your information also depends on you. Where we have given you (or where you have chosen) a password for access to certain parts of our Website, you are responsible for keeping this password confidential. We ask you not to share your password with anyone. We urge you to be careful about giving out information in public areas of the Site like message boards. The information you share in public areas may be viewed by any user of the Site.
    <br>Unfortunately, the transmission of information via the internet is not completely secure. Although we do our best to protect your personal information, we cannot guarantee the security of your personal information transmitted to our Site. Any transmission of personal information is at your own risk. We are not responsible for circumvention of any privacy settings or security measures contained on the Site.
    <br>
    <br>Changes to Our Privacy Policy
    <br>
    <br>It is our policy to post any changes we make to our privacy policy on this page. If we make material changes to how we treat our users' personal information, we will notify you by e-mail to the e-mail address specified in your account or through a notice on the Site home page. The date the privacy policy was last revised is identified at the top of the page. You are responsible for ensuring we have an up-to-date active and deliverable e-mail address for you, and for periodically visiting our Site and this privacy policy to check for any changes.
    <br>
    <br><font size="6">Contact Information</font>
    <br>
    <br>To ask questions or comment about this privacy policy and our privacy practices, contact us at:
    <br>bd@usitrip.com
        <br><br><br><br><br><br>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/js/plugins.generic.js') }}"></script>
    <script>
      $(document).ready(function() {
        $('#hotelHead').html('{{$domain}}');
      });
    </script>
@endsection