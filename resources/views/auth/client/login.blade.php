@extends('layouts.master')

<?php
$isMobile = \Agent::isMobile();
$domain = $is_usitour ? 'Usitour' : 'Usitrip';
$title = $language === 0 ? '酒店预订平台 - 人人赚项目会员登入':$language === 2 ? '飯店預定平台 -人人賺項目會員登入':'Hotel - Affiliate Login';
?>

{{--single page title--}}
@if($language === 0)
    @section('title', $domain. $title)
@elseif($language === 2)
    @section('title', $domain. $title)
@else
    @section('title', $title. $domain)
@endif

@section('metas')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{$domain.$title}}">
    <meta name="author" content="{{$domain}} ">
    <meta name="Keywords" content="{{$domain.$title}}"/>
    <link href="@if($is_usitour&& $language !== 2){{asset('faviconen.ico')}}@else{{asset('favicon.ico')}}@endif" rel="shortcut icon">
    <link rel="canonical" href="{{ config('app.url') }}">
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
@endsection

{{--single page styles--}}
@section('css')
    <style>
        .loginColumns {
            max-width: 1200px;
            margin: 0 auto;
            padding: 100px 20px 20px 20px;
            z-index: 2;
        }
        .btn {
            border-radius: 0;
        }
        .btn-primary {
            background-color: #f1791e;
            border-color: #f1791e;
        }
        .btn-primary:hover {
            background-color: #e76c0e;
            border-color: #e76c0e;
        }
        .btn-login {
            display: -moz-inline-stack;
            display: inline-block;
            background: transparent;
            vertical-align: middle;
            text-align: center;
            color: #f1791e ! important;
            border-color: #f1791e;
            font-size: 13px;
            font-style: normal;
            border-radius: 1px;
        }
        .btn-login:hover {
            display: -moz-inline-stack;
            display: inline-block;
            background: #f1791e;
            vertical-align: middle;
            text-align: center;
            color: #fff ! important;
            border-color: #f1791e;
            font-size: 13px;
            font-style: normal;
            border-radius: 1px;
        }
        .login-intro h3 {
            font-size: 18px;
            margin: 25px 0 10px 0;
            color: #333333;
            font-weight: 600;
        }
        .login-intro p {
            color: #666666;
        }
        .login-intro-img {
            margin-top: -50px;
            margin-bottom: 30px;
        }
        .img-logo {
            margin: 10px auto;
        }
        .ibox-content {
            padding: 15px 20px 20px 20px;
            border-color: #e7eaec;
            border-width: 8px;
            border-style: solid;
        }
        .footer-text {
            font-size: 10px;
            font-weight: bold;
        }
        .login-text {
            float: left;
            font-size: 20px;
            margin-top: 5px;
            margin-bottom: 10px;
        }
        .login-line {
            width: 170px;
            float: left;
            text-align: right;
        }
        .has-error .form-control, .has-error .form-control:focus {
            border-color: #ed5565;
        }
        /*iPhone 6, 7, & 8 in portrait & landscape*/
        @media (min-width: 667px) {
            .col-md-5 {
                margin-top: -50px;
            }
        }
        @media (max-width: 667px) {
            .img-logo {
                margin: 10px auto;
            }
            .login-intro-img {
                margin-top: 20px;
            }
            .loginColumns {
                padding: 30px 20px 10px 20px;
            }
            .login-text {
                margin-top: 5px;
            }
            .login-line {
                width: 38%;
            }
            #en_topbar, #en_nav, #id_nav_head {
                display: none;
            }
            .stay-middle {
                width: 100% !important;
            }
            .register-style {
                margin-top: 15px;
                font-size: 14px;
            }
            .register-login {
                width: 90px;
                font-size: 11px;
            }
            img.img-responsive.img-logo {
                margin: 0 auto;
            }
            .footer-text {
                padding-right: 8vw;
            }
        }
        /* 20190306 may*/
        .text-reg {
            margin: 25px 0 10px 0;
        }
        form {
            margin-bottom: 30px;
        }
    </style>
@endsection

@section('content')
    @if(!$is_b && !$is_aa)
        @include('components.nav-bar')
    @endif
    <div class="loginColumns animated fadeInDown">
        <div class="row">
            {{--PC left: intro start--}}
            @if(!$isMobile)
                @include('components.affiliate-intro')
            @endif
            {{--PC left: intro end--}}

            {{-- login form strat--}}
            <div class="col-md-5">
                <div class="ibox-content">
                    @if($is_usitour)
                        <div class="col-xs-10 col-xs-offset-1">
                            <a href="{{url('/')}}">
                                @if($language == 2)
                                    <img src="/img/general/logo/logo-usitour-tw.png" class="img-responsive img-logo"/>
                                @else
                                    <img src="/img/general/logo/logo-usitour.png" class="img-responsive img-logo"/>
                                @endif
                            </a>
                        </div>
                    @else
                        <a href="{{url('/')}}">
                            <img src="/img/general/logo/logo-usitrip.png" class="img-responsive img-logo"/>
                        </a>
                    @endif
                    <form class="m-t" role="form" method="POST" action="{{ url('affiliate/login') }}">
                        {!! csrf_field() !!}
                        <p class="login-text">@lang('affiliate-login.login')</p>
                        {{--email--}}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control input-lg" name="email" value="{{ old('email') }}"
                                   placeholder="@lang('affiliate-login.email')" autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong style="color:#a94442; font-size:12px; font-weight:400;">{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        {{--password--}}
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control input-lg" name="password" placeholder="@lang('affiliate-login.password')">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong style="color:#a94442; font-size:12px; font-weight:400;">{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        {{--remember me--}}
                        <div class="form-group" style="margin-left:-20px !important;">
                            <div class="checkbox i-checks">
                                <label style="float:left"> <input type="checkbox" name="remember"> &nbsp; &nbsp; @lang('affiliate-login.remember_me')</label>
                            </div>
                            <div class="text-right">
                                <a href="{{ url('affiliate/password/reset') }}" style="margin-bottom:12px; "> @lang('affiliate-login.forgot_password')?</a>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block btn-lg btn-block">@lang('affiliate-login.affiliate_user_login')</button>
                        <p class="text-center text-reg">@lang('affiliate-login.no_account')?</p>
                        <a class="btn btn-block btn-default btn-login" href="/affiliate/register">@lang('affiliate-login.register_now')</a>
                    </form>
                </div>
            </div>
            {{--login form end--}}

            {{--mobile bottom: intro start--}}
            @if($isMobile)
                @include('components.affiliate-intro')
            @endif
            {{--mobile bottom: intro end--}}
        </div>
        <hr/>
    </div>
@endsection

{{--single page scripts--}}
@section('scripts')
    <script src="{{ asset('assets/js/plugins.generic.js') }}"></script>
    <script src="{{asset('assets/js/admin/plugins.affiliate.login.js')}}"></script>
    <script>
      $(document).ready(function() {

        $('.i-checks').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
        });

        $('#hotelHead').html('{{$domain}}');
      });

      @if(session('message'))
      swal('', "{{session('message')}}", 'warning');
        @endif
    </script>
@endsection