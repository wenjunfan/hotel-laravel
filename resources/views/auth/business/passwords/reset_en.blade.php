<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="117book - Global B2B Hotel Booking Platform">
    <meta name="author" content="">
    <meta name="Keywords" content="酒店预订，国际酒店，酒店B2B，酒店批发，酒店对接"/>

    <title>117book - Global B2B Hotel Booking Platform - Reset Password</title>

    <link href="{{asset('117book.ico')}}" rel="shortcut icon">

    <link rel="stylesheet" href="{{ asset('assets/css/plugins.generic.css') }}">

</head>
<style>
    .modal-static {
        position: fixed;
        top: 50% !important;
        left: 50% !important;
        margin-top: -100px;
        margin-left: -100px;
        overflow: visible !important;
    }
    .modal-static,
    .modal-static .modal-dialog,
    .modal-static .modal-content {
        width: 200px;
        height: 200px;
    }
    .modal-static .modal-dialog,
    .modal-static .modal-content {
        padding: 0 !important;
        margin: 0 !important;
    }
    .modal-static .modal-content .icon {
    }

</style>
<body class="white-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <a href="{{url('/')}}"><img style="height:50px; text-align:center;" src="/img/landing/Logo-EN.png"/></a>
        <br/> <br/>
        <img style="width:175px; float:left; margin-left:-30px; text-align:right; margin-top:10px;" src="/img/general/line.png"/>
        <p style="float:left; font-size:20px; margin-left:-16px; margin-top:28px;">Reset Password</p><img style="float:left; margin-left:110px;margin-right:-110px; margin-top:-50px; width:175px; text-align:right;" src="/img/general/line.png"/>

        <form class="m-t" role="form" method="POST" action="{{ url('/password/reset') }}" autocomplete="off">
            {!! csrf_field() !!}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" style="margin-top:90px;">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">

                @if ($errors->has('email'))
                    <span class="help-block">
<strong class="pull-left" style="color:#a94442; font-size:12px; font-weight:400;">Email is required</strong>
                    <!--strong>{{ $errors->first('email') }}</strong-->
						</span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password" placeholder="New Password" onclick="showRule()" autocomplete="new-password">

                @if ($errors->has('password'))
                    <span class="help-block">
			 <strong class="pull-left" style="color:#a94442; font-size:12px; font-weight:400;">Password is required</strong></br>
                    <!--strong>{{ $errors->first('password') }}</strong-->
						</span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password_confirmation" autocomplete="new-password" placeholder="Confirm new password">

                <span class="help-block" style="visibility:hidden;" id="passwordRule">
						   <strong style="color:#f1791e; font-size:12px; font-weight:300; line-height:1;">Contains 8+ characters with 2+ of the following: a-z, A-Z, 0-9, symbols.</strong><br>
                </span>
            </div>

            <button type="submit" data-toggle="modal" data-target="#processing-modal" class="btn btn-primary btn-block">Reset New Password</button>
        </form>
        <div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <img src="/img/general/loading-spinner.gif" class="icon" style="margin-left:-50px;"/>
                <h4>Processing...
                    <button type="button" class="close" style="float: none;" data-dismiss="modal" aria-hidden="true"></button>
                </h4>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/plugins.generic.js') }}"></script>

<script type="text/javascript">
    function showRule () {
        var visibility = document.getElementById('passwordRule').style.visibility;
        if (visibility == "hidden")
            document.getElementById('passwordRule').style.visibility = 'visible';
        else
            document.getElementById('passwordRule').style.visibility = 'hidden';
    }

    document.getElementById('mydiv').onclick = function (e) {
        showToggle();// call the function
    };

    @if (session('status'))
    swal({title: "", text: "{{session('status')}}", type: "success", html: true});
    @endif
</script>
</body>

</html>
