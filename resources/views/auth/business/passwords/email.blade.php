<!DOCTYPE html>
<html lang="zh-Hans">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="要趣订酒店B2B平台">
    <meta name="author" content="">
    <meta name="Keywords" content="酒店预订，国际酒店，酒店B2B，酒店批发，酒店对接"/>

    <title>要趣订酒店B2B预订平台 - 重设密码</title>

    <link href="{{asset('117book.ico')}}" rel="shortcut icon">

    <link rel="stylesheet" href="{{ asset('assets/css/plugins.generic.css') }}">

</head>
<style>
    .modal-static {
        position: fixed;
        top: 50% !important;
        left: 50% !important;
        margin-top: -100px;
        margin-left: -100px;
        overflow: visible !important;
    }
    .modal-static,
    .modal-static .modal-dialog,
    .modal-static .modal-content {
        width: 200px;
        height: 200px;
    }
    .modal-static .modal-dialog,
    .modal-static .modal-content {
        padding: 0 !important;
        margin: 0 !important;
    }
</style>
<body class="white-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <a href="{{url('/')}}"><img style="height:50px; text-align:center;" src="/img/landing/Logo-EN.png"/></a>
        <br/> <br/>
        <div>
            <img style="width:215px; float:left; margin-left:-35px; text-align:right;" src="/img/general/line.png"/>
            <p style="float:left; font-size:20px; margin-left:-16px; margin-top:25px;">重设密码</p>
            <img style="float:left;margin-left:40px;margin-right:-40px; margin-top:-63px; width:215px; text-align:right;" src="/img/general/line.png"/>
        </div>

        <form class="m-t" role="form" method="POST" action="{{ url('/password/email') }}">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" style="padding-bottom:20px;">

                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="邮箱">
                @if ($errors->has('email'))
                    <span class="help-block">
			</br>
                        <strong class="pull-left" style="color:#a94442; font-size:12px; font-weight:400;">邮箱不存在或输入有误</strong>
                    <!--strong>{{ $errors->first('email') }}</strong-->
						</span>
                @endif
                <span style="float:left; text-align:left;"><br/><medium>请输入与您的帐号关联的邮箱地址，我们将通过电子邮件给您发送重置密码的链接。</medium><br/></span>
            </div>

            <br/><br/>
            <button type="submit" data-toggle="modal" data-target="#processing-modal" class="btn btn-primary btn-block">
                <i class="fa fa-envelope"></i> 发送重置链接
            </button>
        </form>
        <div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <img src="/img/general/loading-spinner.gif" class="icon" style="margin-left:-50px;"/>
                <h4>处理中...
                    <button type="button" class="close" style="float: none;" data-dismiss="modal" aria-hidden="true"></button>
                </h4>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/plugins.generic.js') }}"></script>

<script type="text/javascript">
    @if (session('status'))
    swal({
        title: "",
        text: "{{session('status')}}",
        type: "success",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "OK",
    }, function () {
        window.location.href = "{{url('')}}";
    });
    @endif
</script>
</body>

</html>
