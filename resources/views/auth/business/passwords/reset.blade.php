<!DOCTYPE html>
<html lang="zh-Hans">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="要趣订酒店B2B平台">
    <meta name="author" content="">
    <meta name="Keywords" content="酒店预订，国际酒店，酒店B2B，酒店批发，酒店对接"/>

    <title>要趣订酒店B2B预订平台 - 重设密码</title>

    <link href="{{asset('117book.ico')}}" rel="shortcut icon">

    <link rel="stylesheet" href="{{ asset('assets/css/plugins.generic.css') }}">

</head>
<style>
    .modal-static {
        position: fixed;
        top: 50% !important;
        left: 50% !important;
        margin-top: -100px;
        margin-left: -100px;
        overflow: visible !important;
    }
    .modal-static,
    .modal-static .modal-dialog,
    .modal-static .modal-content {
        width: 200px;
        height: 200px;
    }
    .modal-static .modal-dialog,
    .modal-static .modal-content {
        padding: 0 !important;
        margin: 0 !important;
    }
    .modal-static .modal-content .icon {
    }

</style>
<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <h3>重设密码</h3>

        <p>请填写并记住您的新密码</p>

        <form class="m-t" role="form" method="POST" action="{{ url('/password/reset') }}">
            {!! csrf_field() !!}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="邮箱">

                @if ($errors->has('email'))
                    <span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password" placeholder="密码" onclick="showRule()" autocomplete="new-password">

                @if ($errors->has('password'))
                    <span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password_confirmation" autocomplete="new-password" placeholder="确认密码">
                <span class="help-block" style="visibility:hidden;" id="passwordRule">
						<strong style="color:#f1791e; font-size:12px; font-weight:300; line-height:1;">八个以上字符（2个以上 : a-z, A-Z, 0-9, 特殊符号）.</strong>

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
							<strong>{{ $errors->first('password_confirmation') }}</strong>
						</span>
                @endif
            </div>

            <button type="submit" data-toggle="modal" data-target="#processing-modal" class="btn btn-primary btn-block">重新设置</button>

        </form>
        <div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <img src="/img/general/loading-spinner.gif" class="icon" style="margin-left:-50px;"/>
                <h4>处理中...
                    <button type="button" class="close" style="float: none;" data-dismiss="modal" aria-hidden="true"></button>
                </h4>
            </div>
        </div>
        <p class="m-t">
            <small>要趣订酒店B2B平台 All rights reserved &copy; 2017</small>
        </p>
    </div>
</div>

<script src="{{ asset('assets/js/plugins.generic.js') }}"></script>

<script type="text/javascript">
    function showRule () {
        var visibility = document.getElementById('passwordRule').style.visibility;
        if (visibility == "hidden")
            document.getElementById('passwordRule').style.visibility = 'visible';
        else
            document.getElementById('passwordRule').style.visibility = 'hidden';
    }

    document.getElementById('mydiv').onclick = function (e) {
        showToggle();// call the function
    };

    @if (session('status'))
    swal({title: "", text: "{{session('status')}}", type: "success", html: true});
    @endif
</script>
</body>

</html>
