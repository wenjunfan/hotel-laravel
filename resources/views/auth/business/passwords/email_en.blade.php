<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="117book - Global B2B Hotel Booking Platform">
    <meta name="author" content="">
    <meta name="Keywords" content="Your Trusted Hotel Wholesaler"/>

    <title>117book - Global B2B Hotel Booking Platform - Reset Password</title>

    <link href="{{asset('117book.ico')}}" rel="shortcut icon">

    <link rel="stylesheet" href="{{ asset('assets/css/plugins.generic.css') }}">
</head>
<style>
    .modal-static {
        position: fixed;
        top: 50% !important;
        left: 50% !important;
        margin-top: -100px;
        margin-left: -100px;
        overflow: visible !important;
    }
    .modal-static,
    .modal-static .modal-dialog,
    .modal-static .modal-content {
        width: 200px;
        height: 200px;
    }
    .modal-static .modal-dialog,
    .modal-static .modal-content {
        padding: 0 !important;
        margin: 0 !important;
    }
</style>
<body class="white-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <a href="{{url('/')}}"><img style="height:50px; text-align:center;" src="/img/landing/Logo-EN.png"/></a>
        <div style="white-space: nowrap;margin-left: -20px;margin-top: 40px;">
            <img style="width:120px;" src="/img/general/line.png"/>
            <span style="font-size:20px;">Reset Password</span>
            <img style="width:120px;" src="/img/general/line.png"/>
        </div>

        <form class="m-t" role="form" method="POST" action="{{ url('/password/email') }}">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" style="padding-bottom:20px; padding-top:40px;">

                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                @if ($errors->has('email'))
                    <span class="help-block">
<strong class="pull-left" style="color:#a94442; font-size:12px; font-weight:400;">Email is required</strong>
                    <!--strong>{{ $errors->first('email') }}</strong-->

						</span>
                @endif
                <span style="float:left; text-align:left;"><br/><medium>Enter the email address associated with your account, and we'll email you a link to reset your password.  </medium><br/></span>

            </div>

            <br/><br/>
            <button type="submit" data-toggle="modal" data-target="#processing-modal" class="btn btn-primary btn-block" style="background-color:#f1791e;"><i class="fa fa-envelope"></i> Send Reset Link</button>
        </form>
        <div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <img src="/img/general/loading-spinner.gif" class="icon" style="margin-left:-50px;"/>
                <h4>Processing...
                    <button type="button" class="close" style="float: none;" data-dismiss="modal" aria-hidden="true"></button>
                </h4>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/plugins.generic.js') }}"></script>

<script type="text/javascript">
    @if (session('status'))
    swal({
        title: "",
        text: "{{session('status')}}",
        type: "success",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "OK",
    }, function () {
        window.location.href = "{{url('')}}";
    });
    @endif
</script>
</body>

</html>
