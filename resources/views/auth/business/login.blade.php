<!DOCTYPE html>
<html lang="zh-Hans">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="要趣订酒店B2B平台">
    <meta name="author" content="">
    <meta name="Keywords" content="酒店预订，国际酒店，酒店B2B，酒店批发，酒店对接"/>

    <title>要趣订酒店B2B预订平台 - 会员登录</title>

    <link href="{{asset('117book.ico')}}" rel="shortcut icon">

    <link rel="stylesheet" href="{{ asset('assets/css/plugins.generic.css') }}">

</head>
<style>
    .btn-login {
        display: -moz-inline-stack;
        display: inline-block;
        background: transparent;
        text-align: center;
        color: #f1791e ! important;
        border-color: #f1791e;
        font-size: 13px;
        font-style: normal;
        border-radius: 1px;
    }
    .btn-login:hover {
        display: -moz-inline-stack;
        display: inline-block;
        background: #f1791e;
        text-align: center;
        color: #fff ! important;
        border-color: #f1791e;
        font-size: 13px;
        font-style: normal;
        border-radius: 1px;
    }
    .login-text {
        float: left;
        font-size: 20px;
        margin-right: -5px;
        margin-top: 20px;
    }
    .login-line {
        width: 153px;
        float: left;
        text-align: right;
    }
    /*iPhone 6, 7, & 8 in portrait & landscape*/
    @media (max-width: 667px) {
        .login-text {
            margin-top: 12px;
        }
        .login-line {
            width: 43%;
        }
    }
</style>

<body class="white-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <a href="{{url('/')}}"><img style="height:50px; text-align:center;" src="img/landing/Logo-EN.png"/></a> <br/> <br/> <img class="login-line" src="/img/general/line.png"/>
        <p class="login-text">登录</p>
        <img class="login-line" src="/img/general/line.png"/>
        <form class="m-t" role="form" method="POST" action="{{ url('/login') }}">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="邮箱" autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
			<strong class="pull-left" style="color:#a94442; font-size:12px; font-weight:400;">{{ $errors->first('email') }}</strong>
						</span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password" placeholder="密码">

                @if ($errors->has('password'))
                    <span class="help-block">
			<strong class="pull-left" style="color:#a94442; font-size:12px; font-weight:400;">{{ $errors->first('password') }}</strong>
						</span>
                @endif
            </div>

            <div class="form-group pull-left" style="margin-left:-20px !important;">
                <div class="checkbox i-checks">
                    <label style="float:left"> <input type="checkbox" name="remember"> &nbsp; &nbsp;记住我 </label>

                </div>

            </div>
            <a href="{{ url('/password/reset') }}" style="margin-top:12px; " class="pull-right">
                <medium>忘记密码？</medium>
            </a>
            <button type="submit" class="btn btn-primary btn-block">会员登录</button>

            <p style="float:left; margin-top:12px; font-size:13px;" class=" text-center">
                <medium>还没有账号？</medium>
            </p>
            <a class="btn btn-sm btn-login pull-right" style="float:left; margin-top:5px; width:100px; " href="{{ url('/register') }}">注册</a>
        </form>
        <br/>
    </div>
</div>

<script src="{{ asset('assets/js/plugins.generic.js') }}"></script>

<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
    });

    @if(session('message'))
    swal("", "{{session('message')}}", "warning");
    @endif
</script>
</body>

</html>
