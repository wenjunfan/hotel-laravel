<!DOCTYPE html>
<html lang="zh-Hans">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="117book要趣订酒店B2B平台">
    <meta name="author" content="">
    <meta name="Keywords" content="酒店预订，国际酒店，酒店B2B，酒店批发，酒店对接"/>
    <title>117book要趣订酒店B2B预订平台 - 会员注册</title>
    <link href="{{asset('117book.ico')}}" rel="shortcut icon">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins.generic.css') }}">
</head>
<style>
    #noRobots{
        margin: 0 auto 10px;
        display: inline-flex;
    }
    .btn-login {
        color: #f1791e ! important;
        border-color: #f1791e;
        font-size: 13px;
        border-radius: 1px;
    }
    .btn-login:hover {
        background: #f1791e;
        color: #fff ! important;
        border-color: #f1791e;
        font-size: 13px;
        border-radius: 1px;
    }
    .modal-static {
        position: fixed;
        top: 50% !important;
        left: 50% !important;
        margin-top: -100px;
        margin-left: -100px;
        overflow: visible !important;
    }
    .modal-static,
    .modal-static .modal-dialog,
    .modal-static .modal-content {
        width: 200px;
        height: 200px;
    }
    .modal-static .modal-dialog,
    .modal-static .modal-content {
        padding: 0 !important;
        margin: 0 !important;
    }
    .login-text {
        float: left;
        font-size: 20px;
        margin-right: -5px;
        margin-top: 20px;
    }
    .login-line {
        width: 153px;
        float: left;
        text-align: right;
        margin-top: 5px;
    }
    /*iPhone 6, 7, & 8 in portrait & landscape*/
    @media (max-width: 667px) {
        .login-text {
            margin-top: 12px;
        }
        .login-line {
            width: 43%;
        }
    }
    .tipText{
        color:#a94442; font-size:12px; font-weight:400;
    }
    p.text-center.pull-left{
        margin-top: 12px;
    }
</style>

<body class="white-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <a href="{{url('/')}}"><img style="height:50px; text-align:center;" src="img/landing/Logo-EN.png" alt="logo"/></a> <br/> <br/>
        <img class="login-line" src="/img/general/line.png" alt="line"/>
        <p class="login-text">注册</p>
        <img class="login-line" src="/img/general/line.png" alt="line"/>
        <form class="m-t" role="form" method="POST" action="{{ url('/register') }}" autocomplete="off" id="2b-register-form" >
            {!! csrf_field() !!}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="用户名" autofocus required>
                @if ($errors->has('name'))
                    <span class="help-block">
			                <strong class="pull-left tipText" >用户名是必需的</strong>
                        </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="邮箱" required>
                @if ($errors->has('email'))
                    <span class="help-block">
		<strong class="pull-left tipText" >{{ $errors->first('email') }}</strong>
                        </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password" placeholder="密码" onfocus="document.getElementById('passwordRule').style.visibility = document.getElementById('passwordRule').style.visibility === 'hidden' ? 'visible' : 'hidden';" autocomplete="new-password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                            <strong class="pull-left tipText" >{{ $errors->first('password') }}</strong>
                        </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password_confirmation" placeholder="确认密码" autocomplete="new-password" required>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong class="pull-left tipText" >{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
                <span class="help-block" style="visibility:hidden;" id="passwordRule">
                     <strong class="tipText">至少8位，区分大小写，字母、数字、特殊符号中的两种或两种以上.</strong>
			    <br>
                </span>
            </div>

            <input type="checkbox" id="cb" class="pull-left"><span>我已经阅读并同意117Book的<a href="http://www.117book.com/tiaokuan0.html" target="_blank">使用条款</a>以及<a href="http://www.117book.com/tiaokuan.html" target="_blank">隐私政策</a></span></input><br><br>
            <div id="noRobots" class="g-recaptcha" data-sitekey="{{config('app.recaptcha_site_key')}}" data-callback="correctCaptcha"></div>
            <button  type="button" onclick="businessSignUp()" id="bt" disabled class="btn btn-primary btn-block" title="请确认您不是机器人">注册会员</button>
            <p class="text-center pull-left">
                <span>已经有账号？</span>
            </p>
            <a class="btn btn-sm btn-login pull-right"  href="{{ url('/login') }}">会员登录</a>
        </form>
    </div>
</div>
</body>
</html>
<script src="https://recaptcha.net/recaptcha/api.js"></script>
<script src="{{ asset('assets/js/plugins.generic.js') }}"></script>
<script src="{{ asset('assets/js/business/plugins.home.js') }}"></script>
<script>
    var correctCaptcha = function(response) {
        if(response.length !== 0){
            $('#bt').attr('disabled', false);
            $('#bt').attr('title','');
        }
    };
    function businessSignUp() {
        var isValid = true;
        var email = $('input[name="email"]').val();
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");

        $('input').filter('[required]:visible').each(function () {
            $(this).val($(this).val().trim());
            if ($(this).val() === '') {
                $(this).focus();
                isValid = false;
            }
        });

        if ($('input[name="name"]').val().trim() === '') {
            swal( '请填写用户名');
        } else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            swal( '请检查您的邮箱地址');
        } else if ($('input[name="password_confirmation"]').val() !== $('input[name="password"]').val()) {
            swal( '确认密码不相符');
        } else if (isValid === false) {
            swal( '请仔细检查所有的输入框');
        } else if (!document.getElementById('cb').checked) {
            swal( '请阅读并同意使用条款以及隐私政策');
        } else {
            FullscreenSpinner.create();
            $('#2b-register-form').submit();
        }
    }
</script>
