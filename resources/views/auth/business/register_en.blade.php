<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="117book - Global B2B Hotel Booking Platform">
    <meta name="author" content="">
    <meta name="Keywords" content="117book - Global B2B Hotel Booking Platform"/>
    <title>117book - Global B2B Hotel Booking Platform - Account Sign Up</title>
    <link href="{{asset('117book.ico')}}" rel="shortcut icon">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins.generic.css') }}">
</head>
<style>
    #noRobots{
        margin: 0 auto 10px;
        display: inline-flex;
    }
    .btn-login {
        display: -moz-inline-stack;
        display: inline-block;
        background: transparent;
        text-align: center;
        color: #f1791e ! important;
        border-color: #f1791e;
        font-size: 13px;
        font-style: normal;
        border-radius: 1px;
        margin-top: -20px;
    }

    .btn-login:hover {
        display: -moz-inline-stack;
        display: inline-block;
        background: #f1791e;
        text-align: center;
        color: #fff ! important;
        border-color: #f1791e;
        font-size: 13px;
        font-style: normal;
        border-radius: 1px;
    }

    .modal-static {
        position: fixed;
        top: 50% !important;
        left: 50% !important;
        margin-top: -100px;
        margin-left: -100px;
        overflow: visible !important;
    }

    .modal-static,
    .modal-static .modal-dialog,
    .modal-static .modal-content {
        width: 200px;
        height: 200px;
    }

    .modal-static .modal-dialog,
    .modal-static .modal-content {
        padding: 0 !important;
        margin: 0 !important;
    }

    .login-text {
        float: left;
        font-size: 20px;
        margin-right: -5px;
        margin-top: 15px;
    }

    .login-line {
        width: 143px;
        float: left;
        text-align: right;
    }

    /*iPhone 6, 7, & 8 in portrait & landscape*/
    @media (max-width: 667px) {
        .login-text {
            margin-top: 10px;
        }

        .login-line {
            width: 38%;
        }
    }
    .tipText{
        color:#a94442; font-size:12px; font-weight:400;
    }
    p.text-center.pull-left{
        margin-top: 10px;
    }
</style>
<body class="white-bg">
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <a href="{{url('/')}}">
            <img style="height:50px; text-align:center;" src="/img/landing/Logo-EN.png" alt="logo"/></a>
        <br/> <br/>
        <img class="login-line" src="/img/general/line.png" alt="line"/>
        <p class="login-text">Sign Up</p>
        <img class="login-line" src="/img/general/line.png" alt="line"/>
        <form class="m-t" role="form" method="POST" action="{{ url('/register') }}" autocomplete="off"  id="2b-register-form" >
            {!! csrf_field() !!}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Username" autofocus required>
                @if ($errors->has('name'))
                    <span class="help-block">
			        <strong class="pull-left tipText" >Username is required</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>
                @if ($errors->has('email'))
                    <span class="help-block">
		                <strong class="pull-left tipText" >{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password" placeholder="Password" onfocus="document.getElementById('passwordRule').style.display = document.getElementById('passwordRule').style.display === 'none' ? 'block' : 'none';" autocomplete="new-password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong class="pull-left tipText" >{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" autocomplete="new-password" required>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong class="pull-left tipText" >{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
                <span class="help-block" style="display:none;" id="passwordRule">
                    <strong class="tipText">Contains 8+ characters with 2+ of the following: a-z, A-Z, 0-9, symbols.</strong>
                </span>
            </div>
            <input style="margin-top:-10px; " type="checkbox" id="cb"> &nbsp;I have read and agree to the <a
                    href="http://www.117book.com/tiaokuan0.html" target="_blank">Terms of Use</a> and the <a
                    href="http://www.117book.com/tiaokuan.html" target="_blank">Privacy Policy</a>.</input>
            <br><br>
            <div id="noRobots" class="g-recaptcha" data-sitekey="{{config('app.recaptcha_site_key')}}" data-callback="correctCaptcha"></div>
            <button id="bt" disabled class="btn btn-primary btn-block"  type="button" onclick="businessSignUp()" title="Please confirm you are human">Sign Up</button>
            <p class="text-left float-left">
                <span>Already have an account?</span>
            </p>
            <a class="btn btn-sm btn-login pull-right" href="{{ url('/login') }}">Log In</a>
        </form>
    </div>
</div>
</body>
</html>
<script src="https://recaptcha.net/recaptcha/api.js"></script>
<script src="{{ asset('assets/js/plugins.generic.js') }}"></script>
<script src="{{ asset('assets/js/business/plugins.home.js') }}"></script>
<script>
    var correctCaptcha = function(response) {
        if(response.length !== 0){
            $('#bt').attr('disabled', false);
            $('#bt').attr('title', '');
        }
    };
    function businessSignUp() {
        var isValid = true;
        var email = $('input[name="email"]').val();
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");

        $('input').filter('[required]:visible').each(function () {
            $(this).val($(this).val().trim());
            if ($(this).val() === '') {
                $(this).focus();
                isValid = false;
            }
        });

        if ($('input[name="name"]').val().trim() === '') {
            swal( 'Username is needed');
        } else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            swal( 'Please enter valid Email');
        } else if ($('input[name="password_confirmation"]').val() !== $('input[name="password"]').val()) {
            swal( 'Please enter the same confirm password');
        } else if (isValid === false) {
            swal( 'All input is required');
        } else if (!document.getElementById('cb').checked) {
            swal( 'Please check our terms of use and private policy');
        } else {
            FullscreenSpinner.create();
            $('#2b-register-form').submit();
        }
    }
</script>
