<!DOCTYPE html>
<html lang="{{ $language === 0 ? 'zh-Hans' : $language === 2 ? 'zh-Hant': 'en' }}">
<head>
    {{--todo 需要重新写页面--}}
    <meta charset="UTF-8">
    <title>@if($is_b) 117book @elseif($is_usitour) Usitour Hotel @else Usitrip Hotel @endif - Voucher</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href=" @if($is_b) {{asset('117book.ico')}} @elseif($is_usitour && $user_language !== 2) {{asset('faviconen.ico')}} @else {{asset('favicon.ico')}} @endif" rel="shortcut icon">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    @if(config('app.enable_tracking') && !$is_b && !$is_aa)
        @php
            $ga_id = $is_usitour ? 'UA-90212184-1' : 'UA-110660251-1';
            $gtm_id = 'GTM-TG57PFZ';
        @endphp
        <!-- Google Tag Manager -->
        <script>
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','{{ $gtm_id }}');
        </script>
        <!-- End Google Tag Manager -->

        {{--google global site tag--}}
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-825564842"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'AW-825564842');
        </script>

        {{--google analytics--}}
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', '{{ $ga_id }}');
            ga('require', 'ec');
            ga('send', 'pageview');
        </script>
    @endif
    <link rel="stylesheet" href="{{ asset('assets/css/mobile/voucher.css') }}">
</head>
<style>
    * {
        font-family: Roboto, Arial, "Lucida Grande", "Microsoft Yahei", "Hiragino Sans GB", "Hiragino Sans GB W3", SimSun, STHeiti;
    }
    .fullscreen-spinner {
        background-color: black;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=40)";
        background-color: rgba(0, 0, 0, 0.4);
        position: fixed;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        z-index: 10000;
        padding-top: 100px;
    }
    .fullscreen-spinner > div {
        width: 18px;
        height: 18px;
        background-color: #333;
        border-radius: 100%;
        display: inline-block;
        -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
        animation: sk-bouncedelay 1.4s infinite ease-in-out both;
    }
    .fullscreen-spinner .sk-bounce1 {
        -webkit-animation-delay: -0.48s;
        animation-delay: -0.48s;
        color: #fff;
    }
    .fullscreen-spinner .sk-bounce2 {
        -webkit-animation-delay: -0.32s;
        animation-delay: -0.32s;
        color: #fff;
    }
    .fullscreen-spinner .sk-bounce3 {
        -webkit-animation-delay: -0.16s;
        animation-delay: -0.16s;
        color: #fff;
    }
    @-webkit-keyframes sk-bouncedelay {
        0%, 80%, 100% {
            -webkit-transform: scale(0)
        }
        40% {
            -webkit-transform: scale(1.0)
        }
    }
    @keyframes sk-bouncedelay {
        0%, 80%, 100% {
            -webkit-transform: scale(0);
            transform: scale(0);
        }
        40% {
            -webkit-transform: scale(1.0);
            transform: scale(1.0);
        }
    }
    .font18 {
        font-size: 18px;
    }
    .red-star {
        color: #e4393c;
    }
    .hover-more-phone {
        margin-top: 30px;
        margin-left: 80px;
    }
    #left-hotel-status {
        float: left;
        padding: 20px;
        color: #FFF;
        width: 350px;
    }
</style>
<body>
@if(config('app.enable_tracking') && !$is_b && !$is_aa)
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id={{ $gtm_id }}" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
@endif

<div class="container" style="width:1170px">
    <div class="row" style="width:1270px">
        @if($is_usitour)
            <div style="margin-left:15px; width:90%; padding: 21px 0; background-color: #005bac;">
                <div class="pull-left">
                @if($user_language ==2)
                        <img src="https://hotel.usitour.com/img/general/logo/logo-usitour-tw.png" alt="" style=" margin-left:15px;margin-top: -10px;"/>
                @else
                        <img src="https://www.usitour.com/tpl/en.usitrip.com/image/public/web_logo.png" alt="" style="margin-top:10px; margin-left:15px;"/>
                @endif
                </div>
                <div style="margin-left:850px;">
                    <div>
                        <img src="https://www.usitrip.com/tpl/en.usitrip.com/image/contact-phone.png" alt="" style="margin-top:15px;"/>
                    </div>
                </div>
            </div>
        @elseif($is_aa)
            <div style="margin-left:15px; width:90%; padding: 21px 0; background-color: #fff;">
                <div class="pull-left">
                    <img src="{{asset('img/partnerImgs/aaLogo.png')}}" alt="" style="margin-top:10px; margin-left:15px;"/>
                </div>
                <div style="margin-left:850px;">
                    <div>
                        <img src="{{asset('img/partnerImgs/aaPhone.png')}}" alt="" style="margin-top:15px;"/>
                    </div>
                </div>
            </div>
        @elseif($is_b)
            <div class="pull-left">
                <img height="85" width="230" src="{{asset('img/business/new-logo.png')}}" alt="" style="margin-top:10px;"/>
            </div>
            <div style="margin-left:780px;">
                <div>
                    @if ($status == 'ON REQUEST')
                    @elseif ($status)
                        <img src="{{asset('img/general/contact-phone-new.jpg')}}" alt="" style="margin-top:45px; height:55px;"/>
                    @endif
                </div>
            </div>
        @else
            <div style="float:left;">
                &nbsp;<img height="85" width="400" src="{{asset('img/partner/10028/logo.jpg')}}" alt="" style="margin-top:10px;"/>
            </div>
            <div style="margin-left:800px;">
                <div>
                    <img src="{{asset('img/general/contact-phone.png')}}" alt="7x24 hot line: 888-887-2816/25-6668-0241" class="hover-more-phone"/>
                </div>
            </div>
        @endif
    </div>
    <br><br>
    @if ($status == 'ON REQUEST')
        <large style="text-align:center">
            <p style="color:red; font-size:50px;">Booking Pending, Please Contact Us.</p>
            <p style="color:red; font-size:50px;">626-522-2906(weekday)</p>
            <p style="color:red; font-size:50px;">626-434-5201(weekend)</p>
            <p style="color:red; font-size:50px;">626-434-5267(weekend)</p>
        </large>
    @elseif ($status == 'CONFIRMED')
        <p align="center"><span style="font-size:25px">Prepaid Voucher</span></p>
        <p align="right">
            <button type="button" class="btn btn-primary ladda-button ladda-button-search" onclick="window.print()">
                Print
            </button>
        </p>
    @endif
    <div style="padding:0 15px;" class="row">
        @if ($status == 'ON REQUEST')
            <div id="left-hotel-status" style="background:#f1791e;">
                <br>
                <p align="center">
                    Reference Number </p>
                <p align="center">
                    <strong style="font-size:20px">
                        @if(count(explode(",",$orderPReference)) > 1 && explode(",",$orderPReference)[0] == ' ')
                            {{explode(",",$orderPReference)[1]}}
                        @else
                            {{explode(",",$orderPReference)[0]}}
                        @endif
                    </strong>
                </p>
                <p align="center">
                    Booking Status:<strong style="font-size:20px"> Pending</strong>
                </p>
            </div>
        @elseif ($status == 'CONFIRMED')
            <div id="left-hotel-status" style="background:#034ba0;">
                <br>
                <p align="center"> Reference Number </p>
                <p align="center">
                    <strong style="font-size:35px;word-wrap: break-word;">
                        @if(count(explode(",",$orderPReference)) > 1 && explode(",",$orderPReference)[0] == ' ')
                            {{explode(",",$orderPReference)[1]}}
                        @else
                            {{explode(",",$orderPReference)[0]}}
                        @endif
                    </strong>
                </p>
                <p align="center">
                    Booking Status:<strong style="font-size:20px"> Confirmed </strong>
                </p>
            </div>
        @elseif ($status == 'CANCELLED')
            <div id="left-hotel-status" style="background:pink; color:#000000">
                <br>
                <p align="center">
                    Booking Status </p>
                <p align="center">
                    <strong style="font-size:20px"> Cancelled </strong>
                </p>
                <p align="center">
                    Your refund is issued, please wait for your bank to process. </p>
            </div>
        @elseif ($status == 'REJECTED')
            <div id="left-hotel-status" style="background:orange; color:#000000">
                <br>
                <p align="center">
                    Booking Status </p>
                <p align="center">
                    <strong style="font-size:20px"> Rejected </strong>
                </p>
                <p align="center">
                    Please contact us for further instruction. </p>
            </div>
        @endif
        <div id="right-hotel-info" style="float:left; background:#e5f1ff; color:#000;width:790px; border:2px solid #e5f1ff; padding-left: 20px; padding-top: 20px;">
            <p style="text-align:left">
                <strong class="font18">{{$hotelName}}</strong> &nbsp; <span>
						@if($rating>=3)
                        @for ($i = 0; $i < $rating; $i++)
                            <i class="fa fa-star red-star" aria-hidden="true"></i>
                        @endfor
                    @endif
					</span>
            </p>
            <span>{{str_replace(" ,", "", $addr)}}<br>{{$hotelPhone? 'Phone: ' .$hotelPhone : ''}}</span> <br><br> <span style="font-size: 20px;">
                    Check in：<strong>@if($is_usitour) {{ substr($checkinDate, -5) }}
                    -{{ substr($checkinDate, 0, 4) }}  @else {{substr($checkinDate, -10)}} @endif</strong></span> <span style="font-size: 20px; margin-left: 60px;">Check out：<strong>
                        @if($is_usitour) {{ substr($checkoutDate, -5) }}
                    -{{ substr($checkoutDate, 0, 4) }}     @else {{substr($checkoutDate, -10)}} @endif
                    </strong></span>
        </div>
    </div>
    <br>
    @if ($status == 'ON REQUEST')
    @elseif ($status == 'CONFIRMED' || $status == 'CANCELLED' || $status == 'REJECTED')
        <div class="row" style="width:890px">
            &nbsp;&nbsp;&nbsp;&nbsp;<span class="font18">Number of Guests：{{$guestNo}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;Number of Nights：{{round($numDays)}} </span>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                @if ($status == 'ON REQUEST')
                @elseif ($status == 'CONFIRMED' || $status == 'CANCELLED'|| $status == 'REJECTED')
                    <tr>
                        <th bgcolor="#e5f1ff"><span class="font18">Guest Name</span></th>
                        <th bgcolor="#e5f1ff"><span class="font18">Room Type</span></th>
                        <th bgcolor="#e5f1ff"><span class="font18">Num of Rooms</span></th>
                        <th bgcolor="#e5f1ff"><span class="font18">Adults</span></th>
                        <th bgcolor="#e5f1ff"><span class="font18">Children</span></th>
                        <th bgcolor="#e5f1ff"><span class="font18">Board Type</span></th>
                    </tr>
                @endif
                <?php $index = 1 ?>
                @foreach ($rooms as $room)
                    <?php if($index == count($rooms))
                    {
                    ?>
                    @if ($status == 'ON REQUEST')
                    @elseif ($status == 'CONFIRMED' || $status == 'CANCELLED'|| $status == 'REJECTED')
                        <tr>
                            <td>
                                @foreach ($room['paxes'] as $pax)
                                    &nbsp;&nbsp;
                                    <span class="font18"> {{$pax['lastName']}}
                                        , {{$pax['firstName']}} @if ($pax['type'] == 'CH') (Child) @endif<br></span>
                                @endforeach
                            </td>
                            <td><span class="font18">{{$room['name']}} </span></td>
                            <td>
										<span class="font18">
											@if( $room_number == '')
                                                1
                                            @else
                                                {{$room_number}}
                                            @endif
										</span>
                            </td>
                            <td><span class="font18">{{$room['adult']}}</span></td>
                            <td><span class="font18">{{$room['children']}}</span></td>
                            <td><span class="font18">{{$room['boardName']}} </span></td>
                        </tr>
                    @endif
                    <br>
                    @if ($status == 'ON REQUEST')
                    @elseif ($status == 'CONFIRMED')
                        <tr>
                            <td colspan="7" bgcolor="#e5f1ff">
										<span class="font18">
										@if(isset($special_request) && $special_request != "" && $special_request != null)
                                                <strong>Special
												Request</strong>:&nbsp;&nbsp;{!! $special_request !!}     <br/>
                                            @endif
										</span>
                                @if(isset($mandatory_fees) && $mandatory_fees != '')
                                    <span class="font18"><strong>Due at Hotel</strong>: {{$currencyCode . ' ' .$mandatory_fees}}<br/>
										</span>
                                @endif
                                <span class="font18"><strong>Remarks</strong>:&nbsp;&nbsp;{!! $room['comments'] !!}
										</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" bgcolor='#fff' color="#fff">
                                <span class="font18"><strong>Notes to front desk</strong></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" bgcolor="#e5f1ff">
										<span class="font18">This is a prepaid reservation, <strong>DO NOT</strong> charge the
											guest(s) except for extras consumed at the hotel. Payment for the booking
											was made via @if($is_aa) America Asia Travel Center Inc's @else Unitedstar Intl Inc's @endif credit card or the booking is confirmed and paid through a 3rd party
											supplier. Please check for the reservation by guest name, arrival date or
											booking reference. <br/><br/>Please contact one of our offices should you
											have any questions/queries regarding this reservation.</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" bgcolor='#fff' color="#fff">
                                <span class="font18"><strong>Emergency contact</strong></span>
                            </td>
                        </tr>
                        <tr>
                            @if($is_aa)
                                <td colspan="6" bgcolor="#e5f1ff">
                                    <i class="fa fa-phone" aria-hidden="true"></i>&nbsp;
                                    626-571-2988/1-888-628-287
                                </td>
                                @else
                                <td colspan="1" bgcolor="#e5f1ff">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                                    @if($is_b)
                                        bd@117book.com
                                    @elseif($is_usitour)
                                        service@usitour.com
                                    @else
                                        service@usitrip.com
                                    @endif

                                </td>
                                <td colspan="1" bgcolor="#e5f1ff">
                                    <i class="fa fa-phone" aria-hidden="true"></i>&nbsp;
                                    @if($is_b)
                                        626-522-2906  /  626-434-5267
                                    @elseif($is_usitour)
                                        626-898-7658  /  626-434-5267
                                    @else
                                        626-898-7800  /  626-434-5267
                                    @endif
                                </td>
                                <td colspan="1" bgcolor="#e5f1ff">
                                    <i class="fab fa-weixin" aria-hidden="true"></i>&nbsp;usitripalex
                                </td>
                                <td colspan="3" bgcolor="#e5f1ff">
                                    <i class="fab fa-qq" aria-hidden="true"></i>&nbsp;2355652773
                                </td>
                                @endif

                        </tr>
                        <tr>
                            <td colspan="7" bgcolor='#fff' color="#fff">
                                <span class="font18"><strong>Voucher information</strong></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" bgcolor="#e5f1ff">
										<span class="font18">
											<strong>Thank you</strong>
											<p>Thank you for your booking. In conjunction with your voucher, the
												information below is designed to make sure your booking runs smoothly
												and you have a very enjoyable trip.</p>
											<strong>Your voucher</strong>
											<p>Present a copy of your Accommodation Voucher at the reception when you
												arrive.</p>
											<strong>Delayed arrival</strong>
											<p>If you are unable to arrive at your booked hotel on time (before check-in
												time), please notify us as soon as possible using the contact numbers
												listed above so that we can advise the property. Please quote your
												booking reference/ID so that we can assist you as quickly as
												possible.</p>
											<strong>Please note</strong>
											<p>No show will be charged in full. If you do wish to cancel a
												non-Refundable hotel services, you can contact us to cancel the service
												with no refund made.</p>
                                            @if($partnerId == config('app.partner_id'))
                                                <strong>Service With Fee</strong>
                                                <p>Please note, if you want to amend your main guest name before check-in via us, we would charge $25 per order as service fee, or you can contact Hotel directly for your request.</p>
                                            @endif
                                            @if(!$is_b)
                                                <strong>How to cancel my hotel reservation?</strong>
                                                <p>Please sign in your @if($is_b) 117book @elseif($is_usitour)
                                                        Usitour @elseif($is_aa) America Asia @else Usitrip @endif account first, then go to “My Orders”, click on “Hotel Orders” under “Order History” to cancel hotel reservations for refund. For the non-refundable bookings are no cancellations or changes possible.</p>
                                            @endif
                                            <i>
												<strong>We wish you a very enjoyable trip and thank you again for booking with us.
												</strong>
											</i>
											</span>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td colspan="7">
                            {!! csrf_field() !!}
                            @if ($status == 'ON REQUEST')
                            @elseif ($status == 'CONFIRMED')
                                <?php $flag = 0; ?>
                                <?php if(isset($room['cancelPolicies'])){ ?>
                                @foreach ($room['cancelPolicies'] as $i => $cancel)
                                    @if($i > 0)
                                        <br>
                                    @endif <span class="font18">
												<?php
                                        if (strtotime($cancel['end']) > strtotime(date('Y-m-d'))) {
                                            if ($cancel['amount'] == 0 && $flag == 0) {
                                                echo "&nbsp;&nbsp;Free cancellation before ";
                                            }
                                            if (isset($cancel['end'])) {
                                                echo substr(str_replace("T", " ", $cancel['end']), 0, 19);
                                            } else {
                                                echo substr(str_replace("T", " ", $cancel['from']), 0, 19);
                                            }
                                            if ($cancel['amount'] == 0 && $flag == 0) {
                                                echo " (Local hotel time)";
                                                if ($status == 'CONFIRMED' && $partnerId == config('app.partner_id') && $is_usitour) {
                                                    echo '<span id="cancel_$orderReference"><a class="btn btn-danger" style="float:right" onclick="cancelOrder(\''.$orderReference.'\', \'PO\')">Cancel Order</a></span>';
                                                }
                                                $flag = 1;
                                            } else {
                                                echo "Non-Refundable (If you choose to change or cancel this booking you will not be refunded any of the payment.)";
                                            }
                                        } else {
                                            echo "Non-Refundable (If you choose to change or cancel this booking you will not be refunded any of the payment.)";
                                        }
                                        ?>
											</span>
                                @endforeach
                                <?php }?>
                                <br>
                                <strong style="font-size:18px"> All information on the hotel voucher is non-changeable and non-transferable. </strong>
                                <br>
                            @endif
                            <address>
                                <span style="font-size:18px">
                                    @if ($status == 'ON REQUEST')
                                    @elseif ($status == 'CONFIRMED')
                                        <br><br>
                                    @endif
                                    @if($is_usitour)
                                          <strong>www.usitour.com</strong><br>
                                
                                    @elseif($is_b)
                                        <strong>&nbsp;www.117book.com</strong><br>
                                    @elseif($is_aa)
                                        <strong>&nbsp;www.supervacation.net</strong><br>
                                    @else
                                        <strong>&nbsp;www.usitrip.com</strong><br>
                                    @endif
                                        @if($is_aa)
                                    117 E Garvey Ave., Monterey Park, CA 91755 U.S.A
                                        @else
                                    17870 Castleton St. Suite 358-388, City of Industry, CA, 91748. <br>
                                        @endif
                                        Booking ID：<strong> {{$id}}</strong><br>
                                        Booking Date： {{$created_at}}<br>

                                </span>

                            </address>
                        </td>
                    </tr>
                    <?php
                    };
                    $index += 1;
                    ?>
                @endforeach

            </table>
        </div>
    </div>

</div>
</body>
<script src="/assets/js/plugins.voucher.js"></script>
<script type="text/javascript">
    var shouldTrack = Number('{{ $should_track && !$is_aa }}'); // to decide weather should track

    $(function () {
        var leftHeight = $('#left-hotel-status').height();
        $('#right-hotel-info').css('height', leftHeight + 40);
        var options = JSON.parse('<?= $options ?>');
        if (options) {
            var anaData = {
                'id': options.id,
                'name': options.hotelName,
                'roomName': options.roomName,
                'country': options.country,
                'city': options.city,
                'affiliate': options.source,
                'price': options.netPrice,
                'tax': options.tax,
                'roomCount': options.roomCount,
                'dayCount': options.dayCount,
                'brand': options.brand
            };
            onBookComplete(anaData);
        }
    });

    function cancelOrder (id) {
        swal({
            title: 'Are you sure you want to cancel this order?',
            text: 'Please check the cancellation policy carefully before you cancel this order',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Yes',
            cancelButtonColor: 'red',
            confirmButtonColor: '#007FF8',
            confirmButtonText: 'No',
            closeOnConfirm: true
        }, function (isConfirm) {
            if (!isConfirm) {
                click = 1;
                FullscreenSpinner.create();
                $.post("{{url('/order/cancel')}}", {
                    '_token': $('meta[name="csrf_token"]').attr('content'),
                    'id': id
                }, function (data) {
                    if (data.hasOwnProperty('options')) {
                        var options = data.options;
                        if (options) {
                            onCancelComplete(options);
                        }
                    }

                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal({
                            title: 'You have cancelled this order successfully',
                            text: data.message,
                            type: 'success'
                        }, function () {
                            location.reload();
                        });
                    } else if (data.type === 'SESSION_TIMEOUT') {
                        swal({
                            title: 'please refresh this page',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {
                            location.reload();
                        });
                    } else {
                        swal('Cancellation failed, please contact customer service', '', 'error');
                    }
                });
            }
        });
    }

    var FullscreenSpinner = function () {
        return {
            create: function () {
                $('body').
                  append(
                    '<div class="fullscreen-spinner" style="text-align:center"><div class="sk-bounce1"></div> <div class="sk-bounce2"></div><div class="sk-bounce3"></div> </div>');

                resizeSpinner($(window).height());
            },

            destroy: function () {
                $('.fullscreen-spinner').remove();
            }
        };
    }();

    function resizeSpinner (height) {
        var topOffs = (height - 32) / 2;
        $('.fullscreen-spinner').css('padding-top', topOffs + 'px');
    }

    $(window).bind('resize', function () {
        resizeSpinner($(this).height());
    });
</script>
</html>
