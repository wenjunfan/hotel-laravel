{{--b端ExtraPay添加信用卡--}}
@extends( !$is_b  ?  'layouts.master' : 'layouts.master-b' )

@section('title', 'Credit Card Management')

@section('css')
<link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

@endsection

@section('content')
<div class="row animated fadeInRight">
    <div class="col-lg-4 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
        @if (is_null($currentCreditCard))
			添加信用卡
        @else
			修改信用卡
		@endif
            </div>
            <div>
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" method="POST" id="creditform" action="{{ url('/creditcards') }}">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="number">卡号</label>

                            <div class="col-sm-9">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="number" value="{{is_null($currentCreditCard) ? '' : $currentCreditCard->number}}" placeholder="信用卡卡号"/>
                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="cardType">信用卡类别</label>

                            <div class="col-sm-9 form-inline">
                                <select id="cardType" name="cardType" class="form-control">
                                    <option value="Visa" {{(!is_null($currentCreditCard) && $currentCreditCard->cardType == 'Visa') ? 'selected="selected"' : ''}}>Visa</option>
                                    <option value="MasterCard" {{(!is_null($currentCreditCard) && $currentCreditCard->cardType == 'MasterCard') ? 'selected="selected"' : ''}}>MasterCard</option>
                                    <option value="Discover" {{(!is_null($currentCreditCard) && $currentCreditCard->cardType == 'Discover') ? 'selected="selected"' : ''}}>Discover</option>
                                    <!--<option value="Amex" {{(!is_null($currentCreditCard) && $currentCreditCard->cardType == 'Amex') ? 'selected="selected"' : ''}}>American Express</option>-->
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="expirationDate">过期日期</label>

                            <div class="col-sm-9 form-inline">
                                <select id="month" name="month" class="form-control">
                                    <option value="1" {{(!is_null($currentCreditCard) && $currentCreditCard->month == 1) ? 'selected="selected"' : ''}}>1月</option>
                                    <option value="2" {{(!is_null($currentCreditCard) && $currentCreditCard->month == 2) ? 'selected="selected"' : ''}}>2月</option>
                                    <option value="3" {{(!is_null($currentCreditCard) && $currentCreditCard->month == 3) ? 'selected="selected"' : ''}}>3月</option>
                                    <option value="4" {{(!is_null($currentCreditCard) && $currentCreditCard->month == 4) ? 'selected="selected"' : ''}}>4月</option>
                                    <option value="5" {{(!is_null($currentCreditCard) && $currentCreditCard->month == 5) ? 'selected="selected"' : ''}}>5月</option>
                                    <option value="6" {{(!is_null($currentCreditCard) && $currentCreditCard->month == 6) ? 'selected="selected"' : ''}}>6月</option>
                                    <option value="7" {{(!is_null($currentCreditCard) && $currentCreditCard->month == 7) ? 'selected="selected"' : ''}}>7月</option>
                                    <option value="8" {{(!is_null($currentCreditCard) && $currentCreditCard->month == 8) ? 'selected="selected"' : ''}}>8月</option>
                                    <option value="9" {{(!is_null($currentCreditCard) && $currentCreditCard->month == 9) ? 'selected="selected"' : ''}}>9月</option>
                                    <option value="10" {{(!is_null($currentCreditCard) && $currentCreditCard->month == 10) ? 'selected="selected"' : ''}}>10月</option>
                                    <option value="11" {{(!is_null($currentCreditCard) && $currentCreditCard->month == 11) ? 'selected="selected"' : ''}}>11月</option>
                                    <option value="12" {{(!is_null($currentCreditCard) && $currentCreditCard->month == 12) ? 'selected="selected"' : ''}}>12月</option>
                                </select>
                                <select id="year" name="year" class="form-control">
                                    <option value="2020" {{(!is_null($currentCreditCard) && $currentCreditCard->year == 2020) ? 'selected="selected"' : ''}}>2020年</option>
                                    <option value="2021" {{(!is_null($currentCreditCard) && $currentCreditCard->year == 2021) ? 'selected="selected"' : ''}}>2021年</option>
                                    <option value="2022" {{(!is_null($currentCreditCard) && $currentCreditCard->year == 2022) ? 'selected="selected"' : ''}}>2022年</option>
                                    <option value="2023" {{(!is_null($currentCreditCard) && $currentCreditCard->year == 2023) ? 'selected="selected"' : ''}}>2023年</option>
                                    <option value="2024" {{(!is_null($currentCreditCard) && $currentCreditCard->year == 2024) ? 'selected="selected"' : ''}}>2024年</option>
                                    <option value="2025" {{(!is_null($currentCreditCard) && $currentCreditCard->year == 2025) ? 'selected="selected"' : ''}}>2025年</option>
                                    <option value="2026" {{(!is_null($currentCreditCard) && $currentCreditCard->year == 2026) ? 'selected="selected"' : ''}}>2026年</option>
                                    <option value="2027" {{(!is_null($currentCreditCard) && $currentCreditCard->year == 2027) ? 'selected="selected"' : ''}}>2027年</option>
                                    <option value="2028" {{(!is_null($currentCreditCard) && $currentCreditCard->year == 2028) ? 'selected="selected"' : ''}}>2028年</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="CVC">验证码</label>

                            <div class="col-sm-9 form-inline">
                                <input type="text" class="form-control" name="CVC" value="{{is_null($currentCreditCard) ? '' : $currentCreditCard->CVC}}" placeholder="CVC" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="firstName">姓名</label>

                            <div class="col-sm-9 form-inline">
                                <input type="text" class="form-control" name="firstName" value="{{is_null($currentCreditCard) ? '' : $currentCreditCard->firstName}}" placeholder="名字" />
                                <input type="text" class="form-control" name="lastName" value="{{is_null($currentCreditCard) ? '' : $currentCreditCard->lastName}}" placeholder="姓氏" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="street">地址</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="street" value="{{is_null($currentCreditCard) ? '' : $currentCreditCard->street}}" placeholder="街道地址" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="street2">地址2</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="street2" value="{{is_null($currentCreditCard) ? '' : $currentCreditCard->street2}}" placeholder="单元或公寓号" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="city">城市</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="city" value="{{is_null($currentCreditCard) ? '' : $currentCreditCard->city}}" placeholder="城市名" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="state">州/省</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="state" value="{{is_null($currentCreditCard) ? '' : $currentCreditCard->state}}" placeholder="州或省名" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="zip">邮编</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="zip" value="{{is_null($currentCreditCard) ? '' : $currentCreditCard->zip}}" placeholder="邮编" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="countryCode">国家</label>

                            <div class="col-sm-9 form-inline">
                                <select id="countryCode" name="countryCode" class="form-control">
                                    <option value="US" {{(!is_null($currentCreditCard) && $currentCreditCard->countryCode == 'US') ? 'selected="selected"' : ''}}>美国</option>
                                    <option value="CH" {{(!is_null($currentCreditCard) && $currentCreditCard->countryCode == 'CH') ? 'selected="selected"' : ''}}>中国</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <label> <input type="checkbox" value="1" name="defaultCard" class="i-checks" {{(!is_null($currentCreditCard) && $currentCreditCard->defaultCard == 1) ? 'checked="checked"' : ''}}> 设为默认支付 </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6">
                                <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-save"></i> 保存</button>
                            </div>
                            <div class="col-sm-6">
                                <button class="btn btn-warning btn-block" type="reset">重置</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                信用卡列表
            </div>
            <div>
                <div class="ibox-content">
                <?php $pos = 0; ?>
                @foreach($creditCards as $creditCard)
                    @if ($pos > 0 && $pos % 3 == 0)
                    </div>
                    @endif

                    @if ($pos % 3 == 0)
                    <div class="row">
                    @endif
                        <div class="col-md-4">
                            <div class="payment-card {{(!is_null($currentCreditCard) && $currentCreditCard->id == $creditCard->id) ? 'default-card' : ''}}" onclick="editCard({{$creditCard->id}})">
                                <div class="row">
                                    <div class="col-sm-6">
                                    @if ($creditCard->cardType == 'Visa')
                                    <i class="fab fa-cc-visa payment-icon-big text-success"></i>
                                    @elseif ($creditCard->cardType == 'MasterCard')
                                    <i class="fab fa-cc-mastercard payment-icon-big text-warning"></i>
                                    @elseif ($creditCard->cardType == 'Discover')
                                    <i class="fab fa-cc-discover payment-icon-big text-danger"></i>
                                    @elseif ($creditCard->cardType == 'Amex')
                                    <i class="fab fa-cc-amex payment-icon-big text-navy"></i>
                                    @endif
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <button type="button" class="btn btn-xs btn-danger" onclick="deleteCard({{$creditCard->id}})">删除</button>
                                    @if ($creditCard->defaultCard)
                                        <br>
                                        <span class="label label-warning">默认支付</span>
                                    @endif
                                    </div>
                                </div>
                                <h2>
                                    **** **** **** {{substr($creditCard->number, 12)}}
                                </h2>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <small>
                                            <strong>有效期:</strong> {{$creditCard->month}}/{{$creditCard->year}}
                                        </small>
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <small>
                                            <strong>姓名:</strong> {{$creditCard->firstName}} {{$creditCard->lastName}}
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php ++$pos; ?>
                @endforeach
                    @if ($pos > 0)
                    </div>
                    @endif
                </div>
                
                <div class="col-sm-6">

              
              <a href="{{url('/creditcards')}}"><i class="btn btn-primary btn-block">添加新的信用卡</i></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/plugins/validate/jquery.validate.min.js') }}"></script>

<script type="text/javascript">
    jQuery(function($) {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $("#creditform").validate({
            rules: {
                number: {
                    required: true,
                    minlength: 16,
                    maxlength: 16,
                    number: true
                },
                CVC: {
                    required: true,
                    minlength: 3,
                    maxlength: 4,
                    number: true
                },
                firstName: {
                    required: true
                },
                lastName: {
                    required: true
                },
                street: {
                    required: true,
                    minlength: 6
                },
                city: {
                    required: true,
                    minlength: 2
                },
                state: {
                    required: true,
                    minlength: 2
                }
            }
        });
    });

    function editCard(id) {
        self.location = "{{url('/creditcards')}}/" + id;
    }

    function deleteCard(id) {
        event.cancelBubble = true;
        if (event.stopPropagation) event.stopPropagation();

        $.ajax({
            method: "DELETE",
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}" },
            url: "{{url('/creditcards')}}/" + id,
            data: null,
            success: function(data) {
                if (data.success) {
                    swal({
                        title: "删除成功",
                        text: "",
                        type: 'success'
                    }, function() {
                        self.location = "{{ url('/creditcards') }}";
                    });
                }
                else {
                    swal(data.message);
                }
            },
            error: function (xhr, e) {
                swal(xhr.status + " " + e);
            },
        });
    }

    @if (session('message'))
        swal({
            title: "{{ session('message') }}",
            text: "",
            type: 'success'
        });
    @endif
    </script>
@endsection
