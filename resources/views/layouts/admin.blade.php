{{--b端admin和c端affiliate模板--}}

<!DOCTYPE html>
<html lang="{{ $language === 0 ? 'zh-Hans' : $language === 2 ? 'zh-Hant': 'en'  }}">
<?php
$domain =  $is_b? "117book" : $is_usitour? 'Usitour' : 'Usitrip'
?>
    <head>
        <meta charset="utf-8">
        <title> @yield('title') - @if($is_b) 117book要趣订B2B酒店预定平台 @else Unitedstars International Ltd. @endif </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta name="description" content="@if($is_b) 117book要趣订B2B酒店预定平台 @else Unitedstars International Ltd. @endif">
        <meta name="author" content="">
        <meta name="Keywords" content="@if($language == 0)  酒店预订，国际酒店，酒店，酒店批发，酒店对接 @else Hotel booking, international hotels, hotel whole sales, direct connect hotels @endif"/>
        <meta name="csrf_token" content="{{ csrf_token() }}">
        @if($should_track && !$is_aa)
            @php
                $ga_id = $is_usitour ? 'UA-90212184-1' : 'UA-110660251-1';
                $gtm_id = $is_usitour ? 'GTM-TG57PFZ' : 'GTM-M4NZ46D';
            @endphp

            <!-- Google Tag Manager -->
            <script>
                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','{{ $gtm_id }}');
            </script>
            <!-- End Google Tag Manager -->

            {{--google global site tag--}}
            <script async src="https://www.googletagmanager.com/gtag/js?id=AW-825564842"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag() {dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', 'AW-825564842');
            </script>

            {{--google analytics--}}
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                ga('create', '{{ $ga_id }}');
                ga('require', 'ec');
                ga('send', 'pageview');
            </script>
        @endif
        <link rel="shortcut icon" href=" @if($is_b) {{asset('117book.ico')}} @else {{asset('favicon.ico')}} @endif">
        <link rel="stylesheet" href="{{asset('assets/css/admin/plugins.admin.css')}}">
        {{--SINGLE PAGE STYLES--}}
        @yield('css')
    </head>

    <body class="mini-navbar">
        @if(config('app.enable_tracking') && !$is_b && !$is_aa)
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ $gtm_id }}"
                              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
        @endif
        <div id="wrapper">
            {{--admin left nav start--}}
            @include('components.nav-admin')
            {{--admin left nav end--}}
            <div id="page-wrapper" class="white-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-left">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#"><i class="fa fa-bars"></i>{{$language == 0 ? "菜单" : "Menu"}}</a>
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
					<span class="m-r-sm text-muted"><span class="welcome-message">{{$language == 0 ? "您好" : "Hi, "}}</span>{{ Auth::user()->name }}
                        <span class="welcome-message">{{$language == 0 ? "，欢迎您使用走四方酒店分销平台" : ", welcome to our affiliate partnership platform"}}</span></span>
                            </li>
                            <li>
                                <a href="@if(!$is_b) {{ url('affiliate/logout') }} @else {{ url('/logout') }} @endif ">
                                    <i class="fa fa-sign-out-alt"></i>{{$language == 0 ? "安全退出" : "Sign out"}}  </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="wrapper wrapper-content  animated fadeInRight">
                    @yield('content')
                </div>
                <div class="footer">
                    <div>
                        <strong>{{$is_b? "117book" : $is_usitour? 'Usitour' : 'Usitrip'}}</strong> All rights reserved &copy; 2008 - {{ \Carbon\Carbon::now()->year }}
                    </div>
                </div>
            </div>
        </div>

        <script src="{{asset('assets/js/admin/plugins.admin.js')}}"></script>

        {{--admin main stript--}}
        <script type="text/javascript">
            window.usitrip = {
                csrfToken: '{{ csrf_token() }}',
                basePath: '{{ url('/') }}/'
            };

            // get url params
            window.getUrlVars = function getUrlVars() {
                var vars = {};
                var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                    vars[key] = value;
                });

                return vars;
            };

            $(document).ready(function () {
                /* Check width on page load*/
                if ($(window).width() < 767) {
                    $("body").removeClass("mini-navbar");
                }
                $('.navbar-minimalize').click(function () {
                    if ($('body').attr('class').indexOf('mini-navbar') != -1) {
                        // $('nav.navbar-static-top').css('width', '88%');
                    } else {
                        // $('nav.navbar-static-top').css('width', '95%');
                    }
                });

            });
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        </script>

        {{--single page scripts--}}
        @yield('scripts')
    </body>
</html>