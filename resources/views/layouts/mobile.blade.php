<!DOCTYPE html>
<html lang="{{ $language === 0 ? 'zh-Hans' : $language === 2 ? 'zh-Hant': 'en' }}">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    {{--<link href=" @if($is_b) {{asset('117book.ico')}} @else {{asset('favicon.ico')}} @endif" rel="shortcut icon">--}}
    <link href="@if($is_b){{asset('117book.ico')}}@elseif($is_usitour&& $language !== 2){{asset('faviconen.ico')}}@else{{asset('favicon.ico')}}@endif" rel="shortcut icon">
    {{--for ios safari--}}
    <meta name="apple-mobile-web-app-capable" content="yes">
    {{--隐藏状态栏--}}
    <meta name="apple-mobile-web-app-status-bar-style” content=black"/>
    {{--指定的iphone中safari顶端的状态条的样式--}}
    <meta content="black" name="apple-mobile-web-app-status-bar-style"/>
    @yield('metas')

    @if($should_track && !$is_aa)
        @php
            $ga_id = $is_usitour ? 'UA-90212184-1' : 'UA-110660251-1';
            $gtm_id = $is_usitour ? 'GTM-TG57PFZ' : 'GTM-M4NZ46D';
            $register_con_label = $is_usitour ? 'AW-825564842/udvxCLzvgoYBEKq91IkD' : 'AW-825564842/M4IDCIfcxXoQqr3UiQM';
        @endphp

        <!-- Google Tag Manager -->
        <script>
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','{{ $gtm_id }}');
        </script>
        <!-- End Google Tag Manager -->

        {{--google global site tag--}}
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-825564842"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'AW-825564842');
        </script>

        {{--google analytics--}}
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', '{{ $ga_id }}');
            ga('require', 'ec');
            ga('send', 'pageview');
        </script>

        <!-- Event snippet for 注册 conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
        <script>
            function gtag_report_conversion_register (url) {
                var callback = function () {
                    if (typeof(url) != 'undefined') {
                        window.location = url;
                    }
                };
                gtag('event', 'conversion', {
                    'send_to': '{{ $register_con_label }}',
                    'value': 1.0,
                    'currency': 'USD',
                    'event_callback': callback
                });
                return false;
            }
        </script>
    @endif

    @yield('gtag_events')

    {{--plugins styles--}}
    <link rel="stylesheet" href="{{ asset('assets/css/mobile/plugins.mobile.css') }}">

    {{--global styles--}}
    <link rel="stylesheet" href="{{ asset('assets/css/mobile/app.css') }}">

    {{--single page styles--}}
    @yield('css')
</head>

<body>
@if(config('app.enable_tracking') && !$is_b && !$is_aa)
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id={{ $gtm_id }}" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
@endif

@if(!$is_b)
    @include('components.site-notices')
@endif

<div id="app" v-cloak>
    <div class="content-wrapper">
        {{--导航：非酒店关键词，功能性Ui可以使用vue 组件--}}
        <top-nav></top-nav>

        {{-- main content --}}
        <div class="main-content">
            @yield('content')
        </div>

        {{--hotel pics--}}
        <share-links :hide="hideLinks" v-if="!hideLinks" @closelinks="closeLinksPop"></share-links>
        <button @click="hideLinks = false" v-if="langStr === 'en'" :class="isUsitour ? 'share-btn-usitour' : 'share-btn-usitrip'"></button>
        <back-top></back-top>

        @if(!$is_aa)
            <footer-nav></footer-nav>
        @endif
    </div>
</div>

{{--js plugins: jquery,moment--}}
<script src="{{ asset('assets/js/mobile/plugins.mobile.js') }}"></script>

{{--js global vars from laravel--}}
@include('includes.global-vars')

@if(!$is_aa)
    @if(config('app.env') === 'production')
        {{--meiqia online service--}}
        <script type='text/javascript'>
            {{--when click on chat button, send to ga to track:  only show meiqia for customer not 117--}}
            if (window.location.hostname.indexOf('117book') < 0) {
                function setupMeiqiaEvent () {
                    $('#meiqia-container iframe[name="button"]').on('click', function () {
                        onClickChat();
                    })
                }
                (function (m, ei, q, i, a, j, s) {
                    m[i] = m[i] || function () {
                        (m[i].a = m[i].a || []).push(arguments)
                    };
                    j = ei.createElement(q),
                      s = ei.getElementsByTagName(q)[0];
                    j.async = true;
                    j.charset = 'UTF-8';
                    j.src = 'https://static.meiqia.com/dist/meiqia.js?_=t';
                    s.parentNode.insertBefore(j, s);
                })(window, document, 'script', '_MEIQIA');
                @if($language == 1)
                _MEIQIA('entId', 105738);
                _MEIQIA('language', 'en');
                @elseif(!$is_aa)
                _MEIQIA('entId', 103909);
                // 不指定其他客服
                _MEIQIA('fallback', 1);
                // 指定多个客服
                _MEIQIA('assign', {
                    agentToken: '922c24eb45cc9a1f71272b6d63951a1e,98ced06ca64b68c54760092e64730347,03b1d2b76498e93f2d707cb0287b7122,54b6236c5156f756990f13c5ef9a4730,a424142b479d24355ed1946b5d51305a,' +
                    '6e0c1e72337420bbe0eac86a80b0da18,41a355ea543a7efb12c0de9c9a3b6aeb,a63905ad1cb5d936e297fa7b1b40270c,65f5d705ef59752d5e186715a6d850a9,2d3d6a5c7de424430498a6e98d132f56,40c6ad501747b0007076c1be946d4e1a,' +
                    '576c223952056a516c8c1ccdd857510a,2858ae422e6ab947d00ca94e8b12789d,89a0c8d79fa4d8b2fc0443371801b948'
                });
                @endif
                _MEIQIA('allSet', setupMeiqiaEvent)
            }
        </script>
    @endif
    {{--infusionsoft tracking code--}}
    <script type="text/javascript" src="https://zw451.infusionsoft.com/app/webTracking/getTrackingCode" async defer></script>
@endif

{{--single page scripts--}}
@yield('scripts')
</body>
</html>
