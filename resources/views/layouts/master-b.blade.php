{{--b端client页面pc版模板--}}
<?php
$optionName = isset($optionName) ? $optionName : '';
function optionActive($optionName, $name)
{
    return (isset($optionName) && $optionName == $name) ? 'active' : '';
}
?>

<!DOCTYPE html>
<html lang="{{ $language === 0 ? 'zh-Hans' : $language === 2 ? 'zh-Hant': 'en' }}">
    <head>
        <meta charset="utf-8">
        <title>117book - @yield('title')</title>
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta name="description" content="117Book">
        <meta name="author" content="">
        <meta name="Keywords" content="Your Trusted B2B hotel booking platform"/>
        <meta name="csrf_token" content="{{ csrf_token() }}">
        <link href="{{asset('117book.ico')}}" rel="shortcut icon">
        <link rel="stylesheet" href="{{asset('assets/css/pc/plugins.master-b.css')}}">
        {{--SINGLE PAGE STYLES--}}
        @yield('css')
    </head>
    <body>
        {{--back to top--}}
        <div id="backtotop">
            <div class="fixed">
                <a href="#page-wrapper" class="btn btn-success btn-lg"><i class='fa fa-arrow-circle-up'></i></a>
            </div>
        </div>
        {{--/back to top--}}
        <div id="wrapper">
            {{--admin left nav start--}}
            @if(Auth::check())
                @include('components.nav-business')
            @endif
            {{--admin left nav end--}}
            {{--top nav start--}}
            <div id="page-wrapper">
                {{--top nav--}}
                <div class="row">
                    <nav class="navbar navbar-static-top white-bg nav-business-top" role="navigation">
                        <div class="navbar-left">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#">
                                <i class="fa fa-bars"></i>
                                @lang('master.menu')
                            </a>
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <img src=" @lang('master.flag')" style="margin-top:-3px;">
                            </li>
                            <li>
                                <select id="Lang" name="Lang" class="mlang" onchange="updateLanguage(this.id)" Style="background-color:transparent; border: transparent !important;">
                                    <option value="0">@lang('master.schinese')</option>
                                    <option value="1">@lang('master.english')</option>
                                    <option value="2">@lang('master.tchinese')</option>
                                </select>
                            </li>
                            <li>
                                <span class="m-r-sm mname text-muted welcome-message">&nbsp;&nbsp;{{Auth::check() ? Auth::user()->name: '' }}<!--，@lang('master.welcome')--></span>
                            </li>
                            <li class="logout-wrapper">
                                <a href="{{ url('/logout') }}">
                                    <i class="fa fa-sign-out-alt"></i>
                                    <span class="logoutmini">@lang('master.logout1')</span>
                                    <span class="logoutlarge">@lang('master.logout')</span>
                                </a>
                            </li>
                        </ul>

                    </nav>
                </div>
                {{--/top nav--}}
                <div class="animated fadeInRight">
                    @yield('content')
                </div>
            </div>
            {{--top nav end--}}
        </div>
        {{--footer--}}
        <footer>
            <p style="text-align:center;">
                Copyright &copy; 2008 - {{\Carbon\Carbon::now()->year}} || All Rights Reserved . 117book.com
            </p>
        </footer>

        <!-- Custom and plugin javascript -->
        <script src="{{asset('assets/js/pc/plugins.master-b.js')}}"></script>

        {{--js global vars from laravel--}}
        @include('includes.global-vars')

        {{--js business layout--}}
        @include('includes.js-business')

        {{--single page scripts--}}
        @yield('scripts')
    </body>
</html>