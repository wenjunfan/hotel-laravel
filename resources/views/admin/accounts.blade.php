@extends('layouts.admin')
@section('title', '账户列表')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/admin/plugins.orders.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/admin/plugins.accounts.css') }}">
    <style>
        [v-cloak]{
            display: none;
        }
        .ibox-content{
            padding: 15px 0 0 0;
        }
        th, td {
            padding: 6px 5px !important;
            word-wrap: break-word;
            text-align: center;
        }
        td {
            font-size: 12px;
        }
        .th-1{
            width: 42px !important;
        }
        .th-3, .th-15{
            width: 120px !important;
        }
        .th-6{
            width: 32px !important;
        }
        .th-9{
            width: 60px !important;
        }
        .th-10{
            width: 70px !important;
        }
        .th-7, .th-11{
            width: 130px !important;
        }
        .th-17{
            width: 50px !important;
        }
        .processcommend{
            border-style: none;
            border-color:Transparent;
            background-color:transparent;
            resize: none;
            box-shadow: none;
            overflow: auto;
            outline: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            cursor: pointer;
            text-align:left;
        }
         textarea#op_remark {
            width: 100%;
            border: none;
            font-size: 14px;
            resize: none;
	        cursor:pointer;
            display:flex;
         }
    </style>
@endsection

@section('content')
	<div class="row animated fadeInRight" id="accountApp" v-cloak>
		<div class="col-lg-8 col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-noborder">
					<strong>账户查询</strong>
                    <button v-if="adminLevel == 1"  class="btn btn-info" @click="updateAllUserFinanceRecords">一鍵更新用戶消費狀況</button>
				</div>
                <div class="ibox-content" style="padding-bottom: 0;">
                    <div class="form-horizontal">
                        {!! csrf_field() !!}
                        {{--line1 关键词搜索--}}
                        <div class="form-group" style="margin-bottom: 0;">
                            <label class="col-sm-1 control-label" style="padding-left: 0; padding-right: 0;">关键词搜索</label>
                            {{--关键词搜搜--}}
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button" id="keywordType">@{{ keywordText }}<span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li :class="{ active: (keywordType === '1') }">
                                                <a @click="keywordType = '1', keywordText='用户名/公司名/联系人'">用户名/公司名/联系人</a>
                                            </li>
                                            <li :class="{ active: (keywordType === '2') }">
                                                <a @click="keywordType = '2', keywordText='用户id'">用户id</a>
                                            </li>
                                            <li :class="{ active: (keywordType === '3') }">
                                                <a @click="keywordType = '3', keywordText='电话'">电话</a>
                                            </li>
	                                        <li :class="{ active: (keywordType === '5') }">
		                                        <a @click="keywordType = '5', keywordText='邮箱'">邮箱</a>
	                                        </li>
                                            <li class="divider"></li>
                                            <li :class="{ active: (keywordType === '4') }">
                                                <a @click="keywordType = '4', keywordText='佣金比例'">佣金比例</a>
                                            </li>
                                            <li :class="{ active: (keywordType === '6') }">
                                                <a @click="keywordType = '6', keywordText='用户权限'">用户权限</a>
                                            </li>
                                            <li :class="{ active: (keywordType === '7') }">
                                                <a @click="keywordType = '7', keywordText='用户国家'">用户国家</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <input type="text" class="form-control" name="keyword" v-model="keyword" ></div>
                            </div>
                            {{--付款方式--}}
                            <label class="col-sm-1 control-label">付款方式</label>
                            <div class="col-sm-2">
                                <select class="form-control" name="accountType" id="accountType" v-model="payType">
                                    <option value="all">All</option>
                                    <option value="prepay">Prepay</option>
                                    <option value="postpay">Postpay</option>
                                </select>
                            </div>
                            {{--重置--}}
                            <div class="col-sm-3">
                                <a class="btn btn-default btn-block" type="reset" href="/admin/accounts" style="background-color: #f0f0f0;">重置</a>
                            </div>
                        </div>
                        {{--line2 创建日期--}}
                        <div class="form-group" style="margin-bottom: 10px;">
                            <label class="col-sm-1 control-label" style="padding-left: 0; padding-right: 0;" for="from">创建日期</label>
                            <div class="col-sm-2">
                                <div class="input-group date">
                                    <input id="startDate" name="from" v-model="dateStart" type="text" class="form-control" placeholder="年-月-日">
                                    <span class="input-group-addon" style="padding-right:0px;"><i class="fa fa-long-arrow-alt-right"></i></span>
                                </div>
                            </div>
                            <div class="col-sm-2" style="padding-left:0px;">
                                <div class="input-group date btn-block">
                                    <input id="endDate" name="to" type="text" v-model="dateEnd" class="form-control" placeholder="年-月-日">
                                </div>
                            </div>
                            {{--账户状态--}}
                            <label class="col-sm-1 control-label">账户状态</label>
                            <div class="col-sm-2">
                                <select class="form-control" name="accountStatus" id="accountStatus" v-model="accountStatus">
                                    <option value="2">All</option>
                                    <option value="1">已激活</option>
                                    <option value="0">未激活</option>
                                </select>
                            </div>
                            {{--search button--}}
                            <div class="col-sm-3">
                                <button class="btn btn-primary btn-block" id="btn-search-filter" @click="search">
                                    <i class="fa fa-search"></i> 搜索
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
        <div class="col-lg-4" style="color: #f1791e">
            <h3>用户权限</h3>
            <br><span style="color:orange">-超级：改付款方式/添加查看供应商权限/设置用户为分销用户以及以下</span>
            <br><span style="color:red">-财务：查看营业执照以及以下(财务需审核ip为美国的用户信用卡填写情况)</span>
            <br><span style="color:purple;">-BDD：改渠道负责人添加持卡人姓名和后四位/改公司名称、电话、佣金比例/更改账户IP</span>
            <br><span style="color:darkgreen;">-BD：激活账户/查看营业执照/填写用户进程</span>
            <br><span style="color:#428bca;">-普通：无权限（只能改自己的账户信息，如公司电话和公司名称用户名)</span>
            <span class="text-danger" v-if="adminLevel === '1'"><br>(开通c端分销用户不用激活，点蓝色按钮开通自动分配追踪码给该用户)</span>
        </div>
        {{--start list--}}
		<div class="col-lg-12 col-md-12">
			<div class="ibox float-e-margins">
				<div>
					<div class="ibox-content">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTable">
								<thead>
								<tr>
				                                    {{--1--}}
									<th class="th-1">ID</th>
				                                    {{--2--}}
									<th class="th-2">用户名</th>
				                                    {{--3--}}
									<th class="th-3">公司名称</th>
				                                    {{--4--}}
									<th class="th-4">邮箱(不可改) / 电话</th>
				                                    {{--5--}}
									<th class="th-5">联系人</th>
				                                    {{--6--}}
									<th class="th-6">佣金</th>
				                                    {{--7--}}
									<th class="th-6">消費总额</th>
                                    <th class="th-6">财务未审核到账</th>
				                                    {{--8--}}
									<th class="th-8">付款<br>方式</th>
				                                    {{--9--}}
									<th class="th-9">账户状态</th>
				                                    {{--10--}}
									<th class="th-10">创建日期</th>
				                                    {{--12--}}
									<th class="th-12">查看供应<br>商权限</th>
				                                    {{--13--}}
									<th class="th-13">渠道负责人</th>
				                                    {{--14--}}
									<th class="th-14">持卡人姓名&cc后四位</th>
				                                    {{--15--}}
									<th class="th-15">用户进程备注</th>
				                                    {{--16--}}
									<th class="th-16" v-if="adminLevel === '1' || adminLevel === '3'">营业<br>执照</th>
				                                    {{--17--}}
									<th class="th-17" v-if="adminLevel === '1' || adminLevel === '3'">公司<br>税号</th>
				                                    {{--11--}}
                                    <th class="th-11">操作</th>
								</tr>
								</thead>
								<tbody>
								@foreach($users as $user)
									<tr class="gradeX">
										<td>
                                            {{$user->id}}
                                        </td>
                                        {{--用户名--}}
										<td>
                                            {{$user->name}}<br/>
                                            <a target="_blank"href="/admin/orders?partnerId={{$user->id}}&hotelName=&from=&to=&checkin_from={{substr($user->created_at, 0, 10)}}&checkin_to={{date('Y-m-d', strtotime('+365 day'))}}&keywords=&orderStatus=6&financeStatus=5">看订单</a>
                                        </td>
                                        {{--公司名称--}}
										<td>
											<textarea readonly id="company_name{{$user->id}}" name="company_name{{$user->id}}" rows="2" cols="15" maxlength="50" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($user->company_name)){{$user->company_name}}@endif</textarea>
                                            <button v-if="bdd || adminLevel === '1'" name="roEdit" class="label btn"
                                                    id="roEditBtn{{$user->id}}"
                                                    @click="fixUser1('{{$user->id}}')">
												<i class="fa fa-edit"></i></button>
											<button name="roSave" id="roSaveBtn{{$user->id}}" class="label btn hidden" style="margin-top:2px;" @click="fixUser('{{$user->id}}')">
												保存更新
											</button>
										</td>
                                        {{--email, phone--}}
										<td>{{$user->email}}<br/>
											<textarea readonly id="company_phone{{$user->id}}" name="company_phone{{$user->id}}" rows="2" cols="15" maxlength="30" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($user->company_phone)){{$user->company_phone}}@endif</textarea>
											<button v-if="bdd || adminLevel === '1'" name="roEdit1" class="label btn
											pull-right" id="roEditBtn1{{$user->id}}" @click="fixUser2('{{$user->id}}')">
												<i class="fa fa-edit"></i></button>
											<button name="roSave1" id="roSaveBtn1{{$user->id}}" class="label btn hidden" style="margin-top:2px;" @click="fixUsera('{{$user->id}}')">
												保存更新
											</button>
										</td>
                                        {{--联系人--}}
										<td>{{$user->contact_person}}</td>
                                        {{--佣金比例--}}
										<td>{{$user->priceRatio}}</td>
                                        {{--消費总额--}}
										<td>{{$user->balance}}</td>
                                        {{--财务未审核到账--}}
                                        <td>
                                            @if($user->postpay_unpaid != 0)
                                                {{$user->postpay_unpaid}}
                                            @else
                                                0
                                            @endif
                                        </td>
                                        {{--付款方式--}}
										<td>
                                            @if($user->paymentType === 'Prepay')
                                                <span class="text-success"><i class="fa fa-credit-card bigger-160"></i><br>PrePay</span>
                                            @else
                                                <span class="text-info"><i class="fa fa-money-bill-alt bigger-160"></i><br>PostPay</span>
                                            @endif
                                        </td>
                                        {{--账户状态--}}
										<td>
											@if ($user->active)
												<span class="text-info">已激活</span>
											@else
												<span class="text-muted">未激活</span>
											@endif
                                            @if($user->admin === 0 )
                                                    <p style="color:#428bca;">普通</p>
                                            @endif
                                            @if($user->admin === 1 )
                                                    <p  style="color:orange">超级</p>
                                            @endif
                                            @if($user->admin === 2 )
                                                    <p style="color:red">财务</p>
                                            @endif
                                            @if($user->admin === 3 )
                                                @if($user->id === 11313)
                                                    <p style="color:purple">BDD</p>
                                                @else
                                                    <p style="color:darkgreen">BD</p>
                                                @endif
                                            @endif
                                            @if($user->admin === 4 )
                                                    <p  style="color:hotpink">分销</p>
                                            @endif
                                            @if($user->admin === 5 )
                                                    <p style="color:deeppink">新分销申请</p>
                                            @endif
										</td>
                                        {{--创建日期--}}
										<td>{{substr($user->created_at, 0, 10)}}</td>
                                        {{--查看供应商权限--}}
										<td>
										<div v-if="adminLevel === '1'">
                                            @if($user->see_provider == 'false')
                                                <button @click="seeProvider('{{$user->id}}', '{{$user->see_provider}}')" class="btn btn-warning btn-xs" type="button">
                                                    添加权限
                                                </button>
                                            @else
                                                <button @click="seeProvider('{{$user->id}}', '{{$user->see_provider}}')" class="btn btn-danger btn-xs" type="button">
                                                    取消权限
                                                </button>
                                            @endif
                                        </div>
										<div v-else>
                                            {{$user->see_provider}}
                                        </div>
										</td>
                                        {{--渠道负责人--}}
										<td>
											<div v-if="bdd || adminLevel === '1'">
                                                <textarea readonly id="channel{{$user->id}}" name="channel{{$user->id}}" rows="2" cols="3" maxlength="50" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">
                                                    @if(isset($user->channel)){{$user->channel}}@endif
                                                </textarea>
                                                <button name="roEdit2" class="label btn" id="roEditBtn2{{$user->id}}" @click="fixUser3('{{$user->id}}')">
                                                    <i class="fa fa-edit"></i></button>
                                                <button name="roSave2" id="roSaveBtn2{{$user->id}}" class="label btn hidden" style="margin-top:2px;" @click="fixUserb('{{$user->id}}')">
                                                    保存更新
                                                </button>
                                            </div>
                                            <div v-else>
                                                <a id="channel{{$user->id}}" name="channel{{$user->id}}">
                                                    {{ $user->channel }}
                                                </a>
                                            </div>
										</td>
                                        {{--持卡人姓名--}}
										<td>
                                            <div v-if=" bdd || adminLevel === '1' || adminLevel === '2' ">
                                                <textarea readonly id="cardholder{{$user->id}}" name="cardholder{{$user->id}}" rows="2" cols="10" maxlength="20" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($user->cardholder)){{$user->cardholder}}@endif</textarea>
                                                <button name="roEdit3" class="label btn" id="roEditBtn3{{$user->id}}" @click="fixUser4('{{$user->id}}')">
                                                    <i class="fa fa-edit"></i></button>
                                                <button name="roSave3" id="roSaveBtn3{{$user->id}}" class="label btn hidden" style="margin-top:2px;" @click="fixUserc('{{$user->id}}')">
                                                    保存更新
                                                </button>
                                            </div>
                                             <div v-else>
                                                 <a id="cardholder{{$user->id}}" name="cardholder{{$user->id}}">
                                                     {{ $user->cardholder }}
                                                 </a>
                                             </div>
										</td>
                                        {{--用户进程备注--}}
										<td>
                                            <textarea readonly title="{{$user->remark}}" id="remark{{$user->id}}" name="remark{{$user->id}}" rows="4" cols="20" maxlength="1000" class="processcommend">@if(isset($user->remark)){{$user->remark}}@endif
                                            </textarea>
                                            <i v-if="adminLevel === '1' || adminLevel === '3' " class="fa fa-2x fa-edit
                                            pull-right  text-success" @click="openOpCommendModal({{$user->id}})"></i>
                                        </td>
                                        {{--营业执照--}}
										<td v-if="adminLevel === '1' || adminLevel === '3'">
                                            <a href="{{ url('profile/files') . '/' . $user->id }}" target="_blank" style="text-decoration: underline;">@if($user->files == '')上传@else管理@endif文件</a>
										</td>
                                        {{--公司税号--}}
										<td v-if="adminLevel === '1' || adminLevel === '3' ">
                                               {{$user->tax}}
										</td>
                                        {{--操作--}}
                                        <td>
                                            <button @click="changeCommission('{{$user->id}}', '{{$user->priceRatio}}')" class="btn btn-info btn-xs" type="button">
                                                佣金比例
                                            </button>
                                            <div v-if="adminLevel === '1'  || adminLevel === '3'">
                                                @if ($user->active)
                                                    <button @click="deactiveAccount('{{$user->id}}')" class="btn btn-danger btn-xs" type="button">
                                                        取消激活
                                                    </button>
                                                @else
                                                    @if(\Auth::user()->id !== 11313 && $user->Country_Code=='US' && \Auth::user()->admin === 3 )
                                                             请联系BDD激活
                                                    @else
                                                          <button @click="activeAccount('{{$user->id}}','{{$user->company_name === "" ? "false" : "true"}}')" class="btn btn-warning btn-xs" type="button">
                                                             激活账户
                                                         </button>                                                                                                                                               
                                                    @endif
                                                @endif
                                            </div>
                                            <div v-if="adminLevel === '1'|| bdd">
                                                @if($user->Country_Code=='US')
                                                    <button @click="changeCountry_Code('{{$user->id}}','CN')" class="btn btn-warning btn-xs" type="button">
                                                        美国用户
                                                    </button>
                                                @else
                                                    <button @click="changeCountry_Code('{{$user->id}}','US')" class="btn btn-danger btn-xs" type="button">
                                                        中国用户
                                                    </button>
                                                @endif
                                            </div>
                                            <div v-else>
                                                {{$user->Country_Code === 'CN' ? '中国用户':'美国用户'}}
                                            </div>
                                            <button v-if="adminLevel === '1'" @click="changePaymentType
                                            ('{{$user->id}}', '{{$user->paymentType}}')" class="btn btn-primary btn-xs" type="button">
                                                付款方式
                                            </button>
                                            @if(Auth::user()->admin === 1)
                                                @if($user->admin === 0 || $user->admin === 5 )
                                                    <button @click="addAffiliate('{{$user->id}}')" class="btn btn-success btn-xs" type="button">
                                                        设置为分销账户
                                                    </button>
                                                @elseif($user->admin === 4)
                                                    <button @click="removeAffiliate('{{$user->id}}')" class="btn btn-success btn-xs" type="button">
                                                        解除分销账户
                                                    </button>
                                                @endif
                                            @endif
                                        </td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
        {{-- start add op comment Modal--}}
        <div class="modal fade" id="addOpCommentModal" tabindex="-1" role="dialog" aria-labelledby="AddOpCommentModalTitle"
             data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">添加订单 <span id="addCommentModalTitle" class="text-warning"></span>OP进程备注</h4>
                    </div>
                    <div class="modal-body">
                        <textarea rows="5" readonly id="op_remark"></textarea>
                        <hr>
                        <div id="add-area"><textarea name="" id="addComment" cols="90" rows="6" style="resize: none;  width:100%;"></textarea></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button class="btn btn-primary" @click="updateOpCommend()">提交</button>
                    </div>
                </div>
            </div>
        </div>
        {{--end add op comment Modal--}}
        {{--end list--}}
	</div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/js/admin/plugins.orders.js') }}"></script>
    {{--single page scripts--}}
    <script>
        var adminLevel =  '{!! \Auth::user()->admin !!}';
        var userId = '{!! \Auth::id() !!}';
        var bdd = userId === '11313';
        {{--get params from url vars and pass to vue data--}}
        var urlParams = window.getUrlVars();
        var dateStart = urlParams.creationStart === undefined ? "{!! $creationStart !!}" : urlParams.creationStart;
        var dateEnd = urlParams.creationEnd === undefined ? "{!! $creationEnd !!}" : urlParams.creationEnd;
        var keyword = urlParams.keyword === undefined ? "" : decodeURI(urlParams.keyword);
        var keywordType = urlParams.keywordType === undefined ? 0 : decodeURI(urlParams.keywordType);
        var payType = urlParams.payType === undefined ? 'all' : urlParams.payType;
        var accountStatus = urlParams.accountStatus === undefined ? 2 : urlParams.accountStatus;
    </script>
    <script src="{{ asset('assets/js/admin/plugins.accounts.js') }}"></script>
@endsection
