@extends('layouts.admin')

@section('title', '团房请求')

@section('css')
    <link href="{{ asset('assets/css/admin/plugins.orders.css') }}" rel="stylesheet">
    {{--single page styles--}}
    @include('admin.includes.css-group-request')
@endsection

@section('content')
	<div class="row animated fadeInRight">
		@include('components.group-request-form')
		<div class="col-lg-8 col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					团房请求
				</div>
				<div>
					<div class="col-sm-8 ibox-content">
						<form class="form-horizontal" id="search-form" name="search-form" role="form">
							{!! csrf_field() !!}
							<div class="form-group">
								<label class="col-sm-2 control-label">申请时间：</label>
								<div class="col-sm-5">
									<div class="input-group date">
										<input class="form-control datepicker" id="from" name="from" value="{{$from}}" type="text" placeholder="年-月-日">
										<span class="input-group-addon">
											<i class="fa fa-long-arrow-alt-right"></i>
										</span>
									</div>
								</div>
								<div class="col-sm-5">
									<div class="input-group date btn-block">
										<input class="form-control datepicker" id="to" name="to" value="{{$to}}" type="text" placeholder="年-月-日">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">是否跟进: </label>
								<div class="col-sm-3">
									<select class="form-control" id="isFollow" name="isFollow" value="{{$isFollow}}">
										<option class="form-control" selected value="all">
											全部
										</option>
										<option class="form-control" value="0">
											否
										</option>
										<option class="form-control" value="1">
											是
										</option>
									</select>
								</div>
								<label class="col-sm-2 control-label">是否成单：</label>
								<div class="col-sm-3">
									<select class="form-control" id="processStatus" name="processStatus" value="{{$select_value}}">
										<option class="form-control" selected value="all">
											全部
										</option>
										<option class="form-control" value="0">
											否
										</option>
										<option class="form-control" value="1">
											是
										</option>
									</select>
								</div>
								<div class="col-sm-2">
									<button class="btn btn-top btn-block" onclick="search()" style="font-size:13px !important;" type="button">
										<i class="fa fa-search"></i> 查询
									</button>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3" >
									<a class="btn btn-primary btn-block" style="font-size:13px !important; margin-top：20px;" data-toggle="modal" data-target="#myModalHorizontal">
										<i class="fa fa-plus"></i>新增
									</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12 col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					团房请求列表
				</div>
				<div>
					<div class="ibox-content">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover dataTables-groupRequests" >
								<thead>
								<tr>
								        <th style="width:55px;text-align:center">编号</th>
									<th style="width:55px;text-align:center">申请日期</th>
									<th style="width:255px;text-align:center">团组需求内容</th>
									<th style="width:255px;text-align:center">联系人信息</th>
									<th style="text-align:center">处理内容</th>
									<th style="width:55px;text-align:center">是否跟进</th>
									<th style="width:55px;text-align:center">是否成单</th>
									<th style="width:55px;text-align:center">订单金额</th>
									<th style="width:55px;text-align:center">处理人</th>
								</tr>
								</thead>
								<tbody>
								@if (count($groupRequests) > 0)
									@foreach($groupRequests as $groupRequest)
										@if ($groupRequest->destination_one != "" && $groupRequest->checkin_one != "")
											<tr class="gradeX">
												<td>
													{{$groupRequest->id}}
												</td>
												<td>
													{{$groupRequest->created_at}}
												</td>
												<td>
													<span style="color:#f1791e">目的地：</span> {{$groupRequest->destination_one}}</br>
													<span style="color:#f1791e">日期：</span>{{$groupRequest->checkin_one}} - {{$groupRequest->checkout_one}}</br>
													@if ($groupRequest->destination_two != "")
														<span style="color:#f1791e">备选目的地：</span> {{$groupRequest->destination_two}}</br>
													@endif
													@if ($groupRequest->checkin_two != "0000-00-00")
														<span style="color:#f1791e">备选日期：</span>{{$groupRequest->checkin_two}} - {{$groupRequest->checkout_two}}</br>
													@endif
													<span style="color:#f1791e">团体编号：</span>{{$groupRequest->groupnumber}}</br>
													<span style="color:#f1791e">每晚客房数量：</span>{{$groupRequest->roomnumber}}
													<span style="padding-left:30px;"><span style="color:#f1791e">星级：</span>{{$groupRequest->rate}}</span> </br>
													<span style="color:#f1791e">理想的每晚预算：</span>${{$groupRequest->budget}}/night
												</td>
												<td>
													联系人姓名： {{$groupRequest->contactname}}</br>
													公司名称：{{$groupRequest->companyname}}</br>
													电子邮件：{{$groupRequest->email}}</br>
													电话：{{$groupRequest->phone}} </br>
													备注：{{$groupRequest->remark}}
												</td>
												<td>
													<a href="#" style="text-decoration:none; color:black;" class="tooltip-test" title="{{$groupRequest->solution}}">
														<textarea readonly id="solution{{$groupRequest->id}}" rows="4" cols="80" maxlength="1000" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($groupRequest->solution)){{$groupRequest->solution}}@endif</textarea></a>
													<button name="roEdit" class="label btn" type="button" id="roEditBtn{{$groupRequest->id}}" data-toggle="modal" data-target="#myModal-{{$groupRequest->id}}" data-whatever="@if(isset($groupRequest->id)){{$groupRequest->id}}@endif">
														<i class="fa fa-edit"></i>
													</button>
													<!--Modal start-->
													<form method="post" action="{{URL::to('/admin/groupRequests')}}">
														<div class="modal fade" id="myModal-{{$groupRequest->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
															<div class="modal-dialog" role="document" style="padding-top:70px">
																<div class="modal-content">
																	<div class="modal-header">
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																		<h4>新进程：</h4>
																	</div>
																	<div class="modal-body">
																		<textarea autofocus id="new-solution-{{$groupRequest->id}}" name="new-solution-{{$groupRequest->id}}" rows="4" cols="80" maxlength="1000" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;"></textarea>
																		<button name="roSave" id="roSaveBtn{{$groupRequest->id}}" class="label btn" style="margin-top:2px;" onclick="addNewNote({{$groupRequest->id}})">
																			保存更新
																		</button>
																	</div>
																	<div class="modal-footer" style="text-align:left">
																		<textarea readonly id="old-solution-{{$groupRequest->id}}" name="old-solution-{{$groupRequest->id}}" rows="10" cols="91" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto;line-height: 250%; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($groupRequest->solution)){{$groupRequest->solution}}@endif</textarea>
																	</div>
																</div>
															</div>
														</div>
													</form>
													<!--Modal end-->
												</td>
												<td>

													<label class="switch">
														@if ($groupRequest->isFollow == 1)
															<input checked="checked" type="checkbox" >
														@else
															<input type="checkbox" id="isFollowedOrder{{$groupRequest->id}}" name="isFollowedOrder{{$groupRequest->id}}" onclick="isFollowedOrder({{$groupRequest->id}});">
														@endif
														<div class="slider round"></div>
													</label>
												</td>
												<td>
													<label class="switch">
														@if ($groupRequest->isDone == 1)
															<input checked="checked" type="checkbox" id="isDoneOrder{{$groupRequest->id}}" name="isDoneOrder{{$groupRequest->id}}" onclick="isDoneOrder({{$groupRequest->id}});">
														@else
															<input type="checkbox" id="isDoneOrder{{$groupRequest->id}}" name="isDoneOrder{{$groupRequest->id}}" onclick="isDoneOrder({{$groupRequest->id}});">
														@endif
														<div class="slider round"></div>
													</label>
												</td>

												<td>
													<textarea readonly id="amount{{$groupRequest->id}}" rows="3" cols="10" maxlength="10" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($groupRequest->amount)){{$groupRequest->amount}}@endif</textarea>
													<button name="roEdit1" class="label btn" id="roEditBtn1{{$groupRequest->id}}" onclick="fixgroupRequest2({{$groupRequest->id}})">
														<i class="fa fa-edit"></i>
													</button>
													<button name="roSave1" id="roSaveBtn1{{$groupRequest->id}}" class="label btn hidden" style="margin-top:2px;" onclick="fixGroupRequestA({{$groupRequest->id}})">
														保存更新
													</button>
												</td>
												<td>

													@if($groupRequest->rep =="")
														<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
													@else
														@if ($groupRequest->rep == 10150)
                                                            Alex
														@elseif($groupRequest->rep == 10603 && $groupRequest->created_at > "2017-09-28 14:36:09" && $groupRequest->created_at < "2018-03-10 14:36:09")
															Alex
														@elseif($groupRequest->rep == 10603 && $groupRequest->created_at < "2017-09-28 14:36:09" )
															Sean
														@elseif($groupRequest->rep == 10603 && $groupRequest->created_at > "2018-03-10 14:36:09" )
															Todd
														@else
															{{$groupRequest->rep}}
														@endif
													@endif
												</td>
											</tr>
										@else
										@endif
									@endforeach
								@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/js/admin/plugins.checkorders.js') }}"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js'></script>
    {{--single page scripts--}}
    @include('admin.includes.js-group-request')
@endsection
