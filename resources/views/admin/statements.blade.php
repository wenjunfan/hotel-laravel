@extends('layouts.admin')

@section('title', '账户流水')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/admin/plugins.orders.css') }}">
@endsection

@section('content')
<div class="row animated fadeInRight">
    <div class="col-lg-6 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                查询流水
            </div>
            <div>
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" id="search-form" name="search-form">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="id">公司名称</label>

                            <div class="col-sm-9 form-inline">
                                <select id="partnerId" name="partnerId" class="form-control">
                                    <option value="all">全部公司</option>
                            @foreach($users as $user)
                                @if($user->active)
                                    <option value="{{$user->id}}" {{$partnerId == $user->id ? "selected" : ""}}>{{$user->company_name}}</option>
                                @endif
                            @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="from">起始日期</label>

                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="from" name="from" type="text" class="form-control" value="{{$from}}" placeholder="年-月-日">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="to">结束日期</label>

                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="to" name="to" type="text" class="form-control" value="{{$to}}" placeholder="年-月-日">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-primary btn-block" type="button" onclick="search()"><i class="fa fa-search"></i> 查询</button>
                            </div>
                            <!--div class="col-sm-6">
                                <button class="btn btn-warning btn-block" type="reset">重置</button>
                            </div-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                流水列表<!--，当前账户余额 <span class="label label-danger">${{ $balance }}</span>-->
            </div>
            <div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-statements" >
                        <thead>
                        <tr>
                            <th>公司名称</th>
                            <th>订单号</th>
                            <th>订单金额</th>
                            <th>变动金额</th>
                            <th>当前余额</th>
                            <th>备注</th>
                            <th>流水时间</th>
                        </tr>
                        </thead>
                        <tbody>
                    @foreach($statements as $statement)
                        <tr class="gradeX">
                            <td>
                               {{empty($statement->user)?'deleted user':$statement->user->company_name}}
                            </td>
                            <td>
                               {{$statement->orderId == -1 ? '' : $statement->orderId}}
                            </td>
                            <td>
							{{$statement->currencyCode . ' ' . $statement->netPrice}}
                            </td>
                            <td>
                               {{$statement->currencyCode . ' ' . $statement->amount}}
                            </td>
                            <td>
                               {{$statement->currencyCode . ' ' . $statement->balance}}
                            </td>
                            <td>
                               {{$statement->description}}
                            </td>
                            <td>
                               {{$statement->created_at}}
                            </td>
                        </tr>
                    @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/js/admin/plugins.orders.js') }}"></script>
    @include('admin.includes.js-statements')
@endsection
