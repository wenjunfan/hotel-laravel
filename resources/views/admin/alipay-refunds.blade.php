<?php
    function decodeErrorMessage($error) {
        $message = "";
        if (empty($error)) {
           return $message;
        }

        $errors = json_decode($error, true);

        foreach($errors as $e) {
            if (!empty($message)) {
                $message .= '<br>';
            }

            $message .= '<strong>errorCode: </strong>' . $e['errorCode'] . '<br>';
            $message .= '<strong>shortMessage: </strong>' . $e['shortMessage'] . '<br>';
            $message .= '<strong>longMessage: </strong>' . $e['longMessage'] . '<br>';
            $message .= '<strong>severityCode: </strong>' . $e['severityCode'];
        }

        return $message;
    }
?>

@extends('layouts.admin')

@section('title', '支付宝退款记录')

@section('css')
<link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row animated fadeInRight">
    <div class="col-lg-6 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                查询支付宝退款记录 
            </div>
            <div>
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" id="search-form" name="search-form">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="id">公司名称</label>

                            <div class="col-sm-9 form-inline">
                                <select id="partnerId" name="partnerId" class="form-control">
                                    <option value="all">全部公司</option>
                            @foreach($users as $user)
                                @if($user->active)
                                    <option value="{{$user->id}}" {{$partnerId == $user->id ? "selected" : ""}}>{{$user->company_name}}</option>
                                @endif
                            @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="from">起始日期</label>

                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="from" name="from" type="text" value="{{$from}}" class="form-control" placeholder="年-月-日">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="to">结束日期</label>

                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="to" name="to" type="text" class="form-control" value="{{$to}}" placeholder="年-月-日">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-primary btn-block" type="button" onclick="search()"><i class="fa fa-search"></i> 查询</button>
                            </div>
                            <!--div class="col-sm-6">
                                <button class="btn btn-warning btn-block" type="reset">重置</button>
                            </div-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
               支付宝退款记录
            </div>
            <div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-refunds" >
                        <thead>
                        <tr>
                            <th>订单号</th>
							<th>订单流水</th>
							<th>公司名称</th>
                            <th>退款金额</th>
							<th>手续费</th>
							<th>实际退款</th>
                            <th>退款状态</th>
                            <th>退款时间</th>
                        </tr>
                        </thead>
                        <tbody>
                    @foreach($refunds as $refund)
                        <tr class="gradeX">
                            <td>
							@if (!empty($refund->order->id))
								{{$refund->order->id}}
							@endif
							</td>
							<td>
							@if (!empty($refund->order_reference))
								{{$refund->order_reference}}
							@endif
							</td>
							<td>
							@if (!empty($refund->partner->company_name))
								{{$refund->partner->company_name}}
							@endif
                            </td>
                            <td>{{$refund->currency . ' ' . $refund->refund_amount}}</td>
                            <td>{{$refund->currency . ' ' . $refund->transaction_fee}}</td>
                            <td>{{$refund->currency . ' ' . $refund->refund_balance}}</td>
                            <td>
                            @if ($refund->status == 'REFUND')
                                <span class="label label-primary">已退款</span>
                            @elseif ($refund->status == 'PENDING')
                                <span class="label label-warning">处理中</span>
                            @endif
                            </td>
                           <!-- <td class="i{{$refund->id}}"></td> -->
							<td>{{$refund->created_at}}</td>
                            <!--td>
                            (!empty($refund->order_reference))
                                <a target="_blank" href="{{url('/order/' . $refund->order_reference)}}" class="btn btn-info btn-circle"><i class="fa fa-info"></i></a>
							
                            </td-->
                        </tr>
                    @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<script type="text/javascript">
    jQuery(function($) {
        $('#from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('.dataTables-refunds').DataTable({
            "order": [[7, "desc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: '支付宝退款记录'},
                {extend: 'pdf', title: '支付宝退款记录'},
                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    });

    @foreach($refunds as $refund)
        toLocalTime('{{$refund->id}}', '{{$refund->created_at}}');
    @endforeach

    function toLocalTime(id, t) {
        var local = new Date(t);
        var localTime = local.getFullYear() + "-" + (local.getMonth() + 1) + "-" + local.getDate() + " " + local.getHours() + ":" + local.getMinutes() + ":" + local.getSeconds();
        $(".i" + id).html(localTime);
    }

    function search() {
        var partnerId = "/" + $("#partnerId").val();
        var from = $("#from").val();
        var to = $("#to").val();

        if (from != "" && to != "") {
            self.location = "{{url('/admin/refunds')}}" + partnerId + "/afrom/" + from + "/ato/" + to;
        }
        else if (from != "") {
            self.location = "{{url('/admin/refunds')}}" + partnerId + "/afrom/" + from;
        }
        else if (to != "") {
            self.location = "{{url('/admin/refunds')}}" + partnerId + "/ato/" + to;
        }
        else {
            self.location = "{{url('/admin/alipay/refunds')}}" + partnerId;
        }
    }
</script>
@endsection
