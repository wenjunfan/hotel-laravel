{{--本页面是给财务查询订单--}}
@extends('layouts.admin')

@section('title', '财务订单管理')

@section('css')
	<link href="{{ asset('assets/css/admin/plugins.check-all-orders.css') }}" rel="stylesheet">
	<style>
        [v-cloak]{
            display: none;
        }
        .ibox-content{
            padding: 15px 0 0 0;
        }
        th, td{
            padding: 6px 2px !important;
            text-align: center;
            font-size: 12px;
        }
        .js-switch_2, .js-switch_2 .onoffswitch-switch {
            border: 2px solid #ee0000;
        }
        .js-switch_2 .onoffswitch-inner:before{
            background-color: #ee0000;
        }
        .js-switch_1, .js-switch_1 .onoffswitch-switch {
            border: 2px solid #336699;
        }
        .js-switch_1 .onoffswitch-inner:before{
            background-color: #336699;
        }
        .js-switch_3, .js-switch_3 .onoffswitch-switch {
            border: 2px solid lightseagreen;
        }
        .js-switch_3 .onoffswitch-inner:before{
            background-color: lightseagreen;
        }
        .js-switch_4, .js-switch_4 .onoffswitch-switch {
            border: 2px solid yellowgreen;
        }
        .js-switch_4 .onoffswitch-inner:before{
            background-color: yellowgreen;
        }
        #partnerInfoModalBody td:first-child {
            width: 150px;
        }
	</style>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight" id="orderApp" v-cloak>
        <div class="row">
            {{--start filter--}}
            <div class="col-lg-10">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>到账记录查询</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    {{--line 1--}}
                    <div class="ibox-content">
                        {{--下单日期 start--}}
                        <div class="col-lg-4">
                            <div class="form-group form-horizontal">
                                <label class="control-label">下单日期</label>
                                {{--date picker--}}
                                <div class="form-group" id="daterange_1">
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input type="text" class="form-control" id="startDate" name="start" v-model="dateStart"/>
                                        <span class="input-group-addon">至</span>
                                        <input type="text" class="form-control" id="endDate" name="end" v-model="dateEnd"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--下单日期 end--}}
                        {{--check in start--}}
                        <div class="col-lg-4">
                            <div class="form-group form-horizontal">
                                <label class="control-label">入住日期</label>
                                {{--date picker--}}
                                <div class="form-group" id="daterange_1">
                                    <div class="input-daterange input-group" id="datepicker1">
                                        <input type="text" class="form-control" id="checkinStart" name="checkinStart" v-model="checkinStart"/>
                                        <span class="input-group-addon">至</span>
                                        <input type="text" class="form-control" id="checkinEnd" name="checkinEnd" v-model="checkinEnd"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--check in end--}}
                        {{--check out start--}}
                        <div class="col-lg-4">
                            <div class="form-group form-horizontal">
                                <label class="control-label">退房日期</label>
                                {{--date picker--}}
                                <div class="form-group" id="daterange_2">
                                    <div class="input-daterange input-group" id="datepicker2">
                                        <input type="text" class="form-control" id="checkoutStart" name="checkoutStart" v-model="checkoutStart"/>
                                        <span class="input-group-addon">至</span>
                                        <input type="text" class="form-control" id="checkoutEnd" name="checkoutEnd" v-model="checkoutEnd"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--check out end--}}
                    </div>
                    {{--line 2--}}
                    <div class="ibox-content">
                        <div class="col-lg-5">
                            <div class="form-group form-horizontal">
                                <label class="col-lg-2 control-label">订单状态</label>
                                <div class="col-lg-4">
                                    <select data-placeholder="Choose a status..." class="form-control"  tabindex="2" v-model="orderStatus">
                                        <option value="6">全部</option>
                                        <option value="1">处理中</option>
                                        <option value="2">已确认</option>
                                        <option value="3">已取消退款中</option>
                                        <option value="4">已取消</option>
                                        <option value="7">EB拒绝</option>
                                        <option value="5">失败</option>
                                    </select>
                                </div>
                                {{--actions start--}}
                                <label class="col-lg-2 control-label">支付方式</label>
                                <div class="col-lg-4">
                                    <select data-placeholder="Choose a payment method..." class="form-control" tabindex="2" v-model="paymentMethod">
                                        <option value="">全部</option>
                                        <option value="CC">信用卡</option>
                                        <option value="PB">Paypal</option>
                                        <option value="PO">Postpay</option>
                                        <option value="WECHATPAY">微信</option>
                                        <option value="ALIPAY">支付宝</option>
                                        <option value="EB">EB支付</option>
                                    </select>
                                </div>
                                {{--actions end--}}
                            </div>
                        </div>
                        {{--订单状态 end--}}
                        {{--公司名称 start--}}
                        <div class="col-lg-7">
                            <div class="form-group form-horizontal">
                                <label class="col-lg-1 control-label">公司名称</label>
                                <div class="col-lg-3">
                                    <select data-placeholder="选择公司名称" class="chosen-select form-control" tabindex="2" v-model="partnerId">
                                        <option value="0">全部公司</option>
                                        <option v-for="user in users" :value="user.id">@{{user.company_name}}</option>
                                    </select>
                                </div>
                                <label class="col-lg-1 control-label">分销用户</label>
                                <div class="col-lg-3">
                                    <select data-placeholder="选择分销用户" class="chosen-select-affiliate form-control" tabindex="2" v-model="distributorSource">
                                        <option value="0">全部</option>
                                        <option v-for="distributor in distributors" :value="distributor.distributor_code">@{{distributor.distributor_name}}</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <button class="btn btn-text btn-default" id="btn-clear-all" style="margin-right: 20px;
                                    width: 160px;" @click="clearAll"><i class="fa fa-times"></i>重置
                                    </button>
                                </div>
                            </div>
                        </div>
                        {{--公司名称 end--}}
                    </div>
                    <div class="ibox-content">
                        {{--订单状态 start--}}
                        {{--订单号 start--}}
                        <div class="col-lg-5">
                            <div class="form-group form-horizontal">
                                <label class="col-lg-2 control-label">订单号</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" v-model="orderId">
                                </div>
                                <label class="col-lg-2 control-label">销售号</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" v-model="salesId">
                                </div>
                            </div>
                        </div>
                        {{--订单号 end--}}
                        {{--供应商确认号 start--}}
                        <div class="col-lg-7" style="margin-bottom: 10px;">
                            <div class="form-group form-horizontal">
                                <label class="col-lg-1 control-label">供应商确认号</label>
                                <div class="col-lg-3">
                                    <input type="text" class="form-control" v-model="orderPReference">
                                </div>
                                <label class="control-label col-lg-1">上游</label>
                                <div class="col-lg-3">
                                    <select data-placeholder="选择上游..." class="form-control" tabindex="2"
                                            v-model="provider">
                                        <option value="">全部</option>
                                        <option value="HotelBeds">HotelBeds</option>
                                        <option value="Ebooking">EBooking</option>
                                        <option value="Innstant">Innstant</option>
                                        <option value="Tourico">Tourico</option>
                                        <option value="Bonotel">Bonotel</option>
                                        <option value="Priceline">Priceline</option>
                                        <option value="Expedia">Expedia</option>
                                        <option value="Derby">Derby</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <button class="btn btn-primary" id="btn-search-filter" style="width: 160px; " @click="search"><i class="fa fa-search"></i>搜索
                                    </button>
                                </div>
                            </div>
                        </div>
                        {{--供应商确认号 end--}}
                    </div>
                </div>
            </div>
            {{--end filter--}}

            {{--table list start--}}
            <div class="col-lg-12">
                <div class="ibox ibox-table float-e-margins">
                    <div class="ibox-title">
                        <h5>查询结果</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTable" id="dataTable" >
                                <thead>
                                <tr>
                                    {{--1--}}
                                    <th>订单号</th>
                                    {{--2--}}
                                    <th style="width: 66px;">操作</th>
                                    {{--3--}}
                                    <th style="width: 130px;" >订购方</th>
                                    {{--4--}}
                                    <th style="width: 100px;">酒店</th>
                                    {{--5--}}
                                    <th style="width: 66px;">订购日</th>
                                    {{--6--}}
                                    <th style="width: 66px;">入住/退房</th>
                                    {{--隐藏的房晚--}}
                                    <th style="display:none;">房</th>
                                    <th style="display:none;">晚</th>
                                    {{--7--}}
                                    <th>底价</th>
                                    {{--8--}}
                                    <th>卖价</th>
                                    {{--9--}}
                                    <th style="width: 80px;" >状态</th>
                                    {{--10--}}
                                    <th>供应商</th>
                                    {{--11--}}
                                    <th style="width: 88px;">供应商确认号</th>
                                    {{--12--}}
                                    <th>支付方式</th>
                                    {{--13--}}
                                    <th>交易流水号</th>
                                    {{--14--}}
                                    <th>持卡人</th>
                                    {{--15--}}
                                    <th>信用卡人</th>
                                    {{--16--}}
                                    <th style="width: 140px !important;">财务备注</th>
                                    {{--17--}}
                                    <th title="如果有红色的信息需点击查看可疑订单后台二次确认订单真伪">所有备注</th>
                                    {{--18--}}
                                    <th>是否到账<br>是否退款</th>
                                    {{--19--}}
                                    <th>是否上游<br>是否佣金</th>
                                    {{--20--}}
                                    <th>是否已 <br>存银行</th>
                                    {{--隐藏的酒店地址--}}
                                    <th style="display:none;">酒店地址</th>
                                    <th style="display:none;">酒店城市</th>
                                    <th style="display:none;">酒店邮编</th>
                                    <th style="display:none;">酒店州</th>
                                    <th style="display:none;">酒店国家</th>
                                    <th style="width: 66px;">取消时间</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="gradeX" v-for="(order, index) in orders" :key="index">
                                    <td>@{{ order.id }}
                                        <p style="font-size: 0.6em;">
                                                <span v-if="order.order_tem && order.order_tem.is_pc === 1" style="color: blue">PC</span>
                                                <span v-if="order.order_tem && order.order_tem.is_pc === 2" style="color: blueviolet">Mobi</span>
                                        </p>                                    
                                    </td>
                                    {{--2.操作--}}
                                    <td>
                                        <a target="_blank" :href="'/order/' + order.orderReference" class="btn btn-xs btn-info">发票信息</a>
                                        <span v-if="order.status === 'CONFIRMED'">
                                            <a target="_blank" :href="'/voucher/' + order.code" class="label label-success">酒店凭证</a>
                                        </span>
                                        {{--财务不能取消订单,对接ebooking的api的不能取消--}}
                                        <span  v-if="order.rooms && order.rooms[0] && order.rooms[0].cancelPolicies && formatDate(JSON.parse(order.rooms[0].cancelPolicies)[0].end) > now  && order.status === 'CONFIRMED' && isAdmin && order.partner.id !== 10034">
                                            <button class="btn btn-danger btn-xs" style="font-size: 11px; margin-top: 6px;" @click="cancelOrder(order)">取消订单</button>
                                        </span>
                                    </td>
                                    {{--3.订购方--}}
                                    <td style="font-size: 11px;">
                                        <a @click="getPartnerInfo(order.partner.id)" class="btn btn-default btn-xs"><i class="fa fa-info-circle fa"></i></a><br/>
                                        {{--老数据可能没有partner关联需要判断--}}
                                        <span v-if="order.partner != null">
                                            (@{{ order.partner.id }}) <span class="tooltip-test" :title="order.partner.company_name" v-if="order.partner.company_name">@{{ order.partner.company_name.slice(0, 20) }}</span>
                                            <span v-if="order.partner.company_name && order.partner.company_name.length >20">...</span>
                                            <span v-if="order.partner.id === 11087" class="text-success">
                                                <br>@{{ order.bookerFirstName }} @{{ order.bookerLastName }} <br>
                                                <span class="text-danger" v-if="order.order_tem && JSON.parse(order.order_tem.booker).validPhoneNumber">@{{ JSON.parse(order.order_tem.booker).countryCodePhone}}@{{ JSON.parse(order.order_tem.booker).validPhoneNumber}}</span>
                                                <span v-else>
                                                    @{{ order.bookerPhone }}
                                                </span>
                                            </span>
                                        </span>
       				                    {{--销售 330 or 210 or 106 or 219 or dida || ctrip--}}
                                        <p v-if="order.partnerId === 10048 || order.partnerId === 10602 || order.partnerId === 10601 || order.partnerId === 10565 || order.partnerId === 10150"  class="text-danger">@{{ order.remark}}</p>
                                        <p class="text-danger" v-if="order.ctrip">
                                            @{{ order.ctrip.ctrip_reservation_number }}
                                        </p>
				                    </td>
                                    {{--4.酒店--}}
                                    <td class="center" style="font-size: 11px;">
                                        @{{ order.hotelName}}
                                        <span v-if="order.hotelName_zh"> <br>@{{ order.hotelName_zh}} </span>
                                    </td>
                                    {{--5.订购日--}}
                                    <td class="center">@{{ order.created_at.slice(0, 10) }}</td>
                                    {{--6.入住/退房--}}
                                    <td class="center">
                                        @{{ order.checkinDate }} <br> @{{ order.checkoutDate }}
                                        <p class="text-danger" v-if="order.order_tem && order.order_tem.search_info &&
                                        (order
                                        .checkinDate
                                         !== JSON.parse(order.order_tem.search_info).checkin || order.checkoutDate!==
                                          JSON.parse(order.order_tem.search_info).checkout)">有错误，搜索
                                      @{{JSON.parse(order.order_tem.search_info).checkin}}/ @{{JSON.parse(order.order_tem.search_info).checkout}}</p>
                                    </td>
                                    {{--隐藏的房晚--}}
                                    <td class="center" style="display:none;">
                                        @{{ order.room_number }}
                                    </td>
                                    <td class="center" style="display:none;">
                                        @{{ order.day_number }}
                                    </td>
                                    {{--7.底价--}}
                                    <td class="center">
                                        <span v-if="order.currencyCode === 'USD'">$</span> @{{ order.totalOriginalNetPrice }}
                                    </td>
                                    {{--8.卖价--}}
                                    <td class="center">
                                        <span v-if="order.currencyCode === 'USD'">$</span><span v-else>@{{ order.currencyCode }}</span>
                                        <span v-if="order.infact_cost == null || order.infact_cost < order.totalPrice">@{{ order.totalPrice }}</span> <span v-else>@{{ order.infact_cost }}</span>
                                        <span v-if="order.order_tem !== null &&  order.order_tem.coupon_code !== null && order.order_tem.coupon_code !== ''" class="text-danger"><br/>(@{{order.order_tem.coupon_code}})<br/>最终收款： @{{ order.order_tem.amount}}</span>
                                    </td>
                                    {{--9.状态--}}
                                    <td class="center">
                                        {{--9.1 confirmed status--}}
                                        <span v-if="order.status === 'CONFIRMED'" class="label label-info">
                                            已确认
                                        </span>
                                        <span v-else-if="order.status === 'CANCELLED'" class="label label-warning">
                                            <i v-if="order.payment_status === 'PENDING' ">已取消退款中</i>
                                            <i v-else>已取消</i>
                                        </span>
                                        <span v-else-if="order.status === 'ON REQUEST'" class="label label-success">
                                            处理中
                                        </span>
                                        {{--9.4 EBooking Reject status--}}
                                        <span v-else-if="order.status === 'REJECTED' && order.provider === 'EBooking'" class="label label-danger">
                                            EB拒绝
                                        </span>
                                        <span v-else class="label label-danger">
                                            失败
                                        </span>
                                        {{--9.2 ON REQUEST status--}}
                                        <span v-if="order.status === 'ON REQUEST'" class="label">
                                            <button class="label btn" onclick="checkOrder(order)">
                                                更新订单状态
                                            </button>
                                        </span>
                                        {{--9.3 CANCELLED status--}}
                                        <span v-if="order.status === 'CANCELLED' && order.payment_status === 'PENDING'" class="label">
                                            <button class="label btn" type="button" onclick="refund(order)">
                                                退款
                                            </button>
                                        </span>
                                    </td>
                                    {{--10.供应商--}}
                                    <td class="center">@{{ order.provider }}</td>
                                    {{--11.供应商确认号--}}
                                    <td class="center">@{{ order.orderPReference.replace(/,/g, " ") }}</td>
                                    {{--12.支付方式--}}
                                    <td class="center">
                                        <span v-if="order.payment_type === 'ALIPAY'">
                                            支付宝
                                            <span v-if="order.order_tem != null && order.order_tem.cny != null"><br><span class="text-warning" v-if="order.order_tem.cny !== 0">@{{ order.order_tem.cny }}元</span></span>
                                        </span>
                                        <span v-else-if="order.payment_type === 'WECHATPAY'">
                                            微信支付
                                            <span v-if="order.order_tem !== null && order.order_tem.cny != null"><br><span class="text-warning" v-if="order.order_tem.cny !== 0">@{{ order.order_tem.cny }}元</span></span>
                                        </span>
                                        <span v-else-if="order.payment_type === 'CC'">
                                            信用卡
                                            <span class="text-success" v-if="order.payment && order.payment[0] != null && order.payment[0].cc_number !== undefined "><br>(#@{{ order.payment[0].cc_number }})</span>
                                        </span>
                                        <strong v-else-if="order.payment_type === 'PO'" class="text-muted">PostPay</strong>
                                        <strong v-else-if="order.partnerId === 10020 || order.partnerId === 10021  || order.partnerId === 10022 " class="text-muted">Eb支付</strong>
                                        <strong v-else-if="order.payment_type === 'PB'" class="text-muted">Paypal</strong>
                                        <span v-else>支付失败</span>
                                    </td>
                                    {{--13.交易流水号--}}
                                    <td class="center">
                                        <span v-if="order.order_tem != null && (order.payment_type === 'ALIPAY' || order.payment_type === 'WECHATPAY')">
                                            @{{ order.order_tem.Reorderid }}
                                        </span>
                                        <span v-else-if="order.payment && order.payment[0] != null && order.partnerId === 11676">@{{ order.payment[0].ppref }}</span>
                                        <span v-else-if="order.payment && order.payment[0] != null && order.partnerId === 11087">@{{ order.payment[0].ppref }}</span>
                                        <span v-else-if="order.payment && order.payment[0] != null && order.payment[1] != null && order.payment[0].ppref != null">@{{ order.payment[0].ppref }}<br/>@{{ order.payment[1].ppref }}</span>
                                        <span v-else-if="order.payment && order.payment[0] != null && order.payment[0].ppref ">@{{ order.payment[0].ppref }}</span>
                                        <span v-else-if="order.payment && order.payment[0] != null">@{{ order.payment[0].transactionId }}</span>
                                        <span v-else>@{{ order.ppref }}</span>
                                    </td>
                                    {{--14.持卡人--}}
                                    <td class="center">
                                        <span v-if="order.payment_type === 'CC' && order.partnerId !== 11087 && order.partner && order.partner.cardholder">@{{ order.partner.cardholder }}</span>
                                    </td>
                                    {{--15.信用卡姓名--}}
                                    <td class="center">
                                        <span v-if="order.payment && order.payment[0] != null">@{{ order.payment[0].first_name }} @{{ order.payment[0].last_name }}</span>
                                    </td>
                                    {{--16财务备注--}}
                                    <td class="center" style="font-size: 11px;">
                                        <i class="fa fa-2x fa-edit pull-right text-success" @click="showAddCommentModal(order, index)"></i>
                                        <div readonly :id="'old-finance_remark-' + order.id" :name="'old-finance_remark-' + order.id" style="text-align:left; height:80px; overflow-y: auto;cursor: pointer;" v-html="order.finance_remark" :title="order.finance_remark"></div>
                                    </td>
                                    {{--17备注（所有备注）--}}
                                    <td class="center">
                                        <span v-if="order.sales_no && order.sales_no !== 0">@{{ order.sales_no }}</span>
                                        {{--19是财务的意思,相当于自动掉单--}}
                                        <span v-if="!order.sales_no && (order.order_tem && order.order_tem.suspicious && order.order_tem.suspicious.sales_no !== 0 || order.linked_id && order.linked_id !== 0 && order.linked_id.sales_no && order.created_at && order.linked_id.followed_at && order.created_at > order.linked_id.followed_at || order.order_tem && order.order_tem.paid_not_booked === 2 && order.order_tem.sales_no)">19</span>
                                        <span v-if="order.order_tem && order.order_tem.suspicious && order.order_tem.suspicious.sales_no !== 0" class="text-danger">, @{{ order.order_tem.suspicious.sales_no }}</span>
                                        <span v-if="order.order_tem && order.order_tem.paid_not_booked === 2 && order.order_tem.sales_no" class="text-danger">, @{{ order.order_tem.sales_no }}</span>
                                        <span v-if="order.linked_id && order.linked_id != 0 && order.linked_id.sales_no && order.created_at && order.linked_id.followed_at && order.created_at > order.linked_id.followed_at" class="text-info"> , @{{ order.linked_id.sales_no }} </span>
                                        <span v-if="order.sales_no && order.order_tem && order.order_tem.suspicious || (order.linked_id && order.linked_id != 0)">
                                        <br>
                                        </span>
                                        <span v-if="
                                            (order.order_tem && order.order_tem.suspicious  && order.order_tem.suspicious.commend && order.order_tem.suspicious.commend.trim() !== '') ||
                                            (order.remark && order.remark.trim() !== '') ||
                                            (order.order_tem && order.order_tem.remark && order.order_tem.remark.trim() !== '')
                                            || (order.feedback && order.feedback.solution !=='')
                                            || (order.order_tem && order.order_tem.commend && order.order_tem.commend.trim() !== '')||(order.channel_remark && order.channel_remark.trim() !== '')">
                                            {{--带有fraud 备注--}}
                                            <i class="fa fa-info-circle fa-2x text-danger" @click="showCommentModal(order)" v-if="order.order_tem && order.order_tem.suspicious  && order.order_tem.suspicious.commend && order.order_tem.suspicious.commend.trim() !== ''"></i><br/><a v-if="order.order_tem && order.order_tem.suspicious" target="_blank" href="https://hotel.usitrip.com/affiliate/fraud-list">后台</a>
                                            {{--带有其他备注--}}
                                            <i v-else class="fa fa-info-circle fa-2x" @click="showCommentModal(order)"></i>
                                        </span>
                                    </td>
                                    {{--18.是否到账/退款--}}
                                    <td class="center">
                                        {{--postpay 收到钱并且已取消，给他一个是否退款的switch--}}
                                        <div v-if="order.isChecked === 1 && order.payment_type === 'PO' && order.status === 'CANCELLED'">
                                            <div class="switch">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" class="onoffswitch-checkbox" :id="'sw1-' + order.id" checked disabled>
                                                    <label class="onoffswitch-label js-switch_1" :for="'sw1-' + order.id">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="switch switch-danger">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" class="onoffswitch-checkbox" :id="'sw2-'
                                                    +order.id" :checked="order.isReturned === 1" @click="cbisReturned(order)">
                                                    <label class="onoffswitch-label js-switch_2" :for="'sw2-' + order
                                                    .id">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="switch" v-else>
                                            <div class="onoffswitch">
                                                <input type="checkbox" class="onoffswitch-checkbox" :id="'sw1-' + order.id" :checked="order.isChecked === 1" @click="cbisChecked(order)">
                                                <label class="onoffswitch-label js-switch_1" :for="'sw1-' + order.id">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <i v-if="order.isChecked === 1" style="color:#fff">.</i>
                                    </td>
                                    {{--19.是否结算/是否佣金--}}
                                    <td class="center">
                                        {{--是否结算--}}
                                        <div class="switch text-center">
                                            <div class="onoffswitch">
                                                <input type="checkbox" class="onoffswitch-checkbox" :id="'sw3-' +
                                                order.id" :checked="order.isPayed === 1" @click="cbisPayed(order)">
                                                <label class="onoffswitch-label js-switch_3" :for="'sw3-' + order.id">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div v-if="order.feedback !== null && order.feedback.isDone === 0"
                                             class="label label-danger">争议订单</div>
                                        {{--是否结算过了佣金--}}
                                        <div class="switch text-center" v-if=" order.order_tem && order.order_tem.source && order.order_tem.source.trim() !== '' " >
                                            <div class="onoffswitch">
                                                <input type="checkbox" class="onoffswitch-checkbox" :id="'sw4-' + order.id" :checked="order.isAffiliated === 1" @click="cbisAffiliated(order)">
                                                <label class="onoffswitch-label js-switch_4" :for="'sw4-' + order.id">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                            <p v-if="order.source === 'dealmoon' || (order.distributor && order
                                            .distributor.distributor_name === 'dealmoon.com')">Dealmoon</p>
                                            <p v-if="order.distributor&& order
                                            .distributor.distributor_name !== 'dealmoon.com'" :title="order
                                            .distributor.distributor_name">
                                                <span v-if="order.currencyCode === 'USD'">$</span>
                                                <span v-else>@{{order.currencyCode }}</span>
                                                @{{(order.totalPrice/1.15*0.05).toFixed(2) }}
                                            </p>
                                        </div>
                                        <i v-if="order.isPayed === 1" style="color:#fff">.</i>
                                    </td>
                                    {{--20.是否存银行--}}
                                    <td class="center">
                                        <div class="switch">
                                            <div class="onoffswitch">
                                                <input type="checkbox" class="onoffswitch-checkbox" :id="'sw5-' +
                                                order.id" :checked="(order.isChecked==1 && order.payment_type ==
                                                'CC') || (order.isChecked==1 && order.payment_type == 'PB') || order
                                                .isBanked==1 " @click="cbisBanked(order)">
                                                <label class="onoffswitch-label js-switch_5" :for="'sw5-' + order.id">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <i v-if="order.isBanked === 1" style="color:#fff">.</i>
                                    </td>
                                    {{--隐藏的酒店地址--}}
                                    <td class="center" style="display:none"><span v-if="order.hotel && order.hotel.address">@{{ order.hotel.address}}</span></td>
                                    <td class="center" style="display:none"><span v-if="order.hotel && order.hotel.city">@{{ order.hotel.city}}</span></td>
                                    <td class="center" style="display:none"><span v-if="order.hotel && order.hotel.zipcode">@{{ order.hotel.zipcode}}</span></td>
                                    <td class="center" style="display:none"><span v-if="order.hotel && order.hotel.destination && order.hotel.destination.state">@{{ order.hotel.destination.state }}</span></td>
                                    <td class="center" style="display:none"><span v-if="order.hotel && order.hotel.destination && order.hotel.destination.country">@{{ order.hotel.destination.country.countryName }}</span></td>
                                    {{--21.取消时间--}}
                                    <td class="center text-muted">
                                        {{--requestlog [0]是book这次action后面的就是cancel了, ebooking[1]是ali和携程外的cancel，2是ali和携程的cancel--}}
                                        <i v-if="order.requestlog && order.requestlog[1] != null && order.partnerId !== 10020 && order.partnerId !== 10021  && order.partnerId !== 10022">
                                            <span v-if="order.requestlog[1].operation == 'cancel'">
                                                @{{ order.requestlog[1].created_at.slice(0, 10) }}
                                                 <span style="color:red" v-if="order.status !== 'CANCELLED'">取消失败</span>
                                            </span>
                                        </i>
                                        {{--requestlog [2] ali也是cancel，携程是/ctrip/cancelReservation--}}
                                        <i v-else-if="order.requestlog && order.requestlog[2] != null && order.partnerId !== 10020 && order.partnerId !== 10021">
                                            <span v-if="order.requestlog[2].operation == 'cancel'">
                                                @{{ order.requestlog[2].created_at.slice(0, 10) }}
                                                 <span style="color:red" v-if="order.status !== 'CANCELLED'">取消失败</span>
                                            </span>
                                        </i>
                                        <i v-else-if="order.requestlog && order.requestlog[2] != null && order.partnerId !== 10022">
                                            <span v-if="order.requestlog[2].operation == '/ctrip/cancelReservation'">
                                                @{{ order.requestlog[2].created_at.slice(0, 10) }}
                                                <span style="color:red" v-if="order.status !== 'CANCELLED'">取消失败</span>
                                            </span>
                                        </i>
                                        <i v-else><span v-if="order.status === 'CANCELLED'">特殊取消</span></i>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--<!--start show all comment Modal -->--}}
        <div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="AccCommentModalTitle">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="commentModalTitle">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        {{--porder 订单备注--}}
                        <div class="panel panel-default" v-if="comments.order_comment && comments.order_comment.trim() !== ''">
                            <div class="panel-heading">
                                <h3 class="panel-title">客户订单备注</h3>
                            </div>
                            <div class="panel-body" v-html="comments.order_comment"></div>
                        </div>
                        {{--porder 临时订单备注 commend--}}
                        <div class="panel panel-default" v-if="comments.order_tem2 && comments.order_tem2.trim()!== ''">
                            <div class="panel-heading">
                                <h3 class="panel-title">临时订单备注 <span style="font-weight: 300; font-size: 11px;">（财务或者销售对citcon收钱但是没有订上的酒店后续处理的备注）</span></h3>
                            </div>
                            <div class="panel-body" v-html="comments.order_tem2"></div>
                        </div>
                        {{--porder OP 订单备注 commend--}}
                        <div class="panel panel-default" v-if="comments.op_comment && comments.op_comment.trim() !== ''">
                            <div class="panel-heading">
                                <h3 class="panel-title">OP 订单备注</h3>
                            </div>
                            <div class="panel-body" v-html="comments.op_comment"></div>
                        </div>
                        {{--porder Fraud订单备注 commend--}}
                        <div class="panel panel-default" v-if="comments.fraud_comment && comments.fraud_comment.trim() !== ''">
                            <div class="panel-heading">
                                <h3 class="panel-title">Fraud 订单备注</h3>
                            </div>
                            <div class="panel-body" v-html="comments.fraud_comment"></div>
                        </div>
                        {{--Feedback 争议订单备注 commend--}}
                        <div class="panel panel-default" v-if="comments.feedback_comment && comments.feedback_comment.trim() !==  ''">
                            <div class="panel-heading">
                                <h3 class="panel-title">Feedback 订单备注</h3>
                            </div>
                            <div class="panel-body" v-html="comments.feedback_comment"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    </div>
                </div>
            </div>
        </div>
        {{--end modal--}}
        <div class="modal fade" id="partnerInfoModal" tabindex="-1" role="dialog" aria-labelledby="partnerInfoModalTitle" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content guest-name-table" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="partnerInfoModalTitle"></h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered">
                            <tbody id="partnerInfoModalBody">
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- start add accounting comment Modal--}}
        <div class="modal fade" id="addCommentModal" tabindex="-1" role="dialog" aria-labelledby="AddAccCommentModalTitle" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="addCommentModalTitle">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div id="accounting_remark"></div>
                        <hr>
                        <div id="add-area"><textarea name="" id="" cols="90" rows="6" v-model="addComment" style="resize: none;"></textarea></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>  <button class="btn btn-primary" @click="addNewNote">提交</button>
                    </div>
                </div>
            </div>
        </div>
        {{--end add accounting comment Modal--}}
    </div>
@endsection

@section('scripts')
    <script>
        var today = "{{ \Carbon\Carbon::now() }}";
        var orders = {!! $orders !!};
        var users =  {!! $users !!};
        var distributors =  {!! $distributors !!};
        var isAdmin = {!! \Auth::user()->admin !!};
        var userId = "{{ \Auth::id() }}";
        {{--get params from url vars and pass to vue data--}}
        var urlParams = window.getUrlVars();
        var dateStart = !urlParams.creationStart ? "{{ $creationStart }}" : urlParams.creationStart;
        var dateEnd = !urlParams.creationEnd ? "{{ $creationEnd }}" : urlParams.creationEnd;
        var checkinStart = !urlParams.checkinStart ? "" : urlParams.checkinStart;
        var checkinEnd = !urlParams.checkinEnd ? "" : urlParams.checkinEnd;
        var checkoutStart = !urlParams.checkoutStart ? "" : urlParams.checkoutStart;
        var checkoutEnd = !urlParams.checkoutEnd ? "" : urlParams.checkoutEnd;
        var orderPReference = !urlParams.orderPReference ? "" : urlParams.orderPReference;
        var orderId = !urlParams.orderId ? "" : urlParams.orderId;
        var salesId = !urlParams.salesId ? "" : urlParams.salesId;
        var orderStatus = !urlParams.orderStatus ? 6 : urlParams.orderStatus;
        var partnerId = !urlParams.partnerId ? 0 : parseInt(urlParams.partnerId);
        var paymentMethod = !urlParams.paymentMethod ? "" : urlParams.paymentMethod;
        var provider = !urlParams.provider ? "" : urlParams.provider;
        var distributorSource = !urlParams.distributorSource ? 0 : urlParams.distributorSource;
    </script>
    <script src="{{ asset('assets/js/admin/plugins.check-all-orders.js') }}"></script>
    <script src="{{ asset('assets/js/admin/check-all-orders.js') }}"></script>
@endsection
