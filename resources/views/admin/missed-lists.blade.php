@extends('layouts.admin')

@section('title', '可疑微信收款')

@section('css')
	<link href="{{ asset('assets/css/admin/plugins.orders.css') }}" rel="stylesheet">
	<link href="{{ asset('css/hotel/missed-order-list.css') }}" rel="stylesheet">
@endsection

@section('content')
	<div class="row animated fadeInRight">
		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-noborder" style="margin-top:20px; margin-left:20px;">
					<h3 style="font-size: 18px;">可疑微信和支付宝收款</h3>
					<div>
						请注意以下几点：<br/>
                        1：操作之前请先登录走四方后台，或遵循技术部指导在您的浏览器中记录正确的工号。<br/>
						<h5 class="text-danger">2：首先确认客人已付款：点击“查看是否已收款”，若结果为Not Paid则表示不用操作且该临时订单会被自动移除；若结果为charge则表示客人已付款，请点击继续下单。</h5>
                        3：进入详情页后，如果价格不变或变低，请选择正确的房型进入到预定页，点击页面底部的下单按钮，订购成功后，用户将自动收到voucher确认邮件。<br/>
						4：如果发现价格变高，请致电客人，若客人不同意补差价则拒单；若客人同意补差价，则进入预定页继续，直到发送了补差价邮件。客人和550都会收到一封补差价邮件，里面会有补差价二维码，用户完成额外付款后，需由财务更新已收款总额再由销售继续下单。<br/>
						5：如有技术疑问，请找550。
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12 col-md-12">
			<div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover dataTables-missedOrderLists">
							<thead>
								<tr>
									<th>ID</th>
									<th>操作(下单/拒单)</th>
									<th>下单(<span class="text-success">server时间</span>/<span class="text-info">当地时间</span>)</th>
									<th>姓名/公司名/佣金比例</th>
									<th>电话</th>
									<th>citcon确认号(支付方式)</th>
									<th>金额</th>
									<th>订单号</th>
									<th>备注</th>
								</tr>
							</thead>
							<tbody>
								@foreach($missedLists as $tempOrder)
									<?php
                                    $bookerDetail = json_decode($tempOrder->booker, true);
											?>
									<tr class="gradeX">
										<td>
											{{$tempOrder->id}}
										</td>
										<td>
											@if(($tempOrder->paid_not_booked == 1 || $tempOrder->paid_not_booked == 4) && $tempOrder->orderReference == null && ($tempOrder->wechatpayid != null || $tempOrder->alipayid != null))
                                                <p id="{{'order_process_'.$tempOrder->id}}">
                                                    <a id="{{'citcon_'.$tempOrder->id}}" class="btn btn-primary ladda-button" onclick="checkPmyStatus('{{$tempOrder->id}}', 'continue')">查看是否已收款</a>
                                                    <span class="text-danger">new!</span>
                                                </p>
                                                <a class="btn btn-info order-continue hide" target="_blank" href="/hotel/missed/reorder/{{$tempOrder->display_hotelId}}/{{$tempOrder->Reorderid}}">下/拒单链接</a>
											@elseif($tempOrder->paid_not_booked == 3)
												<span class="text-danger">{{$tempOrder->sales_no}} 已拒绝</span>
											@elseif($tempOrder->paid_not_booked === 2)
												{{$tempOrder->sales_no}} 已处理
                                            @else
												无需处理
											@endif
									</td>
									<td>
										<span class="text-success">{{$tempOrder->created_at}}</span><br/>
											<span class="text-info">
												@if(isset($tempOrder->booker_local_time))
                                                    <?php
                                                    $monthStr = substr($tempOrder->booker_local_time,4, 3);
                                                    $monthArray = array("01"=>"Jan", "02"=>"Feb", "03"=>"Mar","04"=>"Apr","05"=>"May","06"=>"Jun",
                                                                        "07"=>"Jul","08"=>"Aug","09"=>"Sep", "10"=>"Oct", "11"=>"Nov", "12"=>"Dec");
                                                    $monthNum = array_search($monthStr,$monthArray);
                                                    ?>
													{{substr($tempOrder->booker_local_time,11, 4). '-'.$monthNum.'-'.substr($tempOrder->booker_local_time,8, 2).' '.substr($tempOrder->booker_local_time,16, 8)}}<br/>
													{{substr($tempOrder->booker_local_time,24)}}
												@endif
											</span>
										</td>
										<td>
											<span class="text-success">{{$bookerDetail['firstname'] .' ' .$bookerDetail['lastname']}}</span>
											<p>{{$tempOrder->partner->company_name}}</p>
											<p>微信支付用户佣金比：{{ $tempOrder->partner->priceRatio}}</p>
										</td>
										<td>
											<span class="text-success">{{$bookerDetail['phone']}}</span>
										</td>
										<td>
											<span id="{{'ref_'.$tempOrder->id}}" valu="{{$tempOrder->Reorderid}}">{{$tempOrder->Reorderid}}</span>
											@if($tempOrder->wechatpayid != null)
												（微信）
											@elseif($tempOrder->alipayid != null)
												（支付宝）
											@else
												（未付款）
											@endif
											<p class="text-info">{{$tempOrder->client_ip}}</p>
										</td>
										<td>
											@if( $tempOrder->paid_not_booked == 1)
											<p id="{{'citcon_status_'.$tempOrder->id}}"><a id="{{'citcon_'.$tempOrder->id}}" onclick="checkPmyStatus({{$tempOrder->id}})">查看是否收款</a></p>
											@endif
											@if(strrpos($tempOrder->commend, '财务确认过到账，现在新的客人已付款') > -1)
												<span class="text-danger">财务已将价格更新至：
														<br/>{{$tempOrder->currencyCode. ' ' .$tempOrder->amount }}</span>
											@else
												@if(strrpos($tempOrder->commend, '是原来付款金额，请同财务确认是否到账') > -1 && $tempOrder->orderReference == null && (Auth::user()->admin ==1|| Auth::user()->admin ==2) )
													原价：{{$tempOrder->currencyCode. ' ' .$tempOrder->amount }}
													<br/>
													<textarea readonly id="new_amount_confirmed_{{$tempOrder->id}}" name="new_amount_confirmed_{{$tempOrder->id}}" rows="1" cols="4" maxlength="4" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline:git  none;-webkit-box-shadow: none;-moz-box-shadow: none;"></textarea>
													<button name="roEdit" class="label btn" id="roEditBtn{{$tempOrder->id}}" onclick="ableAddNewAmount({{$tempOrder->id}})">
														<i class="fa fa-edit"></i></button>
													<button name="roSave" id="roSaveBtn{{$tempOrder->id}}" class="label btn hidden" style="margin-top:2px;" onclick="confirmNewAmount({{$tempOrder->id}})">
														保存更新
													</button>
													<br/>
													<span class="text-danger">如果参考备注的金额确认二次支付收款到位：请将价格改成两次价格总额</span>
												@else
													{{$tempOrder->currencyCode. ' ' .$tempOrder->amount }}
													<br/>
													{{'(RMB ' .$tempOrder->cny. ')'}}
												@endif
											@endif
										</td>
										<td>
											<P class="text-success">
												@if($tempOrder->orderReference != '' )
													<img class='smlpic' src='/img/general/icon/valid.png'>订单号：{{$tempOrder->order->id}}
                                                @endif
											</P>
										</td>
										<td>
											<a href="#" style="text-decoration:none; color:black;" class="tooltip-test" title="{{$tempOrder->commend}}">
												<textarea readonly id="commend{{$tempOrder->id}}" rows="3" cols="10" maxlength="1000" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($tempOrder->commend)){{$tempOrder->commend}}@endif</textarea></a>
											<button name="roEdit" class="label btn" type="button" id="roEditBtn{{$tempOrder->id}}" data-toggle="modal" data-target="#myModal-{{$tempOrder->id}}" data-whatever="@if(isset($tempOrder->id)){{$tempOrder->id}}@endif">
												<i class="fa fa-edit"></i>
											</button>
											<!--Modal start-->
											<form action="{{URL::to('/affiliate/missed-lists')}}">
												<div class="modal fade" id="myModal-{{$tempOrder->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
													<div class="modal-dialog" role="document" style="padding-top:70px">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
																<h4>备注({{$tempOrder->id}})：</h4>
															</div>
															<div class="modal-body">
																<textarea autofocus id="new-commend-{{$tempOrder->id}}" name="new-commend-{{$tempOrder->id}}" rows="4" cols="80" maxlength="1000" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;"></textarea>
																<button name="roSave" id="roSaveBtn{{$tempOrder->id}}" class="label btn" style="margin-top:2px;" onclick="addNewNote({{$tempOrder->id}})">
																	保存更新
																</button>
															</div>
															<div class="modal-footer" style="text-align:left">
																<textarea readonly id="old-commend-{{$tempOrder->id}}" name="old-commend-{{$tempOrder->id}}" rows="10" cols="91" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto;line-height: 250%; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($tempOrder->commend)){{$tempOrder->commend}}@endif</textarea>
															</div>
														</div>
													</div>
												</div>
											</form>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('assets/js/admin/plugins.orders.js') }}"></script>
    {{--single page scripts--}}
    @include('admin.includes.js-missed-lists')
@endsection
