<?php
    function decodeErrorMessage($error) {
        $message = "";
        if (empty($error)) {
           return $message;
        }

        $errors = json_decode($error, true);

        foreach($errors as $e) {
            if (!empty($message)) {
                $message .= '<br>';
            }

            $message .= '<strong>errorCode: </strong>' . $e['errorCode'] . '<br>';
            $message .= '<strong>shortMessage: </strong>' . $e['shortMessage'] . '<br>';
            $message .= '<strong>longMessage: </strong>' . $e['longMessage'] . '<br>';
            $message .= '<strong>severityCode: </strong>' . $e['severityCode'];
        }

        return $message;
    }
?>

@extends('layouts.admin')

@section('title', '信用卡退款记录')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/admin/plugins.orders.css') }}">
@endsection

@section('content')
<div class="row animated fadeInRight">
    <div class="col-lg-6 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                查询信用卡退款记录
            </div>
            <div>
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" id="search-form" name="search-form">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="id">公司名称</label>

                            <div class="col-sm-9 form-inline">
                                <select id="partnerId" name="partnerId" class="form-control">
                                    <option value="all">全部公司</option>
                            @foreach($users as $user)
                                @if($user->active)
                                    <option value="{{$user->id}}" {{$partnerId == $user->id ? "selected" : ""}}>{{$user->company_name}}</option>
                                @endif
                            @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="from">起始日期</label>

                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="from" name="from" type="text" value="{{$from}}" class="form-control" placeholder="年-月-日">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="to">结束日期</label>

                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar-alt"></i></span><input id="to" name="to" type="text" class="form-control" value="{{$to}}" placeholder="年-月-日">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-primary btn-block" type="button" onclick="search()"><i class="fa fa-search"></i> 查询</button>
                            </div>
                            <!--div class="col-sm-6">
                                <button class="btn btn-warning btn-block" type="reset">重置</button>
                            </div-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                信用卡退款记录
            </div>
            <div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-refunds" >
                        <thead>
                        <tr>
                            <th>公司名称</th>
                            <th>订单流水</th>
                            <th>退款状态</th>
                            <th>交易流水</th>
                            <th>退款流水</th>
                            <th>关联号</th>
                            <th>本次退款</th>
                            <th>共退款</th>
                            <th>错误信息</th>
                            <th>退款时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
						@foreach($refunds as $refund)
                        <tr class="gradeX">
                            <td>
							@if (!empty($refund->user->company_name))
								{{$refund->user->company_name}}
							@endif
                            </td>
                            <td>
							@if (!empty($refund->orderReference))
								{{$refund->orderReference}}
							@endif
							</td>
                            <td>
                            @if ($refund->ack == 'Success')
                                <span class="label label-primary">成功</span>
                            @else
                                <span class="label label-warning">失败</span>
                            @endif
                            </td>
                            <td>{{$refund->transactionId}}</td>
                            <td>{{$refund->refundTransactionId}}</td>
                            <td>{{$refund->correlationId}}</td>
                            <td>{{$refund->currency . ' ' . $refund->grossRefundAmount}}</td>
                            <td>{{$refund->currency . ' ' . $refund->totalRefundedAmount}}</td>
                            <td>{!!decodeErrorMessage($refund->errors)!!}</td>
                            <td id="{{$refund->id}}"></td>
                            <td>

                            @if (!empty($refund->orderReference))
                                <a target="_blank" href="{{url('/order/' . $refund->orderReference)}}" class="btn btn-info btn-circle"><i class="fa fa-info"></i></a>
							@endif
                            </td>
                        </tr>
						@endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/js/admin/plugins.orders.js') }}"></script>
    @include('admin.includes.js-refunds')
@endsection
