@extends('layouts.admin')

@section('title', '用户反馈')

@section('css')
	<link href="{{ asset('assets/css/admin/plugins.orders.css') }}" rel="stylesheet">
    {{--single page styles--}}
    @include('admin.includes.css-feedbacks')
@endsection

@section('content')
	<div class="row animated fadeInRight">
		<div class="col-lg-8 col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					供应商信息
				</div>
				<div>
					<div class="col-sm-8 ibox-content">
						<form class="form-horizontal" id="search-form" name="search-form" role="form">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="id">供应商：</label>
								<div class="col-sm-5">
									<select class="form-control" id="provider" name="provider" value="{{$provider}}">
										<option class="form-control" selected value="all">
											-请选择-
										</option>
										<option class="form-control" value="Innstant">
											Innstant
										</option>
										<option class="form-control" value="Tourico">
											Tourico
										</option>
										<option class="form-control" value="HotelBeds">
											HotelBeds
										</option>
										<option class="form-control" value="Expedia">
											Expedia
										</option>
										<option class="form-control" value="Bonotel">
											Bonotel
										</option>
										<option class="form-control" value="Priceline">
											Priceline
										</option>
										<option class="form-control" value="EBooking">
											EBooking
										</option>
									</select>
								</div>
								<label class="col-sm-2 pull-left control-label">订单号</label>
								<div class="col-sm-2">
									<input class="form-control" id="keywords" name="keywords" value="{{$keywords}}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="from">添加时间：</label>
								<div class="col-sm-5">
									<div class="input-group date">
										<input class="form-control" id="from" name="from" value="{{$from}}" placeholder="年-月-日" type="text"><span class="input-group-addon"><i class="fa fa-long-arrow-alt-right"></i></span>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="input-group date btn-block">
										<input class="form-control" id="to" name="to" value="{{$to}}" placeholder="年-月-日" type="text">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="id">处理进度：</label>
								<div class="col-sm-4">
									<select class="form-control" id="processStatus" name="processStatus" value="{{$select_value}}">
										<option class="form-control" selected value="all">
											全部
										</option>
										<option class="form-control" value="0">
											未处理
										</option>
										<option class="form-control" value="1">
											已处理
										</option>
									</select>
								</div>
								<div class="col-sm-5">
									<button class="btn btn-top btn-block" onclick="search()" style="font-size:13px !important;" type="button">
										<i class="fa fa-search"></i> 查询
									</button>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3">
									<button class="btn btn-primary btn-block" id="addRow" style="font-size:13px !important;" type="button">
										<i class="fa fa-plus"></i> 新增
									</button>
								</div>
							</div>
						</form>
					</div>
					<div class="alert alert-warning alert-dismissible" role="alert">
						<button aria-label="Close" class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span></button>
						<strong>&nbsp; &nbsp; Hello! &nbsp; &nbsp;</strong> 请在左侧选择供应商，查看供应商的联系方式。
					</div>
					<div class="col-sm-4 panel panel-default" style="padding:0px !important; border-color:#f2f2f2;">
						<div class="providers" id="Innstant" style="display:none">
							<div class="panel-heading" style="text-align:center; background-color:#f2f2f2;">
								<h3 class="panel-title"><span id="provider_name">Innstant</span></h3>
							</div>
							<div class="panel-body">
								<p style="font-weight:400; font-size:14px;">客服联络电话：+1 646 216 1398</p>
								<p style="font-weight:400; font-size:14px;">客服邮箱：customer.service@innstant.com</p>
							</div>
						</div>
						<div class="providers" id="HotelBeds" style="display:none">
							<div class="panel-heading" style="text-align:center; background-color:#f2f2f2;">
								<h3 class="panel-title">HotelBeds</h3>
							</div>
							<div class="panel-body">
								<p style="font-weight:400; font-size:14px;">美国客服: +1 929 207 0487</p>
								<p style="font-weight:400; font-size:14px;">中国客服: +86 21 3107 1988</p>
								<p style="font-weight:400; font-size:14px;">客服CRC邮箱：online.northamerica@hotelbeds.com</p>
							</div>
						</div>
						<div class="providers" id="Tourico" style="display:none">
							<div class="panel-heading" style="text-align:center; background-color:#f2f2f2;">
								<h3 class="panel-title">Tourico</h3>
							</div>
							<div class="panel-body">
								<p style="font-weight:400; font-size:14px;">北美客服: +1 407 667 8700 或 +1 407 215 9966</p>
								<p style="font-weight:400; font-size:14px;">中国客服: +86 400 120 0251</p>
								<p style="font-weight:400; font-size:14px;">紧急电话: +1 407 212 3535</p>
							</div>
						</div>
						<div class="providers" id="Expedia" style="display:none">
							<div class="panel-heading" style="text-align:center; background-color:#f2f2f2;">
								<h3 class="panel-title">Expedia</h3>
							</div>
							<div class="panel-body">
								<p style="font-weight:400; font-size:14px;">客服邮箱: support@expedia-agents.com</p>
								<p style="font-weight:400; font-size:14px;">北美客服: +1 877 787 7186</p>
								<p style="font-weight:400; font-size:14px; color:red;">致电北美客服对方需要提供账户关联电话，这时候请提供225 803 8239</p>
							</div>
						</div>
						<div class="providers" id="Bonotel" style="display:none">
							<div class="panel-heading" style="text-align:center; background-color:#f2f2f2;">
								<h3 class="panel-title">Bonotel</h3>
							</div>
							<div class="panel-body">
								<p style="font-weight:400; font-size:14px;">客服邮箱: 	bookings@bonotel.com</p>
								<p style="font-weight:400; font-size:14px;">北美客服: +1 (702) 796-5454</p>
								<p style="font-weight:400; font-size:14px;">(WeChat): RogerBangkok -- Director of Sales Asia</p>
							</div>
						</div>
						<div class="providers" id="Priceline" style="display:none">
							<div class="panel-heading" style="text-align:center; background-color:#f2f2f2;">
								<h3 class="panel-title">Priceline</h3>
							</div>
							<div class="panel-body">
								<p style="font-weight:400; font-size:14px;">客服邮箱: 	hotel@cs.travelweb.com</p>
								<p style="font-weight:400; font-size:14px; color:red;">
									邮件客服时，邮件标题包含Hotel,  Confirmation Number, Brief Description of issue
									Example: Hotel, Trip #: 0123456789, Hotel Cannot Find Reservation</p>
								<p style="font-weight:400; font-size:14px;">北美客服: +1 (888) 837-3797</p>
								<p style="font-weight:400; font-size:14px; color:red;">致电北美客服需要告知对方我们是Priceline
									affiliate partner，然后再提供9-11位request number以及酒店名称和酒店的确认号码。</p>
							</div>
						</div>
                        <div class="providers" id="EBooking" style="display:none">
                            <div class="panel-heading" style="text-align:center; background-color:#f2f2f2;">
                                <h3 class="panel-title">EBooking</h3>
                            </div>
                            <div class="panel-body">
                                <p style="font-weight:400; font-size:14px;">问alex</p>
                            </div>
                        </div>
						<div class="providers" id="all">
							<div class="panel-heading" style="text-align:center; background-color:#f2f2f2;">
								<h3 class="panel-title">请选择供应商</h3>
							</div>
							<div class="panel-body">
								<p style="font-weight:400; font-size:14px;">联系人：</p>
								<p style="font-weight:400; font-size:14px;">Email:</p>
								<p style="font-weight:400; font-size:14px;">Cell:</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-8 col-lg-12 ">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					用户反馈问题列表
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table id="dataTables-admin-feedbacks" class="table table-striped table-bordered table-hover dataTables-admin-feedbacks" >
							<thead >
							<tr>
								<th style="width:55px;text-align:center">订单号</th>
								<th style="width:85px;text-align:center">添加/下单日期</th>
								<th style="width:55px;text-align:center">供应商/确认号</th>
								<th style="text-align:center">酒店名称/问题内容</th>
								<th style="text-align:center">处理内容</th>
								<th style="width:55px;text-align:center">订单金额/下单用户</th>
								<th style="width:55px;text-align:center">处理进度</th>
								<th style="width:55px;text-align:center">处理人</th>
							</tr>
							</thead>
							<tbody>
							@if (count($feedbacks) > 0)
								@foreach ($feedbacks as $feedback)
									<tr>
										<td>
											<textarea readonly id="orderId-{{$feedback->porder_id}}" rows="2" cols="4" maxlength="10" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($feedback->porder_id)){{$feedback->porder_id}}@endif</textarea>
										</td>
										<td>
											{{$feedback->created_at}} / {{ $feedback->order->created_at }}
										</td>
										<td>
											<p class="text-danger">
												@if($feedback->order->provider=='HotelBeds')
													HS1001
												@endif
												@if($feedback->order->provider=='Innstant')
													IT1002
												@endif
												@if($feedback->order->provider=='Tourico')
													TO1003
												@endif
												@if($feedback->order->provider=='Expedia')
													EX1004
												@endif
												@if($feedback->order->provider=='Bonotel')
													BT1005
												@endif
												@if($feedback->order->provider=='Priceline')
													PL1006
												@endif
												@if($feedback->order->provider=='EBooking')
													EB1007
												@endif
											</p>
                                            <p style="width:110px">
                                                {{str_replace(","," ",$feedback->order->orderPReference)}}
                                            </p>
										</td>
										<td>
										<p><b>{{$feedback->order->hotelName}}</b></p>
                                						<textarea readonly id="question-{{$feedback->porder_id}}" rows="3" cols="70" maxlength="1000" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow:auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($feedback->question)){{$feedback->question}}@endif</textarea>
                                            @if(Auth::user()->admin == 1 || Auth::user()->id === 10150 || Auth::user()->id === 10603)
                                                <!--questions editable for superuser-->
                                                    <button name="roEdit1" class="label btn" id="roEditBtn1{{$feedback->porder_id}}" onclick="fixFeedback2({{$feedback->porder_id}})">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button name="roSave1" id="roSaveBtn1{{$feedback->porder_id}}" class="label btn hidden" style="margin-top:2px;" onclick="fixFeedbacka({{$feedback->porder_id}})">
                                                        保存更新
                                                    </button>
                                            @else
											<!--data popover for questions channel and finance-->
												<button class="label btn btn-default" data-container="body" data-toggle="popover" data-placement="left" data-content="@if(isset($feedback->question)){{$feedback->question}}@endif">
													<i class="fa fa-eye" aria-hidden="true"></i>
												</button>
											@endif
										</td>
										<td>
											<a href="#" style="text-decoration:none; color:black;" class="tooltip-test" title="{!!$feedback->solution !!}">
												<textarea readonly id="solution-{{$feedback->porder_id}}" rows="3" cols="70" maxlength="1000" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow:hidden; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">@if(isset($feedback->solution)){!!$feedback->solution !!}@endif</textarea></a>
											<button name="roEdit2" class="label btn" id="roEditBtn2{{$feedback->porder_id}}" data-toggle="modal" data-target="#myModal-{{$feedback->porder_id}}" data-whatever="@if(isset($feedback->porder_id)){{$feedback->porder_id}}@endif">
												<i class="fa fa-edit"></i>
											</button>
											<button class="label btn btn-default" data-container="body" data-toggle="popover" data-placement="left" data-content="@if(isset($feedback->solution)){{ str_replace('<hr>', '', $feedback->solution) }}@endif">
												<i class="fa fa-eye" aria-hidden="true"></i>
											</button>
											<!--Modal start-->
											<form method="post" action="{{URL::to('/admin/feedbacks')}}">
												<div class="modal fade" id="myModal-{{$feedback->porder_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
													<div class="modal-dialog" role="document" style="padding-top:70px">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																<h4>New Commend for #<b class="modal-title">{{$feedback->porder_id}}</b></h4>
															</div>
															<div class="modal-body">
																<textarea rows="5" cols="90" maxlength="1000" id="new-solution-{{$feedback->porder_id}}" name="new-solution-{{$feedback->porder_id}}" style="border-style: none; border-color:transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;" autofocus></textarea>
																<button name="roSave2" id="roSaveBtn2{{$feedback->porder_id}}" class="label btn" style="margin-top:2px;" onclick="addNewNote({{$feedback->porder_id}})">
																	保存
																</button>
															</div>
															<div class="modal-footer" style="text-align:left">
															<textarea readonly id="old-solution-{{$feedback->porder_id}}" rows="10" cols="91" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto;line-height: 250%; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;">
																@if(isset($feedback->solution))
																	{!!$feedback->solution !!}
																@endif
															</textarea>
															</div>
														</div>
													</div>
												</div>
											</form>
											<!--Modal end-->
										</td>
										<td>{{$feedback->order->currencyCode. ' ' . $feedback->order->totalPrice}}
                                            <br/>
										@if($feedback->order->partnerId == '11087')
										    <span class="text-danger">(走四方美国C端)</span>
										@else
					                        {{$feedback->order->partnerId}}
										@endif
                                        </td>
										<td>
											<label class="switch">
												@if ($feedback->isDone == 1)
													<input checked="checked" type="checkbox" id="cbisDone{{$feedback->porder_id}}" name="cbisDone{{$feedback->porder_id}}" onclick="cbisDone({{$feedback->porder_id}});">
												@else
													<input type="checkbox" id="cbisDone{{$feedback->porder_id}}" name="cbisDone{{$feedback->porder_id}}" onclick="cbisDone({{$feedback->porder_id}});">
												@endif
												<div class="slider round"></div>
											</label>
										</td>
										<td>
											@if(isset($feedback->order->partner->channel)){{$feedback->order->partner->channel}}@endif
										</td>
									</tr>
								@endforeach
							@endif
							</tbody>
						</table>
						<!-- insert a new row -->
						<table id="newRow" class="table table-striped table-bordered table-hover dataTables-admin-feedbacks">
							<tbody>
							<tr>
								<td>
									<textarea id="orderId-new" rows="2" cols="10" maxlength="20" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;" onkeyup="getOrder()"></textarea>
								</td>
								<td><p id="currentDate"></p></td>
								<td id="newFeedbackProvider"></td>
								<td>
									<textarea id="question-new" rows="3" cols="70" maxlength="1000" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;"></textarea>
								</td>
								<td>
									<textarea id="solution-new" rows="3" cols="70" maxlength="1000" style="border-style: none; border-color: Transparent; background-color:transparent; resize: none; box-shadow: none;overflow: auto; outline: none;-webkit-box-shadow: none;-moz-box-shadow: none;"></textarea>
								</td>
								<td id="newFeedbackTotalPrice"></td>
								<td>
									<label class="switch">
										<input type="checkbox" id="" name="" disabled>
										<div class="slider round" style="cursor: default;"></div>
									</label>
								</td>
								<td id="newFeedbackChannel"></td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')
	<script src="{{ asset('assets/js/admin/plugins.checkorders.js') }}"></script>
    {{--single page script--}}
    @include('admin.includes.js-feedbacks')
@endsection
