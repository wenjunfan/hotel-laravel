
<script type="text/javascript">
    jQuery(function($) {
        $('#from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('.dataTables-statements').DataTable({
            "order": [[6, "desc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: '账号流水'},
                {extend: 'pdf', title: '账号流水'},
                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                          .addClass('compact')
                          .css('font-size', 'inherit');
                    }
                }
            ]
        });
    });

    function search() {
        var partnerId = "/" + $("#partnerId").val();
        var from = $("#from").val();
        var to = $("#to").val();

        if (from != "" && to != "") {
            self.location = "{{url('/admin/statements')}}" + partnerId + "/from/" + from + "/to/" + to;
        }
        else if (from != "") {
            self.location = "{{url('/admin/statements')}}" + partnerId + "/from/" + from;
        }
        else if (to != "") {
            self.location = "{{url('/admin/statements')}}" + partnerId + "/to/" + to;
        }
        else {
            self.location = "{{url('/admin/statements')}}" + partnerId;
        }
    }
</script>