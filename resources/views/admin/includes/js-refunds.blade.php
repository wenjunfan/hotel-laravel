<script type="text/javascript">
    jQuery(function($) {
        $('#from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('.dataTables-refunds').DataTable({
            "order": [[10, "desc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: '信用卡退款记录'},
                {extend: 'pdf', title: '信用卡退款记录'},
                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                          .addClass('compact')
                          .css('font-size', 'inherit');
                    }
                }
            ]
        });
    });

    @foreach($refunds as $refund)
    toLocalTime('{{$refund->id}}', '{{$refund->created_at}}');
    @endforeach

    function toLocalTime(id, t) {
        var local = new Date(t);
        var localTime = local.getFullYear() + "-" + (local.getMonth() + 1) + "-" + local.getDate() + " " + local.getHours() + ":" + local.getMinutes() + ":" + local.getSeconds();
        $("#" + id).html(localTime);
    }

    function search() {
        var partnerId = "/" + $("#partnerId").val();
        var from = $("#from").val();
        var to = $("#to").val();

        if (from != "" && to != "") {
            self.location = "{{url('/admin/refunds')}}" + partnerId + "/from/" + from + "/to/" + to;
        }
        else if (from != "") {
            self.location = "{{url('/admin/refunds')}}" + partnerId + "/from/" + from;
        }
        else if (to != "") {
            self.location = "{{url('/admin/refunds')}}" + partnerId + "/to/" + to;
        }
        else {
            self.location = "{{url('/admin/refunds')}}" + partnerId;
        }
    }
</script>