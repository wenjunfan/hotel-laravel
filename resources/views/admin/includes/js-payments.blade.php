<script type="text/javascript">
    jQuery(function ($) {
        $('#from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('.dataTables-payments').DataTable({
            "order": [[8, "desc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: '扣款记录'},
                {extend: 'pdf', title: '扣款记录'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ]
        });
    });

    @foreach($payments as $payment)
    toLocalTime('{{$payment->id}}', '{{$payment->created_at}}');
    @endforeach

    function toLocalTime(id, t) {
        var local = new Date(t);
        var localTime = local.getFullYear() + "-" + (local.getMonth() + 1) + "-" + local.getDate() + " " + local.getHours() + ":" + local.getMinutes() + ":" + local.getSeconds();
        $("#" + id).html(localTime);
    }

    function search() {
        var partnerId = "/" + $("#partnerId").val();
        var from = $("#from").val();
        var to = $("#to").val();

        if (from != "" && to != "") {
            self.location = "{{url('/admin/payments')}}" + partnerId + "/from/" + from + "/to/" + to;
        }
        else if (from != "") {
            self.location = "{{url('/admin/payments')}}" + partnerId + "/from/" + from;
        }
        else if (to != "") {
            self.location = "{{url('/admin/payments')}}" + partnerId + "/to/" + to;
        }
        else {
            self.location = "{{url('/admin/payments')}}" + partnerId;
        }
    }

</script>