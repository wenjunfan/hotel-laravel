<script type="text/javascript">
    var prchecktemp;
    var prindex = 1;
    jQuery(function ($) {
        $("#processStatus").val("{{$select_value}}");
        $("#isFollow").val("{{$isFollow}}");
        $('#phone').mask('999-999-9999', {placeholder: 'XXX-XXX-XXXX'});
        $('#checkin_one').datepicker({
            startDate: '0d',
            todayBtn: 'linked',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
        $('#checkout_one').datepicker({
            startDate: '1d',
            todayBtn: 'linked',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
        $('#checkin_two').datepicker({
            startDate: '0d',
            todayBtn: 'linked',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
        $('#checkout_two').datepicker({
            startDate: '1d',
            todayBtn: 'linked',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: 'yyyy-mm-dd',
        });
        $('#from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });
        groupRequestSetTime();
        $('.dataTables-groupRequests').DataTable({
            "order": [[0, "desc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {
                    extend: 'csv',
                    title: '团房请求'
                },
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ]
        });
    });

    $("form#grouprequest").submit(function () {
        $(this).find(':input[type=submit]').prop('disabled', true);
    });

    function addNewNote (id) {
        var newSolutions = document.getElementById('new-solution-' + id).value;
        FullscreenSpinner.create();

        var authUser = '{{Auth::user()->id}}';
        if (authUser === '10603') {
            authUser = 668;
        } else if (authUser === '10150') {
            authUser = 628;
        } else if (authUser === '10672') {
            authUser = 550;
        } else if (authUser === '10602') {
            authUser = 106;
        } else if (authUser === '10057') {
            authUser = 22;
        } else {
            authUser = '{{Auth::user()->id}}';
        }
        var newSolution = formatDate(new Date()) + newSolutions + '  (' + authUser + ')';

        $.post("{{url('/group-room-request/update')}}", {
            '_token': window.usitrip.csrfToken,
            'id': id,
            'solution': newSolution
        }, function (data) {
            FullscreenSpinner.destroy();
            if (data.success) {
                $('#old-solution-' + id).text(newSolution + $('#old-solution-' + id).text());
                swal("更新成功", data.message, "success");
                location.reload();
                document.getElementById('solution' + id).readOnly = true;
                $("#roEditBtn" + id).toggleClass("hidden");
                $("#roSaveBtn" + id).toggleClass("hidden");
            } else {
                swal("更新失败", data.message, "error");
            }
        });
    }

    function fixGroupRequestA (id) {
        var solution = document.getElementById('solution' + id).value;
        var amount = document.getElementById('amount' + id).value;

        FullscreenSpinner.create();

        $.post("{{url('/group-room-request/update')}}", {
            '_token': window.usitrip.csrfToken,
            'id': id,
            'solution': solution,
            'amount': amount
        }, function (data) {
            FullscreenSpinner.destroy();
            if (data.success) {
                swal("更新成功", data.message, "success");
                location.reload();

                document.getElementById('amount' + id).readOnly = true;
                $("#roEditBtn1" + id).toggleClass("hidden");
                $("#roSaveBtn1" + id).toggleClass("hidden");
            } else {
                swal("更新失败", data.message, "error");
            }
        });
    }

    function formatDate (date) {
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "June", "July",
            "Aug", "Sept", "Oct",
            "Nov", "Dec"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var month = monthIndex + 1;
        var year = date.getFullYear();
        var hour = date.getHours();
        var minute = date.getMinutes();
        return day + '-' + monthNames[monthIndex] + '-' + year + ' ' + hour + ':' + minute + ' ';
        // return month + '/' + day+ ' ';
    }

    function fixgroupRequest1 (id) {
        $("#roEditBtn" + id).toggleClass("hidden");
        $("#roSaveBtn" + id).toggleClass("hidden");
        $("#solution" + id).val($("#solution" + id).val() + "\n" + formatDate(new Date()));
    }

    function fixgroupRequest2 (id) {
        $("#roEditBtn1" + id).toggleClass("hidden");
        $("#roSaveBtn1" + id).toggleClass("hidden");
    }

    $('[name="roEdit"]').on('click', function () {
        var prev = $(this).prev('textarea'),
            ro = prev.prop('readonly');
        prev.prop('readonly', !ro).focus();
    });

    $('[name="roEdit1"]').on('click', function () {
        var prev = $(this).prev('textarea'),
            ro = prev.prop('readonly');
        prev.prop('readonly', !ro).focus();
    });

    function search () {
        var isFollow = $("#isFollow").val();
        var from = $("#from").val();
        var to = $("#to").val();
        var processStatus = $("#processStatus").val();

        if (from != "" && to != "") {
            self.location = window.usitrip.basePath + "admin/groupRequests/isFollow/" + isFollow + "/from/" + from + "/to/" + to + "/processStatus/" + processStatus;
        } else if (from != "") {
            self.location = window.usitrip.basePath + "admin/groupRequests/isFollow/" + isFollow + "/from/" + from + "/processStatus/" + processStatus;
        } else if (to != "") {
            self.location = window.usitrip.basePath + "admin/groupRequests/isFollow/" + isFollow + "/to/" + to + "/processStatus/" + processStatus;
        } else {
            self.location = window.usitrip.basePath + "admin/groupRequests/isFollow/" + isFollow + "/processStatus/" + processStatus;
        }
    }

    function isFollowedOrder (id) {
        var checkbox = document.getElementById("isFollowedOrder" + id);
        if (checkbox.checked === true) {
            FullscreenSpinner.create();
            $.post("{{url('/group-room-request/followup')}}", {
                '_token': window.usitrip.csrfToken,
                'id': id,
                'rep': "{{Auth::user()->id}}",
                'isFollow': 1
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    swal("更新成功", data.message, "success");
                    location.reload();
                } else {
                    swal("更新失败", data.message, "error");
                    checkbox.checked = false;
                }
            });
        } else {
            checkbox.checked = true;
            var click = 0;
            swal({
                title: "确认取消吗？",
                text: "",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "返回",
                closeOnConfirm: false,
                confirmButtonColor: "#007FF8",
                confirmButtonText: "确定",
                closeOnConfirm: false
            }, function () {
                click = 1;
                FullscreenSpinner.create();
                $.post("{{url('/group-room-request/followup')}}", {
                    '_token': window.usitrip.csrfToken,
                    'id': id,
                    'rep': "no one yet",
                    'isFollow': 0
                }, function (data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal("更新成功", data.message, "success");
                        location.reload();
                        checkbox.checked = false;
                    } else {
                        swal("更新失败", data.message, "error");
                    }
                });
            });
        }
    }

    function isDoneOrder (id) {
        var checkbox = document.getElementById("isDoneOrder" + id);
        if (checkbox.checked == true) {
            FullscreenSpinner.create();
            $.post("{{url('/group-room-request/complete')}}", {
                '_token': window.usitrip.csrfToken,
                'id': id,
                'isDone': 1
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    swal("更新成功", data.message, "success");
                    location.reload();
                } else {
                    swal("更新失败", data.message, "error");
                    checkbox.checked = false;
                }
            });
        } else {
            checkbox.checked = true;

            var click = 0;
            swal({
                title: "确认取消吗？",
                text: "",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "返回",
                closeOnConfirm: false,
                confirmButtonColor: "#007FF8",
                confirmButtonText: "确定",
                closeOnConfirm: false
            }, function () {
                click = 1;

                FullscreenSpinner.create();
                $.post("{{url('/group-room-request/complete')}}", {
                    '_token': window.usitrip.csrfToken,
                    'id': id,
                    'isDone': 0
                }, function (data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal("更新成功", data.message, "success");
                        location.reload();
                        checkbox.checked = false;
                    } else {
                        swal("更新失败", data.message, "error");
                    }
                });
            });
        }
    }

</script>