<script type="text/javascript">
    jQuery(function ($) {
        $('#from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });
        $('#checkin_from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#checkin_to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });
        $("#orderStatus").val("{{$select_value}}");
        $("#financeStatus").val("{{$financeStatus}}");
        $('.dataTables-admin-orders').DataTable({
            "order": [[2, "desc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {
                    extend: 'csv',
                    title: '订单记录 - ' + formatDate(new Date())
                },
                {
                    extend: 'excel',
                    title: '订单记录 -' + formatDate(new Date())
                },
                {
                    extend: 'pdf',
                    title: '订单记录 -' + formatDate(new Date())
                },
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ]
        });

        var selector = $('.chosen-select');
        selector.chosen({width: "100%"});
        selector.trigger('chosen:updated');
    });

    function localize(t) {
        var h = t.replace("T", " ");
        h = h.substring(0, 19);
	    return h;

        // var hotel = new Date(h);
        // var local = new Date(t);
        // return "酒店时间：" + hotel.getFullYear() + "年" + (hotel.getMonth() + 1) + "月" + hotel.getDate() + "日" + hotel.getHours() + "点" + hotel.getMinutes() + "分（"
        // +  "本地时间：" + local.getFullYear() + "年" + (local.getMonth() + 1) + "月" + local.getDate() + "日" + local.getHours() + "点" + local.getMinutes() + "分）"
    }

    function today(t) {
        var h = t.replace("T", " ");
        var hotel = new Date(h);
        var today = new Date();

        return hotel > today;
    }

    function convertCancelPolicies(reference, payment_type, room, policies, currencyCode) {//Arthur 050517
        if (policies === "" || policies == null) {
            return;
        }
        try {
            var html = $("#" + reference).html();
	        var obj = jQuery.parseJSON(policies);
	        var html1 = $("#o" + reference).html();

            $.each(obj, function (i, o) {
	            var amount = o.amount;
	            var end = o.end;

                if (html !== "") {
                    html = "";
                }

                if (amount === 0) {
                    html += localize(end) + "（酒店当地时间）前可以免费取消<br>";
                } else {
                    html += localize(end) + "（酒店当地时间）前取消需缴纳" + currencyCode + amount + "<br>";
                }

                $("#" + reference).html(html);
	            var cancel = new Date(end).getTime();
	            var now = new Date().getTime();
                if (cancel > now) {
                    html1 = "	<span id=\"cancel_" + reference + "\"><a onclick=\"cancelOrder('" + reference + "', \'" + payment_type + "\')\" class=\"\" type=\"button\">取消订单</a></span>"//Arthur 050517
                }

                $("#o" + reference).html(html1);
            });
        }
        catch (err) {
        }
    }

    function search () {
	    var partnerId = $("#partnerId").val();
	    var from = $("#from").val();
	    var to = $("#to").val();
	    var checkin_from = $("#checkin_from").val();
	    var checkin_to = $("#checkin_to").val();
	    var keywords = $("#keywords").val();
	    var orderStatus = $("#orderStatus").val();
	    var financeStatus = $("#financeStatus").val();
	    var hotelName = $("#hotelName").val();
	    var guestName = $("#guestName").val();
	    var orderPReference = $("#orderPReference").val();
	    self.location = "{{url('admin/orders')}}"
			+'?partnerId=' + partnerId
			+ '&hotelName=' + hotelName
			+ '&guestName=' + guestName
		    + '&from=' + from
		    + '&to=' + to
		    + '&checkin_from=' + checkin_from
		    + '&checkin_to=' + checkin_to
		    + '&keywords=' + keywords
		    + '&orderStatus=' + orderStatus
		    + '&financeStatus=' + financeStatus
		    + '&orderPReference=' + orderPReference
	    ;
    }

    function refund(id) {
        swal({
            title: "确认退款吗？",
            text: "退款操作成功后不可撤销.",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "取消退款",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确认退款",
            closeOnConfirm: false
        }, function () {
            FullscreenSpinner.create();
            $.post("{{url('/admin/refunds/refundConfirm')}}", {
                '_token': $("input[name='_token']").val(),
                'id': id
            }, function (data) {
                setTimeout(function () {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal({
                            title: "退款操作成功！",
                            type: "success"
                        }, function () {
                            location.reload();
                        });
                    }
                    else {
                        swal("退款失败", data.message, "error");
                    }
                }, 1000);

            });
        });
    }

    // 获取账号信息
    function getPartnerInfo(id) {
        $('#partnerInfoModalTitle').html('分销商编号：'+id);
        $('#partnerInfoModalBody').html('<h3>Loading...</h3>');
        $('#partnerInfoModal').modal('show');
        $.ajax({
            type: 'GET',
            url: '/partner-info/show/'+id,
            id: id,
            dataType: 'json',
            success: function(data) {
                var files = '上传文件';
                if (data.partner.files !== '')
                    files = '查看+管理文件';
                if(data){
                    var html =
                        '<tr>' +
                        '   <td>用戶名</td>' +
                        '   <td>'+data.partner.name+'</td>' +
                        '</tr><tr>' +
                        '   <td>公司名</td>' +
                        '   <td>'+data.partner.company_name+'</td>' +
                        '</tr><tr>' +
                        '   <td>邮箱</td>' +
                        '   <td>'+data.partner.email+'</td>' +
                        '</tr><tr>' +
                        '   <td>电话</td>' +
                        '   <td>'+data.partner.company_phone+'</td>' +
                        '</tr><tr>' +
                        '   <td>创建时间</td>' +
                        '   <td>'+data.partner.created_at+'</td>' +
                        '</tr><tr>' +
                        '   <td>用戶地区(US可用信用卡)</td>' +
                        '   <td>'+data.partner.Country_Code+'</td>' +
                        '</tr><tr>' +
                        '   <td>佣金比例</td>' +
                        '   <td>'+data.partner.priceRatio+'</td>' +
                        '</tr><tr>' +
                        '   <td>付款方式</td>' +
                        '   <td>'+data.partner.paymentType+'</td>' +
                        '</tr><tr>' +
                        '   <td>持卡人</td>' +
                        '   <td>'+data.partner.cardholder+'</td>' +
                        '</tr><tr>' +
                        '   <td>消费总额</td>' +
                        '   <td>'+data.partner.balance+'</td>' +
                        '</tr>' +
                        '<tr>' +
                        '   <td>财务审核到账/未到账</td>' +
                        '   <td>'+data.partner.postpay_paid+'/'+data.partner.postpay_unpaid+'</td>' +
                        '</tr>' +
                        '<tr>'+
                        '<td>上传证件</td>' +
                        '<td><a target="_blank" href="/profile/files/'+data.partner.id+'">'+files+'</a></td>' +
                        '</tr>' +
                        '<tr>' +
                        '   <td>备注</td>' +
                        '   <td>'+data.partner.remark+'</td>' +
                        '</tr>'
                    ;
                    $('#partnerInfoModalBody').html(html);
                }
            }
        });
    }

    // 获取入住人姓名
    function getGuestName(id) {
      $('#guestNameModalTitle').html('订单号： loading...');
      $('#guestNameModalBody').html('<h3>Loading...</h3>');
      $('#guestNameModal').modal('show');
      $.ajax({
        type: 'GET',
        url: '/guest-name/show/'+id,
        id: id,
        dataType: 'json',
        success: function(data) {
          if(data){
            $('#guestNameModalTitle').html('订单号： ' + id);
            $('.guestNameModalTitle').val(id);
            var html = '';
            for (var i = 0; i < data.guests.length; i++) {
              html +=
                  '   <tr>' +
                  '      <td>' +
                  '        <input class="form-control" name="'+i+'_fn" value="'+data.guests[i].firstName+'"/>' +
                  '      </td>' +
                  '      <td>' +
                  '        <input class="form-control" name="'+i+'_ln" value="'+data.guests[i].lastName+'"/>' +
                  '      </td>' +
                  '   </tr>';
            }
            $('#guestNameModalBody').html(html);
          }
        }
      });
    }

    // 保存入住人更改的姓名
    function saveGuestName() {
      FullscreenSpinner.create();
      event.preventDefault();
      $.get("{{url('/guest-name/update')}}",$('.guest-name-table input').serialize(),
          function(data){
            FullscreenSpinner.destroy();
            if (data.success) {
              swal({
                title: "更新成功",
                text: data.message,
                type: "success"
              }, function () {
                location.reload();
              });
            }
            else {
              swal("更新失败", data.message, "error");
            }
          });
    }

    function cancelOrder(id,type) {
        if(type === 'special'){
            swal({
                  title: "请输入您要特殊取消的订单号",
                  text: "该功能仅限于给客户发送取消邮件，以及将数据库中的订单状态改为已取消，您还需要在feedback填写该订单处理状态，确认上游状态，做相应退款处理（第二种情况需收取$25手续费）",
                  type: "input",
                  showCancelButton: true,
                  cancelButtonText: "取消",
                  confirmButtonText: "确定",
                  closeOnConfirm: false,
                  animation: "slide-from-top",
              },
              function (inputValue) {
                  if (inputValue === false) {
                      return false;
                  }else if (inputValue.trim() === "" || isNaN(inputValue)) {
                      swal.showInputError("请填写订单号");
                      return false;
                  }else{
                    FullscreenSpinner.create();
                    event.preventDefault();
                    $('.confirm').text('loading ...');
                    $('.confirm').attr('disabled',true);
                    $.post("{{url('/order/cancel')}}", {
                      '_token': $("input[name='_token']").val(),
                      'id': inputValue.replace(' ',''),
                      'type': type,
                    }, function (data) {
                      $('.confirm').attr('disabled',false);
                      FullscreenSpinner.destroy();
                      if (data.success) {
                        swal({
                          title: "已取消",
                          text: data.message,
                          type: "success"
                        }, function () {
                          location.reload();
                        });
                      }
                      else {
                        swal("取消失败", data.message, "error");
                      }
                    });
                  }
              });
        }else{
            swal({
                title: "确认要取消这个订单吗？",
                text: "请仔细查看这个酒店的取消条款，超过酒店取消条款时间，酒店会收取相应的费用，请确认后再取消订单。",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "确认取消",
                confirmButtonColor: "#007FF8",
                confirmButtonText: "不取消",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (!isConfirm) {
                    FullscreenSpinner.create();
                    $.post("{{url('/order/cancel')}}", {
                        '_token': $("input[name='_token']").val(),
                        'id': id,
                        'type': type,
                    }, function (data) {
                        FullscreenSpinner.destroy();
                        if (data.success) {
                            swal({
                                title: "已取消",
                                text: data.message,
                                type: "success"
                            }, function () {
                                location.reload();
                            });
                        }
                        else {
                            swal("取消失败", data.message, "error");
                        }
                    });
                }
            });
        }
    }

    function formatDate(date) {
      var monthNames = [
        "Jan", "Feb", "Mar",
        "Apr", "May", "June", "July",
        "Aug", "Sept", "Oct",
        "Nov", "Dec"
      ];

      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();
      var hour = date.getHours();
      var minute = date.getMinutes();
      return day + '-' + monthNames[monthIndex] + '-' + year + ' '+ hour + ':' +minute + ' ';
    }

    function openOpCommendModal(id) {
      $('#addCommentModalTitle').text(id);
      $('#op_remark').html($('#channel_remark'+id).html());
      $('#addOpCommentModal').modal('show');
    }

    function updateOpCommend() {
      var id = $('#addCommentModalTitle').html();
      var authUser = '{{Auth::user()->id}}';
      if (authUser === '11313') {
        authUser = '668';
      } else if (authUser === '10048') {
        authUser = '119';
      }
      var old_remark = $('#op_remark').html().trim();
      var channel_remark = formatDate(new Date()) + $('#addComment').val() + " (" + authUser + ")";
      if(old_remark !== ''){
        channel_remark += '&#013;' + old_remark;
      }

      FullscreenSpinner.create();
      $.post("{{url('/order-channel/update')}}", {
        '_token': $("input[name='_token']").val(),
        'id': id,
        'channel_remark': channel_remark
      }, function (data) {
        FullscreenSpinner.destroy();
        if (data.success) {
          swal("更新成功", data.message, "success");
          $('#addOpCommentModal').modal('hide');
          $('#channel_remark'+id).html(channel_remark);
          $('#addComment').val("");
        }
        else {
          swal("更新失败", data.message, "error");
        }
      });
    }
    //in case customers need email to be send again with information
    function sendAnotherEmail(id){
        FullscreenSpinner.create();
        $.post("{{url('/order/email/resend')}}/"+id, {
            '_token': $("input[name='_token']").val(),
            'id': id
        }, function (data) {
            FullscreenSpinner.destroy();
            if (data.success) {
                swal("已发送成功", data.message, "success");
            } else {
                swal("发送失败", data.message, "error");
            }
        });
    }

    function checkOrder(id) {
	    FullscreenSpinner.create();
	    $.post("{{url('/order/check')}}", {
		    '_token': $("input[name='_token']").val(),
		    'id': id
	    }, function (data) {
		    FullscreenSpinner.destroy();
		    if (data.success) {
			    location.reload();
		    } else {
			    swal("已更新至最新状态", data.message, "success");
		    }
	    });
    }
</script>