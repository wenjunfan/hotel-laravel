
<style>
    table#newRow{
        display:none;
    }

    .popover-content{
        padding: 9px 14px;
        background:lightgreen;
        font-family: Roboto, Arial, 'Lucida Grande', 'Microsoft Yahei', 'Hiragino Sans GB', 'Hiragino Sans GB W3', SimSun, STHeiti;
        max-width:500px !important;
        line-height:1.6;
    }

    .red {
        background-color: red;
        color: white;
    }

    .white {
        background-color: white;
        color: black;
    }

    .green {
        background-color: green;
        color: white;
    }

    .yellow {
        background-color: #f1791e;
        color: white;
    }

    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        display: none;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }
    .modal-body .form-horizontal .col-sm-2,
    .modal-body .form-horizontal .col-sm-10 {
        width: 100%
    }
    .modal-body .form-horizontal .control-label {
        text-align: left;
    }
    .modal-body .form-horizontal .col-sm-offset-2 {
        margin-left: 15px;
    }
    #changetxt:hover { color:#f1791e; }
    #changetxt{color:#767676;}
    .anotherdestination {
        height:auto;
        overflow:hidden;
        transition:opacity 0.5s linear, max-height 0.5s linear;
        opacity:0;
        max-height:0;
    }
    #hidemorerequest[type=checkbox]:checked + .anotherdestination {
        opacity:1;
        max-height:99em;
        border:none;
    }
    .selectpicker {
        background: url("//www.connectedtoindia.com/assets/images/dropdown-arrow1.png") no-repeat scroll right center transparent;
        font-size: 14px;
        font-weight: 400;
        padding-left: 6px;
        -webkit-border-radius: 4px;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        font-family: Roboto, Arial, 'Lucida Grande', 'Microsoft Yahei', 'Hiragino Sans GB', 'Hiragino Sans GB W3', SimSun, STHeiti;
    }
    input:checked + .slider {
        background-color: #32CD32;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }
    select:invalid { color: darkgrey; }
</style>