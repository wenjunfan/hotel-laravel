<script type="text/javascript">
    // this function can be used to delay a ajax call, like waiting for user to finish typing
    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();

    var newSaveButton = '';

    jQuery(function ($) {
      $.ajaxSetup({
        headers: {
          'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
      });
      $("#processStatus").val("{{$select_value}}");
      $("#provider").val("{{$provider}}");

      $('#from').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: "yyyy-mm-dd"
      });
      $('#to').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: "yyyy-mm-dd"
      });

      $('select').change(function() {
        $('#provider_name').html($(this).val());
        var origBGColor = $(this).attr("class");
        $(this).removeClass($(this).attr('class')).addClass($(":selected", this).attr('class'));
      });

      $('#provider').change(function() {
        $('.providers').hide();
        $('#' + $(this).val()).show();
      });

      if($('#provider').val() != 'all'){
        $('#provider_name').html($('#provider').val());
        $('.providers').hide();
        $('#' + $('#provider').val()).show();
      }

      var feedbacksTable =$('#dataTables-admin-feedbacks').DataTable({
        //"order": [[1, "desc"]], // if enable order here, the new added row will be shown on the bottom of the table
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
//					{extend: 'copy'},
          {extend: 'csv', title: '客户反馈'},
//					{extend: 'excel', title: '客户反馈'},
//					{extend: 'pdf', title: '客户反馈'},
          {extend: 'print',
            customize: function (win){
              $(win.document.body).addClass('white-bg');
              $(win.document.body).css('font-size', '10px');
              $(win.document.body).find('table')
              .addClass('compact')
              .css('font-size', 'inherit');
            }
          }
        ],
        "aaSorting": [[ 1, "desc" ]]
      });

      $('#addRow').click(function() {
        var rowHtml = $("#newRow").find("tr")[0].outerHTML
        //feedbacksTable.row.add($(rowHtml)).draw();
        feedbacksTable.row.add($(rowHtml)).draw( false );

        feedbacksTable.order([1, 'asc']).draw();
        feedbacksTable.order([0, 'asc']).draw();
      });

      $('[name="roEdit"]').on('click', function() {
        var prev = $(this).prev('textarea'),
            ro = prev.prop('readonly');
        prev.prop('readonly', !ro).focus();
      });
      $('[name="roEdit1"]').on('click', function() {
        var prev = $(this).prev('textarea'),
            ro = prev.prop('readonly');
        prev.prop('readonly', !ro).focus();
      });
      $('[name="roEdit2"]').on('click', function() {
        var prev = $(this).prev('textarea'),
            ro = prev.prop('readonly');
        prev.prop('readonly', !ro).focus();

      });
    });

    function getOrder() {
        var newFeedbackOrderId = $('#orderId-new').val();
        $.ajax({
            type: 'GET',
            url: '/order/show/'+newFeedbackOrderId,
            dataType: 'json',
            success: function(order) {
                delay(function(){
                    if (order) {
                        $('#newFeedbackProvider').html(order.provider);
                        $('#newFeedbackTotalPrice').html('$' + order.totalPrice);
                        if ($('#orderId-new').next('button').length === 0) {
                            newSaveButton = $('<button name="roSave" class="label btn" id="roSaveBtn-new" style="margin-top:2px;" onclick="fixFeedback(' + order.id + ', true)">保存更新</button>');
                            $('#orderId-new').after(newSaveButton);
                        }
                    }
                }, 1000);
            }
        });
    }
    function ShowLocalDate() {
        var dNow = new Date();
        var localdate = dNow.getFullYear() + '-' + (dNow.getMonth() + 1) + '-' + dNow.getDate() + ' ' + dNow.getHours() + ':' + dNow.getMinutes() + ':' + dNow.getSeconds();
        $('#currentDate').html(localdate)
    }
    ShowLocalDate();


    function fixFeedback(orderId, isNewRecord) {
        var newSolutions = document.getElementById('solution-new').value;
        var authUser ='{{Auth::user()->id}}';

        if(authUser === '10603'){
            authUser= 668;
        }else if(authUser === '10150'){
            authUser= 628;
        }else if(authUser === '10672'){
            authUser= 550;
        }else if(authUser === '10602'){
            authUser= 106;
        }else if(authUser === '10057'){
            authUser= 22;
        }else{
            authUser='{{Auth::user()->id}}';
        }
        var newSolution = formatDate(new Date()) + newSolutions + '  (' + authUser +')';
        if (isNewRecord) {
            var data = {
                '_token': "{{ csrf_token() }}",
                orderId: $('#orderId-new').val(),
                question: $('#question-new').val(),
                solution: newSolution
            }
        } else {
            var data = {
                '_token':"{{ csrf_token() }}",
                orderId: orderId,
                question: $('#question-' + orderId).val(),
                solution: $('#solution-' + orderId).val()
            }
        }

        FullscreenSpinner.create();
        $.post("{{url('/admin/feedback/update')}}", data, function (data) {
            FullscreenSpinner.destroy();
            if (data.success) {
                swal("更新成功", data.message, "success");
                location.reload();
                if (isNewRecord) {
                    $('#orderId-new').attr('readOnly', true);
                    $('#question-new').attr('readOnly', true);
                    $('#solution-new').attr('readOnly', true);
                    $(newSaveButton).hide();
                } else {
                    $('#orderId-' + orderId).attr('readOnly', true);
                    $("#roEditBtn" + orderId).toggleClass("hidden");
                    $("#roSaveBtn" + orderId).toggleClass("hidden");
                }
            } else {
                swal("更新失败", data.message, "error");
            }
        });
    }

    function fixFeedbacka(orderId) {
        var orderId = document.getElementById('orderId-' + orderId).value;
        var question = document.getElementById('question-' + orderId).value;
        var solution = document.getElementById('solution-' + orderId).value;
        FullscreenSpinner.create();
        $.post("{{url('/admin/feedback/update')}}", {
            '_token': "{{ csrf_token() }}",
            orderId: orderId,
            question: $('#question-' + orderId).val(),
            solution: '',
        }, function (data) {
            FullscreenSpinner.destroy();
            if (data.success) {
                swal("更新成功", data.message, "success");
                location.reload();
                document.getElementById('question-' + orderId).readOnly = true;
                $("#roEditBtn1" + orderId).toggleClass("hidden");
                $("#roSaveBtn1" + orderId).toggleClass("hidden");
            } else {
                swal("更新失败", data.message, "error");
            }
        });
    }

    function addNewNote(orderId) {
        var newSolutions = document.getElementById('new-solution-' + orderId).value;
        var authUser ='{{Auth::user()->id}}';

      if(authUser === '10603'){
        authUser= 668;
      }else if(authUser === '10150'){
        authUser= 628;
      }else if(authUser === '10672'){
        authUser= 550;
      }else if(authUser === '10602'){
        authUser= 106;
      }else if(authUser === '10057'){
        authUser= 22;
      }else{
        authUser='{{Auth::user()->id}}';
      }

        var newSolution = formatDate(new Date()) + newSolutions + '  (' + authUser +')';

        $.post("{{url('/admin/feedback/update')}}", {
            _token: "{{ csrf_token() }}",
            orderId: orderId,
            solution: newSolution
        }, function (data) {
            if (data.success) {
                //$("#new-solution-" + orderId ).val("\n" + formatDate(new Date()) + $("#new-solution-" + orderId ).val() +{{Auth::user()->id}}.val());
                $('#old-solution-'+orderId).text(newSolution + $('#old-solution-' + orderId).text());
                swal("更新成功", data.message, "success");
                document.getElementById('solution-' + orderId).readOnly = true;
                $("#roEditBtn2" + orderId).toggleClass("hidden");
                $("#roSaveBtn2" + orderId).toggleClass("hidden");
                location.reload();
            } else {
                swal("更新失败", data.message, "error");
            }
        });
    }

    function formatDate(date) {
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "June", "July",
            "Aug", "Sept", "Oct",
            "Nov", "Dec"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var month = monthIndex+1;
        var year = date.getFullYear();
        var hour = date.getHours();
        var minute = date.getMinutes();
        return day + '-' + monthNames[monthIndex] + '-' + year + ' '+ hour + ':' +minute + ' ';
        // return month + '/' + day+ '/'+year;
    }
    function fixFeedback1(orderId) {
        $("#roEditBtn" + orderId).toggleClass("hidden");
        $("#roSaveBtn" + orderId).toggleClass("hidden");
    }

    function fixFeedback2(orderId) {
        $("#roEditBtn1" + orderId).toggleClass("hidden");
        $("#roSaveBtn1" + orderId).toggleClass("hidden");
    }

    function fixFeedback3(orderId) {
        $("#roEditBtn2" + orderId).toggleClass("hidden");
        $("#roSaveBtn2" + orderId).toggleClass("hidden");
        //("#solution-" + orderId ).val($("#solution-" + orderId ).val() + "\n" + formatDate(new Date()) + {{Auth::user()->id}}+ ": " );
    }

    function search() {
        var provider = $("#provider").val();
        var from = $("#from").val();
        var to = $("#to").val();
        var keywords = $("#keywords").val();
        var processStatus = $("#processStatus").val();

        if (keywords != "") {
            self.location = "{{url('/admin/feedbacks')}}" + "/keywords/" + keywords;
        } else {
            if (from != "" && to != "") {
                self.location = "{{url('/admin/feedbacks')}}" + "/" + provider + "/from/" + from + "/to/" + to + "/processStatus/" + processStatus;
            } else if (from != "") {
                self.location = "{{url('/admin/feedbacks')}}" + "/" + provider + "/from/" + from + "/processStatus/" + processStatus;
            } else if (to != "") {
                self.location = "{{url('/admin/feedbacks')}}" + "/" + provider + "/to/" + to + "/processStatus/" + processStatus;
            } else {
                self.location = "{{url('/admin/feedbacks')}}" + "/" + provider + "/processStatus/" + processStatus;
            }
        }
    }

    function cbisDone(orderId) {
        var checkbox = document.getElementById("cbisDone" + orderId);
        //alert(checkbox.checked);
        if (checkbox.checked == true) {
            FullscreenSpinner.create();
            $.post("{{url('/admin/feedback/update')}}", {
                '_token': "{{ csrf_token() }}",
                'orderId': orderId,
                'isDone': 1
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    swal("更新成功", data.message, "success");
                    location.reload();
                } else {
                    swal("更新失败", data.message, "error");
                    checkbox.checked = false;
                }
            });
        } else {
            checkbox.checked = true;
            //swal
            var click = 0;
            swal({
                title: "确认取消吗？",
                text: "",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "返回",
                closeOnConfirm: false,
                confirmButtonColor: "#007FF8",
                confirmButtonText: "确定",
                closeOnConfirm: false
            }, function () {
                click = 1;
                FullscreenSpinner.create();
                $.post("{{url('/admin/feedback/update')}}", {
                    '_token': "{{ csrf_token() }}",
                    'orderId': orderId,
                    'isDone': 0
                }, function (data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal("更新成功", data.message, "success");
                        location.reload();
                        checkbox.checked = false;
                    } else {
                        swal("更新失败", data.message, "error");
                    }
                });
            });
        }
    }
</script>