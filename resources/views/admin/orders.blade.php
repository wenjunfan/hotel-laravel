@extends('layouts.admin')

@section('title', '订单管理')

@section('css')
    <link href="{{ asset('assets/css/admin/plugins.orders.css') }}" rel="stylesheet">
    <style>
        .shorter {
            width: 35px !important;
        }
        .table > tbody > tr > td{
            padding: 8px 10px;
        }
        .opcommend{
            border-style: none;
            border-color:Transparent;
            background-color:transparent;
            resize: none;
            box-shadow: none;
            overflow: auto;
            outline: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            cursor: pointer;
            text-align:left;
        }
        textarea#op_remark {
            width: 100%;
            border: none;
            display: inline-table;
            font-size: 14px;
            resize: none;
            height: 100%;
            margin-bottom: 35px;
            cursor:pointer;
        }
        #partnerInfoModalBody td:first-child {
            width: 150px;
        }
    </style>
@endsection

@section('content')
	<div class="row animated fadeInRight">
        {{--start earch--}}
		<div class="col-lg-10" style="margin-bottom:0px;">
			<div class="ibox float-e-margins">
				<div class="ibox-noborder" style="margin-top:20px;">
					<strong> 查询订单 </strong>
				</div>
				<div>
					<div class="ibox-content">
						<form class="form-horizontal" role="form" id="search-form" name="search-form">
							{!! csrf_field() !!}
                            <div class="form-group">
                                <div class="col-md-2">
                                <label class="control-label" for="id">公司名称</label>
                                <div>
                                    <select id="partnerId" name="partnerId" placeholder="公司名称" class="chosen-select form-control" tabindex="2">
                                        <option value="">全部公司</option>
                                        @foreach($users as $user)
                                            @if($user->active)
                                                <option value="{{$user->id}}" {{$partnerId == $user->id ? "selected" : ""}}>{{$user->company_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                </div>
                                <div class="col-md-2">
                                <label class="control-label">订单状态</label>
                                <div>
                                    <select class="form-control" id="orderStatus" name="orderStatus" value="{{$select_value}}">
                                        <option value="6">全部</option>
                                        <option value="1">处理中</option>
                                        <option value="2">已确认</option>
                                        <option value="3">已取消退款中</option>
                                        <option value="4">已取消</option>
                                        <option value="7">EB拒绝</option>
                                        <option value="5">失败</option>
                                    </select>
                                </div>
                                </div>

                                <div class="col-md-2">
                                <label class="control-label">审核状态</label>
                                <div>
                                    <select class="form-control" id="financeStatus" name="financeStatus" value="{{$financeStatus}}">
                                        <option value="5">全部</option>
                                        <option value="1">已审</option>
                                        <option value="2">未审</option>
                                        <option value="3">已退</option>
                                        <option value="4">需退</option>
                                    </select>
                                </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="control-label" for="hotelName">酒店名
                                    </label>
                                    <div>
                                        <input class="form-control" type="text" id="hotelName" name="hotelName"
                                               value="{{$hotelName}}"/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="control-label" for="guestName">入住人名(姓/名)
                                    </label>
                                    <div>
                                        <input class="form-control" type="text" id="guestName" name="guestName"
                                               value="{{$guestName}}"/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="control-label" for="orderPReference">供应商确认号
                                    </label>
                                    <div>
                                        <input type="text" class="form-control" id="orderPReference" name="orderPReference"
                                               value="{{$orderPReference}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                <label class="control-label" for="from to">下单日期</label>
                                <div>
                                    <div>
                                        <div class="input-group date">
                                            <input id="from" name="from" type="text" value="{{substr($from, 0 ,10)}}" class="form-control" placeholder="年-月-日"><span class="input-group-addon"><i class="fa fa-long-arrow-alt-right"></i></span>

                                            <input id="to" name="to" type="text" class="form-control" value="{{substr($to, 0 ,10)}}" placeholder="年-月-日">
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="col-md-4">
                                <label class="control-label" for="checkin_from checkin_to">入住日期</label>
                                <div>
                                    <div>
                                        <div class="input-group date">
                                            <input id="checkin_from" name="checkin_from" type="text" value="{{substr($checkin_from, 0 ,10)}}" class="form-control" placeholder="年-月-日"><span class="input-group-addon"><i class="fa fa-long-arrow-alt-right"></i></span>

                                            <input id="checkin_to" name="checkin_to" type="text" class="form-control" value="{{substr($checkin_to, 0 ,10)}}" placeholder="年-月-日">
                                        </div>
                                    </div>
                                </div>
                                </div>

                                <div class="col-md-2">
                                <label class="control-label" for="keywords">订单号</label>
                                <div>
                                    <input class="form-control" type="text" id="keywords" name="keywords" value="{{$keywords}}"/>
                                </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="control-label"></label>
                                    <button class="btn btn-primary btn-block" type="button" onclick="search()">
                                        <i class="fa fa-search"></i> 查询
                                    </button>
                                </div>
                            </div>
                            <div class="form-group " style="margin-bottom:-20px;">
                                @if(Auth::user()->admin == 2 || Auth::user()->admin == 1)
                                    {{--accounting and super admin able to use this function--}}        
				  <button onclick="cancelOrder('', 'special')" class="btn btn-danger" type="button">
                                        特殊取消（1：BD总监同意并执行与上游取消成功后 2：BD总监通知财务做相应的后续退款处理）
                                    </button><br><br><br>
                                @endif
                            </div>
                        </form>
			 <p class="text-danger">特殊取消适用范围（1：客人正常点击取消按钮报错，联系我们取消 2：b2b客人如果需要取消一个订单中的几个房间或者修改入住日期（财务需要收取取消订单部分的70%）
                 3：b端或c端客人对房间入住不满意有争议，与上游部分退款协议达成）
                                 </p>
                        <p class="text-danger">解决方案 1：中国酒店专员，美国BD去联系上游取消，并得到确认（发邮件打电话都可以，联系方式在feedback中可以找到），该情况财务退全款
                            2：BD总监联系上游处理相关客人需求，告知客人该修改会产生$25的手续费，并把最终取消的金额告知财务，财务退回该部分金额-$25给客人。 3：根据上游的的最终退款条款给客人答复，财务相应退款
                        </p>
                    </div>
                </div>
            </div>
        </div>
        {{--end search--}}
        {{--start list--}}
        <div class="col-lg-12 col-md-12">
            <div class="ibox float-e-margins">
                <!--div class="ibox-title">
                    订单列表
                </div-->
                <div>
                    <div id="order-list" class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-admin-orders">
                                <thead>
                                <tr>
                                    {{--1--}}
                                    <th>订单号</th>
                                    {{--2--}}
                                    <th>订购方</th>
                                    {{--3--}}
                                    <th>订购日</th>
                                    {{--4--}}
                                    <th>酒店</th>
                                    {{--5--}}
                                    <th>入住/退房</th>
                                    {{--6--}}
                                    <th>类别</th>
                                    {{--7--}}
                                    <th>金额</th>
                                    {{--8--}}
                                    <th>状态</th>
                                    {{--9--}}
                                    <th>操作</th>
                                    {{--10--}}
                                    <th class="shorter">订购人</th>
                                    {{--11--}}
                                    <th>供应商</th>
                                    {{--12--}}
                                    <th>供应商确认号</th>
                                    {{--13--}}
                                    <th>OP备注</th>
                                    {{--14--}}
                                    <th>备注/特别要求</th>
                                    {{--15--}}
                                    <th>支付方式</th>
                                    {{--16--}}
                                    <th style="display:none;">Due Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        {{--1.订单号--}}
                                        <td>
                                            {{$order->id}} <span class="text-danger">{{$order->user_language == 1 ? 'En' : $order->user_language == 2 ? 'Tw' :''}}<br></span>
                                            <span class="text-info">{{strrpos($order->http_host,'usitour') !== false ? '(Usitour)' : ''}}</span>
                                            @if(Auth::user()->id == 10150 || Auth::user()->id == 10603 || Auth::user()->id == 11313  || Auth::user()->admin == 1)
                                                <br><a onclick="getGuestName('{{$order->id}}')" class="btn btn-default btn-xs">改入住人</a>
                                            @endif

                                        </td>
                                        {{--2.订购方--}}
                                        <td>
                                            <a onclick="getPartnerInfo('{{$order->partnerId}}')" class="btn btn-default btn-xs"><i class="fa fa-info-circle fa"></i></a><br/>
                                            @if (isset($order->user->id))
                                                @if($order->user->id == 11087 || $order->user->id == 11087)
                                                    {{$order->user->company_name. '(' .$order->bookerFirstName. ' ' .$order->bookerLastName. ')'}}
                                                    @if(isset($order->orderTem->booker) && isset(json_decode($order->orderTem->booker, true)['validPhoneNumber']))
                                                        <p class="text-danger">{{json_decode($order->orderTem->booker,true)['countryCodePhone']}}{{json_decode($order->orderTem->booker,true)['validPhoneNumber']}}</p>
                                                    @else
                                                       <p class="text-success">{{$order->bookerPhone}}</p>
                                                    @endif
                                                @else
                                                    {{$order->user->company_name}}
                                                @endif
                                            @else
                                                无
                                            @endif
                                        </td>
                                        {{--3.订购日--}}
                                        <td>
                                            <p style="width:80px">{{$order->created_at}}</p>
                                        </td>
                                        {{--4.酒店名称--}}
                                        <td>
                                            {{$order->hotelName . $order->hotelName_zh}}
                                        </td>
                                        {{--5.入住退房--}}
                                        <td>
                                            <p style="width:80px">{{$order->checkinDate}}/ {{$order->checkoutDate}}</p>
                                        </td>
                                        {{--6.取消政策--}}
                                        <td><p style="width:80px">
                                                @php
                                                    $order_date=strtotime($order['created_at']);
                                                    $room = $order->rooms->first();
                                                    $policies= json_decode($room['cancelPolicies'], true);
                                                    if (isset($policies) && isset($policies[0]) && isset($policies[0]["end"])  &&  count($policies) != 0) {
                                                        $cancel_date = $policies[0]["end"];
                                                        $end = substr(str_replace("T"," ",$cancel_date),0,19);
                                                        $end_date = strtotime($end);
                                                        //echo date('M d, Y H:i:s', $end_date);
                                                        date_default_timezone_set('America/Los_Angeles');
                                                        $now = time();
                                                        //echo date('Y-m-d H:i:s', $now);
                                                        if($now<$end_date){
                                                            echo date('Y-m-d H:i:s', $end_date) . "(R)";
                                                            //echo " ( 酒店当地时间 ) 前可以免费取消";
                                                        }else if ($now>$end_date){
                                                            echo "NR";
                                                        }else{
                                                            echo "N/A";
                                                        }
                                                    }else{
                                                        echo "NR";
                                                    }
                                                @endphp
                                            </p>
                                        </td>
                                        {{--7.订单金额--}}
                                        <td>
                                            @if(isset($order->orderTem->coupon_code) && $order->orderTem->coupon_code !== '')
                                                   {{$order->currencyCode . ' ' .$order->orderTem->amount}}
                                                    <br/><span class="text-danger">({{$order->orderTem->coupon_code}})</span>
                                            @else
                                                @if ($order->payment_type == 'ALIPAY')
                                                    @if ($order->infact_cost_cn == '' || null == $order->infact_cost_cn)
                                                        {{$order->currencyCode . ' ' . $order->totalPrice}}
                                                    @else
                                                        {{'CNY' . ' ' . $order->infact_cost_cn}}
                                                    @endif
                                                @else
                                                    {{$order->currencyCode . ' ' . $order->totalPrice}}
                                                @endif
                                            @endif
                                        </td>
                                        {{--8.订单状态--}}
					                    <td>@if($order->id == 1153 || $order->id == 1677 || $order->id ==  2810)
                                                <span class="label label-warning">Cancelled With Fees</span>
                                            @else
                                                <span class="label {{$order->status == 'CONFIRMED' ? 'label-info' : ($order->status == 'CANCELLED' ? 'btn' : ($order->status == 'ON REQUEST' ? 'label-success' : 'label-danger'))}}">
													@if ($order->status == 'CONFIRMED')
                                                        已确认
                                                    @elseif ($order->status == 'CANCELLED')
                                                        @if($order->payment_status == 'PENDING')
                                                            已取消退款中
                                                        @else
                                                            已取消
                                                        @endif
                                                    @elseif ($order->status == 'ON REQUEST')
                                                        处理中
                                                    @elseif ($order->status == 'REJECTED')
                                                        EB拒绝
                                                    @else
                                                        失败
                                                    @endif
												</span>
                                            @endif
                                            @if ($order->status == 'ON REQUEST')
                                                <button class="label btn" style="margin-top:10px;" onclick="checkOrder({{$order->id}})">
                                                    更新订单状态
                                                </button>
                                                <!--button onclick="" class="btn btn-xs btn-warning" type="button">更新状态</button-->
                                            @elseif(($order->status == 'CANCELLED') && ($order->payment_status == 'PENDING'))
                                                @if(Auth::user()->admin == 1 || Auth::user()->admin == 2)
                                                    <button class="label btn" style="margin-top:10px;" type="button" onclick="refund({{$order->id}})">
                                                        退款
                                                    </button>
                                                @endif
                                            @endif
                                        </td>
                                        {{--9 操作--}}
                                        <td class="text-left">
                                            <a target="_blank" href="{{url('/order/' . $order->orderReference)}}" class="btn btn-xs">发票信息</a>
                                            @if ($order->status == 'CONFIRMED'&& strtotime($order->checkinDate) >= strtotime(date('Y-m-d')))
                                            <a onclick="sendAnotherEmail({{$order->id}})" class="btn btn-xs">确认邮件</a>
                                            @endif
						                    <a target="_blank" href="//{{$order->http_host == '' ? $_SERVER['HTTP_HOST'] :$order->http_host}}/voucher/{{$order->code}}" class="btn btn-xs">酒店凭证</a>
                                            @if($order->provider=='Expedia')
                                            <a  class="btn btn-xs" target="_blank" href="{{json_decode($order->requestlog[0]->response,true)['Links']['WebItinRetrieve']['Href']}}">携程收据</a>
                                            @endif
                                            @if(strtotime($order->checkinDate) > strtotime(date('Y-m-d H:i:s')))
                                                @if($order->partnerId != config('app.partner_id'))
                                                    @if(Auth::user()->admin == 1 || Auth::user()->admin == 2)
                                                    <div class="btn btn-xs  text-warning" id="o{{$order->orderReference}}"></div>
                                                    @endif
                                                @endif
                                            @endif
                                        </td>
                                        {{--10 预订人--}}
                                        <td>
                                            {{$order->bookerLastName . ' ' . $order->bookerFirstName}}
                                        </td>
                                        {{--11 供应商--}}
                                        <td>
                                            @if($order->provider=='HotelBeds')
                                                HS1001
                                            @elseif($order->provider=='Innstant')
                                                IT1002
                                            @elseif($order->provider=='Tourico')
                                                TO1003
                                            @elseif($order->provider=='Expedia')
                                                EX1004
                                            @elseif($order->provider=='Bonotel')
                                                BT1005
                                            @elseif($order->provider=='Priceline')
                                                PL1006
                                            @elseif($order->provider=='EBooking')
                                                EB1007
                                            @elseif($order->provider=='Derby')
                                                DB1008
                                            @endif
                                        </td>
                                        {{--12 供应商确认编号--}}
                                        <td>
                                            <p style="width:110px">
                                                    {{str_replace(","," ",$order['orderPReference'])}}
                                            </p>
                                        </td>
                                        {{--13 op 备注--}}
                                        <td>
                                            <textarea readonly id="channel_remark{{$order->id}}"
                                                      name="channel_remark{{$order->id}}" rows="4" cols="25"
                                                      maxlength="1000" class="opcommend" title="@if(isset
                                                      ($order->channel_remark)){{$order->channel_remark}}@endif">@if(isset
                                                      ($order->channel_remark)){{$order->channel_remark}}@endif
                                            </textarea>
                                            <i class="fa fa-2x fa-edit pull-right text-success" onclick="openOpCommendModal({{$order->id}})">
                                            </i>
                                        </td>
                                        {{--14 特别备注和客户备注--}}
                                        <td>
                                            @if(isset($order->remark)){{$order->remark}}@endif
                                            @if(isset($order->special_request) && $order->special_request !='' &&
                                            $order->special_request !='[""]' && $order->special_request != null)
                                                <p style="color:red;">特别要求：</p>
                                                <p style="text-decoration:underline">{{$order->special_request}}</p>@endif
                                        </td>
                                        {{--15 支付方式--}}
                                        <td>
                                            @if ($order->payment_type == 'ALIPAY')
                                                支付宝
                                            @elseif ($order->payment_type == 'CC')
                                                信用卡
                                                @php
                                                    $payment= $order->payment->first();
                                                @endphp
                                                <p style="font-size:13px;">
                                                    @if(isset($payment->cc_number))
                                                        (#{{$payment->cc_number}})
                                                    @endif
                                                </p>
                                            @elseif ($order->payment_type == 'PO')
                                                PostPay
                                            @elseif ($order->payment_type == 'PB')
                                                paypal
                                            @elseif ($order->payment_type == 'WECHATPAY')
                                                微信支付
                                            @elseif ($order->partnerId === 10028)
                                                走四方C端支付
                                            @elseif ($order->partnerId === 10020 || $order->partnerId === 10021 || $order->partnerId === 10022)
                                                eb支付
                                            @else
                                                支付失败
                                            @endif
                                            @if ($order->isReturned == 1 || ($order->status != 'CONFIRMED' && $order->payment_type != 'PO'))
                                                <span class="label label-success">已退</span>
                                            @elseif ($order->isChecked == 1 && $order->status == 'CONFIRMED')
                                                <span class="label label-info">已审</span>
                                            @elseif($order->isChecked == 1 && $order->status != 'CONFIRMED' && $order->payment_type == 'PO')
                                                <span class="label label-warning">需退</span>
                                            @elseif ($order->isChecked == 0 && $order->status == 'CONFIRMED')
                                                <span class="label label-danger">未审</span>
                                            @endif
                                        </td>
                                        {{--16 postpay用户due day--}}
                                        <td  style="display:none;">@if ($order->status == 'CONFIRMED' && $order->payment_type == 'PO')
                                                <h4 style="font-weight:400; color:#484848">
                                                    @php
                                                        $order_date=strtotime($order['created_at']);
                                                        $due_date = strtotime("+7 day", $order_date);
                                                        $room = $order->rooms->first();
                                                        $policies= json_decode($room['cancelPolicies'], true);
                                                        if (isset($policies) && isset($policies[0]) && count
                                                        ($policies) != 0 && isset($policies[0]["end"])) {
                                                            $cancel_date = $policies[0]["end"];
                                                            $end = substr(str_replace("T"," ",$cancel_date),0,19);
                                                            $end_date = strtotime($end);
                                                            if($end_date<$due_date && $end_date>$order_date)
                                                            {
                                                               echo date('m/d/y', $end_date);
                                                            }elseif($end_date>$due_date){
                                                               echo date('m/d/y', $due_date);
                                                            }else{
                                                               echo date('m/d/y', $order_date);
                                                            }
                                                        }else{
                                                            echo date('m/d/y', $order_date);
                                                        }
                                                    @endphp
                                                </h4>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--end list--}}
    </div>
    {{--start partner info modal--}}
    <div class="modal fade" id="partnerInfoModal" tabindex="-1" role="dialog" aria-labelledby="partnerInfoModalTitle" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content guest-name-table" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="partnerInfoModalTitle"></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <tbody id="partnerInfoModalBody">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
    {{--end name edit modal--}}
    {{--start partner info modal--}}
    <div class="modal fade" id="guestNameModal" tabindex="-1" role="dialog" aria-labelledby="guestNameModalTitle" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content guest-name-table" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="guestNameModalTitle"></h4>
                    <input type="hidden" name="id" class="guestNameModalTitle" value="">
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>名 FirstName</th>
                            <th>姓 LastName</th>
                        </tr>
                        </thead>
                        <tbody id="guestNameModalBody">
                        </tbody>
                        <tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>  <button class="btn btn-primary" onclick="saveGuestName()">保存更新</button>
                </div>
            </div>
        </div>
    </div>
    {{--end name edit modal--}}
    {{-- start add op comment Modal--}}
    <div class="modal fade" id="addOpCommentModal" tabindex="-1" role="dialog" aria-labelledby="AddOpCommentModalTitle"
         data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">添加订单 <span id="addCommentModalTitle" class="text-warning"></span> OP备注</h4>
                </div>
                <div class="modal-body">
                    <textarea readonly id="op_remark"></textarea>
                    <hr>
                    <div id="add-area"><textarea name="" id="addComment" cols="90" rows="6" style="resize: none;  width:100%;"></textarea></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button class="btn btn-primary" onclick="updateOpCommend()">提交</button>
                </div>
            </div>
        </div>
    </div>
    {{--end add op comment Modal--}}
@endsection

@section('scripts')
    <script src="{{ asset('/assets/js/admin/plugins.orders.js?07182018')}}"></script>
    {{--single page scripts--}}
   <Script>
     {{--var orders = {!! json_encode($orders) !!} + '';--}}
   </Script>
    @include('admin.includes.js-orders')
@endsection