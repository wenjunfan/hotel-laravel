/**
 * Created by Arthur.
 */

var U_distributor = U_distributor || {};

U_distributor.getParam = function (name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)');
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return unescape(r[2]);
    return null;
};

U_distributor.putValue = function (name, value) {
    if (typeof (Storage) !== 'undefined') {
        localStorage.setItem(name, value);
    }
};

U_distributor.getValue = function (name) {
    if (typeof (Storage) !== 'undefined') {
        return localStorage.getItem(name);
    }
};