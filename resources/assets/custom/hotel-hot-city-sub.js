var U_hotels = U_hotels || {};

/**
 * 热门目的地
 */
U_hotels.destination = function (desName, desId) {
    var _destinationName = $('#' + desName);//目的地
    var _destinationId = $('#' + desId);//隐藏域，存放desId
    var _wrapper = $('.dest_wrapper');
    var _wrapper_dest = $('.dest_wrapper .dft_dest');//目的地选择器

    _destinationName.focus(function (e) {//获取焦点
        _destinationName.siblings('.check_err').remove();
        if ($(this).val() == '') {//如果当前输入值为空
            $('div[class="autocomplete-suggestions"]').remove();
            _wrapper.show();
        }
    });

    _destinationName.click(function () {
        var desName = $(this).val();
        if (desName) {
            /*把初始值存在tip属性中*/
            $(this).attr('tip', desName);
            if ($(this).val().length == 0) {
                _wrapper.show();
            }
        }
    });

    _destinationName.keyup(function (e) {
        if ($(this).val() == '') {//如果当前输入值为空
            _wrapper.show();
            _destinationId.val('');//顺便清除hidden input value
        } else {
            _wrapper.hide();
        }
    });

    _wrapper_dest.on('click', function (e) {
        e.stopPropagation();//阻止冒泡，避免在目的地选择器上的点击事件引起选择器的隐藏
    });

    $('body').on('click', function (e) {//点击其他地方时，隐藏目的地选择器
        var desId = _destinationId.val();
        var desName = _destinationName.val();
        var desTip = _destinationName.attr('tip');

        if (!desId) {
            _destinationName.attr('tip', '');
        } else if (desName && !desTip) {
            _destinationName.attr('tip', desName);
        }

        $('div[class="autocomplete-suggestions"]').remove();

        if (e.target != _destinationName[0]) {
            _wrapper.hide();
            _destinationName.val(_destinationName.attr('tip'));
        }
    });

    /*
     * 热门洲切换
     * */
    $('.dft_title li').click(function () {
        hotelAddClose = false;
        var _index = $(this).index();
        $(this).addClass('on').siblings().removeClass('on');
        $('.dft_list ul').eq(_index).show().siblings().hide();
    });

    /*
     * 选择城市
     * */
    $('.dft_list ul li').click(function () {
        var _this = $(this);
        _destinationId.val(_this.attr('data-des-id'));
        _destinationName.val(_this.attr('data-cn'));
        _destinationName.attr('tip', _this.attr('data-cn'));
        _destinationName.trigger('change');
        _wrapper.hide();
    });
};