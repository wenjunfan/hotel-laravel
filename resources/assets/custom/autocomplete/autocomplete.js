function setupAutocomplete(options, isCh, nameInput, idInput) {
    var $destinationNameInput = $('#' + nameInput),
        $destinationIdInput = $('#' + idInput),
        $scenicNameInput = $('input[name="scenicName"]'),
        $scenicIdInput = $('input[name="scenicId"]');

    var pairs = [];

    for (var i = 0; i < options.length; i++) {
        var option = options[i];
        var pair = {};

        if (option.type === 'destination') {
            pair.value = option.name;
            if (isCh && option.name_zh) {
                pair.value += ' (' + option.name_zh + ')';
            }

            pair.data = {
                desId: 'D' + option.id,
                type: isCh ? '城市' : 'Cities',
                id: '',
            };

            pairs.push(pair);
        } else if (option.type === 'hotel') {
            pair.value = option.name;
            if (isCh && option.name_zh) {
                pair.value += ' (' + option.name_zh + ')';
            }
            pair.value += option.city ? (', ' + option.city) : '';
            pair.value += option.countryCode ? (', ' + option.countryCode) : '';

            pair.data = {
                desId: 'H' + option.id,
                type: isCh ? '酒店' : 'Hotels',
                id: '',
            };

            pairs.push(pair);
        } else if (option.type === 'airport') {
            pair.value = isCh ? option.name_full : option.name;
            pair.value += option.name_short ? (', ' + option.name_short) : '';
            var des = option.destination_name;
            if (isCh && option.destination_name_zh) {
                des = option.destination_name_zh;
            }
            pair.value += des ? (', ' + des) : '';

            pair.data = {
                desId: 'D' + option.destination_id,
                type: isCh ? '机场/车站' : 'Airports/Stations',
                id: option.id,
            };

            pairs.push(pair);
        } else if (option.type === 'scenic') {
            pair.value = isCh ? option.name_full : option.name;
            var des = option.destination_name;
            if (isCh && option.destination_name_zh) {
                des = option.destination_name_zh;
            }
            pair.value += des ? (', ' + des) : '';

            pair.data = {
                desId: 'D' + option.destination_id,
                type: isCh ? '景点' : 'Landmarks',
                id: option.id,
            };

            pairs.push(pair);
        }
    }

    // selete the first option if user focus out
    if (pairs.length > 0 && '' !== $destinationNameInput.val() && 0 !== $destinationNameInput.val()) {
        var firstPair = pairs[0];
        $scenicIdInput.val(firstPair.data.id);
        $destinationIdInput.val(firstPair.data.desId);
        $destinationNameInput.attr('tip', firstPair.value);
    } else {
        $destinationIdInput.val('');
        $destinationNameInput.attr('tip', '');
    }

    $destinationNameInput.autocomplete({
        flag: 'mainPage',
        lookup: pairs,
        groupBy: 'type',
        onSearchComplete: function(query, suggestions) {
            searchKey = query; // update parent search key to mark in api
        },
        onSelect: function(opt) {
            $destinationNameInput.unbind('focus');
            $destinationNameInput.bind('focus', function() {
                var input = $(this);
                setTimeout(function() {
                    input.select();
                });
            });

            if (opt.data.id) {
                $scenicNameInput.val(opt.value);
                $scenicIdInput.val(opt.data.id);
            }
            $destinationIdInput.val(opt.data.desId);
            $destinationNameInput.val(opt.value);
            $destinationNameInput.attr('tip', opt.value);
        },
    });

    $destinationNameInput.focus();

    $destinationNameInput.keyup(function(e) {
        switch (e.keyCode) {
            case 13:
                var firstPair = pairs[0];
                $scenicIdInput.val(firstPair.data.id);
                $destinationIdInput.val(firstPair.data.desId);
                $destinationNameInput.val(firstPair.value);
                $destinationNameInput.attr('tip', firstPair.value);
                $destinationNameInput.unbind('focus');
                $('.autocomplete-suggestions').hide();
        }
    });
}