function topHover(obj, obj1) {
    $(obj).each(function () {
        var _this = $(this);
        _this.hover(function () {
            _this.find(obj1).show();
        }, function () {
            _this.find(obj1).hide();
        })

    });
}

topHover("li.myusi", ".myusi-list");
topHover("li.cart", ".expand-shoppingcart");
topHover("#topCurrencyBox", "ul.site-language");
topHover(".js-bar-language", ".js-site-language");
topHover(".weixin", ".weixin-bg-wrap");

//修改客户导航按钮
$(function () {
    var management;
    $.ajax({
        url: 'https://post.usitrip.com/ajax.php?mod=index&action=service_management&language_code=gb2312',
        type: 'POST',
        // async:false,
        success: function (data) {
            management = data;
            if (management == '1') {
                $('.contact-wrap li.online-service-hover-2').hide();
                $('.contact-wrap li.online-service-hover').show();
            } else if (management == '2') {
                $('.contact-wrap li.online-service-hover').hide();
                $('.contact-wrap li.online-service-hover-2').show();
            } else if (management == '1,2') {
                $('.contact-wrap li.online-service-hover').show();
                $('.contact-wrap li.online-service-hover-2').show();
            }
        }
    });

    $('.contact-wrap div.online-service-1').on('click', function (e) {
        e.preventDefault();
        $('#MEIQIA-BTN').click();
    });

    $('.contact-wrap div.online-service-2').on('click', function (e) {
        e.preventDefault();
        $('#MEIQIA-BTN').click();
    });

});

