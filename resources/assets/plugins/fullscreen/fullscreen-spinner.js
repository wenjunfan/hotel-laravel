/*
 *
 *   by Louis
 *   05/24/2016
 *
 */

var FullscreenSpinner = function () {
    return {
        create: function () {
            $( "body" ).append( '<div class="fullscreen-spinner"><div class="sk-spinner sk-spinner-three-bounce"> <div class="sk-bounce1"></div> <div class="sk-bounce2"></div> <div class="sk-bounce3"></div> </div> </div>' );

            resizeSpinner($(window).height());
        },

        destroy: function () {
            $( ".fullscreen-spinner" ).remove();
        },
    };
}();

function resizeSpinner(height) {
    var topOffs = (height - 32) / 2;
    $(".fullscreen-spinner").css("padding-top", topOffs + "px");
}

$(window).bind("resize", function () {
    resizeSpinner($(this).height());
});
