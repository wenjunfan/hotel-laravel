var priceChange = new Array();
var roomRecheck = new Array();
var avail = true;

function checkRadio(hotelId, roomCount) {

	avail = true;
	priceChange = new Array();
	roomRecheck = new Array();

	for (var i = 1, j = 0; i <= roomCount; i++) {

		var value = $('input:radio[name="room' + hotelId + '_' + i + '"]:checked').val();

		if (typeof(value) == 'undefined') {
			swal({
				title: "请先选择房间",
				type: "warning"
			});
			return false;
		}

		var o = JSON.parse(value);
		var status = o.status;

		if (status == "RECHECK") {
			o.status = "BOOKABLE";
			if (roomRecheck.length == 0 || (roomRecheck.length > 0 && (!containRef(roomRecheck, o.roomRef)))) {
				roomRecheck[j] = {
					"roomRef": o.roomRef,
					"value": JSON.stringify(o),
					"name": "room" + hotelId + "_" + i
				};
				j++;
			}
		}
	}

	if (roomRecheck.length > 0) {
		var rf = new Array();

		$.each(roomRecheck, function (i, field) {
			rf[i] = {"roomReference": field.roomRef};
		});

		FullscreenSpinner.create();

		$.post("/checkRooms", {
			'_token': $("meta[name='csrf_token']").attr('content'),
			'references': JSON.stringify(rf)
		}, function (data) {

			FullscreenSpinner.destroy();

			//update cancellation policy
			roomRecheck = updateCancellation(roomRecheck, data);

			if ("success" in data && data["success"] == false) {
				//TODO
				if ("type" in data && data['type'] == "SESSION_TIMEOUT") {
					swal({
						title: "请重新搜索。",
						type: "warning",
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "OK",
					}, function () {
						window.location.reload();
					});
				}
			}

			$.each(data, function (i, field) {

				//房间已定完
				if ("result" in field && field.result == "false") {
					//if(field.result == "false"){
					avail = false;
					$(".slot_" + field.roomReference).text("0");
					$(".label_" + field.roomReference + " div").removeClass("checked").addClass("disabled");
					$(".room_" + field.roomReference).removeAttr('checked');
					$(".room_" + field.roomReference).attr('disabled', true);
				}
				else {
					//房间价格已变化
					if (field.netPrice != field.uncheckPrice) {
						var name = $(".name_" + field.roomReference).text();
						priceChange[i] = {
							"netPrice": field.netPrice,
							"oldPrice": field.uncheckPrice,
							"name": name,
							"roomRef": field.roomReference
						};
						$(".price_" + field.roomReference).text(field.netPrice);
					}
				}

				$.each(roomRecheck, function (j, f) {
					if (f.roomRef == field.roomReference) {
						$(".room_" + field.roomReference).val(f.value);
					}
				});

			});

			if (!avail) {
				swal({
					title: "对不起，房间已经被定完，请重新选择",
					type: "warning",
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "OK",
					// closeOnConfirm: true,
					// closeOnCancel: false
				});
				return false;
			}

			if (priceChange.length > 0) {
				var t = "";

				$.each(priceChange, function (i, field) {
					t += field.name + ": " + "原价: " + Math.ceil(field.oldPrice * 100 / roomNum / days) / 100.0 + "  现价: " + Math.ceil(field.netPrice * 100 / roomNum / days) / 100.0 + "\n";
					var v;

					$.each(roomRecheck, function (k, r) {
						if (r.roomRef == field.roomRef)
							v = r.value;
					});
					var m = JSON.parse(v);
					m.netPrice = field.netPrice;
					m.status = "BOOKABLE";
					$(".room_" + field.roomRef).val(JSON.stringify(m));
				});

				swal({
						title: "对不起，酒店价格已经改变，是否继续预定？",
						text: t,
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "是，继续预定!",
						cancelButtonText: "不，我要重新选择！"
						// closeOnConfirm: true,
						// closeOnCancel: false
					},
					function (isConfirm) {
						if (isConfirm) {
							$("form#room-form" + hotelId).submit();
						}
					});
				return false;
			} else {
				$("form#room-form" + hotelId).submit();
			}
		});
		return false;
	} else {
		return true;
	}
}

function containRef(roomRecheck, roomRef) {
	for (var i = 0; i < roomRecheck.length; i++) {
		if (roomRecheck[i].roomRef == roomRef) {
			return true;
		}
	}

	return false;
}

function updateCancellation(roomRecheck, data) {

	$.each(data, function (i, field) {

		for (var j = 0; j < roomRecheck.length; j++) {

			if (field.roomReference == roomRecheck[j].roomRef) {
				var temp = JSON.parse(roomRecheck[j].value);
				temp.cancellationPolicies = field.cancellationPolicies;
				roomRecheck[j].value = JSON.stringify(temp);
			}

		}

	});

	return roomRecheck;
}



