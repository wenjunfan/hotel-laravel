// language
var is_ch = (language === 0);
var lang_no_destination_input = is_ch ?
    '请输入您想去的酒店名或目的地' :
    'Please enter the destination or hotel name';
var lang_search_format = is_ch ? '请选择酒店预订日期' : 'Please select dates';
var lang_search_checkin_valid = is_ch ?
    '请检查入住日期' :
    'Check-in date is not valid, please check again.';
var lang_search_checkout_valid = is_ch ?
    '请检查退房日期' :
    'Check-out date is not valid, please check again.';
var lang_search_limit = is_ch ?
    '退房日期不得大于30天' :
    'Check out date can not exceed 30 days';
var lang_search_group_alert = is_ch ?
    '如需预定九间及以上客房，请提交团房申请' :
    'Please use Group Room Request form if you would like to book more than 9 rooms.';
var lang_coupon_sendmem_again = is_ch ?
    '您已经领取过你的会员红包啦.' :
    'Thank you! You already subscribed!';

var checktemp;
var index = 1;
var searchCount = 0;
var scenicarray;
var dhxhr, roomNum, days;
var isCh = language === 0;
var maxCheckinText = !isCh ? 'Check-in Date should in two years from now' : '入住日期需在两年内';
var searchKey = '';
var td = null;
var tm = null;
var dat = null;

window.selectText = function(element) {
    $('.dest_wrapper .dest_result').hide();
    $('.dest_wrapper').show();
    $('.dest_wrapper .dft_dest').show();
};

window.updateCheckin = function(location) {
    var checkInLocation = $('#' + location + 'Checkin');
    var checkOutLocation = $('#' + location + 'Checkout');
    var checkin = checkInLocation.val();
    var today = new Date();
    var today = today.getFullYear() + '-' + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + '-' + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    var checkin1 = checkin.replace(/-/g, '');
    var today1 = today.replace(/-/g, '');

    if (checkin1 < today1) {
        checkInLocation.datepicker('setDate', today);
    }
    var checkin = checkInLocation.val();
    var checkout = checkOutLocation.val();
    checkin1 = checkin.replace(/-/g, '');
    checkout1 = checkout.replace(/-/g, '');

    //			if (parseInt(checkin1) >= parseInt(checkout1)) {
    checkin = checkin.split('-');
    var checkin = new Date(checkin[0], (checkin[1] - 1), checkin[2]);
    var n = 1;
    var checkout = new Date(checkin - 0 + n * 86400000);
    var checkout = checkout.getFullYear() + '-' + (checkout.getMonth() + 1 > 9 ? checkout.getMonth() + 1 : '0' + (checkout.getMonth() + 1)) + '-' + (checkout.getDate() < 10 ? '0' + checkout.getDate() : checkout.getDate());

    checkOutLocation.datepicker('setDate', checkout);
    //			}

    checkOutLocation.datepicker('setStartDate', checkin);

    var a1 = parseInt(checkin1);
    var a2 = parseInt(checktemp);

    if (a1 === a2 && index !== 1 && index !== 2) {
        checkOutLocation.datepicker('show');
    }
    checktemp = checkin1;
    index++;
    checkOutLocation.datepicker('show');
};

window.updateCheckout = function(location) {
    var checkOutLocation = $('#' + location + 'Checkout');
    var checkInLocation = $('#' + location + 'Checkin');
    var checkout = checkOutLocation.val();
    var checkin = checkInLocation.val();
    var today = new Date();
    var today = today.getFullYear() + '-' + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + '-' + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    var checkout1 = checkout.replace(/-/g, '');
    var today1 = today.replace(/-/g, '');
    var checkin1 = checkin.replace(/-/g, '');
    if (checkout1 <= checkin1) {
        var dcheckin = new Date(checkin);
        var tomorrow = new Date(dcheckin.getTime() + 2 * (24 * 60 * 60 * 1000));
        var tomorrow1 = tomorrow.getFullYear() + '-' + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + '-' + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());
        $('#' + location + 'Checkout').datepicker('setDate', tomorrow1);
    }
    var checkin = checkInLocation.val();
    var checkout = checkOutLocation.val();
    var today = new Date();
    var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));

    today1 = today.getFullYear() + '' + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + '' + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    tomorrow1 = tomorrow.getFullYear() + '' + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + '' + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());
    checkin1 = checkin.replace(/-/g, '');
    checkout1 = checkout.replace(/-/g, '');

    if (parseInt(checkin1) >= parseInt(checkout1)) {
        if (parseInt(checkout1) == parseInt(today1)) {
            swal({
                title: '退房日期不能早于明天',
                type: 'warning',
            }, function() {
                setTimeout(function() {$('#' + location + 'Checkin').focus();}, 1);
            });

            td = today.getFullYear() + '-' + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + '-' + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
            tm = tomorrow.getFullYear() + '-' + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + '-' + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());

            $('#' + location + 'Checkin').datepicker('setDate', td);
            $('#' + location + 'Checkout').datepicker('setDate', tm);
        }
    }
};

// go to list page todo:清理，我们不需要识别不同的location了，因为只有一个
window.showResultPage = function(location) {
    // check destination first
    var destinationId = $('#' + location + 'DestinationId').val();
    if (destinationId === '' || undefined === destinationId) {
        $('#' + location + 'DestinationId').val(0);
        swal({
            title: lang_no_destination_input,
            type: 'warning',
        }, function() {
            setTimeout(function() {$('#' + location + 'DestinationName').focus();}, 1);
        });

        return;
    }
    // end check destination first

    if (searchKey.length > 0) {
        $.get('/keyword/mark/' + encodeURIComponent(searchKey));
    }

    var checkin = $('#' + location + 'Checkin').val();
    var checkout = $('#' + location + 'Checkout').val();

    days = calculateDays(checkin, checkout);
    roomNum = $('#' + location + 'RoomCount').val();

    $('#' + location + 'Checkout').datepicker('hide');

    if (checkin === '') {
        swal({
            title: '请先选择入住日期',
            type: 'warning',
        }, function() {
            setTimeout(function() {$('#' + location + 'Checkin').focus();}, 1);
        });
        return false;
    }

    if (checkout === '') {
        swal({
            title: '请先选择退房日期',
            type: 'warning',
        }, function() {
            setTimeout(function() {$('#' + location + 'Checkout').focus();}, 1);
        });
        return false;
    }

    /* a quick date format validation */
    var dateformat = /^(\d{4})-(\d{2})-(\d{2})$/;
    if (!checkin.match(dateformat) || !checkout.match(dateformat)) {
        swal({
            title: lang_search_format,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'ok',
        });
        return false;
    } else {
        //checkin date validation
        var parts = checkin.split('-');

        var year = parts[0];
        var month = parts[1];
        var day = parts[2];

        var $day = (day.charAt(0) == '0') ? day.charAt(1) : day;
        var $month = (month.charAt(0) == '0') ? month.charAt(1) : month;

        //checkout date validation
        var parts1 = checkout.split('-');

        var year1 = parts1[0];
        var month1 = parts1[1];
        var day1 = parts1[2];

        var $day1 = (day1.charAt(0) == '0') ? day1.charAt(1) : day1;
        var $month1 = (month1.charAt(0) == '0') ? month1.charAt(1) : month1;

        var now = new Date();
        var currentYear = now.getFullYear();

        if ($day > 31 || $day < 1 || $month > 12 || $month < 1 || year < currentYear) {
            swal({
                title: lang_search_checkin_valid,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'ok',
            });
            return false;
        }
        if ($day1 > 31 || $day1 < 1 || $month1 > 12 || $month1 < 1 || year1 < currentYear) {
            swal({
                title: lang_search_checkout_valid,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'ok',
            });
            return false;
        }
    }

    var newDate = new Date();
    var maxdm = newDate.toISOString().substr(4, 6);
    var maxCheckin = (newDate.getFullYear() + 2) + maxdm;

    if (checkin > maxCheckin) {
        swal({
            title: maxCheckinText,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'ok',
        });
        return false;
    }

    if (days > 30) {
        swal({
            title: lang_search_limit,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'ok',
        });
        // 大于30天，换回可用的日期
        if (localStorage.getItem('checkout')) {
            $('.checkout').datepicker('setDate', localStorage.getItem('checkout'));
        }
        return false;
    }

    if (roomNum === '9' && destinationId.length !== '') {
        $('#' + location + 'DestinationId').val(0);
        swal({
            title: lang_search_group_alert,
            type: 'warning',
        }, function() {
            setTimeout(function() {$('#' + location + 'RoomCount').focus();}, 1);
        });
        return;
    }

    var sortMethod = 'desc';
    var searchHotelsCount = 0;
    var locationSearchForm = $('#' + location + 'SearchForm');
    locationSearchForm.attr('action', '/list/' + destinationId + '.html');
    locationSearchForm.submit();
};

window.calculateDays = function(checkin, checkout) {
    var date1 = new Date(checkin);
    var date2 = new Date(checkout);
    var date3 = date2.getTime() - date1.getTime();
    var days = Math.floor(date3 / (24 * 3600 * 1000));
    return days;
};

// show search result
window.showResult = function(destinationName, destinationId) {
    $('#mainDestinationName').val(destinationName);
    $('#mainDestinationId').val('D' + destinationId);

    $('#mainSearchButton').click();
};

// show discounted hotels
window.showDiscount = function(destinationName, destinationId) {
    $('#mainDestinationName').val(destinationName);
    $('#mainDestinationId').val('D' + destinationId);

    var $searchForm = $('#mainSearchForm');
    if ($searchForm.find('input[name*="discount"]').length === 0) {
        $searchForm.append('<input type="hidden" name="discount" value="true"/>');
    }
    $('#mainSearchButton').click();
};

// setup destination when click on destination tab for recommend hotels
window.setupDestination = function(destinationName, destinationId) {
    $('#mainDestinationName').val(destinationName);
    $('#mainDestinationId').val(destinationId);
};

// go to recommend hotel's detail page
window.searchHotel = function(hotelName, hotelId) {
    var anaImData = {
        'id': hotelId,
        'name': unescape(hotelName),
        'position': 1,
        'list': 'HomePage Recommend Hotels',
    };
    // addImpression(anaImData);

    var anaData = {
        'id': hotelId,
        'name': unescape(hotelName),
        'position': 1,
        'list': 'Homepage',
        'page': 'pc home',
    };

    onProductClick(anaData);
    // onProductView(anaData);

    var hotelLink = '/hotel/' + hotelId + '.html';
    window.open(hotelLink, '_blank');
};

window.subscribe = function() {
    var emailvalid = $('#subscribeEmail').val();
    if (validateEmail(emailvalid) && emailvalid !== '') {
        $('.loadingBtnTop').button('loading');

        if (shouldTrack) {
            gtag_report_conversion_subscribe();
        }

        $.post('/subscribe', {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'email': $('#subscribeEmail').val(),
            'pos': 1,
        }, function(data) {
            if (data['success']) {
                if (typeof(Storage) !== 'undefined') {
                    localStorage.setItem('subEmail', 1);
                    localStorage.setItem('userEmail', $('#subscribeEmail').val());
                }

                if (data['message'] === 'sub01') {
                    $('.message-1').show();
                } else {
                    $('.message-2').show();
                }
                $('.loadingBtnTop').button('reset');
            }
        }).fail(function(data) {
            if (data.status === 429) {
                alert(data.responseText);
            }
        });
    } else {
        // 首页顶部订阅按钮邮件检测失败
        swal(lang_error_email, '', 'error');
    }
};

// All document ready events should be put here
$(function() {
    //热门目的地列表
    U_hotels.destination('mainDestinationName', 'mainDestinationId');

    sessionStorage.clear();
    //slider define begin
    var pluginName = 'slider',
      defaults = {
          next: '.slider-nav__next',
          prev: '.slider-nav__prev',
          item: '.slider__item',
          dots: false,
          dotClass: 'slider__dot',
          autoplay: true,
          autoplayTime: 5000, //speed for banner
      };
    //slider end

    //change url for logo to usitrip, instead of hotel.usitrip.com
    $('#top_logo_usitrip').attr('href', language == 2 ?  'https://www.usitrip.com/tw' : 'https://www.usitrip.com/');
    $('#top_logo_usitour').attr('href', 'https://www.usitour.com/');
    $('.top_logo_usitrip_tw').attr('href', 'https://www.usitrip.com/tw');

    if (isUsitour) {
        $('.if-en-change-logo').html('<img class="fl" src="/img/general/logo/logo-usitour-clear.png" alt="logo">');
    } else {
        $('.if-en-change-logo').html('<img class="fl" src="/img/general/logo/logo-usitrip-clear.png" height="60;" width="280px;" alt="logo">');
    }

    var nw = $('#topSearchWrapper');
    $(document).on('scroll', function() {
        var c = $(document).scrollTop();
        var g = $('#h-super-wrap-id').offset().top;
        if (g <= c) {
            nw.css({
                'display': 'block',
            });
        } else {
            nw.css({
                'display': 'none',
            });
        }
    });

    var today = new Date();
    var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
    var dayaftertmr = new Date(today.getTime() + 2 * (24 * 60 * 60 * 1000));

    td = today.getFullYear() + '-' + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + '-' + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    tm = tomorrow.getFullYear() + '-' + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + '-' + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());
    dat = dayaftertmr.getFullYear() + '-' + (dayaftertmr.getMonth() + 1 > 9 ? dayaftertmr.getMonth() + 1 : '0' + (dayaftertmr.getMonth() + 1)) + '-' + (dayaftertmr.getDate() < 10 ? '0' + dayaftertmr.getDate() : dayaftertmr.getDate());

    if ($('#inputDestinationId').val() === '') {
        if (typeof(Storage) !== 'undefined' && localStorage.getItem('destinationId') != null && localStorage.getItem('destinationName') != null) {
            $('.destinationId').val(localStorage.getItem('destinationId'));
            $('.destinationName').val(localStorage.getItem('destinationName'));
        } else {
            $('.destinationId').val();
            $('.destinationName').val();
        }
    }

    if (typeof(Storage) !== 'undefined' && localStorage.getItem('checkin') != null && localStorage.getItem('checkout') != null) {
        if (localStorage.getItem('checkin') >= tm) {
            $('.checkin').val(localStorage.getItem('checkin', tm));
            $('.checkout').val(localStorage.getItem('checkout', dat));
        } else {
            $('.checkin').val(tm);
            $('.checkout').val(dat);
        }
    } else {
        $('.checkin').val(tm);
        $('.checkout').val(dat);
    }

    // Datepicker setup
    $('.checkin').datepicker({
        startDate: '1d',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: 'yyyy-mm-dd',
    });

    $('.checkout').datepicker({
        startDate: '2d',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: 'yyyy-mm-dd',
        defaultDate: dat,
    });

    $(window).resize(function() {
        var height = $(window).height();
        height = height * 0.8;
        $('.fixed').css({'top': height});
    });

    // Destination Autocomplete setup
    var typingTimer;       //timer identifier
    var doneTypingInterval = 300; //delay search time (in ms)
    var $destinationInput = $('.destinationName');

    $destinationInput.bind('input propertychange', function(e) {
        $(this).unbind('focus');
        $(this).unbind('blur');
        $('input[name="scenicName"]').val('');
        $('input[name="scenicId"]').val('');

        var location = ($(this).attr('id')).replace('DestinationName', ''); // check if it top or main search form
        var nameInput = location + 'DestinationName';
        var idInput = location + 'DestinationId';
        var keyword = encodeURIComponent($(this).val().trim());
        clearTimeout(typingTimer);

        typingTimer = setTimeout(function() {
            if (keyword === '') {
                $('div[class="autocomplete-suggestions"]').remove();
                return false;
            }

            if (dhxhr && dhxhr.readyState !== 4) {
                dhxhr.abort();
            }

            dhxhr = $.getJSON('/destination/suggestions?keyword=' + keyword, function(data) {
                setupAutocomplete(data, isCh, nameInput, idInput);
            });
        }, doneTypingInterval);
    });

    $destinationInput.on('focus', function() {
        $(this).select();
    });

    //on keydown, clear the countdown
    $destinationInput.on('keydown', function() {
        clearTimeout(typingTimer);
    });

    $destinationInput.change(function() {
        $('input[name="scenicName"]').val('');
        $('input[name="scenicId"]').val('');
    });

    //banner function begin
    function slider(element, options) {
        this.$document = $(document);
        this.$window = $(window);
        this.$element = $(element);
        this.options = $.extend({}, defaults, options);
        this.init();
    }

    var sliderPrototype = slider.prototype;
    sliderPrototype.init = function() {
        $('.slider__item').show();
        this.setup();
        this.attachEventHandlers();
        this.update();
    };

    sliderPrototype.setup = function(argument) {
        this.$slides = this.$element.find(this.options.item);
        this.count = this.$slides.length;
        this.index = 0;

        this.$next = $(this.options.next);
        this.$prev = $(this.options.prev);

        this.$canvas = $(document.createElement('div'));
        this.$canvas.addClass('slider__canvas').appendTo(this.$element);
        this.$slides.appendTo(this.$canvas);

        this.$dots = $(this.options.dots);
        this.$dots.length && this.createDots();
    };

    sliderPrototype.createDots = function() {
        var dots = [];
        for (var i = 0; i < this.count; i += 1) {
            dots[i] = '<span data-index="' + i + '" class="' + this.options.dotClass + '"></span>';
        }
        this.$dots.append(dots);
    };

    sliderPrototype.attachEventHandlers = function() {
        this.$element.on('prev.slider', this.prev.bind(this));
        this.$document.on('click', this.options.prev, (function(e) {
            this.$element.trigger('prev.slider');
        }).bind(this));

        this.$element.on('next.slider', this.next.bind(this));
        this.$document.on('click', this.options.next, (function(e) {
            this.$element.trigger('next.slider');
        }).bind(this));

        this.$element.on('update.slider', this.update.bind(this));
        this.$window.on('resize load', (function(e) {
            this.$element.trigger('update.slider');
        }).bind(this));

        this.$element.on('jump.slider', this.jump.bind(this));
        this.$document.on('click', ('.' + this.options.dotClass), (function(e) {
            var index = parseInt($(e.target).attr('data-index'));
            this.$element.trigger('jump.slider', index);
        }).bind(this));

        this.$element.on('autoplay.slider', this.autoplay.bind(this));
        this.$element.on('autoplayOn.slider', this.autoplayOn.bind(this));
        this.$element.on('autoplayOff.slider', this.autoplayOff.bind(this));
        this.$element.bind('prev.slider next.slider jump.slider', this.autoplay.bind(this));
        this.options.autoplay && this.$element.trigger('autoplayOn.slider');
    };

    sliderPrototype.next = function(e) {
        this.index = (this.index + 1) % this.count;
        this.slide();
    };

    sliderPrototype.prev = function(e) {
        this.index = Math.abs(this.index - 1 + this.count) % this.count;
        this.slide();
    };

    sliderPrototype.jump = function(e, index) {
        this.index = index % this.count;
        this.slide();
    };

    sliderPrototype.autoplayOn = function(argument) {
        this.options.autoplay = true;
        this.$element.trigger('autoplay.slider');
    };

    sliderPrototype.autoplayOff = function() {
        this.autoplayClear();
        this.options.autoplay = false;
    };

    sliderPrototype.autoplay = function(argument) {
        this.autoplayClear();
        if (this.options.autoplay) {
            this.autoplayId = setTimeout((function() {
                this.$element.trigger('next.slider');
                this.$element.trigger('autoplay.slider');
            }).bind(this), this.options.autoplayTime);
        }
    };

    sliderPrototype.autoplayClear = function() {
        this.autoplayId && clearTimeout(this.autoplayId);
    };

    sliderPrototype.slide = function(index) {
        undefined == index && (index = this.index);
        var position = index * this.width * -1;
        this.$canvas.css({
            'transform': 'translate3d(' + position + 'px, 0, 0)',
        });
        this.updateCssClass();
    };

    sliderPrototype.update = function() {
        this.width = this.$element.width();
        this.$canvas.width(this.width * this.count);
        this.$slides.width(this.width);
        this.slide();
    };

    sliderPrototype.updateCssClass = function() {
        this.$slides.removeClass('active').eq(this.index).addClass('active');

        this.$dots.find('.' + this.options.dotClass).removeClass('active').eq(this.index).addClass('active');
    };

    $.fn[pluginName] = function(options) {
        return this.each(function() {
            !$.data(this, pluginName) && $.data(this, pluginName, new slider(this, options));
        });
    };

    $('#slider').slider({
        prev: '#prev',
        next: '#next',
        dots: '#dots',
        autoplay: true,
    });

    $('[class^="related"]').not(':first').hide();

    $('#roomCount').on('change', function() {
        $('.related_' + this.value + '_content').show().siblings('[class^="related"]').hide();
    });

    // recommend ad 热门目的地
    $('.ad-slider').slick({
        dots: true,//是否显示导航的 点
        arrows: true,
        infinite: true,//是否循环
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 2,
        autoplay: true,
        autoplaySpeed: 5000,
    });

    // start 首页搜索-选择房间数量，成人数量，儿童数量
    var _searchDesc = $('#searchDesc');//房间人数显示框
    var _searchDialog = $('#searchDialog');
     _searchDesc.on('click', function () {
       if ($('#searchDialog').is(':hidden')) {
         _searchDialog.show();
         _searchDesc.addClass('person_desc_active');
       } else {
         _searchDialog.hide();
         _searchDesc.removeClass('person_desc_active');
       }
    });

    _searchDialog.on('click', function (e) {
      e.stopPropagation();
    });

    $('body').on('click', function (e) {//点击其他地方时，隐藏房间人数选择器
      if (e.target !== _searchDesc[0]) {
        _searchDialog.hide();
        _searchDesc.removeClass('person_desc_active');
      }
    });

    // home banner spinner(+-btn to change value) & total to parent input
    $('.btn-subtract, .btn-add').on('click', function() {
        var btn = $(this);
        var btnId = $(this).context.id;
        var targetId = btnId.replace(/Add|Subtract/g, '');

        input = btn.closest('.number-spinner').find('input');
        inputName = input[0].name;
        oldValue = input.val().trim();
        var targetSub = $('#' + targetId + 'Subtract');
        var targetAdd = $('#' + targetId + 'Add');
        if (btn.attr('data-dir') === 'up') {
            if (oldValue < input.attr('max')) {
                oldValue++;
                if (targetSub.hasClass('disabled-btn')) {
                    targetSub.removeClass('disabled-btn');
                    targetSub.attr('disabled', false);
                }
            }
            if (oldValue === input.attr('max')) {
                if (targetAdd.hasClass('disabled-btn')) {
                } else {
                    targetAdd.addClass('disabled-btn');
                    targetAdd.attr('disabled', true);
                }
            } else {
                if (targetAdd.hasClass('disabled-btn')) {
                    targetAdd.removeClass('disabled-btn');
                    targetAdd.attr('disabled', false);
                }
            }
        } else {
            if (oldValue > input.attr('min')) {
                oldValue--;
                if (targetAdd.hasClass('disabled-btn')) {
                    targetAdd.removeClass('disabled-btn');
                    targetAdd.attr('disabled', false);
                }
            }
            if (oldValue === input.attr('min')) {
                if ($('#' + targetId + 'Subtract').hasClass('disabled-btn')) {
                } else {
                    targetSub.addClass('disabled-btn');
                    targetSub.attr('disabled', true);
                }
            } else {
                if (targetSub.hasClass('disabled-btn')) {
                    targetSub.removeClass('disabled-btn');
                    targetSub.attr('disabled', false);
                }
            }
        }

        // 修改 spinner 数字
        input.val(oldValue);
        // 修改上部数字显示和发送给后端的hidden input
        if (inputName === 'roomCountInp') {
            $('#tRoom').text(oldValue);
            $('#topRoomCount').val(oldValue);
            //检测修改单复数单词
            if (1 === isUsitour && 1 === oldValue) {
                $('#tRoom_text').text('room, ');
            } else if (1 === isUsitour && oldValue > 1) {
                $('#tRoom_text').text('rooms, ');
            }

        } else if (inputName === 'adultCountInp') {
            $('#tAdult').text(oldValue);
            $('#topAdultCount1').val(oldValue);
            //检测修改单复数单词
            if (1 === isUsitour && 1 === oldValue) {
                $('#tAdult_text').text('adult, ');
            } else if (1 === isUsitour && oldValue > 1) {
                $('#tAdult_text').text('adults, ');
            }
            if (1 === oldValue) {
                $('#adultCountSubtract').addClass('disabled-btn');
                $('#adultCountSubtract').attr('disabled', true);
            }
        } else if (inputName === 'childCountInp') {
            $('#tChild').text(oldValue);
            $('#topChildCount1').val(oldValue);
            //检测修改单复数单词
            if (1 === isUsitour && oldValue <= 1) {
                $('#tChild_text').text('child, ');
            } else if (1 === isUsitour && oldValue > 1) {
                $('#tChild_text').text('children, ');
            }
            if (0 === oldValue) {
                $('#childCountSubtract').addClass('disabled-btn');
                $('#childCountSubtract').attr('disabled', true);
                $('#childAgeWrapper').addClass('hide');
            } else {
                $('#childAgeWrapper').removeClass('hide');
                var content = $('#childAgeContent');
                var select_box = $('.child-age-select');
                var count = select_box.length;
                if (oldValue > count) {
                    for (var i = count; i < oldValue; i++) {
                        content.append(select_box.last().clone());
                    }
                } else {
                    for (var i = oldValue; i < count; i++) {
                        content.find('.child-age-select:last').remove();
                    }
                }
            }
        }
    });
    // end 选择房间数量，成人数量，儿童数量

    // TODO: 载入图片添加loading效果
    $('.ad-slider .ad-slider-item').fadeIn('fast');
    // 点击 特惠图片，跳转链接
    $('.destinationName, .date').focusin(function() {
        $(this).closest('li').addClass('active');
    });
    $('.destinationName, .date').focusout(function() {
        $(this).closest('li').removeClass('active');
    });

    // subscribe form submit action
    $('#subscribeBox').submit(function(e) {
        e.preventDefault();
        subscribe();
    });

    /*
    * 热点城市切换
    * */
    $('.h-recom-wrap .js-remen li').click(function() {
        var _this = $(this);
        var _index = _this.index();
        _this.find('a').addClass('active');
        _this.siblings().find('a').removeClass('active');
        $('.h-recom-wrap .city-recom-wrap').eq(_index).show().siblings('.city-recom-wrap').hide();
    });

    /*
     * 精品推荐切换
     * */
    $('.h-recom-wrap .js-tuijian li').click(function() {
        var _this = $(this);
        var _index = _this.index();
        _this.find('a').addClass('active');
        _this.siblings().find('a').removeClass('active');
        $('.h-recom-wrap .hotel-recom-wrap').eq(_index).show().siblings('.hotel-recom-wrap').hide();
    });

});
//end document ready
