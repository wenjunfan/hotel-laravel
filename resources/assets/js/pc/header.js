var langCode = 'gb2312';
var lang = localStorage.getItem('lang');
if (lang == 2) {
    langCode = 'big5';
} else if (lang == 1) {
    langCode = 'utf-8';
}

function global () {}

function Url (url) {this.url = url || window.location.href}

function otherPageMenu () {
    var oMenuBox = $(".menu-box"),
        oMenuUl = $("#Js-menu-items"),
        oMenuLi = oMenuUl.find("li"),
        oMenuTagDl = $("#Js-menu-tag"),
        oMenuTagDd = oMenuTagDl.find("dd"),
        oMenuTagWrap = $(".menu-tag-con"),
        timer = null,
        oMenuTimer = null,
        oTheme = $("#Js-theme");
    oTheme.hover(function () {clearTimeout(oMenuTimer), oMenuBox.show()}, function () {oMenuTimer = setTimeout(function () {oMenuBox.hide()}, 200)}), oMenuLi.hover(function () {
        clearTimeout(oMenuTimer), clearTimeout(timer), oMenuTagWrap.show();
        var index = $(this).attr("dataIndex");
        oMenuTagDd.hide(), oMenuTagDd.eq(index).show(), 0 == index ? (oMenuLi.css({
            borderRight: "2px solid #1d8eed",
            borderBottom: "1px dashed #bebebe",
            height: "70px"
        }).last().css({borderBottom: "none"}), $(this).css({
            borderRight: "none",
            borderBottom: "2px solid #1d8eed",
            height: "69px"
        }), oMenuBox.css({
            borderRight: "none",
            borderRadius: "0 0 0 5px"
        }), oMenuTagWrap.css({
            borderTop: "none",
            height: 354,
            top: 40
        })) : index == oMenuLi.length - 1 ? (oMenuLi.css({
            borderRight: "2px solid #1d8eed",
            borderBottom: "1px dashed #bebebe",
            height: "70px"
        }).last().css({borderBottom: "none"}), $(this).css({borderRight: "none"}).prev().css({
            borderBottom: "2px solid #1d8eed",
            height: "69px"
        }), oMenuBox.css({
            borderRight: "none",
            borderRadius: "0 0 0 5px"
        }), oMenuTagWrap.css({
            borderTop: "2px solid #1d8eed",
            height: 158,
            top: 234
        })) : (oMenuLi.css({
            borderRight: "2px solid #1d8eed",
            borderBottom: "1px dashed #bebebe",
            height: "70px"
        }).last().css({borderBottom: "none"}), $(this).css({
            borderRight: "none",
            borderBottom: "2px solid #1d8eed",
            height: "69px"
        }).prev().css({
            borderBottom: "2px solid #1d8eed",
            height: "69px"
        }), oMenuBox.css({
            borderRight: "none",
            borderRadius: "0 0 0 5px"
        }), oMenuTagWrap.css({
            borderTop: "none",
            height: 354,
            top: 40
        })), oMenuUl.width(238), $(this).css("backgroundPosition", "220px -218px")
    }, function () {
        timer = setTimeout(function () {
            oMenuTagWrap.hide(), oMenuBox.css({
                borderRight: "2px solid #1d8eed",
                borderRadius: "0 0 5px 5px"
            }), oMenuLi.css({
                borderRight: "none",
                borderBottom: "1px dashed #bebebe",
                height: "70px",
                backgroundPosition: "220px -148px"
            }).last().css({borderBottom: "none"}), oMenuUl.width(236)
        }, 200), oMenuTimer = setTimeout(function () {oMenuBox.hide()}, 200), $(this).css("backgroundPosition", "220px -148px")
    }), oMenuTagWrap.hover(function () {clearTimeout(timer), clearTimeout(oMenuTimer)}, function () {
        timer = setTimeout(function () {
            oMenuTagWrap.hide(), oMenuBox.css({
                borderRight: "2px solid #1d8eed",
                borderRadius: "0 0 5px 5px"
            }), oMenuLi.css({
                borderRight: "none",
                borderBottom: "1px dashed #bebebe",
                height: "70px",
                backgroundPosition: "220px -148px"
            }).last().css({borderBottom: "none"}), oMenuUl.width(236)
        }, 20), oMenuTimer = setTimeout(function () {oMenuBox.hide()}, 200)
    })
}

var isTouch = function () {
        for (var userAgentInfo = navigator.userAgent, Agents = ["Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod"], flag = !1, v = 0; v < Agents.length; v++) if (userAgentInfo.indexOf(Agents[v]) > 0) {
            flag = !0;
            break
        }
        return flag
    }(),
    canReShoppingCar = !0;

window.usitripLoginStatus = {isLogin: !1}, $(document).ready(function () {
    $("li.userhome").hover(function () {$(this).addClass("uhover"), $(this).find(".uhmenutree").stop(!0, !0).slideDown(300)}, function () {$(this).removeClass("uhover"), $(this).find(".uhmenutree").stop(!0, !0).hide()}), $(".navItems li").
        click(function () {$(this).addClass("current").siblings("li").removeClass("current")}), $("#miniShopCartBox").hover(function () {
        var total = $(this).find("a.shoptotal").find("em").text();
        0 == parseInt(total) ? $("#shopView").hide() : $("#shopView").stop(!0, !0).slideDown(300)
    }, function () {$("#shopView").stop(!0, !0).slideUp(10)}), $(".desthelp").hover(function () {$(this).find(".desthelpTips").toggleClass("hide")}), $(".search_hot_list").find("a").each(function () {"黄石公园" == $(this).text() && $(this).css({color: "#fe8902"})})
}), $(document).ready(function () {$("#pro-fixed-bar").find("a.bar-close").click(function () {$("#pro-fixed-bar").hide()})}), global.prototype = {
    getCookie: function (c_name) {
        var c_start = null,
            c_end = null;
        return document.cookie.length > 0 && (c_start = document.cookie.indexOf(c_name + "="), -1 != c_start)
            ? (c_start = c_start + c_name.length + 1, c_end = document.cookie.indexOf(";", c_start), -1 == c_end && (c_end = document.cookie.length), unescape(document.cookie.substring(c_start, c_end)))
            : ""
    },
    setCookie: function (name, value, expires, path, domain, secure) {
        var str = name + "=" + encodeURI(value);
        expires && (str += "; expires=" + expires.toGMTString()), path && (str += "; path=" + path), domain && (str += "; domain=" + domain), secure && (str += "; secure"), document.cookie = str
    },
    removeCache: function (url) {
        var t_url = window.location.href.toString(),
            url = this.url_rand(url);
        url += "&ajax=true&callback=?", $.getJSON(url, {
            action: "removeCache",
            t_url: t_url
        }, function (json) {"OK" == json.status ? (alert("清除成功数：" + json.success_num + "，路径：" + json.path + "，刷新查看最新页面。"), window.location.reload(!0)) : alert("删除失败！")})
    },
    isModelFix: function (url) {
        var url = this.url_rand(url);
        url += "&ajax=true&action=isAdminLogin&callback=?", $.getJSON(url, function (json) {
            if (json.code && 0 != json.LID) $(".m-pro-cont-l p").each(function () {"" != this.innerHTML && (this.innerHTML = this.innerHTML.replace("旅游团号", "").replace(":", "").replace("：", ""))}), $(".JS_ismodel_box").show(); else {
                var M = $(".JS_ismodel");
                M.length && M.each(function (i) {
                    var old_html = $(this).html(),
                        new_html = old_html.replace(/.+\-/, "");
                    $(this).html(new_html).attr("old", old_html), $(".JS_ismodel_box").show()
                })
            }
        })
    },
    isIE8: function () {
        var userAgent = window.navigator.userAgent.toLowerCase();
        return $.browser.msie8 = $.browser.msie && /msie 8\.0/i.test(userAgent), $.browser.msie7 = $.browser.msie && /msie 7\.0/i.test(userAgent), $.browser.msie6 = !$.browser.msie8 && !$.browser.msie7 && $.browser.msie && /msie 6\.0/i.test(userAgent), $.browser.msie8 || $.browser.msie7 ||
        $.browser.msie6 ? !0 : !1
    },
    addOne: function () {
        $(".addOne").css({
            opacity: "100",
            display: "block",
            left: 20,
            top: 0
        }).stop().animate({
            top: "0",
            opacity: 0,
            display: "none"
        }, 1e3).css({
            left: 20,
            top: 20
        })
    },
    "throws": function (obj) {
        var starX = obj.startTarget.offset().left + obj.startTarget.width() / 2 - 10,
            starY = obj.startTarget.offset().top + obj.startTarget.height() / 2 - 10,
            endTarget = obj.endTarget,
            endX = endTarget.offset().left + endTarget.width() / 2 - 10,
            endY = endTarget.offset().top + endTarget.height() / 2 - 10,
            slowY = 0;
        if (!obj.throwImg.is(":animated")) switch (obj.throwImg.css({
            display: "block",
            left: starX + "px",
            top: starY + "px",
            display: "block",
            opacity: 1,
            width: "50px",
            height: "50px",
            "z-index": 100
        }), obj.throwWay) {
            case 1:
                !function () {
                    obj.throwImg.stop().animate({
                        left: endX + "px",
                        top: endY + "px",
                        width: "14px",
                        height: "14px"
                    }, 1e3).fadeOut(), setTimeout(G.addOne, 1e3)
                }();
                break;
            case 2:
                !function () {
                    var p = 0,
                        X = 0,
                        Y = 0,
                        timer = setInterval(function () {
                            X++, endY > starY ? (p = (endX - starX) * (endX - starX) / (-2 * (endY - starY)), Y = X * X / (-2 * p), obj.throwImg.css({
                                left: X + starX + "px",
                                top: Y + starY + "px"
                            })) : (p = (endY - starY) * (endY - starY) / (2 * (endX - starX)), Y = Math.sqrt(2 * p * X), obj.throwImg.css({
                                left: X + starX + "px",
                                top: starY - Y + "px"
                            })), X + starX >= endX && clearInterval(timer)
                        }, 2);
                    obj.throwImg.stop().animate({
                        width: "14px",
                        height: "14px"
                    }, 1e3).fadeOut(), setTimeout(G.addOne, 1e3)
                }();
                break;
            case 3:
                !function () {
                    slowY = endY >= starY ? -70 : 50, obj.throwImg.stop().animate({
                        left: endX - 14 + "px",
                        top: endY + slowY + "px"
                    }, 700).animate({
                        top: endY + "px",
                        opacity: 0,
                        "z-index": 0
                    }, 1e3), setTimeout(G.addOne, 1500)
                }();
                break;
            case 4:
                !function () {
                    obj.throwImg.css({display: "none"});
                    var img = obj.throwImg[0].src,
                        flyer = $('<img class="flyer-img" src="' + img + '">'),
                        scrollTop = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop,
                        scrollLeft = document.documentElement.scrollLeft || window.pageXOffset || document.body.scrollLeft;
                    flyer.fly({
                        start: {
                            left: starX - scrollLeft,
                            top: starY - scrollTop
                        },
                        end: {
                            left: endX - scrollLeft,
                            top: endY - scrollTop,
                            width: 14,
                            height: 14
                        },
                        onEnd: function () {this.destory(), setTimeout(G.addOne, 500)}
                    })
                }();
                break;
            case 5:
                !function () {
                    var top = 0,
                        d_top = 60,
                        a = 0,
                        b = 0,
                        X = 0,
                        Y = 0,
                        timer = setInterval(function () {
                            if (X++, endY > starY) {
                                top = -d_top;
                                var dY = endY - starY,
                                    dX = endX - starX;
                                b = (Math.sqrt(Math.pow(4 * top * dX, 2) - 16 * dY * top * dX * dX) + 4 * top * dX) / (2 * dY), a = -4 * top / (b * b), Y = a * X * (X - b), obj.throwImg.css({
                                    left: X + starX + "px",
                                    top: Y + starY + "px"
                                })
                            } else {
                                var dY = endY - starY,
                                    dX = endX - starX;
                                top = dY - d_top, b = (Math.sqrt(Math.pow(4 * top * dX, 2) - 16 * dY * top * dX * dX) + 4 * top * dX) / (2 * dY), a = -4 * top / (b * b), Y = a * X * (X - b), obj.throwImg.css({
                                    left: X + starX + "px",
                                    top: Y + starY + "px"
                                })
                            }
                            X + starX >= endX && clearInterval(timer)
                        }, 2);
                    obj.throwImg.stop().animate({
                        width: "14px",
                        height: "14px"
                    }, 500).fadeOut(), setTimeout(G.addOne, 1e3)
                }();
                break;
            default:
                !function () {}()
        }
    },
    showPop: function () {
        var content = $("#hbody").length > 0 ? $("#hbody") : $("#body");
        if (0 != content.length) {
            var contentWidth = content.width(),
                contentOffsetTop = content.offset().top,
                contactWrap = $(".js-contact-wrap"),
                contactWidth = contactWrap.width(),
                winW = $(window).width(),
                popLeft = "0";
            popLeft = contentWidth + 2 * contactWidth + 10 > winW ? winW - contactWidth - 10 : (winW - contentWidth) / 2 + contentWidth + 10;
            var popTop = contentOffsetTop + 5;
            contactWrap.css({
                right: "auto",
                top: popTop + "px",
                left: popLeft + "px"
            })
        }
    },
    hideMessage: function () {setTimeout(function () {$("#message").fadeOut(1e3)}, 3e3)},
    getLoginStatus: function (url) {
        var _this = this;
        this.shoppingCarUrl = url = this.url_rand(url);
        var ID = document.getElementById("loginOrLogin");
        var loginText = '登录';
        if (localStorage.getItem('lang') == 2) {
            loginText = '登入';
        }
        window.is_login = !1, $.ajax({
            type: "GET",
            url: url,
            data: {
                action: "loginAndShoppingCartCheckForTop",
                previousURL: URL.url
            },
            dataType: "jsonp",
            success: function (data) {
                function getid (id) {return document.getElementById(id)}

                function initscroll () {
                    notice_0 = getid("notice_0");
                    notice_1 = getid("notice_1");
                    if (notice_0 === null && notice_1 === null) {
                        return;
                    }
                    for (var num = Math.floor(notice_0.offsetWidth / notice_1.offsetWidth) + 1, i = 0; num > i; i++) {
                        var span = document.createElement("span");
                        span.innerHTML = notice_1.innerHTML, notice_0.appendChild(span)
                    }
                    scroll_time = setInterval(scroll, 30)
                }

                function scroll () {notice_1.offsetWidth <= notice_0.scrollLeft ? notice_0.scrollLeft -= notice_1.offsetWidth - 1 : notice_0.scrollLeft += 1}

                if ("object" == typeof data && 1 == data.status) {
                    var html = "",
                        loginWrap = $("#login-wrap"),
                        returnContent = data.html;
                    returnContent.regUrl = returnContent.regUrl + '&customers_referer_type=1&lang=' + langCode;
                    var currentUrl = window.location.href;

                    var loginUrl = returnContent.loginUrl;
                    var signUpUrl = returnContent.regUrl;
                    if (currentUrl.indexOf('supervacation') > -1) {
                        loginUrl = 'http://www.supervacation.net/index.php?mod=login&language_code=big5';
                        signUpUrl = 'http://www.supervacation.net/index.php?mod=create_account&language_code=big5';
                    } else if (langCode === 'big5') {
                        loginUrl = 'https://post.usitrip.com/index.php?mod=login&language_code=big5';
                        signUpUrl = 'https://post.usitrip.com/index.php?mod=create_account&language_code=big5';
                    }

                    if (returnContent.logStatus ? (window.is_login = !0, window.usitripLoginStatus = {
                        isLogin: !0,
                        name: returnContent.name
                    },
                        html += '<span class="login-status js-login-status"><span class="login-hello">您好，</span>', html += '<a class="login-name js-login-name">', html += '<span class="simple-name">' + returnContent.name + "</span>",
                        html += '<span class="full-name">' + returnContent.name + "</span>",
                        html += "</a>",
                        html += '<a class="go-out" onclick="logoutUsitrip(\'' + returnContent.logOffUrl + '\')">[退出]</a>',
                        html += '<input id="JS_UsiCId" type="hidden" value="' + returnContent.customersId + '">',
                        html += "</span>") : (window.is_login = !1,
                        html += '<span class="logout-status js-logout-status">' +
                            '<a class="logi-btn" href="' + loginUrl + '">' + loginText + '</a>' +
                            '<a class="regi-btn" onclick="gtag_report_conversion_register(\'' + signUpUrl + '\')" href="' + signUpUrl + '">免费注册</a>' +
                            '</span>'), data.announce.length > 0) {
                        html += '<span class="notice"><div  id="notice_0"><span id="notice_1">';
                        for (var i = 0; i < data.announce.length; i++) html += '<a href="' + data.announce[i].hrefInfo + '" target="_blank">' + data.announce[i].title + "</a>";
                        html += "</span></div></span>"
                    }

                    if (loginWrap.children().first().nextAll().detach(), loginWrap.append(html), data.announce.length > 0) {
                        var scroll_time,
                            notice_0,
                            notice_1;
                        $("#notice_0").hover(function () {clearInterval(scroll_time)}, function () {scroll_time = setInterval(scroll, 30)}), $.ready(initscroll())
                    }
                    var simpleName = loginWrap.find(".simple-name"),
                        wid = simpleName.width();
                    wid > 150 && simpleName.css("width", 150), loginWrap.find(".js-login-name").attr("href", returnContent.myAccountUrl);
                    var myUsitrip = $("#js-myusi");
                    if (myUsitrip.find(".myusi-text .title").attr("href", returnContent.myAccountUrl), $(ID).html(data.html), data.shopcart.list && $("#shopviewList li").length) {
                        $("#CarSumTop, #CarSumTop1").text(data.shopcart.sum), $("#cartBoxTotal").text(data.shopcart.money);
                        for (var tpl = $("#shopviewList li").html(), li_html = "", i = 0; i < data.shopcart.list.length; i++) {
                            var DelAction = "G.cartRemove('" + data.shopcart.list[i].id + "','" + url + "&action=remove_product')";
                            li_html += "<li>";
                            var proName = data.shopcart.list[i].name;
                            proName = proName.replace("<", ""), proName = proName.replace(">", ""), li_html += tpl.replace(/___ProductName___/g, proName).
                                replace(/___ProductFinalPrice___/g, data.shopcart.list[i].price_str).
                                replace(/___ProductImgSrc___/g, data.shopcart.list[i].img_src).
                                replace(/___src___/g, "src").
                                replace(/___ProductLinks___/g, data.shopcart.list[i].product_href).
                                replace(/___DelAction___/g, DelAction), li_html += "</li>"
                        }
                        $("#shopviewList").html(li_html)
                    }
                    _this.renderShoppingCar(data)
                } else alert("程序异常getLoginStatus！")
            }
        })
    },

    renderShoppingCar: function (data) {
        var cart,
            cartList = $("#js-cart-list"),
            shopCart = data.shopcart,
            cartContent = shopCart.list,
            sum = shopCart.sum,
            money = shopCart.money,
            content = "";
        money = G.curReplace(money);
        for (var i = 0, len = cartContent.length; len > i; i++) {
            cart = cartContent[i];
            var delAction = "G.cartRemove('" + cart.id + "','" + this.shoppingCarUrl + "&action=remove_product')";
            content += "<dd>", content += '<a href="' + cart.product_href + '" class="pro-detail">', content += '<img src="' + cart.img_src + '">', content += "<span>" + cart.name.replace(/[<>]/g, "") +
                "</span>", content += "</a>", content += '<div class="pro-action">', content += '<strong class="price">' + cart.price_str + '</strong><a onclick="' + delAction + '" href="javascript:void(0);" class="del">删除</a>', content += "</div>", content += "</dd>"
        }
        cartList.empty().append(content);
        var proTotal = $("#js-pro-total");
        proTotal.find("em").text(sum), proTotal.find(".total").text(money);
        var cartWrap = $("#js-cart");
        cartWrap.find(".cart-text .title strong").text(sum), $("#JS_shoppingNum").text(sum)
    },
    reShoppingCar: function () {
        var _this = this;
        1 == canReShoppingCar && (canReShoppingCar = !1, $.ajax({
            type: "GET",
            url: _this.shoppingCarUrl,
            data: {
                action: "loginAndShoppingCartCheckForTop",
                previousURL: URL.url
            },
            dataType: "jsonp",
            success: function (data) {_this.renderShoppingCar(data)}
        }))
    },
    curReplace: function (cur_price) {
        var p = cur_price.replace("&#65509;", "￥");
        return p = p.replace("&#163;", "£").replace("&pound;", "£"), p = p.replace("&euro;", "€"), p = p.replace("&#165;", "¥")
    },
    writeLoginInfo: function (url) {
        url = this.url_rand(url);
        var ID = document.getElementById("loginOrLogin");
        $.ajax({
            type: "GET",
            url: url,
            data: {
                action: "loginAndShoppingCartCheckForTop",
                previousURL: URL.url
            },
            dataType: "jsonp",
            success: function (data) {
                function TopShopCart (data) {
                    if (data.shopcart.list && $("#shopviewList li").length) {
                        $("#CarSumTop, #CarSumTop1").text(data.shopcart.sum), $("#cartBoxTotal").text(data.shopcart.money);
                        for (var tpl = $("#shopviewList li").html(), li_html = "", i = 0; i < data.shopcart.list.length; i++) {
                            var DelAction = 'G.cartRemove("' + data.shopcart.list[i].id + '","' + url + '&action=remove_product")';
                            li_html += "<li>";
                            var proName = data.shopcart.list[i].name;
                            proName = proName.replace("<", ""), proName = proName.replace(">", ""), li_html += tpl.replace(/___ProductName___/g, proName).
                                replace(/___ProductFinalPrice___/g, data.shopcart.list[i].price_str).
                                replace(/___ProductImgSrc___/g, data.shopcart.list[i].img_src).
                                replace(/___src___/g, "src").
                                replace(/___ProductLinks___/g, data.shopcart.list[i].product_href).
                                replace(/___DelAction___/g, DelAction), li_html += "</li>"
                        }
                        $("#shopviewList").html(li_html)
                    }
                }

                "object" == typeof data && 1 == data.status ? ($(ID).html(data.html), TopShopCart(data)) : alert("程序异常writeLoginInfo！")
            }
        })
    },
    cartRemove: function (
        prid_str, url,
        x
    ) {confirm("确定从购物车中删除此商品？") && (url = this.url_rand(url), url += "&ajax=true&action=remove_product&products_id=" + prid_str + "&callback=?", $.getJSON(url, function (json) {json.removeSuccess && "1" == json.removeSuccess && (alert("删除成功！"), window.location.reload())}))},
    getBookingBox: function (url, products_id_str, BookingBoxId, otherParam) {
        "" != products_id_str && (url = this.url_rand(url), otherParam &&
        ("object" == typeof otherParam ? (otherParam.t_companion_id && (url += "&t_companion_id=" + otherParam.t_companion_id), otherParam.preview && (url += "&preview=" + otherParam.preview)) : "string" == typeof otherParam && (url += "&" + otherParam)), url += "&products_id=" + products_id_str +
            "&action=getBookingBox&callback=?", $.getJSON(url, function (json) {
            if ("soldout" == json.status) $(".JS-pro-order-panel").hide(), $("#product_mail_box").attr("src", iframe_url).show(); else {
                if (json.top_div_id = BookingBoxId, usitrip.product.BookingBoxNew) {new usitrip.product.BookingBoxNew(json)} else {new usitrip.product.BookingBox(json)}
                json.products_range_top && (window.products_range_top = json.products_range_top)
            }
        }))
    },
    getCalendarData: function (url, products_id, year, month) {
        "" != products_id && (url = this.url_rand(url), url += "&departure_year=" + year + "&departure_month=" + month + "&products_id=" + products_id + "&action=getCalendarData&callback=?", $.getJSON(url, function (json) {
            if (json) {
                for (var html = "<ul>", i = 0; i < json.length; i++) html += "<li>" + json[i].departure_date + ":" + json[i].departure_year + ":" + json[i].departure_month + ":" + json[i].departure_week + ":" + json[i].products_price + "……</li>";
                html += "</ul>", $("#J_Calendarbox").html(html)
            }
        }))
    },
    get_form_data: function (form_id, output_type) {
        for (var form = document.getElementById(form_id), aparams = new Array, eval_string = new Array, i = 0; i < form.elements.length; i++) {
            var name = encodeURIComponent(form.elements[i].name),
                value = "";
            if ("radio" == form.elements[i].type || "checkbox" == form.elements[i].type) {
                var a = "";
                1 == form.elements[i].checked ? (a = form.elements[i].value, value = encodeURIComponent(a)) : name = ""
            } else value = encodeURIComponent(form.elements[i].value);
            if ("" != name) {
                var _l = aparams.length;
                aparams[_l] = new Array, aparams[_l][name] = value, eval_string[eval_string.length] = name + "=" + value
            }
        }
        if ("array" == output_type) return aparams;
        var string = eval_string.join("&");
        return string += "&ajax=true"
    },
    htmlspecialchars: function (str) {return str = str.replace(/&/g, "&amp;"), str = str.replace(/</g, "&lt;"), str = str.replace(/>/g, "&gt;"), str = str.replace(/'/g, "&acute;"), str = str.replace(/"/g, "&quot;"), str = str.replace(/\|/g, "&brvbar;")},
    _goto: function (obj) {$("html,body").animate({scrollTop: $(obj).position().top})},
    url_rand: function (url) {return url += url.indexOf("?") > 0 ? "&randnumforajaxaction=" + Math.random() : "?randnumforajaxaction=" + Math.random()},
    countDown: function (remainingTime, oDayEl, oHourEl, oMinEl, oSecondEl, callback) {
        function timer () {
            var ts = new Date(remainingTime) - new Date;
            if (0 >= ts) return $(oDayEl).parent().css({visibility: "hidden"}), void (callback && callback instanceof Function && callback());
            var dd = parseInt(ts / 1e3 / 60 / 60 / 24, 10),
                hh = parseInt(ts / 1e3 / 60 / 60 % 24, 10),
                mm = parseInt(ts / 1e3 / 60 % 60, 10),
                ss = parseInt(ts / 1e3 % 60, 10);
            dd = zeroFill(dd), hh = zeroFill(hh), mm = zeroFill(mm), ss = zeroFill(ss), oDayEl && oDayEl.text(dd), oHourEl && oHourEl.text(hh), oMinEl && oMinEl.text(mm), oSecondEl && oSecondEl.text(ss), setTimeout(timer, 1e3)
        }

        function zeroFill (i) {return 10 > i && (i = "0" + i), i}

        timer()
    },
    isIE6: function () {
        var ie6 = !-[1] && !window.XMLHttpRequest;
        return ie6
    },
    hideSubTotal: function (TotalBox) {
        var Box = $(TotalBox);
        2 == Box.length && Box.each(function (i) {"Ot_SubTotal" == $(this).attr("rel") && $(this).hide()})
    },
    updateCurrency: function () {
        var curPrice = {
            bind: function () {
                $("#topCurrencyBox ul li a").click(function () {
                    var self = $(this),
                        v = (self.text(), self.attr("cur").toUpperCase());
                    $.removeCookie("currency", {
                        domain: document.domain,
                        path: "/"
                    }), $.cookie("currency", v, {
                        domain: document.domain,
                        path: "/"
                    }), window.location.reload(!1)
                })
            },
            edit: function (code) {
                $(".js_product_special_price").each(function (i) {
                    var m = $(this).attr("cur_" + code);
                    m && $(this).text(m)
                }), $("em").each(function (i) {
                    var m = $(this).attr("cur_" + code);
                    m && $(this).text(m)
                }), $("del").each(function (i) {
                    var m = $(this).attr("cur_" + code);
                    m && $(this).text(m)
                })
            },
            init: function () {
                var that = this;
                this.bind(), $(function () {
                    // var code = $.cookie("currency");
                    var code = 0; // todo
                    if (void 0 == code || "" == code) {
                        var code = "usd";
                        that.edit(code.toLowerCase())
                    } else {
                        that.edit(code.toLowerCase());
                        var a = new Array;
                        $("#topCurrencyBox ul li a").each(function (i) {a[$(this).attr("cur")] = $(this).text()}), $("#defCurrency").text(a[code.toLowerCase()])
                    }
                })
            }
        };
        curPrice.init()
    },

    inputDefaultTip: function (config) {
        function _inputDefaultTip (config) {
            return config.inputEl = config.inputEl ? config.inputEl : null, this.pla_color = config.pla_color ? config.pla_color : "", null == config.inputEl ? void alert("inputDefaultTip配置参数中inputEl不能为空！") : (this.extra_dt = null, "string" == typeof config.extra_default_tootip &&
            (this.extra_dt = config.extra_default_tootip), this.el = "string" == typeof config.inputEl ? $(config.inputEl) : config.inputEl, this.browserType = "", this.browserVersion = 11, this.checkBrowser(), void ("IE" == this.browserType && this.browserVersion <= 9 &&
                (this.init(), this.bind())))
        }

        return _inputDefaultTip.prototype.checkBrowser = function () {
            navigator.userAgent.indexOf("MSIE") > 0 && (navigator.userAgent.indexOf("MSIE 6.0") > 0 ? (this.browserType = "IE", this.browserVersion = 6) : navigator.userAgent.indexOf("MSIE 7.0") > 0 ? (this.browserType = "IE", this.browserVersion = 7) : navigator.userAgent.indexOf("MSIE 8.0") > 0
                ? (this.browserType = "IE", this.browserVersion = 8)
                : navigator.userAgent.indexOf("MSIE 9.0") > 0 ? (this.browserType = "IE", this.browserVersion = 9) : navigator.userAgent.indexOf("MSIE 10.0") > 0 && (this.browserType = "IE", this.browserVersion = 10))
        }, _inputDefaultTip.prototype.init = function () {"" != this.el.attr("placeholder") && (this.el.val(this.el.attr("placeholder"))[0].style.color = this.pla_color)}, _inputDefaultTip.prototype.bind = function () {
            var that = this;
            this.el.focus(function (e) {e = e || event, e.stopPropagation(), that.el.val() == that.el.attr("placeholder") && (that.el.val("")[0].style.color = ""), null != that.extra_dt && that.el.val() == that.extra_dt && that.el.val("")}), this.el.blur(function (e) {
                e = e || event, e.stopPropagation();
                var val = that.el.val(),
                    reg = /^\s*|\s*$/;
                val = val.replace(reg, ""), "" == val && (that.el.val(that.el.attr("placeholder"))[0].style.color = that.pla_color)
            })
        }, new _inputDefaultTip(config)
    }
};
var G = new global;
Url.prototype = {
    getParame: function () {
        var string = this.url.replace(/.+\?/, "").replace(/\#+.{0,}/, "") || null;
        return this.parse(string)
    },
    getAnchor: function () {
        var string = this.url.split("#")[1] || null;
        return string
    },
    parse: function (string) {
        if (null != string) {
            for (var Parames = [], strs = string.split("&"), i = 0; i < strs.length; i++) Parames[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
            return Parames
        }
        return !1
    },
    getPageAddress: function () {
        var pAddress = this.url.toString(),
            array = pAddress.split("?");
        return array[0]
    },
    clearDomain: function () {return this.url.indexOf("http") > -1 ? "/" + this.url.replace(/https?:\/\/.*\//, "") : this.url}
};
var URL = new Url;

//检查登录状态
var get_login_status_href = "https://post.usitrip.com/ajax.php?mod=login&language_code=" + langCode;

G.getLoginStatus(get_login_status_href);
$('#js-cart').mouseover(function () {
    G.reShoppingCar();
});

window.logoutUsitrip = function (logoutUrl) {
    $.post(logoutUrl, {
        '_token': $("meta[name='csrf_token']").attr('content')
    });
    window.open(logoutUrl, '_self');
};

/*防止ie加载问题*/
!function ($) {
    $(function () {
        $("body").append('<div id="tooltip-wrap" class="tooltip-default"><span id="tooltip-arrow"></span><div id="tooltip-con"></div></div>');
        var b,
            i = $("#tooltip-wrap"),
            c = $("#tooltip-con"),
            j = $("#tooltip-arrow"),
            g = j.width(),
            h = $(window),
            d = i.outerWidth(),
            f = (d - g) / 2;
        j.css({left: f});
        var e = function () {i.fadeOut()};
        $(".tooltip").on("mouseenter", function () {
            clearTimeout(b);
            var o = $(this),
                direction = o.hasClass("top") ? "top" : "bottom",
                n = o.outerHeight(),
                r = o.outerWidth(),
                u = 0,
                m = o.attr("tooltip"),
                t = o.find(".tooltipCon");
            if ((m || "" != t.text()) && "" != m) {
                o.attr("tooltip") ? c.html(m) : (c.html(""), t.clone().show().appendTo(c));
                var s = o.offset().left;
                if ("top" == direction) var q = o.offset().top + -75 + n; else {
                    var q = o.offset().top + 5 + n;
                    j.css("top", "-8px"), j.css("backgroundImage", "url(/usiadmin/images/icons/tip_top.png)")
                }
                var p = h.width(),
                    l = s - (d - r) / 2;
                if (d + l > p) {
                    var k = d + l - p;
                    i.css({left: l - k}), j.css({left: f + k + u})
                } else 0 > l ? (i.css({left: 0}), j.css({left: f + l + u})) : (i.css({left: l}), j.css({left: f + u}));
                i.css({top: q}), i.fadeIn()
            }
        }).on("mouseleave", function () {b = setTimeout(e, 300)})
    })
}(jQuery);
var g_allow_send = 1;
jQuery(document).ready(function ($) {
    function sendData (url, limit, val, tar) {
        0 != g_allow_send && (g_allow_send = 0, url = G.url_rand(url), url += "&ajax=true&val=" + val + "&type=" + type_id + "&limit=" + limit + "&callback=?", $(".hdAutocomplete").hide(), $.getJSON(url, function (data) {
            g_allow_send = 1;
            var _ul,
                i;
            if (data.rows.length > 0) {
                var _ul = tar.parents(".hdsearchIpt").find(".hdAutocomplete");
                _ul.empty(), _sn = -1, $(".hdAutocomplete").hide();
                for (var i = 0; i < data.rows.length; i++) if (data.rows[i].sub_data) for (var j = 0; j < data.rows[i].sub_data.length; j++) $(".hdAutocomplete").hide(), _ul.append('<li class="t_s_list" searchTid="' + data.rows[i].sub_data[j].type_id + '"><span>' + data.rows[i].text +
                    '</span><span class="category">' + data.rows[i].sub_data[j].text + '</span><span class="num">约' + data.rows[i].sub_data[j].count + "个结果</span></li>").show();
                for (var m = 0; m < $(".t_s_list").length; m++) {
                    var span1 = $(".t_s_list").eq(m).find("span:first-child").outerWidth(!0),
                        span2 = $(".t_s_list").eq(m).find(".category").outerWidth(!0),
                        span3 = $(".t_s_list").eq(m).find(".num").outerWidth(!0),
                        spanWidth = span1 + span2 + span3 + 16,
                        searchWidth = $(".t_s_list").eq(0).outerWidth(),
                        searchHeight = $(".t_s_list").eq(0).outerHeight();
                    if (spanWidth > searchWidth) {
                        var resetSpanW = searchWidth - span2 - span3 - 16 - 10;
                        $(".t_s_list").eq(m).find("span:first-child").css({
                            display: "block",
                            width: resetSpanW + "px",
                            height: searchHeight + "px",
                            "float": "left",
                            overflow: "hidden",
                            "text-overflow": "ellipsis",
                            "white-space": "nowrap"
                        }).end().find(".category").css({
                            display: "block",
                            "float": "left"
                        })
                    }
                }
            } else $(".hdAutocomplete").hide()
        }))
    }

    function selLi (num, _this) {
        $(".hdAutocomplete li").eq(num).addClass("hover").siblings().removeClass("hover");
        var _tt = $(".hdAutocomplete li:eq(" + num + ")").find("span").eq(0).text(),
            sid = $(".hdAutocomplete li:eq(" + num + ")").attr("searchtid");
        _this.parents(".hdsearchIpt").find(".hdsearchWords").val(_tt), _sid.val(sid)
    }

    G.hideMessage();
    var type_id = "",
        _url = $(".hdsearchWords").attr("dataurl"),
        _li = $(".hdAutocomplete li"),
        _sn = -1,
        _sid = $("#j-typeId");
    _li.on("click", function () {
        var _key = $(this).find("span").eq(0).text(),
            type_id = $(this).attr("searchTid");
        $("#J-seaType").find("input[name=type_id]").val(type_id), $(this).parents(".hdsearchIpt").find(".hdsearchWords").val(_key);
        var btn = $(this).parents(".hdsearchIpt").next(".hdsearchSubmit");
        btn && $('form[name="quick_find"]').submit()
    }), _li.on("hover", function () {$(this).addClass("hover").siblings().removeClass("hover"), _sn = $(this).index()});
    var searchList = $(".search_hot_list");
    $(".hdsearchWords").focus(function () {
        type_id = $(".hdsearch input[name=type_id]").val();
        var jset = $("#J-seaType"),
            sel = jset.find(".type").html(),
            all = jset.find('li[type="all"]').html(),
            travel = jset.find('li[type="Travel"]').html(),
            _this = (jset.find('li[type="Dingzhi"]').html(), jset.find('li[type="Activity"]').html(), $(this)),
            _val = $(this).val(),
            _url = $(this).attr("dataurl");
        return sel == all && "" == _val ? void searchList.show() : sel == travel && "" == _val ? void searchList.show() : (searchList.hide(), void sendData(_url, 10, _val, _this))
    }).keyup(function (e) {
        type_id = $(".hdsearch input[name=type_id]").val(), searchList.hide();
        var _this = $(this),
            _val = $(this).val(),
            _len = $(".hdAutocomplete li").length;
        switch (e.keyCode) {
            case 40:
                _sn == _len - 1 ? _sn = 0 : _sn += 1, selLi(_sn, _this);
                break;
            case 38:
                0 == _sn ? _sn = _len - 1 : _sn -= 1, selLi(_sn, _this);
                break;
            case 37:
                break;
            case 39:
                break;
            default:
                sendData(_url, 10, _val, _this)
        }
    }), $(document).click(function (event) {
        var e = event || window.event,
            obj = e.srcElement || e.target;
        "hdsearchWords" != obj.className && "t_s_list" !== obj.className && $(obj).find(".hdAutocomplete").hide(), "search_hot_list" != obj.className && "hdsearchWords" != obj.className && searchList.hide()
    }), searchList.click(function (event) {
        var e = event || window.event;
        e.stopPropagation()
    }), $(".close_hot").click(function () {searchList.hide()}), G.updateCurrency()
}), window._artDialog_path = "https://www.usitrip.com/tpl/www/js/artDialog", $(function () {"undefined" == typeof facePageMenu ? otherPageMenu() : facePageMenu()}), $(function () {
    function fnFooter () {
        $(".js-foot-tab").mouseover(function () {
            var index = parseInt($(this).attr("index")),
                hs = $(".m-auth-box-i").eq(index).position().top,
                hy = $(".m-auth-box-i").eq(index).height();
            $(".m-auth-box").css("height", hy), $(".m-auth-box-ov").stop(!0, !0).animate({top: -hs}, 200), $(".js-foot-tab").removeClass("active").eq(index).addClass("active")
        });
        var footSlide = $("#js-foot-slide"),
            btnLeft = footSlide.find(".m-coope-bnt-l"),
            btnRight = footSlide.find(".m-coope-bnt-r"),
            slideUl = footSlide.find(".m-coope-tab-list"),
            slideLi = slideUl.find("li"),
            len = slideLi.length,
            index = 0,
            w = slideLi.outerWidth(!0);
        len > 8 ? (btnLeft.click(function () {return 0 == index ? !1 : (index--, void slideUl.stop(!0, !1).animate({left: -w * index}))}), btnRight.click(function () {return index == len - 8 ? !1 : (index++, void slideUl.stop(!0, !1).animate({left: -w * index}))})) : (btnLeft.add(btnRight).
            hide(), slideUl.css("margin-left", (1152 - w * len) / 2))
    }

    if ($(".nav-down").length && $(".nav-down").mouseover(function () {
        $(this).each(function (index, dom) {
            var elemWidth = $(this).outerWidth(),
                text = $(this).find(".nav_a").text().replace(/^\s+|\s+$/g, "");
            "更多" == text && $(this).find(".nav-down-box").css({width: elemWidth - 2 + "px"}).addClass("more")
        })
    }), $(".m-sort-sdc-r-top").length > 0) {
        var navDl = $(".m-sort-sdc-r-top").find("dl");
        navDl.each(function (index, item) {
            var len = $(item).find("dd").length,
                w = $(this).width();
            len > 10 && $(item).css("width", w * Math.ceil(len / 10))
        })
    }
    $(".m-auth-wrap").length && fnFooter(), $.fn.qiniuImgCheck = function () {
        this.each(function (index, obj) {
            var tmp_src,
                src,
                data_src,
                that = $(obj);
            return src = that.attr("src"), data_src = that.attr("data-original"), "" !== src || void 0 != data_src && "" !== data_src ? void ((void 0 != src || void 0 != data_src) && (tmp_src = src, that.hasClass("lazy") && (tmp_src = data_src), tmp_src && -1 != tmp_src.indexOf("qiniu.") && $.ajax({
                url: tmp_src,
                success: function () {},
                error: function (data) {
                    (404 == data.status || "Not Found" == data.statusText) &&
                    (tmp_src = tmp_src.replace(/^((http:\/\/)|(https:\/\/))?([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}/, ""), that.attr("src") && that.attr("src", tmp_src), that.hasClass("lazy") && that.attr("data-original", tmp_src))
                }
            }))) : !0
        })
    }
}), Usitrip = {}, Usitrip.button = {
    show: function (opts) {
        var settings = {
            ele: ".puree-spinner-button",
            disable: !0,
            callback: null,
            callback2: null
        };
        settings = $.extend(!0, {}, settings, opts), navigator.userAgent.indexOf("MSIE") >= 0 && navigator.userAgent.indexOf("Opera") < 0 && $(settings.ele).addClass("puree-spinner-button-ie"), "function" == typeof settings.callback2 ? settings.callback2() : $(settings.ele).
            click(function (e) {".puree-spinner-button" == settings.ele && $(this).addClass("active").css("cursor", "no-drop").attr("disabled", settings.disable), "function" == typeof settings.callback && settings.callback.apply(this, [this, e])})
    },
    reset: function (opts) {
        var setting = {
            ele: ".puree-spinner-button",
            callback: null
        };
        setting = $.extend(!0, {}, setting, opts), $(setting.ele).hasClass("puree-spinner-button") && $(setting.ele).removeClass("active").attr("disabled", !1).css("cursor", "pointer"), "function" == typeof setting.callback && setting.callback.apply(this, [this])
    }
}, $(function () {Usitrip.button.show(), $(".tell24-img").hover(function () {$(this).siblings(".tell24-split-line").hide(), $(this).siblings(".tell24-other-phone").show()}, function () {$(this).siblings(".tell24-split-line").show(), $(this).siblings(".tell24-other-phone").hide()})}), function ($) {
    $(function () {$(".space-trim").on("blur", function () {Usitrip.String && $(this).val(Usitrip.String.trim(Usitrip.String.ToCDB($(this).val())))})})
}($);

$(function () {
    var isAnim = false;
    var timer;
    var delay = 0; // 修改这个让效果减速
    var start = 0;
    var logoA = $('.logo a');
    var end = parseInt(logoA.width());
    var chrome = navigator.userAgent;

    function anim () {
        if (start > end) {
            clearTimeout(timer);
            start = 0;
            isAnim = false;
        } else {
            logoA.css('webkitMaskImage', "-webkit-gradient(radial, 35 35, " + start + ", 35 35, " + (start + 10) + ", from(rgba(0, 0, 0, 1)),color-stop(0.5, rgba(0, 0, 0, 0.2)),to(rgba(0, 0, 0, 1)))")
            timer = setTimeout(anim, delay);
            start++;
        }
    }

    logoA.mouseover(function () {
        if (!isAnim && chrome.indexOf('Chrome') > -1) {//此特效仅在谷歌下显示
            isAnim = true;
            // 繁體不顯示
            if (localStorage.getItem('lang') != 2) {
                anim();
            }
        }
    })
});
