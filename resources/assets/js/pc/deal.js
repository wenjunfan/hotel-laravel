import AutoComplete from '../includes/destination-autocomplete';

var dealsApp = new Vue({
    el: '#dealsApp',
    components: {AutoComplete},
    data: {
        couponId: couponId,
        appUrl: appUrl,
        isEditing: false,
        editRooms: false,
        destination: {
            name: '',
            id: '',
            city: '',
            scenicName: '',
            scenicId: '',
        },
        searchData: {
            checkin: moment(checkin).format('YYYY-MM-DD'),
            checkout: moment(checkout).format('YYYY-MM-DD'),
            roomCount: 1,
            adultCount1: 2,
            childCount1: 0,
        },
        childAges: [],
    },
    mounted() {
        let self = this;
        this.destination.name = this.langStr === 'cn' ? '请输入目的地' : 'Where to go';

        let params = $('#search-form').serialize();
        $.post('/deal/price/' + this.couponId, {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'checkin': this.searchData.checkin,
            'checkout': this.searchData.checkout,
        }, function(data) {
            if (data.success) {
                if (data.hotels.length > 0) {
                    var hotels = data.hotels;
                    for (var i = 0; i < hotels.length; i++) {
                        var hotel = hotels[i];
                        var $hotelItem = $('#hotelItem' + hotel.id);
                        $hotelItem.find('.hotel-price').text(hotel.currency + hotel.beforeTax);
                        $hotelItem.find('.book-btn').attr('href', self.appUrl + '/hotel/' + hotel.id + '.html?s=' + hotel.sessionKey + '&' + params);
                        if (hotel.discount) {
                            var $label = $('#discountLabel' + hotel.id);
                            $label.html('<span>' + parseInt(hotel.discount * 100) + '%</span> off');
                            $label.show();
                        }
                    }
                }
            }
        });

        // Datepicker setup
        var $checkinInput = $('#checkin'),
            $checkoutInput = $('#checkout');

        $checkinInput.datepicker({
            startDate: '0d',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: 'yyyy-mm-dd',
            defaultDate: this.searchData.checkin,
        }).on('changeDate', function(selected) {
            var date = new Date(selected.date.valueOf());
            self.searchData.checkin = moment(date).format('YYYY-MM-DD');
            var minDate = moment(date).add(1, 'days');
            $checkoutInput.datepicker('setStartDate', minDate.toDate());
            self.searchData.checkout = minDate.format('YYYY-MM-DD');
        });

        $checkoutInput.datepicker({
            startDate: '1d',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: 'yyyy-mm-dd',
            defaultDate: this.searchData.checkout,
        }).on('changeDate', function(selected) {
            self.searchData.checkout = moment(selected.date.valueOf()).format('YYYY-MM-DD');
        });

        $('#dealsApp').on('click', function(e) {
            if (e.target !== $('.rooms-display') && $('.rooms-display').find(e.target).length === 0) {
                self.editRooms = false;
            }
        });

        $('.destination-toggler').on('click', function(e) {
            e.preventDefault();
            $('.destination-toggler').removeClass('active');
            $(this).addClass('active');
            $('html, body').animate({
                scrollTop: $($(this).attr('data-target')).offset().top,
            }, 300);

            return false;
        });

        $('.destination-input').on('click', function() {
            $(this).removeClass('error');
        });

        $('.coupon-details a').click(function () {
            $('#policyModal').modal('show');
            return false;
        });
    },
    methods: {
        updateDestination(newDestination) {
            this.destination.name = newDestination.name;
            this.destination.id = newDestination.id;
            this.destination.scenicName = newDestination.scenicName;
            this.destination.scenicId = newDestination.scenicId;
        },
        submitSearch() {
            if (this.destination.id && this.searchData.checkin && this.searchData.checkout && this.searchData.roomCount) {
                location.href = this.appUrl + '/list/' + this.destination.id + '.html?' + $('#search-form').serialize();
            } else {
                if (!this.destination.id) {
                    $('.destination-input').addClass('error');
                }
                swal('Please type in valid values');
            }
        },
        closePolicy() {
            $('#policyModal').modal('hide');
        },
        changeAge: function (val, index, $event) {
            this.childAges[index] = $event.target.value;
        }
    },
    watch: {
        'searchData.childCount1'(val) {
            let oldVal = this.childAges.length;
            if (val > oldVal) {
                for (let i = oldVal; i < val; i++) {
                    this.childAges.push(12);
                }
            } else {
                this.childAges.splice(val, oldVal - val);
            }
        }
    }
});