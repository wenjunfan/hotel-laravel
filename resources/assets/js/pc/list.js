var lock;
var dhxhr;
var txhr;
var sxhr;
var fxhr;
var fmxhr;

var isCh = page_lang === '0';
var maxCheckinText = !isCh ? 'Check-in Date should in two years from now' : '入住日期需在两年内';
var searchKey = ''; // to mark search auto complete user input, don't change
var scenics = [];
var selectedScenic = null;

// for tmp hotels loading
var hotelLength;
var hotelIndex = 0;
var tmpHotels;
var interval;

var checktemp;
var index = 1;

var processing = true;
var sessionKey; // recommend hotels sessionKey
var searchedHotel = null;
var isHotelSearch = false;

var isAllCheck = false;

var centerHotel = null; // to center map
var hotelMap = null;
var mapHotels = [];
var markers = [];
var highestZIndex = 99;
var originIcon = null;
var overIcon = null;
var selectedIcon = null;
var selectedMaker = null;
var searchedMapHotel = null;
var mapBounds = null;
var mapPadding = {
    top: 100,
    right: 100,
    bottom: 100,
    left: 100
};

window.resetAll = function () {
    resetKeyword();
    clearOption('all');
    clearChainNames();
    noFilterSelected();
    sessionStorage.clear();
};

window.loadMap = function () {
    var node_mapAnchor = $('#mapAnchor');
    originIcon = {
        url: 'https://s3-us-west-1.amazonaws.com/usitrip/map/infor_pc-01.png',
        labelOrigin: new google.maps.Point(40, 18)
    };
    overIcon = {
        url: 'https://s3-us-west-1.amazonaws.com/usitrip/map/infor_pc-02.png',
        labelOrigin: new google.maps.Point(40, 18)
    };
    selectedIcon = {
        url: 'https://s3-us-west-1.amazonaws.com/usitrip/map/infor_pc-03.png',
        labelOrigin: new google.maps.Point(45, 20)
    };
    mapBounds = new google.maps.LatLngBounds();

    // hide inforwindow and map by default
    $('body').prepend($('#mapWrapper'));
    var node_map = $('#mapBannerDesName');
    node_map.html(destinationName);
    $('<span>' + checkin + langList.to + checkout + '</span>').insertAfter(node_map);

    // show mini map
    var langCode = 'en';
    if(page_lang == 2){
        langCode = 'zh-TW';
    }else if(page_lang == 0){
        langCode = 'zh-CN';
    }
    var mapUrl = 'https://maps.googleapis.com/maps/api/staticmap?center='
      + destinationName
      + '&zoom=12&size=270x119&key=' + app_map_key + '&language=' +langCode;

    node_mapAnchor.html('<img class="map-anchor" src="' + mapUrl+'" height="119" width="270" alt="google map"/>');

    // show hotels map when click on mini map
    node_mapAnchor.click(function () {
        if (mapHotels.length > 0) {
            $('#mapWrapper').css('display', 'flex'); // to make sure map is not scrollable
            setTimeout(function () {
                hotelMap.fitBounds(mapBounds, mapPadding);
                clearMarkers();
                // load new hotels based on current bounds
                filterHotelsByBounds();
            }, 300);

            $('.wired-all-wraper-for-hotel').hide(); // hide whole block
            $('#wrapper').hide();
            $('#backtotop').hide();
            $('footer').hide();
        } else {
            $('#mapWrapper').hide();
            $('.wired-all-wraper-for-hotel').show();
            $('#wrapper').show();
            $('#backtotop').show();
            $('footer').show();
        }
    });

};

//show available hotels
window.renderHotels = function () {
    var l = $('.ladda-button-search').ladda();
    l.ladda('start');
    buildLeftFilterBanner(); // build rating and price filter
    preSelectFilters(); // start preselect filters

    if (destinationId.indexOf('D') > -1) {
        buildScenicArea(destinationId.replace('D', "")); // build scenic area if it's destination search
    }

    isLoading(); // show loading state

    if ($('#desId').val().indexOf('H') >= 0) {
        isHotelSearch = true;
        hxhr = $.get('/list/hotel', $('#search-form').serialize(), function (data) {
            if (data.hasOwnProperty('success') && data['success'] === false) {
                if (data.hasOwnProperty('type') && data['type'] === 'SESSION_TIMEOUT') {
                    swal({
                        title: lang_login_expired,
                        type: 'warning',
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: lang_login
                    }, function () {
                        window.location.href = '/';
                    });
                } else {
                    var l = $('.ladda-button-search').ladda();
                    l.ladda('stop');

                    swal({
                        title: lang_not_active + '\n ' + data.errorMessage,
                        type: 'warning',
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: lang_how_to_active
                    }, function () {
                        window.location.href = '/guide';
                    });
                }
            } else if (data.hotel) {
                var hotel = data.hotel;
                searchedHotel = hotel; // global hotel var to be removed from searchedRecommendHotels list

                if (hotel.desId) {
                    buildScenicArea(hotel.desId); // build scenic area with desId in the hotel data
                }
                var searchedHotelSessionKey = data.sessionKey;

                var hotelLink = '/hotel/' + hotel.hotelId + '.html?s=' + searchedHotelSessionKey + '&' + $('#search-form').serialize();

                var thumbnail;
                if (hotel.thumbnail == "") {
                    thumbnail = '/img/general/img_error.jpg';
                } else {
                    thumbnail = hotel.thumbnail;
                }

                var hotelName = "";
                if (page_lang == 0)
                    hotelName = groupName(hotel.name, hotel.name_zh);
                else
                    hotelName = groupName(hotel.name);

                if (hotel.currency == null) {
                    hotel.currency = '$';
                }
                var currency = hotel.currency == 'USD' ? '$' : hotel.currency;

                // <!--右侧搜索后列表-->
                var hotelStatusTemplate = '';
                var lowest_flag = hotel.lowestFlg == 0 ? no_lowest_html : lowest_html;
                if (!hotel.minPrice) {
                    hotelStatusTemplate = "" +
                      '<div>' +
                      '<p class="usitrip-hover-tag sold-out-text">' +
                      '<span style="font-size:18px;">' + lang_sout + '</span>' +
                      '</p>' +
                      '</div>';
                } else {
                    var originalPriceBlock = '';
                    if (hotel.discount > 0) {
                        originalPriceBlock = '<span class="usitrip-fade originalPriceLineThrough">' + currency + hotel.originalPrice + '</span>';
                    }

                    hotelStatusTemplate =
                      '<div class="rate-block">' +
                      '<p>' +
                      '<span style="font-size:14px;"></span>' +
                      '<span style="font-size:14px;">' + originalPriceBlock + '</span>' +
                      '<span class="usitrip-hover-tag">' +
                      '<span style="font-size:33px;" id="hotel' + hotel.hotelId + '-currency">' + currency + '</span>&nbsp;' +
                      '<span style="font-size:33px;" id="hotel' + hotel.hotelId + '-minPrice">' + hotel.minPrice + '</span>' +
                      '</span>' +
                      lowest_flag +
                      '<div id="roomButton' + hotel.hotelId + '">' +
                      '<a id="roomButton' + hotel.hotelId + '" style="color:#fff;" class="btn hotel-pick-btn" target="_blank" href="' + hotelLink + '" onclick="jumpHotel(' + hotel.hotelId + ', \'' + escape(hotel.name) + '\');">' +
                      langList.choose +
                      '</a>' +
                      '</div>' +
                      '</p>' +
                      '</div>';
                }

                var borderColor = '#bce8f1 !important;';
                var cc = '0 1px 3px 0 #bce8f1 !important;';
                var rateBlock = '';
                if (hotel.discount > 0) {
                    rateBlock = '<i>' + Math.round(hotel.discount * 100) + '% off</i>';
                }
                var destinationCity = hotel.city;
                var distance_value = (hotel.distance == null || hotel_city == search_city || search_city == dest_city_match_en || search_city == dest_city_match_cn) ? "" : ' | ' + langList.distance + '<span class="distance-if-long"> ' + search_city.charAt(0).toUpperCase() +
                  search_city.slice(1).toLowerCase() + '</span> ' + langList.is + hotel.distance.value + ' ' + hotel.distance.unit + '</span>';
                var rating = buildRating(hotel.rating, hotel.category);
                var guestRatingLine = '<p class="guest-rating-blank-space"></p>';
                if (hotel.guestRating) {
                    var guestRating = hotel.guestRating + '/5';
                    var guestReviewCount = hotel.guestReviewCount == null ? '0' : hotel.guestReviewCount;
                    guestRatingLine = '<p class="font-14 usitrip-fade">' + langList.guestRating + '&nbsp;<span class="guest-rating">&nbsp;' +
                      guestRating +
                      '&nbsp;</span>&nbsp;<span class="rating-count">(' +
                      guestReviewCount +
                      langList.review + ')</span></p>';
                }

                if (searchedHotel.latitude && searchedHotel.longitude && searchedHotel.minPrice > 0) {
                    searchedMapHotel = {
                        lat: hotel.latitude,
                        lng: hotel.longitude,
                        price: hotel.minPrice,
                        name: hotelName,
                        thumbnail: thumbnail,
                        guestRating: guestRating,
                        guestReviewCount: guestReviewCount,
                        lowest_flag: hotel.lowestFlg == 0,
                        rating: rating,
                        currency: currency,
                        beforeTax: hotel.discount > 0 ? hotel.originalPrice : null,
                        hotelLink: hotelLink,
                        sessionKey: searchedHotelSessionKey
                    };
                    centerHotel = searchedMapHotel;

                    if (hotelMap) {
                        loadMap();
                    }
                }

                var hotelHtml =
                  '<div class="hotel-item">' +
                  '<div class="list-border-product" style="-webkit-box-shadow:' + cc + ';  border-color: ' + borderColor + '">' +
                  '<div class="product-img-box">' +
                  '<div class="product-images">' +
                  rateBlock +
                  '<a target="_blank" href="' + hotelLink + '" onclick="jumpHotel(' + hotel.hotelId + ', \'' + escape(hotel.name) + '\');">' +
                  '<img class="list-hotel-img" src="' + thumbnail + '" onerror="this.src=\'/img/general/img_error.jpg\'" alt="' + hotel.name + '"/>' +
                  '</a>' +
                  '</div>' +
                  '</div>' +
                  '<div class="hotel-list-content">' +
                  '<p class="hotel-list-title" id="hotel' + hotel.hotelId + '-name">' +
                  '<a class="list-hotel-name" target="_blank" href="' + hotelLink + '" onclick="jumpHotel(' + hotel.hotelId + ', \'' + escape(hotel.name) + '\');">' +
                  hotelName +
                  '</a>' +
                  '</p>' +
                  '<p class="list-star">' +
                  rating +
                  '</p>' +
                  '<p class="font-14 list-address usitrip-fade"><span id="hotel' + hotel.hotelId + '-address">' + hotel.address.replace(', , ', ', ' + hotel.city + ', ') + '</span></p>' +
                  '<p class="font-14 usitrip-fade"><span><i class="fa fa-map-marker-alt" aria-hidden="true"></i>&nbsp;' + hotel.city.charAt(0).toUpperCase() + hotel.city.slice(1).toLowerCase() + '&nbsp;' + '</span><span id="hotel' + hotel.hotelId + '-distance">' + distance_value + '</span></p>' +
                  guestRatingLine +
                  hotelStatusTemplate +
                  '</div>' +
                  '</div>' +
                  '<div id="roomOption' + hotel.hotelId + '">' +
                  '</div>' +
                  '</div>';

                $('#searchedHotelBlock').html(hotelHtml);

                searchRecommendHotels();
            }
        });
    } else {
        isHotelSearch = false;
        searchRecommendHotels();
    }
};

// update left filter banner
window.buildLeftFilterBanner = function () {
    var html =
      '<div id="topFilterBlock" style="opacity: 0.4;pointer-events: none;">' +
      '<div>' +
      '<div class="form-group">' +
      '<p class="narrow-result">' + langList.narrowResult + '</p>' +
      '<div id="hasSomeFilterSelected">' +
      '<a id="resetAll" class="hidden btn btn-block resetAllBtn" onclick="resetAll()">' + langList.clearAll + ' &times;</a>' +
      '<div>' +
      '<div id="passedScenicName"></div>' +
      '<span class="filter-title">' +
      langList.list_filter_by_keywords +
      '</span>' +
      '<button type="button" id="keywordToggle" onclick="toggleFilter(\'null\', \'keyword\', \'null\')" class="pull-right fa fa-angle-up"><i></i></button>' +
      '<button class="pull-right hidden" id="keywordButton" onclick="resetKeyword()">' + langList.clear + '</button><br/><br/>' +
      '<div id="filterCollapsedOrNotDivkeyword">' +
      '<div class="row search-box-wrapper">' +
      '<input type="text" required class="search-box" value="' + key + '" id="hotelName" onkeyup="if(event.which == 13 || event.keyCode == 13){searchKeyword();}" placeholder="&#xF236;  ' + langList.egHilton + '"/>' +
      '<button type="button" class="list-hotel-search-go" onclick="searchKeyword()">GO</button>' +
      '</div>' +
      '</div>' +
      '<p id="filterCollapsedOrNotTextkeyword" class="filter-list"></p>' +
      '<div class="border-top">' +
      '<span class="filter-title">' +
      lang_rating +
      '</span><button type="button" id="rateToggle" onclick="toggleFilter(\'null\', \'rate\', \'null\')" class="pull-right fa fa-angle-up"><i></i></button>' +
      '<a class="pull-right hidden" id="ratingButton" onclick="clearOption(\'rating\')">' + lang_reset + '</a><br/> <br/> ' +
      '<div id="filterCollapsedOrNotDivrate" class="filter-list">' +
      '<div class="checkbox checkbox-inline option-box">' +
      '<input id="rating5" name="rating" class="filterCheck" value="5" type="checkbox" onchange="hotelFilter()">' +
      '<label for="rating5"><i class="usitrip_pc_all_img star-icon-red"></i><i class="usitrip_pc_all_img star-icon-red"></i><i class="usitrip_pc_all_img star-icon-red"></i><i class="usitrip_pc_all_img star-icon-red"></i><i class="usitrip_pc_all_img star-icon-red"></i> 5' + langList.stars +
      '</label>' +
      '</div>' +
      '<br/>' +
      '<div class="checkbox checkbox-inline option-box">' +
      '<input id="rating4" class="filterCheck" name="rating" value="4" type="checkbox" onchange="hotelFilter()">' +
      '<label for="rating4"><i class="usitrip_pc_all_img star-icon-red"></i><i class="usitrip_pc_all_img star-icon-red"></i><i class="usitrip_pc_all_img star-icon-red"></i><i class="usitrip_pc_all_img star-icon-red"></i> 4' + langList.stars + '</label>' +
      '</div>' +
      '<br/>' +
      '<div class="checkbox checkbox-inline option-box">' +
      '<input id="rating3" class="filterCheck" name="rating" value="3" type="checkbox" onchange="hotelFilter()">' +
      '<label for="rating3"><i class="usitrip_pc_all_img star-icon-red"></i><i class="usitrip_pc_all_img star-icon-red"></i><i class="usitrip_pc_all_img star-icon-red"></i> 3' + langList.stars + '</label>' +
      '</div>' +
      '<br/>' +
      '<div class="checkbox checkbox-inline option-box">' +
      '<input id="rating2" class="filterCheck" name="rating" value="2" type="checkbox" onchange="hotelFilter()">' +
      '<label for="rating2"><i class="usitrip_pc_all_img star-icon-red"></i><i class="usitrip_pc_all_img star-icon-red"></i> 2' + langList.stars + '</label>' +
      '</div>' +
      '<br/>' +
      '<div class="checkbox  checkbox-inline option-box">' +
      '<input id="rating1" class="filterCheck" name="rating" value="1" type="checkbox" onchange="hotelFilter()">' +
      '<label for="rating1"><i class="usitrip_pc_all_img star-icon-red"></i> 1' + langList.star + '</label>' +
      '</div><br/><br/>' +
      '</div>' +
      '<p id="filterCollapsedOrNotTextrate" class="filter-list"></p>' +
      '</div>' +
      '</div>' +
      '<div>' +
      '<div class="border-top">' +
      '<span class="filter-title">' + lang_PricePerDay + '</span>' +
      '<button type="button" id="priceToggle" onclick="toggleFilter(\'null\', \'price\', \'null\')" class="pull-right fa fa-angle-up"><i></i></button>' +
      '<a class="pull-right hidden" id="priceButton" onclick="clearOption(\'price\')">' + lang_reset + '</a><br/><br/>' +
      '<div id="filterCollapsedOrNotDivprice" class="filter-list">' +
      '<div class="checkbox checkbox-inline option-box">' +
      '<input id="price1" name="price" class="filterCheck" value="1" type="checkbox" onchange="hotelFilter()">' +
      '<label class="price1" for="price1">' + langList.price1 + '</label>' +
      '</div>' +
      '<br/>' +
      '<div class="checkbox checkbox-inline option-box">' +
      '<input id="price2" class="filterCheck" name="price" value="2" type="checkbox" onchange="hotelFilter()">' +
      '<label class="price2" for="price2">‌' + langList.price2 + '</label>' +
      '</div>' +
      '<br/>' +
      '<div class="checkbox checkbox-inline option-box">' +
      '<input id="price3" class="filterCheck" name="price" value="3" type="checkbox" onchange="hotelFilter()">' +
      '<label class="price3" for="price3">' + langList.price3 + '</label>' +
      '</div>' +
      '<br/>' +
      '<div class="checkbox checkbox-inline option-box">' +
      '<input id="price4" class="filterCheck" name="price" value="4" type="checkbox" onchange="hotelFilter()">' +
      '<label class="price4" for="price4">' + langList.price4 + '</label>' +
      '</div>' +
      '<br/>' +
      '<div class="checkbox checkbox-inline option-box">' +
      '<input id="price5" class="filterCheck" name="price" value="5" type="checkbox" onchange="hotelFilter()">' +
      '<label class="price5" for="price5">' + langList.price5 + '</label>' +
      '</div>' +
      '</div>' +
      '<p id="filterCollapsedOrNotTextprice" class="filter-list"></p>' +
      '</div>' +
      '</div>';

    $('#ratePrice').html(html);
    $('.why-us-block').css('opacity', 1);
};

window.preSelectFilters = function () {
    if (rating.length > 0 && rating.length <= 5) {
        $('#ratingButton').removeClass('hidden');
        for (var i = 0; i < rating.length; i++) {
            $('#rating' + (rating[i])).prop('checked', true);
        }
    }

    if (price.length > 0) {
        if (price.length === 2 && price[0] === 1 && price[1] === 5) { // if it's all price
            return;
        }
        $('#priceButton').removeClass('hidden');
        if (price.length === 1) {
            $('#price' + price[0]).prop('checked', true);
        } else {
            var min = price[0];
            var max = price[price.length - 1];
            for (var i = 1; i <= 5; i++) {
                if (i > min && i < max) {
                    $('#price' + i).prop('checked', true);
                    $('#price' + i).prop('disabled', true);
                } else if (i == min || i == max) {
                    $('#price' + i).prop('checked', true);
                }
            }
        }
    }

};

window.buildScenicArea = function (destinationId) {
    var districts = [],
      airports = [],
      zones = [],
      locations = [],
      universities = [];

    $.get('/list/scenic', {
        'destinationId': destinationId
    }, function (data) {
        scenics = data.data;

        if (scenicId) {
            selectedScenic = scenics.find(function (scenic) {
                return scenic.id == scenicId;
            });
        }

        for (var i = 0; i < scenics.length; i++) {
            var scenic = scenics[i];
            if (scenic.type == 'pnlDistrict') {
                districts.push(scenic);
            } else if (scenic.type == 'pnlAirport') {
                airports.push(scenic);
            } else if (scenic.type == 'pnlZone') {
                zones.push(scenic);
            } else if (scenic.type == 'pnlLocation') {
                locations.push(scenic);
            } else if (scenic.type == 'pnlUniversity') {
                universities.push(scenic);
            }
        }

        var scenicHtml = '';
        var districtsHtml = '',
          airportsHtml = '',
          zonesHtml = '',
          locationsHtml = '',
          universitiesHtml = '';

        // build airpots
        if (airports.length > 0) {
            airports.forEach(function (airport) {
                var airport_name = (language == 0) ? airport.name_full : airport.name;
                var airportcode;
                if (airport.name_short == null) {
                    airportcode = '';
                } else {
                    airportcode = airport.name_short + ', ';
                }

                // to display on top
                var name_full = airport.name + ', ' + airportcode + airport.destination_name;
                if (page_lang == 0) {
                    name_full = airport.name_full + ', ' + airportcode + airport.destination_name_zh;
                }
                airportsHtml += '<div class="checkbox">' +
                  '<input id="airport_' + airport.id + '" class="hotelFilterInput filterCheck" name="airport" value="' + airport.id + '" type="checkbox" disabled>' +
                  '<label for="airport_' + airport.id + '">' +
                  '<input type="hidden" class="airport_" id="' + airport.id + '" value="' + name_full + '">' + airport_name + '</label>' +
                  '</div>';
            });

            scenicHtml += '<div id="item1" class="scenic-list-wrapper">' +
              '<input type="checkbox" class="scenic-list-checkbox" style="position:absolute; display:none;" id="hidemorefiltere">' +
              '<div class="form-group scenic-list border-top">' +
              '<span class="filter-title">' + langList.Airport + '</span><button type="button" id="airportToggle" onclick="toggleFilter(\'null\', \'airport\', \'e\')" class="pull-right fa fa-angle-up"><i></i></button>' +
              '<a class="pull-right clearOptionButton hidden" type="button" id="airportButton">' + lang_reset + '</a><br/><br/>' +
              '<div id="filterCollapsedOrNotDivairport" class="filter-list">' +
              airportsHtml +
              '</div>' +
              '</div>' +
              '<p id="filterCollapsedOrNotTextairport" class="filter-list"></p>' +
              '<label for="hidemorefiltere" id="filtermoree" onclick="changeContent(\'e\')" class="filterLongToggle">' + langList.lessFilter + '</label><br/>' +
              '</div>';
        }

        // build districts
        if (districts.length > 0) {
            districts.forEach(function (district) {
                var district_name = (language == 0) ? district.name_full : district.name;
                var name_full = district.name_full + ', ' + district.destination_name_zh;
                if (page_lang == 0) {
                    name_full = district.name + ', ' + district.destination_name;
                }

                districtsHtml += '<div class="checkbox">' +
                  '<input id="district_' + district.id + '" class="hotelFilterInput filterCheck districtFilterCheck" name="district" value="' + district.id + '" type="checkbox" disabled>' +
                  '<label  for="district_' + district.id + '">' +
                  '<input type="hidden" class="district_" id="' + district.id + '" value="' + name_full + '">' + district_name + '</label>' +
                  '</div>';
            });

            scenicHtml +=
              '<div id="item2" class="scenic-list-wrapper">' +
              '<input type="checkbox" class="scenic-list-checkbox" style="position:absolute; display:none;" id="hidemorefiltera">' +
              '<div class="form-group scenic-list border-top">' +
              '<span class="filter-title">' + langList.Attractions + '</span><button type="button" id="districtToggle" onclick="toggleFilter(\'null\', \'district\', \'a\')" class="pull-right fa fa-angle-up"><i></i></button>' +
              '<a class="pull-right clearOptionButton hidden" type="button" id="districtButton">' + lang_reset + '</a><br/><br/>' +
              '<div id="filterCollapsedOrNotDivdistrict" class="filter-list">' +
              districtsHtml +
              '</div>' +
              '</div>' +
              '<p id="filterCollapsedOrNotTextdistrict" class="filter-list"></p>' +
              '<label for="hidemorefiltera" id="filtermorea" onclick="changeContent(\'a\')" class="filterLongToggle">' + langList.lessFilter + '</label><br/>' +
              '</div>';
        }

        // build zones
        if (zones.length > 0) {
            zones.forEach(function (zone) {
                var zone_name = (language == 0) ? zone.name_full : zone.name;
                var name_full = zone.name_full + ', ' + zone.destination_name_zh;
                if (page_lang == 0) {
                    name_full = zone.name + ', ' + zone.destination_name;
                }

                zonesHtml +=
                  '<div class="checkbox">' +
                  '<input id="zone_' + zone.id + '" class="hotelFilterInput filterCheck zoneFilterCheck" name="zone" value="' + zone.id + '" type="checkbox" disabled>' +
                  '<label for="zone_' + zone.id + '">' +
                  '<input type="hidden" class="zone_" id="' + zone.id + '" value="' + name_full + '">' + zone_name + '</label>' +
                  '</div>';
            });

            scenicHtml +=
              '<div id="item3" class="scenic-list-wrapper">' +
              '<input type="checkbox" class="scenic-list-checkbox" style="position:absolute; display:none;" id="hidemorefilterb">' +
              '<div class="form-group scenic-list border-top">' +
              '<span class="filter-title">' + langList.Zone + '</span><button type="button" id="zoneToggle" onclick="toggleFilter(\'null\', \'zone\', \'b\')" class="pull-right fa fa-angle-up"><i></i></button>' +
              '<a class="pull-right clearOptionButton hidden" type="button" id="zoneButton">' + lang_reset + '</a><br/><br/>' +
              '<div id="filterCollapsedOrNotDivzone" class="filter-list">' +
              zonesHtml +
              '</div>' +
              '</div>' +
              '<p id="filterCollapsedOrNotTextzone" class="filter-list"></p>' +
              '<label for="hidemorefilterb" id="filtermoreb" onclick="changeContent(\'b\')" class="filterLongToggle">' + langList.lessFilter + '</label>' +
              '</div>';
        }

        // build locations
        if (locations.length > 0) {
            locations.forEach(function (location) {
                var location_name = (language == 0) ? location.name_full : location.name;
                var name_full = location.name_full + ', ' + location.destination_name_zh;
                if (page_lang == 0) {
                    name_full = location.name + ', ' + location.destination_name;
                }

                locationsHtml +=
                  '<div class="checkbox">' +
                  '<input id="location_' + location.id + '" class="hotelFilterInput filterCheck" name="location" value="' + location.id + '" type="checkbox" disabled>' +
                  '<label for="location_' + location.id + '">' +
                  '<input type="hidden" class="location_" id="' + location.id + '" value="' + name_full + '">' + location_name + '</label>' +
                  '</div>';
            });

            scenicHtml +=
              '<div id="item4" class="scenic-list-wrapper">' +
              '<input type="checkbox" class="scenic-list-checkbox" style="position:absolute; display:none;" id="hidemorefilterc">' +
              '<div class="form-group scenic-list border-top">' +
              '<span class="filter-title">' + langList.District + '</span><button type="button" id="locationToggle" onclick="toggleFilter(\'null\', \'location\', \'c\')" class="pull-right fa fa-angle-up"><i></i></button>' +
              '<a class="pull-right clearOptionButton hidden" type="button" id="locationButton">' + lang_reset + '</a><br/><br/>' +
              '<div id="filterCollapsedOrNotDivlocation" class="filter-list">' +
              locationsHtml +
              '</div>' +
              '</div>' +
              '<p id="filterCollapsedOrNotTextlocation" class="filter-list"></p>' +
              '<label for="hidemorefilterc" id="filtermorec" onclick="changeContent(\'c\')" class="filterLongToggle">' + langList.lessFilter + '</label>' +
              '</div>';
        }

        // build universities
        if (universities.length > 0) {
            universities.forEach(function (university) {
                var university_name = (language == 0) ? university.name_full : university.name;
                var name_full = university.name_full + ', ' + university.destination_name_zh;
                if (page_lang == 0) {
                    name_full = university.name + ', ' + university.destination_name;
                }

                universitiesHtml +=
                  '<div class="checkbox">' +
                  '<input id="university_' + university.id + '" class="hotelFilterInput filterCheck" name="university" value="' + university.id + '" type="checkbox" disabled>' +
                  '<label for="university_' + university.id + '">' +
                  '<input type="hidden" class="university_" id="' + university.id + '" value="' + name_full + '">' + university_name + '</label>' +
                  '</div>';
            });

            scenicHtml +=
              '<div id="item5" class="scenic-list-wrapper">' +
              '<input type="checkbox" class="scenic-list-checkbox" style="position:absolute; display:none;" id="hidemorefilterf">' +
              '<div class="form-group scenic-list border-top">' +
              '<span class="filter-title">' + langList.University + '</span><button type="button" id="universityToggle" onclick="toggleFilter(\'null\', \'university\', \'f\')" class="pull-right fa fa-angle-up"><i></i></button>' +
              '<a class="pull-right clearOptionButton hidden" type="button" id="universityButton">' + lang_reset + '</a><br><br>' +
              '<div id="filterCollapsedOrNotDivuniversity" class="filter-list">' +
              universitiesHtml +
              '</div>' +
              '</div>' +
              '<p id="filterCollapsedOrNotTextuniversity" class="filter-list"></p>' +
              '<label for="hidemorefilterf" id="filtermoref" onclick="changeContent(\'f\')" class="filterLongToggle">' + langList.lessFilter + '</label>' +
              '</div>';

        }

        $('#scenicArea').html(scenicHtml);

        if (scenicAreaArr.length > 0) {
            $('#scenicArea .hotelFilterInput').each(function () {
                if (scenicAreaArr.indexOf(this.value) > -1) {
                    $(this).prop('checked', true);
                }
            });
            // show on top of filter if there is only one scenic passed
            if (scenicAreaArr.length === 1) {
                var selectedScenicId = scenicAreaArr[0];
                selectedScenic = scenics.find(function (scenic) {
                    return scenic.id == selectedScenicId;
                });

                var type = '';
                if (selectedScenic.type == 'pnlDistrict') {
                    type = 'district';
                } else if (selectedScenic.type == 'pnlAirport') {
                    type = 'airpot';
                } else if (selectedScenic.type == 'pnlZone') {
                    type = 'zone';
                } else if (selectedScenic.type == 'pnlLocation') {
                    type = 'location';
                } else if (selectedScenic.type == 'pnlUniversity') {
                    type = 'university';
                }

                var selectedScenicName = page_lang == 0 && selectedScenic.name_zh ? selectedScenic.name_zh + ' (' + selectedScenic.name + ')' : selectedScenic.name;
                var passedHtml = '<div id="remove' + selectedScenic.id + '" class="' + type + '">' +
                  '<label class="searchedSmallLocation">' + selectedScenicName + '</label>' +
                  '</div>';
                $('#passedScenicName').html(passedHtml);
                if (passedHtml != '' && $('#resetAll').hasClass('hidden')) {
                    // 分享链接有scenic的话显示reset all的btn
                    $('#resetAll').removeClass('hidden');
                }
            }
        }

        if (airports.length < 6) {
            $('#filtermoree').addClass('hidden');
        }
        if (districts.length < 6) {
            $('#filtermorea').addClass('hidden');
        }
        if (zones.length < 6) {
            $('#filtermoreb').addClass('hidden');
        }
        if (locations.length < 6) {
            $('#filtermorec').addClass('hidden');
        }
        if (universities.length < 6) {
            $('#filtermoref').addClass('hidden');
        }
        if (!sessionKey) $('#scenicArea').css('opacity', 0.4);
    });
};

// search tmp and recommend hotels
window.searchRecommendHotels = function () {
    $('#hotelLoadingPicture').remove();
    txhr = $.get('/list/static', $('#search-form').serialize(), function (data) {
        if (data.hasOwnProperty('success') && data['success'] == false) {
            if ('type' in data && data['type'] == 'SESSION_TIMEOUT') {
                swal({
                    title: lang_login_expired,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: lang_login
                }, function () {
                    window.location.href = '/';
                });
            }
        } else if (sxhr && sxhr.readyState != 4) {
            if (data.length != 0) {
                var l = $('.ladda-button-search').ladda();
                l.ladda('start');
                $('#hotel_starter').show();
                hotelIndex = 0;
                hotelLength = data.hotels ? data.hotels.length : 0;
                tmpHotels = data.hotels;
                interval = self.setInterval(insertHotel, 1000);
            }
        }

        var l = $('.ladda-button-search').ladda();
        l.ladda('stop');
    });

    sxhr = $.get('/list/recommend', $('#search-form').serialize(), function (data) {
        var l = $('.ladda-button-search').ladda();
        l.ladda('stop');
        window.clearInterval(interval);

        if (data.hasOwnProperty('success') && data['success'] == false) {
            if ('type' in data && data['type'] == 'SESSION_TIMEOUT') {
                swal({
                    title: lang_login_expired,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: lang_login
                }, function () {
                    window.location.href = '/';
                });
            } else {
                swal({
                    title: lang_not_active + '\n ' + data.errorMessage,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: lang_how_to_active
                }, function () {
                    window.location.href = '/guide';
                });
            }
        } else if (data.length != 0) {
            $('#hotel_starter').hide();
            if (data.hasOwnProperty('errorId')) {
                $('#recommendedHotelsBlock').html(lang_no_result);
            } else {
                // if hotel is not available and has recommended hotels when search a hotel, insert this comment box - jie 06/22/17
                if (isHotelSearch && data.totalHotels && data.totalHotels > 0) {
                    var recommendBoxText = "";
                    if (searchedHotel.minPrice) {
                        var l = $('.ladda-button-search').ladda();
                        l.ladda('stop');
                        recommendBoxText = lang_more_option;
                    } else {
                        recommendBoxText = ' ' + lang_see_others;
                    }

                    var searchedHotelNotes = '<div class="col-md-12 alert alert-dismissible alert-dismissible-tag" role="alert"> ' +
                      '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                      '<span aria-hidden="true">x</span>' +
                      '</button>' +
                      '<p>' + recommendBoxText + '</p>' +
                      '</div>';

                    $(searchedHotelNotes).insertAfter('#searchedHotelBlock');
                }

                sessionKey = data.sessionKey; // set global recommend hotel sessionKey

                $('#withRecommendHotels').removeClass('hidden');

                // buildSortBanner(); // update sort banner with sessionKey
                $('#hotelSort').show();

                $('#topFilterBlock').removeAttr('style'); // enable rating and price filter
                updateScenicArea(); // update scenic fileds with sessionKey

                if (scenicId.length > 0) { // if has passed scenicId
                    scenicAreaArr.push(scenicId);
                    $('#scenicArea input[value="' + scenicId + '"]').prop('checked', true); //todo
                    var passedHtml;
                    var passedType = '';
                    // detect passed ScenicType
                    if ($('#' + scenicId).hasClass('district_')) {
                        passedType = 'district';
                    } else if ($('#' + scenicId).hasClass('airport_')) {
                        passedType = 'airport';
                    } else if ($('#' + scenicId).hasClass('zone_')) {
                        passedType = 'zone';
                    } else if ($('#' + scenicId).hasClass('location_')) {
                        passedType = 'location';
                    } else if ($('#' + scenicId).hasClass('university_')) {
                        passedType = 'university';
                    }
                    // show reset btn for this passed type
                    $('#' + passedType + 'Button').removeClass('hidden');
                    passedHtml = '<div id="remove' + scenicId + '">' +
                      '<label class="searchedSmallLocation">' + scenicName + '</label>' +
                      '</div>';
                    $('#passedScenicName').append(passedHtml);

                    hotelFilter();
                    hasFilterSelected();
                }

                desUniqId = $('#desId').val() + 'U' + $('#scenicId').val();
                scenicarray = [];
                if ((scenicarray != '' && scenicarray != null && scenicarray.indexOf(desUniqId) > -1)) {
                    scenicarrayafter = scenicarray.replace(desUniqId, '');
                    scenicarray = JSON.parse(scenicarrayafter);
                }

                if ($('input.districtFilterCheck:checked').length > 0) {
                    $('#filtermorea').click();
                }
                if ($('input.zoneFilterCheck:checked').length > 0) {
                    $('#filtermoreb').click();
                }
                hotelFilter(false); // do not go to page
                sort('discount', sortMethod);
            }
        } else {
            $('#recommendedHotelsBlock').html(lang_no_result);
        }
    });
};

// update scenic area fields with sessionKey
window.updateScenicArea = function () {
    $('#scenicArea').css('opacity', 1);
    $('#scenicArea .hotelFilterInput').attr('disabled', false);
    $('#scenicArea .hotelFilterInput').change(function () {
        hotelFilter();
        // any scenic checked, append to left top selected section
        var ScenicSelected = this.value;
        var selectedScenicname = this.name;
        if ($('#' + selectedScenicname + '_' + ScenicSelected).checked) {
            $('#remove' + ScenicSelected).remove();
        } else {
            var selectedScenicLabel = $('#' + ScenicSelected + '').val();
            $('#' + selectedScenicname + '_' + ScenicSelected + ':checked').each(function () {
                selectedScenicHtml = '<div id="remove' + ScenicSelected + '">' +
                  '<div class="checkbox checkbox-red-x">' +
                  '<input class="usitrip-hover-tag" onchange="removeFromSelectedOptions(' + ScenicSelected + ')" type="checkbox" checked>' +
                  '<label class="usitrip-hover-tag"> ' +
                  selectedScenicLabel +
                  '</label>' +
                  '</div>' +
                  '</p>';
            });
        }
    });

    $('#scenicArea .clearOptionButton').click(function () {
        var type = $(this).attr('id').replace('Button', '');
        clearOption(type);
    });
};

window.sort = function (newOrder, x) {
    if (fxhr) {
        fxhr.abort();
    }

    if (orderBy != newOrder) { // check whether it's the same filter method and give it the default sort
        orderBy = newOrder;
    }

    if (newOrder != 'discount') {
        sortMethod = x;
    } else {
        sortMethod = sortMethod == 'desc' ? 'asc' : 'desc'; // toggle order
        if (newOrder == 'discount') {
            sortMethod = 'asc';
        }
    }

    $('#icon-name').html(arrowIcon('name'));
    $('#icon-minPrice').html(arrowIcon('minPrice'));
    $('#icon-rating').html(arrowIcon('rating'));

    $('#button-name').attr('class', buttonStyle('name'));
    $('#button-minPrice').attr('class', buttonStyle('minPrice'));
    $('#button-rating').attr('class', buttonStyle('rating'));
    $('#button-discount').attr('class', buttonStyle('discount'));
    $('#button-distance').attr('class', buttonStyle('distance'));

    goPage(page, 'sort');
};

window.hotelFilter = function (shouldGo) {
    if (shouldGo === undefined) {
        shouldGo = true;
    }

    if (fxhr && fxhr.readyState !== 4) {
        fxhr.abort();
    }

    clearResultBlock();

    scenicAreaArr = [];
    rating = [];
    price = [];

    // gets selected scenic fields
    $('#scenicArea input[type="checkbox"].hotelFilterInput:checked').each(function () {
        var selectedScenicHtml = '';
        scenicAreaArr.push(this.value);
    });

    // update value for each section selected
    $('#airportCheckedLength').val($('#scenicArea input[name="airport"].hotelFilterInput:checked').length);
    $('#locationCheckedLength').val($('#scenicArea input[name="location"].hotelFilterInput:checked').length);
    $('#zoneCheckedLength').val($('#scenicArea input[name="zone"].hotelFilterInput:checked').length);
    $('#districtCheckedLength').val($('#scenicArea input[name="district"].hotelFilterInput:checked').length);
    $('#universityCheckedLength').val($('#scenicArea input[name="university"].hotelFilterInput:checked').length);

    //add filter reset btn for each scenic
    if ($('input[name="airport"]:checked').length > 0) {
        $('#airportButton').removeClass('hidden');
    } else if (!$('#airportButton').hasClass('hidden')) {
        $('#airportButton').addClass('hidden');
    }
    if ($('input[name="zone"]:checked').length > 0) {
        $('#zoneButton').removeClass('hidden');
    } else if (!$('#zoneButton').hasClass('hidden')) {
        $('#zoneButton').addClass('hidden');
    }
    if ($('input[name="location"]:checked').length > 0) {
        $('#locationButton').removeClass('hidden');
    } else if (!$('#locationButton').hasClass('hidden')) {
        $('#locationButton').addClass('hidden');
    }
    if ($('input[name="district"]:checked').length > 0) {
        $('#districtButton').removeClass('hidden');
    } else if (!$('#districtButton').hasClass('hidden')) {
        $('#districtButton').addClass('hidden');
    }
    if ($('input[name="university"]:checked').length > 0) {
        $('#universityButton').removeClass('hidden');
    } else if (!$('#universityButton').hasClass('hidden')) {
        $('#universityButton').addClass('hidden');
    }

    if (scenicAreaArr.length === 1) {
        var selectedScenicId = scenicAreaArr[0];
        selectedScenic = scenics.find(function (scenic) {
            return scenic.id == selectedScenicId;
        });

        // this class name by scenic type is used to clear scenic when clearOption passed a specific scenic type
        var type = '';
        if (selectedScenic.type == 'pnlDistrict') {
            type = 'district';
        } else if (selectedScenic.type == 'pnlAirport') {
            type = 'airpot';
        } else if (selectedScenic.type == 'pnlZone') {
            type = 'zone';
        } else if (selectedScenic.type == 'pnlLocation') {
            type = 'location';
        } else if (selectedScenic.type == 'pnlUniversity') {
            type = 'university';
        }

        var selectedScenicName = page_lang == 0 && selectedScenic.name_zh ? selectedScenic.name_zh + ' (' + selectedScenic.name + ')' : selectedScenic.name;
        var passedHtml = '<div id="remove' + selectedScenic.id + '" class="' + type + '">' +
          '<label class="searchedSmallLocation">' + selectedScenicName + '</label>' +
          '</div>';
        $('#passedScenicName').html(passedHtml);
    } else {
        selectedScenic = null;
        $('#passedScenicName').html('');
    }

    if (scenicAreaArr != null && scenicAreaArr != [] && scenicAreaArr != '') {
        var scenicNameShow = '';
        if ($('#scenicArea input[value="' + scenicId + '"]').prop('checked')) {
            scenicNameShow = scenicId;
        } else {
            scenicNameShow = scenicAreaArr[0];
        }
        finalScenicNameShow = $('#' + scenicNameShow + '').val();
    }

    // check whether use unchecked passed scenicid
    var passedScenicChecked = $('#scenicArea input[value="' + scenicId + '"]').prop('checked');
    if (!passedScenicChecked) {
        $('#scenicId').val('');
    }

    var anyScenicChecked = $('input.filterCheck:checked').length > 0;
    var anyChainChecked = $('#chainCheckedLength').val() > 0;

    //price
    $('input[name="price"]:checked').each(function () {
        price.push(this.value);
        hasFilterSelected();
    });

    if (price.length > 0) {
        $('#priceButton').removeClass('hidden');
    } else {
        $('#priceButton').addClass('hidden');
    }

    price.sort(function (a, b) {
        return a - b;
    });

    if (price.length > 1) {
        var priceRange = [];
        var min = price[0];
        var max = price[price.length - 1];
        for (var i = 1; i <= 5; i++) {
            if (i > min && i < max) { // select those in the middle and enable those at the begin and end
                $('#price' + i).prop('checked', true);
                $('#price' + i).prop('disabled', true);
            } else if (i == min || i == max) {
                $('#price' + i).prop('disabled', false);
            }
        }
        priceRange.push(min);
        priceRange.push(max);
        price = priceRange;
    }

    //rating
    $('input[name="rating"]:checked').each(function () {
        rating.push(this.value);
        hasFilterSelected();
    });

    if (rating.length > 0) {
        $('#ratingButton').removeClass('hidden');
    } else {
        $('#ratingButton').addClass('hidden');
    }

    // if none of these filters checked
    if (rating.length === 0 && price.length === 0 && !anyScenicChecked && !passedScenicChecked && !anyChainChecked) {
        noFilterSelected();
        $('#scenicName').val('');
        $('#scenicId').val('');
        $('#scenicId').val('');
    } else {
        hasFilterSelected();
    }

    if (shouldGo) {
        if ($('#button-name').hasClass('btn-primary') || $('#button-discount').hasClass('btn-primary') ||
          $('#button-minPrice').hasClass('btn-primary') || $('#button-rating').hasClass('btn-primary')) {
            goPage(1, 'hotelfilter');
        } else {
            sortMethod = 'asc';
            goPage(1, 'hotelfilter');
        }
    }

    $('#icon-name').html(arrowIcon('name'));
    $('#icon-minPrice').html(arrowIcon('minPrice'));
    $('#icon-rating').html(arrowIcon('rating'));

    $('#button-name').attr('class', buttonStyle('name'));
    $('#button-minPrice').attr('class', buttonStyle('minPrice'));
    $('#button-rating').attr('class', buttonStyle('rating'));
    $('#button-discount').attr('class', buttonStyle('discount'));

    var scenicarray = scenicAreaArr;
    uniquePassedScenicId = $('#scenicId').val();
    uniquedesId = $('#desId').val();
    sessionStorage.setItem('scenicarray', uniquedesId + 'U' + uniquePassedScenicId + JSON.stringify(scenicarray));
};

window.goPage = function (page, type) {
    window.scrollTo(0, 0);
    processing = true;
    $('#noMoreHotels').hide();
    var key = document.getElementById('hotelName').value.trim();

    isLoading();

    $('#hotelPage').html('');
    $('#searchPage').html('');

    window.clearInterval(interval);
    if (txhr && txhr.readyState !== 4) {
        txhr.abort();
    }

    if (sessionKey) {
        // update url with new params
        var params = {
            'desCity': destinationCity,
            'orderBy': orderBy,
            'sort': sortMethod,
            'key': key,
            'pagePos': page,
            'rating': rating,
            'price': price,
            'scenicArea': scenicAreaArr,
            'chainNames': chainNames,
            'discount': showAllDiscount
        };

        var query = encodeParams(params);
        var url = location.pathname + '?' + query + '&' + $('#search-form').serialize();

        history.pushState({url: url, title: document.title}, document.title, url);

        var filters = {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'orderBy': orderBy,
            'sort': sortMethod,
            'key': key,
            'pagePos': page,
            'rating': rating,
            'price': price,
            'roomCount': roomCount,
            'dayCount': dayCount,
            'checkin': checkin,
            'checkout': checkout,
            'scenicArea': scenicAreaArr,
            'desId': $('#desId').val().trim(),
            'chainNames': chainNames,
            'discount': showAllDiscount
        };

        fxhr = $.post('/list/filter/' + sessionKey, filters, function (data) {
            if (!data.success) {
                swal({
                    title: lang_login_expired,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: lang_login
                }, function () {
                    location.reload();
                });
            } else if (data.success) {
                if (data.hotelChains) {
                    $('#chainBlock').show();
                    buildHotelChains(data.hotelChains);
                } else {
                    $('#chainBlock').hide();
                }

                if (data.hotels && data.hotels.length != 0) {
                    $('#totalHotels').html(data.totalHotels);

                    if (isHotelSearch && searchedHotel) {
                        for (var i = 0; i < data.hotels.length; i++) {
                            if (searchedHotel.hotelId == data.hotels[i].hotelId) {
                                data.hotels.splice(i, 1);
                            }
                        }
                    }

                    buildHotels(data.hotels);
                    buildPages(Number(data.totalPages), Number(data.currentPage), Number(data.totalHotels));

                    $('#recommendedHotelsBlock').attr('style', "");
                } else if (data.hotels && data.hotels.length == 0) {
                    if (!isHotelSearch) {
                        $('#totalHotels').html(data.totalHotels);
                        if (data.totalHotels == 0) {
                            $('#recommendedHotelsBlock').html('<p id="noResult">' + lang_no_result + '</p>').attr('style', 'text-align:left; font-size:14px; font-weight:400; border:1px solid #dce0e0; margin-top:14px; margin-left:10px; margin-right:10px; padding:25px 20px 25px 25px;');
                        }
                    } else {
                        if (data.totalHotels == 0) {
                            $('#recommendedHotelsBlock').html(lang_no_result).attr('style', 'text-align:center; font-size:14px; font-weight:400; margin-top:14px; margin-left:10px; margin-right:10px; padding:25px 20px 25px 25px;');
                        }
                    }
                } else {
                    $('#totalHotels').html(data.totalHotels);
                    $('#recommendedHotelsBlock').html(lang_no_result).attr('style', 'text-align:left; font-size:14px; font-weight:400; border:1px solid #dce0e0; margin-top:14px; margin-left:10px; margin-right:10px; padding:25px 20px 25px 25px;'); //Ann noResult Style

                }
            } else {
                $('#totalHotels').html(data.totalHotels);
                $('#recommendedHotelsBlock').html(lang_no_result).attr('style', 'text-align:left; font-size:14px; font-weight:400; border:1px solid #dce0e0; margin-top:14px; margin-left:10px; margin-right:10px; padding:25px 20px 25px 25px;'); //Ann noResult Style
            }
        });
        var anyScenicChecked = $('input.filterCheck:checked').length > 0;
        var keywordval = $('#hotelName').val();
        var anykeyword = keywordval != '';

        if (anykeyword) {
            $('#keywordButton').removeClass('hidden'); //detect if keyword when reload
        }
        if (anyScenicChecked || anykeyword) { //detect if any filter except chian when reload
            hasFilterSelected();
        }
    } else {
        $('#totalHotels').html(0);
        $('#recommendedHotelsBlock').html(lang_no_result).attr('style', 'text-align:left; font-size:14px; font-weight:400; border:1px solid #dce0e0; margin-top:14px; margin-left:10px; margin-right:10px; padding:25px 20px 25px 25px;'); //Ann noResult Style
    }
};

window.encodeParams = function (data) {
    var query = [];
    for (var d in data) {
        if (typeof data[d] === 'number' || data[d].length > 0) {
            query.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
        }
    }
    return query.join('&');
};

window.clearResultBlock = function () {
    currentPage = 1;
    $('#noMoreHotels').hide();
    $('#recommendedHotelsBlock').html('');
    window.scrollTo(0, 0);
};

window.selectText = function (element) {
    $('.dest_wrapper .dest_result').hide();
    $('.dest_wrapper').show();
    $('.dest_wrapper .dft_dest').show();
};

window.updateCheckin = function () {
    var checkin = $('#checkin').val();
    var today = new Date();
    var today = today.getFullYear() + '-' + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + '-' + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    var checkin1 = checkin.replace(/-/g, '');
    var today1 = today.replace(/-/g, '');
    if (checkin1 < today1) {
        $('#checkin').datepicker('setDate', today);
    }
    var checkin = $('#checkin').val();
    var checkout = $('#checkout').val();
    checkin1 = checkin.replace(/-/g, '');
    checkout1 = checkout.replace(/-/g, '');

    checkin = checkin.split('-');
    var checkin = new Date(checkin[0], (checkin[1] - 1), checkin[2]);
    var n = 1;
    var checkout = new Date(checkin - 0 + n * 86400000);
    var checkout = checkout.getFullYear() + '-' + (checkout.getMonth() + 1 > 9 ? checkout.getMonth() + 1 : '0' + (checkout.getMonth() + 1)) + '-' + (checkout.getDate() < 10 ? '0' + checkout.getDate() : checkout.getDate());
    $('#checkout').datepicker('setDate', checkout);

    $('#checkout').datepicker('setStartDate', checkin);

    var a1 = parseInt(checkin1);
    var a2 = parseInt(checktemp);

    if (a1 == a2 && index != 1 && index != 2) {
        $('#checkout').datepicker('show');
    }
    checktemp = checkin1;
    $('#checkout').datepicker('show');
    index++;
};

window.updateCheckout = function () {
    var checkout = $('#checkout').val();
    var checkin = $('#checkin').val();
    var today = new Date();
    var today = today.getFullYear() + '-' + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + '-' + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    var checkout1 = checkout.replace(/-/g, '');
    var today1 = today.replace(/-/g, '');
    var checkin1 = checkin.replace(/-/g, '');
    if (checkout1 <= checkin1) {
        var dcheckin = new Date(checkin);
        var tomorrow = new Date(dcheckin.getTime() + 2 * (24 * 60 * 60 * 1000));
        var tomorrow1 = tomorrow.getFullYear() + '-' + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + '-' + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());
        $('#checkout').datepicker('setDate', tomorrow1);
    }
    var checkin = $('#checkin').val();
    var checkout = $('#checkout').val();
    var today = new Date();
    var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));

    today1 = today.getFullYear() + "" + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + "" + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    tomorrow1 = tomorrow.getFullYear() + "" + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + "" + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());
    checkin1 = checkin.replace(/-/g, '');
    checkout1 = checkout.replace(/-/g, '');

    if (parseInt(checkin1) >= parseInt(checkout1)) {
        if (parseInt(checkout1) == parseInt(today1)) {
            swal({
                title: '退房日期不能早于明天',
                type: 'warning'
            }, function () {
                setTimeout(function () {$('#checkin').focus();}, 1);
            });

            td = today.getFullYear() + '-' + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + '-' + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
            tm = tomorrow.getFullYear() + '-' + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + '-' + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());

            $('#checkin').datepicker('setDate', td);
            $('#checkout').datepicker('setDate', tm);
        }
    }
};

//show available hotels
window.showResultPage = function () {
    $('#enMarginTop').hide();
    $('#change-search-toggled').removeClass('change-search-toggled');
    $('#list-search-info-toggle').show();

    if (searchKey.length > 0) {
        $.get('/keyword/mark/' + encodeURIComponent(searchKey), function (data) {
        });
    }

    $('#leftFiterWrapper, #searchResultWrapper').css({
        'opacity': 0.5,
        'pointer-events': 'none'
    });

    if (txhr && txhr.readyState != 4) {
        txhr.abort();
    }
    if (sxhr && sxhr.readyState != 4) {
        sxhr.abort();
    }

    $('#checkout').datepicker('hide');
    var checkin = $('#checkin').val(),
      checkout = $('#checkout').val(),
      destinationId = $('#desId').val(),
      days = calculateDays(checkin, checkout);

    if (checkin === "") {
        swal({
            title: '请先选择入住日期',
            type: 'warning'
        }, function () {
            setTimeout(function () {$('#checkin').focus();}, 1);
        });
        unblurBlocks();
        return false;
    }

    if (checkout === "") {
        swal({
            title: '请先选择退房日期',
            type: 'warning'
        }, function () {
            setTimeout(function () {$('#checkout').focus();}, 1);
        });
        unblurBlocks();
        return false;
    }

    /* a quick date format validation */
    var dateformat = /^(\d{4})-(\d{2})-(\d{2})$/;
    if (!checkin.match(dateformat) || !checkout.match(dateformat)) {
        swal({
            title: lang_format,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'ok'
        });
        unblurBlocks();
        return false;
    } else {
        //checkin date validation
        var parts = checkin.split('-');

        var year = parts[0];
        var month = parts[1];
        var day = parts[2];

        var $day = (day.charAt(0) == '0') ? day.charAt(1) : day;
        var $month = (month.charAt(0) == '0') ? month.charAt(1) : month;

        //checkout date validation
        var parts1 = checkout.split('-');

        var year1 = parts1[0];
        var month1 = parts1[1];
        var day1 = parts1[2];

        var $day1 = (day1.charAt(0) == '0') ? day1.charAt(1) : day1;
        var $month1 = (month1.charAt(0) == '0') ? month1.charAt(1) : month1;

        var now = new Date();
        var currentYear = now.getFullYear();

        if ($day > 31 || $day < 1 || $month > 12 || $month < 1 || year < currentYear) {
            swal({
                title: lang_checkin_valid,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'ok'
            });
            unblurBlocks();
            return false;
        }
        if ($day1 > 31 || $day1 < 1 || $month1 > 12 || $month1 < 1 || year1 < currentYear) {
            swal({
                title: lang_checkout_valid,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'ok'
            });
            unblurBlocks();
            return false;
        }
    }
    var newDate = new Date();
    var maxdm = newDate.toISOString().substr(4, 6);
    var maxCheckin = (newDate.getFullYear() + 2) + maxdm;

    if (checkin > maxCheckin) {
        swal({
            title: maxCheckinText,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'ok'
        });
        // TODO: 大于两年，换回可用的日期
        // return false;
    }

    if (days > 30) {
        swal({
            title: lang_limit,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'ok'
        });
        // 大于30天，换回可用的日期
        if (localStorage.getItem('checkout')) {
            $('#checkout').datepicker('setDate', localStorage.getItem('checkout'));
        }
        unblurBlocks();
        return false;
    }

    if (roomCount === '10' && destinationId.length !== "") {
        $('#desId').val(0);
        swal({
            title: lang_group_alert,
            type: 'warning'
        }, function () {
            setTimeout(function () {$('.roomCount').focus();}, 1);
        });
        unblurBlocks();
        return false;
    }

    if (destinationId === "") {
        $('#desId').val(0);
    }

    if (destinationId === 0) {
        swal({
            title: lang_no_destination_inp,
            type: 'warning'
        }, function () {
            setTimeout(function () {$('#desName').focus();}, 1);
        });
        unblurBlocks();
        return false;
    }

    var i_des = $('#desName').val();
    var i_tip = $('#desName').attr('tip');
    if (i_des != i_tip) {
        $('#desName').val(i_tip);
    }

    // clear filters
    $('#hotelName').val('');
    $('#leftFiterWrapper input:checkbox').removeAttr('checked');

    $('#search-form').attr('action', '/list/' + destinationId + '.html');
    $('#search-form').submit();
};

window.calculateDays = function (checkin, checkout) {
    var date1 = new Date(checkin);
    var date2 = new Date(checkout);
    var date3 = date2.getTime() - date1.getTime();
    var days = Math.floor(date3 / (24 * 3600 * 1000));
    return days;
};

//insert tmp hotel box
window.insertHotel = function () {
    var html = '';
    if (hotelIndex >= hotelLength) {
        hotelIndex = 0;
    }

    var thumbnail;
    var field = tmpHotels[hotelIndex];

    if (field.thumbnail == "") {
        thumbnail = '/img/general/img_error.jpg';
    } else {
        thumbnail = field.thumbnail;
    }

    var hotelName = page_lang == 0 ? groupName(field.name, field.name_zh) : groupName(field.name);

    if (field.currency == null) {
        field.currency = '$';
    }
    var currency = field.currency == 'USD' ? '$' : field.currency;

    // <!--右侧搜索中列表-->
    var str = field.address;
    if (field.city && field.city != '') {
        str += ', ' + field.city;
    }
    if (field.zipcode && field.zipcode != '') {
        str += ', ' + field.zipcode;
    }

    var hotel_city = field.city == null ? "" : field.city.charAt(0).toUpperCase() + field.city.slice(1).toLowerCase();
    html = '<div class="hotel-item">' +
      '<div class="list-border-product border-nopadding1">' +
      '<div class="product-img-box">' +
      '<div class="product-images">' +
      '<div><img  class="list-hotel-img" src="' + thumbnail + '" onerror="this.src=\'/img/general/img_error.jpg\'" alt="' + hotelName + '"/></div>' +
      '</div>' +
      '</div>' +
      '<div class="hotel-list-content">' +
      '<h2 style="font-weight:400;font-size:20px;" class="m-b-xs" id="hotel' + field.hotelId + '-name">' + hotelName + '</h2>' +
      '<p class="list-star">' +
      buildRating(field.rating, field.category) +
      '</p>' +
      '<p class="font-14 list-address usitrip-fade"><span id="hotel' + field.hotelId + '-address">' + str + '</span></p>' +
      '<p><span class="font-14 usitrip-fade"><i class="fa fa-map-marker-alt" aria-hidden="true"></i>&nbsp; ' + hotel_city + '&nbsp;</span></p>' +
      '<div><span></span><p style="float:right; font-weight:400; font-size:16px; color:#ed5565; padding-top:45px;  padding-right:25px;">' + langList.fetching + '</p></div>' +
      '</div>' +
      '</div>' +
      '</div>';
    hotelIndex++;

    $(html).insertAfter('#firstTmpHotel').hide().slideDown();
};

window.hasFilterSelected = function () {
    $('#resetAll').removeClass('hidden');
    $('#ifAnyFilterSelected').removeClass('hidden');

    $('#passedScenicName').addClass('ifScenicFilterNotEmpty');
};

window.noFilterSelected = function () {
    $('#passedScenicName').html('');
    $('#resetAll').addClass('hidden');
    $('#ifAnyFilterSelected').addClass('hidden');
    $('#passedScenicName').removeClass('ifScenicFilterNotEmpty');
};

window.removeFromSelectedOptions = function (id) {
    var type = '';
    // detect passed ScenicType
    if ($('#' + id).hasClass('district_')) {
        type = 'district_';
    } else if ($('#' + id).hasClass('airport_')) {
        type = 'airport_';
    } else if ($('#' + id).hasClass('zone_')) {
        type = 'zone_';
    } else if ($('#' + id).hasClass('location_')) {
        type = 'location_';
    } else if ($('#' + id).hasClass('university_')) {
        type = 'university_';
    } else {
        type = 'chain_';
    }
    $('#remove' + id).remove();
    $('#' + type + id).click();
    hotelFilter();
};

//搜索中动图
window.isLoading = function () {
    var html =
      '<div id="hotelLoadingPicture" style="margin-top:30px;" class="sk-spinner sk-spinner-three-bounce"> ' +
      '<div class="sk-bounce1"></div>' +
      '<div class="sk-bounce2"></div>' +
      '<div class="sk-bounce3"></div>' +
      '</div>';
    $('#recommendedHotelsBlock').html(html);
};

//搜索结果hotels
window.buildHotels = function (hotels) {
    if (typeof google !== 'undefined') {
        clearMarkers(); // clear markers from last page
    }

    $('#hotelLoadingPicture').remove();

    var html = "";

    $.each(hotels, function (i, field) {
        // hotel link to open for hotel details

        var hotelLink = '/hotel/' + field.hotelId + '.html?s=' + sessionKey + '&' + $('#search-form').serialize();

        // build thumbnail link
        var thumbnail;
        if (field.thumbnail == "") {
            thumbnail = '/img/general/img_error.jpg';
        } else {
            thumbnail = field.thumbnail;
        }

        // build hotel name
        var hotelName = page_lang == 0 ? groupName(field.name, field.name_zh) : groupName(field.name);

        // decide currency
        if (field.currency == null) {
            field.currency = '$';
        }
        var currency = field.currency == 'USD' ? '$' : field.currency;

        // show distance from search city for selected scenic
        var search_city = destinationCity ? destinationCity : cityName;//搜索名称
        var dest_city_match_en = field.destinationName.split(',', 1)[0];
        var dest_city_match_cn = field.destinationName_zh.split(',', 1)[0];
        var hotel_city = field.city == null ? "" : field.city.charAt(0).toUpperCase() + field.city.slice(1).toLowerCase();

        if (selectedScenic) {
            var selectedScenicName = page_lang == 0 && selectedScenic.name_zh ? selectedScenic.name_zh : selectedScenic.name;
            if (orderBy == 'distance' && field.scenicDistance) {
                var distance = field.scenicDistance;

                var distance_value = '<span> | ' + langList.distance + '<span class="distance-if-long">&nbsp;' + selectedScenicName + '</span> ' + langList.is + distance + ' km</span></span>';
            } else if (selectedScenic.northeast_lat && selectedScenic.northeast_lng && field.latitude && field.longitude) {
                var distance = 0;
                if (selectedScenic.northeast_lat && selectedScenic.northeast_lng && field.latitude && field.longitude) {
                    distance = getDistance(selectedScenic.northeast_lat - 0.03, selectedScenic.northeast_lng - 0.03, field.latitude, field.longitude);
                }

                var distance_value = '<span> | ' + langList.distance + '<span class="distance-if-long">&nbsp;' + selectedScenicName + '</span> ' + langList.is + distance + ' km</span></span>';
            }
        } else if (field.distance && !(hotel_city == search_city || search_city == dest_city_match_en || search_city == dest_city_match_cn)) {
            var distance_value = ' | ' + langList.distance + ' <span class="distance-if-long"> ' + search_city.charAt(0).toUpperCase() + search_city.slice(1).toLowerCase() + '</span> ' + langList.is + field.distance.value + ' ' + field.distance.unit + '</span>';
        } else {
            var distance_value = "";
        }

        // build guest rating, guest review and lowest flag

        var lowest_flag = field.lowestFlg == 0 ? no_lowest_html : lowest_html;
        var rating = buildRating(field.rating, field.category);
        var guestRatingLine = '<p class="guest-rating-blank-space"></p>';
        if (field.guestRating) {
            var guestRating = field.guestRating + '/5';
            var guestReviewCount = field.guestReviewCount == null ? '0' : field.guestReviewCount;
            guestRatingLine = '<p class="font-14 usitrip-fade">' + langList.guestRating + '&nbsp;<span class="guest-rating">&nbsp;' +
              guestRating +
              '&nbsp;</span>&nbsp;<span class="rating-count">(' +
              guestReviewCount +
              langList.review + ')</span></p>';
        }
        // build hotel click button
        var hotelStatusTemplate = '';
        if (!field.minPrice || field.minPrice < 0) {
            hotelStatusTemplate =
              '<div>' +
              '<p class="sold-out-text usitrip-hover-tag">' +
              '<span style="font-size:18px;">' + lang_sout + '</span>' +
              '</p>' +
              '</div>';
        } else {
            var originalPriceBlock = '';
            if (field.discount > 0) {
                originalPriceBlock = '<span class="usitrip-fade originalPriceLineThrough">' + currency + field.originalPrice + '</span>';
            }
            hotelStatusTemplate =
              '<div class="rate-block">' +
              '<span style="font-size:14px;">' + originalPriceBlock + '</span>' +
              '<span class="usitrip-hover-tag"> ' +
              '<span style="font-size:33px;" id="hotel' + field.hotelId + '-currency">' + currency + '</span>' +
              '<span style="font-size:33px;" id="hotel' + field.hotelId + '-minPrice">' + field.minPrice + '</span>' +
              '</span>' +
              lowest_flag +
              '<div id="roomButton' + field.hotelId + '">' +
              '<a type="button" style="color:#fff;" class="btn hotel-pick-btn" target="_blank" href="' + hotelLink + '" onclick="jumpHotel(' + field.hotelId + ', \'' + escape(field.name) + '\');">' +
              langList.choose +
              '</a>' +
              '</div>' +
              '</div>';
        }

        // push hotel to mapHotels to show on map
        if (field.latitude && field.longitude && field.minPrice > 0) {
            var mapHotel = {
                lat: field.latitude,
                lng: field.longitude,
                price: field.minPrice,
                name: hotelName,
                thumbnail: thumbnail,
                guestRating: guestRating,
                guestReviewCount: guestReviewCount,
                lowest_flag: field.lowestFlg == 0,
                rating: rating,
                currency: currency,
                beforeTax: field.discount > 0 ? field.originalPrice : null,
                hotelLink: hotelLink
            };

            mapHotels.push(mapHotel);

            if (!centerHotel) {
                centerHotel = mapHotel;

                if (hotelMap) {
                    loadMap();
                }
            }
        }

        var rateBlock = '';
        if (field.discount > 0) {
            rateBlock = '<i><span class="f_22 f_w_800">' +
              Math.round(field.discount * 100) + '% </span><span class="f_11">off<span></i>';
        }

        html += '<div class="hotel-item">' +
          '<div class="list-border-product">' +
          '<div class="product-img-box">' +
          '<div class="product-images">' +
          rateBlock +
          '<a target="_blank" href="' + hotelLink + '" onclick="jumpHotel(' + field.hotelId + ', \'' + escape(field.name) + '\');" class="product-img-anchor">' +
          '<img class="list-hotel-img" src="' + thumbnail + '" onerror="this.src=\'/img/general/img_error.jpg\'" alt="' + field.name + '"/>' +
          '</a>' +
          '</div>' +
          '</div>' +
          '<div class="hotel-list-content">' +
          '<p class="hotel-list-title" id="hotel' + field.hotelId + '-name">' +
          '<a class="list-hotel-name" target="_blank" href="' + hotelLink + '" onclick="jumpHotel(' + field.hotelId + ', \'' + escape(field.name) + '\');">' +
          hotelName +
          '</a>' +
          '</p>' +
          '<p class="list-star">' +
          rating +
          '</p>' +
          '<p class="font-14 list-address usitrip-fade"><span id="hotel' + field.hotelId + '-address">' + field.address.replace(', , ', ', ' + field.city + ', ') + '</span></p>' +
          '<p class="font-14 usitrip-fade"><span><i class="fa fa-map-marker-alt" aria-hidden="true"></i>&nbsp;' + hotel_city + '&nbsp;' + '</span><span id="hotel' + field.hotelId + '-distance">' + distance_value + '</span></p>' +
          guestRatingLine +
          hotelStatusTemplate +
          '</div>' +
          '</div>' +
          '<div id="roomOption' + field.hotelId + '">' +
          '</div>' +
          '</div>';
    });

    $('#recommendedHotelsBlock').append(html);

    if (typeof google !== 'undefined') {
        addMarkers();
    }

    // add subscribe banner and insert into hotel list
    if (!isHotelSearch) {
        if ($('#recommendedHotelsBlock .hotel-item').length > 6) {
            $('#listCouponBox').insertAfter('#recommendedHotelsBlock .hotel-item:nth-child(6)');
            $('#affiliateBox').insertAfter('#recommendedHotelsBlock .hotel-item:nth-child(3)');
        } else {
            $('#listCouponBox').insertAfter('#recommendedHotelsBlock .hotel-item:nth-child(1)');
            $('#affiliateBox').insertAfter('#recommendedHotelsBlock .hotel-item:nth-child(1)');
        }
        $('#listCouponBox').show();
        $('#affiliateBox').show();
    } else {
        if ($('#recommendedHotelsBlock .hotel-item').length > 6) {
            $('#listCouponBox').insertAfter('#recommendedHotelsBlock .hotel-item:nth-child(6)');
            $('#affiliateBox').insertAfter('#recommendedHotelsBlock .hotel-item:nth-child(3)');
        } else if ($('#recommendedHotelsBlock .hotel-item').length < 2) {
            $('#listCouponBox').insertAfter('#searchedHotelBlock');
            $('#affiliateBox').insertAfter('#searchedHotelBlock');
        } else {
            $('#listCouponBox').insertAfter('#recommendedHotelsBlock .hotel-item:nth-child(4)');
            $('#affiliateBox').insertAfter('#recommendedHotelsBlock .hotel-item:nth-child(2)');
        }
        $('#listCouponBox').show();
        $('#affiliateBox').show();
    }

    processing = false;
};

window.getDistance = function (lat1, lon1, lat2, lon2) {
    var radlat1 = Math.PI * lat1 / 180;
    var radlat2 = Math.PI * lat2 / 180;
    var theta = lon1 - lon2;
    var radtheta = Math.PI * theta / 180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;

    return (dist * 1.609344).toFixed(2);
};

//show page button
window.buildPages = function (totalPages, currentPage, totalHotels) {
    var tPages = totalPages;
    var cPage = currentPage;
    var tHotels = totalHotels;
    var html = "";
    var text = "";
    if (tPages > 1 && cPage > 1) {
        html += '<button class="page-button page-text-focus" onclick="goPage(' + (cPage - 1) + ')">' + lang_previous + '</button>';
    } else {
        html += '<button class="page-button page-text">' + lang_previous + '</button>';
    }

    var from = 1;
    var to = Math.min(from + 4, tPages);

    if (tPages > 6 && cPage > 2) {
        from = Math.min(tPages - 4, cPage - 2);
        to = from + 4;
    }

    if (from > 1) {
        html += '<button type="button" class="btn list-page-btn-normal page-button" onclick="goPage(' + (cPage - 1) + ')">...</button>';
    }

    for (var i = from; i <= to; i++) {
        if (i == cPage) {
            html += '<button type="button" class="btn list-page-btn-focus page-button" onclick="goPage(' + i + ')">' + i + '</button>';
        }
        else {
            html += '<button type="button" class="btn list-page-btn-normal page-button" onclick="goPage(' + i + ')">' + i + '</button>';
        }
    }

    if (to < tPages) {
        html += '<button type="button" class="btn list-page-btn-normal page-button" onclick="goPage(' + (to + 1) + ')">...</button>';
    }

    if (tPages > 1 && cPage < tPages) {
        html += '<button class="page-button page-text-focus" onclick="goPage(' + (cPage + 1) + ')">' + lang_next + '</button>';
        text += langList.at + '<span class="page-name-if-long">' + destinationName + '</span> ' + langList.finds +
          tHotels +
          langList.propertiesFound + langList.showing +
          ((cPage - 1) * 20 + 1) +
          '-' +
          ((cPage - 1) * 20 + 20) +
          langList.result;
    }
    else {
        html += '<button class="page-text">' + lang_next + '</button>';
        text += langList.at + '<span class="page-name-if-long">' + destinationName + '</span> ' + langList.finds +
          tHotels +
          langList.propertiesFound + langList.showing +
          ((cPage - 1) * 20 + 1) +
          '-' +
          tHotels +
          langList.result;
    }

    $('#searchPage').html(text);
    $('#hotelPage').html(html);
};

window.jumpHotel = function (hotelId, hotelName) {
    var anaImData = {
        'id': hotelId,
        'name': unescape(hotelName),
        'position': 1,
        'list': 'Search Results'
    };
    if (isHotelApp) {
        // addImpression(anaImData);
    }
    var anaData = {
        'id': hotelId,
        'name': unescape(hotelName),
        'position': 1,
        'list': destinationCity,
        'page': 'pc list'
    };
    onProductClick(anaData);
    // onProductView(anaData);
};

window.buttonStyle = function (name) {
    return orderBy == name ? 'btn list-btn-focus' : 'btn list-btn-clear ';
};

//pass if a-z or low to high for text besides sort banner btn
window.arrowIcon = function (name) {
    if (name == 'name') {
        return orderBy == name ? (sortMethod == 'desc' ? langList.nameDesc : langList.nameAsc) : '';
    } else {
        return orderBy == name ? (sortMethod == 'desc' ? langList.nameDesc : langList.nameAsc) : '';
    }
};

window.buildRating = function (rating, category) {
    var r = Number(rating);
    var h = '';
    if (r >= 5) {
        r = 5;
    }
    if (r != 1 && r != 2 && r != 3 && r != 4 && r != 5 && r != 6) {
        h = '<span class="usitrip-hover-tag">' + langList.noRate + '</span>';
        return h;
    }
    for (var i = 0; i < r; i++) {
        h += '<span class="usitrip_pc_all_img star-icon-red"></span>';
    }

    return h;
};

window.searchKeyword = function () { //todo
    lock = 1;
    var key = document.getElementById('hotelName').value.trim();
    if (key.length > 0) {
        $.get('/keyword/mark/' + encodeURIComponent(key) + '/filter', function () {
            var keywordHtml;
            keywordHtml = '<div id="keywordAppend" class="selectedFilters"><div class="checkbox checkbox-red-x">' +
              '<input type="checkbox" onchange="resetKeyword()" checked>' +
              '<label>' + key + '</label>' +
              '</div></div>';
            $('#keywordButton').removeClass('hidden');
        });
    }
    hotelFilter();
    var keywordtag = document.getElementById('hotelName').value;
    if ($('#resetAll').hasClass('hidden') && keywordtag != '') {
        hasFilterSelected();
    }
};

window.resetKeyword = function () {
    lock = 2;
    $('#hotelName').val("");
    $('#keywordAppend').remove();
    $('#keywordButton').addClass('hidden');
    document.getElementById('hotelName').value = "";
    hotelFilter();

    if ($('input.filterCheck:checked').length > 0 || $('#hotelName').val() != '' || $('#chainCheckedLength').val() > 0) {
        hasFilterSelected();
    } else {
        noFilterSelected();
    }

    if ($('#keywordToggle').hasClass('fa-angle-down')) {$('#keywordToggle').click();}
};

window.clearOption = function (choice) {
    if (fxhr && fxhr.readyState !== 4) {
        fxhr.abort();
    }

    $('#' + choice + 'Button').addClass('hidden');

    if (choice == 'rating') {
        rating = [];

        $('input[name="rating"]').removeAttr('checked').prop('disabled', false);
        if ($('#ratingButton').hasClass('hidden')) {} else {$('#ratingButton').addClass('hidden');}
        if ($('#rateToggle').hasClass('fa-angle-down')) {$('#rateToggle').click();}
    } else if (choice == 'price') {
        price = [];

        $('input[name="price"]').removeAttr('checked').prop('disabled', false);
        if ($('#priceButton').hasClass('hidden')) {} else {$('#priceButton').addClass('hidden');}
        if ($('#priceToggle').hasClass('fa-angle-down')) {$('#priceToggle').click();}
    } else if (choice == 'airport') {
        $('#passedScenicName .airport').remove();
        var airports = [];

        $('input[name="airport"]:checked').each(function () {
            airports.push(this.value);
            // $('#airport_' + this.value).remove();
        });
        var myArray = scenicAreaArr;
        var toRemove = airports;
        scenicAreaArr = $.grep(myArray, function (value) {
            return $.inArray(value, toRemove) < 0;
        });

        $('input[name="airport"]').removeAttr('checked').prop('disabled', false);
        var scenicarray = scenicAreaArr;
        uniquePassedScenicId = $('#scenicId').val();
        uniquedesId = $('#desId').val();
        sessionStorage.setItem('scenicarray', uniquedesId + 'U' + uniquePassedScenicId + JSON.stringify(scenicarray));
        if ($('#airportButton').hasClass('hidden')) {} else {$('#airportButton').addClass('hidden');}
        if ($('#airportToggle').hasClass('fa-angle-down')) {$('#airportToggle').click();}
    } else if (choice == 'location') {
        $('#passedScenicName .location').remove();
        var locations = [];

        $('input[name="location"]:checked').each(function () {
            // pass all checked value to location section as array and remove them after with grep func
            locations.push(this.value);
            // for each checked value of location section remove from selected section on left top
            // $('#location_' + this.value).remove();
        });
        var myArray = scenicAreaArr;
        var toRemove = locations;
        scenicAreaArr = $.grep(myArray, function (value) {
            return $.inArray(value, toRemove) < 0;
        });
        $('input[name="location"]').removeAttr('checked').prop('disabled', false);
        var scenicarray = scenicAreaArr;
        uniquePassedScenicId = $('#scenicId').val();
        uniquedesId = $('#desId').val();
        sessionStorage.setItem('scenicarray', uniquedesId + 'U' + uniquePassedScenicId + JSON.stringify(scenicarray));
        if ($('#locationButton').hasClass('hidden')) {} else {$('#locationButton').addClass('hidden');}
        if ($('#locationToggle').hasClass('fa-angle-down')) {$('#locationToggle').click();}
    } else if (choice == 'university') {
        $('#passedScenicName .university').remove();
        var universities = [];

        $('input[name="university"]:checked').each(function () {
            // pass all checked value to university section as array and remove them after with grep func
            universities.push(this.value);
        });
        var myArray = scenicAreaArr;
        var toRemove = universities;
        scenicAreaArr = $.grep(myArray, function (value) {
            return $.inArray(value, toRemove) < 0;
        });
        $('input[name="university"]').removeAttr('checked').prop('disabled', false);
        var scenicarray = scenicAreaArr;
        uniquePassedScenicId = $('#scenicId').val();
        uniquedesId = $('#desId').val();
        sessionStorage.setItem('scenicarray', uniquedesId + 'U' + uniquePassedScenicId + JSON.stringify(scenicarray));
        if ($('#universityButton').hasClass('hidden')) {} else {$('#universityButton').addClass('hidden');}
        if ($('#universityToggle').hasClass('fa-angle-down')) {$('#universityToggle').click();}
    } else if (choice == 'district') {
        $('#passedScenicName .district').remove();
        var districts = [];

        $('input[name="district"]:checked').each(function () {
            districts.push(this.value);
            // $('#district_' + this.value).remove();
        });
        var myArray = scenicAreaArr;
        var toRemove = districts;
        scenicAreaArr = $.grep(myArray, function (value) {
            return $.inArray(value, toRemove) < 0;
        });
        $('input[name="district"]').removeAttr('checked').prop('disabled', false);
        var scenicarray = scenicAreaArr;
        uniquePassedScenicId = $('#scenicId').val();
        uniquedesId = $('#desId').val();
        sessionStorage.setItem('scenicarray', uniquedesId + 'U' + uniquePassedScenicId + JSON.stringify(scenicarray));
        if ($('#districtButton').hasClass('hidden')) {} else {$('#districtButton').addClass('hidden');}
        if ($('#districtToggle').hasClass('fa-angle-down')) {$('#districtToggle').click();}
    } else if (choice == 'zone') {
        $('#passedScenicName .zone').remove();
        var zones = [];

        $('input[name="zone"]:checked').each(function () {
            zones.push(this.value);
            // $('#zone_' + this.value).remove();
        });
        var myArray = scenicAreaArr;
        var toRemove = zones;
        scenicAreaArr = $.grep(myArray, function (value) {
            return $.inArray(value, toRemove) < 0;
        });
        $('input[name="zone"]').removeAttr('checked').prop('disabled', false);
        var scenicarray = scenicAreaArr;
        uniquePassedScenicId = $('#scenicId').val();
        uniquedesId = $('#desId').val();
        sessionStorage.setItem('scenicarray', uniquedesId + 'U' + uniquePassedScenicId + JSON.stringify(scenicarray));
        if ($('#zoneButton').hasClass('hidden')) {} else {$('#zoneButton').addClass('hidden');}
        if ($('#zoneToggle').hasClass('fa-angle-down')) {$('#zoneToggle').click();}
    } else if (choice == 'all') {
        rating = [];
        price = [];
        scenicAreaArr = [];

        $('input.filterCheck:checked').removeAttr('checked').prop('disabled', false);
        $('#passedScenicName').html('');
        $('#scenicName').val('');
        $('#scenicId').val('');
        if ($('#zoneButton').hasClass('hidden')) {} else {$('#zoneButton').addClass('hidden');}

        if ($('#districtButton').hasClass('hidden')) {} else {$('#districtButton').addClass('hidden');}

        if ($('#locationButton').hasClass('hidden')) {} else {$('#locationButton').addClass('hidden');}

        if ($('#universityButton').hasClass('hidden')) {} else {$('#universityButton').addClass('hidden');}

        if ($('#priceButton').hasClass('hidden')) {} else {$('#priceButton').addClass('hidden');}

        if ($('#airportButton').hasClass('hidden')) {} else {$('#airportButton').addClass('hidden');}

        if ($('#ratingButton').hasClass('hidden')) {} else {$('#ratingButton').addClass('hidden');}

        if ($('#zoneToggle').hasClass('fa-angle-down')) {$('#zoneToggle').click();}

        if ($('#districtToggle').hasClass('fa-angle-down')) {$('#districtToggle').click();}

        if ($('#locationToggle').hasClass('fa-angle-down')) {$('#locationToggle').click();}

        if ($('#universityToggle').hasClass('fa-angle-down')) {$('#universityToggle').click();}

        if ($('#airportToggle').hasClass('fa-angle-down')) {$('#airportToggle').click();}

        if ($('#priceToggle').hasClass('fa-angle-down')) {$('#priceToggle').click();}

        if ($('#rateToggle').hasClass('fa-angle-down')) {$('#rateToggle').click();}

    }

    if ($('input.filterCheck:checked').length > 0 || $('#hotelName').val() != '' || $('#chainCheckedLength').val() > 0) {
        hasFilterSelected();
    } else {
        noFilterSelected();
    }

    if (sessionKey != null) {
        goPage(1, 'clearoption');
    }

    var passedScenicChecked = $('#scenicArea input[value="' + scenicId + '"]').prop('checked');
    if (!passedScenicChecked) {
        $('#scenicId').val('');
    }

    var anyScenicChecked = $('input.filterCheck:checked').length > 0;
    var anyChainChecked = $('#chainCheckedLength').val() > 0;
    if (!anyScenicChecked && !passedScenicChecked && !anyChainChecked) {
        $('#passedScenicName').html('');
        $('#scenicName').val('');
        $('#scenicId').val('');
    }

    if (scenicAreaArr != null && scenicAreaArr != [] && scenicAreaArr != '') {
        var scenicNameShow = '';
        if (passedScenicChecked) {
            scenicNameShow = scenicId;
        } else {
            scenicNameShow = scenicAreaArr[0];
        }
    }

    // update value for each section selected
    $('#airportCheckedLength').val($('#scenicArea input[name="airport"].hotelFilterInput:checked').length);
    $('#locationCheckedLength').val($('#scenicArea input[name="location"].hotelFilterInput:checked').length);
    $('#universityCheckedLength').val($('#scenicArea input[name="university"].hotelFilterInput:checked').length);
    $('#zoneCheckedLength').val($('#scenicArea input[name="zone"].hotelFilterInput:checked').length);
    $('#districtCheckedLength').val($('#scenicArea input[name="district"].hotelFilterInput:checked').length);
};

window.groupName = function (eng, chs) {
    if (chs == '' || chs == null) {
        return eng;
    } else {
        return eng + ' (' + chs + ')';
    }
};

window.unblurBlocks = function () {
    $('#leftFiterWrapper, #searchResultWrapper').css({
        'opacity': 1,
        'pointer-events': ''
    });
};

// filter collapse and show more toggle content change
window.changeContent = function (id) {
    if (id) {
        var id = 'filtermore' + id;
        var content = document.getElementById(id);
        if (content.innerHTML.match(langList.moreFilter)) {
            content.innerHTML = langList.lessFilter;
        } else {
            content.innerHTML = langList.moreFilter;
        }
    }
};

window.getCookie = function (c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + '=');
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(';', c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
};

// build left side chain filter block
window.buildHotelChains = function (chains) {
    var chainsHtml = '';
    chains.forEach(function (chain, index) {
        var chainName = isCh && chain.name_zh ? chain.name_zh : chain.name;
        var checkboxInput = '<input type="checkbox" id="chain_' + index + '" value="' + chainName + '" onchange="filterByChainNames(this)">';
        var labelText = isCh && chain.name_zh ? chain.name_zh + ' (' + chain.name + ')' : chain.name;
        if (chainNames.indexOf(chainName) > -1) {
            checkboxInput = '<input type="checkbox" id="chain_' + index + '" value="' + chainName + '" class="filterCheck" onchange="filterByChainNames(this)" checked>';
        }

        chainsHtml += '' +
          '<div class="checkbox">' +
          checkboxInput +
          '<label class="chain-label" for="chain_' + index + '">' +
          labelText +
          '<span class="pull-right"> (' + chain.count + ')</span></label>' +
          '</div>';
    });

    // only show chian block if exist

    if (chainsHtml == '') {
        var chainBlockHtml = '';
    } else {
        var chainBlockHtml =
          '<input type="checkbox" class="scenic-list-checkbox" style="position:absolute; display:none;" id="hidemorefilterd">' +
          '<div class="form-group scenic-list border-top">' +
          '<span class="filter-title">' + langList.Chain + '</span><button type="button" id="chainToggle" onclick="toggleFilter(' + chainNames.length + ', \'chain\', \'d\')" class="pull-right fa fa-angle-up"><i></i></button>' +
          '<a class="pull-right clearOptionButton hidden" type="button" id="chainButton" onclick="clearChainNames()">' + lang_reset + '</a><br><br>' +
          '<div id="filterCollapsedOrNotDivchain" class="filter-list">' +
          chainsHtml +
          '</div>' +
          '<p id="filterCollapsedOrNotTextchain" class="filter-list"></p>' +
          '</div>' +
          '<label for="hidemorefilterd" id="filtermored" onclick="changeContent(\'d\')" class="filterLongToggle">' + langList.lessFilter + '</label>';

        $('#chainBlock').html(chainBlockHtml);
    }
    //update reset btn for chain
    if (chainNames.length > 0) {
        $('#chainButton').removeClass('hidden');
    } else {
        $('#chainButton').addClass('hidden');
    }
    // if chain length lower than 7, not show show more toggle
    if (chains.length < 7) {
        $('#filtermored').addClass('hidden');
    }
    if ($('input.filterCheck:checked').length > 0 && $('#resetAll').hasClass('hidden')) { //detect if chian selected when reload
        hasFilterSelected();
    }
};

// toggle filter collapse and show how many selected or non selected
window.toggleFilter = function (length, type, no) {

    if (type == 'chain') {
        length = length;
    } else if (type == 'keyword') {
        if ($('#hotelName').val() != '') {
            length = $('#hotelName').val();
        } else {
            length = 0;
        }
    } else if (type == 'rate') {
        length = $('input[name="rating"]:checked').length;
    } else if (type == 'price') {
        length = $('input[name="price"]:checked').length;
    } else if (type == 'university') {
        length = $('input[name="university"]:checked').length;
    } else {
        length = $('#' + type + 'CheckedLength').val();
    }

    var type = type.toString();
    if ($('#' + type + 'Toggle').hasClass('fa-angle-up')) {
        $('#' + type + 'Toggle').removeClass('fa-angle-up');
        $('#' + type + 'Toggle').addClass('fa-angle-down');
        $('#filterCollapsedOrNotDiv' + type).hide();
        if (no != null) {
            $('#filtermore' + no).hide();
        }
        if (type === 'zone') {
            $('#filterCollapsedOrNotTextzone').css('padding-bottom', '20px');
        }
        if (type === 'keyword') {
            $('#filterCollapsedOrNotTextkeyword').css('margin-top', '0px').css('padding-bottom', '40px');
        }
        if (type === 'rate') {
            $('#filterCollapsedOrNotTextrate').css('margin-top', '0px').css('padding-bottom', '40px');
        }
        if (type === 'location') {
            $('#filterCollapsedOrNotTextlocation').css('padding-bottom', '20px');
        }
        if (type === 'university') {
            $('#filterCollapsedOrNotTextuniversity').css('padding-bottom', '20px');
        }
        if (type === 'price') {
          $('#filterCollapsedOrNotTextprice').css('padding-bottom', '20px');
        }
        if (length == 0) {
            $('#filterCollapsedOrNotText' + type).html(langList.any);
        } else {
            $('#filterCollapsedOrNotText' + type).html(langList.selected + ' ( ' + length + ' )');
        }
    } else if ($('#' + type + 'Toggle').hasClass('fa-angle-down')) {
        $('#' + type + 'Toggle').removeClass('fa-angle-down');
        $('#' + type + 'Toggle').addClass('fa-angle-up');
        $('#filterCollapsedOrNotDiv' + type).show();
        if (no != null) {
            $('#filtermore' + no).show();
        }
        if (type == 'zone') {
            $('#filterCollapsedOrNotTextzone').css('padding-bottom', '0');
        }
        if (type == 'keyword') {
            $('#filterCollapsedOrNotTextkeyword').css('margin-top', '-10px');
        }
        if (type == 'rate') {
            $('#filterCollapsedOrNotTextrate').css('margin-top', '-10px');
        }
        if (type == 'location') {
            $('#filterCollapsedOrNotTextlocation').css('padding-bottom', '0px');
        }
        if (type == 'university') {
            $('#filterCollapsedOrNotTextuniversity').css('padding-bottom', '0px');
            $('#filterCollapsedOrNotTextuniversity').css('margin-top', '0px');
        }
        $('#filterCollapsedOrNotText' + type).html('');
    }
};

// update hotel results with chain names
window.filterByChainNames = function (element) {
    var chainName = $(element).val();
    var id = element.id.replace(/chain_/gi, '');
    if ($(element).prop('checked')) {
        hasFilterSelected();
        chainNames.push(chainName);
        $('#chainCheckedLength').val(chainNames.length);
        var chainHtml;
        chainHtml = '<div id="remove' + id + '"><div class="checkbox checkbox-red-x">' +
          '<input class="usitrip-hover-tag" onchange="removeFromSelectedOptions(' + id + ')" type="checkbox" checked>' +
          '<label class="usitrip-hover-tag">' + chainName + '</label>' +
          '</div></div>';
    } else {
        chainNames.splice(chainNames.indexOf(chainName), 1);
        $('#remove' + id).remove();
    }

    hotelFilter();
};

window.clearChainNames = function () {
    // clear chain from selected section on top left section
    var str = chainNames.toString();
    // if chain selected
    if (str != '') {
        var chainArray = new Array();
        chainArray = str.split(',');
        chainArray.forEach(function (element) {
            var id = $('#chainBlock').find('input[value="' + element + '"]').attr('id').replace(/chain_/gi, '');
            $('#remove' + id).remove();
        });
        chainNames = [];
        $('#chainCheckedLength').val(0);

        hotelFilter();
    }
};

window.subscribe = function () {
    var emailvalid = $('#subscribeEmail').val();
    if (validateEmail(emailvalid) == true && emailvalid != '') {
        $('.loadingBtnList').button('loading');

        if (shouldTrack) {
            gtag_report_conversion_subscribe();
        }

        $.post('/subscribe', {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'email': $('#subscribeEmail').val(),
            'pos': 2
        }, function (data) {
            if (data['success'] == true) {
                if (typeof(Storage) !== 'undefined') {
                    localStorage.setItem('subEmail', 1);
                    localStorage.setItem('userEmail', $('#subscribeEmail').val());
                }

                if (data['message'] == 'sub01') {
                    $('.message-1').show();
                } else {
                    $('.message-2').show();
                }

                $('.loadingBtnList').button('reset');
                setTimeout(function () {
                    $('#listCouponBox').fadeOut('fast');
                }, 5000);
            }
        }).fail(function (data) {
            if (data.status == 429) {
                alert(data.responseText);
            }
        });
    } else {
        // 列表页顶部订阅按钮邮件检测失败
        swal(langList.errorEmail, "", 'error');
    }
};

window.clearMap = function () {
    $('#infowindow').hide();
    if (selectedMaker != null) {
        var label = selectedMaker.getLabel();
        label.fontSize = '20px';
        selectedMaker.setIcon(originIcon);
        selectedMaker.setLabel(label);
        selectedMaker.setZIndex(selectedMaker.opt);
        selectedMaker = null;
    }
};

window.initMap = function () {
    hotelMap = new google.maps.Map(document.getElementById('hotelsMap'), {
        zoom: 12,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP
        },
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP
        }
    });

    hotelMap.addListener('click', function () {
        clearMap();
    });

    hotelMap.addListener('zoom_changed', function () {
        if (hotelMap.getBounds()) {
            filterHotelsByBounds();
        }
    });

    hotelMap.addListener('dragend', function () {
        if (hotelMap.getBounds()) {
            filterHotelsByBounds();
        }
    });
};

window.addMarkers = function () {
    if (searchedMapHotel) {
        mapHotels.push(searchedMapHotel);
    }

    // add each hotel's marker
    mapHotels.forEach(function (hotel, index) {
        var guestRatingLine = '<p class="guest-rating-blank-space"></p>';
        if (hotel.guestRating) {
            var guestRating = hotel.guestRating + '/5';
            var guestReviewCount = hotel.guestReviewCount == null ? '0' : hotel.guestReviewCount;
            guestRatingLine = '<p class="font-14 usitrip-fade" style="margin-top:-18px;">' + langList.guestRating + '&nbsp;<span class="guest-rating">&nbsp;' +
              guestRating +
              '&nbsp;</span>&nbsp;<span class="rating-count">(' +
              guestReviewCount +
              langList.review + ')</span></p>';
        }
        var feature = hotel.lowest_flag ? '<span><i class="fa fa-check"></i>' + langList.lowest + '</span>' : '';
        if (hotel.freeCancel === 1) {
            feature += '<br><span><i class="fa fa-check"></i>' + langList.free_cancel + '</span>';
        }
        var price = hotel.beforeTax ? '<strike class="beforeTax">' + hotel.currency + hotel.beforeTax + '</strike><span class="afterTax">' + hotel.currency + hotel.price + '</span>' : '<span>' + hotel.currency + hotel.price + '</span>';

        var infoContent =
          '<div>' +
          '<a href="' + hotel.hotelLink + '" target="_blank" onclick="jumpHotel(' + hotel.hotelId + ', \'' + escape(hotel.name) + '\');">' +
          '<img class="infowindow-img" src="' + hotel.thumbnail + '" height="149" width="300" onerror="this.src=\'/img/general/img_error.jpg\'"/>' +
          '</a>' +
          '<div class="hotel-info-wrapper">' +
          '<a href="' + hotel.hotelLink + '" target="_blank" class="hotel-name" onclick="jumpHotel(' + hotel.hotelId + ', \'' + escape(hotel.name) + '\');">' + hotel.name + '</a>' +
          '<div>' + hotel.rating + '</div><br/><br/>' +
          guestRatingLine +
          '<div class="hotel-feature">' +
          '<div class="map-hotel-feature">' +
          feature +
          '</div>' +
          '<div class="map-hotel-price">' + price + '</div>' +
          '</div>' +
          '</div>' +
          '</div>';

        var position = new google.maps.LatLng(hotel.lat, hotel.lng);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(hotel.lat, hotel.lng),
            label: {
                text: (hotel.currency === 'USD' ? '$' : hotel.currency) + hotel.price,
                color: 'white',
                fontSize: '20px'
            },
            icon: originIcon,
            map: hotelMap,
            opt: index,
            zIndex: index
        });

        markers.push(marker);
        mapBounds.extend(position);

        marker.addListener('click', function () {
            if (selectedMaker != this) {
                var label = this.getLabel();
                label.fontSize = '22px';
                label.color = 'white';
                this.setIcon(selectedIcon);
                this.setLabel(label);
                this.setZIndex(highestZIndex);
                if (selectedMaker != null) {
                    var label = selectedMaker.getLabel();
                    label.fontSize = '20px';
                    label.color = 'white';
                    selectedMaker.setIcon(originIcon);
                    selectedMaker.setLabel(label);
                    selectedMaker.setZIndex(selectedMaker.opt);
                }
                selectedMaker = this;
                $('#infowindow').empty();
                $('#infowindow').append(infoContent);
                $('#infowindow').css({'visibility': 'visible', 'display': 'block'});
            }
        });
        marker.addListener('mouseover', function (evt) {
            var label = this.getLabel();
            label.fontSize = '22px';
            label.color = '#2681ff';
            if (selectedMaker != this) {
                this.setIcon(overIcon);
                this.setLabel(label);
                this.setZIndex(100);
            }
        });
        marker.addListener('mouseout', function (evt) {
            var label = this.getLabel();
            label.fontSize = '20px';
            label.color = 'white';
            if (selectedMaker != this) {
                this.setIcon(originIcon);
                this.setLabel(label);
                this.setZIndex(this.opt);
            }
        });
    });
};

window.clearMarkers = function () {
    $('#infowindow').hide();
    // clear marker array
    mapHotels = [];
    // clear markers on map
    if (markers.length > 0) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
    }
};

window.backToList = function () {
    $('#mapWrapper').hide();
    $('.wired-all-wraper-for-hotel').show();
    $('#wrapper').show();
    $('#backtotop').show();
    $('footer').show();
    clearMap();
}

window.filterHotelsByBounds = function () {
    if (fmxhr && fmxhr.readyState !== 4) {
        fmxhr.abort();
    }

    var bounds = hotelMap.getBounds();
    var northeast = bounds.getNorthEast(), southwest = bounds.getSouthWest(), zoom = hotelMap.getZoom();

    if (sessionKey) {
        var filters = {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'roomCount': roomCount,
            'dayCount': dayCount,
            'northeast_lat': northeast.lat(),
            'northeast_lng': northeast.lng(),
            'southwest_lat': southwest.lat(),
            'southwest_lng': southwest.lng(),
            'zoom': zoom
        };

        fmxhr = $.post('/list/map/filter/' + sessionKey, filters, function (data) {
            if (!data.success) {
                swal({
                    title: lang_login_expired,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: lang_login
                }, function () {
                    location.reload();
                });
            } else if (data.success) {
                if (data.hotels && data.hotels.length != 0) {
                    if (isHotelSearch && searchedHotel) {
                        for (var i = 0; i < data.hotels.length; i++) {
                            if (searchedHotel.hotelId == data.hotels[i].hotelId) {
                                data.hotels.splice(i, 1);
                            }
                        }
                    }

                    buildMapHotels(data.hotels);
                }
            }
        })
    }
};

window.buildMapHotels = function (hotels) {
    clearMarkers(); // clear markers from last page
    mapHotels = [];

    $.each(hotels, function (i, field) {
        // hotel link to open for hotel details
        var hotelLink = '/hotel/' + field.hotelId + '.html?s=' + sessionKey + '&' + $('#search-form').serialize();

        // build thumbnail link
        var thumbnail;
        if (field.thumbnail == "") {
            thumbnail = '/img/general/img_error.jpg';
        } else {
            thumbnail = field.thumbnail;
        }

        // build hotel name
        var hotelName = page_lang == 0 ? groupName(field.name, field.name_zh) : groupName(field.name);

        // decide currency
        if (field.currency == null) {
            field.currency = '$';
        }
        var currency = field.currency == 'USD' ? '$' : field.currency;

        // show distance from search city for selected scenic
        var search_city = destinationCity == "" ? cityName : destinationCity;//搜索名称
        var dest_city_match_en = field.destinationName.split(',', 1)[0];
        var dest_city_match_cn = field.destinationName_zh.split(',', 1)[0];
        var hotel_city = field.city == null ? "" : field.city.charAt(0).toUpperCase() + field.city.slice(1).toLowerCase();

        // push hotel to mapHotels to show on map
        if (field.latitude && field.longitude && field.minPrice > 0) {
            var mapHotel = {
                lat: field.latitude,
                lng: field.longitude,
                price: field.minPrice,
                name: hotelName,
                thumbnail: thumbnail,
                guestRating: field.guestRating ? field.guestRating + '/5' : '',
                guestReviewCount: field.guestReviewCount == null ? '0' : field.guestReviewCount,
                lowest_flag: field.lowestFlg == 0,
                rating: buildRating(field.rating, field.category),
                currency: currency,
                beforeTax: field.discount > 0 ? field.originalPrice : null,
                hotelLink: hotelLink,
                sessionKey: sessionKey
            };

            mapHotels.push(mapHotel);
        }
    });

    addMarkers();
};

// All document ready events should be put here
$(function () {
    //热门目的地列表
    U_hotels.destination('desName', 'desId');

    window.onbeforeunload = function (event) {
        if (txhr) txhr.abort();
        if (sxhr) sxhr.abort();
        if (fxhr) fxhr.abort();
    };

    $('[class^="related"]').not(':first').hide();

    $('.roomCount').on('change', function () {
        $('.related_' + this.value + '_content').show().siblings('[class^="related"]').hide();
    });

    //如果filter
    var collapsedSizep = '47px';
    $('#scenicArea').each(function () {
        var x = $('#scenicArea').height();
        var div = $(this);
        if (x > 50) {
            div.css('height', collapsedSizep);
            div.after('<a style="padding-top:10px; outline:none;" id="seemore" class="photoslider text-center" href="#">' + langList.morep + '</a>');
            var link = div.next();
            link.click(function (p) {
                p.stopPropagation();

                if (link.text() !== langList.lessp) {
                    link.text(langList.lessp);
                    div.animate({'height': x});
                } else {
                    div.animate({
                        'height': collapsedSizep
                    });
                    link.text(langList.morep);
                }
            });
        }

    });

    if (isHotelApp) {
        $('#master-main').addClass('usitrip-pc-bg');
    }

    $('#list-search-info-toggle').click(
      function () {
          // enMarginTop这个命名很早的时候用来做别的，在swal出来后会有display为none， 下面这一条临时解决这个问题
          $('#enMarginTop').css('display', 'block');
          $('#enMarginTop').removeClass('hidden');
          $('#change-search-toggled').addClass('change-search-toggled');
          $('#list-search-info-toggle').hide();
      });
    $('#cancel-re-search').click(
      function () {
          $('#enMarginTop').addClass('hidden');
          $('#change-search-toggled').removeClass('change-search-toggled');
          $('#list-search-info-toggle').show();
      });

    //left filter stay on top with scroll
    var counter = 0;
    var whyUs = $('#whyUs');
    var whyUsClass = $('.why-us');
    var sortToTop = $('#counter');
    var pos = sortToTop.position();
    whyUsClass.css('display', 'none');
    $(window).scroll(function () {
        var windowpos = $(window).scrollTop();
        // if scroll why us part cover footer, display none
        if (($(document).height() - windowpos - $(window).height()) < (914 - $(document).height() + windowpos)) {
            $('#whyUs').hide();
        } else {
            $('#whyUs').show();
        }

        if (windowpos > ($('.if-hit-stick-to-top').height() + 650)) {
            sortToTop.removeClass('hidden');
            sortToTop.addClass('stick');
            whyUs.addClass('stick');
            whyUsClass.css('display', 'block');
            $('.why-us-block').css('margin-top', '120px');
        } else {
            sortToTop.removeClass('stick');
            whyUs.removeClass('stick');
            sortToTop.addClass('hidden');
            whyUsClass.css('display', 'none');
            $('.why-us-block').css('margin-top', '57px');
        }
    });
    // 点击返回头部
    sortToTop.click(function () {
        $('body, html').animate({
            scrollTop: 0
        }, 600);
    });

    // preselect passed search params
    $('#checkin').val(checkin);
    $('#checkout').val(checkout);
    $('.roomCount').val(roomCount);
    $('.adultCount1').val(adultCount1);
    $('.childCount1').val(childCount1);
    if (childCount1 > 0) {
        $('#childAgeWrapper').removeClass('hide');
        var content = $('#childAgeContent');
        var select_box = $('.child-age-select');
        select_box.val(childAges[0]);
        for (var i = 1; i < childCount1; i++) {
            var new_select = select_box.last().clone();
            new_select.val(childAges[i]);
            content.append(new_select);
        }
    }

    //检测修改单复数单词
    if (is_usitour) {
        if (roomCount <= 1) {
            $('#tRoom_text').text('room, ');
        } else {
            $('#tRoom_text').text('rooms, ');
        }
        if (adultCount1 <= 1) {
            $('#tAdult_text').text('adult, ');
        } else {
            $('#tAdult_text').text('adults, ');
        }
        if (childCount1 <= 1) {
            $('#tChild_text').text('child');
        } else {
            $('#tChild_text').text('children');
        }
    }
    // before click to show the dialog, detect if any of those btn is disabled
    if (roomCount == 1) {
        $('#roomCountSubtract').addClass('disabled-btn');
        $('#roomCountSubtract').attr('disabled', true);
    }
    if (roomCount == 8) {
        $('#roomCountAdd').addClass('disabled-btn');
        $('#roomCountAdd').attr('disabled', true);
    }
    if (adultCount1 == 1) {
        $('#adultCountSubtract').addClass('disabled-btn');
        $('#adultCountSubtract').attr('disabled', true);
    }
    if (adultCount1 == 4) {
        $('#adultCountAdd').addClass('disabled-btn');
        $('#adultCountAdd').attr('disabled', true);
    }
    if (childCount1 == 0) {
        $('#childCountSubtract').addClass('disabled-btn');
        $('#childCountSubtract').attr('disabled', true);
    }
    if (childCount1 == 4) {
        $('#childCountAdd').addClass('disabled-btn');
        $('#childCountAdd').attr('disabled', true);
    }

    // Datepicker setup
    $('#checkin').datepicker({
        startDate: '0d',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: 'yyyy-mm-dd',
        defaultDate: checkin
    });

    $('#checkout').datepicker({
        startDate: '1d',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: 'yyyy-mm-dd',
        defaultDate: checkout
    });

    // get hotels
    renderHotels();

    $('#desName').click(function (e) {
        sessionStorage.clear();
    });

    if (typeof(Storage) !== 'undefined') {
        localStorage.setItem('destinationId', $('#desId').val());
        localStorage.setItem('destinationName', $('#desName').val());
        localStorage.setItem('checkin', $('#checkin').val());
        localStorage.setItem('checkout', $('#checkout').val());
    }

    $(window).resize(function () {
        var height = $(window).height();
        height = height * 0.8;
        $('.fixed').css({'top': height});
    });

    var typingTimer;                //timer identifier
    var doneTypingInterval = 300;  //delay search time (in ms)
    var $destinationInput = $('#desName');

    $destinationInput.bind('input propertychange', function (e) {
        $(this).unbind('focus');
        $(this).unbind('blur');

        $('#desName').attr('tip', $(this).val());
        var keyword = encodeURIComponent($(this).val().trim());
        clearTimeout(typingTimer);

        typingTimer = setTimeout(function () {
            if (keyword == "") {
                $('div[class="autocomplete-suggestions"]').remove();
                return false;
            }

            if (dhxhr && dhxhr.readyState != 4) {
                dhxhr.abort();
            }

            dhxhr = $.getJSON('/destination/suggestions?keyword=' + keyword, function (data) {
                setupAutocomplete(data, isCh, 'desName', 'desId');
            });
        }, doneTypingInterval);
    });

    $destinationInput.on('focus', function () {
        $(this).select();
    });

    //on keydown, clear the countdown
    $destinationInput.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    $destinationInput.change(function () {
        $('input[name="scenicName"]').val('');
        $('input[name="scenicId"]').val('');
    });

    // start room， adult， child count
    var _searchDesc = $('#room-guest-toggle');//房间人数显示框
    var _searchDialog = $('#searchDialog');
    var roomTemplateHtml = $('#roomTemplate').text();
    var childAgeHtml = $('#childAgeTemplate').text();
    _searchDesc.on('click', function () {
        if ($('#searchDialog').is(':hidden')) {
            _searchDialog.show();
            _searchDesc.addClass('person_desc_active');
        } else {
            _searchDialog.hide();
            _searchDesc.removeClass('person_desc_active');
        }
    });

    _searchDialog.on('click', function (e) {
        e.stopPropagation();
    });

    $('body').on('click', function (e) {//点击其他地方时，隐藏房间人数选择器
        if (e.target != _searchDesc[0]) {
            _searchDialog.hide();
            _searchDesc.removeClass('person_desc_active');
        }
    });

    // start 选择房间数量，成人数量，儿童数量
    // home banner spinner(+-btn to change value) & total to parent input
    $('.btn-subtract, .btn-add').on('click', function () {
        var btn = $(this);
        var btnId = $(this).context.id;
        var targetId = btnId.replace(/Add|Subtract/g, '');

        input = btn.closest('.number-spinner').find('input');
        inputName = input[0].name;
        oldValue = input.val().trim();

        if (btn.attr('data-dir') == 'up') {
            if (oldValue < input.attr('max')) {
                oldValue++;
                if ($('#' + targetId + 'Subtract').hasClass('disabled-btn')) {
                    $('#' + targetId + 'Subtract').removeClass('disabled-btn');
                    $('#' + targetId + 'Subtract').attr('disabled', false);
                }
            }
            if (oldValue == input.attr('max')) {
                if ($('#' + targetId + 'Add').hasClass('disabled-btn')) {
                } else {
                    $('#' + targetId + 'Add').addClass('disabled-btn');
                    $('#' + targetId + 'Add').attr('disabled', true);
                }
            } else {
                if ($('#' + targetId + 'Add').hasClass('disabled-btn')) {
                    $('#' + targetId + 'Add').removeClass('disabled-btn');
                    $('#' + targetId + 'Add').attr('disabled', false);
                }
            }
        } else {
            if (oldValue > input.attr('min')) {
                oldValue--;
                if ($('#' + targetId + 'Add').hasClass('disabled-btn')) {
                    $('#' + targetId + 'Add').removeClass('disabled-btn');
                    $('#' + targetId + 'Add').attr('disabled', false);
                }
            }
            if (oldValue == input.attr('min')) {
                if ($('#' + targetId + 'Subtract').hasClass('disabled-btn')) {
                } else {
                    $('#' + targetId + 'Subtract').addClass('disabled-btn');
                    $('#' + targetId + 'Subtract').attr('disabled', true);
                }
            } else {
                if ($('#' + targetId + 'Subtract').hasClass('disabled-btn')) {
                    $('#' + targetId + 'Subtract').removeClass('disabled-btn');
                    $('#' + targetId + 'Subtract').attr('disabled', false);
                }
            }
        }
        // 修改 spinner 数字
        input.val(oldValue);
        // 修改上部数字显示和发送给后端的hidden input
        if (inputName === 'roomCount') {
            $('#tRoom').text(oldValue);
            $('.roomCount').val(oldValue);
            //检测修改单复数单词
            if (is_usitour && 1 === oldValue) {
                $('#tRoom_text').text('room, ');
            } else if (is_usitour && oldValue > 1) {
                $('#tRoom_text').text('rooms, ');
            }
        } else if (inputName === 'adultCount1') {
            $('#tAdult').text(oldValue);
            $('.adultCount1').val(oldValue);
            //检测修改单复数单词
            if (is_usitour && 1 === oldValue) {
                $('#tAdult_text').text('adult, ');
            } else if (is_usitour && oldValue > 1) {
                $('#tAdult_text').text('adults, ');
            }
        } else if (inputName === 'childCount1') {
            $('#tChild').text(oldValue);
            $('.childCount1').val(oldValue);
            //检测修改单复数单词
            if (is_usitour && oldValue <= 1) {
                $('#tChild_text').text('child');
            } else if (is_usitour && oldValue > 1) {
                $('#tChild_text').text('children');
            }
            if (0 === oldValue) {
                $('#childAgeWrapper').addClass('hide');
            } else {
                $('#childAgeWrapper').removeClass('hide');
                var content = $('#childAgeContent');
                var select_box = $('.child-age-select');
                var count = select_box.length;
                if (oldValue > count) {
                    for (var i = count; i < oldValue; i++) {
                        content.append(select_box.last().clone());
                    }
                } else {
                    for (var i = oldValue; i < count; i++) {
                        content.find('.child-age-select:last').remove();
                    }
                }
            }
        }
    });

    $('#tRoom').change(function () {
        $('.roomCount').val($(this).val());
        $('#tRoom').html($(this).val());
    });

    $('#tAdult').change(function () {
        $('.adultCount1').val($(this).val());
        $('#tAdult').html($(this).val());
    });

    $('#tChild').change(function () {
        $('.childCount1').val($(this).val());
        $('#tChild').html($(this).val());
    });
    //  end room， adult， child count

    // subscribe form submit action
    $('#listCouponBox').submit(function (e) {
        e.preventDefault();
        subscribe();
    });
});