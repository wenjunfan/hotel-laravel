/**
 * 仅英文站加载
 * Created by 509 on 2016/11/10.
 */
/**
 * 全局类
 * @author Howard
 */
function global () {
    //设置artDialog的路径
    window._artDialog_path = '/tpl/www/js/artDialog';
}

global.prototype = {
    /**
     * 取得COOKIE值【PHP标准】
     * @param name
     * @returns {string}
     */
    getCookie: function (c_name) {
        var c_start = null, c_end = null;
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) c_end = document.cookie.length;
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    },

    /**
     * 设置COOKIE
     * @param {String} name COOKIE名称
     * @param {String} value 内容
     * @param {Date} expires 日期对象
     * @param {String} path 路径
     * @param {String} domain 域名
     * @param secure
     */
    setCookie: function (name, value, expires, path, domain, secure) {
        var str = name + "=" + encodeURI(value);
        if (expires) {
            str += "; expires=" + expires.toGMTString();
        }
        if (path) {
            str += "; path=" + path;
        }
        if (domain) {
            str += "; domain=" + domain;
        }
        if (secure) {
            str += "; secure";
        }
        document.cookie = str;
    },
    /**
     * 清除当前页面的html静态缓存
     */
    removeCache: function (url) {
        var t_url = window.location.href.toString();
        var url = this.url_rand(url);
        url += '&ajax=true&callback=?';
        $.getJSON(url, {action: "removeCache", t_url: t_url}, function (json) {
            if (json['status'] == 'OK') {
                alert('清除成功数：' + json['success_num'] + '，路径：' + json['path'] + '，刷新查看最新页面。');
                window.location.reload(true);
            } else {
                alert('删除失败！');
            }
        });
    },
    /**
     * 把团号中的-号之前的信息去掉
     */
    isModelFix: function (url) {
        var url = this.url_rand(url);
        url += '&ajax=true&action=isAdminLogin&callback=?';
        $.getJSON(url, function (json) {
            if (!json['code'] || json['LID'] == 0) {
                var M = $('.JS_ismodel');
                if (M.length) {
                    M.each(function (i) {
                        var old_html = $(this).html();
                        var new_html = old_html.replace(/.+\-/, '');
                        $(this).html(new_html).attr('old', old_html);
                        $(".JS_ismodel_box").show();
                    });
                }
            } else {//如果管理员登录，把搜索列表中的旅游团号隐藏。
                $(".m-pro-cont-l p").each(function () {
                    if (this.innerHTML != "") {
                        //注意，其中的文字内容包含“：”和“:”，需要考虑存在中文 “：”的情况。
                        this.innerHTML = this.innerHTML.replace("旅游团号", "").replace(":", "").replace("：", "");
                    }
                });
                $(".JS_ismodel_box").show();
            }
        });
    },
    /**
     * 判断浏览器是否是IE8或以前的版本
     * @returns {boolean}
     */
    isIE8: function () {
        var userAgent = window.navigator.userAgent.toLowerCase();
        $.browser.msie8 = $.browser.msie && /msie 8\.0/i.test(userAgent);
        $.browser.msie7 = $.browser.msie && /msie 7\.0/i.test(userAgent);
        $.browser.msie6 = !$.browser.msie8 && !$.browser.msie7 && $.browser.msie && /msie 6\.0/i.test(userAgent);
        if ($.browser.msie8 || $.browser.msie7 || $.browser.msie6) {
            return true;
        } else {
            return false;
        }
    },
    /**
     * method addOne 购物车增加一个产品效果
     * @param url
     */
    addOne: function () {
        $('.addOne').css({
            'opacity': '100',
            'display': 'block',
            'left': 20,
            'top': 0
        }).stop().animate({
            'top': '0',
            'opacity': 0,
            'display': 'none'
        }, 1000).css({
            'left': 20,
            'top': 20
        });
    },
    /**
     * method throw 购物车抛掷效果
     * @param obj.startTarget:投掷开始点JQ对象
     * @param obj.endTarget:投掷结束点JQ对象
     * @param obj.throwImg:投掷图片JQ对象
     * @param obj.throwWay:投掷方式,1为直线,2为抛物线,3为分阶段路线
     */
    throws: function (obj) {
        var starX = (obj.startTarget.offset().left + obj.startTarget.width() / 2) - 10,
          starY = (obj.startTarget.offset().top + obj.startTarget.height() / 2) - 10,
          endTarget = obj.endTarget,
          endX = (endTarget.offset().left + endTarget.width() / 2) - 10,
          endY = (endTarget.offset().top + endTarget.height() / 2) - 10,
          slowY = 0;

        if (!obj.throwImg.is(':animated')) {
            obj.throwImg.css({
                'display': 'block',
                'left': starX + 'px',
                'top': starY + 'px',
                'display': 'block',
                'opacity': 1,
                'width': '50px',
                'height': '50px',
                'z-index': 100
            });
            switch (obj.throwWay) {
              // 直线购物车效果
                case 1 :
                    (function () {
                        obj.throwImg.stop().animate({
                            'left': endX + 'px',
                            'top': endY + 'px',
                            'width': '14px',
                            'height': '14px'
                        }, 1000).fadeOut();
                        setTimeout(G.addOne, 1000);
                    })();
                    break;
              // 曲线购物车效果
                case 2 :
                    (function () {
                        var p = 0,
                          X = 0,
                          Y = 0,
                          timer = setInterval(function () {
                              X++;
                              if (endY > starY) {
                                  p = (endX - starX) * (endX - starX) / (-2 * (endY - starY));
                                  Y = X * X / (-2 * p);
                                  obj.throwImg.css({
                                      'left': X + starX + 'px',
                                      'top': Y + starY + 'px'
                                  });
                              }
                              else {
                                  p = (endY - starY) * (endY - starY) / (2 * (endX - starX));
                                  Y = Math.sqrt(2 * p * X);
                                  obj.throwImg.css({
                                      'left': X + starX + 'px',
                                      'top': starY - Y + 'px'
                                  });
                              }
                              if ((X + starX) >= endX) {
                                  clearInterval(timer);
                              }
                          }, 2);
                        obj.throwImg.stop().animate({
                            'width': '14px',
                            'height': '14px'
                        }, 1000).fadeOut();
                        setTimeout(G.addOne, 1000);
                    })();
                    break;
              // 分步阶段效果
                case 3 :
                    (function () {
                        if (endY >= starY) {
                            slowY = -70;
                        } else {
                            slowY = +50;
                        }
                        obj.throwImg.stop().animate({
                            'left': endX - 14 + 'px',
                            'top': endY + slowY + 'px'
                        }, 700).animate({
                            'top': endY + 'px',
                            'opacity': 0,
                            'z-index': 0
                        }, 1000);
                        setTimeout(G.addOne, 1500);
                    })();
                    break;
              // 抛物线购物车效果，引入jquery.fly.js
                case 4 :
                    (function () {
                        obj.throwImg.css({
                            'display': 'none'
                        });
                        var img = obj.throwImg[0].src;
                        //alert(img);
                        var flyer = $('<img class="flyer-img" src="' + img + '">');//抛物体对象
                        var scrollTop = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop//卷去的顶部
                        var scrollLeft = document.documentElement.scrollLeft || window.pageXOffset || document.body.scrollLeft;//卷去的左边
                        flyer.fly({
                            start: {
                                left: starX - scrollLeft,//starX,// //抛物体起点横坐标
                                top: starY - scrollTop//starY//////抛物体起点纵坐标
                            },
                            end: {
                                left: endX - scrollLeft, //抛物体终点横坐标
                                top: endY - scrollTop, //抛物体终点纵坐标
                                width: 14,
                                height: 14
                            },
                            onEnd: function () {
                                this.destory();//销毁抛物体
                                setTimeout(G.addOne, 500);
                            }
                        });
                    })();
                    break;
              // 抛物线购物车效果,自己计算的。缺陷：左抛出问题
                case 5 :
                    (function () {
                        var top = 0,
                          d_top = 60,
                          a = 0,
                          b = 0,
                          X = 0,
                          Y = 0,
                          timer = setInterval(function () {
                              X++;
                              if (endY > starY) {
                                  top = -d_top;
                                  var dY = endY - starY;
                                  var dX = endX - starX;
                                  b = (Math.sqrt(Math.pow(4 * top * dX, 2) - 16 * dY * top * dX * dX) + 4 * top * dX) / (2 * dY);
                                  a = -4 * top / (b * b);
                                  Y = a * X * (X - b);
                                  obj.throwImg.css({
                                      'left': X + starX + 'px',
                                      'top': Y + starY + 'px'
                                  });
                              }
                              else {
                                  var dY = endY - starY;
                                  var dX = endX - starX;
                                  top = dY - d_top;
                                  b = (Math.sqrt(Math.pow(4 * top * dX, 2) - 16 * dY * top * dX * dX) + 4 * top * dX) / (2 * dY);
                                  a = -4 * top / (b * b);
                                  Y = a * X * (X - b);
                                  obj.throwImg.css({
                                      'left': X + starX + 'px',
                                      'top': Y + starY + 'px'
                                  });
                              }
                              if ((X + starX) >= endX) {
                                  clearInterval(timer);
                              }
                          }, 2);
                        obj.throwImg.stop().animate({
                            'width': '14px',
                            'height': '14px'
                        }, 500).fadeOut();
                        setTimeout(G.addOne, 1000);
                    }());
                    break;
                default  :
                    (function () {
                    }());
            }
        }
    },

    /**
     * method throw 设置右边客服浮动盒子
     * @param url
     */
    showPop: function () {
        var content = $("#hbody").length > 0 ? $('#hbody') : $('#body');
        if (content.length != 0) {
            var contentWidth = content.width();
            var contentOffsetTop = content.offset().top;
            var contactWrap = $('.js-contact-wrap');
            var contactWidth = contactWrap.width();
            var winW = $(window).width();
            var popLeft = "0";
            if (contentWidth + contactWidth * 2 + 10 > winW) {
                popLeft = winW - contactWidth - 10
            } else {
                popLeft = (winW - contentWidth) / 2 + contentWidth + 10;
            }
            var popTop = contentOffsetTop + 5;
            if ((popLeft + contactWidth) > winW) {
                //popLeft = winW - contactWidth;
            }
            contactWrap.css({
                'right': 'auto',
                'top': popTop + 'px',
                'left': popLeft + 'px'
            });
        }

    },
    /**
     * 隐藏公共的消息提示
     */
    hideMessage: function () {
        setTimeout(function () {
            $("#message").fadeOut(1000);
        }, 3000);

    },
    /**
     * method getLoginStatus 获取登录状态
     * @param url
     */
    getLoginStatus: function (url) {
        var _this = this;
        this.shoppingCarUrl = url = this.url_rand(url);
        var ID = document.getElementById('loginOrLogin');
        //以下必须用GET的方式处理，如果用POST的话有些旧的浏览器不能解决跨域问题
        $.ajax({
            type: 'GET', url: url, data: {'action': 'loginAndShoppingCartCheckForTop', 'previousURL': URL.url}, dataType: 'jsonp',
            success: function (data) {
                if (typeof(data) == 'object' && data['status'] == 1) {
                    var html = '';
                    var loginWrap = $('#login-wrap');
                    var returnContent = data['html'];
                    if (returnContent['logStatus']) { //登录成功
                        //登录后保存为全局的登录状态
                        is_login = true;
                        window.usitripLoginStatus = {
                            isLogin: true,
                            name: returnContent['name']
                        };
                        html += '<span class="login-status js-login-status"><span class="login-hello">您好，</span>';
                        html += '<a class="login-name js-login-name">';
                        html += '<span class="simple-name">' + returnContent['name'] + '</span>';
                        html += '<span class="full-name">' + returnContent['name'] + '</span>';
                        html += '</a>';
                        html += '<a class="go-out" href="' + returnContent['logOffUrl'] + '">[退出]</a>';
                        html += '<input id="JS_UsiCId" type="hidden" value="' + returnContent['customersId'] + '">';
                        html += '</span>';
                    } else {
                        html += '<span class="logout-status js-logout-status"><a class="logi-btn" href="' + returnContent['loginUrl'] + '">登录</a><a class="regi-btn" href="' + returnContent['regUrl'] + '">免费注册</a></span>';
                    }

                    //添加公告
                    if (data.announce.length > 0) {
                        html += '<span class="notice"><div  id="notice_0"><span id="notice_1">';
                        for (var i = 0; i < data.announce.length; i++) {
                            html += '<a href="' + data.announce[i].hrefInfo + '" target="_blank">' + data.announce[i].title + '</a>';
                        }
                        html += '</span></div></span>';
                    }

                    loginWrap.children().first().nextAll().detach();//这里的处理是为了避免多次请求登录状态导致重复增加
                    loginWrap.append(html);

                    var simpleName = loginWrap.find('.simple-name');
                    var wid = simpleName.width();
                    if (wid > 150) {
                        simpleName.css('width', 150);
                    }
                    loginWrap.find('.js-login-name').attr('href', returnContent['myAccountUrl']);

                    var myUsitrip = $('#js-myusi');
                    myUsitrip.find('.myusi-text .title').attr('href', returnContent['myAccountUrl']);

                    $(ID).html(data['html']);	//项部登录信息数据
                    //渲染迷你购物车
                    if (data['shopcart']['list'] && $('#shopviewList li').length) {
                        $('#CarSumTop, #CarSumTop1').text(data['shopcart']['sum']);
                        $('#cartBoxTotal').text(data['shopcart']['money']);
                        var tpl = $('#shopviewList li').html();
                        var li_html = '';
                        for (var i = 0; i < data['shopcart']['list'].length; i++) {
                            var DelAction = "G.cartRemove('" + data['shopcart']['list'][i]['id'] + "','" + url + "&action=remove_product')";
                            li_html += '<li>';
                            var proName = data['shopcart']['list'][i]['name'];
                            proName = proName.replace('<', '');
                            proName = proName.replace('>', '');

                            li_html += tpl.replace(/___ProductName___/g, proName).
                              replace(/___ProductFinalPrice___/g, data['shopcart']['list'][i]['price_str']).
                              replace(/___ProductImgSrc___/g, data['shopcart']['list'][i]['img_src']).
                              replace(/___src___/g, 'src').
                              replace(/___ProductLinks___/g, data['shopcart']['list'][i]['product_href']).
                              replace(/___DelAction___/g, DelAction);
                            li_html += '</li>';
                        }
                        $('#shopviewList').html(li_html);
                    }
                    _this.renderShoppingCar(data);

                } else {
                    alert('程序异常getLoginStatus！');
                }
            }
        });

    },
    /**
     * 渲染迷你购物车
     * @param data  请求数据
     */
    renderShoppingCar: function (data) {
        var cartList = $('#js-cart-list'),
          shopCart = data['shopcart'],
          cartContent = shopCart['list'],
          sum = shopCart['sum'],
          money = shopCart['money'],
          cart,
          content = '';

        money = G.curReplace(money);

        for (var i = 0, len = cartContent.length; i < len; i++) {
            cart = cartContent[i];
            var delAction = "G.cartRemove('" + cart['id'] + "','" + this.shoppingCarUrl + "&action=remove_product')";
            content += '<dd>';
            content += '<a href="' + cart['product_href'] + '" class="pro-detail">';
            content += '<img src="' + cart['img_src'] + '">';
            content += '<span>' + cart['name'].replace(/[<>]/g, '') + '</span>';
            content += '</a>';
            content += '<div class="pro-action">';
            content += '<strong class="price">' + cart['price_str'] + '</strong><a onclick="' + delAction + '" href="javascript:void(0);" class="del">删除</a>';
            content += '</div>';
            content += '</dd>';
        }
        cartList.empty().append(content);

        var proTotal = $('#js-pro-total');
        proTotal.find('em').text(sum);
        proTotal.find('.total').text(money);

        var cartWrap = $('#js-cart');
        cartWrap.find('.cart-text .title strong').text(sum);
        if ($('#JS_shoppingNum').length) {
            $('#JS_shoppingNum').html(sum).parent('.shopping').click(function () {
                window.location.href = cartWrap.find('.cart-text a').attr('href');
            });
        }
    },

    reShoppingCar: function () {
        var _this = this;
        $.ajax({
            type: 'GET'
            , url: _this.shoppingCarUrl
            , data: {'action': 'loginAndShoppingCartCheckForTop', 'previousURL': URL.url}
            , dataType: 'jsonp'
            , success: function (data) {
                _this.renderShoppingCar(data);
            }
        });
    },

    /**
     * 把金额的货币符号切换成js能认得到的符号
     * @param {string} cur_price 带货币符号金额
     */
    curReplace: function (cur_price) {
        var p = cur_price.replace('&#65509;', '￥');
        p = p.replace('&#163;', '£').replace('&pound;', '£');
        p = p.replace('&euro;', '€');
        p = p.replace('&#165;', '¥');

        return p;
    },
    /**
     * 登录和购物车检查 loginOrLogin 能跨域
     * @param {string} url 目标地址
     */
    writeLoginInfo: function (url) {
        url = this.url_rand(url);
        var ID = document.getElementById('loginOrLogin');
        //以下必须用GET的方式处理，如果用POST的话不能解决跨域的问题
        $.ajax({
            type: 'GET', url: url, data: {'action': 'loginAndShoppingCartCheckForTop', 'previousURL': URL.url}, dataType: 'jsonp',
            success: function (data) {
                if (typeof(data) == 'object' && data['status'] == 1) {
                    $(ID).html(data['html']);	//项部登录信息数据
                    function TopShopCart (data) {	//迷你购物车数据
                        if (data['shopcart']['list'] && $('#shopviewList li').length) {
                            $('#CarSumTop, #CarSumTop1').text(data['shopcart']['sum']);
                            $('#cartBoxTotal').text(data['shopcart']['money']);
                            var tpl = $('#shopviewList li').html();
                            var li_html = '';
                            for (var i = 0; i < data['shopcart']['list'].length; i++) {
                                var DelAction = 'G.cartRemove("' + data['shopcart']['list'][i]['id'] + '","' + url + '&action=remove_product")';
                                li_html += '<li>';
                                var proName = data['shopcart']['list'][i]['name'];
                                proName = proName.replace('<', '');
                                proName = proName.replace('>', '');
                                li_html += tpl.replace(/___ProductName___/g, proName).
                                  replace(/___ProductFinalPrice___/g, data['shopcart']['list'][i]['price_str']).
                                  replace(/___ProductImgSrc___/g, data['shopcart']['list'][i]['img_src']).
                                  replace(/___src___/g, 'src').
                                  replace(/___ProductLinks___/g, data['shopcart']['list'][i]['product_href']).
                                  replace(/___DelAction___/g, DelAction);
                                li_html += '</li>';
                            }
                            $('#shopviewList').html(li_html);
                        }
                    }
                    TopShopCart(data);
                } else {
                    alert('程序异常writeLoginInfo！');
                }
            }
        });
    },
    /**
     * 删除顶部购物车的产品项
     * @param {string} prid_str 产品id字符串
     * @param {string} url 提交的目标网址
     */
    cartRemove: function (prid_str, url, x) {
        if (confirm("确定从购物车中删除此商品？")) {
            url = this.url_rand(url);
            url += '&ajax=true&action=remove_product&products_id=' + prid_str + '&callback=?';
            $.getJSON(url, function (json) {
                if (json['removeSuccess'] && json['removeSuccess'] == '1') {
                    alert('删除成功！');
                    window.location.reload();
                }
            });
        }
    },
    /**
     * 快速搜索关键词
     * @param string keyword_str 输入的关键词
     * @param string language_encoding 网页的语言编码，gb2312或big5
     * @param string url 提交的目标网址
     fastGetKeyword : function(keyword_str, language_encoding, url){
		if(keyword_str){
			url = this.url_rand(url);
			url += '&ajax=true&action=keywordsQuickSearch&language_encoding='+ language_encoding +'&w='+ keyword_str +'&callback=?';
			$.getJSON(url, function(json){
				//返回json，根据json的值来处理快速搜索菜单的数据

			});
		}
	},
     */

    /**
     * 实时载入J_BookingBox
     * @param {string} url 数据源url
     * @param {string} products_id_str 产品id字符串
     * @param {string} BookingBoxId 该购买面板的元素id名称
     * @param {array|string} otherParam 其它GET参数
     */
    getBookingBox: function (url, products_id_str, BookingBoxId, otherParam, callback) {
        if (products_id_str != "") {
            url = this.url_rand(url);
            if (otherParam) {
                if (typeof(otherParam) == 'object') {
                    if (otherParam['t_companion_id']) {
                        url += '&t_companion_id=' + otherParam['t_companion_id'];
                    }
                    if (otherParam['preview']) {
                        url += '&preview=' + otherParam['preview'];
                    }
                } else if (typeof(otherParam) == 'string') {
                    url += '&' + otherParam;
                }
            }

            url += '&products_id=' + products_id_str + '&action=getBookingBox&callback=?';
            var status = false;
            //$.getJSON(url, function (json) {
            //    //写购买面板的html内容
            //    json['top_div_id'] = BookingBoxId;
            //    //var BBox = new booking_box(json);
            //    var box = new usitrip.product.BookingBox(json);
            //    status=true;
            //});

            $.ajax({
                url: url,
                dataType: "json",
                success: function (json) {
                    if (json.status == 'soldout') {
                        $('.sold-out').show();
                        $('.no-sold-out').hide();
                    } else {
                        $('.no-sold-out').show();
                        //写购买面板的html内容
                        json['top_div_id'] = BookingBoxId;
                        //var BBox = new booking_box(json);
                        var box = new usitrip.product.BookingBox(json);
                    }
                },
                complete: callback || function () {return;}
            });
        }
    },

    /**
     * 产品详细页面大日历框
     * @param {string} url 数据源的地址
     * @param {int} products_id 产品id号
     * @param {number} year 要取得的年份。如：2014
     * @param {number} month 要取得的月份。如：12
     */
    getCalendarData: function (url, products_id, year, month) {
        if (products_id != "") {
            url = this.url_rand(url);
            url += '&departure_year=' + year + '&departure_month=' + month + '&products_id=' + products_id + '&action=getCalendarData&callback=?';
            $.getJSON(url, function (json) {
                if (json) {
                    //@todo 这里处理返回的json对象，到时安排前端人员写动态日历框，下面的代码只是示例
                    var html = '<ul>';
                    for (var i = 0; i < json.length; i++) {
                        html += '<li>' + json[i]['departure_date'] + ':' + json[i].departure_year + ':' + json[i].departure_month + ':' + json[i].departure_week + ':' + json[i].products_price + '……</li>'
                    }
                    html += '</ul>';
                    $('#J_Calendarbox').html(html);
                }
            });
        }
    },

    /**
     * 收集表单数据信息，用于ajax提交表单前的数据收集工作。类似jquery中的.serialize()
     * @param {string} form_id 表单ID
     * @param {string} output_type 输出类型
     * @return {array|string}
     */
    get_form_data: function (form_id, output_type) {
        var form = document.getElementById(form_id);
        var aparams = new Array();
        /* 创建一个阵列存表单所有元素和值 */
        var eval_string = new Array();
        for (var i = 0; i < form.elements.length; i++) {
            var name = encodeURIComponent(form.elements[i].name);
            /* 取得表单元素名 */
            var value = '';
            if (form.elements[i].type == "radio" || form.elements[i].type == "checkbox") {        /* 处理单选、复选按钮值 */
                var a = '';
                if (form.elements[i].checked == true) {
                    a = form.elements[i].value;
                    value = encodeURIComponent(a);
                    /* 获得表单元素值 */
                }
                else {
                    name = "";
                }

            } else {
                value = encodeURIComponent(form.elements[i].value);
                /* 获得表单元素值1 */
            }

            if (name != "") {
                var _l = aparams.length;
                aparams[_l] = new Array();
                aparams[_l][name] = value;
                eval_string[eval_string.length] = name + '=' + value;
            }
        }
        if (output_type == "array") {
            return aparams;
        }
        var string = eval_string.join('&');
        string += "&ajax=true";

        return string;
    },

    /**
     * 格式化转换特殊符号
     * @param str 源字符串
     * @return {string|XML}
     */
    htmlspecialchars: function (str) {
        str = str.replace(/&/g, '&amp;');
        str = str.replace(/</g, '&lt;');
        str = str.replace(/>/g, '&gt;');
        str = str.replace(/'/g, '&acute;');
        str = str.replace(/"/g, '&quot;');
        str = str.replace(/\|/g, '&brvbar;');
        return str;
    },

    /**
     * 快速到达当前页面某个位置
     * @param obj 目标元素如#id或对象
     */
    _goto: function (obj) {
        $("html,body").animate({scrollTop: $(obj).position().top});
    },

    /**
     * 给url添加随机参数
     * @param {string} url
     * @return {String}
     */
    url_rand: function (url) {
        if (url.indexOf("?") > 0) {
            url += "&randnumforajaxaction=" + Math.random();
        } else {
            url += "?randnumforajaxaction=" + Math.random();
        }
        return url;
    },
    /**
     * 倒计时程序
     * @param remainingTime 结束时间的毫秒时间戳
     * @param {jQueryEl} oDayEl 倒计时天数节点
     * @param {jQueryEl} oHourEl 倒计时小时节点
     * @param {jQueryEl} oMinEl 倒计时分钟节点
     * @param {jQueryEl} oSecondEl 倒计时秒数节点
     */
    countDown: function (remainingTime, oDayEl, oHourEl, oMinEl, oSecondEl, callback) {
        // var task;

        function timer () {
            var ts = new Date(remainingTime) - new Date();//计算剩余的毫秒数
            if (ts <= 0) {
                // clearInterval(task);
                $(oDayEl).parent().css({'visibility': 'hidden'});
                callback && callback instanceof Function && callback();
                return;
            }
            var dd = parseInt(ts / 1000 / 60 / 60 / 24, 10);//计算剩余的天数
            var hh = parseInt(ts / 1000 / 60 / 60 % 24, 10);//计算剩余的小时数
            var mm = parseInt(ts / 1000 / 60 % 60, 10);//计算剩余的分钟数
            var ss = parseInt(ts / 1000 % 60, 10);//计算剩余的秒数

            //dd = zeroFill(dd);
            hh = zeroFill(hh);
            mm = zeroFill(mm);
            ss = zeroFill(ss);

            oDayEl && oDayEl.text(dd);
            oHourEl && oHourEl.text(hh);
            oMinEl && oMinEl.text(mm);
            oSecondEl && oSecondEl.text(ss);
            setTimeout(timer, 1000);
        }
        timer();

        // task = setInterval(timer, 1000);
        function zeroFill (i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
    },
    /**
     * 判断浏览器是不是IE6
     * @returns {boolean}
     */
    isIE6: function () {
		var ie6 = !-[1, ] && !window.XMLHttpRequest;
        return ie6;
    },
    /**
     * 去除购物车和订单中小计数据
     * @param TotalBox 价格显示的节点 #J_cartTotal > p
     */
    hideSubTotal: function (TotalBox) {
        var Box = $(TotalBox);
        if (Box.length == 2) {
            Box.each(function (i) {
                if ($(this).attr('rel') == 'Ot_SubTotal') {
                    $(this).hide();
                }
            });
        }
    },
    /**
     * 更换货币
     */
    updateCurrency: function () {
        /**
         * 修改显示货币
         */
        var curPrice = {
            //绑定事件
            bind: function () {
                var that = this;
                $('#topCurrencyBox ul li a').click(function () {
                    var self = $(this),
                      text = self.text();
                    //把货币代码存入cookie【是大写的】
                    var v = (self.attr('cur')).toUpperCase();
                    $.removeCookie('currency', {'domain': document.domain, 'path': '/'});
                    $.cookie('currency', v, {'domain': document.domain, 'path': '/'});
                    /*
					 //取出当前存在cookie的货币代码
					 var code = ($.cookie('currency')).toLowerCase();
					 //调用修改货币方法
					 that.edit(code);
					 $('#defCurrency').text(text);
					 */

                    window.location.reload(false);
                });
            },
            //修改价格
            edit: function (code) {
                //做循环取标签里面的值写入标签
                $('.js_product_special_price').each(function (i) {
                    var m = $(this).attr('cur_' + code);
                    if (m) {
                        $(this).html(m.replace('$', '<span class="min_cur_tag">$</span>'));
                    }
                });
                $('em').each(function (i) {
                    var m = $(this).attr('cur_' + code);
                    if (m) {
                        $(this).html(m.replace('$', '<span class="min_cur_tag">$</span>'));
                    }
                });
                $('del').each(function (i) {
                    var m = $(this).attr('cur_' + code);
                    if (m) {
                        $(this).html(m.replace('$', '<span class="min_cur_tag">$</span>'));
                    }
                })
            },
            //初始化
            init: function () {
                var that = this;
                this.bind();
                $(function () {
                    /*					<!--//取出当前存在cookie的货币代码-->
					<!--var code = $.cookie('currency');-->
					<!--//对货币类型做判断-->
					<!--if (code == undefined || code=='') {-->
						<!--var code = 'usd';-->
						<!--that.edit(code.toLowerCase());-->
					<!--} else {-->
						<!--that.edit(code.toLowerCase());-->
						<!--//将货币和对应的code放入数组-->
						<!--var a = new Array();-->
						<!--$('#topCurrencyBox ul li a').each(function(i){-->
							<!--a[$(this).attr('cur')] = $(this).text();-->
						<!--})-->
						<!--$('#defCurrency').text(a[code.toLowerCase()]);-->
					<!--}-->*/
                })
            }
        };
        curPrice.init();
    },

    /**
     * 兼容IE9及以下不支持placeholder的功能
     * @param {object} config {inputEl:绑定的输入框,extra_default_tootip:额外的默认提示}
     */
    inputDefaultTip: function (config) {
        function _inputDefaultTip (config) {
            config.inputEl = config.inputEl ? config.inputEl : null;
            if (config.inputEl == null) {
                alert("inputDefaultTip配置参数中inputEl不能为空！");
                return;
            }
            this.extra_dt = null;
            if (typeof config.extra_default_tootip == "string") {
                this.extra_dt = config.extra_default_tootip;
            }
            this.el = (typeof config.inputEl == "string" ? $(config.inputEl) : config.inputEl);
            this.browserType = "";
            this.browserVersion = 11;
            this.checkBrowser();
            if (this.browserType == "IE" && this.browserVersion <= 9) {
                this.init();
                this.bind();
            }
        }
        _inputDefaultTip.prototype.checkBrowser = function () {
            if (navigator.userAgent.indexOf("MSIE") > 0) {
                if (navigator.userAgent.indexOf("MSIE 6.0") > 0) {
                    this.browserType = "IE";
                    this.browserVersion = 6;
                } else if (navigator.userAgent.indexOf("MSIE 7.0") > 0) {
                    this.browserType = "IE";
                    this.browserVersion = 7;
                } else if (navigator.userAgent.indexOf("MSIE 8.0") > 0) {
                    this.browserType = "IE";
                    this.browserVersion = 8;
                } else if (navigator.userAgent.indexOf("MSIE 9.0") > 0) {
                    this.browserType = "IE";
                    this.browserVersion = 9;
                } else if (navigator.userAgent.indexOf("MSIE 10.0") > 0) {
                    this.browserType = "IE";
                    this.browserVersion = 10;
                }
            }
        };
        _inputDefaultTip.prototype.init = function () {
            if (this.el.attr("placeholder") != "") {
                this.el.val(this.el.attr("placeholder"));
            }
        };
        _inputDefaultTip.prototype.bind = function () {
            var that = this;
            this.el.focus(function (e) {
                e = e || event;
                e.stopPropagation();
                if (that.el.val() == that.el.attr("placeholder")) {
                    that.el.val("");
                }
                if (that.extra_dt != null && that.el.val() == that.extra_dt) {
                    that.el.val("");
                }
            });
            this.el.blur(function (e) {
                e = e || event;
                e.stopPropagation();
                var val = that.el.val();
                var reg = /^\s*|\s*$/;
                val = val.replace(reg, '');
                if (val == "") {
                    that.el.val(that.el.attr("placeholder"));
                }
            });
        };
        return new _inputDefaultTip(config);
    }

    /*这里添加此文档的新方法(fucntion)或变量var...................................................................................................................*/
};
var G = new global();

/**
 * url 处理对象
 * @param {string} url 要处理的网址
 * @constructor
 * @author LWK
 */
function Url (url) {
    this.url = url || window.location.href;
}
Url.prototype = {
    /**
     * 取得网址的GET参数，返回数组，与PHP的相同
     * @return {Array|false}
     */
    getParame: function () {
        var string = this.url.replace(/.+\?/, '').replace(/\#+.{0,}/, '') || null;
        return this.parse(string);
    },
    /**
     * 取得#号后面的锚点参数，返回字符串
     * @return {array|false}
     */
    getAnchor: function () {
        var string = this.url.split("#")[1] || null;
        return string;
    },
    parse: function (string) {
        if (string != null) {
            var Parames = [];
            var strs = string.split('&');
            for (var i = 0; i < strs.length; i++) {
                Parames[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
                //alert(Parames[strs[i].split("=")[0]]);
            }
            return Parames;
        }
        return false;
    },
    /**
     * @method getPageAddress 取得页面地址
     */
    getPageAddress: function () {
        var pAddress = this.url.toString();
        var array = pAddress.split('?');
        return array[0];
    },
    /**
     * @method clearDomain 清除域名
     */
    clearDomain: function () {
        if (this.url.indexOf('http') > -1) {
            return '/' + this.url.replace(/https?:\/\/.*\//, '');
        }
        return this.url;
    }
};
var URL = new Url();

/* tooltip提示信息 end } */

/**
 * 修复搜索框未输入内容,搜索为where bug
 * param {dom对象}
 */

function clearInput (elem) {
    $input = $(elem).parent().find(".hdsearchWords");
    if ($input.val() == 'where to?') {
        $input.val('');
    }
}

/* tooltip提示信息 start { */
$(function () {
    $("body").append('<div id="tooltip-wrap" class="tooltip-default"><span id="tooltip-arrow"></span><div id="tooltip-con"></div></div>');
    var i = $("#tooltip-wrap");     //box父节点
    var c = $("#tooltip-con");      //要显示的内容
    var j = $("#tooltip-arrow");    //上三角符号
    var g = j.width();              //上三角的   宽
    var b;
    var h = $(window);              //window对象
    var d = i.outerWidth();         //box父节点  宽
    var f = (d - g) / 2;            //父 / 三   /2 宽
    j.css({                         //设置三角 left位置
        left: f
    });
    var e = function () {           //隐藏 box父节点
        i.fadeOut()
    };
    var conWidth;
    $(".tooltip").on("mouseenter",
      function () {
          clearTimeout(b);
          var o = a(this);
          var direction = o.hasClass('top') ? 'top' : 'bottom';
          var n = o.outerHeight();    //触发 元素 高
          var r = o.outerWidth();     //触发 元素 宽
          var u = 0;
          var m = o.attr("tooltip");  //要显示的txt文本
          var t = o.find(".tooltipCon");  //将要放文本的 盒子
          if (!m) {
              if (t.text() == "") {
                  return
              }
          }
          if (m == "") {
              return
          }
          if (!o.attr("tooltip")) {
              c.html("");
              t.clone().show().appendTo(c);
          } else {
              c.html(m)
          }

          var s = o.offset().left;                    //触发元素 left值

          if (direction == 'top') {
              var q = o.offset().top + (-75) + n;
          } else {
              var q = o.offset().top + 5 + n;
              j.css('top', '-8px');
              j.css('backgroundImage', 'url(/usiadmin/images/icons/tip_top.png)');
          }

          i.fadeIn().css({
              top: q, zIndex: 120
          });

          if (i.css("display") == "block" && conWidth === undefined) {
              conWidth = i.width();
          }

          var p = h.width();              //window 宽
          var l = s - (d - r) / 2;        //触发left  -  （hover元素 - 触发元素）/2
          if (d + l > p) {         //p  window宽
              var k = d + l - p;
              i.css({             //hover 元素
                  left: l - k
              });

              j.css({             //三角形
                  left: f + k + u
              })
          } else {
              if (l < 0) {
                  if ($(document).width() - s > conWidth) {
                      i.css({         //hover 元素
                          left: 0
                      });
                      j.css({         //三角形
                          left: f + l + u
                      });
                  } else {
                      i.css({         //hover 元素
                          left: -conWidth + 1.65 * g
                      });
                      j.css({         //三角形
                          left: f + l + u + conWidth - 1.65 * g
                      });
                  }
              } else {
                  if ($(document).width() - s > conWidth) {
                      i.css({         //hover 元素
                          left: l
                      });
                      j.css({         //三角形
                          left: f + u
                      });
                  } else {
                      i.css({         //hover 元素
                          left: l - conWidth + 1.65 * g
                      });
                      j.css({         //三角形
                          left: f + u + conWidth - 1.65 * g
                      });
                  }
              }
          }
      }).on("mouseleave",
      function () {
          b = setTimeout(e, 300)
      });

    /*i.hover(function() {
   clearTimeout(b)
   },
   function() {
   b = setTimeout(e, 300)
   })*/

    /**
     * Created by 509 on 2016/3/10.
     */
    // if (window.location.href.indexOf("usitour") > -1) {
    //     $('.if-en-hide').css('display', 'none');
    //     $('.if-en-hide').css('background-color', 'transparent');
    //     $('.en_language').hide();
    // }
});

/**
 * 公用头+尾JS效果 start
 */
$(function () {
    /**
     * topbar 悬停效果
     */
    var public_topbar = {
        //顶部li
        menu_list_Li: $(".en_menu-list li"),
        menu_list_Li_Lang: $(".en_language"),
        //顶部li 里面的 dd
        menu_list_Dd: $(".en_menu-list dl dd"),
        menu_list_Dd_Lang: $(".pngFix")
    };

    public_topbar.menu_list_Li.hover(function () {
        var _this = $(this);
        _this.find("dl").fadeIn(100).parents("li").siblings().find("dl").fadeOut(100);
        _this.find(".title").removeClass("color_fff").addClass("color_fe8426").parents("li").siblings().find(".title").removeClass("color_fe8426");
    }, function () {

        var _this = $(this);
        _this.find("dl").fadeOut(100);
        _this.find(".title").addClass("color_fff");
    });

    public_topbar.menu_list_Li_Lang.hover(function () {
        $(".selected-language").css("background-color", '#003e82');
    }, function () {
        $(".selected-language").css("background-color", '#ffff');
    });
    public_topbar.menu_list_Dd.hover(function () {
        $(this).find("a").addClass("color_fe8426");
    }, function () {
        $(this).find("a").removeClass("color_fe8426");
    });

    public_topbar.menu_list_Dd_Lang.hover(function () {
        $(".selected-language").css("background-color", '#003e82');
    }, function () {
        $(".selected-language").css("background-color", '#ffff');
    });

    /**
     * 导航
     */
    var public_nav = {
        //搜索
        search_index: $("#en_nav .JS_searchIndex"),
        search_box: $("#en_nav .en_search_index"),
        hot_close: $("#en_nav .search_hot_list").find(".close_hot"),  //热门搜索关闭
        //导航
        list_group: $("#en_nav .en_list_group")
    };
    //  鼠标移入 搜索图标 line 1380
    public_nav.search_index.mouseenter(function () {
        $(this).addClass("active").siblings('input').css({visibility: "visible"}).stop(true).animate({width: "290px"}, 200);
    });

    var g_allow_send = 1;
    jQuery(document).ready(function ($) {
        G.hideMessage();
        /* 顶部搜索框 */
        var type_id = "";

        function sendData (url, limit, val, tar) {
            if (g_allow_send == 0) {
                return;
            }
            g_allow_send = 0;
            url = G.url_rand(url);
            url += '&ajax=true&val=' + val + '&type=Travel&language_id=1&limit=' + limit + '&callback=?';
            $('.en_hdAutocomplete').hide();
            $.getJSON(url, function (data) {
                g_allow_send = 1;
                //返回json，根据json的值来处理快速搜索菜单的数据
                if (0) {
                    //老的数据返回(暂时隐藏掉)
                    if (data.rows.length > 0) {
                        var _ul = tar.parents('.hdsearchIpt').find('.hdAutocomplete');
                        _ul.empty();
                        _sn = -1;
                        $('.hdAutocomplete').hide();
                        for (var i = 0; i < data.rows.length; i++) {
                            $('.hdAutocomplete').hide();
                            _ul.append('<li class="t_s_list">' + data.rows[i].text + '</li>').show();
                        }
                    } else {
                        $('.hdAutocomplete').hide();
                    }
                } else {

                    if (data.rows.length > 0) {
                        var _ul = tar.parents('.en_search_index').find('.en_hdAutocomplete');
                        _ul.empty();
                        _sn = -1;
                        $('.en_hdAutocomplete').hide();
                        for (var i = 0; i < data.rows.length; i++) {
                            if (data.rows[i].sub_data) {
                                for (var j = 0; j < data.rows[i].sub_data.length; j++) {
                                    $('.en_hdAutocomplete').hide();
                                    _ul.append('<li class="t_s_list" searchTid="' + data.rows[i].sub_data[j].type_id + '">' + '<span>' + data.rows[i].text + '</span><span class="category">' + data.rows[i].sub_data[j].text + '</span><span class="num">' + data.rows[i].sub_data[j].count +
                                      ' Tours</span></li>').show();
                                }
                            }
                        }
                        _ul.find('li').on('click', function () {
                            //_ul.parent().find('.hdsearchWords').val($(this).find('span').eq(0).text());
                            //_ul.find('.hdsearchSubmit').click();
                        });

                    } else {
                        $('.en_hdAutocomplete').hide();
                    }
                }
            });
        }

        var _url = $('.hdsearchWords').attr('dataurl');
        //var _url = $('.hdsearchWords').attr('dataurl').replace("en.lmy-trunk.com","post.usitrip.com");

        var _sn = -1;
        var _sid = $("#j-typeId");

        $(document).click(function (event) {
            var e = event || window.event;
            var obj = e.srcElement || e.target;
            if (obj.className != 'hdsearchWords' && obj.className !== 't_s_list') {
                $(obj).find('.hdAutocomplete').hide();
            }
            if (obj.className != 'search_hot_list' && obj.className != 'hdsearchWords') {

                $(".en_search_hot_list").hide();
            }
        });
        $('.close_hot').click(function () {
            $(this).parents(".JS_searchList").find(".en_search_hot_list").hide();
        });
        G.updateCurrency();
    });
    //  hot_close
    public_nav.hot_close.click(function () {
        $(this).parents(".en_search_hot_list").hide();
    });

    //  阻止冒泡
    public_nav.search_box.find(".hdsearchWords").click(function (e) {
        var oEvent = e || event;
        oEvent.stopPropagation();
        if ($("#m-banner")) {
            $("#m-banner .en_search_hot_list").hide();
        }
    });

    $(document).click(function () {
        public_nav.search_index.removeClass("active");
        public_nav.search_box.find("input").animate({width: "10px"}, 200, function () {$(this).css({visibility: "hidden"});});

        public_nav.search_box.find(".en_hdAutocomplete").hide();//    搜索结果
        public_nav.search_box.find(".en_search_hot_list").hide();// 热门列表
        $(".en_hdAutocomplete").hide();
        // public_nav.search_box.find("input").val("");
        $(".js_language_currency").find("dl").hide();   //底部 语言/货币关闭
    });

    //  导航
    public_nav.list_group.hover(function () {
        $(this).addClass("color_e77621").find(".list").fadeIn(100).parents(".en_list_group").siblings().removeClass("color_e77621").find(".list").fadeOut(100);
    }, function () {
        $(this).removeClass("color_e77621").find(".list").fadeOut(100);
    });

    //  关闭搜索列表
    $("#en_nav .close_hot").on("click", function () {
        $(this).parents(".en_search_hot_list").hide();
    });

    /**
     * 底部效果 语言+货币
     */
    var public_footer = {
        oUl: $(".js_language_currency")
    };
    public_footer.oUl.find("li").click(function (e) {
        var oEvent = e || event;
        oEvent.stopPropagation();
        var _selBottom = document.documentElement.clientHeight + $(document).scrollTop() - $(this).offset().top - 40;
        var _dlHeight = $(this).find("dl").height();
        if (_dlHeight > _selBottom) {
            $(this).find("dl").css({
                top: -(_dlHeight + 42) + 'px'
            })
        }
        $(this).find("dl").show();
        $(this).siblings("li").find("dl").hide();
    });

    /* 公用头+尾JS效果 end*/

    /**
     * method getLoginStatus 获取登录状态
     * @param url
     */
    function getLoginStatus (url) {
        if (url.indexOf("?") > 0) {
            url += "&randnumforajaxaction=" + Math.random();
        } else {
            url += "?randnumforajaxaction=" + Math.random();
        }
        //以下必须用GET的方式处理，如果用POST的话有些旧的浏览器不能解决跨域问题
        $.ajax({
            type: 'GET', url: url, data: {'action': 'loginAndShoppingCartCheckForTop', 'previousURL': URL.url}, dataType: 'jsonp',
            success: function (data) {
                if (typeof(data) == 'object' && data['status'] == 1) {
                    var html = '';
                    var returnContent = data['html'];
                    if (returnContent['logStatus']) { //登录成功
                        $("#js_myusi_list").css({"width": "130px"});
                        html += '<input id="JS_UsiCId" type="hidden" value="' + returnContent['customersId'] + '">';
                        html += '<dd class="alignLeft"><a href="' + returnContent['myOrdersUrl'] + '" rel="nofollow" >My Orders</a></dd>';
                        //我的收藏
                        html += '<dd class="alignLeft myWishList"><a href="#" rel="nofollow" >My Wish List</a></dd>';
                        //个人信息
                        html += '<dd class="alignLeft"><a href="' + returnContent['myProfileUrl'] + '" rel="nofollow" >Edit Profile</a></dd>';
                        html += '<dd class="alignLeft"><a href="' + returnContent['logOffUrl'] + '" rel="nofollow">Sign Out</a></dd>';
                    } else {
                        $("#js_myusi_list").css({"width": "190px"});
                        html += '<dd><a href="https://post.usitour.com/index.php?mod=login&language_code=utf-8" rel="nofollow" class="js_myusi_color">Sign In</a></dd>';
                        html += '<dd><a id="signUpBtn" href="https://post.usitour.com/index.php?mod=create_account&language_code=utf-8" rel="nofollow">Create an Account</a></dd>';
                    }
                    $('#js_myusi_list').html(html);

                    //为我的收藏赋值 href
                    $("#js_myusi_list .myWishList").find("a").attr("href", $("#my_favorite_href").attr("dataUrl"));
                } else {
                    alert('程序异常getLoginStatus！');
                }
            }
        });
    }
    getLoginStatus('//post.usitour.com/ajax.php?mod=login');

    /**
     * 更换货币
     */
    function updateCurrency () {
        /**
         * 修改显示货币
         */
        var curPrice = {
            //绑定事件
            bind: function () {
                var that = this;
                $('.js_currencies a').click(function () {
                    var self = $(this),
                      text = self.text();
                    var cur = self.attr('cur');
                    if (cur) {
                        //把货币代码存入cookie【是大写的】
                        var v = cur.toUpperCase();
                        $.removeCookie('currency', {'domain': document.domain, 'path': '/'});
                        $.cookie('currency', v, {'domain': document.domain, 'path': '/'});
                        window.location.reload(false);
                    }
                });
            },
            //修改价格
            edit: function (code) {
                //做循环取标签里面的值写入标签
                $('.js_product_special_price').each(function (i) {
                    var m = $(this).attr('cur_' + code);
                    if (m) {
                        $(this).text(m);
                    }
                });
                $('em').each(function (i) {
                    var m = $(this).attr('cur_' + code);
                    if (m) {
                        $(this).text(m);
                    }
                });
                $('del').each(function (i) {
                    var m = $(this).attr('cur_' + code);
                    if (m) {
                        $(this).text(m);
                    }
                });
            },
            //初始化
            init: function () {
                var that = this;
                this.bind();
                /*				$(function() {
					<!--//取出当前存在cookie的货币代码-->
					<!--var code = $.cookie('currency');-->
					<!--//对货币类型做判断-->
					<!--if (code == undefined || code=='') {-->
						<!--var code = 'usd';-->
						<!--that.edit(code.toLowerCase());-->
					<!--} else {-->
						<!--that.edit(code.toLowerCase());-->
						<!--$('.js_currencies a').each(function(i){-->
							<!--if(typeof $(this).attr('cur') != 'undefined') {-->
								<!--if(code.toLowerCase()==$(this).attr('cur')){-->
									<!--$('.js_defCurrency').text($(this).text());-->
									<!--$(this).parent('dd').remove();-->
									<!--return;-->
								<!--}-->
							<!--}-->
						<!--})-->
					<!--}-->
				})*/
            }
        };
        curPrice.init();
    }
    updateCurrency();

    /**
     * 针对七牛服务器破图检测
     * @param options
     * 调用 ： $('img.broken').qiniuImgCheck();
     */
    $.fn.qiniuImgCheck = function () {
        this.each(function (index, obj) {
            var tmp_src, src, data_src;
            var that = $(obj);
            src = that.attr('src');
            data_src = that.attr('data-original');  //懒加载url
            //空的img标签
            if (src === "" && (data_src == undefined || data_src === "")) { return true; }
            //非空img or 懒加载图片
            if (src != undefined || data_src != undefined) {
                tmp_src = src;
                if (that.hasClass('lazy')) {
                    tmp_src = data_src;
                }
                if (tmp_src && tmp_src.indexOf('qiniu.') != -1) {
                    $.ajax({
                        url: tmp_src,
                        success: function () { /*加载成功*/ },
                        error: function (data) {
                            if (data.status == 404 || data.statusText == "Not Found") {
                                //删除域名
                                tmp_src = tmp_src.replace(/^((http:\/\/)|(https:\/\/))?([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}/, '');
                                if (that.hasClass('lazy')) {
                                    that.attr('data-original', tmp_src);
                                }
                                //1.若src=""懒加载插件会取data-original处理src值；
                                if (that.attr('src')) {
                                    that.attr('src', tmp_src);
                                }
                            }
                        }
                    });
                }
            }
        })
    };
});

/**
 * 公共class效果控制
 * @author LGJ
 */
(function ($) {
    $(function () {
        /**
         * class => .space-trim
         * 清除首尾空格（包含全角转半角）
         */
        $('.space-trim').on('blur', function () {
            //先进行全角转半角处理，再进行剔除首尾空格
            if (Usitrip.String) {
                $(this).val(Usitrip.String.trim(Usitrip.String.ToCDB($(this).val())));
            }
        })
    });
})($);
/*! Sea.js 2.3.0 | seajs.org/LICENSE.md */
!function(a,b){function c(a){return function(b){return{}.toString.call(b)=="[object "+a+"]"}}function d(){return z++}function e(a){return a.match(C)[0]}function f(a){for(a=a.replace(D,"/"),a=a.replace(F,"$1/");a.match(E);)a=a.replace(E,"/");return a}function g(a){var b=a.length-1,c=a.charAt(b);return"#"===c?a.substring(0,b):".js"===a.substring(b-2)||a.indexOf("?")>0||"/"===c?a:a+".js"}function h(a){var b=u.alias;return b&&w(b[a])?b[a]:a}function i(a){var b=u.paths,c;return b&&(c=a.match(G))&&w(b[c[1]])&&(a=b[c[1]]+c[2]),a}function j(a){var b=u.vars;return b&&a.indexOf("{")>-1&&(a=a.replace(H,function(a,c){return w(b[c])?b[c]:a})),a}function k(a){var b=u.map,c=a;if(b)for(var d=0,e=b.length;e>d;d++){var f=b[d];if(c=y(f)?f(a)||a:a.replace(f[0],f[1]),c!==a)break}return c}function l(a,b){var c,d=a.charAt(0);if(I.test(a))c=a;else if("."===d)c=f((b?e(b):u.cwd)+a);else if("/"===d){var g=u.cwd.match(J);c=g?g[0]+a.substring(1):a}else c=u.base+a;return 0===c.indexOf("//")&&(c=location.protocol+c),c}function m(a,b){if(!a)return"";a=h(a),a=i(a),a=j(a),a=g(a);var c=l(a,b);return c=k(c)}function n(a){return a.hasAttribute?a.src:a.getAttribute("src",4)}function o(a,b,c){var d=K.createElement("script");if(c){var e=y(c)?c(a):c;e&&(d.charset=e)}p(d,b,a),d.async=!0,d.src=a,R=d,Q?P.insertBefore(d,Q):P.appendChild(d),R=null}function p(a,b,c){function d(){a.onload=a.onerror=a.onreadystatechange=null,u.debug||P.removeChild(a),a=null,b()}var e="onload"in a;e?(a.onload=d,a.onerror=function(){B("error",{uri:c,node:a}),d()}):a.onreadystatechange=function(){/loaded|complete/.test(a.readyState)&&d()}}function q(){if(R)return R;if(S&&"interactive"===S.readyState)return S;for(var a=P.getElementsByTagName("script"),b=a.length-1;b>=0;b--){var c=a[b];if("interactive"===c.readyState)return S=c}}function r(a){var b=[];return a.replace(U,"").replace(T,function(a,c,d){d&&b.push(d)}),b}function s(a,b){this.uri=a,this.dependencies=b||[],this.exports=null,this.status=0,this._waitings={},this._remain=0}if(!a.seajs){var t=a.seajs={version:"2.3.0"},u=t.data={},v=c("Object"),w=c("String"),x=Array.isArray||c("Array"),y=c("Function"),z=0,A=u.events={};t.on=function(a,b){var c=A[a]||(A[a]=[]);return c.push(b),t},t.off=function(a,b){if(!a&&!b)return A=u.events={},t;var c=A[a];if(c)if(b)for(var d=c.length-1;d>=0;d--)c[d]===b&&c.splice(d,1);else delete A[a];return t};var B=t.emit=function(a,b){var c=A[a],d;if(c){c=c.slice();for(var e=0,f=c.length;f>e;e++)c[e](b)}return t},C=/[^?#]*\//,D=/\/\.\//g,E=/\/[^/]+\/\.\.\//,F=/([^:/])\/+\//g,G=/^([^/:]+)(\/.+)$/,H=/{([^{]+)}/g,I=/^\/\/.|:\//,J=/^.*?\/\/.*?\//,K=document,L=location.href&&0!==location.href.indexOf("about:")?e(location.href):"",M=K.scripts,N=K.getElementById("seajsnode")||M[M.length-1],O=e(n(N)||L);t.resolve=m;var P=K.head||K.getElementsByTagName("head")[0]||K.documentElement,Q=P.getElementsByTagName("base")[0],R,S;t.request=o;var T=/"(?:\\"|[^"])*"|'(?:\\'|[^'])*'|\/\*[\S\s]*?\*\/|\/(?:\\\/|[^\/\r\n])+\/(?=[^\/])|\/\/.*|\.\s*require|(?:^|[^$])\brequire\s*\(\s*(["'])(.+?)\1\s*\)/g,U=/\\\\/g,V=t.cache={},W,X={},Y={},Z={},$=s.STATUS={FETCHING:1,SAVED:2,LOADING:3,LOADED:4,EXECUTING:5,EXECUTED:6};s.prototype.resolve=function(){for(var a=this,b=a.dependencies,c=[],d=0,e=b.length;e>d;d++)c[d]=s.resolve(b[d],a.uri);return c},s.prototype.load=function(){var a=this;if(!(a.status>=$.LOADING)){a.status=$.LOADING;var c=a.resolve();B("load",c);for(var d=a._remain=c.length,e,f=0;d>f;f++)e=s.get(c[f]),e.status<$.LOADED?e._waitings[a.uri]=(e._waitings[a.uri]||0)+1:a._remain--;if(0===a._remain)return a.onload(),b;var g={};for(f=0;d>f;f++)e=V[c[f]],e.status<$.FETCHING?e.fetch(g):e.status===$.SAVED&&e.load();for(var h in g)g.hasOwnProperty(h)&&g[h]()}},s.prototype.onload=function(){var a=this;a.status=$.LOADED,a.callback&&a.callback();var b=a._waitings,c,d;for(c in b)b.hasOwnProperty(c)&&(d=V[c],d._remain-=b[c],0===d._remain&&d.onload());delete a._waitings,delete a._remain},s.prototype.fetch=function(a){function c(){t.request(g.requestUri,g.onRequest,g.charset)}function d(){delete X[h],Y[h]=!0,W&&(s.save(f,W),W=null);var a,b=Z[h];for(delete Z[h];a=b.shift();)a.load()}var e=this,f=e.uri;e.status=$.FETCHING;var g={uri:f};B("fetch",g);var h=g.requestUri||f;return!h||Y[h]?(e.load(),b):X[h]?(Z[h].push(e),b):(X[h]=!0,Z[h]=[e],B("request",g={uri:f,requestUri:h,onRequest:d,charset:u.charset}),g.requested||(a?a[g.requestUri]=c:c()),b)},s.prototype.exec=function(){function a(b){return s.get(a.resolve(b)).exec()}var c=this;if(c.status>=$.EXECUTING)return c.exports;c.status=$.EXECUTING;var e=c.uri;a.resolve=function(a){return s.resolve(a,e)},a.async=function(b,c){return s.use(b,c,e+"_async_"+d()),a};var f=c.factory,g=y(f)?f(a,c.exports={},c):f;return g===b&&(g=c.exports),delete c.factory,c.exports=g,c.status=$.EXECUTED,B("exec",c),g},s.resolve=function(a,b){var c={id:a,refUri:b};return B("resolve",c),c.uri||t.resolve(c.id,b)},s.define=function(a,c,d){var e=arguments.length;1===e?(d=a,a=b):2===e&&(d=c,x(a)?(c=a,a=b):c=b),!x(c)&&y(d)&&(c=r(""+d));var f={id:a,uri:s.resolve(a),deps:c,factory:d};if(!f.uri&&K.attachEvent){var g=q();g&&(f.uri=g.src)}B("define",f),f.uri?s.save(f.uri,f):W=f},s.save=function(a,b){var c=s.get(a);c.status<$.SAVED&&(c.id=b.id||a,c.dependencies=b.deps||[],c.factory=b.factory,c.status=$.SAVED,B("save",c))},s.get=function(a,b){return V[a]||(V[a]=new s(a,b))},s.use=function(b,c,d){var e=s.get(d,x(b)?b:[b]);e.callback=function(){for(var b=[],d=e.resolve(),f=0,g=d.length;g>f;f++)b[f]=V[d[f]].exec();c&&c.apply(a,b),delete e.callback},e.load()},t.use=function(a,b){return s.use(a,b,u.cwd+"_use_"+d()),t},s.define.cmd={},a.define=s.define,t.Module=s,u.fetchedList=Y,u.cid=d,t.require=function(a){var b=s.get(s.resolve(a));return b.status<$.EXECUTING&&(b.onload(),b.exec()),b.exports},u.base=O,u.dir=O,u.cwd=L,u.charset="utf-8",t.config=function(a){for(var b in a){var c=a[b],d=u[b];if(d&&v(d))for(var e in c)d[e]=c[e];else x(d)?c=d.concat(c):"base"===b&&("/"!==c.slice(-1)&&(c+="/"),c=l(c)),u[b]=c}return B("config",a),t}}}(this);
