var hotelId = hotel.hotelId + '';// int to string
var citconRequest = null;
var poxhr = null;
var checkRequest = null;
var roomRef = '';
var vendorType = '';
var reference = '';
var bookForm = $('#book-form');
var totalPrice = 0;
var appliedAmount = 0;

if(isUsitour){
  $('.exchange-rate-warning').show();
}

$('input[type=radio][name=imgsel]').change(function() {
  if (this.value === 'pb' || this.value === 'cc') {
    $('.exchange-rate-warning').show();
  }else{
    $('.exchange-rate-warning').hide();
  }
});

window.saveEmailInfo = function() {
  $('#booker_email').val( $('#booker_email').val().toLowerCase().replace(' ',''));
  var email =  $('#booker_email').val();
  $.post("/saveEmailInfo", {
    '_token': $('meta[name="csrf_token"]').attr('content'),
    'email': email,
  }, function(data) {
  });
};

window.buildRating = function(rating, category) {
    var r = Number(rating);
    if (r == NaN) {
        return category;
    }
    if (r >= 5) {
        r = 5;
    }
    var h = '';
    for (var i = 0; i < r; i++) {
        h += '<i class="fa fa-star"></i>';
    }
    return h;
};

window.savePBInfo = function(data, type) {
    $.post("/paypal/button", {
        '_token': $('meta[name="csrf_token"]').attr('content'),
        'data': data,
        'type': type,
    }, function(res) {
        if (res.payId !== '') {
            if (document.getElementById("payId") === null) {
                bookForm.append('<input type="hidden" id="payId" name="payId"  value="' + res.payId + '"/>');
            }
        }
        if (type === 'response') {
            bookRooms();
        }
    });
};

window.doRoomCheck = function() {
    var rf = [];
    var status = room.status;

    for (var i = 0; i < roomCount; i++) {
        rf[i] = {'roomReference': roomRef};
    }

    $('.ladda-button-book').ladda().ladda('start');
    $.post('/rooms/check', {
        '_token': $('meta[name="csrf_token"]').attr('content'),
        'references': JSON.stringify(rf),
        'roomCount': roomCount,
        'dayCount': dayCount,
        'displayId': displayId,
    }, function(data) {
        if ('type' in data && data['type'] === 'SESSION_TIMEOUT') {
            document.getElementById('bt').disabled = true;
            $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + lang_login_expired + '</p>');

            swal({
                title: lang_login_expired,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: lang_login,
            }, function() {
                window.location.href = hotelPath;
            });
        } else if (data.errorId === 'IT1003') {
            document.getElementById('bt').disabled = true;
            $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + langBook.networkErr + '</p>');

            swal({
                title: langBook.networkErr,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'OK',
            }, function() {
                window.location.href = hotelPath;
            });
        } else if (data.errorId === 'ER1401' || data.errorId === 'ER1301') {
            document.getElementById('bt').disabled = true;
            $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + langBook.soldout + '</p>');

            swal({
                title: langBook.soldout,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'OK',
            }, function() {
                window.location.href = hotelPath;
            });
        } else if (data.errorId === 'ER1413') {
            document.getElementById('bt').disabled = true;
            $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + langBook.limit + '</p>');

            swal({
                title: langBook.limit,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'OK',
            }, function() {
                window.location.href = hotelPath;
            });
        } else if (data.errorId === 'ER1411' || data.errorId === 'ER1402') {
            document.getElementById('bt').disabled = true;
            $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + langBook.pchange + '</p>');

            swal({
                title: langBook.pchange,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'OK',
            }, function() {
                window.location.href = hotelPath;
            });
        } else if (data.errorId === 'ER1004') {
            document.getElementById('bt').disabled = true;
            $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + langBook.timeout + '</p>');

            swal({
                title: langBook.timeout,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'OK',
            }, function() {
                window.location.href = hotelPath;
            });
        } else if (data.errorId === 'ER1501') {
            document.getElementById('bt').disabled = true;
            $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + langBook.soldout + '</p>');

            swal({
                title: langBook.soldout,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'OK',
            }, function() {
                window.location.href = hotelPath;
            });
        } else {
            roomCount = data.length;
            var html_comment = 'None';
            var cp = langBook['non-ref'];
            //房间已定完
            if (roomCount === 0) {
                cp = langBook.soldout;
                swal({
                    title: langBook.soldout,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'OK',
                }, function() {
                    window.location.href = hotelPath;
                });
            } else {
                room = data[0];

                // cancellation policy detect
                if (room && room['cancellationPolicies'] && room['cancellationPolicies'][0]) {
                    var cancel_amount = room['cancellationPolicies'][0]['amount'];
                    if (cancel_amount === 0) {
                        var cancel_date = room['cancellationPolicies'][0]['end'];
                        cp = langBook.freeb + " " + cancel_date.substr(0, 19).replace(/T/g, ' ') + langBook.ltime + "<br>";
                    }
                }
                // pass roomReference to form input
                document.getElementById('roomRef').value = room.roomReference;

                // pass room comments
                if (room && room.comments && room.comments[0] && room.comments[0].comments && room.comments[0].comments !== '') {
                    html_comment = room.comments[0].comments;
                }

                // use search room's mandatory fees - Ann 051418
                if (room && (room.totalMandatoryFees && room.totalMandatoryFees !== '0.0') || (room.mandatoryFeesDetails && room.mandatoryFeesDetails[0] && room.mandatoryFeesDetails[0]['amount'] && room.mandatoryFeesDetails[0]['amount'] !== '0.0')) {
                    var currency = room.currency;
                    if (currency === 'USD') {
                        currency = '$';
                    }
                    if (room && room.totalMandatoryFees) {
                        $('#HotelDue').removeClass('hidden');
                        $('.HotelDue').text(currency + room.totalMandatoryFees);
                        $('#total_mandatory_fees').val(room.totalMandatoryFees);  //如果book拿到mandatoryfee的值，写去order_temp数据库
                    } else {
                        $('#HotelDue').removeClass('hidden');
                        $('.HotelDue').text(currency + room.mandatoryFeesDetails[0]['amount']);
                        $('#total_mandatory_fees').val(room.mandatoryFeesDetails[0]['amount']);  //如果book拿到mandatoryfee的值，写去order_temp数据库
                    }
                }

                //pass final due today price to html
                if (room.netPrice) {
                    temp = parseInt(roomCount);
                    temp2 = room.netPrice;
                    temp1 = parseInt(temp2 / temp);
                    var res = parseInt(temp1 * temp);
                    if (room.beforeTax) {
                        $('.roomPrice').text(room.beforeTax / temp / dayCount);
                    }
                    $('#roomTax').text(room.tax);
                    $('.totalPrice').text(res);
                    $('#book-form').append('<input type="hidden" name="totalPrice"  value="' + res + '"/>');
                    if (room.cleanFee !== 0) {
                        $('#cleanFee').removeClass('hidden');
                        $('.clean-fee').text('$' + room.cleanFee);
                        $('#cleanFee').append('<br/>');
                    }
                } else {
                    temp = parseInt(roomCount);
                    temp1 = singleRoomNightPrice;
                    var res = temp1 * temp;
                    res = parseInt(res);
                    $('.roomPrice').text(temp1);
                    $('.totalPrice').text(res);
                    $('#book-form').append('<input type="hidden" name="totalPrice"  value="' + res + '"/>');
                }

                totalPrice = res;
                $('.ladda-button-book').ladda().ladda('stop');
                $('#ppBtn').attr('disabled',false);
            }

            //pass hotel cancel policy and hotel comment to html
            $('#hotel-cancel').html(cp);
            $('#hotel-comment').html(html_comment);
            // 得到最终结果update右下方区块高度
            updateHeight();
        }
    });
}

// 得到最终结果update右下方区块高度
window.updateHeight = function() {
    var subHeight = 94;
    if(is117book){
      subHeight = 24;
    }
    var scrollHeight = $('.book-info-box').height() - $('.coupon-block').height() - $('.hotel-detail-top').height() - subHeight;
    $('.note-and-why-us-box').css('height', scrollHeight);
    if (($('#hotel-comment').height() + $('.hotel-detail-top').height()) < scrollHeight) {
        $('.note-and-why-us-box').css('overflow-y', 'hidden');
    } else {
        $('.note-and-why-us-box').css('overflow-y', 'auto');
    }
};

window.calc = function() {
    if (document.getElementById('cb').checked === true) {
        document.getElementById('bt').disabled = false;
    } else {
        document.getElementById('bt').disabled = true;
    }
};

window.applyCoupon = function() {
    var code = $('#codeInput').val().trim();
    if (code) {
        $.post('/coupon/apply', {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'code': code,
            'hotelId': hotelId,
            'checkin': checkin
        }, function(data) {
            if (data.success && !isNaN(data.amount)) {
                $('.error-message').hide();
                $('.code-input').removeClass('error');
                $('#couponAmount').html('-$' + data.amount);
                appliedAmount = data.amount;
                $('.applied-coupon-block').removeClass('hide');
                $('.totalPrice').text(totalPrice - data.amount);
                $('#couponCode').val(code);
                updateHeight();
            } else {
                if (data.message) {
                    $('.error-message').html(data.message);
                }
                $('.error-message').show();
                if (!$('.code-input').hasClass('error')) {
                    $('.code-input').addClass('error');
                }
                updateHeight();
            }

        });
    }
};

window.removeCoupon = function() {
    $.get('/coupon/remove', function(data) {
        if (data.success) {
            appliedAmount = 0;
            $('.applied-coupon-block').addClass('hide');
            $('.totalPrice').text(totalPrice);
            $('#couponCode').val('');
            updateHeight();
        }
    });
};

window.bookRooms = function() {
    vendorType = 'po';
    var radio = document.getElementsByName('imgsel');
    for (i = 0; i < radio.length; i++) {
        if (radio[i].checked) {
            vendorType = radio[i].value;
        }
    }

    bookForm.append('<input type="hidden" name="tempType"  value="' + vendorType + '"/>');

    var v = bookForm.valid();

    if (!v) {
        swal(langBook.plzcomplete);
        updateHeight();
        return;
    } else {
        //change check box to img to avoid second payment - 03132018
        $('#paymentAuth').html('<img class="validPic" src="/img/general/icon/valid.png">');
    }

    if (vendorType !== 'pb') {
        preventClick();
    }

    var anaData = {
        'id': hotelId,
        'name': hotel.name,
        'variant': room.name,
        'brand': room.roomReference.substr(0, 1),
        'price': room.netPrice,
        'qty': roomCount,
        'vendor': vendorType,
    };

    onInfoComplete(anaData);

    var names = new Array();
    var i, j;
    for (i = 1; i <= roomCount; i++) {
        names.push($('#guest' + i + '_a1_ln').val() + $('#guest' + i + '_a1_fn').val());
    }

    var samename = false;
    loopouter:
        for (i = 0; i < names.length - 1; i++) {
            for (j = i + 1; j < names.length; j++) {
                if (names[i] === names[j]) {
                    samename = true;
                    break loopouter;
                }
            }
        }

    if (samename && vendorType !== 'pb') {
        swal({
                title: '',
                text: langBook.samename,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: lang_cancel_yes,
                cancelButtonText: lang_cancel_no,
            },
            function(isConfirm) {
                if (isConfirm) {
                    bookRoomsGo(); //确认相同姓名还要下单
                } else {
                    ableClick(); //返回相同姓名更改再下单
                }
            });
    } else {
        bookRoomsGo();
    }
};

window.bookRoomsGo = function() {
    var url = '/reserve/' + sessionKey + '?rooms_key=' + rooms_key;
    $.post(url, bookForm.serialize(), function(data) {
        if (data.success && (vendorType === 'we' || vendorType === 'al')) {
            var payMethod = 'wechat';
            if (vendorType === 'al') {
                payMethod = 'alipay';
            }
            if (data === '') {
                swal({
                    title: langBook.bookSuccessEmailLater,
                    type: 'success',
                    confirmButtonColor: '#DD6B55',
                    allowOutsideClick: false,
                    confirmButtonText: 'OK'
                }, function () {
                    window.location.href = hotelPath;
                });
            } else {
                $('#qr_img').attr('src', data.voucherUrl);

                if (language === 0) {
                    if (vendorType === 'al') {
                        payMethod = '支付宝';
                    } else {
                        payMethod = '微信';
                    }
                }
                $('#paymentType').text(payMethod);

                var qrModal = $('#qr_modal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                qrModal.modal('show');

                if (data.hasOwnProperty('reference')) {
                    reference = data.reference;

                    var getUrl = "/ccpay/inquire";
                    var timesRun = 0;
                    citconRequest = setInterval(function () {
                        //先这样
                        if (timesRun === 10) {
                            $.post('/payment/issue/email', {
                                '_token': $('meta[name="csrf_token"]').attr('content'),
                                'Reorderid': reference,
                                'vendor': vendorType
                            });
                        }

                        if (timesRun === 36) {
                            clearInterval(citconRequest);
                            qrModal.modal('hide');
                            swal({
                                title: langBook.session_back_hotel,
                                type: 'warning',
                                allowOutsideClick: false,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: lang_back_to_search
                            }, function () {
                                window.location.href = hotelPath;
                            });
                        }

                        $.get(getUrl, {
                            'reference': reference
                        }, function (inquireData) {
                            if (inquireData.type === 'charge' && inquireData.status === 'success') {
                                clearInterval(citconRequest);

                                if (inquireData.hasOwnProperty('options') && inquireData.options.length > 0) {
                                    var hotelData = {
                                        'id': inquireData.options.hotelId,
                                        'name': inquireData.options.hotelName
                                    };
                                    onPaymentComplete(hotelData);
                                }

                                var loadingHtml =
                                  '<div id="HotelLoadingPicture" class="sk-spinner sk-spinner-three-bounce" style="height: 300px;padding-top: 120px;">' +
                                  '<div class="sk-bounce1"></div>' +
                                  '<div class="sk-bounce2"></div>' +
                                  '<div class="sk-bounce3"></div>' +
                                  '</div>';
                                $('#qr_img').replaceWith(loadingHtml);

                                if (!poxhr) {
                                    poxhr = $.post('/ccpay/order', {
                                        '_token': $('meta[name="csrf_token"]').attr('content'),
                                        'Reorderid': reference,
                                        'vendor': vendorType
                                    }, function (data) {
                                        checkOrderStatus(data);
                                    });
                                }
                            }
                        });

                        timesRun++;
                    }, 5000);
                }
            }
        } else if (data.voucherUrl) {
            self.location = data.voucherUrl;
        } else {
            checkOrderStatus(data);
        }
    });
};

window.checkOrderStatus = function(data) {
    if (data.success && data.voucherUrl) {
        window.location.replace(data.voucherUrl);
    } else if (data.message === 'wait') {
        var timesCalled = 0;
        checkRequest = setInterval(function() {
            if (timesCalled === 6) {
                clearInterval(checkRequest);
            }
            $.getJSON("/checkOrder/" + reference, function(data) {
                if (data.success) {
                    window.location.href = data.voucherUrl;
                }
            });
            timesCalled++;
        }, 5000);
    } else if (data.errorId === 'IT1004' ) {
        swal({
            title: langBook.bookSuccessEmailLater,
            type: 'success',
            confirmButtonColor: '#DD6B55',
            allowOutsideClick: false,
            confirmButtonText: 'OK'
        }, function () {
            window.location.href = hotelPath;
        });
    } else if (data.errorId === 'ER1401' || data.errorId === 'ER1301') {
        document.getElementById('bt').disabled = true;
        $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + langBook.soldout + '</p>');
        swal({
            title: langBook.soldout,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            allowOutsideClick: false,
            confirmButtonText: lang_back_to_search,
        }, function() {
            window.location.href = hotelPath;
        });
    } else if (data.errorId === 'ER1413') {
        swal(langBook.limit, data.message, 'error');
    } else if (data.errorId === 'ER1411' || data.errorId === 'ER1402' || data.errorId === 'ER1302') {
        document.getElementById('bt').disabled = true;
        $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + langBook.pchange + '</p>');
        swal({
            title: langBook.pchange,
            type: 'warning',
            allowOutsideClick: false,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: lang_back_to_search,
        }, function() {
            window.location.href = '/';
        });
    } else if (data.errorId === 'ER1501') {
        document.getElementById('bt').disabled = true;
        $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + langBook.soldout + '</p>');
        swal({
            title: langBook.soldout,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            allowOutsideClick: false,
            confirmButtonText: lang_back_to_search,
        }, function() {
            window.location.href = hotelPath;
        });
    } else if (data.errorId === 'ER1414') {
        var errMsg =  this.langBook.duplicate.replace('xxx', data.errorMessage);
        document.getElementById('bt').disabled = true;
        $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + errMsg + '</p>');
        swal({
            title:'',
            text: errMsg,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            allowOutsideClick: false,
            confirmButtonText: lang_back_to_search,
        }, function() {
            window.location.href = hotelPath;
        });
    } else if (data.errorId === 'ER1004') {
        document.getElementById('bt').disabled = true;
        $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + langBook.timeout + '</p>');
        swal({
            title: langBook.timeout,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            allowOutsideClick: false,
            confirmButtonText: lang_back_to_search,
        }, function() {
            window.location.href = hotelPath;
        });
    } else if (data.errorId === 'IT1001') {
        document.getElementById('bt').disabled = true;
        $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + langBook.cnetwork + '</p>');
        swal({
            title: langBook.cnetwork,
            type: 'warning',
            text: data.message,
            allowOutsideClick: false,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: lang_back_to_search,
        }, function() {
            window.location.href = hotelPath;
        });
    } else if (data.errorId === 'ER1415' && reference) { // for duplicate order check
        $('#qr_modal').modal('hide');
        swal({
            title: langBook.confirmDu,
            type: 'warning',
            confirmButtonText: langBook.confirm,
            showCancelButton: true,
            cancelButtonText: langBook.cancel,
        }, function(confirmed) {
            if (confirmed) {
                $('#qr_modal').modal('show');
                $.post('/duplicate', {
                    '_token': $('meta[name="csrf_token"]').attr('content'),
                    'action': 'continue',
                    'Reorderid': reference,
                    'vendor': vendorType,
                }, function(data) {
                    checkOrderStatus(data);
                });
            } else {
                $.post('/duplicate', {
                    '_token': $('meta[name="csrf_token"]').attr('content'),
                    'action': 'cancel',
                    'Reorderid': reference,
                    'vendor': vendorType,
                }, function(data) {
                    if (!data.success) {
                        data['message'] = langBook.cfailed;
                    }
                    swal({
                        title: data.message,
                        type: 'warning',
                        allowOutsideClick: false,
                        confirmButtonText: 'OK',
                    }, function() {
                        window.location.href = hotelPath;
                    });
                });
            }
        });
    } else {
        document.getElementById('bt').disabled = true;
        $('#bt').after('</br><br/><br/><p class="usitrip-hover-tag">' + langBook.cfailed + '</p>');
        swal({
            title: langBook.cfailed,
            text: data.message,
            type: 'error',
            allowOutsideClick: false,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: lang_back_to_search,
        }, function() {
            window.location.href = hotelPath;
        });
    }
};

window.preventClick = function() {
    $('.ladda-button-book').ladda().ladda('start');
    $('#openProcessingModal').click();
    document.getElementById("bt").disabled = true;
};

window.ableClick = function() {
    $('.ladda-button-book').ladda().ladda('stop');
    $('#closeProcessingModal').click();
    document.getElementById("bt").disabled = false;
};

$(function() {
    // 这一段是给booker-phone-country这个component用的，数据很多，如果使用的话需将之前的select和other废弃
    $('.area-box').click(function(){
        $('.area-wrap').css('display','block');
    });
    $('.area-wrap').find('dd').each(function(){
        // 国家名称中文
        $(this).find('span[lang=zh]').html($(this).find('span[lang=zh]').attr('zh'));

        if(language === 0){
            $(this).find('span[lang=zh]').css('display','block');
            $(this).find('span[lang=en]').css('display','none');
        }
        $(this).click(function(){
            if($(this).hasClass('addOther')){
                $('.area-box').html('<input id="otherCode" name="otherCode" class="form-control" placeholder="+XXX" type="text"/>');
            }else{
                var selectedAreaCode = $(this).find('em').html();
                var selectedFlag = $(this).find('i').css("background-position");
                $(this).addClass('highlight').siblings().removeClass('highlight');
                $('.area-box').html('<input name="phone_country" id="phone_country" readonly="" class="form-control" type="text" value="'+selectedAreaCode+'" placeholder="">' +
                  '<span class="country-with-flag"><i class="flag" style="background-position:'+selectedFlag+'"></i><i class="fa fa-angle-down"></i></span>');
            }
            $('.area-wrap').css('display','none');
        });
    });
    //B端客户提交给酒店的特殊需求
    var collapsedSizep = '5px';
    $('.specialrequest').each(function() {
        var x = this.scrollHeight;
        var div = $(this);
        if (x > 10) {
            div.css('height', collapsedSizep);
            div.after('<a style="padding:25px 0 25px 15px; outline:none;" id="seemore" class="specialrequest text-left"  onclick=this.outerHTML="" href="#">' + langBook.addrequestoptional + '</a>');
            var link = div.next();
            link.click(function(p) {
                p.stopPropagation();
                if (link.text() !== '') {
                    link.text('');
                    div.animate({
                        'height': x,
                    });
                }
            });
        }
    });

    // 更新高度
    $('#seemore').click(function() {
        $('.note-and-why-us-box').css('height', $('.note-and-why-us-box').height() + 160);
    });

    var div_rating = $('#hotel-rating');
    div_rating.html(buildRating(hotel.rating, hotel.category));
    div_rating.fadeIn();

    $('#booker_local_time').val(new Date());

    source = U_distributor.getValue('source');
    $('#source').val(source);

    if (room !== null) {
        roomRef = room.roomRef;
        roomCount = roomCount;
    }

    $('.ladda-button-book').prop('disabled', true);
    doRoomCheck();
    $('.ladda-button-book').prop('disabled', false);

    $('#weChatCloseBtn').click(function() {
        ableClick();
    });

    $.validator.addMethod('accept', function(value, element, param) {
        return value.match(new RegExp('.' + param + '$')) || value === '';
    }, langBook.englishonly);

    $.validator.addMethod('phone', function(value, element) {
        // allow only number
        return this.optional(element) || /^[0-9]+$/.test(value);
    }, langBook.numberOnly);

    $('#qr_modal').on('hide.bs.modal', function() {
        clearInterval(citconRequest);
        $('.ladda-button-book').ladda().ladda('stop');
    });

    // for usitour, remove wechatpay and alipay from pmt method, neighter american nor taiwanness use them
    if (isUsitour) {
        $('#aliPaymentLabel').hide();
        $('#wechatPaymentLabel').hide();
    }

    $('#guest1_a1_fn').change(function() {
        var booker_firstname = $('#guest1_a1_fn').val();
        $('#booker_fn').val(booker_firstname);
    });

    $('#guest1_a1_ln').change(function() {
        var booker_lastname = $('#guest1_a1_ln').val();
        $('#booker_ln').val(booker_lastname);
    });

    $('#otherCode').change(function () {
        $('.area-wrap').css('display','none');
    });

    $('#booker_phone').on('click', function() {
        // 收起国家code选项
        $('.area-wrap').css('display','none');
        var otherCode = $('#otherCode').val();
        //other country code in additional to provided
        if (otherCode !== '' && otherCode !== undefined) {
            $('.area-box').html('<input name="phone_country" id="phone_country" readonly="" class="form-control" type="text" value="'+otherCode+'" placeholder="">' +
              '<span class="country-with-flag"><i class="fa fa-question-circle"></i><i class="fa fa-angle-down"></i></span>');
            $('.area-col-dl').append('<dd class="added highlight"><i class="fa fa-question-circle"></i><span lang="zh" zh="" style="display: block;"></span><span lang="en" en="" style="display: none;"></span><em>+ '+otherCode+'</em></dd>');
            $('#phone_country').val(otherCode);
            if ($('#countryCodePhone').val() !== '') {
                $('#otherCode').remove();
            }
        }
    });

    $('#ppBtn').click(function() {
        var v = bookForm.valid();
        if ($(this).is(':checked')) {
            if (!v) {
                swal(langBook.plzcomplete);
                document.getElementById("ccpay").checked = true;
            } else {
                $('#paymentAuth').html('<img class="validPic" src="/img/general/icon/valid.png">');
                $('#paypal-button').show();
                $('#bt').hide();
            }
            updateHeight();
        }
    });

    $('#ccpay').click(function() {
        $('#paypal-button').hide();
        $('#bt').show();
    });

    $('#wechatpay').click(function() {
        $('#paypal-button').hide();
        $('#bt').show();
    });

    $('#alipay').click(function() {
        $('#paypal-button').hide();
        $('#bt').show();
    });

    var rules = {
        booker_ln: {
            required: true,
            minlength: 2,
        },
        booker_fn: {
            required: true,
            minlength: 2,
        },
        booker_phone: {
            required: true,
            minlength: 6,
            phone: true,
        },
        booker_email: {
            required: true,
            email: true,
        },
        specialrequest: {
            maxlength: 100,
            accept: '[a-zA-Z]+',
        },
    };

    for (var i = 1; i <= roomCount; i++) {
        rules['guest' + i + '_a1_ln'] = {
            required: true,
            minlength: 2
        };
        rules['guest' + i + '_a1_fn'] = {
            required: true,
            minlength: 1
        }
    }

    bookForm.validate({
        debug: true,
        rules: rules
    });

    var transactions = [{
        amount: {
            total: room.netPrice + room.cleanFee - appliedAmount,
            currency: roomCurrency,
            details: {
                subtotal: room.beforeTax + room.cleanFee - appliedAmount,
                tax: room.tax,
            }
        },
        item_list: {
            items: [
                {
                    name: hotel.name,
                    description: hotel.address + ', ' + hotel.city + ', ' + hotel.zipcode,
                    quantity: roomCount * dayCount,
                    price: singleRoomNightPriceB4Tax,
                    tax: singleRoomNightTax,
                    sku: 'H' + hotelId, //Item #:
                    currency: roomCurrency
                },
            ],
        }
    }];

    if (appliedAmount !== 0) {
        transactions[0]['item_list']['items'].push(
          {
              name: 'Coupon',
              price: -appliedAmount,
              currency: "USD",
              quantity: 1,
              tax: 0
          });
    }

    if (room.cleanFee !== 0) {
        transactions[0]['item_list']['items'].push(
          {
              name: 'Clean Fee',
              price: room.cleanFee,
              currency: "USD",
              quantity: 1,
              tax: 0
          });
    }
    /*
    paypal.Button.render({
        // Configure environment
        env: app_env, // sandbox | production

        client: {
            sandbox: app_pp_btn,
            production: app_pp_btn
        },
        // Customize button (optional)
        locale: locale,  //paypal button shown language
        style: {
            label: 'pay', // buynow | paypal | pay | checkout | credit
            size: 'medium', // small | medium | large | responsive
            shape: 'rect',   // pill | rect
            color: 'gold',  // gold | blue | silver | black
            tagline: false, // optional :The safer, easier way to pay
        },
        // Set up a payment
        payment: function(data, actions) {
            return actions.payment.create({
                experience: {
                    input_fields: {
                        no_shipping: 1  //no shipping needed
                    }
                },
                payment: {
                    transactions: transactions,
                    note_to_payer: checkin + ' - ' + checkout + ', ' + roomCount + ' room(s)'
                },
            });
        },
        onAuthorize: function(data, actions) {
            // save transaction request to payment
            savePBInfo(JSON.stringify(data), 'request');
            return actions.payment.execute().then(function(res) {
                // check for ERROR CODE=INSTRUMENT_DECLINED and restart
                if (res.error === 'INSTRUMENT_DECLINED') {
                    return actions.restart();
                } else {
                    // save transaction response to payment
                    preventClick();
                    savePBInfo(JSON.stringify(res), 'response');
                }
            });
        },
        onError: function(err) {
            // Show an error page here, when an error occurs
            swal({
                title: "",
                text: paypalError + err,
                type: 'error',
                allowOutsideClick: false,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: "OK",
            }, function() {
                window.location.href = hotelPath;
            });
        }
    }, '#paypal-button');
    */
});
