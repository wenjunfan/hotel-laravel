var app = new Vue({
    el: '#hotelUpdateApp',
    data: {
        hotelOriginal: hotel,
        hotel: hotel,
        imgRoot: 'https://usitrip.s3.us-west-1.amazonaws.com/Extra_Image/',
        selectedImage: '',
        globalSave: true,
        is_new: is_new,
        tab: tab,
        replaceImg: '',
        isLoading: false,
        toDelete: [],
    },
    mounted() {
        if (this.is_new == 1) {
            this.imgRoot = this.imgRoot.replace('Extra_Image/', 'Hotel_Image/');
        }
        //sticky header
        let $header = $('#infoController');
        let origin = $header.offset().top;
        $(window).scroll(function() {
            if (window.pageYOffset > origin) {
                $('body').addClass('sticky-header');
                $('.scroll-top').show(200)
            } else {
                $('body').removeClass('sticky-header');
                $('.scroll-top').hide(200);
            }
        });
        //form input validation
        $('#hotelDetailForm, #hotelAreaForm, #hotelFacilityForm, #hotelRestaurantForm').validate({
            focusCleanup: true
        });
        //activate tab indicated in url
        if (this.tab) {
            $('.nav-tabs li, .hotel-info form').removeClass('active');
            $('#' + tab).addClass('active');
            $('#' + tab + 'Form').addClass('active');
        } else {
            $('#hotelDetail, #hotelDetailForm').addClass('active');
        }
    },
    methods: {
        openModal(number, name) {
            this.activeForm = number;
            this.modalTitle = name;
            this.modalDescription = modalDescriptionArr[number - 1];
            $('#wikiModal').modal('show')
        },
        openTab(id, globalSave, e) {
            $('body').css('visibility', 'hidden');
            $('.hotel-info-toggler li').removeClass('active');
            $(e.target).parent().addClass('active');
            $('.hotel-info form').removeClass('active');
            $('#' + id + 'Form').addClass('active');
            //save whole page or not
            this.globalSave = globalSave;
            $('html, body').animate({ scrollTop: 0 }, 'fast', function () {
                $('body').css('visibility', 'visible');
            });
        },
        toggleCollapse(e) {
            let $groupElement = $(e.target).closest('.collapse-parent');
            $groupElement.toggleClass('collapsed');
            if ($groupElement.hasClass('collapsed')) {
                $(e.target).text('Show more');
            } else {
                $(e.target).text('Show less');
            }
        },
        doSaveForm() {
            let self = this;
            if ($('.form-horizontal.active').valid()) {
                swal({
                    title: '确定保存当前数据吗？',
                    showCancelButton: true,
                    confirmButtonColor: '#e4393c',
                    confirmButtonText: 'Yes'
                }, function (val) {
                    if (val) {
                        self.saveForm();
                    }
                })
            }
        },
        saveForm() {
            let $form = $('.form-horizontal.active'),
                url = $form.attr('action'),
                attr = $form.attr('data-attr'),
                action = '',
                form = null;

            let data = {
                'hotelId': [this.hotel.hotelId],
                'is_new': this.is_new
            };

            if (attr === 'detail') {
                form = $('#hotelDetailForm');
                let data_arr = form.serializeArray();
                for (let i = 0; i < data_arr.length; i++) {
                    data[data_arr[i]['name']] = data_arr[i]['value'];
                }
                action = 'detail';
            } else if (attr === 'area') {
                form = $('#hotelAreaForm');
                data['hotel_pois'] = this.hotel.hotel_area;
                data['hotel_pois_zh'] = this.hotel.hotel_area_zh;
                action = 'area';
            } else if (attr === 'facility') {
                form = $('#hotelFacilityForm');
                data['hotel_facility'] = this.hotel.hotel_facility;
                data['hotel_facility_zh'] = this.hotel.hotel_facility_zh;
                action = 'facility';
            } else if (attr === 'restaurant') {
                form = $('#hotelRestaurantForm');
                data['hotel_restaurant'] = this.hotel.hotel_restaurant.filter(function (item) {
                    return item.name;
                });
                data['hotel_restaurant_zh'] = this.hotel.hotel_restaurant_zh.filter(function (item) {
                    return item.name;
                });
                action = 'restaurant';
            } else if (attr === 'thumbnail') {
                form = $('#hotelThumbnailForm');
                data['thumbnail'] = (this.is_new == 1 ? 'Hotel_Image/' : 'Extra_Image/') + this.selectedImage;
                action = 'thumbnail';
            }

            if (form && form.valid()) {
                $.post('/hotel/update/info', {
                    '_token': $('meta[name="csrf_token"]').attr('content'),
                    'action': action,
                    'data': data
                }, function (data) {
                    if (data.success) {
                        swal({
                            title: '更新成功'
                        }, function () {
                            window.location.reload();
                        });
                    }
                })
            }
        },
        addAttr(attr, lang) {
            if (attr === 'hotel_area') {
                this.$set(this.hotel, 'hotel_area', []);
                this.$set(this.hotel, 'hotel_area_zh', []);
                if (lang) {
                    this.addAreaGroup(lang, null);
                }
            } else if (attr === 'hotel_facility') {
                this.$set(this.hotel, 'hotel_facility', {
                    popular: [],
                    all: []
                });
                this.$set(this.hotel, 'hotel_facility_zh', {
                    popular: [],
                    all: []
                });
                if (lang) {
                    this.addFacilityPopular(lang);
                    this.addFacilityGroup(lang, null);
                }
            } else if (attr === 'hotel_reviews') {
                this.$set(this.hotel, 'hotel_reviews', []);
                this.$set(this.hotel, 'hotel_reviews_zh', []);
                if (lang) {
                    this.addReview(lang, null);
                }
            } else if (attr === 'hotel_restaurant') {
                this.$set(this.hotel, 'hotel_restaurant', []);
                this.$set(this.hotel, 'hotel_restaurant_zh', []);
                if (lang) {
                    this.addRestaurant(lang, null);
                }
            } else if (attr === 'hotel_cards') {
                this.$set(this.hotel, 'hotel_cards_accepted', {
                    cards: [''],
                    description: '',
                    description_zh: ''
                })
            }
        },
        addAreaGroup(lang, e) {
            let areaGroup = {
                sortName: '',
                pois: [
                    {
                        distance: '0 miles',
                        name: ''
                    }
                ]
            };
            if (lang === 'en') {
                this.hotel.hotel_area.push(areaGroup);
            } else {
                this.hotel.hotel_area_zh.push(areaGroup);
            }
            if (e) {
                setTimeout(function () {
                    let $el = $(e.target).parent().find('.area-group:last-child');
                    $('html, body').animate({
                        scrollTop: $el.offset().top - 80
                    }, 'slow')
                }, 300);
            }
        },
        removeAreaGroup(lang, index) {
            if (lang === 'en') {
                this.hotel.hotel_area.splice(index, 1);
            } else {
                this.hotel.hotel_area_zh.splice(index, 1);
            }
        },
        addAreaItem(lang, index, e) {
            $(e.target).closest('.collapse-parent').removeClass('collapsed');
            let areaItem = {
                distance: 0,
                name: ''
            };
            if (lang === 'en') {
                this.hotel.hotel_area[index].pois.push(areaItem);
            } else {
                this.hotel.hotel_area_zh[index].pois.push(areaItem);
            }
        },
        removeAreaItem(lang, index, line) {
            if (lang === 'en') {
                this.hotel.hotel_area[index].pois.splice(line, 1);
            } else {
                this.hotel.hotel_area_zh[index].pois.splice(line, 1);
            }
        },
        addFacilityPopular(lang) {
            let facilityPopular = {
                facilityCode: '',
                facilityName: '',
                isNew: true
            };
            if (lang === 'en') {
                this.hotel.hotel_facility.popular.push(facilityPopular);
            } else {
                this.hotel.hotel_facility_zh.popular.push(facilityPopular);
            }
        },
        removeFacilityPopular(lang, index) {
            if (lang === 'en') {
                this.hotel.hotel_facility.popular.splice(index, 1);
            } else {
                this.hotel.hotel_facility_zh.popular.splice(index, 1);
            }
        },
        addFacilityGroup(lang, e) {
            let facilityGroup = {
                code: '',
                sortName: '',
                facilities: []
            };
            if (lang === 'en') {
                this.hotel.hotel_facility.all.push(facilityGroup);
            } else {
                this.hotel.hotel_facility_zh.all.push(facilityGroup);
            }
            if (e) {
                setTimeout(function () {
                    let $el = $(e.target).parent().find('.hotel-facility-group:last-child');
                    $('html, body').animate({
                        scrollTop: $el.offset().top - 80
                    }, 'slow')
                }, 300);
            }
        },
        removeFacilityGroup(lang, index) {
            if (lang === 'en') {
                this.hotel.hotel_facility.all.splice(index, 1);
            } else {
                this.hotel.hotel_facility_zh.all.splice(index, 1);
            }
        },
        addFacilityItem(lang, index, e) {
            $(e.target).closest('.collapse-parent').removeClass('collapsed');
            if (lang === 'en') {
                this.hotel.hotel_facility.all[index].facilities.push(null)
            } else {
                this.hotel.hotel_facility_zh.all[index].facilities.push(null)
            }
        },
        removeFacilityItem(lang, index, line) {
            if (lang === 'en') {
                this.hotel.hotel_facility.all[index].facilities.splice(line, 1);
            } else {
                this.hotel.hotel_facility_zh.all[index].facilities.splice(line, 1);
            }
        },
        //maybe useful in the future
        updateFacilityKey(lang, key, e) {
            let newKey = $(e.target).val();
            let facilities = [];
            if (lang === 'en') {
                facilities = this.hotel.hotel_facility.all[key];
                this.$delete(this.hotel.hotel_facility.all, key);
                this.$set(this.hotel.hotel_facility.all, newKey, facilities);
            } else {
                facilities = this.hotel.hotel_facility.all[key];
                this.$delete(this.hotel.hotel_facility_zh.all, key);
                this.$set(this.hotel.hotel_facility_zh.all, newKey, facilities);
            }
        },
        addReview(lang, e) {
            let review = {
                id: null,
                content: '',
                country: '',
                name: '',
                rate: 0,
                review_date: ''
            };
            if (lang === 'en') {
                this.hotel.hotel_reviews.push(review);
            } else {
                this.hotel.hotel_reviews_zh.push(review)
            }
            if (e) {
                setTimeout(function () {
                    let $el = $(e.target).parent().find('.hotel-review-item:last-child');
                    $('html, body').animate({
                        scrollTop: $el.offset().top - 80
                    }, 'slow')
                }, 300);
            }
        },
        removeReview(lang, index) {
            let self = this;
            let review = null;
            if (lang === 'en') {
                review = this.hotel.hotel_reviews[index];
            } else {
                review = this.hotel.hotel_reviews_zh[index];
            }
            if (review && review['id']) {
                swal({
                    title: '确定删除该评论吗？',
                    showCancelButton: true,
                    confirmButtonColor: '#4c8ecb'
                }, function (val) {
                    if (val) {
                        self.updateReview(lang, 'delete', index);
                    }
                })
            } else {
                if (lang === 'en') {
                    this.hotel.hotel_reviews.splice(index, 1);
                } else {
                    this.hotel.hotel_reviews_zh.splice(index, 1);
                }
            }
        },
        updateReview(lang, action, index) {
            let review = null;
            if (lang === 'en') {
                review = this.hotel.hotel_reviews[index];
            } else {
                review = this.hotel.hotel_review_zh[index];
            }

            if (review['name']) {
                review['language'] = (lang === 'cn') ? 1 : 2;
                $.post('/hotel/update/reviews', {
                    '_token': $('meta[name="csrf_token"]').attr('content'),
                    'action': action,
                    'hotelId': this.hotel.hotelId,
                    'review': review
                }, function (data) {
                    if (data.success) {
                        let message = '更新成功';
                        if (action === 'add') {
                            message = '新增成功';
                        } else if (action === 'delete') {
                            message = '删除成功'
                        }
                        swal({
                            title: message
                        }, function () {
                            window.location.reload();
                        })
                    } else {
                        swal('操作失败');
                    }
                })
            }
        },
        addRestaurant(lang, e) {
            let restaurant = {
                food: '',
                menu: '',
                name: '',
                open_for: ''
            };
            if (lang === 'en') {
                this.hotel.hotel_restaurant.push(restaurant);
            } else {
                this.hotel.hotel_restaurant_zh.push(restaurant);
            }
            if (e) {
                setTimeout(function () {
                    let $el = $(e.target).parent().find('.hotel-restaurant-item:last-child');
                    $('html, body').animate({
                        scrollTop: $el.offset().top - 80
                    }, 'slow')
                }, 300);
            }
        },
        removeRestaurant(lang, index) {
            if (lang === 'en') {
                this.hotel.hotel_restaurant.splice(index, 1);
            } else {
                this.hotel.hotel_restaurant_zh.splice(index, 1);
            }
        },
        updateHotelThumb(image) {
            this.selectedImage = image.url ? image.url : image;
            return false;
        },
        addCardType() {
            this.hotel.hotel_cards_accepted.cards.push('')
        },
        removeCardType(index) {
            this.hotel.hotel_cards_accepted.cards.splice(index, 1);
        },
        scrollTop() {
            $('html, body').animate({
                scrollTop: 0
            }, 300);
        },
        manageImage(method, image = '') {
            let self = this;
            this.replaceImg = '';
            let url = image.url ? image.url : image;
            if (method === 'replace') {
                $('#imageModal').modal('show');
                this.replaceImg = url;
            } else if (method === 'add') {
                $('#imageModal').modal('show');
            } else {
                swal({
                    title: 'Are you sure you want to delete these images from this hotel?',
                    text: this.toDelete.join(', '),
                    showCancelButton: true
                }, function (val) {
                    if (val) {
                        self.deleteImage();
                    }
                })
            }
        },
        uploadImage() {
            var self = this;
            var fd = new FormData();
            var files = $('#imageModal input')[0].files;
            for(var i = 0; i < files.length; i++) {
                fd.append('file[]', files[i]);
            }
            fd.append('_token', $('meta[name="csrf_token"]').attr('content'));
            fd.append('replaceImg', this.replaceImg);
            fd.append('hotelId', this.hotel.hotelId);
            fd.append('productId', this.hotel.productId);
            fd.append('is_new', this.is_new);
            fd.append('method', this.replaceImg ? 'replace' : 'add');
            this.isLoading = true;

            $.ajax({
                url: '/hotel/update/image',
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.success) {
                        swal({
                            title: '更新成功!'
                        }, function (val) {
                            if (val) {
                                window.location.reload();
                            }
                        });
                    } else {
                        swal({
                            title: '更新失败',
                            text: response.errorMessage
                        });
                    }
                    self.isLoading = false;
                }
            })
        },
        deleteImage() {
            $.post('/hotel/update/image', {
                '_token': $('meta[name="csrf_token"]').attr('content'),
                'images': this.toDelete,
                'method': 'delete',
                'hotelId': this.hotel.hotelId,
                'productId': this.hotel.productId,
                'is_new': this.is_new
            }, function (response) {
                if (response.success) {
                    swal({
                        title: '删除成功!'
                    }, function (val) {
                        if (val) {
                            window.location.reload();
                        }
                    });
                } else {
                    swal({
                        title: '删除失败'
                    });
                }
            })
        }
    }
});