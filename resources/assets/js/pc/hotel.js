var sxhr,
  shxhr,
  srxhr;

var checkin = '', checkout = '', adultCount1 = 2, childCount1 = 0;
var price = [0, 1000000];
var maxCheckinText = language === '1' ? 'Check-in Date should in two years from now' : '入住日期需在两年内';
var noPriceAvail = language === 0 ? '请选择其他日期查看价格' : 'Change the date to see rooms available';
var passedpartnerId = $('#passedPartnerId').val();

// maps
var hotelMap = null;
var highestZIndex = 99;
var originIcon = null;
var selectedIcon = null;
var selectedMaker = null;
var mapBounds = null;
var mapInited = false;

var rooms_data = [];
var rooms_list = [];
var recSessionKey = '';
var rooms_key = '';

//搜索框初始化
window.initialization = function () {
    var html_adult = '';
    var html_child = '';
    var occ = searchInfo;

    if (occ === null) { //if search info session expired， set a default one
        occ = {
            'checkin': '2018-10-08',
            'checkout': '2018-10-09',
            'destinationId': hotelDesId,
            'occupancies': [
                {
                    'adults': 2,
                    'children': 0,
                    'paxes': [
                        {'type': 'AD'},
                        {'type': 'AD'}
                    ],
                    'rooms': 1
                }]
        };
    }

    checkin = occ['checkin'];
    checkout = occ['checkout'];
    roomCount = Number(occ['occupancies'][0]['rooms']);//初始化

    adultCount1 = Number(occ['occupancies'][0]['adults']);
    childCount1 = Number(occ['occupancies'][0]['children']);

    $('#checkin').val(checkin);
    $('#checkout').val(checkout);

    // 顶部初始化搜索条件显示
    $('#searched-date').html(checkin + ' ' + getDayOfWeek(checkin) + ' - ' + checkout + ' ' + getDayOfWeek(checkout));
    $('#searched-roomCount').text(roomCount);

    //给显示dialog赋值
    $('#tAdult').text(adultCount1);
    $('#tChild').text(childCount1);
    $('#tRoom').text(roomCount);

    //给disabled dialog赋值
    $('.adult-count-disable-in-dialog').val(adultCount1);
    $('.child-count-disable-in-dialog').val(childCount1);

    //给隐藏adult和child count赋值
    $('#adultCount1').val(adultCount1);
    $('#childCount1').val(childCount1);
    if (childCount1 > 0) {
        $('#childAgeWrapper').removeClass('hide');
        var content = $('#childAgeContent');
        var select_box = $('.child-age-select');
        select_box.val(childAges[0]);
        for (var i = 1; i < childCount1; i++) {
            var new_select = select_box.last().clone();
            new_select.val(childAges[i]);
            content.append(new_select);
        }
    }

    // 浮动的顶部搜索条件
    var html = checkin + ' ,  ' + getDayOfWeek(checkin) + ' - ' + checkout + ' ,  ' +
      getDayOfWeek(checkout) + ' , ' + dayCount + langHotel.night + ' , ' + roomCount + langHotel.room + ' , ' +
      adultCount1 + langHotel.adult + ' , ' + childCount1 + langHotel.children + ' ';
    $('#float-searched-info').html(html);

    initialDialog();
};

// 初始化显示数据
window.initialDialog = function () {
    // before click to show the dialog, detect if any of those btn is disabled
    var adultCount = $('#adultCount1').val();
    var childCount = $('#childCount1').val();
    //检测修改单复数单词
    if (is_usitour) {
        if (roomCount <= 1) {
            $('#tRoom_text').text('room, ');
        } else {
            $('#tRoom_text').text('rooms, ');
        }
        if (adultCount <= 1) {
            $('#tAdult_text').text('adult, ');
        } else {
            $('#tAdult_text').text('adults, ');
        }
        if (childCount <= 1) {
            $('#tChild_text').text('child, ');
        } else {
            $('#tChild_text').text('children, ');
        }
    }
    if (roomCount === 1) {
        $('#roomCountSubtract').addClass('disabled-btn');
        $('#roomCountSubtract').attr('disabled', true);
    }
    if (roomCount === 8) {
        $('#roomCountAdd').addClass('disabled-btn');
        $('#roomCountAdd').attr('disabled', true);
    }
    if (adultCount === 1) {
        $('#adultCountSubtract').addClass('disabled-btn');
        $('#adultCountSubtract').attr('disabled', true);
    }
    if (adultCount === 4) {
        $('#adultCountAdd').addClass('disabled-btn');
        $('#adultCountAdd').attr('disabled', true);
    }
    if (childCount === 0) {
        $('#childCountSubtract').addClass('disabled-btn');
        $('#childCountSubtract').attr('disabled', true);
    }
    if (childCount === 4) {
        $('#childCountAdd').addClass('disabled-btn');
        $('#childCountAdd').attr('disabled', true);
    }
};

// replace view all img to single one
window.replaceViewAllToSingle = function (url, index) {
    var single_img_html = '<div class="pull-left hotel-smaller-photo-gallery">' +
      '<div class="hotel-gallery">' +
      '<div class="hotel-modal-gallery-player">';

    single_img_html += '<a onclick="prev()" class="btn control-prev" onclick="return false;"><i' +
        ' class="fa' +
        ' fa-angle-left photo-btn-prev"></i></a>' +
      '<a onclick="next()"  class="btn control-next" onclick="return false;"><i class="fa' +
        ' fa-angle-right photo-btn-next"></i></a>';

    single_img_html += '<img alt="default hotel image" id="currentUrl" src="' + url + '">' +
      '<p id="indexUrl">' +
      (index + 1) +
      '/' +
      imgUrlArray.length +
      '</p>' +
      '</div>' +
      '</div>' +
      '</div>';

    if ($('#view-all-img-icon').hasClass('hidden')) {
        $('#view-all-img-icon').removeClass('hidden');
    }
    $('.modal-body-all-img').css('overflow-y', 'auto');

    var $allImgsWrapper = $('.modal-body-all-img .all-images');
    var $singleImgWrapper = $('.modal-body-all-img .single-image');
    $singleImgWrapper.html(single_img_html);
    $allImgsWrapper.addClass('hide');
    $singleImgWrapper.removeClass('hide');

};

// img prev and next for gallary
window.getCurrentImageIndex = function (url) {
    return imgUrlArray.indexOf(url);
};

window.prev = function () {
    //differentiate images from our data and other suppliers
    var index = document.getElementById("indexUrl").innerHTML;
    index = parseInt(index.split('/')[0]);
    $('#currentUrl').attr('src', imgUrlArray[index-2]['url']);
    $('#indexUrl').html((index-1) + '/' + imgUrlArray.length);
};

window.next = function () {
    var index = document.getElementById("indexUrl").innerHTML;
    index = parseInt(index.split('/')[0]);
    $('#currentUrl').attr('src', imgUrlArray[index]['url']);
    $('#indexUrl').html((index+1) + '/' + imgUrlArray.length);
};

window.backToAllImg = function () {
    var $allImgsWrapper = $('.modal-body-all-img .all-images');
    var $singleImgWrapper = $('.modal-body-all-img .single-image');
    $allImgsWrapper.removeClass('hide');
    $singleImgWrapper.addClass('hide');

    $('.modal-body-all-img').css('overflow-y', 'auto');
    if (!$('#view-all-img-icon').hasClass('hidden')) {
        $('#view-all-img-icon').addClass('hidden');
    }
};

window.getCookie = function (cname) {
    var name = cname + '=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return '';
};

//go back to list page and search the whole area again with same param
window.backList = function (id) {
  var current = '/hotel/' + hotelId;
  var target = '/list/D' + id;
  window.location.href =  window.location.href.replace(current,target);
};

window.updateCheckin = function () {
    var checkin = $('#checkin').val();
    var checkout = $('#checkout').val();
    var today = new Date();
    today = today.getFullYear() + '-' + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + '-' + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    var checkin1 = checkin.replace(/-/g, '');
    var today1 = today.replace(/-/g, '');
    if (checkin1 < today1) {
        $('#checkin').datepicker('setDate', today);
    }
    checkout1 = checkout.replace(/-/g, '');

    checkin = checkin.split('-');
    checkin = new Date(checkin[0], (checkin[1] - 1), checkin[2]);
    var n = 1;
    checkout = new Date(checkin - 0 + n * 86400000);
    checkout = checkout.getFullYear() + '-' + (checkout.getMonth() + 1 > 9 ? checkout.getMonth() + 1 : '0' + (checkout.getMonth() + 1)) + '-' + (checkout.getDate() < 10 ? '0' + checkout.getDate() : checkout.getDate());
    $('#checkout').datepicker('setDate', checkout);
    $('#checkout').datepicker('setStartDate', checkin);
    $('#checkout').datepicker('show');
};

window.updateCheckout = function () {
    var checkout = $('#checkout').val();
    var checkin = $('#checkin').val();
    var checkout1 = checkout.replace(/-/g, '');
    var checkin1 = checkin.replace(/-/g, '');
    if (checkout1 <= checkin1) {
        var dcheckin = new Date(checkin);
        var tomorrow = new Date(dcheckin.getTime() + 2 * (24 * 60 * 60 * 1000));
        var tomorrow1 = tomorrow.getFullYear() + '-' + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + '-' + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());
        $('#checkout').datepicker('setDate', tomorrow1);
    }

    today = new Date();
    tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));

    var today1 = today.getFullYear() + "" + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + "" + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
    checkin1 = checkin.replace(/-/g, '');
    checkout1 = checkout.replace(/-/g, '');

    if (parseInt(checkin1) >= parseInt(checkout1)) {
        if (parseInt(checkout1) === parseInt(today1)) {
            swal({
                title: '退房日期不能早于明天',
                type: 'warning'
            }, function () {
                setTimeout(function () {$('#checkin').focus();}, 1);
            });

            td = today.getFullYear() + '-' + (today.getMonth() + 1 > 9 ? today.getMonth() + 1 : '0' + (today.getMonth() + 1)) + '-' + (today.getDate() < 10 ? '0' + today.getDate() : today.getDate());
            tm = tomorrow.getFullYear() + '-' + (tomorrow.getMonth() + 1 > 9 ? tomorrow.getMonth() + 1 : '0' + (tomorrow.getMonth() + 1)) + '-' + (tomorrow.getDate() < 10 ? '0' + tomorrow.getDate() : tomorrow.getDate());

            $('#checkin').datepicker('setDate', td);
            $('#checkout').datepicker('setDate', tm);
        }
    }
};

//todo: delete
window.updatePlayer = function () {
    var player = $('.hotel-gallery-player');
    if ($.isEmptyObject(player)) {
        return;
    }
    var height = player.width() * 3 / 5;

    player.height(height);
    player.css('line-height', height + 'px');

    $('a.control-prev').css('top', (height - 32) / 2 + 'px');
    $('a.control-next').css('top', (height - 32) / 2 + 'px');
};

window.getDayOfWeek = function (date) {
    var dayOfWeek = new Date(date).getDay();
    if (isNaN(dayOfWeek)) {
        return null;
    }
    if (language === 0) {
        return ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天'][dayOfWeek];
    } else {
        return ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'][dayOfWeek];
    }
};

window.showRecommendHotels = function () {
    var checkin = $('#checkin').val();
    var checkout = $('#checkout').val();

    var l = $('.ladda-button-search').ladda();
    l.ladda('start');

    var loading = '<div class="sk-spinner sk-spinner-three-bounce">' + '<div class="sk-bounce1"></div>' +
      '<div class="sk-bounce2"></div>' + '<div class="sk-bounce3"></div>' + '</div>';

    $('#hotelsBlock').html(loading);

    price[2] = price[0] * roomCount;
    price[3] = price[1] * roomCount;
    price[4] = checkin;
    price[5] = checkout;

    shxhr = $.get('/hotel/recommend', $('#search-form').serialize(), function (data) {
        if (data.hotels.length > 0) {
            buildHotels(data);
        } else {
            $('#nearHotel').html('');
        }
    });
};

window.buildRating = function (rating, color) {
    var r = Number(rating);
    var h = '';
    if (r >= 5) {
        r = 5;
    }
    if (r !== 1 && r !== 2 && r !== 3 && r !== 4 && r !== 5 && r !== 6) {
        h = '<span class="usitrip-hover-tag">' + langHotel.noRate + '</span>';
        return h;
    }
    for (var i = 0; i < r; i++) {
        h += '<i class="fa fa-star ' + color + '"></i>';
    }
    return h;
};

window.buildHotels = function (data) {
    var hotels = data.hotels;
    var hotelsBlock = '';
    recSessionKey = data.sessionKey;

    for (var i = 0; i < hotels.length; i++) {
        var hotel = hotels[i];
        if (hotelMap) {
            addMapMarker(hotel, false);
        }
        var discountBlock = '';
        if (hotel.discount > 0) {
            discountBlock = '<i>' + Math.round(hotel.discount * 100) + '% off</i>';
        }
        if (hotel.currency === 'USD') {
            hotel.currency = '$';
        }

        var hotelLink = '/hotel/' + hotel.hotelId + '.html?s=' + recSessionKey + '&' + $('#search-form').serialize();

        hotelsBlock +=
          '<div class="col-sm-3">' +
          '<a href="'+ hotelLink +'">' +
          '<div class="hotel-info-nearby-box-img"> ' +
          discountBlock +
          '<img alt="' + hotel.name + '" src="' + hotel.thumbnail + '" onerror="this.src=\'/img/general/img_error.jpg\'" width="100%" height="100%">' +
          '</div>' +
          '<div class="hotel-info-nearby-box-btm rec-hotel-box clear">' +
          '<p class="rec-hotel-block-name hotel-block-name">' + hotel.name + '</p>' +
          '<span class="rating">' + buildRating(hotel.rating, 'usitrip-hover-tag') + '</span>' +
          '<span class="rec-hotel-block-price pull-right">' + hotel.currency + '<em>' + hotel.minPrice + '</em></span>' +
          '</div>' +
          '</a>' +
          '</div>';
    }
    $('#hotelsBlock').html(hotelsBlock);

    if (typeof google !== 'undefined') {
        hotelMap.fitBounds(mapBounds); // extend map view
    }
};

//todo: delete
window.prevGallery = function (gallery) {
    var e = $('.' + gallery + '-options a.active');
    if ($.isEmptyObject(e)) {
        return;
    }

    var p;
    if (e.is(':first-child')) {
        p = $('.' + gallery + '-options a').last();
    } else {
        p = e.prev();
    }

    e.attr('class', '');

    p.attr('class', 'active');
    $('.' + gallery + '-player > img').attr('src', p.attr('href'));
};

//todo: delete
window.nextGallery = function (gallery) {
    var e = $('.' + gallery + '-options a.active');
    if ($.isEmptyObject(e)) {
        return;
    }

    var p;
    if (e.is(':last-child')) {
        p = $('.' + gallery + '-options a').first();
    } else {
        p = e.next();
    }

    e.attr('class', '');

    p.attr('class', 'active');
    $('.' + gallery + '-player > img').attr('src', p.attr('href'));
};

// 动画效果定焦，且id不加在url里，避免更改语言时的冲突
window.goToByScroll = function (id) {
    $('html,body').animate({scrollTop: $('#' + id).offset().top - 120}, 'slow');
};

//显示可预订房间
window.showRoomAvail = function (hotelId) {
    var l = $('.ladda-button-search').ladda();
    l.ladda('start');

    var loading = '<div class="sk-spinner sk-spinner-three-bounce"> ' +
      '<div class="sk-bounce1"></div>' +
      '<div class="sk-bounce2"></div>' +
      '<div class="sk-bounce3"></div>' +
      '</div>';

    // $('#roomOption').html(loading);
    $('#roomOption').html('<tr><td colspan="4">' + loading + '</td></tr>');
    $('#desId').val('H' + hotelId);

    if (hasCachedHotel === '1') {
        if (!reorderId) {
            showRecommendHotels(sessionKey);
        }
        getRoomOptions(sessionKey, hotelId, passedpartnerId);
    } else {
        sxhr = $.get('/hotel/new', $('#search-form').serialize(), function (data) {
            if (data.sessionKey) {
                sessionKey = data.sessionKey;
                searchedHotel = data.hotel;
                $('#sessionKey').val(sessionKey);

                // 更新从searchHotel来的guestRating和reviewCount, original price and discount;
                if (data.hotel.guestRating) {
                    $('#guest-rating').html(data.hotel.guestRating + '/5');
                }
                if (data.hotel.guestReviewCount) {
                    $('#guest-review-count').html(data.hotel.guestReviewCount === null ? 0 : data.hotel.guestReviewCount);
                }
                var currency = data.hotel.currency === 'USD' ? '$' : data.hotel.currency;
                $('.original-price-line-through').html(currency + Math.round(data.hotel.minPrice / (1 - data.hotel.discount), -1));
                $('.hotel-min-price').html(currency + data.hotel.minPrice);
                if (mapInited) {
                    updateMap(data.hotel);
                }
                getRoomOptions(sessionKey, data.hotel.hotelId, passedpartnerId);
                if (!reorderId) {
                    showRecommendHotels(sessionKey);
                }
            } else {
                $('.ladda-button-search').ladda('stop');
                $('#roomOption').html('<tr><td colspan="4">' + lang_noResult + '</td></tr>');
                $('.price-block').html('<a onclick="goToByScroll(\'checkin\')">' + noPriceAvail + '</a>');
            }
        });
    }
};

// update search
window.updateSearch = function (hotelId) {
    //get rooms and days
    var checkin = $('#checkin').val();
    var checkout = $('#checkout').val();
    dayCount = calculateDays(checkin, checkout);

    /* a quick date format validation */
    var dateformat = /^(\d{4})-(\d{2})-(\d{2})$/;
    if (!checkin.match(dateformat) || !checkout.match(dateformat)) {
        swal({
            title: lang_format,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'ok'
        });
        return false;
    } else {
        //checkin date validation
        var parts = checkin.split('-');

        var year = parts[0];
        var month = parts[1];
        var day = parts[2];

        var $day = (day.charAt(0) === '0') ? day.charAt(1) : day;
        var $month = (month.charAt(0) === '0') ? month.charAt(1) : month;

        //checkout date validation
        var parts1 = checkout.split('-');

        var year1 = parts1[0];
        var month1 = parts1[1];
        var day1 = parts1[2];

        var $day1 = (day1.charAt(0) === '0') ? day1.charAt(1) : day1;
        var $month1 = (month1.charAt(0) === '0') ? month1.charAt(1) : month1;

        var now = new Date();
        var currentYear = now.getFullYear();

        if ($day > 31 || $day < 1 || $month > 12 || $month < 1 || year < currentYear) {
            swal({
                title: lang_checkInvalid,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'ok'
            });
            return false;
        }
        if ($day1 > 31 || $day1 < 1 || $month1 > 12 || $month1 < 1 || year1 < currentYear) {
            swal({
                title: lang_checkOutValid,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'ok'
            });
            return false;
        }

    }

    var newDate = new Date();
    var maxdm = newDate.toISOString().substr(4, 6);
    var maxCheckin = (newDate.getFullYear() + 2) + maxdm;

    if (checkin > maxCheckin) {
        swal({
            title: maxCheckinText,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'ok'
        });
        return false;
    }

    if (dayCount > 30) {
        swal({
            title: lang_limit,
            type: 'warning',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'ok'
        });
        return false;
    }

    $('#dayCount').val(dayCount);

    var $searchForm = $('#search-form');
    $searchForm.attr('action', '/hotel/' + hotelId + '.html');
    $searchForm.submit();
};

window.getRoomOptions = function (sessionKey, hotelId, passedpartnerId) {
    var html = '';
    srxhr = $.get('/hotel/rooms', $('#search-form').serialize(), function (data) {
        var l = $('.ladda-button-search').ladda();
        l.ladda('stop');
        // 动态的 checkin checkout pet policy child policy and so on for hotel room service info section
        if (data && data.policies) {
            if (data.policies.checkInStartTime) {
                var checkinstartContentItems = '<h3 class="hotel-policy-head usitrip-bright">' + langHotel.hotel_checkin_start + '</h3><p class="hotel-policy-text">' +
                  data.policies.checkInStartTime +
                  '</p>';
                $('#dynamic-checkin-start-box').append(checkinstartContentItems);
                $('#dynamic-checkin-start-box').removeClass('hidden');
            }
            if (data.policies.checkInEndTime) {
                var checkinendContentItems = '<h3 class="hotel-policy-head usitrip-bright">' + langHotel.hotel_checkin_end + '</h3><p class="hotel-policy-text">' +
                  data.policies.checkInEndTime +
                  '</p>';
                $('#dynamic-checkin-end-box').append(checkinendContentItems);
                $('#dynamic-checkin-end-box').removeClass('hidden');
            }
            if (data.policies.checkOutTime) {
                var checkoutContentItems = '<h3 class="hotel-policy-head usitrip-bright">' + langHotel.hotel_checkout + '</h3><p class="hotel-policy-text">' +
                  data.policies.checkOutTime +
                  '</p>';
                $('#dynamic-checkout-box').append(checkoutContentItems);
                $('#dynamic-checkout-box').removeClass('hidden');
            }
            var childContentItems = '';
            if (data.policies.childrenAndExtraBedsPolicies) {
                childContent = data.policies.childrenAndExtraBedsPolicies;
                childContentItems += '<h3 class="hotel-policy-head usitrip-bright">' + langHotel.hotel_child + '</h3>';
                $.each(childContent, function (index, value) {
                    childContentItems += '<p class="hotel-policy-text">' + value + '</p>';
                });
                $('#dynamic-child-box').append(childContentItems);
                $('#dynamic-child-box').removeClass('hidden');
            }
            var petContentItems = '';
            if (data.policies.petPolicies && data.policies.petPolicies[0] !== null) {
                petFriendlyContent = data.policies.petPolicies;
                petContentItems += '<h3 class="hotel-policy-head usitrip-bright">' + langHotel.hotel_pet + '</h3>';
                $.each(petFriendlyContent, function (index, value) {
                    petContentItems += '<p class="hotel-policy-text">' + value + '</p>';
                });
                $('#dynamic-pet-box').append(petContentItems);
                $('#dynamic-pet-box').removeClass('hidden');
            }
        }

        if (data && data.type && data.type === 'SESSION_TIMEOUT') {
            swal({
                title: lang_loginExpired,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'OK'
            }, function () {
                window.location.href = '/';
            });
        } else if (data && data.rooms && data.rooms.length > 0 && data.rooms[0].length > 0) {
            rooms_key = data.rooms_key;
            var rooms = data['rooms'][0];
            var minPriceCurrency = rooms[0]['currency'] === 'USD' ? '$' : rooms[0]['currency'];
            $('.hotel-min-price').html(minPriceCurrency + rooms[0]['beforeTax']);

            rooms_data = rooms;

            if (is_pairing) {
                getRoomList();
            }

            $.each(rooms, function (j, room) {
                var roomData = room.extraInfo;

                room.description = '';
                if (roomData) {
                    room.description = language === 0 && roomData.description_zh ? roomData.description_zh : roomData.description;
                }

                var facilities = '<br><br><span><strong>客房设施：</strong><small>';
                var dayPrice = room.beforeTax;
                var choice = 0;

                if (room.facilities === '') {
                    facilities = '';
                } else {
                    $.each(room.facilities, function (m, key) {
                        var length = room.facilities.length;

                        if (key.val === null) {
                            facilities += key.fee === 1 ? '<span class="text-danger">' : '<span>';
                            facilities += '' + key.title_zh + (m === (length - 1) ? '' : ', ') + '</span>';
                        } else {
                            facilities += key.fee === 1 ? '<span class="text-danger">' : '<span>';
                            facilities += '' + key.title_zh + '：' + key.val + (m === (length - 1) ? '' : ', ') + '</span>';
                        }

                        if (key.fee === 1)
                            choice = 1;
                    });

                    if (choice === 1) {
                        facilities += '</span></small><br><small><span class="text-danger">*红色字是酒店收费服务</span><small>';
                    } else {
                        facilities += '</span></small>';
                    }
                }

                // 酒店房间异步加载的房型table开始
                // 1：房间名称和照片
                // a.房间照片
                var roomPic = '/img/room-images/default_room_image_' + Math.floor(Math.random() * 45) + '.jpg';
                var roomPicStyle = '';
                if (roomData && roomData['room_images'] && roomData['room_images'].length > 0) {
                    roomPic = extraImgPath + roomData['room_images'][0]['url'];
                    if (roomData['room_images'].length > 1) {
                        roomPicStyle = 'cursor: pointer;';
                    }
                } else if (room.hasOwnProperty('roomImgs') && room.roomImgs !== '' && room.roomImgs !== null && room.roomImgs.length !== 0) {
                    roomPic = room.roomImgs[Math.floor(Math.random() * room.roomImgs.length)];
                }

                var roomImgs = '<img alt="' + room.name + '" class="hotel-room-img" src="' + roomPic + '" style="' + roomPicStyle + '" data-ref="' + room.roomReference + '">';
                var roomImgsAnchor = '';
                if (roomData && roomData['room_images'] && roomData['room_images'].length > 1) {
                    roomImgsAnchor = '<div class="hotel-room-img-more" data-ref="' + room.roomReference + '"><i class="fa fa-image"></i> ' + (language === 1 ? 'More' : '更多') + '</div>';
                }

                // b.房间名称
                var finalName = room.name;
                if (language === 0) {
                    finalName = groupName(finalName, room.name_zh)
                }

                var room_name_html = '<p class="hotel-room-name" data-ref="'+ room.roomReference +'" data-hotelId="'+ room.hotelId +'" data-roomId="'+ room.roomId +'">' + finalName + '</p>';
                if (roomData) {
                    if (roomData.room_images && roomData.room_images.length > 1) {
                        room_name_html = '<p class="hotel-room-name underlined" data-ref="' + room.roomReference + '" data-hotelId="' + room.hotelId + '" data-roomId="' + room.roomId + '">' + finalName + '</p>';
                    }
                    if (roomData.size) {
                        room_name_html += '<p class="hotel-room-size">Room Size: '+ (language === 0 && roomData.size_zh ? roomData.size_zh : roomData.size) +'</p>'
                    }
                }

                if (is_pairing) {
                    if (roomData && roomData.name_booking) {
                        room_name_html += '<p class="hotel-room-name-paired">' +
                          '<b>已匹配:</b> '+ roomData.name_booking + (roomData.name_booking_zh ? (' (' + roomData.name_booking_zh + ')') : '') +
                          '</p>';
                    }
                }

                html += '<tr class="hotel-room">' +
                  '<td  width="20%" class="hotel-service-special-li hotel-room-col1" data-ref="' + room.roomReference + '">' +
                  '<div class="hotel-room-img-wrapper">' +
                  roomImgs +
                  roomImgsAnchor +
                  '</div>' +
                  room_name_html +
                  '</td>';

                // 2：房间床型与解释
                // a.床型
                var bedType = '';
                if (room.hasOwnProperty('bedType') && room.bedType !== '' && room.bedType !== null) {
                    bedType = room.bedType;
                } else {
                    bedType = langHotel.one_or_two_beds;
                }
                // b.解释
                var roomCommend = '';
                if (room.hasOwnProperty('roomAmenities')) {
                    if (room.roomAmenities !== '' && room.roomAmenities !== null) {
                        $.each(room.roomAmenities, function (index, value) {
                            roomCommend += '<p>(' + value.name + ')</p>';
                        });
                    }
                }

                html += '<td width="26.67%" class="hotel-service-special-li">' +
                  '<p><b>' + bedType + '</b></p>' +
                  '<p>' + (room.description ? room.description : '') + '</p>' +
                  '<p>' + roomCommend + '</p>' +
                  '</td>';

                // All Inclusive
                var allInclusive = '';
                if (hotelName.toLowerCase().indexOf('inclusive') !== -1) {
                    allInclusive = '<span>' + langHotel.all_inclusive;
                    var valid_hotels = ['1209914', '1110778', '1060790', '1056212', '1056212', '1157838', '1079759'];
                    if (valid_hotels.indexOf(hotelId.toString()) > -1) {
                        allInclusive += '<span class="facility-all-inclusive-info"><i class="icon-info-all-inclusive"></i>';
                        var notes = language === 0 ? '<b>三餐</b>:房价包括所有餐点和指定饮料。' : '<b>Meals</b>: All meals and select beverages are included in the room rate.';
                        if (hotelId == 1209914) {
                            notes = language === 0 ? '<b>三餐</b>:房价包括所有餐点和指定饮料。<br/><b>预付款项</b>: 无需预付。' : '<b>Meals</b>: All meals and select beverages are included in the room rate.<br/><b>Prepayment</b>: No prepayment is needed.';
                        }
                        allInclusive += '<span class="inclusive-tooltips">'+ notes +'</span></span>';
                    }
                    allInclusive += '</span><br/>';
                    if ($('.facilities-banner .icon-wrapper').length >= 5) {
                        $('.facilities-banner .icon-wrapper:nth-child(5)').hide();
                    }
                    $('.icon-all-inclusive-wrapper').removeClass('hidden');
                }

                // 3:房间设施取消政策早餐等信息介绍
                // a.取消政策
                var  cancellationPolicy = '';
                if (room.rateClass === '可取消') {
                    $.each(room.cancellationPolicies, function (i, value) {
                        if (value.amount === 0) {
                            cancellationPolicy = '<i class="fa fa-check usitrip-hover-tag padding-right-5"></i>' +
                              '<span>' + langHotel.free_cancel + '</br><span class="padding-left-19">' + langHotel.free_cancel_until + value.end.substring(0, 19).replace('T', ' ') + '</span></span>';
                        }
                    });
                } else {
                    cancellationPolicy = '<i class="fa fa-check usitrip-hover-tag padding-right-5"></i><span>' + langHotel.cancel_not_show + '</span>';
                }

                // c. 早餐
                // 早餐类型：All Inclusive, Room only，Free breakfast ， Breakfast， Bed and breakfast 其中包括收费早餐
                var boardNameHtml = '';
                var strborder = (language === 0 && room.boardName_zh) ? (room.boardName + '<br/>('+ room.boardName_zh +')') : room.boardName;
                strborder = strborder.toLowerCase();
                if (strborder.match(/free.*breakfast/gi) || hotelName.toLowerCase().indexOf('inclusive') !== -1 || (strborder.indexOf('含早') !== -1 && strborder.indexOf('不含早') === -1)) {
                    // 免费早餐
                    boardNameHtml = '<i class="fa fa-coffee usitrip-hover-tag padding-right-5"></i><span>' + langHotel.free_breakfast + '</span>';
                } else if (strborder.indexOf('breakfast') !== -1 || strborder.indexOf('inclusion') !== -1 ) {
                    // 有早餐但是可能收费，如实显示返回早餐类型值
                    boardNameHtml = '<i class="fa fa-coffee usitrip-hover-tag padding-right-5"></i><span style="text-transform: capitalize;">' + strborder + '</span>';
                }

                // d. 停车 e. 上网 f. 接驳车
                var facilityParking = '';
                var facilityWifi = '';
                var facilityShuttle = '';
                $.each(data.facilities, function (value, key) {
                    if (key.title.toLowerCase().indexOf('free') !== -1 && key.title.toLowerCase().indexOf('parking') !== -1) {
                        facilityParking = '<i class="fa fa-car usitrip-hover-tag padding-right-5"></i><span>' + langHotel.free_parking + '</span>';
                    }
                    if (key.title.toLowerCase().indexOf('free') !== -1 && key.title.toLowerCase().indexOf('wifi') !== -1) {
                        facilityWifi = '<i class="fa fa-wifi usitrip-hover-tag padding-right-5"></i><span>' + langHotel.free_wifi + '</span>';
                    }
                    if (key.title.toLowerCase().indexOf('free') !== -1 && key.title.toLowerCase().indexOf('shuttle') !== -1) {
                        facilityShuttle = '<i class="fa fa-bus usitrip-hover-tag padding-right-7-left-2"></i><span>' + langHotel.free_shuttle + '</span>';
                    }
                    if (key.title.toLowerCase().indexOf('free') !== -1 && key.title.toLowerCase().indexOf('breakfast') !== -1) {
                        // 如果整个酒店介绍有免费早餐，显示一条note，以每个房型备注为主
                        $('.hotel-facility-note').html(langHotel.hotel_facility_note);
                    }
                });

                // f.免费接驳车等。rateAmenities
                var rateAmenities = '';
                if (room.hasOwnProperty('rateAmenities')) {
                    if (room.rateAmenities !== '' && room.rateAmenities !== null) {
                        $.each(room.rateAmenities, function (index, value) {
                            // 目前只显示如接驳车，等除开早餐，parking和wifi的extra信息
                            if ((value.name.toLowerCase().indexOf('shuttle') !== -1 || value.id === '2196') && facilityShuttle === '') {
                                rateAmenities += '<i class="fa fa-bus usitrip-hover-tag padding-right-6"></i><span>' + value.name + '</span><br/>';
                            } else if (value.name.toLowerCase().indexOf('internet') !== -1 || value.name.toLowerCase().indexOf('上网') !== -1 ||
                              value.name.toLowerCase().indexOf('wifi') !== -1 || value.name.toLowerCase().indexOf('Wireless') !== -1) {
                                rateAmenities += '';
                            } else if (value.name.toLowerCase().indexOf('parking') !== -1 || value.name.toLowerCase().indexOf('停车') !== -1) {
                                rateAmenities += '';
                            } else if (value.name.toLowerCase().indexOf('breakfast') !== -1 || value.name.toLowerCase().indexOf('早餐') !== -1) {
                                rateAmenities += '';
                            } else if (value.id === '2111' || hotelName.toLowerCase().indexOf('inclusive') !== -1 || value.name.toLowerCase().indexOf('全包') !== -1) {
                                // 只有expedia显示全包
                                rateAmenities += '';
                            } else {
                                rateAmenities += '<i class="fa fa-check usitrip-hover-tag padding-right-5"></i><span>' + value.name + '</span><br/>';
                            }
                        });
                    }
                }

                // 添加从bookings来的额外信息
                if (roomData && roomData.facilities) {
                    var room_facilites_html = '<p class="room-facility-wrapper facility-small">';
                    var roomFacilities = language === 0 && roomData.facilities_zh ? roomData.facilities_zh : roomData.facilities;
                    roomFacilities = $.parseJSON(roomFacilities);
                    for (var key in roomFacilities) {
                        if (!roomFacilities.hasOwnProperty(key)) continue;
                        room_facilites_html += '<span class="room-facility-item facility-item">' +
                          '<i class="facility-icon facility-icon-'+ key.replace('_', '-') +'"></i>' + roomFacilities[key] +
                          '</span>';
                    }
                    room_facilites_html += '</p>';
                    if (roomData.more) {
                        room_facilites_html += '<p class="room-facility-more hide">';
                        var facilities_more = language === 0 && roomData.more_zh ? roomData.more_zh : roomData.more;
                        for (var i = 0; i < facilities_more.length; i++) {
                            room_facilites_html += '<span class="room-facility-more-item">' + facilities_more[i] + '</span>';
                        }
                        room_facilites_html += '</p>';
                        room_facilites_html += '<a class="room-facility-more-btn">More</a>'
                    }
                } else {
                    room_facilites_html = '';
                }

                html += '<td width="32.67%" class="hotel-service-special-li">' +
                  '<div class="facility-all-inclusive">' + allInclusive + '</div>' +
                  '<div>' + cancellationPolicy + '</div>' +
                  '<div><i class="fa fa-check usitrip-hover-tag padding-right-5"></i><span>' + langHotel.no_cc_fee + '</span></div>' +
                  '<div><i class="fa fa-check usitrip-hover-tag padding-right-5"></i><span>' + langHotel.hotel_confirm_instant + '</span></div>' +
                  '<div>' + facilityParking + '</div>' +
                  '<div>' + facilityWifi + '</div>' +
                  '<div>' + facilityShuttle + '</div>' +
                  '<div>' + boardNameHtml + '</div>' +
                  '<div>' + rateAmenities + '</div>' +
                  '<div>' + room_facilites_html + '</div>' +
                  '</td>';

                // 4:房间每房晚价格，预订按钮，度假费，折扣信息
                var roomRef = room.roomReference;
                var provider = roomRef.substring(0, 2).replace(':', '');
                if (room.currency === 'USD') {
                    room.currency = '$';
                }
                var roomRate = '<span class="hotel-room-price">' + room.currency + '<span>' + ' ' + dayPrice + '</span></span>';
                if (room.status && room.status.toLowerCase() === 'oos') {
                    roomRate += '';
                }

                if (showPr === 'true') {
                    roomRate += ' ( ' + provider + ' )';
                }
                if (is117book){
                  roomRate += '<span class="subText">' +
                      ' (' + langHotel.taxIncluded + ')</span>';
                }
                var bookLink = '/book/' + hotelId + '/' + encodeURIComponent(roomRef) + (passedpartnerId ? ('/' + passedpartnerId) : '') + '?s=' + sessionKey +
                    '&' + $('#search-form').serialize() +'&rooms_key='+ rooms_key;

                if (reorderId) {
                    bookLink += '&Reorderid=' + reorderId;
                }

                if (window.location.href.indexOf('hotel/missed/reorder') > -1) {
                    bookLink += '&type=missedOrder';
                }

                var bookButton = '';

                if(userId === adUserId){
                  bookButton = '<a id="ConfirmToBookBtn" class="btn' +
                      ' bookButton registerButton"  onclick="registerToBook(' + hotelId + ',\'' + checkin + '\', \'' + checkout + '\', ' + roomCount + ', ' + adultCount1 + ', ' + childCount1 + ')">'  + langHotel.registerToBook + '</a>';
                }else{
                   bookButton = '<a id="ConfirmToBookBtn" class="btn bookButton bookButtonStyle" target="_blank" href="' + bookLink + '" ' +
                      'onclick="addRoomToCart(' + hotelId + ', \'' + escape(room.name) + '\', \'' + room.roomReference + '\', ' + room.netPrice + ')">' + lang_reserve + '</a>';
                }

                if (room.status && room.status.toLowerCase() === 'oos') {
                    bookButton = '<input type="submit" value="' + lang_roomSoldOut + '" class="btn bookButton bookButtonStyle" disabled/>';
                } else if (myCookie === null && window.location.href.indexOf('reorder') > -1) {
                    bookButton = '<input type="submit" value="无权限" class="btn bookButton" disabled/>';
                }

                var mandatoryFees = '';
                if (room.hasOwnProperty('mandatoryFees') && room.mandatoryFees !== '' && room.mandatoryFees !== null && room.mandatoryFees !== 0) {
                    mandatoryFees = langHotel.not_include + room.currency + (room.mandatoryFees / roomCount / dayCount * 100 / 100).toFixed(2) + langHotel.resort_fee;
                }

                html += '<td width="20.66%" class="table-align-right">' +
                  '<medium class="hotel-room-rate">' + roomRate + '</medium>' +
                  '<p class="mandatory-fee">' + mandatoryFees + '</p>' +
                  bookButton +
                  '<p class="hotel-room-book-notes">'+ lang_bookNotes +'</p>' +
                  '</td>' +
                  '</tr>';
            });

            $('#roomOption').html(html);

            $('.hotel-room-img, .hotel-room-name, .hotel-room-img-more').click(function () {
                var reference = $(this).attr('data-ref');
                if (reference) {
                    var room = rooms_data.find(function (item) {
                        if (item.extraInfo) {
                            return item.roomReference === reference;
                        }
                    });

                    if (room && room.extraInfo.room_images.length > 1) {
                        showRoomInfo(room);
                    }
                }
            });

            $('.room-facility-more-btn').click(function () {
                var $moreFacilityWrapper = $(this).prev();
                $moreFacilityWrapper.toggleClass('hide');
                if ($moreFacilityWrapper.hasClass('hide')) {
                    $(this).text('More');
                } else {
                    $(this).text('Less');
                }
            });
        } else {
            $('#roomOption').html('<tr><td colspan="4">' + lang_noResult + '</td></tr>');
            $('.price-block').html('<a onclick="goToByScroll(\'checkin\')">' + noPriceAvail + '</a>');
        }
    });
};

window.getRoomList = function() {
    $.post('/hotel/rooms/list', {
        '_token': $('meta[name="csrf_token"]').attr('content'),
        'hotelId': hotelId
    }, function (response) {
        if (response.data && response.data.length > 0) {
            rooms_list = response.data;
            var list = response.data;
            var select_html = '<select class="hotel-room-list">';
            for (var i = 0; i<list.length; i++) {
                select_html += '<option value="'+ list[i]['bookingRoomId'] +'">'+ list[i]['bookingRoomName'] + ', 床型: ' + list[i]['bookingBedType'] +'</option>';
            }
            select_html += '</select><br>' +
              '<button class="hotel-room-list-cancel">取消匹配</button>' +
              '<button class="hotel-room-list-submit">提交匹配</button>';

            $('.hotel-room-col1').append(select_html);

            $('.hotel-room-name-paired').siblings('.hotel-room-list-cancel').css('display', 'inline-block');

            //取消匹配
            $('.hotel-room-list-cancel').click(function () {
                var roomId = $(this).siblings('.hotel-room-name').attr('data-roomId');
                if (roomId) {
                    var self = this;
                    $.post('/hotel/rooms/pair/cancel', {
                        '_token': $('meta[name="csrf_token"]').attr('content'),
                        'roomId': roomId
                    }, function (cancel_res) {
                        if (cancel_res.data && cancel_res.data.success) {
                            $(self).siblings('.hotel-room-name-paired').remove();
                            swal('取消匹配成功');
                        } else {
                            swal('取消匹配失败');
                        }
                    });
                }
            });
            //提交匹配
            $('.hotel-room-list-submit').click(function () {
                var hotelId = $(this).siblings('.hotel-room-name').attr('data-hotelId');
                var roomId = $(this).siblings('.hotel-room-name').attr('data-roomId');
                var bookingId = $(this).siblings('.hotel-room-list').val();

                if (roomId) {
                    var self = this;
                    $.post('/hotel/rooms/pair/submit', {
                        '_token': $('meta[name="csrf_token"]').attr('content'),
                        'hotelId': hotelId,
                        'roomId': roomId,
                        'bookingId': bookingId,
                    }, function (pair_res) {
                        if (pair_res.data && pair_res.data.success) {
                            if ($(self).siblings('.hotel-room-name-paired').length > 0) {
                                $(self).siblings('.hotel-room-name-paired').html('<b>新匹配:</b> ' + $(self).siblings('.hotel-room-list').find('option:selected').text());
                            } else {
                                $(self).siblings('.hotel-room-name').after('<p class="hotel-room-name-paired"><b>新匹配:</b> ' + $(self).siblings('.hotel-room-list').find('option:selected').text() + '</p>');
                            }
                            swal('匹配成功');
                        } else {
                            swal('匹配失败');
                        }
                    });
                }
            });
        }
    });
};

window.registerToBook = function (hotelId, checkin, checkout, roomCount, adultCount, childCount) {
  var bookLink = '/hotel/' + hotelId + '?checkin=' + checkin +  '&checkout=' + checkout + '&roomCount=' + roomCount + '&adultCount1=' + adultCount + '&childCount1=' + childCount;
  $('#RegisterBookLink').val(bookLink);
  $('#registerModal').modal('show');
};

window.addRoomToCart = function (hotelId, roomName, roomRef, price) {
    // report book conversion
    if (shouldTrack) {
        gtag_report_conversion_book();
    }

    var anaData = {
        'id': hotelId,
        'name': hotelName,
        'variant': unescape(roomName),
        'brand': roomRef.substr(0, 1),
        'price': price,
        'qty': roomCount
    };
    addProductToCart(anaData);
    // onCheckoutBegin(anaData);
};

window.calculateDays = function (checkin, checkout) {
    var date1 = new Date(checkin);
    var date2 = new Date(checkout);
    var date3 = date2.getTime() - date1.getTime();
    var dayCount = Math.floor(date3 / (24 * 3600 * 1000));

    return dayCount;
};

window.groupName = function (eng, chs) {
    if (chs) {
        return eng + '<br/>(' + chs + ')';
    } else {
        return eng;
    }
};

window.fraudEmail = function () {
    if (window.location.href.indexOf('hotel/missed/reorder') > -1) {
        if (is117book) {
            var url = '/business/missed-order/decline/' + reorderId;
        } else {
            var url = '/missed-order/decline/' + reorderId;
        }
    } else {
        var url = '/potential-order/decline/' + reorderId;
    }
    $.post(url, {
        '_token': $('meta[name="csrf_token"]').attr('content')
    }, function (data) {
        if (data.success) {
            $('#declineBtn').button('reset');
            swal({
                title: '订单拒绝成功，返回搜索',
                type: 'success',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'OK'
            }, function () {
                window.location.href = '/search';
            });
        } else {
            $('#declineBtn').button('reset');
            swal({
                title: data.message,
                type: 'warning',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'OK'
            }, function () {
                window.location.href = self.location;
            });
        }
    });
};

//FOR COUNT DOWN
window.countdown = function (elementName, minutes, seconds) {
    var element, endTime, hours, mins, msLeft, time;

    function twoDigits (n) {
        return (n <= 9 ? '0' + n : n);
    }

    function updateTimer () {
        msLeft = endTime - (+new Date);
        if (msLeft < 1000) {
            countdown('countdown', 978, 0); //redo after times out
        } else {
            time = new Date(msLeft);
            hours = time.getUTCHours();
            mins = time.getUTCMinutes();
            element.innerHTML = (hours ? hours + ' : ' + twoDigits(mins) : '00 : ' + mins) + ' : ' + twoDigits(time.getUTCSeconds());
            setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
        }
    }

    element = document.getElementById(elementName);
    endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
    updateTimer();
};

// google maps callback
window.initMap = function () {
    mapBounds = new google.maps.LatLngBounds();
    originIcon = {
        url: 'https://s3-us-west-1.amazonaws.com/usitrip/map/infor_m-01.png',
        labelOrigin: new google.maps.Point(25, 12)
    };
    selectedIcon = {
        url: 'https://s3-us-west-1.amazonaws.com/usitrip/map/infor_m-03.png',
        labelOrigin: new google.maps.Point(30, 13)
    };

    var center = {lat: searchedHotel.latitude, lng: searchedHotel.longitude};
    hotelMap = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: center,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP
        },
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP
        }
    });

    addMapMarker(searchedHotel, true); // set center marker for map
    mapInited = true;
};

window.addMapMarker = function (hotel, isCenter) {
    var position = {lat: hotel.latitude, lng: hotel.longitude};
    var marker = new google.maps.Marker({
        position: position,
        map: hotelMap,
        label: {
            text: (hotel.currency === 'USD' ? '$' : hotel.currency ? hotel.currency : '') + hotel.minPrice,
            color: 'white',
            fontSize: "15px"
        },
        icon: originIcon
    });
    mapBounds.extend(position);

    var guestRatingLine = '<p class="guest-rating-blank-space"></p>';
    if (hotel.guestRating) {
        var guestRating = hotel.guestRating + '/5';
        var guestReviewCount = hotel.guestReviewCount === null ? '0' : hotel.guestReviewCount;
        guestRatingLine = '<div class="hotel-guestrating">' + langHotel.guestRating +
          '<p class="guest-rating"><span>' + guestRating + '&nbsp;</span>&nbsp;<span class="rating-count">(' + guestReviewCount + langHotel.review + ')</span></p>' +
          '</div>';
    }
    var feature = hotel.lowestFlg ? '<p><i class="fa fa-check"></i>' + langHotel.lowest + '</p>' : '';
    if (hotel.freeCancel === 1) {
        feature += '<p><i class="fa fa-check"></i>' + langHotel.free_cancel + '</p>';
    }

    var currency = hotel.currency === 'USD' ? '$' : hotel.currency;
    var price = '<span>' + currency + hotel.minPrice + '</span>';
    if (hotel.discount) {
        price = '<span class="beforeTax">' + currency + Math.round(hotel.minPrice / (1 - hotel.discount), -1) + '</span><span class="afterTax">' + currency + hotel.minPrice + '</span>';
    }

    // hotel link to open for hotel details
    var hotelLink = '/hotel/' + hotel.hotelId + '.html?s=' + (isCenter ? sessionKey : recSessionKey) + '&' + $('#search-form').serialize();

    var infoContent =
      '<a href="' + hotelLink + '" target="_blank"><img alt="'+ hotel.name +'" class="infowindow-img" src="' + hotel.thumbnail +
      '" height="100" width="200" onerror="this.src=\'' + '/img/general/img_error.jpg' + '\'"/></a>' +
      '<div class="hotel-info-wrapper">' +
      '<a href="' + hotelLink + '" target="_blank" class="hotel-name">' + hotel.name + '</a>' +
      '<i class="hotel-rating hotel-rating-' + hotel.rating + '"></i>' +
      guestRatingLine +
      '<div class="hotel-feature">' +
      '<div>' + feature + '</div>' +
      '<div class="map-hotel-price">' + price + '</div>' +
      '</div>' +
      '</div>';

    marker.addListener('click', function () {
        if (selectedMaker !== this) {
            var label = this.getLabel();
            label.fontSize = "22px";
            label.color = 'white';
            this.setIcon(selectedIcon);
            this.setLabel(label);
            this.setZIndex(highestZIndex);
            if (selectedMaker !== null) {
                var label = selectedMaker.getLabel();
                label.fontSize = "20px";
                label.color = 'white';
                selectedMaker.setIcon(originIcon);
                selectedMaker.setLabel(label);
                selectedMaker.setZIndex(selectedMaker.opt);
            }
            selectedMaker = this;
            $('#infowindow').empty();
            $('#infowindow').append(infoContent);
            $('#infowindow').css({'visibility': 'visible', 'display': 'block'});
        }
    });

    if (isCenter) {
        new google.maps.event.trigger(marker, 'click');
    }
};

//更新map上的酒店信息
window.updateMap = function (hotel) {
    var currency = hotel.currency === 'USD' ? '$' : hotel.currency;
    var price = '<span>' + currency + hotel.minPrice + '</span>';
    if (hotel.beforeTax) {
        price = '<span class="beforeTax">' + currency + Math.round(hotel.minPrice / (1 - hotel.discount), -1) + '</span><span class="afterTax">' + currency + hotel.minPrice + '</span>';
    }
    $('.map-hotel-price').html(price);

    addMapMarker(hotel, true);
};

window.showRoomInfo = function (room) {
    $('.room-modal-title').text(hotelName.replace(/&amp;/g, '&') + ' ' + room.name);
    var images = room.extraInfo.room_images;
    var image_list = '';
    for (var i = 0; i < images.length; i++) {
        if (i === 0) {
            image_list += '<a href="' + (extraImgPath + images[i]["url"]) + '" class="active hotel-thumbnail-imgs"' +
                ' style="background-image: url(' + (extraImgPath + images[i]["url"]) + ');" onclick="return false;"></a>'
        } else {
            image_list += '<a href="' + (extraImgPath + images[i]["url"]) + '" class="hotel-thumbnail-imgs" style="background-image: url(' + (extraImgPath + images[i]["url"]) + ');" onclick="return false;"></a>'
        }
    }
    var img_carousel = '<div id="carousel-room-images" class="room-gallery">' +
      '<div class="room-gallery-player">' +
      '<a href="" class="btn room-gallery-control room-gallery-prev" onclick="return false;"><i class="fa fa-angle-left"></i></a>' +
      '<a href="" class="btn room-gallery-control room-gallery-next" onclick="return false;"><i class="fa fa-angle-right"></i></a>' +
      '<img src="' + (extraImgPath + images[0]["url"]) + '" alt="' + room['room_name'] + '" class="room-gallery-image">' +
      '</div>' +
      '<div class="room-gallery-options">' +
      image_list +
      '</div>' +
      '</div>';
    $('.room-modal-images').html(img_carousel);

    //add event listeners
    $('.room-gallery-options > a').click(function () {
        $('.room-gallery-options > a.active').attr('class', '');
        $(this).attr('class', 'active');
        $('.room-gallery-player > img').attr('src', $(this).attr('href'));
    });
    $('.room-gallery-prev').click(function (e) {
        e.preventDefault();
        prevGallery('room-gallery');
    });
    $('.room-gallery-next').click(function (e) {
        e.preventDefault();
        nextGallery('room-gallery');
    });

    var room_details = '';
    if (room.extraInfo.size) {
        room_details += '<p>Room size: ' + room.extraInfo.size + '</p>';
    }
    if (room.description) {
        room_details += '<p class="room-description">' + room.description + '</p>';
    }
    if (room.extraInfo.more) {
        var room_facilities = language === 0 && room.extraInfo.more_zh ? room.extraInfo.more_zh : room.extraInfo.more;
        var room_facilities_html = '<h5 class="room-facility-title">Room facilities:</h5><ul>';
        for (var i = 0; i < room_facilities.length; i++) {
            room_facilities_html += '<li class="room-facility">' + room_facilities[i] + '</li>';
        }
        room_facilities_html += '</ul>';
        room_details += room_facilities_html;
    }
    $('.room-modal-details').html(room_details);

    $('#roomInfoModal').modal('show');
};

jQuery(function ($) {
    if (isUsitour && countdown_hotel.indexOf(hotelId) > -1) {
        if (getCookie('currentTime')) {
            var now = new Date().getTime();
            var before = getCookie('currentTime');
            var pasttime = now - before;

            var minute = Math.ceil(pasttime / 1000 / 60);
            countdown('countdown', 978 - minute, 24);
        } else {
            countdown('countdown', 978, 0);
        }
    }

    if (isHotelApp) {
        $('#master-main').addClass('usitrip-pc-bg');
    }
    window.onbeforeunload = function (event) {
        if (sxhr && sxhr.readyState !== 4) {
            sxhr.abort();
        }
        if (shxhr && shxhr.readyState !== 4) {
            shxhr.abort();
        }
        if (srxhr && srxhr.readyState !== 4) {
            srxhr.abort();
        }
    };

    initialization();

    $('#hotel-rating').html('&nbsp;&nbsp;' + buildRating(hotelRating, 'usitrip-hover-tag'));
    $('#img-hotel-rating').html(buildRating(hotelRating, hotelCategory));

    $('.map-input').hide();
    $('.mcontrols').focus(function () {
        $('.map-input').show();
    });

    $('#declineBtn').on('click', function () {
        $('#declineBtn').button('loading');
    });

    $('#checkin').datepicker({
        startDate: '0d',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    var checkoutMin = checkin ? checkin : '1d';
    $('#checkout').datepicker({
        startDate: checkoutMin,
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    //detail section stay on top with scroll begin
    var goToSection = $('#go-to-section');
    var floatTop = $('#float-top');
    var pos = goToSection.position();
    $(window).scroll(function () {
        var windowpos = $(window).scrollTop();
        if (windowpos >= (pos.top + 10 + goToSection.height())) {
            floatTop.removeClass('hidden');
        } else {
            if (!floatTop.hasClass('hidden')) {
                floatTop.addClass('hidden');
            }
        }
    });
    //back to top btn
    $('.go-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
    });
    //detail section stay on top with scroll end

    showRoomAvail(hotelId);

    if (typeof (Storage) !== 'undefined') {
        localStorage.setItem('checkin', $('#checkin').val());
        localStorage.setItem('checkout', $('#checkout').val());
    }

    //todo: delete
    // updatePlayer();
    $('#showGallery').on('click', function () {
        $.fancybox($('a.fancybox'), {padding: 0});
    });

    window.onresize = function () {
        updatePlayer();
    };

    $('.hotel-gallery-options > a').click(function () {
        $('.hotel-gallery-options > a.active').attr('class', '');
        $(this).attr('class', 'active');
        $('.hotel-gallery-player > img').attr('src', $(this).attr('href'));
    });

    $('a.control-prev').click(function () {
        prevGallery('hotel-gallery');
    });

    $('a.control-next').click(function () {
        nextGallery('hotel-gallery');
    });

    function autoPlayGallery () {
        nextGallery('hotel-gallery');
        nextGallery('room-gallery');
        setTimeout(autoPlayGallery, 3000);
    }

    // setTimeout(autoPlayGallery, 3000);
    //todo: end of delete

    // start room， adult， child count
    var _searchDesc = $('#hotel-room-guest-toggle');//房间人数显示框
    var _searchDialog = $('#searchDialog');
    _searchDesc.on('click', function () {
        if ($('#searchDialog').is(':hidden')) {
            _searchDialog.show();
            _searchDesc.addClass('person_desc_active');
        } else {
            _searchDialog.hide();
            _searchDesc.removeClass('person_desc_active');
        }
    });

    _searchDialog.on('click', function (e) {
        e.stopPropagation();
    });

    $('body').on('click', function (e) {//点击其他地方时，隐藏房间人数选择器
        if (e.target !== _searchDesc[0]) {
            _searchDialog.hide();
            _searchDesc.removeClass('person_desc_active');
        }
    });

    // start 选择房间数量，成人数量，儿童数量
    $('.btn-subtract, .btn-add').on('click', function () {
        var btn = $(this);
        var btnId = $(this).context.id;
        var targetId = btnId.replace(/Add|Subtract/g, '');

        input = btn.closest('.number-spinner').find('input');
        inputName = input[0].name;
        oldValue = input.val().trim(); // the value when user click on increase or decrease button
        var subBtn = $('#' + targetId + 'Subtract');
        var addBtn = $('#' + targetId + 'Add');
        if (btn.attr('data-dir') === 'up') {
            if (oldValue < input.attr('max')) {
                oldValue++;
                if (subBtn.hasClass('disabled-btn')) {
                    subBtn.removeClass('disabled-btn');
                    subBtn.attr('disabled', false);
                }
            }
            if (oldValue === input.attr('max')) {
                if (!addBtn.hasClass('disabled-btn')) {
                    addBtn.addClass('disabled-btn');
                    addBtn.attr('disabled', true);
                }
            } else {
                if (addBtn.hasClass('disabled-btn')) {
                    addBtn.removeClass('disabled-btn');
                    addBtn.attr('disabled', false);
                }
            }
        } else {
            if (oldValue > input.attr('min')) {
                oldValue--;
                if (addBtn.hasClass('disabled-btn')) {
                    addBtn.removeClass('disabled-btn');
                    addBtn.attr('disabled', false);
                }
            }
            if (oldValue === input.attr('min')) {
                if (!subBtn.hasClass('disabled-btn')) {
                    subBtn.addClass('disabled-btn');
                    subBtn.attr('disabled', true);
                }
            } else {
                if (subBtn.hasClass('disabled-btn')) {
                    subBtn.removeClass('disabled-btn');
                    subBtn.attr('disabled', false);
                }
            }
        }

        // 修改 spinner 数字
        input.val(oldValue); // now oldValue is new value

        // 修改上部数字显示和发送给后端的hidden input
        if (inputName === 'roomCount') {
            $('#tRoom').text(oldValue);
            $('#roomCount').val(oldValue);
            //检测修改单复数单词
            if (is_usitour && 1 === oldValue) {
                $('#tRoom_text').text('room, ');
            } else if (is_usitour && oldValue > 1) {
                $('#tRoom_text').text('rooms, ');
            }
        } else if (inputName === 'adultCount1') {
            $('#tAdult').text(oldValue);
            $('#adultCount1').val(oldValue);
            //检测修改单复数单词
            if (is_usitour && 1 === oldValue) {
                $('#tAdult_text').text('adult, ');
            } else if (is_usitour && oldValue > 1) {
                $('#tAdult_text').text('adults, ');
            }
        } else if (inputName === 'childCount1') {
            $('#tChild').text(oldValue);
            $('#childCount1').val(oldValue);
            //检测修改单复数单词
            if (is_usitour && oldValue <= 1) {
                $('#tChild_text').text('child, ');
            } else if (is_usitour && oldValue > 1) {
                $('#tChild_text').text('children, ');
            }
            if (0 === oldValue) {
                $('#childAgeWrapper').addClass('hide');
            } else {
                $('#childAgeWrapper').removeClass('hide');
                var content = $('#childAgeContent');
                var select_box = $('.child-age-select');
                var count = select_box.length;
                if (oldValue > count) {
                    for (var i = count; i < oldValue; i++) {
                        content.append(select_box.last().clone());
                    }
                } else {
                    for (var i = oldValue; i < count; i++) {
                        content.find('.child-age-select:last').remove();
                    }
                }
            }
        }
    });

    $('#tRoom').change(function () {
        $('#roomCount').val($(this).val());
        $('#tRoom').html($(this).val());
    });

    $('#tAdult').change(function () {
        $('#adultCount1').val($(this).val());
        $('#tAdult').html($(this).val());
    });

    $('#tChild').change(function () {
        $('#childCount1').val($(this).val());
        $('#tChild').html($(this).val());
    });
    //  end room， adult， child count

    //todo: delete
    // 全部加载完毕再移动图片，防止跳动
    $('.hotel-gallery-player img').css('margin-top', '-90px');

    // when modal dismiss. reopen the view all instead of a single image
    $('#hotelAllPicturesModal').on('hidden.bs.modal', function () {
        backToAllImg();
    });
    //also the view all img text click back to all img
    $('#view-all-img-text').click(function () {
        backToAllImg();
    });

    if (isUsitour && countdown_hotel.indexOf(hotelId) > -1) {
        $(window).unload(function () {
            if (!window.getCookie('currentTime')) {
                var now = new Date();
                var time = now.getTime();
                var expireTime = new Date(time + (8 * 60 * 60 * 1000));   //countdown记住上一次的生效时间八小时就过期
                document.cookie = 'currentTime=' + time + '; expires=' + expireTime;
            }
        });
    }

    //如果c端，展示为什么选择走四方，需要折叠酒店的overview
    if (isHotelApp) {
        //如果酒店描述超过144px
        var collapsedSizei = '144px';
        $('.description-right-overview').each(function () {
            var h = this.scrollHeight;
            var div = $(this);
            // 加上padding，整体大于160就显示view more
            if (h > 166) {
                div.css('height', collapsedSizei);
                div.after('<button id="moreFacility" class="btn btn-link hotel-description-more item" href="#">' + langHotel.check_more_desc + '</button>');
                var link = div.next();
                link.click(function (e) {
                    e.stopPropagation();
                    if (link.text() !== rooms_datac) {
                        link.text(langHotel.check_less_desc);
                        div.animate({
                            'height': h + 10
                        });
                    } else {
                        div.animate({
                            'height': collapsedSizei
                        });
                        link.text(langHotel.check_more_desc);
                    }
                    e.preventDefault();
                });
            }
        });
    }

    //如果酒店facility超过180px
    var collapsedSize = '190px';
    $('.facility-item').each(function () {
        var h = this.scrollHeight;
        var div = $(this);
        // 加上padding，整体大于204就显示view more
        if (h > 204) {
            div.css('height', collapsedSize);
            div.after('<button id="moreFacility" class="btn btn-link hotel-facility-more facility-item" href="#">' + langHotel.more + '</button>');
            var link = div.next();
            link.click(function (e) {
                e.stopPropagation();
                if (link.text() !== langHotel.less) {
                    link.text(langHotel.less);
                    div.animate({
                        'height': h
                    });
                } else {
                    div.animate({
                        'height': collapsedSize
                    });
                    link.text(langHotel.more);
                }
                e.preventDefault();
            });
        }
    });

    $('.hotel-price').hide();

    $('.loadingdayprice').fadeOut(2500);
    $('.hotel-price').fadeIn(2500);

    $('.pic-pet').hover(function () {
        $('.pet-f-content').fadeIn();
    }, function () {
        $('.pet-f-content').fadeOut();
    });

    //area info动态样式
    if ($('.hotel-area-list').length > 0) {
        var area_wrapper = $('.hotel-area');
        var column_2 = $('.hotel-area-column-2');
        var area_toggler = $('.hotel-area-toggler');
        var break_list = Math.ceil($('.hotel-area-list').length / 2);

        column_2.append($('.hotel-area-list:nth-child(n+' + break_list + ')'));
        if (column_2.height() > 350) { // this height should be the same value as collapsed area_wrapper height
            area_wrapper.addClass('collapsed');
            area_toggler.removeClass('hidden');

            area_toggler.click(function (e) {
                e.preventDefault();
                area_wrapper.toggleClass('collapsed');
                if (area_wrapper.hasClass('collapsed')) {
                    $(this).text('Show more');
                } else {
                    $(this).text('Show less');
                }
            });
        }
        area_wrapper.css('visibility', 'visible');
    }

    if ($('.hotel-review').length > 0) {
        $('#review-slider').carousel();
    }

    if ($('.facilities-banner').children('.icon-wrapper').length > 1) {
        $('.facilities-banner').css('display', 'block');
    }

    if ($('.facility-group-more').length > 0) {
        $('.facility-group-more').click(function () {
            var $group_wrapper = $(this).parent();
            $group_wrapper.toggleClass('collapsed');

            if ($group_wrapper.hasClass('collapsed')) {
                $(this).text('Show more');
                $group_wrapper.find('.facility-group-item:nth-child(n+11)').css('display', 'none');
            } else {
                $(this).text('Show less');
                $group_wrapper.find('.facility-group-item:nth-child(n+11)').css('display', 'block');
            }
            return false;
        });
    }

});
