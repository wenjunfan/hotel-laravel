window.validateEmail = function($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
};

window.fnFooter = function() {
    //底部切换
    $('.js-foot-tab').mouseover(function() {
        var index = parseInt($(this).attr('index'));
        var hs = $('.m-auth-box-i').eq(index).position().top;
        var hy = $('.m-auth-box-i').eq(index).height();
        $('.m-auth-box').css('height', hy);
        $('.m-auth-box-ov').stop(true, true).animate({'top': -hs}, 200);
        $('.js-foot-tab').removeClass('active').eq(index).addClass('active');
    });

    //合作伙伴轮播
    var footSlide = $('#js-foot-slide');
    var btnLeft = footSlide.find('.m-coope-bnt-l');
    var btnRight = footSlide.find('.m-coope-bnt-r');
    var slideUl = footSlide.find('.m-coope-tab-list');
    var slideLi = slideUl.find('li');
    var len = slideLi.length;
    var index = 0;
    var w = slideLi.outerWidth(true);

    if (len > 8) {
        btnLeft.click(function() {
            if (index == 0) return false;
            index--;
            slideUl.stop(true, false).animate({left: -w * index});
        });

        btnRight.click(function() {
            if (index == len - 8) return false;
            index++;
            slideUl.stop(true, false).animate({left: -w * index});
        });
    } else {
        btnLeft.add(btnRight).hide();
        slideUl.css('margin-left', (1152 - w * len) / 2);
    }
};

window.updateLanguage = function(x) {
    var lang = x;
    if (typeof(Storage) !== 'undefined') {
        localStorage.setItem('lang', lang);
    }

    $.post('/lang/update', {
        '_token': $('meta[name="csrf_token"]').attr('content'),
        'lang': lang,
        'href': window.location.href,
    }, function(data) {
        if (data.success) {
            location.replace(data.url);
        }
    });
};

$(function() {
    var reg = new RegExp('(^|&)lang=([^&]*)(&|$)');
    var r = window.location.search.substr(1).match(reg);
    var passedLang = null;
    if (r != null) {
        passedLang = unescape(r[2]);
    }

    var storedLang = null;
    if (typeof(Storage) !== 'undefined') {
        storedLang = localStorage.getItem('lang');
    }

    $('#currentYear').text((new Date()).getFullYear());
    if (location.hostname.indexOf('usitour') === -1 && location.hostname.indexOf('supervacation') === -1 && storedLang != null && passedLang == null && storedLang != language) { // update lang if lang is not updated and no lang passed in url and has local data
        $.post('/lang/update', {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'lang': storedLang,
            'href': window.location.href,
        }, function(data) {
            if (data.success) {
                location.replace(data.url);
            }
        });
    } else {
        if (typeof(Storage) !== 'undefined')
            localStorage.setItem('lang', language);
    }

    if (isUsitour && language === null) {
        updateLanguage(1);
    }

    // sticky site notes and sales banner
    $(window).scroll(function() {
        var scrollTop = $(this).scrollTop();
        if (scrollTop > 55) {
            if ($('#salesBanner').length > 0) {
                $('#salesBanner').addClass('fixedBanner');
            } else {
                $('#siteNotes').addClass('fixedBanner');
            }
        } else if (scrollTop == 0) {
            if ($('#salesBanner').length > 0) {
                $('#salesBanner').removeClass('fixedBanner');
            } else {
                $('#siteNotes').removeClass('fixedBanner');
            }
        }
    });

    if ($('.m-auth-wrap').length) {
        fnFooter();
    }

    // 右侧的联系方式
    var returnLi = $('.contact-wrap .return-top-hover');
    $(window).scroll(function() {
        if ($(window).scrollTop() > 700) {
            returnLi.fadeIn(200);
        } else {
            returnLi.fadeOut(200);
        }
    }).resize(function() {
    });

    $('#Lang').val(language);
    $('.navbar-minimalize').click(function() {

        if ($('body').attr('class').indexOf('mini-navbar') != -1) {

            $('nav.navbar-static-top').css('width', '88%');

        } else {
            $('nav.navbar-static-top').css('width', '95%');
        }
    });

    if (window.location.href.indexOf('usitour') > -1 && language !== 2) {
        $('.if-en-lang-change-to-logo').html('<div class="m-logo"><a href="https://hotel.usitour.com"><img class="fl" src="https://www.usitour.com/tpl/en.usitrip.com/image/public/web_logo.png" alt="logo"></a></div>');
    }

});

window.closeNotice = function() {
    document.getElementById('siteNotes').style.display = 'none';
    $.get('/site-notes/close');
};