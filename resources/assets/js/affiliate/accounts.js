let app = new Vue({
    el: '#affiliateAccount',
    data: {
        users: users,
        userId: userId,
        isAdmin: isAdmin,
        from: from,
        to: to,
        keywords: keywords,
        now: moment(),
        csrfToken: $('[name="csrf_token"]').attr('content'),
        addComment: "",
        oldComment: "",
        addCommentUserId: 0,
        addCommentUserIndex: 0
    },
    mounted () {
        // {{--table插件 一定要写到mounted里，否则容易失效--}}
        $('.dataTables-accounts').DataTable({
            "order": [[0, "desc"]],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {
                    extend: 'excel',
                    title: '分销账户列表'
                },
                {
                    extend: 'pdf',
                    title: '分销账户列表'
                },
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ]
        });

        // watch date change
        $('#from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        }).on(
            'changeDate', () => {
                this.from = $('#from').val();
            }
        );

        $('#to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        }).on(
            'changeDate', () => {
                this.to = $('#to').val()
            }
        );

        setInterval(this.refreshToken, 3600000); // 1 hour
    },
    methods: {
        search () {
            let search_filter = $('.search-btn');
            search_filter.text('loading ...');
            search_filter.addClass('disabled');
            window.location.href = '/affiliate/accounts?from=' + this.from + '&to=' + this.to + '&keywords=' + this.keywords;
        },
        refreshToken () {
            $.get('refresh-csrf').done(function (data) {
                this.csrfToken = data;
            });
        },
        showAddCommentModal (obj, index) {
            let self = this;
            self.addCommentUserId = obj.partner_id;
            self.addComment = '';
            self.oldComment = obj.partner.remark === null ? "" : obj.partner.remark;
            $('#addCommentModalTitle').text('用户' + obj.distributor_name + ' 进程备注');
            $('#old_remark').html(self.oldComment);
            $('#addCommentModal').modal('show');
            self.addCommentUserIdIndex = index
        },
        addNewNote () {
            FullscreenSpinner.create();
            // redefine user id
            let authUser = this.userId;
            switch (authUser) {
                case '11173':
                    authUser = '1001';
                    break;
                case '10057':
                    authUser = '22';
                    break;
                case '10575':
                    authUser = '20';
                    break;
                case '10672':
                    authUser = '550';
                    break;
                case '11178':
                    authUser = '19';
                    break;
                default:
            }
            this.addComment = moment().format('YYYY-MM-DD HH:mm:ss') + ' ' + this.addComment + '  (' + authUser + ')';

            let self = this;
            $.post(window.usitrip.basePath + 'affiliate/accounts/update/' + self.addCommentUserId, {
                '_token': window.usitrip.csrfToken,
                'id': self.addCommentUserId,
                'remark': self.addComment,
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    $('#addCommentModal').modal('hide');
                    toastr.success("添加备注成功！", "用户id：" + self.addCommentUserId);
                    $('#remark-' + self.addCommentUserId).html(self.addComment + '\n' + self.oldComment);
                } else {
                    swal("更新失败", data.message, "error");
                }
            });
        },
        ratioToggle (id, ratio) {
            FullscreenSpinner.create();
            $.post(window.usitrip.basePath + 'affiliate/accounts/update/' + id, {
                '_token': this.csrfToken,
                'id': id,
                ratio: ratio,
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    toastr.success(id + "更新佣金成功");
                    $('#' + id + '_rate').html(ratio);
                } else {
                    swal("更新失败", data.message, "error");
                }
            });
        },
        changeAffiliateCommission (id, ratio) {
            let self = this;
            swal({
                    title: "请填写分销佣金大小",
                    text: "请填写0~0.15范围的小数（即0%至15%）",
                    type: "input",
                    showCancelButton: true,
                    cancelButtonText: "取消",
                    confirmButtonText: "确定",
                    closeOnConfirm: false,
                    animation: "slide-from-top",
                    inputPlaceholder: ratio
                },
                function (inputValue) {
                    if (inputValue === false) {
                        return false;
                    } else if (inputValue === "" || isNaN(inputValue)) {
                        swal.showInputError("请填写一个小数");
                        return false;
                    } else if (parseFloat(inputValue) < 0 || parseFloat(inputValue) > 0.15) {
                        swal.showInputError("请填写0~0.15范围的小数（0.05即是5%）");
                        return false;
                    } else {
                        swal("...", "正在修改这个账户的分销佣金", "success");
                        self.ratioToggle(id, inputValue);
                    }
                });
        },
        admin (id, adminStatus) {
            FullscreenSpinner.create();
            $.post(window.usitrip.basePath + 'affiliate/accounts/update/' + id, {
                '_token': this.csrfToken,
                'id': id,
                'admin': adminStatus,
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    swal({
                        title: (adminStatus === 4 ? "账户已经设为分销账户" : "账户已经关闭分销功能"),
                        text: "",
                        confirmButtonText: "确定",
                        type: "success"
                    }, function () {
                        location.reload();
                    });
                } else {
                    swal(data.message);
                }
            });
        },
        active (id, currentAdmin) {
            var adminStatus, text;
            if (currentAdmin === 4) {
                adminStatus = 5;
                text = '关闭';
            } else {
                adminStatus = 4;
                text = '激活'
            }
            let self = this;
            swal({
                    title: "确认要" + text + "这个用户的分销身份吗？",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "取消",
                    confirmButtonColor: "#dd6b55",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal("...", "正在" + text + "这个用户的分销身份", "success");
                        self.admin(id, adminStatus);
                    }
                });
        },
    },
});