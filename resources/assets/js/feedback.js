var feedbackApp = new Vue({
    el: '#app',
    data: {
        aboutPrice: '',
        suggestions: [],
        otherNotes: '',
        submitted: false,
    },
    methods: {
        submitForm() {
            if (this.aboutPrice && this.suggestions.length > 0) {
                var self = this;
                $.get('/feedback/submit', $('#feedbackForm').serialize(), function(data) {
                    self.submitted = true;
                })
                .fail(function(data) {
                    swal('You already submitted, thanks!')
                });
            }
        }
    }
})