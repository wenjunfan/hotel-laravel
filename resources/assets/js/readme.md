### 文件夹内容
- admin：公司内部管理员后台  
- affiliate：c端分销用户管理员后台  
- mobile：前台手机端页面   
- pc：前台pc端页面
- includes：各组件的样式文件

##

### 注意事项
- 该文件夹中所有的子文件夹（除了includes文件夹）都会在`public/assets/js`中自动创建；   
- 该文件夹及其子文件夹（除includes文件夹）中的文件都会自动编译到`public/assets/js`中的对应路径，文件名相同；   
- 若需要添加添加一个文件夹，但不需要编译该文件夹，则需在`webpack.mix.js`文件中的`js_folder_excludes`变量中添加该文件夹名；