function onProductClick(data) {
    if (typeof ga === 'function') {
        ga('ec:addImpression', {
            'id': data.id,
            'name': data.name,
            'variant': data.hasOwnProperty('variant') ? data.variant : '',
            'list': data.list,
            'position': data.hasOwnProperty('position') ? data.position : ''
        });

        ga('ec:addProduct', {
            'id': data.id,
            'name': data.name,
            'position': data.hasOwnProperty('position') ? data.position : 1
        });

        ga('ec:setAction', 'detail');

        ga('send', 'event', 'hotel', 'click', data.page);
    }
}

function addProductToCart(data) {
    if (typeof ga === 'function') {
        ga('ec:addProduct', {
            'id': data.id,
            'name': data.name,
            'brand': data.brand,
            'variant': data.hasOwnProperty('variant') ? data.variant : '',
            'price': data.price,
            'quantity': data.qty
        });

        ga('ec:setAction', 'add');

        ga('ec:addProduct', {
            'id': data.id,
            'name': data.name,
            'brand': data.brand,
            'variant': data.hasOwnProperty('variant') ? data.variant : '',
            'price': data.price,
            'quantity': data.qty
        });

        ga('ec:setAction', 'checkout', {
            'step': 1,
            'option': data.vendor
        });

        ga('send', 'event', 'room', 'click', 'add to cart');
    }
}

function onInfoComplete(data) {
    if (typeof ga === 'function') {
        ga('ec:addProduct', {
            'id': data.id,
            'name': data.name,
            'variant': data.hasOwnProperty('variant') ? data.variant : '',
            'brand': data.brand,
            'price': data.price,
            'quantity': data.qty
        });

        ga('ec:setAction', 'checkout', {'step': 2});

        ga('send', 'event', 'checkout', 'info complete', {
            hitCallback: function () {
                // Advance to next page.
            }
        });
    }
}

function onPaymentComplete(data) {
    if (typeof ga === 'function') {
        ga('ec:addProduct', {
            'id': data.id,
            'name': data.name,
            'brand': data.brand,
            'variant': data.hasOwnProperty('variant') ? data.variant : '',
            'price': data.price,
            'quantity': data.qty
        });

        ga('ec:setAction', 'checkout', {'step': 3});

        ga('send', 'event', 'checkout', 'payment complete', {
            hitCallback: function () {
                // Advance to next page.
            }
        });
    }
}

function onBookComplete(data) {
    if (typeof ga === 'function') {
        ga('ec:addProduct', {
            'id': data.roomName,
            'name': data.name,
            'category': data.country,
            'brand': data.brand,
            'variant': data.city,
            'price': data.price,
            'quantity': data.roomCount
        });

        ga('ec:setAction', 'purchase', {
            'id': data.id,
            'affiliation': data.affiliate,
            'revenue': data.price,
            'tax': data.tax,
            'shipping': data.dayCount
        });

        ga('send', 'event', 'checkout', 'book complete', {
            hitCallback: function () {
                // window.location.replace(data.url);
            }
        });
    }
}

function onCancelComplete(data) {
    if (typeof ga === 'function') {
        ga('ec:setAction', 'refund', {
            'id': data.id
        });

        ga('send', 'event', 'cancel', 'cancel complete');
    }
}

function onClickChat() {
    if (typeof ga === 'function') {
        var first = $(location).attr('pathname');
        first.toLowerCase();
        first = first.split("/")[1];
        first = first ? first : 'home';

        ga('send', 'event', 'chat', first);
    }
}