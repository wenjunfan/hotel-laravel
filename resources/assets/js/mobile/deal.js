import AutoComplete from '../includes/destination-autocomplete';

new Vue({
    el: '#app',
    components: {AutoComplete},
    data: {
        couponId: couponId,
        appUrl: appUrl,
        isEditing: false,
        editRooms: false,
        policyModalOpen: false,
        stickyTop: false,
        checkinMin: moment().add(1, 'days').format('YYYY-MM-DD'),
        checkoutMin: moment().add(2, 'days').format('YYYY-MM-DD'),
        destination: {
            name: '',
            id: '',
            city: '',
            scenicName: '',
            scenicId: '',
        },
        searchData: {
            checkin: checkin,
            checkout: checkout,
            roomCount: 1,
            adultCount1: 2,
            childCount1: 0,
        },
        childAges: []
    },
    mounted() {
        let self = this;
        this.destination.name = this.langStr === 'cn' ? '请输入目的地' : 'Where to go';

        let params = $('#search-form').serialize();
        $.post('/deal/price/' + this.couponId, {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'checkin': this.searchData.checkin,
            'checkout': this.searchData.checkout,
        }, function (data) {
            if (data.success) {
                if (data.hotels.length > 0) {
                    var hotels = data.hotels;
                    for (var i = 0; i < hotels.length; i++) {
                        var hotel = hotels[i];
                        var $hotelItem = $('#hotelItem' + hotel.id);
                        $hotelItem.find('.hotel-price').text(hotel.currency + hotel.beforeTax);
                        $hotelItem.find('.book-btn').attr('href', self.appUrl + '/hotel/' + hotel.id + '.html?s=' + hotel.sessionKey + '&' + params);
                        if (hotel.discount) {
                            var $label = $('#discountLabel' + hotel.id);
                            $label.html('<span>' + parseInt(hotel.discount * 100) + '%</span> off');
                            $label.show();
                        }
                    }
                }
            }
        });

        $('#dealsApp').on('click', function (e) {
            if (e.target !== $('.rooms-display') && $('.rooms-display').find(e.target).length === 0) {
                self.editRooms = false;
            }
        });

        $('.destination-toggler').on('click', function () {
            $('.destination-toggler').removeClass('active');
            $(this).addClass('active');
            $('html, body').animate({
                scrollTop: $($(this).attr('data-target')).offset().top - 40,
            }, 300);
            return false;
        });

        if ($('.destination-block').length > 0) {
            $(window).scroll(function () {
                self.stickyTop = $(this).scrollTop() > $('.destination-block').offset().top
            });
        }

        $('.destination-input').on('click', function () {
            $(this).removeClass('error');
        });

        $('.coupon-details a').click(function () {
            self.policyModalOpen = true;
        });
    },
    methods: {
        updateDestination(newDestination) {
            this.destination.name = newDestination.name;
            this.destination.id = newDestination.id;
            this.destination.scenicName = newDestination.scenicName;
            this.destination.scenicId = newDestination.scenicId;
        },
        submitSearch() {
            if (this.destination.id && this.searchData.checkin && this.searchData.checkout && this.searchData.roomCount) {
                location.href = this.appUrl + '/list/' + this.destination.id + '.html?' + $('#search-form').serialize();
            } else {
                if (!this.destination.id) {
                    $('.destination-input').addClass('error');
                }
                swal('Please type in valid values');
            }
        },
        changeAge: function (val, index, $event) {
            this.childAges[index] = $event.target.value;
        }
    },
    watch: {
        searchData: {
            handler: function (val) {
                var newDate = moment(val.checkin).add(1, 'days').format('YYYY-MM-DD');
                this.checkoutMin = newDate;
                this.searchData.checkout = newDate;

                let childCount = val.childCount1;
                let oldVal = this.childAges.length;
                if (childCount > oldVal) {
                    for (let i = oldVal; i < childCount; i++) {
                        this.childAges.push(12);
                    }
                } else {
                    this.childAges.splice(childCount, oldVal - childCount);
                }
            },
            deep: true,
        }
    },
});