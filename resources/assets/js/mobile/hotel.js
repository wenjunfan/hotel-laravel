// 引用 Vue 组件
import DatePicker from './components/date-picker.vue';
import HomeSelection from './components/home-selection.vue';
import RoomItem from './components/room-item.vue';
import HotelItem from './components/hotel-item.vue';

import HotelInfo from './components/hotel-info.vue';
import HotelImages from './components/hotel-images.vue';
import InfoHotel from './components/infowindow-hotel';
import RoomDialog from './components/room-dialog';
import SearchEditor from './components/search-editor';

var app = new Vue({
    el: '#app',
    components: {
        DatePicker,
        HomeSelection,
        RoomItem,
        HotelItem,
        HotelInfo,
        HotelImages,
        InfoHotel,
        RoomDialog,
        SearchEditor
    },
    data: {
        // static
        langHotel: langHotel,
        seeProvider: isSeeProvider === 'true',
        inputIconMap: ':fa fa-map-marker',
        orderPopup: false,//default false
        filterPopup: false,//default false
        searchInfo: searchInfo,
        countdown_hotel: countdown_hotel,
        // dynamic
        searchData: {
            checkin: searchInfo.checkin,
            checkout: searchInfo.checkout,
            roomCount: parseInt(roomCount),
            dayCount: parseInt(dayCount),
            adultCount1: parseInt(searchInfo.occupancies[0].adults),
            childCount1: parseInt(searchInfo.occupancies[0].children),
        },
        searchDataTemp: {
            checkin: searchInfo.checkin,
            checkout: searchInfo.checkout,
            roomCount: parseInt(roomCount),
            dayCount: parseInt(dayCount),
            adultCount1: parseInt(searchInfo.occupancies[0].adults),
            childCount1: parseInt(searchInfo.occupancies[0].children),
        },
        sessionKey: sessionKey,
        hotel: hotel,
        timeLength: 0,
        rooms: [],
        roomsLess: [],
        showMoreRoom: '',
        roomLimit: 10,
        policies: {},// pet etc
        imgUrlArray: imgUrlArray,
        recHotels: [],
        recSessionKey: '',
        petFriendly: true,
        facilities: facilities,
        hotelPolicy: {},
        hidePics: true,
        showRooms: false,
        showRecHotels: false,
        viewButton: '',
        Reorderid: Reorderid,
        hasCachedHotel: hasCachedHotel,
        dialogRoom: null,
        dialogOpen: false,
        editorOpen: false,

        roomxhr: null,
        hotelxhr: null,
        hotelsxhr: null,
        isApi: userEmail === 'api@117book.com',
        isAd: userEmail === 'ad@test.com',

        element: '',
        endTime: '',
        hours: '',
        mins: '',
        msLeft: '',
        time: '',
        minute: null,

        mapState: window.mapState,
        hotelMap: null,
        centerLatLng: null,
        showInfowindow: false,
        infoHotel: null, // hotel to be shown on info window
        originIcon: null,
        selectedIcon: null,
        selectedMaker: null,
        mapBounds: null,
        mapKey: mapKey,

        hotelFacilityOpen: false,
        hotelReviewOpen: false,
        extraImgPath: extraImgPath,
        rooms_key: '',
        childAges: childAges,
    },
    computed: {
        searchParams() {
            return 'checkin=' + this.searchData.checkin + '&checkout=' + this.searchData.checkout +
              '&dayCount=' + this.searchData.dayCount + '&roomCount=' + this.searchData.roomCount +
              '&adultCount1=' + this.searchData.adultCount1 + '&childCount1=' + this.searchData.childCount1 +
              '&rooms_key=' + this.rooms_key;
        },
        formattedCheckin() {
            return moment(this.searchData.checkin).format('ddd MMM DD');
        },
        formattedCheckout() {
            return moment(this.searchData.checkout).format('ddd MMM DD');
        },
        errorImg() {
            if (this.langId === 2) {
                if (this.isAA) {
                    return '/img/error/aa_img_error.gif';
                } else {
                    return '/img/error/tw_img_error.png';
                }
            } else {
                if (this.isUsitour) {
                    return '/img/error/usitour_img_error.png';
                } else if (this.is117book) {
                    let number = Math.random(1, 10);
                    return '/img/error/DefaultImage_' + number + '.jpg';
                } else {
                    return '/img/error/hotel_img_error.jpg';
                }
            }
        }
    },
    mounted() {
        let usitour_hotel_id = this.hotel.hotelId + '';
        //number to string can use above method
        if (isUsitour && this.countdown_hotel.indexOf(usitour_hotel_id) > -1) {
            if (window.getCookie('currentTime')) {
                let now = new Date().getTime();
                let before = window.getCookie('currentTime');
                let pasttime = now - before;
                let minute = Math.ceil(pasttime / 1000 / 60);
                this.countdown('countdown', 978 - minute, 24);
            } else {
                this.countdown('countdown', 978, 0);
            }
        }

        window.onbeforeunload = function() {
            if (this.roomxhr) this.roomxhr.abort();
            if (this.hotelxhr) this.hotelxhr.abort();
            if (this.hotelsxhr) this.hotelsxhr.abort();
        };

        if (isUsitour && this.countdown_hotel.indexOf(usitour_hotel_id) > -1) {
            window.onunload = function() {
                if (!window.getCookie('currentTime')) {
                    let now = new Date();
                    let time = now.getTime();
                    let expireTime = new Date(time + (8 * 60 * 60 * 1000));   //countdown记住上一次的生效时间八小时就过期
                    this.document.cookie = 'currentTime=' + time + '; expires=' + expireTime;
                }
            };
        }

        $('.appbar-title').html(this.langHotel.title);

        const self = this;

        if (this.hasCachedHotel === 1) {
            this.hotel.sessionKey = this.sessionKey;
            this.getRooms();
            this.getRecHotels();
        } else {
            this.hotelxhr = $.get('/hotel/new', $('#search-form').serialize(), function(data) {
                if (data.sessionKey) {
                    self.sessionKey = data.sessionKey;
                    self.hotel.sessionKey = data.sessionKey;
                    let hotel = Object.assign({}, self.hotel, data.hotel);
                    hotel.guestRating = self.hotel.guestRating;
                    hotel.guestReviewCount = self.hotel.guestReviewCount;
                    self.hotel = hotel;
                    //wait for updating data in the form
                    setTimeout(function () {
                        self.getRooms();
                        if (!self.Reorderid) {
                            self.getRecHotels();
                        }
                    })
                } else {
                    self.rooms = [];
                    self.showRooms = true;
                    self.showRecHotels = true;
                }
            });
        }

        // 少于三行description隐藏view more
        if (($('.real-desc').height()) < $('.clamp').height()) {
            $('.toggle-button').hide();
        } else {
            $('.toggle-button').show();
        }

        $('#app').prepend($('#mapWrapper'));
        $('#mapWrapper').hide();

        // add static map anchor
        // show mini map
        var langCode = 'en';
        if(page_lang == 2){
            langCode = 'zh-TW';
        }else if(page_lang == 0){
            langCode = 'zh-CN';
        }

        var mapUrl = 'https://maps.googleapis.com/maps/api/staticmap?center=' + this.hotel.latitude + ',' + this.hotel.longitude +
          '&zoom=12&markers=size:mid%7Ccolor:blue%7C' + this.hotel.latitude + ',' + this.hotel.longitude + '&size=' + (window.innerWidth - 30) + 'x65' + '&key=' + this.mapKey+ '&language=' +langCode;
        $('#mapAnchor img').attr('src', mapUrl);

        $('.more-facility-anchor').click(function () {
            $('html,body').animate({
                scrollTop: $('.hotel-info-facility-new').offset().top
            }, 800);
            self.hotelFacilityOpen = true;
            $('.hotel-info-facility-new').trigger('click');
        });
    },
    methods: {
        getRooms: function() {
            let self = this;
            this.roomxhr = $.get('/hotel/rooms', $('#search-form').serialize(), function(data) {
                if (data.policies && Object.keys(data.policies).length > 0) {
                    self.hotelPolicy = data.policies;
                    if (data.policies.petPolicies[0].indexOf('not allowed') > -1 || data.policies.petPolicies[0].indexOf('不可') > -1) {
                        self.petFriendly = false;
                    }
                }

                if (self.hotel.checkin_time) {
                    let policies = data.policies ? data.policies : {};
                    //merge in static policy
                    if (self.hotel.checkin_time) {
                        let hotel = self.hotel;
                        policies.checkin_time = hotel.checkin_time;
                        policies.checkout_time = hotel.checkout_time;
                        policies.child_policy = hotel.child_policy;
                        policies.pet_policy = hotel.pet_policy;
                        policies.cards_accepted = hotel.cards_accepted;
                    }
                    policies = Object.assign({}, policies, self.hotelPolicy);
                    self.hotelPolicy = policies;
                }

                if (data.rooms && data.rooms.length > 0 && data.rooms[0].length > 0) {
                    self.rooms_key = data.rooms_key;
                    let rooms = data.rooms[0];
                    self.rooms = rooms;
                    if (rooms && rooms.length > self.roomLimit) {
                        self.roomsLess = rooms.slice(0, self.roomLimit);
                        self.showMoreRoom = true;
                    } else {
                        self.roomsLess = rooms;
                    }

                    if (rooms != null && rooms[0]) {
                        if (rooms[0].beforeTax < self.hotel.minPrice) {
                            self.hotel.minPrice = rooms[0].beforeTax;
                        }
                    }
                }
                self.showRooms = true;
            });
        },
        goGuestReview: function() {
            if ($('.hotel-info-review').length > 0) {
                let self = this;
                $('html,body').animate({
                    scrollTop: $('.hotel-info-review').offset().top
                }, 800);
                self.hotelReviewOpen = true;
                $('.hotel-info-review').trigger('click');
            }
        },
        hotelBackBtn: function() {
            if (window.history.length > 3) {
                window.location.href = '/list/D' + this.hotel.hotelId + '.html';
            } else {
                window.goBackIfHistory();
                return false;
            }
        },
        getRecHotels: function() {
            let self = this;
            this.hotelsxhr = $.get('/hotel/recommend', $('#search-form').serialize(), function(data) {
                self.showRecHotels = true;
                if (data.hotels.length > 0) {
                    self.recHotels = data.hotels;
                    self.recSessionKey = data.sessionKey;
                    self.loadHotelsMap(); // load hotels map
                }
            });
        },
        changeDate: function(checkin, checkout) {
            this.searchDataTemp.checkin = checkin.toString();
            this.searchDataTemp.checkout = checkout.toString();
        },
        changeSelections: function(room, adult, child) {
            this.searchDataTemp.roomCount = room;
            this.searchDataTemp.adultCount1 = adult;
            this.searchDataTemp.childCount1 = child;
        },
        submitSearch: function() {
            let link = '/hotel/' + this.hotel.hotelId + '.html';
            let $searchForm = $('#search-form');
            $searchForm.attr('action', link);

            let newDate = new Date();
            let maxdm = newDate.toISOString().substr(4, 6);
            let maxCheckin = (newDate.getFullYear()+2)+ maxdm;

            if(this.searchDataTemp.checkin > maxCheckin){
                toastr.error(this.langHotel.maxCheckinText, 'Error');
                return;
            }

            let dayCount = this.calculateDays (this.searchDataTemp.checkin, this.searchDataTemp.checkout);

            if (dayCount > 30) {
                toastr.error(this.langHotel.maxCheckoutText, 'Error');
                return;
            }

            this.searchData = this.searchDataTemp;
            this.searchData.dayCount = dayCount;

            // set global search parameters cache
            window.setSearchCache(this.searchData.checkin, this.searchData.checkout, this.searchData.roomCount, this.searchData.adultCount1, this.searchData.childCount1);

            setTimeout(function() {
                $searchForm.submit();
            }, 300);
        },
        calculateDays (checkInDate, checkOutDate) {
            let date1 = new Date(checkInDate);
            let date2 = new Date(checkOutDate);
            let date3 = date2.getTime() - date1.getTime();
            return Math.floor(date3 / (24 * 3600 * 1000));
        },
        closePicsPop: function(val) {
            let self = this;
            window.lockBody(false);
            self.hidePics = val;
        },
        toggleClamp() {
            let hotelDescription = $('.hotel-description p');
            if (hotelDescription.hasClass('clamp')) {
                hotelDescription.removeClass('clamp');
                this.viewButton = this.langHotel.viewLess;
            } else {
                hotelDescription.addClass('clamp');
                this.viewButton = this.langHotel.viewMore;
            }
        },
        countdown: function(elementName, minutes, seconds) {
            this.element = document.getElementById(elementName);
            this.endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
            this.updateTimer();
        },
        twoDigits: function(n) {
            return (n <= 9 ? '0' + n : n);
        },
        updateTimer: function() {
            let msLeft = this.endTime - (+new Date);
            if (msLeft < 1000) {
                this.countdown('countdown', 978, 0); //redo after times out
            } else {
                let time = new Date(msLeft);
                let hours = time.getUTCHours();
                let mins = time.getUTCMinutes();
                this.element.innerHTML = (hours ? hours + ' : ' + this.twoDigits(mins) : '00 : ' + mins) + ' : ' + this.twoDigits(time.getUTCSeconds());
                setTimeout(this.updateTimer, time.getUTCMilliseconds() + 500);
            }
        },
        initMap() {
            this.mapBounds = new google.maps.LatLngBounds();
            this.originIcon = {
                url: 'https://s3-us-west-1.amazonaws.com/usitrip/map/infor_m-01.png',
                labelOrigin: new google.maps.Point(25, 12),
            };
            this.selectedIcon = {
                url: 'https://s3-us-west-1.amazonaws.com/usitrip/map/infor_m-03.png',
                labelOrigin: new google.maps.Point(30, 13),
            };
            this.hotelMap = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_TOP
                },
                zoomControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_TOP
                }
            });
        },
        loadHotelsMap() {
            this.centerLatLng = {lat: this.hotel.latitude, lng: this.hotel.longitude};

            if (this.hotelMap) {
                this.hotelMap.setCenter(this.centerLatLng);
                this.addMapMarker(this.hotel, true);

                if (this.recHotels.length > 0) {
                    let self = this;
                    this.recHotels.forEach(function(hotel) {
                        if (hotel.latitude && hotel.longitude) {
                            hotel.sessionKey = self.recSessionKey;
                            self.addMapMarker(hotel, false);
                        }
                    })
                }
            }
        },
        toggleMap() {
            if (this.centerLatLng && typeof google !== 'undefined') { // if loaded hotel map
                this.showMap = !this.showMap;
                if (this.showMap) {
                    $('#mapWrapper').show();
                    $('.content-wrapper').hide();
                    this.hotelMap.fitBounds(this.mapBounds);
                    $('body').css({
                        overflow: 'hidden',
                        position: 'fixed',
                    });
                } else { // should not show map until finished loading
                    $('#mapWrapper').hide();
                    $('.content-wrapper').show();
                    $('body').css({
                        overflow: 'auto',
                        position: 'relative',
                    });
                }
            }
        },
        addMapMarker(hotel, isCenter) {
            let self = this;
            let position = {lat: hotel.latitude, lng: hotel.longitude};
            let marker = new google.maps.Marker({
                position: position,
                map: this.hotelMap,
                label: {
                    text: (hotel.currency === 'USD' ? '$' : hotel.currency) + hotel.minPrice,
                    color: 'white',
                    fontSize: '15px',
                },
                icon: this.originIcon,
            });
            this.mapBounds.extend(position); // extend map
            marker.addListener('click', function() {
                if (self.selectedMaker != this) {
                    var label = this.getLabel();
                    label.fontSize = '18px';
                    this.setIcon(self.selectedIcon);
                    this.setLabel(label);
                    this.setZIndex(self.highestZIndex);
                    if (self.selectedMaker != null) {
                        var label = self.selectedMaker.getLabel();
                        label.fontSize = '15px';
                        self.selectedMaker.setIcon(self.originIcon);
                        self.selectedMaker.setLabel(label);
                        self.selectedMaker.setZIndex(self.selectedMaker.opt);
                    }
                    self.selectedMaker = this;
                }

                self.infoHotel = hotel;
                self.showInfowindow = true;
            });

            if (isCenter) {
                new google.maps.event.trigger(marker, 'click');
            }
        },
        toggleRoomDialog(show, room = null) {
            if (show && room && room.extraInfo && room.extraInfo.room_images && room.extraInfo.room_images.length > 1) {
                this.dialogRoom = room;
                this.dialogOpen = true;
            } else {
                this.dialogOpen = false;
            }
        },
        toggleSearchEditor(open) {
            this.editorOpen = open;
        },
        toggleHotelFacility() {
            this.hotelFacilityOpen = !this.hotelFacilityOpen;
        },
        toggleHotelReview() {
            this.hotelReviewOpen = !this.hotelReviewOpen;
        },
    },
    watch: {
        'mapState.initMap'(val) {
            if (val) {
                this.initMap();
            }
        },
    },

});
