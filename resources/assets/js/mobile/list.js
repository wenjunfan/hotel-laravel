// 引用 Vue 组件
import DatePicker from './components/date-picker.vue';
import AutoComplete from './components/auto-complete.vue';
import HomeSelection from './components/home-selection.vue';
import HotelItem from './components/hotel-item.vue';
import InfoHotel from './components/infowindow-hotel.vue';
import OrderPopup from './components/order-popup.vue';
import FilterPopup from './components/list-filter-popup.vue';
import SearchPopup from './components/search-popup.vue';
import HomeInput from './components/home-input.vue';

var app = new Vue({
    el: '#app',

    components: {
        DatePicker,
        HomeInput,
        AutoComplete,
        HomeSelection,
        HotelItem,
        InfoHotel,
        OrderPopup,
        FilterPopup,
        SearchPopup,
    },

    data: {
        searchPopup: false,
        districts: [],
        airports: [],
        zones: [],
        locations: [],
        universities: [],
        chainNames: [],

        inputIconMap: ':fa fa-map-marker',
        sortIcon: {
            asc: 'fa fa-long-arrow-alt-up',
            desc: 'fa fa-long-arrow-alt-down',
        },
        langList: langList,
        arrowIcon: 'keyboard_arrow_down',

        searchedHotel: '',
        isHotelSearch: false,
        recommendHotels: '',
        searchedHotelSessionKey: '',
        recHotelSessionKey: '',
        // follow name conventions in other two list pages
        searchData: {
            checkin: checkIn,
            checkout: checkOut,
            roomCount: roomCount ? parseInt(roomCount) : 1,
            dayCount: dayCount ? parseInt(dayCount) : 1,
            adultCount1: adultCount ? parseInt(adultCount) : 2,
            childCount1: childCount? parseInt(childCount) : 0,
        },
        searchDataTemp: {
            checkin: checkIn,
            checkout: checkOut,
            roomCount: roomCount ? parseInt(roomCount) : 1,
            dayCount: dayCount ? parseInt(dayCount) : 1,
            adultCount1: adultCount ? parseInt(adultCount) : 2,
            childCount1: childCount? parseInt(childCount) : 0,
        },
        // filters for filter popup
        filterData: {
            keyword: key,
            orderBy: orderBy,
            sortBy: sortBy,
            price: price,
            rating: rating,
            scenicIds: scenicAreaArr,
            scenicNames: scenicName ? [scenicName] : [],
            chainNames: chainNames,
            showAllDiscount: showAllDiscount,
        },

        destination: {
            name: destinationName,
            id: destinationId,
            city: destinationCity,
            scenicName: scenicName,
            scenicId: scenicId,
        },
        destinationTemp: {
            name: destinationName,
            id: destinationId,
            city: destinationCity,
            scenicName: scenicName,
            scenicId: scenicId,
        },

        // filters to apply
        isFilter: false,
        orderPopup: false,//default false
        filterPopup: false,//default false
        disableFilter: false,

        hotelChains: [],

        hotelSessionKey: '', // to jump hotel
        showSubDetail: false,
        subscriberEmail: '',
        subscribeMsg: '',

        hotelxhr: null,
        tmpxhr: null,
        scenicxhr: null,
        hotelsxhr: null,
        filterxhr: null,
        fmxhr: null,
        interval: null,

        tempHotelIndex: null,
        tempHotelLength: 0,
        tempHotels: [],
        loadingTempHotels: [], // tmp hotels showing on page
        currentPage: currentPage,
        totalHotels: 0,
        numLimit: 10,
        isLoading: true, // show three dots
        showPages: false,
        showTempHotels: false,
        showRecHotelsBlock: false,
        showSearchedHotel: false,
        showSearch: false,
        isWechat: false,
        scenics: [],
        selectedScenic: null,

        mapState: window.mapState,
        showMap: false,
        showInfowindow: false,
        centerHotel: null, // hotel to center map
        infoHotel: null, // hotel to be shown on info window
        hotelMap: null,
        markers: [],
        highestZIndex: 99,
        originIcon: null,
        selectedIcon: null,
        selectedMaker: null,
        mapBounds: null,
        mapHotels: [],

        stickyTop: false,
        childAges: childAges,
    },

    computed: {
        labelTotalHotels () {
            if (this.showRecHotelsBlock) {
                if (this.totalHotels > 0) {
                    return this.totalHotels;
                } else {
                    return '';
                }
            } else {
                return '...';
            }
        },
        formattedCheckin () {
            return moment(this.searchData.checkin).format('ddd MMM DD');
        },
        formattedCheckout () {
            return moment(this.searchData.checkout).format('ddd MMM DD');
        },
        errorImg() {
            if (this.langId === 2) {
                if (this.isAA) {
                    return '/img/error/aa_img_error.gif';
                } else {
                    return '/img/error/tw_img_error.png';
                }
            } else {
                if (this.isUsitour) {
                    return '/img/error/usitour_img_error.png';
                } else if (this.is117book) {
                    let number = Math.random(1, 10);
                    return '/img/error/DefaultImage_' + number + '.jpg';
                } else {
                    return '/img/error/hotel_img_error.jpg';
                }
            }
        }
    },

    mounted () {
        if (this.isUsitour) {
            $('#sortBtn').insertBefore($('#filterBtn'));
        }

        $('.appbar-title').html(this.langList.title);
        window.onbeforeunload = function () {
            if (this.tmpxhr) this.tmpxhr.abort();
            if (this.hotelxhr) this.hotelxhr.abort();
            if (this.hotelsxhr) this.hotelsxhr.abort();
            if (this.filterxhr) this.filterxhr.abort();
            if (this.scenicxhr) this.scenicxhr.abort();
        };

        $('#app').prepend($('#mapWrapper'));
        $('#mapWrapper').hide();

        if (this.filterData.price.length > 1) { // preselect price in the middle min and max
            var min = this.filterData.price[0];
            var max = this.filterData.price[1];
            for (var i = 1; i <= 5; i++) {
                if (i > min && i < max) {
                    this.filterData.price.push(i);
                }
            }
        }

        var self = this;
        self.langList.hintTextGo = self.destination.scenicName ? self.destination.scenicName : self.destination.name;

        if (this.destination.id.indexOf('D') > -1) {
            this.isHotelSearch = false;
            this.searchRecommendHotels();
            this.getScenic(this.destination.id.slice(1));
        } else {
            this.isHotelSearch = true;
            this.hotelxhr = $.get('/list/hotel', $('#search-form').serialize(), function (data) {
                var hotel = data.hotel;
                self.searchedHotel = hotel;
                self.showSearchedHotel = true;
                self.searchedHotelSessionKey = data.sessionKey;

                if (hotel.latitude && hotel.longitude) {
                    // map center will be the searched hotel
                    self.centerHotel = hotel;
                }

                if (hotel.desId) {
                    self.getScenic(hotel.desId);
                    self.searchRecommendHotels('H' + hotel.hotelId);
                }
            });
        }

        this.isWechat = this._isWechatBrowser();

        if (this.mapState.initMap) {
            this.loadMap();
        }

        $(window).scroll(function () {
            if ($(this).scrollTop() > $('.hotels-block').offset().top) {
                self.stickyTop = true;
                if (!$('#filterBlock').hasClass('sticky')) {
                    $('#filterBlock').addClass('sticky');
                }
            } else {
                $('#filterBlock').removeClass('sticky');
                self.stickyTop = false;
            }
        })
    },

    watch: {
        'mapState.initMap' (val) {
            if (val) {
                this.loadMap();
            }
        },
    },

    methods: {
        changeDate: function (checkin, checkout) {
            this.searchDataTemp.checkin = checkin.toString();
            this.searchDataTemp.checkout = checkout.toString();
        },
        changeSelections: function (room, adult, child) {
            this.searchDataTemp.roomCount = room;
            this.searchDataTemp.adultCount1 = adult;
            this.searchDataTemp.childCount1 = child;
        },
        submitSearch: function () {
            if (typeof this.destination.name !== 'string' || !this.destination.name) {
                toastr.error(this.langList.goingTo + '?', 'Error');
                return;
            }

            if (moment(this.searchDataTemp.checkout) > moment(this.searchDataTemp.checkin).add(30, 'days')) {
                toastr.error(this.langList.datesOverRange);
                return;
            }

            // TO TEST before submit ,check check-in  < today and check-out < tomorrow
            this.searchDataTemp.checkin = window.checkDate(this.searchDataTemp.checkin, 'checkin', 1);
            this.searchDataTemp.checkout = window.checkDate(this.searchDataTemp.checkout, 'checkout', 2);

            let newDate = new Date();
            let maxdm = newDate.toISOString().substr(4, 6);
            let maxCheckin = (newDate.getFullYear() + 2) + maxdm;

            if (this.searchDataTemp.checkin > maxCheckin) {
                toastr.error(this.langList.maxCheckinText, 'Error');
                return;
            }

            let dayCount = this.calculateDays(this.searchDataTemp.checkin, this.searchDataTemp.checkout);

            if (dayCount > 30) {
                toastr.error(this.langList.maxCheckoutText, 'Error');
                return;
            }

            this.searchData = this.searchDataTemp;
            this.searchData.dayCount = dayCount;
            this.destination =  this.destinationTemp;

            // set global search parameters cache
            window.setSearchCache(this.searchData.checkin, this.searchData.checkout, this.searchData.roomCount, this.searchData.adultCount1, this.searchData.childCount1);

            let $searchForm = $('#search-form');
            let actionUrl = '/list/' + this.destination.id + '.html';
            $searchForm.attr('action', actionUrl);

            setTimeout(function () {
                $searchForm.submit();
            }, 300);
        },
        calculateDays (checkInDate, checkOutDate) {
            let date1 = new Date(checkInDate);
            let date2 = new Date(checkOutDate);
            let date3 = date2.getTime() - date1.getTime();
            return Math.floor(date3 / (24 * 3600 * 1000));
        },
        setOrderPopupStatus: function (status) {
            this.orderPopup = status;
        },

        setFilterPopupStatus: function (status) {
            window.lockBody(status);
            this.filterPopup = status;
        },

        subscribe () {
            let self = this;
            var emailvalid = this.subscriberEmail.trim();
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var isEmailValid = emailReg.test(emailvalid);

            if (isEmailValid && emailvalid !== '') {
                if (shouldTrack) {
                    gtag_report_conversion_subscribe();
                }

                let subBtn = $('.subscribe-button');
                subBtn.attr('disabled', true);
                subBtn.css('background-color', 'slategrey');
                subBtn.text('......');
                $.post('/subscribe', {
                    '_token': $('meta[name="csrf_token"]').attr('content'),
                    'email': this.subscriberEmail.trim(),
                    'pos': 4,
                }, function (data) {
                    if (data.success) {
                        if (typeof(Storage) !== 'undefined') {
                            localStorage.setItem('subEmail', 1);
                        }
                        if (data.message === 'sub01') {
                            self.subscribeMsg = self.langList.subedBefore;
                        } else {
                            self.subscribeMsg = self.langList.subedSuccessfuly;
                        }
                    } else {
                        subBtn.attr('disabled', false);
                        subBtn.css('background-color', '#2196f3');
                        subBtn.text(self.langList.subscribe);
                    }
                });
            } else {
                // 订阅按钮邮件检测失败
                swal({
                    title: '',
                    text: self.langList.errorEmail,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'OK',
                });
            }
        },

        // necessary
        getScenic (desId) {
            let self = this;
            this.scenicxhr = $.get('/list/scenic', {
                destinationId: desId,
            }, function (data) {
                let scenics = data.data;
                self.scenics = scenics;

                let passedScenicIds = self.filterData.scenicIds;
                if (passedScenicIds.length === 1) {
                    self.selectedScenic = scenics.find(function (scenic) {
                        return scenic.id === passedScenicIds[0];
                    });
                }

                var districts = [], airports = [], zones = [], locations = [],
                  universities = [];

                let selectedScenic = scenics.filter(
                  x => self.filterData.scenicIds.indexOf(String(x.id)) > -1);

                self.filterData.scenicNames = selectedScenic.map((n) => n.name);

                for (let i = 0; i < scenics.length; i++) {
                    let scenic = scenics[i];
                    if (scenic.type === 'pnlDistrict') {
                        districts.push(scenic);
                    } else if (scenic.type === 'pnlAirport') {
                        airports.push(scenic);
                    } else if (scenic.type === 'pnlZone') {
                        zones.push(scenic);
                    } else if (scenic.type === 'pnlLocation') {
                        locations.push(scenic);
                    } else if (scenic.type === 'pnlUniversity') {
                        universities.push(scenic);
                    }
                }

                self.airports = airports;
                self.districts = districts;
                self.zones = zones;
                self.locations = locations;
                self.universities = universities;
                self.disableFilter = false;
            });
        },

        searchRecommendHotels (destinationId) {
            var self = this;
            if (!destinationId) {
                destinationId = this.destination.id;
            }

            // loading temp hotels
            this.tmpxhr = $.get('/list/static', {
                desId: destinationId,
            }, function (data) {
                if (self.hotelsxhr && self.hotelsxhr.readyState !== 4 && data.length !== 0) {
                    self.showTempHotels = true;
                    self.tempHotelIndex = 0;
                    self.tempHotelLength = data.hotels ? data.hotels.length : 0;
                    self.tempHotels = data.hotels;
                    self.interval = setInterval(self.insertHotel, 1500);
                    setTimeout(function () {
                        self.isLoading = false;
                    }, 1500);
                }
            });

            // get recommend hotels
            this.hotelsxhr = $.get('/list/recommend', $('#search-form').serialize(), function (data) {
                if (data.length !== 0 && !data.errorId) { // if has data and not failed
                    self.recHotelSessionKey = data.sessionKey;
                    if (self.destination.scenicId) { // passed searched scenic to scenic filter
                        self.filterData.scenicIds.push(self.destination.scenicId);
                    }

                    if (self.tmpxhr && self.tmpxhr.readyState !== 4) {
                        self.tmpxhr.abort();
                    }

                    self.showTempHotels = false;
                    self.isLoading = true;

                    self.updatePage(self.currentPage, data.sessionKey, true); // show the first page
                } else {
                    self.showTempHotels = false;
                    self.showRecHotelsBlock = true;
                }
            });
        },

        insertHotel () { // interval loading tmp hotels
            if (this.tempHotelIndex >= this.tempHotelLength) {
                this.tempHotelIndex = 0;
            }

            var field = this.tempHotels[this.tempHotelIndex];
            this.loadingTempHotels.unshift(field);

            this.tempHotelIndex++;
        },

        applyFilters (filtersData) {
            if (filtersData.price.length > 0 && filtersData.price.length < 5) {
                this.filterData.price = filtersData.price;
            } else {
                this.filterData.price = [];
            }

            if (filtersData.rating.length > 0 && filtersData.rating.length < 5) {
                this.filterData.rating = filtersData.rating;
            } else {
                this.filterData.rating = [];
            }

            this.filterData.scenicIds = filtersData.scenicIds.length > 0
              ? filtersData.scenicIds
              : [];
            if (this.filterData.scenicIds.length === 1) {
                var selectedScenicId = this.filterData.scenicIds[0];
                this.selectedScenic = this.scenics.find(function (scenic) {
                    return scenic.id === selectedScenicId;
                });
            } else {
                this.selectedScenic = null;
            }

            this.filterData.keyword = filtersData.keyword;
            this.filterData.chainNames = (filtersData.chainNames != undefined &&
              filtersData.chainNames.length > 0) ? filtersData.chainNames : [];

            this.currentPage = 1;

            this.filterHotels();
        },

        setOrder (orderObj) {
            this.filterData.sortBy = orderObj.sort;
            this.filterData.orderBy = orderObj.orderBy;

            this.filterHotels();
        },

        goPage (newPage) {
            this.currentPage = newPage;
            window.scrollTo(0, 0);
            this.isLoading = true;

            this.updatePage(newPage, this.recHotelSessionKey);
        },

        filterHotels () {
            if (!this.isWechat) {
                this.showRecHotelsBlock = false;
                this.isLoading = true;
                this.showPages = false;
            }

            this.updatePage(this.currentPage, this.recHotelSessionKey);
        },

        updatePage (page, sessionKey, firstTime) {
            this.clearMarkers();

            var self = this;
            self.showInfowindow = false;
            var roomCount = this.searchData.roomCount;

            if (this.filterxhr && this.filterxhr.readyState !== 4) {
                this.filterxhr.abort();
            }

            if (this.filterData.orderBy === 'discount') {
                this.filterData.sortBy = 'asc';
            }

            if (sessionKey) {
                // update url with new params
                var params = {
                    'desId': this.destination.id,
                    'checkin': this.searchData.checkin,
                    'checkout': this.searchData.checkout,
                    'roomCount': this.searchData.roomCount,
                    'adultCount1': this.searchData.adultCount1,
                    'childCount1': this.searchData.childCount1,
                    'desName': this.destination.name,
                    'desCity': this.destination.city,
                    'scenicId': this.destination.scenicId,
                    'scenicName': this.destination.scenicName,
                    'orderBy': this.filterData.orderBy,
                    'sort': this.filterData.sortBy,
                    'key': this.filterData.keyword,
                    'pagePos': page,
                    'numLimit': this.numLimit,
                    'rating': this.filterData.rating,
                    'price': this.filterData.price,
                    'scenicArea': this.filterData.scenicIds,
                    'chainNames': this.filterData.chainNames,
                    'discount': this.filterData.showAllDiscount,
                };

                var query = this.encodeQueryData(params);
                var url = location.pathname + '?' + query;

                if (!firstTime && this.isWechat) { // wechat browser does not support push state to history
                    location.href = url;
                    return;
                } else {
                    history.pushState({url: url, title: document.title}, document.title,
                      url);
                }

                var filters = {
                    '_token': $('meta[name=\'csrf_token\']').attr('content'),
                    'desId': this.destination.id,
                    'roomCount': this.searchData.roomCount,
                    'dayCount': this.searchData.dayCount,
                    'checkin': this.searchData.checkin,
                    'checkout': this.searchData.checkout,
                    'orderBy': this.filterData.orderBy,
                    'sort': this.filterData.sortBy,
                    'key': this.filterData.keyword,
                    'pagePos': page,
                    'numLimit': this.numLimit,
                    'rating': this.filterData.rating,
                    'price': this.filterData.price,
                    'scenicArea': this.filterData.scenicIds,
                    'chainNames': this.filterData.chainNames,
                };

                this.filterxhr = $.post('/list/filter/' + sessionKey, filters, function (data) {
                      self.isLoading = false;
                      self.showRecHotelsBlock = true;

                      if (!data.success) {
                          swal({
                              title: '',
                              text: self.langList.timeOut,
                              type: 'warning',
                              confirmButtonColor: '#DD6B55',
                              confirmButtonText: 'OK',
                          }).then(function () {
                              location.reload();
                          });
                      } else if (data.success) {
                          if (data.hotelChains) {
                              self.hotelChains = data.hotelChains;
                          }

                          if (data.hotels && data.hotels.length !== 0) {
                              // remove searched hotel from recommend hotels list
                              if (self.isHotelSearch) {
                                  for (var i = 0; i < data.hotels.length; i++) {
                                      if (self.searchedHotel && self.searchedHotel.hotelId === data.hotels[i].hotelId) { // move searchedHotel from recommend hotels
                                          data.hotels.splice(i, 1);
                                      }
                                  }
                              }

                              self.totalHotels = data.totalHotels;
                              let recHotels = data.hotels;
                              if (self.searchedHotel) {
                                recHotels = recHotels.filter(function (hotel) {
                                    return hotel.hotelId !== self.searchedHotel.hotelId
                                });
                              }
                              self.recommendHotels = recHotels;
                              self.mapHotels = data.hotels;
                              self.showPages = true;

                              if (!self.centerHotel) {
                                  self.centerHotel = data.hotels.find(function (hotel) {
                                      return hotel.latitude && hotel.longitude;
                                  });
                              }

                              if (self.hotelMap) {
                                  self.loadMap();
                                  self.addMarkers();
                              }

                              // insert ad
                              setTimeout(function () {
                                  $('#affiliateBox').insertAfter('.hotel-item:nth-child(2)');
                                  $('#affiliateBox').show();
                              }, 300);
                          } else {
                              self.totalHotels = 0;
                              self.recommendHotels = [];
                              self.mapHotels = [];
                          }
                      }
                });
            } else {
                this.isLoading = false;
                this.showRecHotelsBlock = true;
                this.totalHotels = 0;
                this.recommendHotels = [];
            }
        },

        loadMap () {
            this.mapBounds = new google.maps.LatLngBounds();
            this.originIcon = {
                url: 'https://s3-us-west-1.amazonaws.com/usitrip/map/infor_m-01.png',
                labelOrigin: new google.maps.Point(25, 12),
            };
            this.selectedIcon = {
                url: 'https://s3-us-west-1.amazonaws.com/usitrip/map/infor_m-03.png',
                labelOrigin: new google.maps.Point(30, 13),
            };
            this.initMap();
        },

        encodeQueryData (data) {
            var query = [];
            for (var d in data) {
                if (typeof data[d] === 'number' || data[d].length > 0) {
                    query.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
                }
            }
            return query.join('&');
        },

        _isWechatBrowser () {
            var ua = navigator.userAgent.toLowerCase();
            return (/micromessenger/.test(ua));
        },

        initMap () {
            var self = this;
            this.hotelMap = new google.maps.Map(document.getElementById('map'), {
                zoom: 10,
                gestureHandling: 'greedy',
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_TOP,
                },
                zoomControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_TOP,
                },
            });

            this.hotelMap.addListener('click', function () {
                self.showInfowindow = false;
                if (self.selectedMaker != null) {
                    var label = self.selectedMaker.getLabel();
                    label.fontSize = '15px';
                    self.selectedMaker.setIcon(self.originIcon);
                    self.selectedMaker.setLabel(label);
                    self.selectedMaker.setZIndex(self.selectedMaker.opt);
                    self.selectedMaker = null;
                }
            });

            this.hotelMap.addListener('zoom_changed', function () {
                if (self.hotelMap.getBounds()) {
                    self.filterHotelsByBounds();
                }
            });

            this.hotelMap.addListener('dragend', function () {
                if (self.hotelMap.getBounds()) {
                    self.filterHotelsByBounds();
                }
            });
        },

        clearMarkers () {
            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }
        },

        addMarkers () {
            if (this.mapHotels.length > 0) {
                var self = this;

                if (this.searchedHotel && this.searchedHotel.latitude &&
                  this.searchedHotel.longitude) { // if is hotel search, show searched hotel on map each time
                    this.mapHotels.push(this.searchedHotel);
                }

                this.mapHotels = this.mapHotels.filter(function (hotel) {
                    return hotel.minPrice > 0;
                });

                this.mapHotels.forEach(function (hotel, index) {
                    var position = new google.maps.LatLng(hotel.latitude, hotel.longitude);
                    var marker = new google.maps.Marker({
                        position: position,
                        label: {
                            text: (hotel.currency === 'USD' ? '$' : hotel.currency) + hotel.minPrice,
                            color: 'white',
                            fontSize: '15px',
                        },
                        icon: self.originIcon,
                        map: self.hotelMap,
                        opt: index,
                        zIndex: index,
                    });

                    self.markers.push(marker);
                    self.mapBounds.extend(position);

                    marker.addListener('click', function () {
                        if (self.selectedMaker !== this) {
                            var label = this.getLabel();
                            label.fontSize = '18px';
                            this.setIcon(self.selectedIcon);
                            this.setLabel(label);
                            this.setZIndex(self.highestZIndex);
                            if (self.selectedMaker != null) {
                                var label = self.selectedMaker.getLabel();
                                label.fontSize = '15px';
                                self.selectedMaker.setIcon(self.originIcon);
                                self.selectedMaker.setLabel(label);
                                self.selectedMaker.setZIndex(self.selectedMaker.opt);
                            }
                            self.selectedMaker = this;
                        }

                        self.infoHotel = hotel;
                        self.showInfowindow = true;
                    });
                });
            }
        },

        toggleMap () {
            let self = this;
            this.showMap = !this.showMap;
            if (this.showMap) {
                $('#mapWrapper').show();
                $('.content-wrapper').hide();
                $('body').css({
                    overflow: 'hidden',
                    position: 'fixed',
                });

                setTimeout(function () {
                    self.hotelMap.fitBounds(self.mapBounds);
                    self.clearMarkers();
                    self.filterHotelsByBounds(); // show all hotels in current map
                }, 300);
            } else if (this.showRecHotelsBlock) { // should not show map until finished loading
                $('#mapWrapper').hide();
                $('.content-wrapper').show();
                $('body').css({
                    overflow: 'auto',
                    position: 'relative',
                });
            }
        },

        setDestination: function (desObj) {
            this.destinationTemp.name = desObj.name;
            this.destinationTemp.id = desObj.id;
            this.destinationTemp.scenicName = desObj.scenicName ? desObj.scenicName : '';
            this.destinationTemp.scenicId = desObj.scenicId ? desObj.scenicId : '';
            this.langList.hintTextGo = (typeof desObj.name === 'string' ? desObj.name : desObj.name[this.langStr]);
        },
        setPopupClose: function (status) {
            window.lockBody(status);
            this.searchPopup = status;
        },
        openPopup: function (status) {
            window.lockBody(status);
            this.searchPopup = status;
        },

        filterHotelsByBounds () {
            if (this.fmxhr && this.fmxhr.readyState !== 4) {
                this.fmxhr.abort();
            }

            var bounds = this.hotelMap.getBounds();
            var northeast = bounds.getNorthEast(), southwest = bounds.getSouthWest(), zoom = this.hotelMap.getZoom();

            if (this.recHotelSessionKey) {
                var filters = {
                    '_token': $('meta[name=\'csrf_token\']').attr('content'),
                    'roomCount': this.searchData.roomCount,
                    'dayCount': this.searchData.dayCount,
                    'northeast_lat': northeast.lat(),
                    'northeast_lng': northeast.lng(),
                    'southwest_lat': southwest.lat(),
                    'southwest_lng': southwest.lng(),
                    'zoom': zoom,
                };

                var self = this;
                this.fmxhr = $.post('/list/map/filter/' + this.recHotelSessionKey, filters, function (data) {
                    if (!data.success) {
                        swal({
                            title: '',
                            text: self.langList.timeOut,
                            type: 'warning',
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'OK',
                        }).then(function () {
                            location.reload();
                        });
                    } else if (data.success) {
                        if (data.hotels && data.hotels.length !== 0) {
                            if (self.isHotelSearch && self.searchedHotel) {
                                for (var i = 0; i < data.hotels.length; i++) {
                                    if (self.searchedHotel.hotelId === data.hotels[i].hotelId) {
                                        data.hotels.splice(i, 1);
                                    }
                                }
                            }

                            self.mapHotels = data.hotels;
                            self.addMarkers();
                        }
                    }
                });
            }
        },

        showSearchBlock () {
            this.showSearch = true;
            $('html,body').animate({
                scrollTop: 0,
            }, 1000);
        },
    },
});
