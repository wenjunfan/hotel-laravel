// 引用 Vue 组件
import DatePicker from './components/date-picker.vue'
import HomeInput from './components/home-input.vue'
import HomeSelection from './components/home-selection.vue'
import HomePic from './components/home-pic.vue'
import HomeScenicPic from './components/home-scenic-pic.vue'
import SearchPopup from './components/search-popup.vue'
import IconButtons from './components/icon-buttons'

var app = new Vue({
    el: '#app',
    components: {
        DatePicker,
        HomeInput,
        HomeSelection,
        HomePic,
        HomeScenicPic,
        SearchPopup,
        IconButtons,
    },
    data: {
        searchData: {
            checkInDate: '',
            checkOutDate: '',
            roomCount: parseInt(window.roomCount),
            adult: parseInt(window.adultCount),
            child: parseInt(window.childCount),
        },
        destination: {
            name: '',
            id: '',
            scenicName: '',
            scenicId: '',
        },
        checkInDate: window.checkInDate,
        checkOutDate: window.checkOutDate,
        autoClose: true,
        selectDate: '',
        inputIconMap: ":fa fa-map-marker",
        searchPopup: false,
        langHome: langHome,
        sessionKey: sessionKey,
        showSubDetail: false,
        subscriberEmail: '',
        subscribeMsg: '',
        childAges: [12],
    },
    methods: {
        changeDate: function (checkInDate, CheckOutDate) {
            this.searchData.checkInDate = checkInDate;
            this.searchData.checkOutDate = CheckOutDate;
        },
        setDestination: function (desObj) {
            this.destination.name = desObj.name;
            this.destination.id = desObj.id;
            this.destination.scenicName = desObj.scenicName ? desObj.scenicName : '';
            this.destination.scenicId = desObj.scenicId ? desObj.scenicId : '';
            this.langHome.hintTextGo = (typeof desObj.name === 'string' ? desObj.name : desObj.name[this.langStr]);
        },
        setPopupClose: function (status) {
            window.lockBody(status);
            this.searchPopup = status;
        },
        // 用户修改数量
        changeSelections: function (room, adult, child) {
            this.searchData.roomCount = room;
            this.searchData.adult = adult;
            this.searchData.child = child;
        },
        showDiscountHotels (desObj) {
            this.setDestination(desObj);
            this.submitSearch();
        },
        submitSearch: function () {
            let self = this;

            if (typeof this.destination.name === 'string' && this.destination.name === '') {
                toastr.error(this.langHome.goingTo + '?', 'Error');
                return;
            }

            // TO TEST before submit ,check check-in  < today and check-out < tomorrow
            self.searchData.checkInDate = window.checkDate(self.searchData.checkInDate, 'checkin', 1);
            self.searchData.checkOutDate = window.checkDate(self.searchData.checkOutDate, 'checkout', 2);

            let newDate = new Date();
            let maxdm = newDate.toISOString().substr(4, 6);
            let maxCheckin = (newDate.getFullYear() + 2) + maxdm;

            if (this.searchData.checkInDate > maxCheckin) {
                toastr.error(this.langHome.maxCheckinText, 'Error');
                return;
            }

            let dayCount = this.calculateDays(this.searchData.checkInDate, this.searchData.checkOutDate);

            if (dayCount > 30) {
                toastr.error(this.langHome.maxCheckoutText, 'Error');
                return;
            }

            // save search info to cache
            window.setSearchCache(this.searchData.checkInDate, this.searchData.checkOutDate, this.searchData.roomCount, this.searchData.adult, this.searchData.child);

            let $searchForm = $('#search-form');
            let actionUrl = '/list/' + this.destination.id + '.html';

            setTimeout(function () {
                $searchForm.attr('action', actionUrl);
                $searchForm.submit();
            }, 100);
        },
        calculateDays (checkInDate, checkOutDate) {
            let date1 = new Date(checkInDate);
            let date2 = new Date(checkOutDate);
            let date3 = date2.getTime() - date1.getTime();
            return Math.floor(date3 / (24 * 3600 * 1000));
        },
        openPopup: function (status) {
            window.lockBody(status);
            this.searchPopup = status;
        },
        subscribe () {
            let self = this;
            var emailvalid = this.subscriberEmail.trim();
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var isEmailValid = emailReg.test(emailvalid);

            if (isEmailValid && emailvalid !== '') {
                let SubBtn = $('.subscribe-button');
                if (shouldTrack) {
                    gtag_report_conversion_subscribe();
                }

                SubBtn.attr('disabled', true);
                SubBtn.text('......');
                SubBtn.css({
                    "background-color": "grey",
                    "font-size": "0.35rem",
                    "color": "white",
                    "text-align": "center",
                    "width": "1.5rem",
                });

                $.post("/subscribe", {
                    '_token': $('meta[name="csrf_token"]').attr('content'),
                    'email': this.subscriberEmail.trim(),
                    'pos': 3,
                }, function (data) {
                    if (data.success) {
                        if (typeof(Storage) !== 'undefined') {
                            localStorage.setItem('subEmail', 1);
                        }
                        if (data.message === 'sub01') {
                            self.subscribeMsg = self.langHome.subedBefore;
                        } else {
                            self.subscribeMsg = self.langHome.subedSuccessfuly;
                        }
                    } else {
                        SubBtn.attr('disabled', false);
                        SubBtn.css('background-color', '#2196f3');
                        SubBtn.text(self.langHome.subscribe)
                    }
                });
            } else {
                // 首页顶部订阅按钮邮件检测失败
                swal({
                    title: self.langHome.errorEmail,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'OK',
                })
            }
        },
    },
});
