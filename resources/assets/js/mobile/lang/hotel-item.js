export default {
    eachNightPrice: {
        'cn': '每晚均价',
        'tw': '每晚均價',
        'en': 'avg/night'
    },
    lowPrice: {
        'cn': '低价保障',
        'tw': '低價保障',
        'en': 'Price Guarantee'
    },
    freeCancel: {
        'cn': '免费取消',
        'tw': '免費取消',
        'en': 'Free Cancellation'
    },
    allInclusive: {
        'cn': '全包',
        'tw': '全包',
        'en': 'All Inclusive'
    },
    soldOut: {
        'cn': '选择日期已售完',
        'tw': '選擇日期已售完',
        'en': 'Sold Out'
    },
    guestRating: {
        'cn': '用户评分',
        'tw': '用戶評分',
        'en': 'Guest Rating'
    },
    confirmNow: {
        'cn': '立即确认',
        'tw': '立即確認',
        'en': 'Instantly Hotel Confirm'
    },
    noFee: {
        'cn': '无预订或信用卡手续费',
        'tw': '無預訂或信用卡手續費',
        'en': 'No Booking or Credit Card Fees'
    },
    downtown: {
        'cn': '市中心',
        'tw': '市中心',
        'en': 'Downtown'
    },
    review: {
        'cn': '点评',
        'tw': '點評',
        'en': 'reviews'
    }
};
