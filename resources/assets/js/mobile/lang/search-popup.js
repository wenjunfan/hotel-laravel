export default {
    close: {
        'cn': '关闭',
        'tw': '關閉',
        'en': "Close"
    },
    searchTitle: {
        'cn': '搜索酒店',
        'tw': '搜索饭店',
        'en': "Search Hotels"
    },
    hintTextGo: {
        'cn': '目的地或者酒店名称',
        'tw': '目的地或者饭店名稱',
        'en': "Destination, hotel name, airport ..."
    },
    goingTo: {
        'cn': '目的地',
        'tw': '目的地',
        'en': "Going To"
    },
    labelSearch: {
        'cn': '搜索',
        'tw': '搜索',
        'en': 'Search'
    },
    labelSearchIng: {
        'cn': '搜索中...',
        'tw': '搜索中...',
        'en': 'Searching...'
    },
    destination: {
        'cn': '目的地或者酒店名称',
        'tw': '目的地或者飯店名稱',
        'en': 'Destination, hotel name, airport ...'
    },
    hotDestinations: {
        'cn': '热门目的地',
        'tw': '熱門目的地',
        'en': 'Hot Destinations'
    },
    america: {
        'cn': '美洲',
        'tw': '美洲',
        'en': 'America'
    },
    europe: {
        'cn': '欧洲',
        'tw': '歐洲',
        'en': 'Europe'
    },
    oceania: {
        'cn': '大洋洲',
        'tw': '大洋洲',
        'en': 'Oceania'
    },
    asia: {
        'cn': '亚洲',
        'tw': '亞洲',
        'en': 'Asia'
    },
    africa: {
        'cn': '非洲',
        'tw': '非洲',
        'en': 'Africa'
    }
}