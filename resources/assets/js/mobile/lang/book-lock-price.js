export default {
    lock: {
        'cn': '锁定价格',
        'tw': '鎖定價格',
        'en': "Lock your price"
    },
    hurry: {
        'cn': '赶快行动！价格与供应情况随时可能变动！',
        'tw': '趕快行動！價格與供應情況隨時可能變動！',
        'en': "Don’t let this price get away! Never pay more for your hotels"
    },
}
