export default {
    hotel: {
        'cn': '酒店首页',
        'tw': '飯店首頁',
        'en': "Hotel Home"
    },
    searchTitle: {
        'cn': '搜索酒店',
        'tw': '搜索飯店',
        'en': "Search Hotels"
    },
    hotline: {
       'cn': '7X24小时服务热线',
       'tw': '24小時專線',
       'en': '7/24 Customer Service'
    },
    us: {
        'cn': '美',
        'tw': '美國',
        'en': 'US'
    },
    cn: {
        'cn': '中',
        'tw': '台灣',
        'en': 'China'
    },
    assistant: {
        'cn': '在线咨询',
        'tw': '在線咨詢',
        'en': 'Online Assistant'
    },
    cancel: {
        'cn': '取消',
        'tw': '取消',
        'en': 'Cancel'
    }
}
