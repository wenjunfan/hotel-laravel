export default {
    adult: {
        'cn': '成人',
        'tw': '成人',
        'en': "Adults"
    },
    child: {
        'cn': '儿童',
        'tw': '兒童',
        'en': "Children"
    },
    room: {
        'cn': '客房',
        'tw': '客房',
        'en': "Rooms"
    },
    each: {
        'cn': '间',
        'tw': '間',
        'en': "R"
    },
    childAge: {
        'cn': '小孩年龄',
        'tw': '小孩年齡',
        'en': "Children's age"
    },
    yearsOld: {
        'cn': '岁',
        'tw': '歲',
        'en': ''
    }
}
