export default {
    hotelName: {
        'cn': '如：hilton',
        'tw': '如：hilton',
        'en': 'Ex.: hilton'
    },
    clear: {
        'cn': '全部清除',
        'tw': '全部清除',
        'en': 'Clear All'
    },
    close: {
        'cn': '关闭',
        'tw': '關閉',
        'en': 'Close'
    },
    search: {
        'cn': "搜索",
        'tw': "搜索",
        'en': "Search"
    },
    reset: {
        'cn': "重置",
        'tw': "重置",
        'en': "Reset"
    },
    filter: {
        'cn': "过滤",
        'tw': "過濾",
        'en': "Filter"
    },
    selectedFilter: {
        'cn': "已选过滤",
        'tw': "已選過濾",
        'en': "Selected Filter"
    },
    filterTitle: {
        'cn': "缩小搜索范围",
        'tw': "縮小搜索範圍",
        'en': "Narrow down search results"
    },
    searchTitle: {
        'cn': "按酒店名称或地址",
        'tw': "按飯店名稱或地址",
        'en': "Search by property name or address"
    },
    filterBy: {
        'cn': "酒店筛选方式",
        'tw': "飯店篩選方式",
        'en': "Filter property by"
    },
    class: {
        'cn': "酒店星级",
        'tw': "飯店星级",
        'en': "Property Class"
    },
    pricePerNight: {
        'cn': "酒店每晚价格",
        'tw': "飯店每晚價格",
        'en': "Price Per Night"
    },
    airport: {
        'cn': "机场",
        'tw': "機場",
        'en': "Airport"
    },
    attractions: {
        'cn': "景点",
        'tw': "景點",
        'en': "Attractions"
    },
    businessZone: {
        'cn': "商圈",
        'tw': "商圈",
        'en': "Business Zone"
    },
    administrativeArea: {
        'cn': "行政区",
        'tw': "行政區",
        'en': "Administrative Area"
    },
    universities: {
        'cn': "大学",
        'tw': "大學",
        'en': "Universities"
    },
    applyBtn: {
        'cn': '应用筛选',
        'tw': '應用篩選',
        'en': 'Apply Filter'
    },
    stars: {
        'cn': '星级',
        'tw': '星级',
        'en': 'Stars'
    },
    chain: {
        'cn': '连锁酒店',
        'tw': '連鎖飯店',
        'en': 'Chain'
    }
};
