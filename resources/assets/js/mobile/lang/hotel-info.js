export default {
    cancelPolicyHead: {
        'cn': "取消政策",
        'tw': "取消政策",
        'en': "Cancellation Policy"
    },

    cancelPolicy: {
        'cn': "不同类型的客房附带不同的取消预订和预先付费政策，选择上述客房时，请参阅“客房政策”。若提前离店，酒店将收取一定费用。预订不得取消不得修改房型 ，酒店将在预订之后立即扣款.",
        'tw': "不同類型的客房附帶不同的取消預訂和預先付費政策，選擇上述客房時，請參閱“客房政策”。若提前離店，飯店將收取一定費用。預訂不得取消不得修改房型 ，飯店將在預訂之後立即扣款.",
        'en': "Different room types are subject to different cancellation and prepayment policies. Please refer to the Room Policy when booking these rooms. In case of early departure, a certain fee will be charged. Reservations cannot be modified or transferred. Some reservations are non-refundable meaning that reservations cannot be cancelled without penalty. You will be charged immediately after booking."
    },

    onlinePriceHead: {
        'cn': "网上价格与实际价格",
        'tw': "网上价格与实际价格",
        'en': "Online Price and Actual Price"
    },

    onlinePrice: {
        'cn': "酒店实际价格通常与网站上的报价一致，除非出现明显的错误。价格可能会随时变化，但并不影响已接受的预订。走四方会尽最大努力为您服务，但网站上列出的一些酒店服务仍有可能报错价格。走四方明确保留对本网站上的任何定价错误和/或基于错误定价的待处理预订进行更正的权利。在此情况下，如有可能，您可选择以正确的价格保留待处理的预订，否则我们将取消您的预订而不扣除任何罚金.",
        'tw': "飯店實際價格通常與網站上的報價一致，除非出現明顯的錯誤。價格可能會隨時變化，但並不影響已接受的預訂。走四方會盡最大努力為您服務，但網站上列出的一些飯店服務仍有可能報錯價格。走四方明確保留對本網站上的任何定價錯誤和/或基於錯誤定價的待處理預訂進行更正的權利。在此情況下，如有可能，您可選擇以正確的價格保留待處理的預訂，否則我們將取消您的預訂而不扣除任何罰金.",
        'en': "The actual hotel price is usually the same as the offer price on the website, unless a noticeable error occurs. Prices may change at any time, but do not affect a confirmed reservation. usitrip will do our best to serve you, but some hotels or room types that are listed on the website are still likely to show the incorrect offer prices. It is expressly stated that we reserve the right to correct and handle any pricing error on this website and/or modify and adjust pending bookings if mispriced. In this case, if possible, you may choose to reserve the pending reservation at the corrected price, otherwise we will cancel your booking without any penalty."
    },

    bedTypeHead: {
        'cn': "房间床型与加床费用",
        'tw': "房間床型與加床費用",
        'en': "Room Type and Extra Bed Fee"
    },
    bedType: {
        'cn': "您预订酒店时所选择的床型（大床、双床等），走四方会发送至酒店申请安排，无法保证最终确认。预订三人及四人房，走四方展示的价格是加了允许第三人及第四人入住的费用，不包含加床费用。如果您到酒店要求加床，酒店可能需要收取额外加床费用.",
        'tw': "您預訂飯店時所選擇的床型（大床、雙床等），走四方會發送至飯店申請安排，無法保證最終確認。預訂三人及四人房，走四方展示的價格是加了允許第三人及第四人入住的費用，不包含加床費用。如果您到飯店要求加床，飯店可能需要收取額外加床費用.",
        'en': "The bedding type (e.g., king bed, double beds) you choose during booking will be sent to the hotel to be arranged. We cannot guarantee before a final confirmation is made by the hotel. The price shown on the website is the room cost allowing the number of people you’ve selected at reservation, which does not include extra bed fees. If you require an extra bed, additional charges may apply."
    },

    ccRefundTermHead: {
        'cn': "特殊条款",
        'tw': "特殊條款",
        'en': "Special Instructions"
    },
    ccRefundTerm: {
        'cn': "信用卡授权解除需时1-3个月。视不同国家、城市之银行操作时间而定 酒店已确认的订单，如在最晚取消时间前取消订单，并再重新预订，酒店可能会拒绝使用新价格, 根据各国不同情况，您可能还需要缴纳当地其他的税费.",
        'tw': "信用卡授權解除需時1-3個月。視不同國家、城市之銀行操作時間而定 飯店已確認的訂單，如在最晚取消時間前取消訂單，並再重新預訂，飯店可能會拒絕使用新價格, 根據各國不同情況，您可能還需要繳納當地其他的稅費.",
        'en': "Release of credit card authorization may take 1-3 months, depending on different countries, cities and banks. For a reservation has been confirmed with the hotel, if you cancel that order before the cancellation cut-off time and re-book, the hotel may reject the updated price. You may also need to pay other taxes and fees that may occur according to local requirements."
    },
    perRoomDetail: {
        'cn': "注意：订单中早餐类型以每个房型的备注为准",
        'tw': "註意：訂單中早餐類型以每個房型的備註為準",
        'en': "Note: The type of breakfast in the order is based on the commends of each room type"
    },
    hotelAreaInfo: {
        'cn': "附近信息",
        'tw': "附近信息",
        'en': "Area Info"
    },
    hotelAreaIntw: {
        'cn': "附近信息",
        'tw': "附近信息",
        'en': "Area Info"
    },
    hotelAmenities: {
        'cn': "酒店服务与设施",
        'tw': "饭店服務與設施",
        'en': "Hotel Services and Amenities"
    },
    hotelAmenititw: {
        'cn': "酒店服务与设施",
        'tw': "饭店服務與設施",
        'en': "Hotel Services and Amenities"
    },
    hotelAmenitiesHot: {
        'cn': "热门设施/服务",
        'tw': "熱門設施/服務",
        'en': "Popular Facility"
    },
    hotelAmenitiesHtw: {
        'cn': "热门设施/服务",
        'tw': "熱門設施/服務",
        'en': "Popular Facility"
    },
    hotelReview: {
        'cn': "评论",
        'tw': "評論",
        'en': "Reviews"
    },
    restaurants: {
        'cn': "内设餐厅",
        'tw': "內設餐廳",
        'en': "Restaurants"
    },
    restaurantw: {
        'cn': "内设餐厅",
        'tw': "內設餐廳",
        'en': "Restaurants"
    },
    hotelPolicy: {
        'cn': "酒店政策",
        'tw': "飯店政策",
        'en': "Hotel Policy"
    },
    hotelPolitw: {
        'cn': "酒店政策",
        'tw': "飯店政策",
        'en': "Hotel Policy"
    },
    restaurantFood: {
        'cn': '类型',
        'tw': '類型',
        'en': 'Food'
    },
    restaurantMenu: {
        'cn': '菜单',
        'tw': '菜單',
        'en': 'Menu'
    },
    cardsAccepted: {
        'cn': '接受信用卡类型',
        'tw': '接受信用卡類型',
        'en': 'Cards accepted at this property'
    }
}
