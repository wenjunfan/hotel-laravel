export default {
    perPrice: {
        'cn': '晚',
        'tw': '晚',
        'en': 'night'
    },

    freeCan: {
        'cn': "免费取消",
        'tw': "免費取消",
        'en': "Free Cancellation"
    },

    until: {
        'cn': "截至",
        'tw': "截至",
        'en': "until"
    },

    freeParking: {
        'cn': "免费停车",
        'tw': "免費停車",
        'en': "Free Parking"
    },
    allInclusive: {
        'cn': "全包",
        'tw': "全包",
        'en': "All Inclusive"
    },
    freeWiFi: {
        'cn': "免费无线网络",
        'tw': "免費無線網絡",
        'en': "Free WiFi"
    },

    freeBreakfast: {
        'cn': "免费早餐",
        'tw': "免費早餐",
        'en': "Free Breakfast"
    },

    excludeResortFee: {
        'cn': "不含每日度假费",
        'tw': "不含每日度假費",
        'en': "exclude daily <br>resort fee"
    },
    noFees: {
        'cn': "无预订或信用卡手续费",
        'tw': "無預訂或信用卡手續費",
        'en': "No Booking or Credit Card Fees"
    },
    confirmNow: {
        'cn': "立即确认",
        'tw': "立即確認",
        'en': "Instant Hotel Confirm"
    },
    inTwoMins: {
        'cn': "两分钟内完成",
        'tw': "兩分鐘內完成",
        'en': "Book in 2 Mins"
    },
    perBook: {
        'cn': "取消政策详情请见预订页",
        'tw': "取消政策詳情請見預訂頁",
        'en': "100% Price Guarantee"
    },
    soldOut: {
        'cn': "已售完",
        'tw': "已售完",
        'en': "SOLD OUT"
    },
    viewMore: {
        'cn': '更多信息',
        'tw': '更多信息',
        'en': 'View more'
    },
    imageMore: {
        'cn': '更多',
        'tw': '更多',
        'en': 'More'
    },
    taxIncluded: {
      'cn': '含税',
      'tw': '含稅',
      'en': 'Tax Included'
    },
};
