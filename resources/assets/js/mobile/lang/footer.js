export default {
    about: {
        'cn': '关于我们',
        'tw': '關於我們',
        'en': "About Us"
    },
    contact: {
        'cn': '联系我们',
        'tw': '聯系我們',
        'en': "Contact Us"
    },
    suggest: {
        'cn': '意见反馈',
        'tw': '意見反饋',
        'en': "Feedback"
    },
}
