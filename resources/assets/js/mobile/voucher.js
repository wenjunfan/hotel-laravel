var app = new Vue({
	el: '#app',
	data: {
      langVoucher: langVoucher,
      freeBefore:{
          'cn': '免费取消需早于',
          'tw': '免費取消需早於',
          'en': 'Free cancellation before '
      },
      nonRef:{
          'cn': '不可取消',
          'tw': '不可取消',
          'en': 'Non-Refundable'
      },
      roomCount :roomCount,
      checkin: checkin,
      checkout: checkout,
      dayNameListObj: dayNameListObj,
      roomCountListObj: roomCountListObj,
      checkinDayName: window.getDayName(checkin),
      checkoutDayName: window.getDayName(checkout),
	},
    mounted () {
          $('.appbar-title').html(this.langVoucher.title);
	}
});
