window.Vue = require('vue');
import MuseUI from 'muse-ui';

// 导入全部组件
Vue.use(MuseUI);

// 全局 components
Vue.component('top-nav', require('./components/top-nav.vue'));
Vue.component('footer-nav', require('./components/footer.vue'));
Vue.component('back-top', require('./components/back-top.vue'));
Vue.component('share-links', require('./components/share-links.vue'));

//全局mixin
Vue.mixin({
    data: function() {
        return {
            isUsitour: isUsitour,
            is117book: is117book,
            isAA: isAA,
            langId: language,
            hideLinks: true
        }
    },
    computed: {
        langStr: function() {
            if (this.langId == 2) {
                return 'tw';
            } else if (this.langId == 1) {
                return 'en';
            } else {
                return 'cn';
            }
        }
    },
    mounted: function() {
        // different app background for different domain
        if (this.isUsitour) {
            $('body').addClass('usitour');
        }
        window.resize();
        window.addEventListener('resize', resize, false);
    },
    methods: {
        closeLinksPop: function(val) {
            this.hideLinks = val;
            window.lockBody(false);
        }
    }
});

window.resize = function () {
    var screenW = window.screen.width, scale = 7.5, fontSize = 100;
    if (!!navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
        screenW = window.innerWidth;
    }
    fontSize = Math.floor(screenW / scale);
    /**
     * window.ComputedFontSize 换算比例
     * @type {公式} <==> 真实px(750设计稿)  /  ComputedFontSize =  当前屏幕下的px值
     */
    window.ComputedFontSize = (100 / fontSize).toFixed(2);
    document.documentElement.style.fontSize = fontSize + 'px';
};

//toastr config
toastr.options = {
    'newestOnTop': true, // "progressBar": true,
    "preventDuplicates": true,
    "preventOpenDuplicates": true,
    'positionClass': 'toast-top-full-width',
    'showDuration': '300',
    'hideDuration': '1000',
    'timeOut': '2000',
    'extendedTimeOut': '1000',
    'showEasing': 'swing',
    'hideEasing': 'linear',
    'showMethod': 'fadeIn',
    'hideMethod': 'fadeOut'
};

// define global functions ////////////////////////////////////
window.getCookie = function (cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

// for calendar selection.
window.addDays = function (dateStr, days) {
    return moment(dateStr).add(days, 'days').format('YYYY-MM-DD');
};

window.addDaysEn = function (dateStr, days) {
    return moment(dateStr).add(days, 'days').format('MM-DD-YYYY');
};

window.lockBody = function (value) {
    if (value) {
        $('body').css('overflow-y', 'hidden');
        $('body').css('height', '100vh'); // 最好还是保持原来的位置?
    } else {
        $('body').css('overflow-y', 'auto');
        $('body').css('height', 'auto');
    }
};
// 手机端兼容性很差，暂时废弃 201804
window.loadingStart = function () {
    // $(".fullscreen-spinner").css("display", "inline-block");
    // window.lockBody(true);
};

window.loadingEnd = function () {
    $(".fullscreen-spinner").css("display", "none");
    window.lockBody(false);
};

// 检测localStorage是否存有 Checkin 和 Checkout, 房间数， 成人数和儿童数量 等数据，
// 日期校验是否大于今天。如果是就选择localStorage的数据。
// 注意， undefined 要用引号
let initValue = function (valueLabel, value) {
    let checkValue = (typeof (Storage) !== 'undefined') ? window.localStorage.getItem(valueLabel) : '';

    if (checkValue) {
        if (valueLabel === 'checkin' || valueLabel === 'checkout') {
            checkValue = window.addDays(window.today, value);
        } else {
            checkValue = value;
        }

        // window.localStorage.setItem(valueLabel, checkValue);
    } else { // set default value if no history exist
        if (valueLabel === 'checkin') {
            // check date from today
            checkValue = window.addDays(window.today, value);
        } else if (valueLabel === 'checkout') {
            // check date from today
            checkValue = window.addDays(window.today, value);
        } else {
            checkValue = value;
        }

        // if (typeof(Storage) !== 'undefined') {
        //     window.localStorage.setItem(valueLabel, checkValue);
        // }
    }

    return checkValue;
};

// check checkin and checkout date before submit
window.checkDate = function (dateStr, valueLabel, addDateNum) {
    if (valueLabel == 'checkin' && window.today > dateStr) {
        dateStr = window.addDays(window.today, addDateNum);
        window.checkInDate = dateStr;

        // if (typeof(Storage) !== 'undefined')
        //     window.localStorage.setItem('checkin', dateStr);
    }
    if (valueLabel == 'checkout' && window.tomorrow > dateStr) {
        dateStr = window.addDays(window.today, addDateNum);
        window.checkOutDate = dateStr;

        // if (typeof(Storage) !== 'undefined')
        //     window.localStorage.setItem('checkout', dateStr);
    }

    return dateStr;
};
window.countDays = function (dateStr1, dateStr2) {
    return moment(dateStr2).diff(dateStr1, 'days');
};
window.getDayName = function (dateStr) {
    moment().locale('zh-cn');
    return moment(dateStr).locale('zh-cn').format('dddd');
};

// set user's search data to localStorage
window.setSearchCache = function (checkin, checkout, room, adult, child) {
    if (typeof (Storage) !== 'undefined') {
        window.localStorage.setItem('checkin', checkin);
        window.localStorage.setItem('checkout', checkout);
        window.localStorage.setItem('roomCount', room);
        window.localStorage.setItem('adult', adult);
        window.localStorage.setItem('child', child);
    }
};

// global functions ////////////////////////////////////
// window.onbeforeunload = loadingStart
window.goBackIfHistory = function () {
    var result = window.history.length;
    if (result <= 1) {
        window.location.href = "/search";
    } else {
        window.history.back();
    }
};

// global variable ////////////////////////////////////
window.today = moment().format('YYYY-MM-DD');
window.tomorrow = window.addDays(window.today, 1);

window.dayAbbreviation = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
window.dayNameList = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
window.dayNameListObj = {
    'Sunday': {
        'cn': "星期日",
        'en': "Sun"
    },
    'Monday': {
        'cn': "星期一",
        'en': "Mon"
    },
    'Tuesday': {
        'cn': "星期二",
        'en': "Tue"
    },
    'Wednesday': {
        'cn': "星期三",
        'en': "Wed"
    },
    'Thursday': {
        'cn': "星期四",
        'en': "Thu"
    },
    'Friday': {
        'cn': "星期五",
        'en': "Fri"
    },
    'Saturday': {
        'cn': "星期六",
        'en': "Sat"
    }
};
window.roomCountListObj = {
    '1': {
        'cn': "一",
        'en': "1st"
    },
    '2': {
        'cn': "二",
        'en': "2nd"
    },
    '3': {
        'cn': "三",
        'en': "3rd"
    },
    '4': {
        'cn': "四",
        'en': "4th"
    },
    '5': {
        'cn': "五",
        'en': "5th"
    },
    '6': {
        'cn': "六",
        'en': "6th"
    },
    '7': {
        'cn': "七",
        'en': "7th"
    },
    '8': {
        'cn': "八",
        'en': "8th"
    }
};
window.monthNameList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
window.monthLongNameList = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

// global variable for init user search data //////////
window.checkInDate = initValue('checkin', 1);
window.checkOutDate = initValue('checkout', 2);
window.roomCount = initValue('roomCount', 1);
window.adultCount = initValue('adult', 2);
window.childCount = initValue('child', 0);

// stop loading when page show
$(window).bind("pageshow", function (event) {
    // TODO
});

// sticky site notes
$(window).scroll(function () {
    var scrollTop = $(this).scrollTop();
    if (scrollTop > 55) {
        $('#siteNotes').addClass('fixedBanner');
    } else if (scrollTop == 0) {
        $('#siteNotes').removeClass('fixedBanner');
    }
});

window.closeNotice = function () {
    document.getElementById('siteNotes').style.display = 'none';
    $.get('/site-notes/close');
};
