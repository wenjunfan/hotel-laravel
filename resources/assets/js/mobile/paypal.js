var app = new Vue({
    el: '#app',
    data: {
        Reorderid: $('#Reorderid').val(),
        showComment: false,
        langPaypal: langPaypal,
        cardData: {
            number: '',
            month: '01',
            year: '20',
            cvc: '',
            phone: '',
            firstName: '',
            lastName: '',
            street: '',
            street2: '',
            city: '',
            state: '',
            zip: '',
            countryCode: 'US',
            checkInOut: checkIn + '/' + checkOut,
        },
        processing: false,
        showForm: false,
    },
    mounted: function () {
        let self = this;
        var node_needVerify = $('.NeedVerify');
        var node_phone = $('#phone');
        // 这一段是给booker-phone-country这个component用的，数据很多，如果使用的话需将之前的select和other废弃
        $('.area-box').click(function () {
            $('.area-wrap').css('display', 'block');
        });
        $('.area-wrap').find('dd').each(function () {
            // 国家名称中文
            $(this).
                find('span[lang=zh]').
                html($(this).find('span[lang=zh]').attr('zh'));
            if (language === 0) {
                $(this).find('span[lang=zh]').css('display', 'block');
                $(this).find('span[lang=en]').css('display', 'none');
            }
            $(this).click(function () {
                if ($(this).hasClass('addOther')) {
                    $('.area-box').
                        html(
                            '<input id="otherCode" name="otherCode" class="form-control" placeholder="+XXX" type="text"/>');
                } else {
                    var selectedAreaCode = $(this).find('em').html();
                    var selectedFlag = $(this).find('i').css('background-position');
                    $(this).addClass('highlight').siblings().removeClass('highlight');
                    $('.area-box').
                        html(
                            '<input name="countryCodePhone" id="countryCodePhone" readonly="" class="form-control" type="text" value="' +
                            selectedAreaCode + '" placeholder="">' +
                            '<span class="country-with-flag"><i class="flag" style="background-position:' +
                            selectedFlag + '"></i><i class="fa fa-angle-down"></i></span>');
                }
                $('.area-wrap').css('display', 'none');
            });
        });

        var vb = $('.verifingBtn');
        $('#hotelHead').text(this.langPaypal.headtitle);
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
        node_needVerify.hide();
        //b端用户不需要手机验证
        if (is117book) {
            $('#validatePhoneSection').addClass('valid');
        } else {
            var node_nomber = $('#number');
            $('#cvc').change(function () {
                if (node_nomber.val().trim().length >= 15) {
                    var inputNumber = node_nomber.val();
                    var number = inputNumber.substr(0, 6);
                    var month = $('#month').val();
                    var year = $('#year').val();
                    var uniquenumber = inputNumber.substr(8, 8) + month + year;
                    $.ajax({
                        type: 'GET',
                        url: '/card/check/' + number + '/' + self.Reorderid,
                        dataType: 'json',
                        success: function (data) {
                            if (data.success === true) {
                                $('.NeedVerify').hide();
                                $('#validatePhoneSection').addClass('valid');
                            } else {
                                $.ajax({
                                    type: 'GET',
                                    url: '/card/validate/' + uniquenumber,
                                    dataType: 'json',
                                    success: function (data) {
                                        if (data['errorId'] === 'BL1001') {
                                            swal({
                                                title: '',
                                                text: self.langPaypal.changecard,
                                                type: 'warning',
                                                confirmButtonColor: '#dd6b55',
                                                confirmButtonText: 'ok'
                                            }).then(function () {
                                                window.location.href = '/paypal/' + self.Reorderid;
                                            });
                                        } else {
                                            $('.NeedVerify').fadeIn();
                                            node_phone.val(phoneNum);
                                            // update height of right description
                                            self.updateHeight();
                                            $('#validatePhoneSection').removeClass('valid');
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }

        this.updateHeight();
        var element = $('#fixedDiv');
        var top = $('html').offset().top;
        var width = $(window).width();
        $(window).scroll(function () {
            var scrolls = $(this).scrollTop();
            var height = $(document).height();
            var divHeight = $('#fixedDiv').height();

            var bottom = 480 + divHeight;
            var maxtop = height - bottom;
            if ((scrolls > 100) && (maxtop > scrolls) && width > 1004) {
                if (window.XMLHttpRequest) {
                    element.css({
                        position: 'fixed',
                        top: 0,
                        width: '28%'
                    });
                } else {
                    element.css({
                        top: 0,
                        width: '100%'
                    });
                }
            } else if (width < 1004) {

            } else {
                element.css({
                    position: 'absolute',
                    top: top,
                    width: '100%'
                });
            }
        });

        $('#hotel-rating').html(this.buildRating(hotelRating, hotelCategory));

        // phonechange hide area wrao selection part
        node_phone.change(function () {
            $('.area-wrap').css('display', 'none');
        });

        // only send code at first time
        vb.on('click', function () {
            if (node_phone.val().trim()) {
                vb.attr('disabled', 'disabled');
                vb.prop('disabled', true);
            } else {
                swal(self.langPaypal.phone_not_enter, '', 'error');
            }
            if (vb.html() === self.langPaypal.sendCode) {
                self.validatePhone();
            }
        });

        node_phone.on('click', function () {
            var node_otherCode = $('#otherCode');
            var otherCode = node_otherCode.val();
            var node_countryCodePhone = $('#countryCodePhone');
            //other country code in additional to provided
            if (otherCode !== '' && node_countryCodePhone.val() === '') {
                $('.area-box').
                    html(
                        '<input name="countryCodePhone" id="countryCodePhone" readonly="" class="form-control" type="text" value="' +
                        otherCode + '" placeholder="">' +
                        '<span class="country-with-flag"><i class="fa fa-question-circle"></i><i class="fa fa-angle-down"></i></span>');
                $('.area-col-dl').
                    append(
                        '<dd class="added highlight"><i class="fa fa-question-circle"></i><span lang="zh" zh="" style="display: block;"></span><span lang="en" en="" style="display: none;"></span><em>+ ' +
                        otherCode + '</em></dd>');
                node_countryCodePhone.val(otherCode);
                if (node_countryCodePhone.val() !== ' ') {
                    node_otherCode.remove();
                }
            }
        });

        $('#creditform').validate({
            rules: {
                number: {
                    required: true,
                    minlength: 15,
                    maxlength: 16,
                    number: true
                },
                CVC: {
                    required: true,
                    minlength: 3,
                    maxlength: 4,
                    number: true
                },
                firstName: {
                    required: true
                },
                lastName: {
                    required: true
                },
                street: {
                    required: true,
                    minlength: 6
                },
                zip: {
                    required: true,
                    minlength: 3 //most ppl in TaiWai uses 3 number as zipCode
                },
                city: {
                    required: true,
                    minlength: 2
                },
                state: {
                    required: true,
                    minlength: 2
                }
            }
        });
        $('.checkin').text(checkIn);
        $('.checkout').text(checkOut);
        // loading modal change style for mobile
        if (screen.width < 740) {
            $('.modal-dialog').css({
                'margin-top': '180px !important'
            });
        }
        this.carryPhone();
    },
    methods: {
        preventClick: function () {
            this.processing = true;
            $('.ladda-button-book').ladda().ladda('start');
        },
        ableClick: function () {
            this.processing = false;
            $('.ladda-button-book').ladda().ladda('stop');
        },
        carryPhone: function () {
            var node_phone = $('#phone');
            var node_countryCodePhone = $('#countryCodePhone');
            node_countryCodePhone.val(phoneCountryCode);
            node_phone.val(phoneNum);

            if (phoneCountryCode !== '') {
                node_countryCodePhone.val(phoneCountryCode);
                var taregt = $('.area-con-dl').
                    find('em:contains(' + phoneCountryCode + ')');
                if (taregt.html() === phoneCountryCode) {
                    var position = taregt.parent().find('i').css('background-position');
                    taregt.parent().addClass('highlight');
                    $('.country-with-flag .flag').css('background-position', position);
                } else {
                    $('.area-box').html('<input id="otherCode" name="otherCode" class="form-control" value="' + phoneCountryCode + '" placeholder="+XXX" type="text"/>');
                }
            } else {
                $('.area-box').html('<input id="otherCode" name="otherCode" class="form-control" placeholder="+XXX" type="text"/>');
            }
        },
        updateHeight: function () {
            var node_whyUsBox = $('.note-and-why-us-box');
            var scrollHeight = $('.book-info-box').height() -
                $('.hotel-detail-top').height() - 20;
            node_whyUsBox.css('height', scrollHeight);
            if (($('#hotel-comment').height() + 483) < scrollHeight) {
                node_whyUsBox.css('overflow-y', 'hidden');
            } else {
                node_whyUsBox.css('overflow-y', 'scroll');
            }
        },
        validatePhone: function () {
            let self = this;
            var node_phone = $('#phone');
            var node_otherCode = $('#otherCode');
            var otherCode = node_otherCode.val();
            var node_countryCodePhone = $('#countryCodePhone');
            var node_verifyCodeBtn = $('#verifyCodeBtn');

            if (otherCode !== '' && node_countryCodePhone.val() === '') {
                $('.area-box').html('<input name="countryCodePhone" id="countryCodePhone" readonly="" class="form-control" type="text" value="' + otherCode + '" placeholder="">' +
                    '<span class="country-with-flag"><i class="fa fa-question-circle"></i><i class="fa fa-angle-down"></i></span>');
                $('.area-col-dl').append('<dd class="added highlight"><i class="fa fa-question-circle"></i><span lang="zh" zh="" style="display: block;"></span><span lang="en" en="" style="display: none;"></span><em>+ ' + otherCode + '</em></dd>');
                node_countryCodePhone.val(otherCode);
                if (node_countryCodePhone.val() !== '') {
                    node_otherCode.remove();
                }
            }
            if (node_phone.val().trim() !== '') {
                var vb = $('.verifingBtn');
                var vc = $('.validateCode');
                var timer = 60;
                var codeTimer = setInterval(function () {
                    var minute = Math.floor(timer / 60);
                    var second = '00' + (timer % 60);
                    var val = minute + ':' + second.substring(second.length - 2);
                    vb.html(val + '<i class=\'fa fa-spinner fa-spin\'></i>');
                    if (timer > 0) {
                        vb.attr('disabled', 'disabled');
                        vb.prop('disabled', true);
                        timer--;
                    } else {
                        vb.removeAttr('disabled');
                        vb.prop('disabled', false);
                        vb.html(self.langPaypal.sendCode);
                        clearInterval(codeTimer);
                    }
                }, 1000);

                $.post('/sms-code/send', {
                    '_token': $('input[name=\'_token\']').val(),
                    'mobile': node_phone.val(),
                    'country_code': node_countryCodePhone.val().trim() === '' ?
                        node_otherCode.val() :
                        node_countryCodePhone.val()
                }, function (data) {
                    if (data.success) {
                        vb.removeAttr('disabled');
                        vb.prop('disabled', false);
                        vc.removeAttr('disabled');
                        vc.prop('disabled', false);
                        vc.css('background-color', '#ea8523');
                        vc.click(function () {
                            if ($('#validatePhoneCode').val().trim() !== '') {
                                if (node_verifyCodeBtn.html() === self.langPaypal.vrfCode) {
                                    self.validBtnLoading();
                                    self.checkValidatePhone();
                                }
                            } else {
                                swal(self.langPaypal.code_not_enter, '', 'warning');
                            }
                        });
                    } else {
                        swal(data.message, '', 'warning');
                    }
                });
            }
        },
        validBtnLoading: function () {
            var node_verifyCodeBtn = $('#verifyCodeBtn');
            node_verifyCodeBtn.attr('disabled', true);
            node_verifyCodeBtn.html(
                this.langPaypal.vrfCode + '<i class="fa fa-spinner fa-spin"></i>');
        },
        stopValidBtnLoading: function () {
            var node_verifyCodeBtn = $('#verifyCodeBtn');
            node_verifyCodeBtn.attr('disabled', false);
            node_verifyCodeBtn.html(this.langPaypal.vrfCode);
        },
        checkValidatePhone: function () {
            let self = this;
            var node_phone = $('#phone');
            var code = $('#validatePhoneCode').val();
            var node_otherCode = $('#otherCode');
            var node_countryCodePhone = $('#countryCodePhone');
            var phoneCountryCode = node_countryCodePhone.val().trim() === '' ?
                node_otherCode.val() :
                node_countryCodePhone.val();
            var phone = phoneCountryCode + node_phone.val();

            var phoneNumber = node_phone.val().trim();
            // loading for validate code
            $.get('/sms-code/verify', {
                'phone': phone,
                'code': code,
                'Reorderid': this.Reorderid
            }, function (data) {
                self.stopValidBtnLoading();
                if (data.success) {
                    swal(self.langPaypal.valid, phone, 'success');
                    $('.validatePhone').
                        html(
                            '<img class=\'validPic\' src=\'/img/general/icon/valid.png\'><input type=\'hidden\' id=\'countryCodePhone\' name=\'countryCodePhone\' value=\'' +
                            phoneCountryCode +
                            '\'><input type=\'hidden\' id=\'phone\' name=\'phone\' value=\'' +
                            phoneNumber +
                            '\'><input type=\'hidden\' name=\'validatePhoneCode\' value=\'' +
                            code +
                            '\'><span class=\'grey\'> ' + phone + '</span>');
                    $('#validatePhoneSection').addClass('valid');
                    self.updateHeight();
                } else if (data.errorId === 'IT0105' || data.errorId === 'IT0106' ||
                    data.errorId === 'IT0107') {
                    swal({
                        title: '',
                        text: self.langPaypal.tooManyTimes,
                        type: 'warning',
                        confirmButtonColor: '#dd6b55',
                        confirmButtonText: lang_backtoSearch
                    }).then(function () {
                        window.location.href = hotelPath;
                    });
                } else {
                    swal(self.langPaypal.phonerecheck, phone, 'warning');

                }
            });
        },
        calc: function () {
            document.getElementById('bt').disabled = !document.getElementById('cb').checked;
        },
        buildRating: function (rating, category) {
            if (isNaN(rating)) {
                return category;
            }
            var r = Number(rating);
            if (r >= 5) {
                r = 5;
            }
            var h = '';
            for (var i = 0; i < r; i++) {
                h += '<i class="fa fa-star"></i>';
            }
            return h;
        },
        submitPayment: function () {
            let self = this;
            var node_phone = $('#phone');
            var node_countryCodePhone = $('#countryCodePhone');
            var r = $('#creditform').valid();
            if (!r) {
                swal(this.langPaypal.sa);
                return;
            }
            // replace href with book ur, to avoid reload and duplicate order
            history.replaceState('', '', document.referrer.replace('https:', ''));
            swal('', this.langPaypal.do_not_refresh);
            if ($('#validatePhoneSection').hasClass('valid')) {
                //change check box to img to avoid second cc payment - 03132018
                $('#ccAuth').html('<img class="validPic" src="/img/general/icon/valid.png">');
                this.preventClick();
                var url = '/paypal/submit';
                $.post(url, $('#creditform').serialize(), function (data) {
                    self.checkOrderStatus(data);
                });
            } else {
                this.ableClick();
                $('.NeedVerify').fadeIn();
                node_countryCodePhone.val(phoneCountryCode);
                node_phone.val(phoneNum);
                this.updateHeight();
                swal(this.langPaypal.phonenotvalid, '', 'warning');
            }
        },
        checkOrderStatus: function (data) {
            let self = this;
            if (data.success && data.voucherUrl) {
                window.location.replace(data.voucherUrl);
            } else {
                this.ableClick();
                if (data['errorId'] === 'ER3999') {
                    if (data.hasOwnProperty('options') && data.options.length > 0) {
                        var hotelData = {
                            'id': data.options.id,
                            'name': data.options.name
                        };
                        onPaymentComplete(hotelData);
                    }
                    var errMessage = '';
                    var errId = 'ER3999';
                    if (data['errorMessage'] !== '[]' && data['errorMessage'] !== '') {
                        var jsonData = JSON.parse(data.errorMessage);
                        for (var i = 0; i < jsonData.length; i++) {
                            errMessage = jsonData[i]['errDesc'];
                            errId = jsonData[i]['errId'];
                        }
                    }
                    swal({
                        title: self.langPaypal.cardInDan,
                        type: 'warning',
                        text: errId + ': ' + errMessage,
                        confirmButtonColor: '#dd6b55',
                        confirmButtonText: self.langPaypal.pleaseUploadFile
                    }).then(function () {
                        window.location.href = '/file-upload/' + self.Reorderid;
                    });
                } else if (data.errorId === 'ER1401' || data.errorId === 'ER1301') {
                    swal({
                        title: '',
                        text: lang_soldout,
                        type: 'warning',
                        confirmButtonColor: '#dd6b55',
                        confirmButtonText: lang_backtoSearch
                    }).then(function () {
                        window.location.href = hotelPath;
                    });
                } else if (data.errorId === 'ER1004') {
                    swal({
                        title: '',
                        text: 'Session Time out',
                        type: 'warning',
                        confirmButtonColor: '#dd6b55',
                        confirmButtonText: lang_backtoSearch
                    }).then(function () {
                        window.location.href = hotelPath;
                    });
                } else if (data['errorId'] === 'ER1411' || data['errorId'] === 'ER1402') {
                    swal({
                        title: '',
                        text: lang_priceChange,
                        type: 'warning',
                        confirmButtonColor: '#dd6b55',
                        confirmButtonText: lang_backtoSearch
                    }).then(function () {
                        window.location.href = hotelPath;
                    });
                } else if (data['errorId'] === 'ER1413') {
                    swal(lang_limit, data.message, 'error');
                } else if (data['errorId'] === 'ER1404') {
                    swal({
                        title: '',
                        text: lang_timeout,
                        type: 'warning',
                        confirmButtonColor: '#dd6b55',
                        confirmButtonText: lang_backtoSearch
                    }).then(function () {
                        window.location.href = hotelPath;
                    });
                } else if (data['errorId'] === 'CC15005') {
                    swal({
                        title: '',
                        type: 'warning',
                        text: data.message + '\n' + lang_auth,
                        confirmButtonColor: '#dd6b55',
                        confirmButtonText: 'OK'
                    }).then(function () {
                        // for card failure, refresh page
                        window.location.href = '/paypal/' + self.Reorderid;
                    });
                } else if (data['errorId'] === 'ER1414') {
                    swal({
                        title: '',
                        text: lang_duplicate.replace('xxx', data['errorMessage']),
                        type: 'warning',
                        confirmButtonColor: '#dd6b55',
                        confirmButtonText: lang_backtoSearch
                    }).then(function () {
                        window.location.href = hotelPath;
                    });
                } else if (data['errorId'] === 'IT1001') {
                    swal({
                        title: lang_cnetwork,
                        type: 'warning',
                        text: data.message,
                        confirmButtonColor: '#dd6b55',
                        confirmButtonText: lang_backtoSearch
                    }).then(function () {
                        window.location.href = hotelPath;
                    });
                } else if (data['errorId'] === 'PHONE1001') {
                    swal(self.langPaypal.phonenotvalid, '', 'warning');
                } else if (data['errorId'] === 'ER1415') { // for duplicate order check
                    swal({
                        title: '',
                        text: lang_confirmDu,
                        type: 'warning',
                        confirmButtonText: lang_confirm,
                        showCancelButton: true,
                        cancelButtonText: lang_cancel
                    }).then(function (confirmed) {
                        if (confirmed) {
                            self.preventClick();

                            $.post('/duplicate/proceed', {
                                '_token': $('meta[name="csrf_token"]').attr('content'),
                                'action': 'continue',
                                'Reorderid': data.Reorderid,
                                'vendor': 'cc',
                                'p_id': data.p_id
                            }, function (data) {
                                self.checkOrderStatus(data);
                            });
                        } else {
                            $.post('/duplicate/proceed', {
                                '_token': $('meta[name="csrf_token"]').attr('content'),
                                'action': 'cancel',
                                'Reorderid': data.Reorderid,
                                'vendor': 'cc',
                                'p_id': data.p_id
                            }, function (data) {
                                if (!data.success) {
                                    data['message'] = lang_failed;
                                }
                                swal({
                                    title: '',
                                    text: data.message,
                                    type: 'warning',
                                    confirmButtonText: 'OK'
                                }).then(function () {
                                    window.location.href = hotelPath;
                                });
                            });
                        }
                    });
                } else if (data.errorId) {
                    swal({
                        title: '',
                        text: data.errorId,
                        type: 'warning',
                        confirmButtonColor: '#dd6b55',
                        confirmButtonText: lang_backtoSearch
                    }).then(function () {
                        window.location.href = hotelPath;
                    });
                } else {
                    swal({
                        title: lang_cfailed,
                        type: 'warning',
                        text: data.message,
                        confirmButtonColor: '#dd6b55',
                        confirmButtonText: lang_backtoSearch
                    }).then(function () {
                        window.location.href = hotelPath;
                    });
                }
            }
        },
        printForm: function () {
            this.showForm = false;
            setTimeout(function () {
                window.print();
            });
        }
    }
});
