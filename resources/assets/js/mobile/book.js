// 引用 Vue 组件
import BookLockPrice from './components/book-lock-price.vue';
import Modal from './components/modal.vue'; // a simple modal
import Processing from './components/processing.vue';
import SpecialRequest from './components/special-request.vue';

var app = new Vue({
    el: '#app',
    components: {
        BookLockPrice,
        SpecialRequest,
        Modal,
        Processing,
    },
    data: {
        roomRef: roomRef,
        dayCount: dayCount,
        currency: currency,
        singleRoomNightPriceB4Tax: singleRoomNightPriceB4Tax,
        singleRoomNightTax: singleRoomNightTax,
        hotelId: hotelId,
        hotelName: hotelName,

        hotelAddress: hotelAddress,
        roomNum: roomCount,
        ipLocation: ipLocation,

        samename: false, //same name check
        isWechat: false,
        isQQ: false,
        reference: '', // payment reference, not roomRef
        vendorType: '',
        source: source,
        rooms: rooms,
        room: room,
        hotel: hotel,
        checkin: checkin,
        checkout: checkout,
        checkinDayName: window.getDayName(checkin),
        checkoutDayName: window.getDayName(checkout),
        firstName: {'cn': '名（英文/拼音）', 'tw': '名（英文/拼音）', 'en': 'First Name'},
        lastName: {'cn': '姓（英文/拼音）', 'tw': '姓（英文/拼音）', 'en': 'Last Name'},
        phoneHint: {'cn': '方便酒店与您联系', 'tw': '方便酒店與您聯系', 'en': 'For hotel to reach you out'},
        emailHint: {'cn': '用于接受酒店凭证', 'tw': '用於接受酒店憑證', 'en': 'For receive reservation voucher'},
        comments: comments,
        roomComments: roomComments,
        status: status,
        roomCount: roomCount,
        netPrice: netPrice,
        totalPrice: null,
        appliedAmount: 0,
        cleanFee: cleanFee,
        beforeTax: beforeTax,
        tax: tax,
        singleRoomNightPrice: singleRoomNightPrice,
        guests: guests,
        booker: booker,
        sessionKey: sessionKey,
        hotelPath: hotelPath,

        bottomPopup: false,
        topPopup: false,
        leftPopup: false,
        rightPopup: false,
        dayNameListObj: dayNameListObj,
        roomCountListObj: roomCountListObj,
        langBook: langBook,
        showModal: false,
        startLoading: false,
        modalContent: langBook.confirmPaid,
        displayId: displayId,
        showSpecialRequests: false,
        showComment: false,
        env: paypalEnv,
        app_pp_btn: app_pp_btn,
        token: '',
        couponCode: '',
        invalidCoupon: false,
        couponErrorMsg: '',
        rooms_key: rooms_key,
    },
    mounted() {
        this.token = $('meta[name="csrf_token"]').attr('content');
        $('.appbar-title').html(this.langBook.title);
        let isQQBrowser = this._isQQBrowser();
        if (this.isUsitour) {
          $('.exchange-rate-warning').show();
        }

        $('input[type=radio][name=imgsel]').change(function() {
           if (this.value ==='pb') {
              var r = $('#book-form').valid();
             $('.exchange-rate-warning').show();
              if (!r) {
                toastr.error(self.langBook.plzComplete , 'Error');
                document.getElementById("ccpay").checked = true;
              }else{
                $('#paymentAuth').html('<img class="validPic" src="/img/general/icon/valid.png">');
                $('#paypal-button').show();
                $('#bt').hide();
              }
            }else {
                 if (this.value ==='al') {
                   $('.alipay-warning').show();
                 }else{
                   $('.alipay-warning').hide();
                 }
                 if (this.value ==='cc') {
                   $('.exchange-rate-warning').show();
                 }else{
                   $('.exchange-rate-warning').hide();
                 }
                 $('#paypal-button').hide();
                 $('#bt').show();
            }
        });

        if (isQQBrowser) {
            // hide wechat and alipay in qq browser as redirecting back after payment is not supported
            $('#alipayWrapper').hide();
            $('#wechatpayWrapper').hide();
            $('#ccPaymentLabel input').attr('checked', true);
        }

        $('#booker_local_time').val(new Date());

        $.validator.addMethod('accept', function(value, element, param) {
            return value.match(new RegExp('.' + param + '$')) || value === '';
        }, this.langBook.alphabetOnly);

        $.validator.addMethod('phone', function(value, element) {
            // allow only number
            return this.optional(element) || /^[0-9]+$/.test(value);
        }, this.langBook.numberOnly);

        if (typeof(Storage) !== 'undefined' && localStorage.getItem('reference') !== null) {
            this.reference = localStorage.getItem('reference');
            this.vendorType = localStorage.getItem('temp');

            this.showModal = true; // user confirm whether has paid
        }

        this.isWechat = this._isWechatBrowser();

        if (this.isWechat) {
            $('#alipayWrapper').hide();
            $('#wechatpayWrapper').show();
            $('#ppBtnPaymentLabel').hide(); //微信浏览器不支持页面跳转，其中ios连button都无法显示，所以先屏蔽
        } else {
            // 中国非微信浏览器隐藏微信支付
            if (this.ipLocation.toLowerCase() === 'cn') {
                $('#wechatpayWrapper').hide();
            }
        }

        loadingStart();
        this.doRoomCheck();

        let rules = {
            booker_ln: {
                required: true,
                minlength: 2,
            },
            booker_fn: {
                required: true,
                minlength: 2,
            },
            booker_phone: {
                required: true,
                minlength: 6,
                phone: true,
            },
            specialrequest: {
                maxlength: 100,
                accept: '[a-zA-Z]+',
            },
            booker_email: {
                required: {
                    depends: function() {
                        $(this).val($.trim($(this).val()));
                        return true;
                    },
                },
                email: true,
            },
            phone_country: {
                required: true,
            },
        };

        for (let i = 0; i < this.roomCount; i++) {
            rules['guest' + (i + 1) + '_a1_fn'] = {
                required: true,
                minlength: 2,
            };

            rules['guest' + (i + 1) + '_a1_ln'] = {
                required: true,
                minlength: 1,
            };
        }

        $('#book-form').validate({
            rules: rules,
        });
        let self = this;

        let transactions = [{
          amount: {
            total: self.netPrice-self.appliedAmount, //netPrice包含cleanFee如果有的话
            currency: self.currency,
            details: {
              subtotal: self.beforeTax-self.appliedAmount, //beforeTax包含cleanFee如果有的话
              tax: self.tax,
            }
          },
          item_list: {
            items: [
              {
                name: self.hotelName,
                description: self.hotelAddress,
                quantity: self.roomCount * self.dayCount,
                price: self.singleRoomNightPriceB4Tax,
                tax: self.singleRoomNightTax,
                sku: 'H' + self.hotelId, //Item #:
                currency: self.currency
              },
            ],
          }
        }];

        if (self.appliedAmount !== 0) {
            transactions[0]['item_list']['items'].push(
                {
                  name: self.langBook.coupon,
                  quantity: 1,
                  price: -self.appliedAmount,
                  tax: 0,
                  currency: 'USD'
                });
        }

        if (self.cleanFee !== 0) {
            transactions[0]['item_list']['items'].push(
              {
                  name: self.langBook.cleanFee,
                  quantity: 1,
                  price: self.cleanFee,
                  tax: 0,
                  currency: 'USD'
              });
        }
        /*
        paypal.Button.render({
            // Configure environment
            env: self.env, // sandbox | production

            client: {
                sandbox: self.app_pp_btn,
                production: self.app_pp_btn
            },
            // Customize button (optional)
            locale: self.langBook.locale,   //paypal button shown language
            style: {
                label: 'pay', // buynow | paypal | pay | checkout | credit
                size: 'responsive', // small | medium | large | responsive
                shape: 'rect',   // pill | rect
                color: 'gold',  // gold | blue | silver | black
                tagline: false, // optional :The safer, easier way to pay
            },
            // Set up a payment
            payment: function (data, actions) {
                return actions.payment.create({
                    experience: {
                        input_fields: {
                            no_shipping: 1  //no shipping needed
                        }
                    },
                    payment: {
                        transactions: transactions,
                        note_to_payer: self.checkin + ' - ' + self.checkout + ', ' + self.roomCount + ' room(s)'
                    },
                });
            },
            onAuthorize: function (data, actions) {
                // save transaction request to payment
                self.savePBInfo(JSON.stringify(data), 'request');
                return actions.payment.execute().then(function (res) {
                    // check for ERROR CODE=INSTRUMENT_DECLINED and restart
                    if (res.error === 'INSTRUMENT_DECLINED') {
                        return actions.restart();
                    } else {
                        self.preventClick();
                        // save transaction response to payment
                        self.savePBInfo(JSON.stringify(res), 'response');
                    }
                });
            },
            onError: function (err) {
                // Show an error page here, when an error occurs
                swal({
                    title: '',
                    text: self.langBook.pbError + err,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'OK',
                    allowOutsideClick: false,
                }).then(function () {
                    window.location.href = self.hotelPath;
                });
            }
        }, '#paypal-button');
        */
        // for usitour, remove wechatpay and alipay from pmt method
        if (this.isUsitour) {
            $('#wechatpayWrapper').hide();
            $('#alipayWrapper').hide();
        }

        $('#guest1_a1_fn').change(function() {
            let booker_firstname = $('#guest1_a1_fn').val();
            $('#booker_fn').val(booker_firstname);
        });

        $('#guest1_a1_ln').change(function() {
            let booker_lastname = $('#guest1_a1_ln').val();
            $('#booker_ln').val(booker_lastname);
        });

        $('#phone_country').change(function() {
            if ($('#phone_country').val() === ' ') {
                $('.other').append('<input id="otherCode" name="otherCode" class="form-control" placeholder="+XXX" type="text" />');
            } else {
                $('#otherCode').remove();
            }
        });

        $('#booker_phone').on('click', function() {
          let otherCode = $('#otherCode').val();
          //other country code in additional to provided
          if (otherCode !== '' && otherCode !== undefined) {
            let select = document.getElementById('phone_country');
            select.options[select.options.length] = new Option(otherCode, otherCode);
            $('#phone_country').val(otherCode);
            if ($('#countryCodePhone').val() !== '') {
              $('#otherCode').remove();
            }
          }
        });
    },
    methods: {
        openPop(position) {
            this[position + 'Popup'] = true;
        },
        close(position) {
            this[position + 'Popup'] = false;
        },
        saveEmailInfo(){
          var be = $('#booker_email');
          be.val(be.val().toLowerCase().replace(' ',''));
          var email =  be.val();
          $.post("/saveEmailInfo", {
            '_token': $('meta[name="csrf_token"]').attr('content'),
            'email': email,
          }, function(data) {
          });
        },
        preventClick() {
            this.startLoading = true; // start full page loading
            document.getElementById('bt').disabled = true;
            $('#bt').removeClass('btn-ready');
        },
        ableClick() {
            this.startLoading = false; // stop full page loading
            document.getElementById('bt').disabled = false;
            $('#bt').addClass('btn-ready');
        },
        calc() {
            if (document.getElementById('cb').checked) {
                this.ableClick();
            } else {
                this.preventClick();
            }
        },
        _checkOrderStatus(data) { // citcon book result modal for android chrome
            let self = this;
            self.startLoading = false; // stop full page loading
            if (data.voucherUrl) {
                window.location.replace(data.voucherUrl);
            }else if (data === '') {
               this.modalContent = this.langBook.bookSuccessEmailLater;
            } else if (data['errorId'] ==='ER1401' || data['errorId'] ==='ER1301' || data['errorId'] ==='ER1501' || data['errorId'] ==='ER1413') {
                this.modalContent = this.langBook.shortSoldOut + '' + this.langBook.bookFailedRefund;
            } else if (data['errorId'] ==='ER1411' || data['errorId'] ==='ER1402' || data['errorId'] ==='ER1302') {
                this.modalContent = this.langBook.shortPriceLimited + '' + this.langBook.bookFailedRefund;
            } else if (data['errorId'] ==='ER1414') {
                this.modalContent = this.langBook.duplicate.replace('xxx', data['errorMessage']);
            } else if (data['errorId'] ==='ER1004') {
                this.modalContent = this.langBook.shortSessionExpired + '' + this.langBook.bookFailedRefund;
            } else if (data['errorId'] ==='IT1001') {
                this.modalContent = this.langBook.cnetwork;
            } else if (data['errorId'] ==='ER1415') {
                this.showModal = false;
                swal({
                    title: '',
                    text: this.langBook.confirmDu,
                    type: 'warning',
                    confirmButtonText: this.langBook.confirm,
                    showCancelButton: true,
                    cancelButtonText: this.langBook.cancel,
                }).then(function(confirmed) {
                    if (confirmed) {
                        self.showModal = true;
                        $.post('/duplicate/proceed', {
                            '_token': self.token,
                            'action': 'continue',
                            'Reorderid': self.reference,
                            'vendor': self.vendorType,
                        }, function(data) {
                            self._checkOrderStatus(data);
                        });
                    } else {
                        $.post('/duplicate/proceed', {
                            '_token': self.token,
                            'action': 'cancel',
                            'Reorderid': self.reference,
                            'vendor': self.vendorType,
                        }, function(data) {
                            if (!data.success) {
                                data['message'] = self.langBook.bookFailed;
                            }
                            swal({
                                title: '',
                                text: data.message,
                                type: 'warning',
                                confirmButtonText: 'OK',
                            }).then(function() {
                                window.location.href = self.hotelPath;
                            });
                        });
                    }
                });
            } else {
                this.modalContent = this.langBook.cnetwork;
            }
        },
        checkRoomStatus(data) { // checkrooms and reserve result
            var self = this;
            self.startLoading = false;
            if (data.errorId === 'ER1401' || data.errorId === 'ER1301' || data['errorId'] === 'ER1501') {
                document.getElementById('bt').disabled = true;
                $('#bt').after('</br><p style=\'color:red\'>' + this.langBook.soldOut + '</p>');
                $('#book-form :input').prop('disabled', true);
                swal({
                    title: '',
                    text: this.langBook.soldOut,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'OK',
                    allowOutsideClick: false,
                }).then(function() {
                    window.location.href = self.hotelPath;
                });
            } else if (data['errorId'] === 'ER1413') {
                document.getElementById('bt').disabled = true;
                $('#bt').after('</br><p style=\'color:red\'>' + this.langBook.priceLimit + '</p>');
                $('#book-form :input').prop('disabled', true);
                swal({
                    title: '',
                    text: this.langBook.priceLimit,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'OK',
                    allowOutsideClick: false,
                }).then(function() {
                    window.location.href = self.hotelPath;
                });
            } else if (data['errorId'] === 'ER1411' || data['errorId'] === 'ER1402' || data['errorId'] === 'ER1302') {
                document.getElementById('bt').disabled = true;
                $('#bt').after('</br><p style=\'color:red\'>' + this.langBook.priceChange + '</p>');
                $('#book-form :input').prop('disabled', true);
                swal({
                    title: '',
                    text: this.langBook.priceChange,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'OK',
                    allowOutsideClick: false,
                }).then(function() {
                    window.location.href = self.hotelPath;
                });
            } else if (data['errorId'] === 'ER1414') {
                var errMsg =  this.langBook.duplicate.replace('xxx',  data['errorMessage']);
                document.getElementById('bt').disabled = true;
                $('#bt').after('</br><p style=\'color:red\'>' + errMsg + '</p>');
                $('#book-form :input').prop('disabled', true);
                swal({
                    title:'',
                    text: errMsg,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: this.langBook.backToSearch,
                    allowOutsideClick: false,
                }).then(function() {
                    window.location.href = self.hotelPath;
                });
            } else if (data['errorId'] === 'ER1004') {
                document.getElementById('bt').disabled = true;
                $('#bt').after('</br><p style=\'color:red\'>' + this.langBook.sessionExpire + '</p>');
                $('#book-form :input').prop('disabled', true);
                swal({
                    title: '',
                    text: this.langBook.sessionExpire,
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    allowOutsideClick: false,
                    confirmButtonText: 'OK',
                }).then(function() {
                    window.location.href = self.hotelPath;
                });
            } else if (data['errorId'] === 'IT1001') {
                document.getElementById('bt').disabled = true;
                $('#bt').after('</br><p style=\'color:red\'>' + this.langBook.cnetwork + '</p>');
                $('#book-form :input').prop('disabled', true);
                swal({
                    title: this.langBook.cnetwork,
                    type: 'warning',
                    text: data.message,
                    confirmButtonColor: '#DD6B55',
                    allowOutsideClick: false,
                    confirmButtonText: this.langBook.backToSearch,
                }).then(function() {
                    window.location.href = self.hotelPath;
                });
            } else if (data === '') {
              swal({
                title: this.langBook.bookSuccessEmailLater,
                type: 'success',
                confirmButtonColor: '#DD6B55',
                allowOutsideClick: false,
                confirmButtonText: 'OK',
              }).then(function() {
                window.location.href = self.hotelPath;
              });
            } else {
                document.getElementById('bt').disabled = true;
                $('#bt').after('</br><p style=\'color:red\'>' + data['errorMessage'] + '</p>');
                $('#book-form :input').prop('disabled', true);
                swal({
                    title: '',
                    text: data['errorMessage'],
                    type: 'warning',
                    confirmButtonColor: '#DD6B55',
                    allowOutsideClick: false,
                    confirmButtonText: this.langBook.backToSearch,
                }).then(function() {
                    window.location.href = self.hotelPath;
                });
            }
        },
        _isWechatBrowser() {
            var ua = navigator.userAgent.toLowerCase();
            return (/micromessenger/.test(ua)) ? true : false;
        },
        _isQQBrowser() {
            var ua = navigator.userAgent.toLowerCase();
            return (/qq/.test(ua)) && !(/mqqbrowser/.test(ua)) ? true : false;
        },
        doRoomCheck() {
            this.preventClick();
            if (typeof(Storage) !== 'undefined') {
                localStorage.removeItem('reference');
                localStorage.removeItem('temp');
            }
            var rf = new Array();
            var status = this.room.status;

            for (var i = 0; i < this.roomCount; i++) {
                rf[i] = {'roomReference': this.roomRef};
            }

            let self = this;
            $.post('/rooms/check', {
                '_token': this.token,
                'references': JSON.stringify(rf),
                'roomCount': this.roomCount,
                'dayCount': this.dayCount,
                'displayId': this.displayId,
            }, function(data) {
                if (data.hasOwnProperty('success') && !data['success']) {
                    if ('type' in data && data['type'] === 'SESSION_TIMEOUT') {
                        document.getElementById('bt').disabled = true;
                        $('#bt').after('</br><p style=\'color:red\'>' + self.langBook.shortSessionExpired + '</p>');
                        $('#book-form :input').prop('disabled', true);
                        swal({
                            title: '',
                            text: self.langBook.shortSessionExpired,
                            type: 'warning',
                            confirmButtonColor: '#DD6B55',
                            allowOutsideClick: false,
                            confirmButtonText: self.langBook.backToSearch,
                        }).then(function() {
                            window.location.href = self.hotelPath;
                        });
                    } else if (data['errorId'] === 'IT1003') {
                        document.getElementById('bt').disabled = true;
                        $('#book-form :input').prop('disabled', true);
                        $('#bt').after("</br><p style='color:red'>"+self.langBook.networkErr+"</p>");
                        swal({
                          title: self.langBook.networkErr,
                          type: 'warning',
                          confirmButtonColor: '#DD6B55',
                          allowOutsideClick: false,
                          confirmButtonText: 'OK',
                        }).then(() => {
                          window.location.href = self.hotelPath;
                        });
                    }
                } else if (data['errorId']) {
                    self.checkRoomStatus(data);
                } else {
                    var html = '';
                    var html2 = 'None';
                    var cp = '';
                    //房间已定完
                    if (data.length === 0) {
                        cp = self.langBook.soldOut;
                        window.lockBody(true);
                        document.getElementById('bt').disabled = true;
                        $('#bt').after('</br><p style=\'color:red\'>' + self.langBook.networkErr + '</p>');
                        $('#book-form :input').prop('disabled', true);
                        swal({
                            title: '',
                            text: self.langBook.soldOut,
                            type: 'warning',
                            confirmButtonColor: '#DD6B55',
                            allowOutsideClick: false,
                            confirmButtonText: 'OK',
                        }).then(() => {
                          window.location.href = self.hotelPath;
                        });
                    } else {
                        var searched_room = data[0];
                        self.room = searched_room;

                        var count = 0;
                        if (searched_room.cancellationPolicies && searched_room.cancellationPolicies.length > 0) {
                            let value = 0;
                            $.each(searched_room.cancellationPolicies, function(j, cancel) {
                                if (cancel.amount == 0 && count == 0) {
                                    value = 1;
                                    cp = self.langBook.freeCancelBefore + cancel.end.substr(0, 19).replace(/T/g, ' ') + '<br>';
                                    count++;
                                }
                                else {
                                    value = 1;
                                    cp = self.langBook.nonRef;
                                }
                            });
                            if (0 == value) {
                                cp = self.langBook.nonRef;
                            }
                        } else {
                            cp = self.langBook.nonRef;
                        }

                        // use check rooms roomRef for booking
                        // pass room comments
                        self.roomRef = searched_room.roomReference;
                        if (searched_room && searched_room.comments && searched_room.comments[0] && searched_room.comments[0].comments && searched_room.comments[0].comments !== '') {
                            html2 = searched_room.comments[0].comments;
                        } else if (roomComments != null && roomComments !== '' && roomComments !== '[]') {
                            html2 = roomComments;
                        }
                        // show mandatory fee

                        if (searched_room.totalMandatoryFees && searched_room.totalMandatoryFees !== '0.0' || searched_room.mandatoryFeesDetails && searched_room.mandatoryFeesDetails[0] && searched_room.mandatoryFeesDetails[0]['amount']
                          && searched_room.mandatoryFeesDetails[0]['amount'] !== '0.0') {
                            var currency = searched_room.currency;
                            if (currency === 'USD') {
                                currency = '$';
                            }
                            if (searched_room.totalMandatoryFees != null) {
                                $('#HotelDue').removeClass('hidden');
                                $('.HotelDue').text(currency + searched_room.totalMandatoryFees);
                                $('#total_mandatory_fees').val(searched_room.totalMandatoryFees); //如果book拿到mandatoryfee的值，写去order_temp数据库
                            } else {
                                $('#HotelDue').removeClass('hidden');
                                $('.HotelDue').text(searched_room.mandatoryFeesDetails[0]['currency'] + ' ' + searched_room.mandatoryFeesDetails[0]['amount']);
                                $('#total_mandatory_fees').val(searched_room.mandatoryFeesDetails[0]['amount']);  //如果book拿到mandatoryfee的值，写去order_temp数据库
                            }
                        }

                        if (searched_room.netPrice) {
                            let temp = parseInt(self.roomCount);
                            let temp2 = searched_room.netPrice;
                            let temp1 = parseInt(temp2 / temp);
                            var res = parseInt(temp1 * temp);
                            if (searched_room.beforeTax) {
                                $('.roomPrice').text(searched_room.currency + ' ' + searched_room.beforeTax / temp / dayCount);
                            }
                            $('#roomTax').text(searched_room.tax);
                            $('.totalPrice').text(res);
                            $('#book-form').append('<input type="hidden" name="totalPrice"  value="' + res + '"/>');

                            if(searched_room.cleanFee !== 0){
                                $('#cleanFee').removeClass('hidden');
                                $('.clean-fee').text(searched_room.cleanFee);
                                $('#netPrice').html(res);
                            }else{
                              $('#cleanFee').hide();
                            }
                        } else {
                            let temp = parseInt(self.roomCount);
                            let temp1 = self.singleRoomNightPrice;
                            var res = temp1 * temp;
                            res = parseInt(res);
                            $('.roomPrice').text(self.currency + ' ' + temp1);
                            $('.totalPrice').text(res);
                            $('#book-form').append('<input type="hidden" name="totalPrice"  value="' + res + '"/>');
                        }
                        $('#ppBtn').attr('disabled',false);
                        self.totalPrice = res;
                    }

                    html += cp;
                    document.getElementById('bt').disabled = false;
                    $('#bt').addClass('btn-ready');
                    $('#hotel-cancel').html(html);
                    $('#hotel-comment').html(html2);
                    loadingEnd();
                }
            });
            this.ableClick();
        },
        buildRating(rating, category) {
            var r = Number(rating);
            if (r == NaN) {
                return category;
            }
            if (r >= 5) {
                r = 5;
            }
            var h = '';
            for (var i = 0; i <= r; i++) {
                h += '<span class="entypo-star"></span>';
            }

            return h;
        },
        bookRooms() {
            //change check box to img to avoid second payment - 03132018
            $('#paymentAuth').html('<img class="validPic" src="/img/general/icon/valid.png">');

            let vendorType = 'po';
            var radio = document.getElementsByName('imgsel');
            for (let i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    vendorType = radio[i].value;
                }
            }

            var bookForm = $('#book-form');
            bookForm.append('<input type="hidden" name="tempType"  value="' + vendorType + '"/>');

            if (this.isWechat) {
                bookForm.append('<input type="hidden" name="isWechatBrowser"  value="true"/>');
            }

            var r = bookForm.valid();
            if (!r) {
                // complete inputs!
                toastr.error(this.langBook.plzComplete, 'Error');
                return;
            }
            if (vendorType === 'al') {
                toastr.warning(this.langBook.alipayComeBack);
            }

            this.preventClick();

            var anaData = {
                'id': this.hotelId,
                'name': this.hotelName,
                'variant': this.room.name,
                'brand': this.room.roomReference.substr(0, 1),
                'price': this.room.netPrice,
                'qty': this.roomCount,
                'vendor': this.vendorType,
            };
            onInfoComplete(anaData);

            // 添加相同名字检查
            var guestNames = new Array();
            for (var i = 1; i <= this.roomCount; i++) {
                guestNames.push($('#guest' + i + '_a1_ln').val() + $('#guest' + i + '_a1_fn').val());
            }

            for (var i = 0; i < guestNames.length - 1; i++) {
                for (var j = i + 1; j < guestNames.length; j++) {
                    if (guestNames[i] == guestNames[j]) {
                        this.samename = true;
                    }
                }
            }

            let self = this;
            if (this.samename) {
                swal({
                    title: '',
                    text: this.langBook.samename,
                    type: 'warning',
                    showCancelButton: true,
                    allowOutsideClick: false,
                    confirmButtonText: this.langBook.yes,
                    cancelButtonText: this.langBook.no,
                }).then(
                    (result) => {
                        if (result.value) {
                            swal(
                                self.langBook.continueBooking,
                                self.langBook.processingBooking,
                                'success',
                            );
                            self.bookRoomsGo(vendorType);
                        } else {
                            self.ableClick();
                        }
                    });
            } else {
                self.bookRoomsGo(vendorType); //沒有相同姓名繼續下单
            }
        },
        bookRoomsGo(vendorType) {
            var bookForm = $('#book-form');
            let url = '/reserve/' + this.sessionKey + '?rooms_key=' + this.rooms_key;
            let self = this;

            $.post(url, bookForm.serialize(), function(data) {
                if (data.success) {
                    var mobileSystem = self._getMobileOperatingSystem();

                    // store vendor type and reference to localstorage for android phone continue order
                    if (typeof (Storage) !== 'undefined' && mobileSystem !== 'ios' && (vendorType ==='al' || vendorType ==='we')) {
                        localStorage.setItem('reference', data.reference);
                        localStorage.setItem('temp', vendorType);
                    }
                    //   跳转到 paypal
                    window.location.href = data.voucherUrl;
                } else {
                    self.checkRoomStatus(data);
                }
            });
        },
        savePBInfo(data,type) {
          let self = this;
          $.post("/paypal/button", {
            '_token': this.token,
            'data': data,
            'type': type,
          }, function (res) {
            if(res.payId !== ''){
              if(document.getElementById("payId")===null){
                $('#book-form').append('<input type="hidden" id="payId" name="payId" value="' +  res.payId + '"/>');
              }
            }
            if(type === 'response'){
              self.bookRooms();
            }
          });
        },
        _getMobileOperatingSystem() {
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;

            // Windows Phone must come first because its UA also contains "Android"
            if (/windows phone/i.test(userAgent)) {
                return 'Windows Phone';
            }

            if (/android/i.test(userAgent)) {
                return 'android';
            }

            // iOS detection from: //stackoverflow.com/a/9039885/177710
            if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                return 'ios';
            }

            return 'unknown';
        },
        cancelBook() {
            this.showModal = false;
            this.confirmBook(); // in case user paid and clicked cancel by mistake
        },
        confirmBook() { // for android chrome when it comes back to book page after payment without opening callback url
            let self = this;
            let getUrl = '/ccpay/inquire';

            $.get(getUrl, {
                'reference': this.reference
            }, function(inquireData) {
                if (inquireData.type === 'charge' && inquireData.status === 'success') {
                    self.modalContent = self.langBook.placeOrder;

                    $.post('/ccpay/order', {
                        '_token': self.token,
                        'Reorderid': self.reference,
                        'vendor': self.vendorType,
                    }, function(data) {
                        self._checkOrderStatus(data);
                    });
                } else if (inquireData.type === 'refund' && inquireData.status === 'refunded') {
                    self.modalContent = self.langBook.bookFailed[self.langStr] + ', ' + self.langBook.refunded[self.langStr];
                } else {
                    self.modalContent = self.langBook.paymentFailed;
                }
            });
        },
        applyCoupon() {
            if (this.couponCode) {
                let self = this;
                $.post('/coupon/apply', {
                    '_token': this.token,
                    'code': this.couponCode,
                    'hotelId': this.displayId,
                    'checkin': this.checkin
                }, function (data) {
                    if (data.success && data.amount) {
                        self.invalidCoupon = false;
                        $('.applied-coupon-block').show();
                        $('#couponAmount').text('-USD ' + data.amount);
                        $('#netPrice').text(self.totalPrice - data.amount);
                        $('#couponCode').val(self.couponCode);
                        self.appliedAmount = data.amount;
                    } else {
                        self.invalidCoupon = true;
                        if (data.message) {
                            self.couponErrorMsg = data.message;
                        }
                    }
                });
            }
        },
        removeCoupon() {
            let self = this;
            $.get('/coupon/remove', function (data) {
                if (data.success) {
                    self.appliedAmount = 0;
                    $('#couponCode').val('');
                    $('.applied-coupon-block').hide();
                    $('#netPrice').text(self.totalPrice);
                }
            });
        }
    },
    watch: {
        topPopup(val) {
            if (val) {
                setTimeout(() => {
                    this.topPopup = false;
                }, 2000);
            }
        },
    },
});

