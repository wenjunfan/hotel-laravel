var dealsListApp = new Vue({
    el: '#deals_list_app',
    data: {
        _token: '',
        appUrl: appUrl,
        isUpdateCoupon: false,
        coupons: coupons,
        coupon: null,
        img_pc: null,
        img_mobile: null,
    },
    computed: {
        imageRequired: function () {
            return !this.isUpdateCoupon && (this.coupon.hide_image !== 1);
        }
    },
    mounted () {
        var self = this;
        this._token = $('meta[name="csrf_token"]').attr('content');
        this.coupon = this.newCouponGen();

        $('#coupons_list').DataTable();

        jQuery.validator.addMethod("greaterThan", function (value, element, params) {
            if (value && $(params[0]).val()) {
                if (!/Invalid|NaN/.test(new Date(value))) {
                    return new Date(value) > new Date($(params[0]).val());
                }

                return true;
            } else {
                return true;
            }
        }, 'Must be greater than {1}.');

        $('#coupon_form').validate({
            focusCleanup: true,
            rules: {
                'expires_at': {greaterThan: ['#start_at', 'start date']},
                'checkin_end_date': {greaterThan: ['#checkin_start_date', 'start date']}
            }
        });

        setTimeout(function () {
            //datepicker setup
            $('.datepicker').datepicker({
                startDate: '0d',
                todayBtn: "linked",
                autoclose: true,
                format: "yyyy-mm-dd",
            }).on('changeDate', function (selected) {
                if (selected.date) {
                    var target = $(selected.target).attr('id');
                    var date = moment(new Date(selected.date.valueOf())).format('YYYY-MM-DD');
                    if (target === 'start_at') {
                        self.coupon.start_at = date;
                        $('#expires_at').datepicker('setStartDate', date);
                        self.coupon.expires_at = moment(date).add(1, 'days').format('YYYY-MM-DD');
                    } else if (target === 'expires_at') {
                        self.coupon.expires_at = date;
                    } else if (target === 'checkin_start_date') {
                        self.coupon.checkin_start_date = date;
                        $('#checkin_end_date').datepicker('setStartDate', date);
                        self.coupon.checkin_end_date = moment(date).add(1, 'days').format('YYYY-MM-DD');
                    } else if (target === 'checkin_end_date') {
                        self.coupon.checkin_end_date = date;
                    }
                }
            });
        }, 300);
    },
    methods: {
        newCouponGen: function () {
            return {
                hide_image: 0,
                title: '',
                title_zh: '',
                name: '',
                name_zh: '',
                code: '',
                amount: '',
                hotelIds: '',
                start_at: '',
                expires_at: '',
                checkin_start_date: '',
                checkin_end_date: '',
                enabled: 1
            };
        },
        processFile (event, fileName) {
            this[fileName] = event.target.files[0];
        },
        openNewCouponModal () {
            this.isUpdateCoupon = false;
            this.coupon = this.newCouponGen();
            $('#couponModal').modal('show');
        },
        openUpdateCouponModal (couponId) {
            this.coupon = this.coupons.find(function (item) {
                return item.id == couponId;
            });
            this.isUpdateCoupon = true;
            $('#couponModal').modal('show');
        },
        createCoupon () {
            var $couponForm = $('#coupon_form');
            if ($couponForm.valid()) {
                var self = this;
                var ladda = $('#submit_btn').ladda();
                ladda.ladda('start');
                this.coupon['_token'] = this._token;
                $.post('/affiliate/create-coupon', this.coupon, function (data) {
                    if (data.success) {
                        ladda.ladda('stop');
                        self.uploadImages(data.id);
                    }
                });
            }
        },
        updateCoupon () {
            if (this.coupon && $('#coupon_form').valid()) {
                var self = this;
                var ladda = $('#submit_btn').ladda();
                ladda.ladda('start');
                this.coupon['_token'] = this._token;
                delete this.coupon['coupon_history'];

                $.post('/affiliate/update-coupon/' + this.coupon.id, this.coupon, function (data) {
                    ladda.ladda('stop');
                    $('.btn-disable').ladda().ladda('stop');
                    if (data.success) {
                        if (self.img_pc || self.img_mobile) {
                            self.uploadImages(data.id);
                        } else {
                            $('#couponModal').modal('hide');
                            location.reload();
                        }
                    } else if (data.message) {
                        swal(data.message);
                    }
                });
            }
        },
        uploadImages (id) {
            var formData = new FormData();
            formData.append('_token', this._token);
            formData.append('img_pc', this.img_pc);
            formData.append('img_mobile', this.img_mobile);

            var xhr = new XMLHttpRequest();
            var url = this.appUrl + '/affiliate/coupon/upload-images/' + id;
            xhr.open('post', url, true);
            xhr.send(formData);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    var response = $.parseJSON(xhr.response);
                    if (response.success) {
                        $('#couponModal').modal('hide');
                        location.reload();
                    } else {
                        alert(response.errorMessage ? response.errorMessage : 'Something went wrong')
                    }
                }
            }
        },
        disableCoupon () {
            this.coupon.enabled = 0;
            $('.btn-disable').ladda().ladda('start');
            this.updateCoupon();
        },
        toggleDisabled () {
            let $disabledCoupons = $('.coupon-item-disabled');
            let $btnToggler = $('.btn-toggle-disabled');
            if ($disabledCoupons.hasClass('hidden')) {
                $disabledCoupons.removeClass('hidden');
                $btnToggler.find('span').text('隐藏');
            } else {
                $disabledCoupons.addClass('hidden');
                $btnToggler.find('span').text('显示');
            }
        }
    }
});