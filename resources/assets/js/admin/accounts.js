new Vue({
    el: '#accountApp',
    data: {
        keywordType: parseInt(keywordType),
        keyword: keyword,
        keywordText: 'Select',
        now: moment(),
        adminLevel: adminLevel,
        userId: userId,
        bdd: userId === '11313',  // 是否是商务负责人
        dateStart: dateStart,
        dateEnd: dateEnd,
        payType: payType,
        accountStatus: accountStatus,
        csrfToken: $('[name="csrf_token"]').attr('content'),
    },
    mounted () {
        var self = this;
        switch (this.keywordType) {
            case 1:
                self.keywordText = '用户名/公司名/联系人';
                break;
            case 2:
                self.keywordText = '用户id';
                break;
            case 3:
                self.keywordText = '电话';
                break;
            case 4:
                self.keywordText = '佣金比例';
                break;
            case 5:
                self.keywordText = '用户邮箱';
                break;
            case 6:
                self.keywordText = '用户权限';
                break;
            case 7:
                self.keywordText = '用户国家';
                break;
            default:
                self.keywordText = 'Select'
        }
        // {{--table插件 一定要写到mounted里，否则容易失效--}}
        $('#dataTable').DataTable({
            // "pageLength": 25,
            'order': [[0, 'desc']],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {
                    extend: 'excel',
                    title: '合作商账号列表'
                },
                {
                    extend: 'pdf',
                    title: '合作商账号列表'
                },
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit')
                    }
                }
            ]
        });
        // format date picker format
        $('#endDate, #startDate').datepicker({
            format: 'yyyy-mm-dd',
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
        });
        // watch date change
        $('#startDate').datepicker().on(
            'changeDate', () => {
                self.dateStart = $('#startDate').val();
                self.dateEnd = $('#endDate').val()
            }
        );
        $('#endDate').datepicker().on(
            'changeDate', () => {
                self.dateEnd = $('#endDate').val()
            }
        );

        $('#from').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#to').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            format: "yyyy-mm-dd"
        });
    setInterval(this.refreshToken, 3600000); // 1 hour
    },
    methods: {
        search () {
            let search_filter = $('#btn-search-filter');
            search_filter.text('loading ...');
            search_filter.addClass('disabled');
            let newUrl = '/admin/accounts?creationStart=' + this.dateStart + '&creationEnd=' + this.dateEnd;

            if (this.accountStatus !== "" && this.accountStatus < 2) {
                newUrl += '&accountStatus=' + this.accountStatus
            }
            if (this.keywordType !== "" && this.keyword !== "") {
                newUrl += '&keywordType=' + this.keywordType + '&keyword=' + this.keyword
            }
            if (this.payType !== "" && this.payType !== 'all') {
                newUrl += '&payType=' + this.payType
            }
            window.location.href = newUrl
        },
        updateAllUserFinanceRecords () {
            FullscreenSpinner.create();
            let url = window.usitrip.basePath + 'update-all-user-finance-records';
            $.post(url, {
                '_token': this.csrfToken,
            }, function (data) {
                if (data.success) {
                    FullscreenSpinner.destroy();
                    swal("更新成功", data.message, "success");
                    location.reload();
                } else {
                    FullscreenSpinner.destroy();
                    swal("更新失败", data.message, "error");
                }
            });
        },
        refreshToken () {
            $.get('refresh-csrf').done(function (data) {
                this.csrfToken = data;
            });
        },
        fixUser (id) {
            var company_name = document.getElementById('company_name' + id).value;
            var company_phone = document.getElementById('company_phone' + id).value;
            var channel = document.getElementById('channel' + id).value;
            var cardholder = document.getElementById('cardholder' + id).value;
            var remark = document.getElementById('remark' + id).value;
            FullscreenSpinner.create();
            $.post(window.usitrip.basePath + 'business/user/update', {
                '_token': this.csrfToken,
                'id': id,
                'company_name': company_name,
                'company_phone': company_phone,
                'channel': channel,
                'cardholder': cardholder,
                'remark': remark
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    swal("更新成功", data.message, "success");
                    location.reload();
                    document.getElementById('company_name' + id).readOnly = true;
                    $("#roEditBtn" + id).toggleClass("hidden");
                    $("#roSaveBtn" + id).toggleClass("hidden");
                } else {
                    swal("更新失败", data.message, "error");
                }
            });
        },
        fixUsera (id) {
            var company_name = document.getElementById('company_name' + id).value;
            var company_phone = document.getElementById('company_phone' + id).value;
            var channel = document.getElementById('channel' + id).value;
            var cardholder = document.getElementById('cardholder' + id).value;
            var remark = document.getElementById('remark' + id).value;
            FullscreenSpinner.create();
            $.post(window.usitrip.basePath + 'business/user/update', {
                '_token': this.csrfToken,
                'id': id,
                'company_name': company_name,
                'company_phone': company_phone,
                'channel': channel,
                'cardholder': cardholder,
                'remark': remark
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    swal("更新成功", data.message, "success");
                    location.reload();
                    document.getElementById('company_phone' + id).readOnly = true;
                    $("#roEditBtn1" + id).toggleClass("hidden");
                    $("#roSaveBtn1" + id).toggleClass("hidden");
                } else {
                    swal("更新失败", data.message, "error");
                }
            });
        },
        fixUserb (id) {
            var company_name = document.getElementById('company_name' + id).value;
            var company_phone = document.getElementById('company_phone' + id).value;
            var channel = document.getElementById('channel' + id).value;
            var cardholder = document.getElementById('cardholder' + id).value;
            var remark = document.getElementById('remark' + id).value;
            FullscreenSpinner.create();
            $.post(window.usitrip.basePath + 'business/user/update', {
                '_token': this.csrfToken,
                'id': id,
                'company_name': company_name,
                'company_phone': company_phone,
                'channel': channel,
                'cardholder': cardholder,
                'remark': remark
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    swal("更新成功", data.message, "success");
                    location.reload();
                    document.getElementById('channel' + id).readOnly = true;
                    $("#roEditBtn2" + id).toggleClass("hidden");
                    $("#roSaveBtn2" + id).toggleClass("hidden");
                } else {
                    swal("更新失败", data.message, "error");
                }
            });
        },
        fixUserc (id) {
            var company_name = document.getElementById('company_name' + id).value;
            var company_phone = document.getElementById('company_phone' + id).value;
            var channel = document.getElementById('channel' + id).value;
            var cardholder = document.getElementById('cardholder' + id).value;
            var remark = document.getElementById('remark' + id).value;
            FullscreenSpinner.create();
            $.post(window.usitrip.basePath + 'business/user/update', {
                '_token': this.csrfToken,
                'id': id,
                'company_name': company_name,
                'company_phone': company_phone,
                'channel': channel,
                'cardholder': cardholder,
                'remark': remark
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    swal("更新成功", data.message, "success");
                    location.reload();
                    document.getElementById('cardholder' + id).readOnly = true;
                    $("#roEditBtn3" + id).toggleClass("hidden");
                    $("#roSaveBtn3" + id).toggleClass("hidden");
                } else {
                    swal("更新失败", data.message, "error");
                }
            });
        },
        formatDate (date) {
            var monthNames = [
                "Jan", "Feb", "Mar",
                "Apr", "May", "June", "July",
                "Aug", "Sept", "Oct",
                "Nov", "Dec"
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var month = monthIndex + 1;
            var year = date.getFullYear();
            var hour = date.getHours();
            var minute = date.getMinutes();
            return day + '-' + monthNames[monthIndex] + '-' + year + ' ' + hour + ':' + minute + ' ';
            // return day + '/' + month + '/'+ year;
        },
        fixUser1 (id) {
            $('#company_name' + id).prop('readonly', false).focus();
            $("#roEditBtn" + id).toggleClass("hidden");
            $("#roSaveBtn" + id).toggleClass("hidden");
        },
        fixUser2 (id) {
            $('#company_phone' + id).prop('readonly', false).focus();
            $("#roEditBtn1" + id).toggleClass("hidden");
            $("#roSaveBtn1" + id).toggleClass("hidden");
        },
        fixUser3 (id) {
            $('#channel' + id).prop('readonly', false).focus();
            $("#roEditBtn2" + id).toggleClass("hidden");
            $("#roSaveBtn2" + id).toggleClass("hidden");
        },
        fixUser4 (id) {
            $('#cardholder' + id).prop('readonly', false).focus();
            $("#roEditBtn3" + id).toggleClass("hidden");
            $("#roSaveBtn3" + id).toggleClass("hidden");
        },
        admin (id, adminStatus) {
            FullscreenSpinner.create();
            $.ajax({
                method: 'PUT',
                headers: {'X-CSRF-TOKEN': this.csrfToken},
                url: window.usitrip.basePath + 'admin/accounts/' + id,
                data: {'admin': adminStatus},
                success: function (data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal({
                            title: (adminStatus === 4 ? "账户已经设为分销账户" : "账户已经关闭分销功能"),
                            text: "",
                            confirmButtonText: "确定",
                            type: "success"
                        }, function () {
                            location.reload();
                        });
                    } else {
                        swal(data.message);
                    }
                },
                error: function (xhr, e) {
                    swal(xhr.status + " " + e);
                },
            });
        },
        seeProviderToggle (id, wantStatus) {
            FullscreenSpinner.create();
            $.ajax({
                method: 'PUT',
                headers: {'X-CSRF-TOKEN': this.csrfToken},
                url: window.usitrip.basePath + 'admin/accounts/' + id,
                data: {'see_provider': wantStatus},
                success: function (data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal({
                            title: '更改成功',
                            text: '',
                            confirmButtonText: "确定",
                            type: "success"
                        }, function () {
                            location.reload();
                        });
                    } else {
                        swal(data.message);
                    }
                },
                error: function (xhr, e) {
                    FullscreenSpinner.destroy();
                    swal(xhr.status + " " + e);
                },
            });
        },
        countryCodeToggle (id, code) {
            FullscreenSpinner.create();
            $.ajax({
                method: 'PUT',
                headers: {'X-CSRF-TOKEN': this.csrfToken},
                url: window.usitrip.basePath + 'admin/accounts/' + id,
                data: {'Country_Code': code},
                success: function (data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal({
                            title: '更改成功',
                            text: '',
                            confirmButtonText: "确定",
                            type: "success"
                        }, function () {
                            location.reload();
                        });
                    } else {
                        swal(data.message);
                    }
                },
                error: function (xhr, e) {
                    FullscreenSpinner.destroy();
                    swal(xhr.status + " " + e);
                },
            });
        },
        ratioToggle (id, f) {
            FullscreenSpinner.create();
            $.ajax({
                method: 'PUT',
                headers: {'X-CSRF-TOKEN': this.csrfToken},
                url: window.usitrip.basePath + 'admin/accounts/' + id,
                data: {'ratio': f},
                success: function (data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal({
                            title: "更改成功",
                            text: "",
                            confirmButtonText: "确定",
                            type: "success"
                        }, function () {
                            location.reload();
                        });
                    } else {
                        swal(data.message);
                    }
                },
                error: function (xhr, e) {
                    FullscreenSpinner.destroy();
                    swal(xhr.status + " " + e);
                },
            });
        },
        paymentToggle (id, targetType) {
            FullscreenSpinner.create();
            $.ajax({
                method: 'PUT',
                headers: {'X-CSRF-TOKEN': this.csrfToken},
                url: window.usitrip.basePath + 'admin/accounts/' + id,
                data: {'paymentType': targetType},
                success: function (data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal({
                            title: '更改成功',
                            text: '',
                            confirmButtonText: "确定",
                            type: "success"
                        }, function () {
                            location.reload();
                        });
                    } else {
                        swal(data.message);
                    }
                },
                error: function (xhr, e) {
                    FullscreenSpinner.destroy();
                    swal(xhr.status + " " + e);
                },
            });
        },
        active (id, activeStatus) {
            FullscreenSpinner.create();
            $.ajax({
                method: 'PUT',
                headers: {'X-CSRF-TOKEN': this.csrfToken},
                url: window.usitrip.basePath + 'admin/accounts/' + id,
                data: {'active': activeStatus},
                success: function (data) {
                    FullscreenSpinner.destroy();
                    if (data.success) {
                        swal({
                            title: (activeStatus === 1 ? "账户已经激活" : "账户已经关闭"),
                            text: "",
                            confirmButtonText: "确定",
                            type: "success"
                        }, function () {
                            location.reload();
                        });
                    } else {
                        swal(data.message);
                    }
                },
                error: function (xhr, e) {
                    FullscreenSpinner.destroy();
                    swal(xhr.status + " " + e);
                },
            });
        },
        deactiveAccount (id) {
            let self = this;
            swal({
                    title: "确认要关闭这个账户吗？",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "取消",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal("...", "正在关闭这个账户!", "success");
                        self.active(id, 0);
                    }
                });
        },
        addAdmin (id) {
            let self = this;
            swal({
                    title: "确认要把这个用户设为管理员吗？",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "取消",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal("...", "正在把这个用户设为管理员", "success");
                        self.admin(id, 1);
                    }
                });
        },
        addAffiliate (id) {
            swal({
                    title: "确认要把这个用户设为分销账户吗？",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "取消",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal("...", "正在添加该用户为分销账户", "success");
                        FullscreenSpinner.create();
                        $.post(window.usitrip.basePath + 'distributor/assign', {
                            '_token': $("input[name='_token']").val(),
                            'id': id,
                        }, function (data) {
                            if (data.success) {
                                FullscreenSpinner.destroy();
                                swal("更新成功", data.message, "success");
                                location.reload();
                            } else {
                                FullscreenSpinner.destroy();
                                swal("更新失败", data.message, "error");
                            }
                        });
                    }
                });
        },
        removeAdmin (id) {
            let self = this;
            swal({
                    title: "确认要关闭这个用户的管理员身份吗？",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "取消",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal("...", "关闭这个用户的管理员身份", "success");
                        self.admin(id, 0);
                    }
                });
        },
        removeAffiliate (id) {
            let self = this;
            swal({
                    title: "确认要关闭这个用户的分销身份吗？",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "取消",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal("...", "正在关闭这个用户的分销身份", "success");
                        self.admin(id, 0);
                    }
                });
        },
        seeProvider (id, see_provider) {
            let self = this;
            var nowStatus = (see_provider === 'false') ? '不可看' : '可看';
            var wantStatus = (see_provider === 'true') ? 'false' : 'true';
            swal({
                    title: "设置看供应商权限",
                    text: "当前看供应商权限为：" + nowStatus + "，确定要更改权限吗？",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "取消",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确认更改",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal("...", "正在修改用户查看供应商权限", "success");
                        self.seeProviderToggle(id, wantStatus);
                    }
                });
        },
        activeAccount (id, company) {
            let self = this;
            if (!company) {
                swal("公司信息没有填写完整，暂时不能激活");
                return;
            }
            swal({
                    title: "确认要激活这个账户吗？",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "取消",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确认",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal("...", "正在激活这个账户", "success");
                        self.active(id, 1);
                    }
                });
        },
        changeCountry_Code (id, code) {
            let self = this;
            let title = '';
            if (code === 'CN') {
                title = '中国用户'
            } else {
                title = '美国用户'
            }
            swal({
                    title: "确定要更改为" + title + '吗？',
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "取消",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal("...", "正在修改这个账户所属国家", "success");
                        self.countryCodeToggle(id, code);
                    }
                });
        },
        changeCommission (id, ratio) {
            let self = this;
            swal({
                    title: "请填写佣金大小",
                    text: "请填写0~0.3范围的小数（即0%至30%）",
                    type: "input",
                    showCancelButton: true,
                    cancelButtonText: "取消",
                    confirmButtonText: "确定",
                    closeOnConfirm: false,
                    animation: "slide-from-top",
                    inputPlaceholder: ratio
                },
                function (inputValue) {
                    if (inputValue === false) {
                        return false;
                    } else if (inputValue === "" || isNaN(inputValue)) {
                        swal.showInputError("请填写一个小数");
                        return false;
                    } else if (parseFloat(inputValue) < 0 || parseFloat(inputValue) > 0.3) {
                        swal.showInputError("请填写0~0.3范围的小数（0.03即是3%）");
                        return false;
                    } else {
                        swal("...", "正在修改这个账户佣金", "success");
                        self.ratioToggle(id, inputValue);
                    }
                });
        },
        changePaymentType (id, type) {
            let self = this;
            var typeName = (type === 'Prepay') ? '预付' : '后付';
            var targetType = (type === 'Prepay') ? 'Postpay' : 'Prepay';
            swal({
                    title: "设置付款方式",
                    text: "当前付款方式为" + typeName + "，确定要更改付款方式吗？",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "取消",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确认更改",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal("...", "正在修改这个账户付款方式", "success");
                        self.paymentToggle(id, targetType);
                    }
                });
        },
        openOpCommendModal (id) {
            $('#addCommentModalTitle').text(id);
            $('#op_remark').html($('#remark' + id).html());
            $('#addOpCommentModal').modal('show');
        },
        updateOpCommend () {
            var id = $('#addCommentModalTitle').html();
            var company_name = document.getElementById('company_name' + id).value;
            var company_phone = document.getElementById('company_phone' + id).value;
            var channel = document.getElementById('channel' + id).value;
            var cardholder = document.getElementById('cardholder' + id).value;
            var authUser = '{{Auth::user()->id}}';
            if (authUser === '11313') {
                authUser = '668';
            } else if (authUser === '10048') {
                authUser = '119';
            }
            var old_remark = $('#op_remark').html().trim();
            var remark = '';
            if (old_remark !== '') {
                remark += old_remark + '&#013;';
            }
            remark += this.formatDate(new Date()) + $('#addComment').val() + " (" + this.userId + ")";

            FullscreenSpinner.create();
            $.post(window.usitrip.basePath + 'business/user/update', {
                '_token': this.csrfToken,
                'id': id,
                'company_name': company_name,
                'company_phone': company_phone,
                'channel': channel,
                'cardholder': cardholder,
                'remark': remark
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    swal("更新成功", data.message, "success");
                    $('#addOpCommentModal').modal('hide');
                    $('#remark' + id).html(remark);
                    $('#addComment').val("");
                } else {
                    swal("更新失败", data.message, "error");
                }
            });
        },
    },
});