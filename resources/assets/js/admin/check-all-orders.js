import { Spinner } from 'spin.js'
/* ORDER STATUS
* CONFIRMED 确认
* REJECTED 拒绝
* CANCELLED 取消
* ON REQUEST 待定
* FAILED失败
* */
//start vue
let app = new Vue({
    el: '#orderApp',
    data: {
        now: moment(),
        isAdmin: isAdmin === 1, // 是否超级管理员
        userId: userId,
        // filter data start
        dateStart: dateStart,
        dateEnd: dateEnd,
        checkinStart: checkinStart,
        checkinEnd: checkinEnd,
        checkoutStart: checkoutStart,
        checkoutEnd: checkoutEnd,
        partnerId: partnerId,
        distributorSource: distributorSource,
        orderStatus: orderStatus,
        paymentMethod: paymentMethod,
        provider: provider,
        orderPReference: orderPReference,
        orderId: orderId,
        salesId: salesId,
        // filter data end
        orders: orders,
        users: users,
        distributors: distributors,
        comments: {
            order_comment:'',
            order_tem1: '',// 临时订单备注 remark
            order_tem2: '', // 临时订单 commend 财务或者销售对没有citcon没有收钱但是叮当的酒店后续处理的备注
            op_comment: '',
            finance_comment:'',
            fraud_comment:'',
            feedback_comment:''
        },
        showCommentIcon: false,
        addComment: "",
        addCommentOrderId: 0,
        addCommentOrderIndex: 0
    },
    mounted () {
      let selector = $('.chosen-select');
	    selector.chosen({width: "100%"});

	    let selectorA = $('.chosen-select-affiliate');
      selectorA.chosen({width: "100%"});
        // {{--table插件 一定要写到mounted里，否则容易失效--}}
        $('#dataTable').DataTable({
            // "pageLength": 25,
            'order': [[0, 'desc']],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv',  title:'财务对账记录 - ' + moment().format('YYYY-MM-DD HH:mm:ss')},
                {extend: 'excel', title: '财务对账记录 - ' + moment().format('YYYY-MM-DD HH:mm:ss')},
                {extend: 'pdf', title: '财务对账记录 - ' + moment().format('YYYY-MM-DD HH:mm:ss')},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ]
        });
        // format date picker format
        $('#daterange_1 .input-daterange, #daterange_2 .input-daterange').datepicker({
            format: 'yyyy-mm-dd',
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
        });
        // watch date change
        $('#startDate').datepicker().on(
          'changeDate', () => {
              this.dateStart = $('#startDate').val();
              this.dateEnd = $('#endDate').val();
          }
        );
        $('#endDate').datepicker().on(
          'changeDate', () => {
              this.dateEnd = $('#endDate').val();
          }
        );
        $('#checkinStart').datepicker().on(
          'changeDate', () => {
              this.checkinStart = $('#checkinStart').val();
              this.checkinEnd = $('#checkinEnd').val();
          }
        );
        $('#checkinEnd').datepicker().on(
          'changeDate', () => {
              this.checkinEnd = $('#checkinEnd').val()
            }
        );
        $('#checkoutStart').datepicker().on(
          'changeDate', () => {
              this.checkoutStart = $('#checkoutStart').val();
              this.checkoutEnd = $('#checkoutEnd').val();
          }
        );
        $('#checkoutEnd').datepicker().on(
          'changeDate', () => {
              this.checkoutEnd = $('#checkoutEnd').val()
          }
        );

	    selector.chosen().on(
          'change', (e) => {
              this.partnerId = $(e.target).val()
          });

	    selector.value = this.partnerId;
	    selector.trigger('chosen:updated');

	    selectorA.chosen().on(
          'change', (e) => {
              this.distributorSource = $(e.target).val()
          });

      selectorA.value = this.distributorSource;
      selectorA.trigger('chosen:updated');
    },
    methods: {
        formatDate(date){
            return moment(date)
        },
        clearAll(){
            //reload page
            $('.ibox-table').children('.ibox-content').addClass('sk-loading');
            $('#btn-clear-all').text('loading ...');
            window.location.href = '/admin/order/list';
        },
        cancelOrder(obj) {
            swal({
                title: "确认要取消这个订单吗？",
                text: "请仔细查看这个酒店的取消条款，超过酒店取消条款时间，酒店会收取相应的费用，请确认后再取消订单。",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "确认取消",
                confirmButtonColor: "#007FF8",
                confirmButtonText: "不取消",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (!isConfirm) {
                    FullscreenSpinner.create();
                    // return;
                    $.post("/order/cancel", {
                        '_token': window.usitrip.csrfToken,
                        'id': obj.orderReference,
                    }, function (data) {
                        if (data.hasOwnProperty('options')) {
                            var options = data.options;
                            onCancelComplete(options);
                        }

                        FullscreenSpinner.destroy();
                        if (data.success) {
                            swal({
                                title: "已取消",
                                text: data.message,
                                type: "success"
                            }, function () {
                                obj.status = 'CANCELLED'
                            });
                        }
                        else {
                            swal("取消失败", data.message, "error");
                        }
                    });
                }
            });
            },
        search(){
            $('#btn-search-filter').text('loading ...');
            $('#btn-search-filter').addClass('disabled');
            let newUrl = '/admin/order/list?creationStart=' + this.dateStart + '&creationEnd=' + this.dateEnd;
            if(this.checkinStart !== ""){
                newUrl += '&checkinStart='+ this.checkinStart
            }
            if(this.checkinEnd !== ""){
                newUrl += '&checkinEnd='+ this.checkinEnd
            }
            if(this.checkoutStart !== ""){
                newUrl += '&checkoutStart='+ this.checkoutStart
            }
            if(this.checkoutEnd !== ""){
                newUrl += '&checkoutEnd='+ this.checkoutEnd
            }
            if(this.partnerId !== 0){
                newUrl += '&partnerId='+ this.partnerId
            }
            if(this.distributorSource !== 0){
                newUrl += '&distributorSource='+ this.distributorSource
            }
            if(this.orderStatus !== ""){
                newUrl += '&orderStatus='+ this.orderStatus
            }
            if(this.orderPReference !== ""){
                newUrl += '&orderPReference='+ this.orderPReference
            }
            if(this.salesId !== ""){
                newUrl += '&salesId='+ this.salesId
            }
            if(this.orderId !== ""){
                newUrl += '&orderId='+ this.orderId
            }
          if(this.paymentMethod !== ""){
            newUrl += '&paymentMethod='+ this.paymentMethod
          }
          if(this.provider !== ""){
            newUrl += '&provider='+ this.provider
          }
            window.location.href = newUrl
        },
        showCommentModal(obj){
            $('#commentModalTitle').text('订单：' + obj.id);
            this.comments.order_comment = obj.remark == null ? '' :  obj.remark;
	          this.comments.order_tem1 = (obj.order_tem == null || obj.order_tem.remark == null) ? '' : obj.order_tem.remark;
	          this.comments.order_tem2 =  (obj.order_tem == null || obj.order_tem.commend == null) ? '' :  obj.order_tem.commend;
	          this.comments.op_comment =  obj.channel_remark == null ? '' : obj.channel_remark;
            this.comments.finance_comment = obj.finance_remark == null ? '' : obj.finance_remark;
            this.comments.fraud_comment = (obj.order_tem == null || obj.order_tem.suspicious == null) ? '' : obj.order_tem.suspicious.commend;
            this.comments.feedback_comment = obj.feedback == null ? '' : '<b>问题: </b>'+ obj.feedback.created_at +'<br/>' + obj.feedback.question  + '<br/><b>解决方案:</b><br/>' +  obj.feedback.solution;
            $('#commentModal').modal('show');
        },
        showAddCommentModal(obj, index){
            let self = this;
            self.addCommentOrderId = obj.id;
            self.addComment = '';
            $('#addCommentModalTitle').text('添加订单：' + obj.id + ' 财务备注');
            $('#accounting_remark').html(obj.finance_remark === null ? "" : obj.finance_remark);
            $('#addCommentModal').modal('show');
            self.addCommentOrderIndex = index
        },
        // 获取账号信息
         getPartnerInfo(id) {
             FullscreenSpinner.create();
            $('#partnerInfoModalTitle').html('分销商编号：'+id);
            $('#partnerInfoModalBody').html('<h3>Loading...</h3>');
            $('#partnerInfoModal').modal('show');
            $.ajax({
                type: 'GET',
                url: '/partner-info/show/'+id,
                id: id,
                dataType: 'json',
                success: function(data) {
                    if(data){
                        FullscreenSpinner.destroy();
                        var files = '上传文件';
                        if (data.partner.files !== '')
                            files = '查看+管理文件';
                        var html =
                            '<tr>' +
                            '   <td>用戶名</td>' +
                            '   <td>'+data.partner.name+'</td>' +
                            '</tr><tr>' +
                            '   <td>公司名</td>' +
                            '   <td>'+data.partner.company_name+'</td>' +
                            '</tr><tr>' +
                            '   <td>邮箱</td>' +
                            '   <td>'+data.partner.email+'</td>' +
                            '</tr><tr>' +
                            '   <td>电话</td>' +
                            '   <td>'+data.partner.company_phone+'</td>' +
                            '</tr><tr>' +
                            '   <td>创建时间</td>' +
                            '   <td>'+data.partner.created_at+'</td>' +
                            '</tr><tr>' +
                            '   <td>用戶地区(US可用信用卡)</td>' +
                            '   <td>'+data.partner.Country_Code+'</td>' +
                            '</tr><tr>' +
                            '   <td>佣金比例</td>' +
                            '   <td>'+data.partner.priceRatio+'</td>' +
                            '</tr><tr>' +
                            '   <td>付款方式</td>' +
                            '   <td>'+data.partner.paymentType+'</td>' +
                            '</tr><tr>' +
                            '   <td>持卡人</td>' +
                            '   <td>'+data.partner.cardholder+'</td>' +
                            '</tr>' +
                            '<tr>' +
                            '   <td>消费总额</td>'+
                            '   <td>'+data.partner.balance+'</td>' +
                            '</tr>' +
                            '<tr>' +
                            '   <td>财务审核到账/未到账</td>' +
                            '   <td>'+data.partner.postpay_paid+'/'+data.partner.postpay_unpaid+'</td>' +
                            '</tr>' +
                            '<tr>'+
                            '<td>上传证件</td>' +
                            '<td><a target="_blank" href="/profile/files/'+data.partner.id+'">'+files+'</a></td>' +
                            '</tr>' +
                            '<tr>' +
                            '   <td>备注</td>' +
                            '   <td>'+data.partner.remark+'</td>' +
                            '</tr>'
                        ;
                        $('#partnerInfoModalBody').html(html);
                    }
                }
            });
        },
        addNewNote(){
            FullscreenSpinner.create();
            // redefine user id
            let authUser = this.userId;
            switch(authUser){
                case '11173':
                    authUser = '1001';
                    break;
                case '10057':
                    authUser = '22';
                    break;
                case '10575':
                    authUser = '20';
                    break;
                case '10672':
                    authUser = '550';
                    break;
                case '11178':
                    authUser = '19';
                    break;
                default:
            }
            this.addComment = moment().format('YYYY-MM-DD HH:mm:ss') + ' ' + this.addComment + '  (' + authUser + ')';

            let self = this;
            $.post("/admin/order/comment/accounting", {
                '_token': window.usitrip.csrfToken,
                'id': self.addCommentOrderId,
                'finance_remark': self.addComment,
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    toastr.success("添加财务备注成功！",  "订单号：" + self.addCommentOrderId );
                    self.orders[self.addCommentOrderIndex].finance_remark = data.finance_remark;
                    $('#addCommentModal').modal('hide');
                } else {
                    swal("更新失败", data.message, "error");
                }
            });
        },

        checkOrder(obj) {
            FullscreenSpinner.create();
            $.post("{{url('/order/check')}}", {
                '_token': window.usitrip.csrfToken,
                'id': obj.id
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    location.reload();
                } else {
                    swal("已更新至最新状态", data.message, "success");
                }
            });
        },

        refund(obj) {
            swal({
                title: "确认退款吗？",
                text: "退款操作成功后不可撤销。",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "取消退款",
                closeOnConfirm: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确认退款",
            }, function () {
                FullscreenSpinner.create();
                $.post("/admin/refunds/refundConfirm'", {
                    '_token': window.usitrip.csrfToken,
                    'id': obj.id
                }, function (data) {
                    setTimeout(function () {
                        FullscreenSpinner.destroy();
                        if (data.success) {
                            swal({
                                title: "退款操作成功！",
                                type: "success"
                            }, function () {
                                //location.reload();
                            });
                        }
                        else {
                            swal("退款失败", data.message, "error");
                        }
                    }, 1000);

                });
            });
        },

        //switchs status， TODO: 以下四个methods可以合并
        // 是否存银行
        cbisBanked(obj){
            FullscreenSpinner.create();
            $.post("/admin/order/update/banked", {
                '_token': window.usitrip.csrfToken,
                'id': obj.id,
                'isBanked': obj.isBanked === 1 ? 0 : 1
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    toastr.success("是否存银行更新成功", "订单号：" + obj.id);
                    //location.reload();
                    obj.isBanked =  obj.isBanked === 1 ? 0 : 1;
                } else {
                    toastr.success("是否存银行更新失败",  "订单号：" + obj.id + data.message);
                    obj.isBanked =  obj.isBanked === 1 ? 1 : 0;
                }
            });
        },
        // 是否结算
        cbisPayed(obj){
            FullscreenSpinner.create();
            $.post("/admin/order/update/paid", {
                '_token': window.usitrip.csrfToken,
                'id': obj.id,
                'isPayed': obj.isPayed === 1 ? 0 : 1
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    toastr.success("是否结算更新成功", "订单号：" + obj.id);
                    //location.reload();
                    obj.isPayed =  obj.isPayed === 1 ? 0 : 1;
                } else {
                    toastr.success("是否结算更新失败",  "订单号：" + obj.id + data.message);
                    obj.isPayed =  obj.isPayed === 1 ? 1 : 0;
                }
            });

        },
        // 是否到账
        cbisChecked(obj){
            FullscreenSpinner.create();
            $.post("/admin/order/update/checked", {
                '_token': window.usitrip.csrfToken,
                'id': obj.id,
                'isChecked': obj.isChecked === 1 ? 0 : 1
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    toastr.success("是否到账更新成功", "订单号：" + obj.id);
                    //location.reload();
                    obj.isChecked =  obj.isChecked === 1 ? 0 : 1;
                } else {
                    toastr.success("是否到账更新失败",  "订单号：" + obj.id + data.message);
                    obj.isChecked =  obj.isChecked === 1 ? 1 : 0;
                }
            });
        },
        // 是否退款
        cbisReturned(obj){
            FullscreenSpinner.create();
            $.post("/admin/order/update/returned", {
                '_token': window.usitrip.csrfToken,
                'id': obj.id,
                'isReturned': obj.isReturned === 1 ? 0 : 1
            }, function (data) {
                FullscreenSpinner.destroy();
                if (data.success) {
                    toastr.success("是否退款更新成功", "订单号：" + obj.id)
                    //location.reload();
                    obj.isReturned =  obj.isReturned === 1 ? 0 : 1;
                } else {
                    toastr.success("是否退款更新失败",  "订单号：" + obj.id + data.message)
                    obj.isReturned =  obj.isReturned === 1 ? 1 : 0;
                }
            });
        },
      // 是否退款
      cbisAffiliated(obj){
        FullscreenSpinner.create();
        $.post("/admin/order/update/affiliated", {
          '_token': window.usitrip.csrfToken,
          'id': obj.id,
          'isAffiliated': obj.isAffiliated === 1 ? 0 : 1
        }, function (data) {
          FullscreenSpinner.destroy();
          if (data.success) {
            toastr.success("是否结算佣金更新成功", "订单号：" + obj.id)
            //location.reload();
            obj.isAffiliated =  obj.isAffiliated === 1 ? 0 : 1;
          } else {
            toastr.success("是否结算佣金更新失败",  "订单号：" + obj.id + data.message)
            obj.isAffiliated =  obj.isAffiliated === 1 ? 1 : 0;
          }
        });
      },
    },
});
