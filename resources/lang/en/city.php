<?php

return [
    // 搜索弹窗部分
    'home_close' => 'Close',
    'home_destination_hot' => 'Hot Destinations',
    'home_destination_america' => 'America',
    'home_destination_europe' => 'Europe',
    'home_destination_oceania' => 'Oceania',
    'home_destination_asia' => 'Asia',
    'home_destination_africa' => 'Africa',

    //地址自动填充框
    'scenic_city' => 'City',
    'scenic_hotel' => 'Hotel',
    'scenic_attraction' => 'Attraction',
    'scenic_airport_station' => 'Airport/Station',


    // 城市名称部分
    // 热门
    // 纽约
    // 洛杉矶
    // 芝加哥
    // 多伦多
    // 悉尼
    // 伦敦
    // 柏林
    // 罗马
    // 威尼斯
    // 马德里
    'city_geneva' => 'Geneva',
    // 拉斯维加斯
    // 阿姆斯特丹
    'city_luxembourg' => 'Luxembourg',

    //美洲
    'city_new_york' => 'New York',
    'city_los_angeles' => 'Los Angeles',
    'city_san_francisco' => 'San Francisco',
    'city_las_vegas' => 'Las Vegas',
    'city_boston' => 'Boston',
    'city_chicago' => 'Chicago',
    'city_washington' => 'Washington, D.C.',
    'city_honolulu' => 'Honolulu',
    'city_orlando' => 'Orlando',
    'city_miami' => 'Miami',
    'city_seattle' => 'Seattle',
    'city_vancouver' => 'Vancouver',
    'city_toronto' => 'Toronto',
    'city_victoria' => 'Victoria',
    'city_montreal' => 'Montreal',
    'city_cancun' => 'Cancun',
    // 欧洲
    'city_london' => 'London',
    'city_edinburgh' => 'Edinburgh',
    'city_paris' => 'Paris',
    'city_nice' => 'Nice',
    'city_lyon' => 'Lyon',
    'city_berlin' => 'Berlin',
    'city_munich' => 'Munich',
    'city_frankfurt' => 'Frankfurt',
    'city_rome' => 'Rome',
    'city_venice' => 'Venice',
    'city_florence' => 'Florence',
    'city_barcelona' => 'Barcelona',
    'city_madrid' => 'Madrid',
    'city_zurich' => 'Zurich',
    'city_amsterdam' => 'Amsterdam',
    'city_athens' => 'Athens',
    'city_milan' => 'Milan',

    // 大洋洲
    'city_sydney' => 'Sydney',
    'city_melbourne' => 'Melbourne',
    'city_gold_coast' => 'Gold Coast',
    'city_keynes' => 'Keynes',
    'city_brisbane' => 'Brisbane',
    'city_adelaide' => 'Adelaide',
    'city_perth' => 'Perth',
    'city_canberra' => 'Canberra',
    'city_great_barrier_reef'=> 'Great Barrier Reef',
    'city_oakland' => 'Oakland',
    'city_queens_town' => 'Queenstown',
    'city_wellington' => 'Wellington',
    'city_christchurch' => 'Christchurch',
    'city_phillip_island' => 'Phillip Island',
    'city_dublin' => 'Dublin',

    // 亚洲
    'city_singapore' => 'Singapore',
    'city_seoul' => 'Seoul',
    'city_jeju_island' => 'Jeju Island',
    'city_bali' => 'Bali',
    'city_bangkok' => 'Bangkok',
    'city_phuket' => 'Phuket',
    'city_chiang_mai' => 'Chiang Mai',
    'city_pattaya' => 'Pattaya',
    'city_sumei_island' => 'Sumei Island',
    'city_kuala_lumpur' => 'Kuala Lumpur',
    'city_langkawi_island' => 'Langkawi',
    'city_tokyo' => 'Tokyo',
    'city_osaka' => 'Osaka',
    'city_beijing' => 'Beijing',
    'city_hong_kong' => 'Hong Kong',

    // 非洲
    'city_cairo' => 'Cairo',
    'city_johannesburg' => 'Johannesburg',
    'city_cape_town' => 'Cape Town',
    'city_casablanca' => 'Casablanca',
    'city_nairobi' => 'Nairobi',
    'city_durban' => 'Durban',
    'city_tunisia' => 'Tunisia',
    'city_bizerte' => 'Bizerte',
    'city_luxor' => 'Luxor',
    'city_lusaka' => 'Lusaka',
    'city_mauritius' => 'Mauritius',
    'city_accra' => 'Accra',
    'city_harare' => 'Harare',
    'city_marrakesh' => 'Marrakesh',


];
