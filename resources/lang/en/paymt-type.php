<?php

return [
    'soc' => "Summary of Charges",
    'totalcharge' => "Total Due Today",
    'pymt' => "Payment Method",
    'cc' => "Credit/Debit Card",
    'paynow' => "Pay Now",
    'plzcomplete' => "Please complete the booking information in right format.",
    'failed' => "Something went wrong, please go to your My Booking page to check this booking result",

];