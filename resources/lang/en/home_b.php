<?php

return [
    'meta_description' => '117book - Your Trusted B2B Hotel Wholesaler',
    'title' => 'Your Trusted Hotel Wholesaler',
    // nav
    'home' => 'Home',
    'strengths' => 'Our Strengths',
    'how_to_book' => 'How To Book',
    'reviews' => 'Partner Reviews',
    'about_us' => 'About Us',
    'contact_us' => 'Contact Us',
    "input_email" => 'Email',
    "input_pwd" => 'Password',
    // 主体部分
    // banner
    'slogan' => 'Your Trusted B2B Hotel Wholesaler',
    'slogan_subtitle' => 'Instant booking confirmation, <br>24/7 multilingual customer service',
    'login' => 'Login',
    'sign_up' => 'Sign Up',
    'partner_login' => 'Partner Login',
    'group_order' => 'Group Hotel Request',
    'group_order_info' => '',
    'remember_me' => 'Remember me',
    'forgot_password' => 'Forgot password?',
    'hello' => 'HELLO',
    'my_account' => 'My Account',
    'sign_out' => 'Sign Out',

    // reviews
    'reviews_title' => 'WHAT OUR PARTNERS SAY',
    'reviews_subtitle' => 'To Hear the Voices from Our Valued Partners',

    'reviews_1_1' => '117book has very rich hotel resources. Its hotel coverage is wide, especially in North American market. Its hotel price is also fairly competitive, which has really solved our problems of the global hotel booking.',
    'reviews_1_2' => 'Ctrip',
    'reviews_1_3' => 'One of China\'s largest tourism brands',

    'reviews_2_1' => '117book hotel booking system is really convenient to use. The system responds quickly. It\'s very easy to search hotels and you can easily narrow down the results by hotel star ratings, locations and price.',
    'reviews_2_2' => 'Qunar',
    'reviews_2_3' => 'One of China\'s largest tourism brands',

    'reviews_3_1' => 'The 117book really helped our private tour business development. The hotel choosing process becomes very easy for us. The hotel coverage is really wide, as well as the hotel price is very competitive in the market.',
    'reviews_3_2' => 'Fliggy (Alitrip)',
    'reviews_3_3' => 'One of China\'s largest tourism brands',

    // strengths
    'strengths_title' => 'GLOBAL HOTELS COVERAGE',
    'strengths_subtitle' => '197 Countries   40,000+ Destinations   350,000+ Hotels',
    'strengths_1_1' => 'GREAT HOTELS DEALS',
    'strengths_1_2' => 'We provide 350,000 global hotels over 40,000 travel destinations',
    'strengths_2_1' => 'LOWEST NET RATES',
    'strengths_2_2' => 'We will help you to find the lowest hotel net rate in the industry.',
    'strengths_3_1' => 'SECURE PAYMENT',
    'strengths_3_2' => 'We provide secure, convenient, flexible and safe payment process',
    'strengths_4_1' => 'MULTI-LINGUAL SERVICE',
    'strengths_4_2' => 'We provide 24/7 Multi-lingual customer service to solve problems',
    // book
    'book_title' => 'OUR BOOKING PROCESS',
    'book_subtitle' => 'To Complete Hotel Booking with Only 4 Easy Steps',
    'book_1_1' => 'Step 1',
    'book_1_2' => 'Register an account for free',
    'book_1_3' => 'Click to register ->fill out the user name, email address and password -> fill out the company name and contact information -> the account application will be approved by 117book',
    'book_2_1' => 'Step 2',
    'book_2_2' => 'Login account',
    'book_2_3' => 'Enter the e-mail and password -> click to log in -> If you meet any problem during the login, please feel free to contact customer service for help',
    'book_3_1' => 'Step 3',
    'book_3_2' => 'Check hotel room availability',
    'book_3_3' => 'Select and enter a destination or keyword -> select the hotel check in and check out dates and the number of rooms -> click to search for the target hotel',
    'book_4_1' => 'Step 4',
    'book_4_2' => 'Booking and payment',
    'book_4_3' => 'First of all, please place the credit card information -> select the hotel and room type -> agree to the "Terms and conditions of the service" -> system will automatically deduct the hotel booking payment from credit card -> If you cancel the order, the hotel booking payment will be returned to your credit card',
    //ad banner
    'ad_banner_title' => 'Start your hotel booking now',
    'ad_banner_subtitle' => 'We will find the deals for you, best price guaranteed',
    'btn_check_now' => 'My Account',
    // about us
    'about_title' => 'ABOUT 117BOOK',
    'about_subtitle' => 'To Achieve Global Hotel B2B Distribution with Intelligent Platform',
    'about_content' => '117book is the B2B hotel booking platform of Unitedstars International, Ltd, which has 13+ years industry experience in tourism resources aggregation, hotel wholesales, API 
connectivity and customer experience optimization. We have 7000+ Direct Contracted U.S. Hotels. 117book is one of the most preferred global hotel booking platform in industry.<br>With our user-friendly booking system, 117book will provide secure and convenient payment process, free booking modification, e-voucher and invoice with your own logo, to our business partners.',
    // about us
    'contact_title' => 'CONTACT US',
    'contact_subtitle' => 'Feedbacks and Cooperation are Welcomed',
    'hotline' => 'Hotline',
    'country_us' => 'US',
    'country_cn' => 'CN',
    "contact_email" => 'Email',
];
