<?php

return [
    'title' => '404 error',
    'error_desc' => 'We couldn\'t find the page you are looking for...',
    'error_so_title' => 'You can',
    'error_so_1' => '1. You may have typed the address(URL) incorrectly.',
    'error_so_2' => '2. Please call our hotline: <a href="tel:1-626-898-7658">1-626-898-7658 (24/7)</a>.',
    'back_home' => 'home page'
];