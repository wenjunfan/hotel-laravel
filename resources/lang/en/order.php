<?php

return [
    //cfg-old-order
    'title' => "Order Information",
    'cinfo' => "Hotel Check-in Information",
    'cphone' => "Phone",
    'cemail' => "Email",
    'Room#' => "Rooms",
    'guest' => "Guest",
    'comment' => "The Fine Print",
    'invoice' => "INVOICE",
    'invoiceId' => "Invoice No.",
    'due' => "Due Date",
    'desc' => "Description",
    'bill' => "Bill to:",
    'cname' => "Customer Name",
    'amount' => "Amount",
    'otherc' => "Other Comments",
    'hcell' => "Hotel Phone",
    'specialrequest' => "Special requests",


];