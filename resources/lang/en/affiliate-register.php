<?php

return [
    'username' => 'Username',
    'username_is_required' => 'Username is required',
    'email' => 'Email',
    'password' => 'Password',
    'confirm_password' => 'Confirm password',
    'password_rules' => 'Contains 8+ characters with 2+ of the following: a-z, A-Z, 0-9, symbols.',
    'affiliate_user_login' => 'Affiliate User Login',
    'have_account' => 'Already have an account',
    'sign_up' => 'Sign Up',
    'sign_up_member' => 'Sign Up',
    'agree' => 'I have read and agree to the',
    's' => "'s",
    'term_of_use' => "Terms of Use",
    'and' => " and ",
    'private_policy' => "Private Policy",
    'human' => "Please confirm you are human",
    'username_error' => "Username is needed ",
    'email_error' => "Please enter valid Email",
    'password_error' => "Please enter the same confirm password",
    'input_error' => "All input is required",
    'must_agree' => "Please check our terms of use and private policy",
];
