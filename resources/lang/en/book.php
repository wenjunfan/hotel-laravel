<?php

return [
    'cancellation_policy' => 'cancellation policy',
    'coupon_title' => 'Apply a coupon code',
    'coupon_placeholder' => 'Enter a coupon code',
    'coupon_btn' => 'Apply',
    'coupon_applied_title' => 'Discount',
    'coupon_applied_desc' => 'Your coupon has been applied',
    'coupon_remove' => 'Remove',
    'coupon_error_msg' => 'We don’t recognize that coupon code',

    //cfg-old-book
    'cleanFee' => "Clean Fee",
    'coupon' => "Coupon",
    'bdetail' => "Booking Detail",
    'bInfo' => "Booker Detail",
    'bName' => "Full Name",
    'tax' => "Total Taxes",
    'roomNum' => "rooms",
    'bCell' => "Phone Number",
    'bEmail' => "Email Address",
    'bEmailr' => "Email (for receiving booking voucher, if not, check Junk mail)",
    'bEmailInfo' => "For receiving Voucher",
    'room' => "Room",
    'pguest' => " Primary Guest Name",
    'guest' => "Guest Name",
    'aguest' => "+ Add another guest",
    'adult' => "adult",
    'children' => "children",
    'fname' => "First Name",
    'lname' => "Last Name",
    'tip' => "Please enter the name of the person checking into each room. This primary guest must present a valid photo ID and credit\debit card at check-in.",
    'specialrequest' => "Any special requests ? ",
    'englishonly' => "Alphabets Only",
    'numberOnly' => "Please enter only number for valid phone",
    'request' => "Please note special requests are not guaranteed and may incur additional charges, but we will pass your request to the hotel and it will be subject to availability at check-in. If necessary, please contact the hotel directly. ",
    'requestoptional' => "Other special requests (optional) ",
    'addrequestoptional' => "+ Add your special requests (optional) ",
    'one' => "I'd like two beds",
    'two' => "I'd like one (large) bed",
    'three' => "I'd like a quiet room",
    'four' => "I'd like a room on ground floor",
    'five' => "I'd like a room on high floor",
    'six' => "I'd like a room with view",
    'contactinfo' => "Contact Information",
    'cname' => "Contact Name",
    'cphone' => "Contact Phone",
    'cemail' => "Contact Email",
    'notes' => "Notes/Remarks",
    'phone' => "Phone Number",
    'email' => "Email",
    'noteoption' => "Note for your own record (optional)",
    'soc' => "Summary of Charges",
    'roomcost' => "Room Cost",
    'rooms' => "Rooms",
    'nights' => "Nights  ",
    'totalcharge' => "Total Due Today",
    'totalchargeHotel' => "Total Due at Hotel",
    'resortFee' => "Resort Fee",
    'pymts' => "Select Payment Method",
    'pymt' => "Payment Method",
    'cc' => "Credit/Debit Card",
    'ali' => "Alipay",
    'we' => "Wechat Pay",
    'paynow' => "Pay Now",
    'warmtip' => "By clicking Pay Now, I have reviewed and confirmed all the booking information and ",
    'warmtiplink' => "the fine print on this page.",
    'warning' => "Please note that foreign currency payment will be processed at the current exchange rate.",
    'checkin' => "Check-in",
    'checkout' => "Check-out",
    'nit' => "- nights stay",
    'canc' => "Cancellation Policy",
    'non-ref' => "Non-refundable",
    'fineprint' => "The Fine Print ",
    'free-can' => "and before is free cancellation(at hotel local time)",
    'charge' => "(at hotel local time) and before cancel, hotel needs charge",
    'ltime' => " (at hotel local time)",
    'backtoOrders' => "Go to My Booking",
    'failed' => "Something went wrong, please go to your My Booking page to check this booking result",
    'network' => "Network Error，please go to My Bookings to check your voucher",
    'cfailed' => "Something went wrong, please contact customer services to check this booking result, if book failed, your refund is in processing, depends on your bank, refund will be issued 3-7 days after.",
    'cnetwork' => "You may have already booked successfully, but due to poor network conditions, please log in with your booking email to check your order results or contact customer service to view your order results.",
    'bookSuccessEmailLater' => "Thanks! Your hotel reservation is been processing, please contact customer representative for book result, if book success, we will send you a confirmation E-mail",
    'freeb' => "Free Cancellation before",
    'soldout' => "This room has been sold out. Please check other hotels or choose another room type",
    'timeout' => "Session timeout, please re-book",
    'limit' => "This order total has exceeded the maximum transaction amount of your account. Please contact our customer service directly.",
    'pchange' => "The price of this room has been changed. Please check the new price or other hotels or choose another room type",
    'plzcomplete' => "Please complete the booking information in right format.",
    'duplicate' => "You already have a same order #xxx. If you are placing a new order, please return to the hotel details page and search again before placing a new order.",
    'auth' => "Please review your payment information and try again. If you need further assistance, please contact us directly.",
    'refunded' => "Already refund",
    'placeOrder' => "Please wait while we are placing your order...",
    'paymentFailed' => "Your payment is failed",
    'paymentCheck' => "Please confirm your payment to continue the order",
    'confirm' => "Continue to order",
    'cancel' => "No",
    'confirmDu' => "It may be a duplicate order, want to continue?",
    'session_back_hotel' => "Session expires. Please choose room again from hotel detail page.",
    'alipay_back_here' => "In order to complete your order, please come back to this site after payment done, and we will generating voucher for you",

    //book-pc
    'hurry' => 'Don’t let this price get away! Never pay more for your hotels',
    'two_mins' => 'It only takes 2 minutes!',
    'question' => 'Questions? Call our 7/24 customer support: 1-626-898-7658',
    'step_one' => '&nbsp;&nbsp;&nbsp;&nbsp;Booking details',
    'step_two' => 'Payment details',
    'step_three' => 'Confirmed reservation',
    'good_choice' => 'Good choice – you picked the cheapest room! Don\'t let it get away.',
    'name' => 'Name',
    'phone_hint' => 'So the property can reach you',
    'email_hint' => 'Your confirmation email goes here',
    'remark' => 'Note/Remark',
    'remark_hint' => 'Note for your own record(optional)',
    'usa' => 'USA ',
    'tai_wan' => 'Taiwan CN ',
    'canada' => 'Canada ',
    'hong_kong' => 'Hong Kong ',
    'macau' => 'Macau ',
    'china' => 'China ',
    'check_in' => 'Check-in',
    'check_out' => 'Check-out',
    'samename' => 'Same main guest name detected for multiple rooms, booking might be declined, still processing?',
    'wait' => "Please wait patiently...",
    'pp_btn_ad' => "Cheap & Discount Hotel Deals",
    'pp_btn' => "Paypal",
    'clean_fee' => "Clean Fee",
    'networkErr' => "Network error. Please check other hotels or choose another room type",
    'exchange_rate' => " recommends that customer makes payments in United States dollar (USD). We can also accept multiple currencies; however, customer needs to be responsible for the loss due to the change of exchange rate. ",

    //moved from mobile
    'title' => 'Hotel Book',
    'locale' => 'en_US',
    'sun' => 'SUN',
    'mon' => 'MON',
    'tue' => 'TUE',
    'wed' => 'WED',
    'thu' => 'THU',
    'fri' => 'FRI',
    'sat' => 'SAT',
    'guestroom' => ' Room',
    'duration' => 'Duration',
    'night' => 'night(s)',
    'cancellationPolicy' => 'Cancellation policy',
    'localTime' => '(Hotel Local Time)',
    'nonRef' => 'Non-Refundable',
    'priceDetail' => 'Price Detail',
    'the' => '',
    'seven' => '7th',
    'eight' => '8th',
    'child' => ' Child',
    'ratePerRN' => 'Rate per Room Night',
    'totalTax' => 'Total Taxes',
    'dueToday' => 'Total Due Today',
    'dueHotel' => 'Due at Hotel',
    'resort' => '(Resort Fee)',
    'bookInfo' => 'Book Info',
    'goodChoice' => 'Good choice – you picked the cheapest room! Don’t let it get away.',
    'mainGuest' => 'Main Guest Name ',
    'specialRequest' => 'Any Special Request',
    'specialRequestNote' => 'Please note special requests are not guaranteed and may incur additional charges, but we will pass your request to the hotel and it will be subject to availability at check-in. If necessary, please contact the hotel directly. ',
    'srOne' => 'I\'d like two beds',
    'srTwo' => 'I\'d like one (large) bed',
    'srThree' => 'I\'d like a quiet room',
    'srFour' => 'I\'d like a room on ground floor ',
    'srFive' => 'I\'d like a room on high floor',
    'srSix' => 'I\'d like a room with view',
    'contactInfo' => 'Contact Information',
    'contactName' => 'Name',
    'contactPhone' => 'Phone',
    'contactEmail' => 'E-mail',
    'pmtMethod' => 'Payment Method',
    'wechat' => 'WechatPay',
    'alipay' => 'AliPay',
    'pmtInfoConfirm' => 'By clicking Pay Now, I have reviewed and confirmed all the booking information and the fine print on this page. Please note that foreign currency payment will be processed at the current exchange rate.',
    'payNow' => 'Pay Now',
    'finePrint' => 'The Fine Print ',
    'sessionExpire' => 'Session Expired， please re search again ',
    'confirmPaid' => 'Please confirm whether you have paid',
    'plzComplete' => 'Please fill the form completely',
    'bookFailed' => 'Something went wrong, please go to your My Booking page to check this booking result',
    'soldOut' => 'Sorry this room is sold out, please search again',
    'backToSearch' => 'Back to Search',
    'networkError' => 'Internet Error, please contact us for your booking result',
    'duplicateOrder' => 'Order duplicate, please check your booking from your order management from back office',
    'priceChange' => 'The price of this room has been changed. Please check the new price or other hotels or choose another room type',
    'priceLimit' => 'This order total has exceeded the maximum transaction amount of your account. Please contact our customer service directly.',
    'bookFailedRefund' => 'refunded',
    'shortSoldOut' => 'Sold Out',
    'shortPriceChange' => 'Price changed',
    'shortPriceLimited' => 'Price exceed limit',
    'shortDuplicate' => 'Duplicate order',
    'shortSessionExpired' => 'Session Expired',

    'shortBookFailed' => 'Book failed',
    'freeCancelBefore' => 'Free cancellation before ',
    'useQQ' => 'Please use QQ Browser',
    'cn' => 'China',
    'taiwan' => 'Tai Wan CN',
    'hongkong' => 'Hong Kong CN',
    'uk' => 'United Kingdom',
    'germany' => 'Germany',
    'france' => 'France',
    'other' => 'others',
    'yes' => 'Yes',
    'no' => 'No',
    'continueBooking' => 'Still booking？',
    'processingBooking' => 'Ok! We are processing your order now',
    'alipayComeBack' => 'In order to complete your order, please come back to this site after Alipay payment done, and we will generating voucher for you',
    'exchangeRateWaiver' => ' recommends that customer makes payments in United States dollar (USD). We can also accept multiple currencies; however, customer needs to be responsible for the loss due to the change of exchange rate. ',
    'alphabetOnly' => 'Alphabets Only',
    'pbError' => 'Please try other payment method, Paypal button has error: ',
];
