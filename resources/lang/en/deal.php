<?php

return [
    'search_destination' => 'Destination',
    'search_dates' => 'Dates',
    'search_rooms' => 'Rooms',
    'search_change' => 'Change searches',
    'search_cancel' => 'Cancel',
    'search_submit' => 'Search hotels',
    'search_checkin' => 'Check-in',
    'search_checkout' => 'Check-out',
    'search_room_count' => 'Room Number',
    'search_adult_count' => 'Adults/Room',
    'search_child_count' => 'Children/Room',
    'coupon_step1' => '1. Copy the coupon code',
    'coupon_step2' => '2. Paste coupon code on <br> the checkout page.',
    'coupon_step3' => '3. Discount applied. Enjoy <br> your hotel.',
    'popular_destination' => 'Popular Destinations',
    'more_hotels' => 'View more hotels',
    'policy_title' => 'Terms & Conditions',
    'policy_content' => 'Sale on participating properties and checkin date from <span class="text-danger">:start</span> to <span class="text-danger">:end</span> only. Offers vary by property. Rooms and prices are subject to availability. Prices do not include taxes and fees and are subject to change without notice. Additional restrictions and blackout dates may apply.',
    'book_now' => 'Book now',
];