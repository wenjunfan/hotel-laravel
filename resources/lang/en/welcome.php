<?php

return [
    'welcome' => 'Congratulations! You are now an affiliate member of Usitour.com. From now on, as long as your friend 
successfully booked the hotel through the link you shared, you will receive a 5% commission of the total 
price (excluding tax).',
    'code_below' => 'Your affiliate code is here',
    'link_below' => 'Your affiliate link is here',
    'title1' => 'Affiliate link use instruction',
    'text1' => 'The commission link consists of "website home link + your commission code”.
After your friend enters the official website through this link, all the orders will be classified.
In your settlement order, if you want to recommend a specific hotel, please let your friends pass first.
The above link will go to the homepage of search.',
    'title2' => 'Commission percentage',
    'text2' => '5% of total booking Price (excluding tax)',
    'title3' => 'Commission settlement instructions',
    'text3' => 'The commission earned can be accumulated in the personal account, only when the commission is accumulated to $50, You can choose your check or Paypal to withdraw your commission.',
    'title4' => 'Add commission settlement method',
    'text4' => 'Check or Paypal',
    'thanks' => 'Thanks for your registration, our hotel representative will email you the activation email and the instruction of how to use the earn money via our system. Please be patient.',
    'cashing_request_true' => 'success, our accountant will be in contact soon for your payment method',
    'cashing_request_false_amount' => 'Failed, your commission is lower than $50, please try again later.',
    'cashing_request_false' => 'Failed, please try again later.',
];