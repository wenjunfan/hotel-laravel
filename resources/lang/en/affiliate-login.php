<?php

return [

    'password' => 'Password',
    'email' => 'Email',
    'login' => 'Log In',
    'remember_me' => 'Remember me',
    'forgot_password' => 'Forgot password',
    'affiliate_user_login' => 'Affiliate User Login',
    'no_account' => 'No Account',
    'register_now' => 'Register Now',

];
