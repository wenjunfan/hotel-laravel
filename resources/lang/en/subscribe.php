<?php

return [

    // 订阅部分
    'subscribe_agreement' => 'you accept to receive deals and promotion emails from Usitrip, you can unsubscribe it at anytime.',
    'subscribe_sub'=> 'Subscribe',
    'subscribe_mem'=> 'Get',
    'subscribe_subscribe'=> 'Subscribe',
    'subscribe_to_see'=> ' to see Secret',
    'subscribe_deals'=> ' to see Secret',
    'subscribe_deals_mem'=> 'Exclusive Member Discounts',
    'subscribe_welcome'=> 'Welcome!',
    'subscribe_email'=> 'Enter Email',
    'subscribe_signup'=> 'Sign me Up',
    'subscribe_signup_mem'=> 'Get Now!',
    'subscribe_send_mem'=> 'Deals sent, check your email out!',
    'subscribe_return_user'=> 'Welcome Back, Exclusive Member!',
    'subscribe_send_mem_again'=> 'Thank you! You already subscribed!',
    'subscribe_welcome_mem'=> 'Welcome Back, Exclusive Member!',
    'subscribe_send'=> 'Deals sent, check your email out!',
    'subscribe_send_again'=> 'Thank you! You already subscribed!',
    'subscribe_close'=> 'Close',
    'subscribe_send_deals'=> 'Deals has been sent to your email!',
    'subscribe_send_deals_again'=> 'Thank you! You already subscribed!',
    'subscribe_head'=> 'Exclusive Deals on Global Hotels',
    'subscribe_get_now_one'=> 'Subscribe now for your Secret ',
    'subscribe_get_now_two'=> 'Discounts and get exclusive offers',
    'subscribe_get_deals_one'=> 'Save up to 50% with exclusive coupons',
    'subscribe_get_deals_two'=> 'by checking your inbox regularly',
    'subscribe_get_another'=> 'Get another ',
    'subscribe_off'=> ' off',
    'subscribe_sub_already'=> 'Thank you! You already subscribed!',
    'subscribe_sub_success'=> '',
    'subscribe_sub_register_success'=> '',

    //subscribe-cfg
    'sub'            => "Subscribe to get",
    'mem'            => "Get",
    'subscribe'      => "Subscribe",
    'deals'          => " 10% off coupon",
    'dealsmem'       => "Exclusive Member Discounts",
    'welcome'        => "Welcome!",
    'email'          => "Enter Email",
    'signup'         => "Subscribe",
    'signupmem'      => "Get Now!",
    'sendmem'        => "Deals sent, check your email out!",
    'returnUser'     => "Welcome Back, Exclusive Member!",
    'sendmemagain'   => "Thank you! You already subscribed!",
    'send'           => "Deals sent, check your email out!",
    'sendagain'      => "Thank you! You already subscribed!",
    'close'          => "Close",
    'senddeals'      => "Deals has been sent to your email!",
    'senddealsagain' => "Thank you! You already subscribed!",
    'head'           => "Exclusive Deals on Global Hotels",
    'getnow1'        => "Subscribe now for your Secret ",
    'getnow2'        => "Discounts and get exclusive offers",
    'getdeals1'      => "Save up to 50% with exclusive coupons",
    'getdeals2'      => "by checking your inbox regularly",
    'another'        => "Get another ",
    'off'            => " off",
    'sub01'          => "Thank you! You already subscribed!",
    'sub02'          => "",
    'sub03'          => "",
    'errorEmail'     => "Please enter right email address to subscribe",
];
