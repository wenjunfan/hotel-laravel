<?php

return [
//    group hotel request
    'group_title' => 'Create Your Group Hotel Request',
    'group_description' => 'Please tell us about your requests by filling out the form below. Be sure to review all the details before submitting, and our representative will be in touch soon.',
    'group_input_city' => 'Destination',
    'group_input_checkin' => 'Check-in',
    'group_input_checkout' => 'Check-out',
    'group_add_another' => 'Add another destination or dates',
    'group_add_close' => '- Close',
    'group_input_backup_city' => 'Back-up Destination',
    'group_input_group_code' => 'Group Code (Optional)',
    'group_input_rpn' => '# Rooms Per Night (9+)',
    'group_input_star' => 'Ideal Star Rating',
    'stars' => 'Stars',
    'group_input_budget' => 'Ideal Nightly Budget',
    'group_contact' => 'Where should we send quotes?',
    'group_input_name' => 'Full Name',
    'group_input_company' => 'Company Name (Optional)',
    'group_input_email' => 'Email',
    'group_input_phone' => 'Phone',
    'group_input_additional' => 'Additional Comments/Requests (Optional)',
    'modal_btn_submit' => 'Submit',
    'btn_submit_check' => 'Ready to submit? Please review before submitting your request.',
    'btn_on_submit' => 'We will provide the best quotes ASAP, thanks!',
    'please_check_input' => 'Please check your required filed carefully, and try again',
];
