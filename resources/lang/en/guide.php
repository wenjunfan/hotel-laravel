<?php

return [
    //cfg-old-guide
    'pageTitle' => "Help Center",
    'Title' => "Getting Started",
    'first' => "Step 1: Fill in your account information, including company name/address, contact person, phone number and the Affiliate representative you’d like.",
    'second' => "Step 2: After all the above information are completed, your Affiliate representative will take the initiative to contact you to sign up the contract.",
    'third' => "Step 3: You can get your unique url to start share and sale the hotel through our system after your account is activated.",
    'hotline' => "Hotline ",
    'hotlinea' => " Mr. Allan (CN service representative)",
    'hotlineb' => " Mr. Alex (US service representative)",
    'hotlinec' => " Mr. Jimmy (CN service representative)",
    'qa' => "Frequently Asked Questions",
    'q1' => "Why can’t I search and book a hotel?",
    'q2' => "How do I activate my account?",
    'q3' => "How can I search for a desirable hotel in a more efficient way?",
    'q4' => "How can I book one room with two guests and another room with three guests?",
    'q5' => "Hotel prices may be displayed in multiple currencies, such as EUR, CAD, etc. How do I pay for those currencies?",
    'q6' => "Is it safe if I use my credit/debit card to book hotels through your system?",
    'q7' => "What do I need to do before checking into the hotel I booked through your system?",
    'q8' => "How long does it take for the money refunded to my bank account if I cancel my booking before the free cancellation cutoff time?",
    'a1' => "Your account need to be activated before you can start searching and see the hotel price.",
    'a2' => "Please fill our your information in “My Account” section, and your professional service representative will take the initiative to contact you to complete the activation process",
    'a3' => "You need to enter your destination or hotel name you’d like, and then select the check-in/out dates, number of rooms and guests to start the search. You can also use the sorting and filtering function to narrow down the results.",
    'a4' => "This system can only book the room type with the same number of guests a time. In this case, you need to book twice. ",
    'a5' => "If you choose to pay with a credit card, all the other currencies will be converted automatically into US Dollars and you only need to pay in USD. If you choose to pay with WechatPay or Alipay Alipay, all the other currencies will be converted automatically into CNY, and you only need to pay in CNY.",
    'a6' => "We promise and guarantee that all the information you provide will be protected and secured.",
    'a7' => "Please print or download your hotel voucher in “My Bookings” section before check-in, and show your valid photo ID and the voucher at the hotel.",
    'a8' => "In free cancellation period, once you cancel the order, we immediately process the refund automatically. Depends on your bank, the refund processing cycle is 7-10 workdays.",

];