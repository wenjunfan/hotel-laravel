<?php

return [
    //cfg-old-master
    'home_hi' => "Welcome to ",
    'welcome' => "Welcome to use usitrip",
    'logout' => "Log out",
    'logout1' => "Log out",
    'return' => "Return",
    'menu' => "Menu",
    'navlogo' => "http://hotelstest.usitrip.com/img/business/117.png",
    'supper_admin' => "Supper Admin",
    'finacial_admin' => "Finacial Admin",
    'channel_admin' => "Channel Admin",
    'flag' => "/img/country/FlagEn.png",
    'dash_board' => "My Dashboard",
    'search' => "Search Hotels",
    'research' => "Change search",
    'hotel_order' => "My Hotel Bookings",
    'tour_order' => "My Tour Bookings",
    'bcard' => "Refund History (CC)",
    'alipay' => "Refund History (Alipay)",
    'account' => "My Account",
    'guide' => "User Guide",
    'header' => "Hotel",
    'schinese' => "简体中文",
    'tchinese' => "繁體中文",
    'english' => "English",
    'tours' => "Search Tours",
    'copyright' => "117book",

    'member' => "My Account",
    'my_order' => "My Orders",
    'order_look_up' => "Order Look Up",
    'shopping_cart' => "Shopping Cart",
    'total_amount' => "Total Amount",
    'check_out' => "Check Out",

    'USD' => "$ USD",
    'CNY' => "￥ CNY",
    'EUR' => "€ EUR",
    'GBP' => "£ GBP",
    'AUD' => "AUD AUD",
    'HKD' => "HKD HKD",
    'TWD' => "NT$ TWD",
    'CAD' => "CAD CAD",
    'NZD' => "NZD NZD",

];