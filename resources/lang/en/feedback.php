<?php

return [
    'title' => 'Please tell us what you think',
    'message' => 'Thank you for the feedback. Best regards here from :site!',
    'describe' => 'Thank you in advance for your opinion! This questionnaire consists of just 3 questions. We use your feedback to improve our service for you and other visitors.',
    'desc_mobile' => 'Thank you! This questionnaire consists of just 3 questions.',
    'question1' => '1. What do you think of our hotel prices?',
    'question2' => '2. What could Usitour.com improve according to you? (Multiple options)',
    'question3' => '3. Do you have other remarks or suggestions?',
    'submit' => 'Send',
];