<?php

return [
    'title' => 'Prepaid Voucher',
    'referenceNumber' => 'Reference Number',
    'bookStatus' => 'Booking Status',
    'cancelled' => 'Cancelled',
    'onRequest' => 'On Request',
    'confirmed' => 'Confirmed',
    'checkIn' => 'Check-in',
    'checkOut' => 'Check-out',
    'room' => 'Room(s)',
    'night' => 'Night(s)',
    'numOfNight' => 'Num of Nights',
    'boardType' => 'Board Type',
    'cancellationPolicy' => 'Cancellation Policy',
    'paidAmount' => 'Paid Amount',
    'adult' => ' Adult',
    'the' => '',
    'child' => ' Children',
    'guestRoom' => ' Room',
    'dueHotel' => 'Due at Hotel',
    'one' => '1st',
    'two' => '2nd',
    'three' => '3rd',
    'four' => '4th',
    'five' => '5th',
    'six' => '6th',
    'seven' => '7th',
    'eight' => '8th',
    'thankYou' => 'Thank You',
    'thankYouText' => 'Thank you for your booking. In conjunction with your voucher, the information below is designed to make sure your booking runs smoothly and you have a very enjoyable trip.',
    'yourVoucher' => 'Your Voucher',
    'yourVoucherText' => 'Present a copy of your Accommodation Voucher at the reception when you arrive.',
    'delayedArrival' => 'Delayed Arrival',
    'delayedArrivalText' => 'If you are unable to arrive at your booked hotel on time (before check-in time), please notify us as soon as possible using the contact numbers listed above so that we can advise the property. Please quote your booking reference/ID so that we can assist you as quickly as possible.',
    'pleaseNote' => 'Please Note',
    'pleaseNoteText' => 'No show will be charged in full. If you do wish to cancel a non-Refundable hotel services, you can contact us to cancel the service with no refund made.',
    'chargeFee' => 'Service With Fee',
    'extraChargeText' => 'Please note, if you want to amend your main guest name before check-in via us, we would charge $25 per order as service fee, or you can contact Hotel directly for your request.',
    'howToCancel' => 'How to cancel my hotel reservation?',
    'howToCancelText' => 'Please sign in your Usitrip account first, then go to “My Orders”, click on “Hotel Orders” under “Order History” to cancel hotel reservations for refund. For the non-refundable bookings are no cancellations or changes possible.',
    'remarks' => 'Remarks',
    'noteFrontDesk' => 'Notes to Front Desk',
    'emergencyContact' => 'Emergency contact',
    'voucherInformation' => 'Voucher information',
    'bookingId' => 'Booking ID',
    'bookingDate' => 'Booking Date',
    'note1' => 'This is a prepaid reservation, DO NOT charge the guest(s) except for extras consumed at the hotel. Payment for the booking was made via unitedStars Inc\'s credit card or the booking is confirmed and paid through a 3rd party supplier. Please check for the reservation by guest name, arrival date or booking reference.\n',
    'note2' => 'Please contact one of our offices should you have any questions/queries regarding this reservation. ',
    'wish' => 'We wish you a very enjoyable trip and thank you again for booking with us.',
];
