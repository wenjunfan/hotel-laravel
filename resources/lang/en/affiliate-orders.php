<?php

return [
    'cashing_request_true' => 'success, our accountant will be in contact soon for your payment method',
    'cashing_request_false_amount' => 'Failed, your commission is lower than $50, please try again later.',
    'cashing_request_false' => 'Failed, please try again later.',
    'cashing_not_accrued' => ' Potential Amount if all check-out',
    'redirect_pc' => '  Sorry, please use PC to view your order information',
];