<?php

return [
    // home page
    'title_home' => "Hotels: Cheap & Discount Hotel Deals, Hotel Booking | :domain",
    'description_home' => ":domain's Hotel provides 350,000 hotels worldwide with great value. Book now for instant savings with our 100% price guarantee policy.'",
    'keywords_home' => "hotels, motel, cheap hotels, booking hotel",

    // list page
    'title_list' => "The 20 Best Cheap Hotels in :name - :domain",
    'description_list' => ":domain's Hotel provides the best hotels deals in :name. Book now for instant savings in :name with our 100% price guarantee policy.",
    'keywords_list' => ":name Hotel,:name Cheap Hotel,:name Hotel Reservation.",

    // hotel page
    'title_hotel' => ":name 預訂,:name 特價,:name折扣",
    'description_hotel' => ":name's Hotel provides the best room deals in :name.Book now for instant savings in :name with our 100% price guarantee policy.",
    'keywords_hotel' => ":name reserve,:name special deals,:name discount",

];