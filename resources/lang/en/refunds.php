<?php

return [
    //cfg-old-refunds
    'title' => "Credit/Debit Card Refund History",
    'sd' => "From",
    'ed' => "To",
    'orders' => "Routing ID",
    'cn' => "Card Number",
    'status' => "Refund Status",
    'transactions' => "Transaction ID",
    'refunds' => "Refund ID",
    'rn' => "Reference ID",
    'cr' => "Refund Amount",
    'ar' => "Total Refund",
    'em' => "Errors",
    'refundst' => "Refund Date",
    'action' => "Invoice",
    's' => "Success",
    'f' => "Failed",
    'YMD' => "YYYY-MM-DD",
    'Search' => "Search Hotels",
    'BookingID' => "Booking ID",

];