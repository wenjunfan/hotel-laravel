<?php

return [
    'title' => "Alipay Refund History",
    'handling' => "Handling Fee",
    'arefund' => "Actual Refund",
    'refunds' => "Refund Status",
    'sd' => "From",
    'YMD' => "YYYY-MM-DD",
    'ed' => "To",
    'Search' => "Search",
    'BookingID' => "Booking ID",
    'orders' => "Routing ID",
    'ar' => "Total Refund",
    'status' => "Refund Status",
    'refundst' => "Refund Date",
    'Refunded' => "Refunded",
    'Processing' => "Processing",

];