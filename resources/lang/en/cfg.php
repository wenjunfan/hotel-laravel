<?php
return [
    //RecHotels
    'RecHotels.dd' => "Stratosphere Casino, Hotel & Tower",
    'RecHotels.v1' => "The Venetian Resort Hotel Casino",
    'RecHotels.v2' => "Luxor Hotel and Casino",
    'RecHotels.v3' => "MGM Grand",
    'RecHotels.v4' => "Monte Carlo Resort and Casino",
    'RecHotels.v5' => "Circus Circus Hotel and Casino",
    'RecHotels.v6' => "Excalibur Hotel Casino",
    'RecHotels.l1' => "Holiday Inn Los Angeles International Airport",
    'RecHotels.l2' => "Hilton Anaheim",
    'RecHotels.l3' => "Millennium Biltmore Hotel Los Angeles",
    'RecHotels.l4' => "Radisson Suites Hotel Buena Park",
    'RecHotels.l5' => "Clarion Hotel Anaheim Resort",
    'RecHotels.l6' => "Hilton Los Angeles Airport",
    'RecHotels.s1' => "Sofitel San Francisco Bay",
    'RecHotels.s2' => "Comfort Inn and Suites San Francisco Airport North",
    'RecHotels.s3' => "Red Roof Inn Plus San Francisco Airport",
    'RecHotels.s4' => "Hyatt Regency San Francisco Airport",
    'RecHotels.s5' => "Days Inn San Francisco International Airport West",
    'RecHotels.s6' => "Sheraton Fisherman's Wharf",
    'RecHotels.n1' => "Hyatt Reg Jersey City",
    'RecHotels.n2' => "Westin Jersey City Newport",
    'RecHotels.n3' => "Yotel New York at Times Square",
    'RecHotels.n4' => "Hyatt Place Flushing/Laguardia",
    'RecHotels.n5' => "Holiday Inn Manhattan Financial District",
    'RecHotels.n6' => "Wellington Hotel",
    'RecHotels.h1' => "Holiday Inn Express Honolulu ",
    'RecHotels.h2' => "Hilton Hawaiian Village Waikiki Beach Resort",
    'RecHotels.h3' => "Hawaii Prince Hotel Waikiki",
    'RecHotels.h4' => "Hyatt Regency Waikiki Beach Resort And Spa",
    'RecHotels.h5' => "The Kahala Hotel & Resort",
    'RecHotels.h6' => "The Ritz-Carlton Residences",
    'RecHotels.m1' => "Grand Beach Hotel Surfside",
    'RecHotels.m2' => "Hyatt Place Miami Airport West - Doral",
    'RecHotels.m3' => "Courtyard by Marriott Miami Airport West",
    'RecHotels.m4' => "Hilton Garden Inn Miami Airport West",
    'RecHotels.m5' => "Holiday Inn Express & Suites Miami-Hialeah",
    'RecHotels.m6' => "Rodeway Inn Miami",

    //alipay
    'alipay.title' => "Alipay Refund History",
    'alipay.handling' => "Handling Fee",
    'alipay.arefund' => "Actual Refund",
    'alipay.refunds' => "Refund Status",

    'co.waiting_order_status' => "We are waiting to confirm your order status...",
    'co.warning' => "Please click ok to continue order and wait patiently for voucher if payment status is success.",
    'ep.use_previous_card' => "Please use previous card to pay",
    'cancel.alipaycharge' => " 1.We will charge a 3.5% handling fee if you cancel this order which was paid with Alipay. \n2.",
    'pm.wait' => "Please wait patiently...",
];