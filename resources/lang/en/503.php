<?php

return [
    'title' => '503 Be Right Back',
    'server_down' => 'The server encountered a temporary error and could not complete your request.',
    'try_later' => 'Please try again 10 minutes later.',
];