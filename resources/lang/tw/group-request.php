<?php

return [
//    group hotel request
    'group_title' => '創建您的團體飯店需求',
    'group_description' => '請通過填寫以下表格來告訴我們您的需求，請務必在提交之前檢查所有詳細信息，我們的代表將很快與您聯系。',
    'group_input_city' => '目的地城市',
    'group_input_checkin' => '入住日期',
    'group_input_checkout' => '退房日期',
    'group_add_another' => '添加另一備選目的地或者其他日期',
    'group_add_close' => '- 收起',
    'group_input_backup_city' => '備選目的地城市',
    'group_input_group_code' => '團體編號 (選填)',
    'group_input_rpn' => '每晚客房數量 (9+)',
    'group_input_star' => '理想飯店星級',
    'stars' => '星級',
    'group_input_budget' => '理想的每晚預算',
    'group_contact' => '我們應將報價發送至哪裏?',
    'group_input_name' => '聯系人姓名',
    'group_input_company' => '公司名稱 (選填)',
    'group_input_email' => '電子郵件',
    'group_input_phone' => '聯系電話',
    'group_input_additional' => '其他備註或請求 (選填)',
    'modal_btn_submit' => '提交',
    'btn_submit_check' => '請在提交前檢查上述申請信息的準確與完整性',
    'btn_on_submit' => '提交成功，我們會盡快聯系您並提供最優的飯店報價',
    'please_check_input' => '請仔細檢查必填選項，謝謝',
];
