<?php

return [
    'search_destination' => '目的地',
    'search_dates' => '日期',
    'search_rooms' => '客房',
    'search_change' => '更改搜索',
    'search_cancel' => '取消',
    'search_submit' => '搜索飯店',
    'search_checkin' => '入住時間',
    'search_checkout' => '退房時間',
    'search_room_count' => '房間數',
    'search_adult_count' => '成人/房',
    'search_child_count' => '兒童/房',
    'coupon_step1' => '1. 復制優惠碼',
    'coupon_step2' => '2. 在付款頁面粘貼<br>飯店優惠碼',
    'coupon_step3' => '3. 打折成功，付款完成<br>享受獨家優惠。',
    'popular_destination' => '熱門目的地',
    'more_hotels' => '更多飯店',
    'policy_title' => '條款和條件',
    'policy_content' => '此促銷頁面提供的折扣優惠僅適用於本頁面飯店，並且僅對入住開始時間<span class="text-danger">:start</span>到<span class="text-danger">:end</span>有效，所有房價需視具體供應情況而定。顯示的房價不包含稅費和其它小費，走四方飯店網擁有最終解釋權。',
    'book_now' => '預定',
];