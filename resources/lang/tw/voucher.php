<?php

return [
    'title' => '飯店預付憑證',
    'referenceNumber' => '憑證編碼',
    'bookStatus' => '預訂狀態',
    'cancelled' => '已取消',
    'onRequest' => '處理中',
    'confirmed' => '已確認',
    'checkIn' => '入住',
    'checkOut' => '退房',
    'room' => '間房',
    'night' => '晚',
    'numOfNight' => '入住晚數',
    'boardType' => '早餐類型',
    'cancellationPolicy' => '取消政策',
    'paidAmount' => '已付款',
    'adult' => '位成人',
    'the' => '第',
    'child' => '位兒童',
    'guestRoom' => '間客房',
    'dueHotel' => '到飯店付款（度假費）',
    'one' => '一',
    'two' => '二',
    'three' => '三',
    'four' => '四',
    'five' => '五',
    'six' => '六',
    'seven' => '七',
    'eight' => '八',
    'thankYou' => '感謝您',
    'thankYouText' => '感謝您的預訂。結合您的預付憑證，以下信息旨在確保您的入住順利，並且祝願您旅行愉快。',
    'yourVoucher' => '您的飯店憑證',
    'yourVoucherText' => '在辦理入住手續時，給飯店接待處出示一份英文版的預訂憑證副本能幫助您順利入住',
    'delayedArrival' => '延遲辦理住宿手續',
    'delayedArrivalText' => '如果您無法按時抵達您預訂的飯店（飯店規定最晚入住時間），請盡快通過上面列出的聯系電話通知我們，以便我們和飯店溝通。請引用您憑證下方的飯店預訂號，以便我們盡快為您提供幫助',
    'pleaseNote' => '請註意',
    'pleaseNoteText' => '請註意，如果您沒有在入住時間入住飯店，飯店依然將收取全額。如果您確實希望取消不可退款的飯店服務，可以聯系我們取消該飯店服務，但無法保證退款，謝謝',
    'chargeFee' => '收費項目',
    'extraChargeText' => '請註意，如果您入住前希望由走四方申請更改入住人姓名，走四方將收取$25的服務費，或者您也可以嘗試自己致電飯店更改',
    'howToCancel' => '如何取消我的飯店訂單？',
    'howToCancelText' => '請登錄您的走四方賬號，在“我的走四方”下拉菜單中點擊“我的訂單”後，從“訂單管理”中點擊“飯店訂單”，選取需要取消的訂單後方可完成取消流程。此操作不適用於不可取消的飯店訂單。',
    'remarks' => '預訂須知',
    'noteFrontDesk' => '飯店前台須知',
    'emergencyContact' => '緊急聯系人',
    'voucherInformation' => '預訂憑證須知',
    'bookingId' => '預訂號',
    'bookingDate' => '預訂時間',
    'note1' => '這是一個預付費的預訂，除了在飯店消費的額外費用外，請不要向客人收費。該預訂通過unitedStars Inc的信用卡已經支付預訂費用，或通過第三方供應商確認預訂。請通過客人的姓名，抵達日期或預訂參考查詢預訂情況。',
    'note2' => ' 這是預付款如果您對此預訂有任何問題或疑問，請按照預付憑證的聯系電話聯系我們的客服人員。',
    'wish' => '我們希望您的旅途入住愉快，感謝您使用我們的飯店預定系統',
];
