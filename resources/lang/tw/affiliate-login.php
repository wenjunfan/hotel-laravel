<?php

return [

    'password' => '密碼',
    'email' => '電子郵箱',
    'login' => '登入',
    'remember_me' => '記住我',
    'forgot_password' => '忘記密碼',
    'affiliate_user_login' => '分銷用戶登入',
    'no_account' => '沒有賬號',
    'register_now' => '馬上註冊',
];
