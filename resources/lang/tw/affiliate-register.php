<?php

return [
    'username' => '用戶名',
    'username_is_required' => '用戶名是必需的',
    'email' => '電子郵箱',
    'password' => '密碼',
    'confirm_password' => '確認密碼',
    'password_rules' => '至少8位，區分大小寫，字母、數字、特殊符號中的兩種或兩種以上.',
    'affiliate_user_login' => '分銷用戶登入',
    'have_account' => '已經有賬號',
    'sign_up' => '註冊',
    'sign_up_member' => '註冊人人賺分銷會員',
    'agree' => '我已經閱讀並同意',
    's' => "的",
    'term_of_use' => "使用條款",
    'and' => "以及",
    'private_policy' => "隱私政策",
    'human' => "請確認您不是機器人",
    'username_error' => "請填寫用戶名",
    'email_error' => "請檢查您的郵箱地址",
    'password_error' => "確認密碼不相符",
    'input_error' => "請仔細檢查所有的輸入框",
    'must_agree' => "請同意並閱讀使用條款以及隱私政策",
];
