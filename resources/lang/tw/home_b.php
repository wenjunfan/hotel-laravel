<?php

return [
    'meta_description' => '要趣訂飯店B2B預訂平台',
    'title' => '要趣訂飯店B2B預訂平台',
    // nav
    'home' => '首頁',
    'strengths' => '我們的優勢',
    'how_to_book' => '訂購流程',
    'reviews' => '客戶評價',
    'about_us' => '關於我們',
    'contact_us' => '聯系我們',
    "input_email" => '電子郵箱',
    "input_pwd" => '密碼',
    // 主體部分
    // banner
    'slogan' => '您值得信賴的B2B飯店批發商',
    'slogan_subtitle' => '實時確認 高入住率 中英雙語24/7服務',
    'login' => '登錄',
    'sign_up' => '免費註冊',
    'partner_login' => '會員登錄',
    'group_order' => '團體預定',
    'group_order_info' => '（免註冊）',
    'remember_me' => '記住密碼',
    'forgot_password' => '忘記密碼?',
    'hello' => '您好',
    'my_account' => '我的賬戶',
    'sign_out' => '退出登錄',

    // review
    'reviews_title' => '我們的客戶這樣評價',
    'reviews_subtitle' => '聽聽從我們的客戶那裏得到的反饋 了解背後的聲音',

    'reviews_1_1' => '要趣訂飯店預訂平台上面的飯店資源非常的豐富，而且資源分布的區域也比較廣泛，尤其以北美地區飯店的價格最具競爭優勢，為我們的同事很好地解決了預訂飯店這個難題。',
    'reviews_1_2' => '攜程旅遊',
    'reviews_1_3' => '中國最大的旅遊品牌之一',

    'reviews_2_1' => '要趣訂飯店預訂系統在使用的時候非常便利，響應速度相當快，飯店的顯示也很方便查找，可以按照星級、商圈及價格進行搜索，極大地方便了我們飯店預訂同事的操作和查找，選擇飯店。',
    'reviews_2_2' => '去哪兒',
    'reviews_2_3' => '中國最大的旅遊品牌之一',

    'reviews_3_1' => '自從使用了要趣訂飯店預訂系統，我們私家包團事業部的同事操作起團來，選擇給顧客訂購飯店時顯得更加遊刃有余了。平台的飯店資源分布很廣，而且價格普遍在市場中具有優勢。',
    'reviews_3_2' => '飛豬（阿裏旅行）',
    'reviews_3_3' => '中國最大的旅遊品牌之一',
    // strengths
    'strengths_title' => '我們的飯店遍布全球',
    'strengths_subtitle' => '197個國家 40000+目的地 350000+飯店',
    'strengths_1_1' => '海量的飯店資源',
    'strengths_1_2' => '覆蓋全球五大洲<br>超過10萬座城市，<br>提供超過35萬家飯店資源',
    'strengths_2_1' => '行業最低的價格',
    'strengths_2_2' => '自動實時加載飯店價格，<br>一鍵查詢快速找到<br>同行業的最低價',
    'strengths_3_1' => '安全方便的結算',
    'strengths_3_2' => '專業同行業結算體系，<br>保證便捷、靈活、安全的<br>付款結算模式',
    'strengths_4_1' => '專業保障的客服',
    'strengths_4_2' => '7X24小時專業客戶服務保障，<br>為您迅速處理客人提出的<br>各種服務要求',
    // book
    'book_title' => '我們的訂購流程',
    'book_subtitle' => '四步輕松完成飯店采購 操作輕便快捷無誤',
    'book_1_1' => '步驟1',
    'book_1_2' => '免費註冊賬號',
    'book_1_3' => '點擊免費註冊 -> 填寫用戶名、電子郵箱及密碼即可 -> 填寫公司名稱和聯系方式 -> 工作人員審核通過即可',
    'book_2_1' => '步驟2',
    'book_2_2' => '登錄賬號',
    'book_2_3' => '輸入電子郵箱及密碼 -> 點擊登錄即可 -> 如果登錄遇到困難，請聯系客服幫助',
    'book_3_1' => '步驟3',
    'book_3_2' => '查詢飯店空房',
    'book_3_3' => '選擇或輸入目的地或關鍵字 -> 選擇入住日期和房間數量 -> 點擊查詢並找到目標飯店',
    'book_4_1' => '步驟4',
    'book_4_2' => '預訂並支付',
    'book_4_3' => '首先配置好支付的信用卡信息 -> 選擇需要預訂的飯店及房型 -> 同意要趣訂《服務條款》並提交 -> 系統自動扣取飯店費用 -> 取消訂單，會把相關費用退還到您設置的信用卡上',
    //ad banner
    'ad_banner_title' => '還等什麽？現在即可預訂海量飯店',
    'ad_banner_subtitle' => '您只需要登錄，剩下的比價交給我們來完成',
    'btn_check_now' => '立即查看',
    // about us
    'about_title' => '我們所專註的事情',
    'about_subtitle' => '用智能平台突破全球飯店B2B分銷難題',
    'about_content' => '117book要趣訂”是美國聯星旅遊集團（Unitedstars）旗下B2B平台。在旅遊資源整合、飯店分銷、系統對接、客戶體驗優化等方面有十多年的研發和運營經驗，是旅遊業界同行采購預訂國際飯店的首選平台。<br>目前，117book要趣訂已擁有35萬+國際飯店覆蓋北美、歐洲、澳新、日韓以及東南亞等地區200+國家，4萬+境外參團產品。並且將市場上備受熱捧的國際飯店以及目的地當地參團及玩樂產品等通過技術以及分銷平台，以底價實現網上全流程操作，達到即時確認，動態打包，提升效率。',
    // about us
    'contact_title' => '聯系我們',
    'contact_subtitle' => '歡迎各同行的反饋和意見 咨詢與合作',
    'hotline' => '咨詢熱線',
    'country_us' => '美',
    'country_cn' => '中',
    "contact_email" => '聯系郵箱',
];
