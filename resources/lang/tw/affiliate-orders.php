<?php

return [
    'cashing_request_true' => '我們已經收到您的請求，財務稍後會聯系您兌現方式',
    'cashing_request_false' => '出錯啦，請稍後再試。謝謝！',
    'cashing_request_false_amount' => '出錯啦，您還沒有達到$50的最低傭金兌現標準！',
    'cashing_not_accrued' => ' 所有訂單退房後的傭金',
    'redirect_pc' => ' 很抱歉，請使用電腦去查閱您的訂單詳情，我們暫時不支持手機查看訂單',
];