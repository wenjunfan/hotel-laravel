<?php

return [
    'title' => '503 馬上回來',
    'server_down' => '服務器暫時不可用，請刷新重試。',
    'try_later' => '如果多次刷新仍未恢復正常，請在10分鐘後再次嘗試。',
];