<?php

return [
    'title' => '404錯誤',
    'error_desc' => '十分的抱歉！找不到您要找的頁面.....',
    'error_so_title' => '您可以',
    'error_so_1' => '1.請檢查輸入的網址是否正確',
    'error_so_2' => '2.致電走四方旅遊網熱線：<a href="tel:25-6668-0241">25-6668-0241</a>。',
    'back_home' => '返回首頁'
];