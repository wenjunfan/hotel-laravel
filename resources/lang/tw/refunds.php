<?php

return [
    //cfg-old-refunds
    'title' => "銀行卡退款記錄",
    'sd' => "起始日期",
    'ed' => "結束日期",
    'orders' => "訂單流水",
    'cn' => "卡號",
    'status' => "退款狀態",
    'transactions' => "交易流水",
    'refunds' => "退款流水",
    'rn' => "關聯號",
    'cr' => "本次退款",
    'ar' => "共退款",
    'em' => "錯誤信息",
    'refundst' => "退款時間",
    'action' => "發票",
    's' => "成功",
    'f' => "失敗",
    'YMD' => "‌年-月-日",
    'Search' => "飯店搜索",
    'BookingID' => "‌訂單號",

];