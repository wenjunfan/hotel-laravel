<?php

return [
	'cn' => "卡號",
	'fname' => "‌名（英文/拼音）",
	'lname' => "‌姓（英文/拼音）",
	'USA' => "美國",
	'failed' => "出錯了，請到您的訂單管理頁查詢訂購結果",
	'title' => "信用卡在線付款",
	'cardInfo' => "信用卡信息",
	'addrInfo' => "詳細地址",
	'cn' => "銀行卡卡號",
	'ct' => "信用卡類別",
	'ed' => "過期日期",
	'jan' => "一月",
	'feb' => "二月",
	'mar' => "三月",
	'apr' => "四月",
	'may' => "五月",
	'jun' => "六月",
	'jul' => "七月",
	'aug' => "八月",
	'sep' => "九月",
	'oct' => "十月",
	'nov' => "十一月",
	'dec' => "十二月",
	'month' => "",
	'month1' => "月",
	'year' => "",
	'year1' => "年",
	'name' => "姓名",
	'cvc' => "安全碼",
	'add' => "地址",
	'add2' => "地址 2",
	'st' => "街道地址",
	'apt' => "單元或公寓號",
	'city' => "城市",
	'county' => "州/省",
	'cityname' => "城市名",
	'countyname' => "州或省名",
	'zip' => "郵編",
	'country' => "國家",
	'pmt' => "確認付款",
	'sa' => "請填寫完整的銀行卡信息",
	'phoneValid' => "手機號碼驗證",
	'sendCode' => "發送驗證碼",
	'secure' => "最後一步，請上傳您的有效證件和信用卡正面照",
	'thanksChoosingUs' => "感謝您的預訂！您的訂單正在處理中。",
	'upload' => "安全上傳",
	'pickImg' => "請根據上傳照片格式選擇一張您的照片上傳!",
	'demo' => "示例圖片",
	'secured' => "安全保障",
	'ssl' => "SSL 安全認證",
	'read' => "我已經閱讀此次",
	'ccAuth' => "銀行卡付款授權書",
	'confirmTrans' => ",並授權該筆支付.",
	'print' => "打印",
	'close' => "關閉",
	'safe' => "感謝您的上傳，我們將盡快處理您的訂單！",
	'phonerecheck' => "請再次檢查您的手機，驗證碼錯誤",
	'phonenotvalid' => "請驗證您的手機號碼以保障您的銀行卡安全！",
	'filenotvalid' => "您的訂單正在審核中",
	'CodetopPhone' => "您的手機驗證碼已發送！",
	'cardInDan' => "為了保障您的銀行卡使用安全",
	'pleaseUploadFile' => "請進一步提交您的銀行卡信息驗證",
	'otherCountry' => "其他",
	'leftToPay' => "待付款",
	'enterVrfCode' => "請輸入5位驗證碼",
	'vrfCode' => "確認",
	'tooManyTimes' => "您已嘗試多次手機驗證失敗，由於飯店價格浮動很快，請重新搜索",
	'declineok' => "訂單拒絕成功，返回搜索",
	'declinefailed' => "拒絕失敗，請刷新再試一次",
	'requestDecline' => "取消，申請退款 ",
	'changecard' => "您的卡被檢測到有被盜的風險，請換一張卡再試,謝謝！",
	'cardholder' => "持卡人",
	'ccsecure' => "我們采用安全傳輸",
	'pisecure' => "我們保護您的個人信息，並且不會收取任何銀行卡手續費！",
	'usitrip-hotel-reserve' => "飯店",
	'file_upload_explain' => "為了確認訂購的真實性，我們需要您提供訂購所使用的信用卡正面以及信用卡持卡人的有效證件(駕照，護照或學生證)。",
	'file_upload_explain_2' => "所有的信息僅用於核實確認，絕對保密。",
	'file_upload_explain_3' => "感謝您的配合！",

];