<?php

return [

    // 搜索彈窗部分
    'home_close' => '關閉',
    'home_destination_hot' => '熱門',
    'home_destination_america' => '美洲',
    'home_destination_europe' => '歐洲',
    'home_destination_oceania' => '大洋洲',
    'home_destination_asia' => '亞洲',
    'home_destination_africa' => '非洲',

    //地址自動填充框
    'scenic_city' => '城市',
    'scenic_hotel' => '飯店',
    'scenic_attraction' => '景點',
    'scenic_airport_station' => '機場/車站',


    // 城市名稱部分
    // 熱門
    // 紐約
    // 洛杉磯
    // 芝加哥
    // 多倫多
    // 悉尼
    // 倫敦
    // 柏林
    // 羅馬
    // 威尼斯
    // 馬德裏
    'city_geneva' => '日內瓦',
    // 拉斯維加斯
    // 阿姆斯特丹
    'city_luxembourg' => '盧森堡',

    //美洲
    'city_new_york' => '紐約',
    'city_los_angeles' => '洛杉磯',
    'city_san_francisco' => '舊金山',
    'city_las_vegas' => '拉斯維加斯',
    'city_boston' => '波士頓',
    'city_chicago' => '芝加哥',
    'city_washington' => '華盛頓',
    'city_honolulu' => '檀香山',
    'city_orlando' => '奧蘭多',
    'city_miami' => '邁阿密',
    'city_seattle' => '西雅圖',
    'city_vancouver' => '溫哥華',
    'city_toronto' => '多倫多',
    'city_victoria' => '維多利亞',
    'city_montreal' => '蒙特利爾',
    'city_cancun' => '坎昆',

    // 歐洲
    'city_london' => '倫敦',
    'city_edinburgh' => '愛丁堡',
    'city_paris' => '巴黎',
    'city_nice' => '尼斯',
    'city_lyon' => '裏昂',
    'city_berlin' => '柏林',
    'city_munich' => '慕尼黑',
    'city_frankfurt' => '法蘭克福',
    'city_rome' => '羅馬',
    'city_venice' => '威尼斯',
    'city_florence' => '佛羅倫薩',
    'city_barcelona' => '巴塞羅那',
    'city_madrid' => '馬德裏',
    'city_zurich' => '蘇黎世',
    'city_amsterdam' => '阿姆斯特丹',
    'city_athens' => '雅典',
    'city_milan' => '米蘭',

    // 大洋洲
    'city_sydney' => '悉尼',
    'city_melbourne' => '墨爾本',
    'city_gold_coast' => '黃金海岸',
    'city_keynes' => '凱恩斯',
    'city_brisbane' => '布裏斯班',
    'city_adelaide' => '阿德萊德',
    'city_perth' => '珀斯',
    'city_canberra' => '堪培拉',
    'city_great_barrier_reef'=> '大堡礁',
    'city_oakland' => '奧克蘭',
    'city_queens_town' => '皇後鎮',
    'city_wellington' => '威靈頓',
    'city_christchurch' => '基督城',
    'city_phillip_island' => '菲利普島',
    'city_dublin' => '都柏林',

    // 亞洲
    'city_singapore' => '新加坡',
    'city_seoul' => '首爾',
    'city_jeju_island' => '濟州島',
    'city_bali' => '巴厘島',
    'city_bangkok' => '曼谷',
    'city_phuket' => '普吉島',
    'city_chiang_mai' => '清邁',
    'city_pattaya' => '芭提雅',
    'city_sumei_island' => '蘇梅島',
    'city_kuala_lumpur' => '吉隆坡',
    'city_langkawi_island' => '淩家衛島',
    'city_tokyo' => '東京',
    'city_osaka' => '大阪',
    'city_beijing' => '北京',
    'city_hong_kong' => '香港',

    // 非洲
    'city_cairo' => '開羅',
    'city_johannesburg' => '約翰內斯堡',
    'city_cape_town' => '開普敦',
    'city_casablanca' => '卡薩布蘭卡',
    'city_nairobi' => '內羅畢',
    'city_durban' => '德班',
    'city_tunisia' => '突尼斯',
    'city_bizerte' => '比塞大',
    'city_luxor' => '盧克索',
    'city_lusaka' => '盧薩卡',
    'city_mauritius' => '毛裏求斯',
    'city_accra' => '阿克拉',
    'city_harare' => '哈拉雷',
    'city_marrakesh' => '馬拉喀什',

];

