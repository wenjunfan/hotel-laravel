<?php

return [
    //cfg-old-order
    'title' => "訂單詳情",
    'cinfo' => "入住信息",
    'cname' => "‌聯系人",
    'cphone' => "‌電話",
    'cemail' => "‌郵箱",
    'Room#' => "房間數",
    'guest' => "住客",
    'comment' => "預訂須知",
    'invoice' => "發票",
    'invoiceId' => "發票號",
    'due' => "結算截止日期",
    'desc' => "詳情",
    'bill' => "購貨單位：",
    'amount' => "金額",
    'otherc' => "其他註意事項",
    'hcell' => "飯店電話",
    'specialrequest' => "特殊要求",

];