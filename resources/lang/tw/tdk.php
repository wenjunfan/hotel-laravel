<?php

return [
    // home page
    'title_home' => "飯店預訂，最便宜的北美飯店查詢預訂平台 - :domain走四方飯店",
    'description_home' => "走四方北美飯店預訂平台,為您提供美國、加拿大等北美飯店在線預訂服務,您可以實時查詢飯店房型價格、房態、品牌、設施、電話、地址、星級等信息,在線預訂北美飯店,享受更多優惠價格。",
    'keywords_home' => "便宜飯店，北美飯店，美國飯店，好康飯店",
    
    // list page
    'title_list' => ":name饭店預訂,:name特價饭店,折扣饭店在線預訂- :domain旅遊網",
    'description_list' => "走四方國際飯店預訂平台,為您提供:name在線預訂服務, 您可以實時查詢:name的房間價格、房態、設施、電話、地址、星級等信息,選擇最優惠、最讓您滿意的房型。",
    'keywords_list' => ":name飯店預訂,:name特價飯店預訂,:name折扣飯店預訂",

    // hotel page
    'title_hotel' => ":name 預訂,:name 特價,:name折扣",
    'description_hotel' => "走四方國際飯店預訂平台,為您提供:name在線預訂服務, 您可以實時查詢:name的房間價格、房態、設施、電話、地址、星級等信息,選擇最優惠、最讓您滿意的房型。",
    'keywords_hotel' => ":name 預訂,:name 特價,:name折扣",

];