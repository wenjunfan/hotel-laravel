<?php

return [
    'soc' => "‌預訂金額",
    'totalcharge' => "‌今日應付總額",
    'pymt' => "‌支付方式",
    'cc' => "‌銀行卡",
    'paynow' => "‌立即付款",
    'plzcomplete' => "請用正確的格式完整填寫預訂信息.",
    'failed' => "出錯了，請到您的訂單管理頁查詢訂購結果",

];