<?php

return [
    'title' => "支付寶退款記錄",
    'handling' => "手續費",
    'arefund' => "實際退款",
    'refunds' => "退款狀態",
    'sd' => "起始日期",
    'YMD' => "‌年-月-日",
    'ed' => "結束日期",
    'Search' => "‌查詢",
    'BookingID' => "‌訂單號",
    'orders' => "訂單流水",
    'ar' => "共退款",
    'status' => "退款狀態",
    'refundst' => "退款時間",
    'Refunded' => "‌已取消已退款",
    'Processing' => "‌處理中",

];