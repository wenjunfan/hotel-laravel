<?php

return [
    'soc' => "‌预订金额",
    'totalcharge' => "‌今日应付总额",
    'pymt' => "‌支付方式",
    'cc' => "‌银行卡",
    'paynow' => "‌立即付款",
    'plzcomplete' => "请用正确的格式完整填写预订信息.",
    'failed' => "出错了，请到您的订单管理页查询订购结果",

];