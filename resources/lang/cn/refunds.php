<?php

return [
    //cfg-old-refunds
    'title' => "银行卡退款记录",
    'sd' => "起始日期",
    'ed' => "结束日期",
    'orders' => "订单流水",
    'cn' => "卡号",
    'status' => "退款状态",
    'transactions' => "交易流水",
    'refunds' => "退款流水",
    'rn' => "关联号",
    'cr' => "本次退款",
    'ar' => "共退款",
    'em' => "错误信息",
    'refundst' => "退款时间",
    'action' => "发票",
    's' => "成功",
    'f' => "失败",
    'YMD' => "‌年-月-日",
    'Search' => "酒店搜索",
    'BookingID' => "‌订单号",

];