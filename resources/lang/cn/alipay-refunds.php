<?php

return [
    'title' => "支付宝退款记录",
    'handling' => "手续费",
    'arefund' => "实际退款",
    'refunds' => "退款状态",
    'sd' => "起始日期",
    'YMD' => "‌年-月-日",
    'ed' => "结束日期",
    'Search' => "‌查询",
    'BookingID' => "‌订单号",
    'orders' => "订单流水",
    'ar' => "共退款",
    'status' => "退款状态",
    'refundst' => "退款时间",
    'Refunded' => "‌已取消已退款",
    'Processing' => "‌处理中",

];