<?php

return [

    // 搜索弹窗部分
    'home_close' => '关闭',
    'home_destination_hot' => '热门',
    'home_destination_america' => '美洲',
    'home_destination_europe' => '欧洲',
    'home_destination_oceania' => '大洋洲',
    'home_destination_asia' => '亚洲',
    'home_destination_africa' => '非洲',

    //地址自动填充框
    'scenic_city' => '城市',
    'scenic_hotel' => '酒店',
    'scenic_attraction' => '景点',
    'scenic_airport_station' => '机场/车站',


    // 城市名称部分
    // 热门
    // 纽约
    // 洛杉矶
    // 芝加哥
    // 多伦多
    // 悉尼
    // 伦敦
    // 柏林
    // 罗马
    // 威尼斯
    // 马德里
    'city_geneva' => '日内瓦',
    // 拉斯维加斯
    // 阿姆斯特丹
    'city_luxembourg' => '卢森堡',

    //美洲
    'city_new_york' => '纽约',
    'city_los_angeles' => '洛杉矶',
    'city_san_francisco' => '旧金山',
    'city_las_vegas' => '拉斯维加斯',
    'city_boston' => '波士顿',
    'city_chicago' => '芝加哥',
    'city_washington' => '华盛顿',
    'city_honolulu' => '檀香山',
    'city_orlando' => '奥兰多',
    'city_miami' => '迈阿密',
    'city_seattle' => '西雅图',
    'city_vancouver' => '温哥华',
    'city_toronto' => '多伦多',
    'city_victoria' => '维多利亚',
    'city_montreal' => '蒙特利尔',
    'city_cancun' => '坎昆',

    // 欧洲
    'city_london' => '伦敦',
    'city_edinburgh' => '爱丁堡',
    'city_paris' => '巴黎',
    'city_nice' => '尼斯',
    'city_lyon' => '里昂',
    'city_berlin' => '柏林',
    'city_munich' => '慕尼黑',
    'city_frankfurt' => '法兰克福',
    'city_rome' => '罗马',
    'city_venice' => '威尼斯',
    'city_florence' => '佛罗伦萨',
    'city_barcelona' => '巴塞罗那',
    'city_madrid' => '马德里',
    'city_zurich' => '苏黎世',
    'city_amsterdam' => '阿姆斯特丹',
    'city_athens' => '雅典',
    'city_milan' => '米兰',

    // 大洋洲
    'city_sydney' => '悉尼',
    'city_melbourne' => '墨尔本',
    'city_gold_coast' => '黄金海岸',
    'city_keynes' => '凯恩斯',
    'city_brisbane' => '布里斯班',
    'city_adelaide' => '阿德莱德',
    'city_perth' => '珀斯',
    'city_canberra' => '堪培拉',
    'city_great_barrier_reef'=> '大堡礁',
    'city_oakland' => '奥克兰',
    'city_queens_town' => '皇后镇',
    'city_wellington' => '威灵顿',
    'city_christchurch' => '基督城',
    'city_phillip_island' => '菲利普岛',
    'city_dublin' => '都柏林',

    // 亚洲
    'city_singapore' => '新加坡',
    'city_seoul' => '首尔',
    'city_jeju_island' => '济州岛',
    'city_bali' => '巴厘岛',
    'city_bangkok' => '曼谷',
    'city_phuket' => '普吉岛',
    'city_chiang_mai' => '清迈',
    'city_pattaya' => '芭提雅',
    'city_sumei_island' => '苏梅岛',
    'city_kuala_lumpur' => '吉隆坡',
    'city_langkawi_island' => '凌家卫岛',
    'city_tokyo' => '东京',
    'city_osaka' => '大阪',
    'city_beijing' => '北京',
    'city_hong_kong' => '香港',

    // 非洲
    'city_cairo' => '开罗',
    'city_johannesburg' => '约翰内斯堡',
    'city_cape_town' => '开普敦',
    'city_casablanca' => '卡萨布兰卡',
    'city_nairobi' => '内罗毕',
    'city_durban' => '德班',
    'city_tunisia' => '突尼斯',
    'city_bizerte' => '比塞大',
    'city_luxor' => '卢克索',
    'city_lusaka' => '卢萨卡',
    'city_mauritius' => '毛里求斯',
    'city_accra' => '阿克拉',
    'city_harare' => '哈拉雷',
    'city_marrakesh' => '马拉喀什',

];

