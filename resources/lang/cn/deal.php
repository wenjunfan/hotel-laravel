<?php

return [
    'search_destination' => '目的地',
    'search_dates' => '日期',
    'search_rooms' => '客房',
    'search_change' => '更改搜索',
    'search_cancel' => '取消',
    'search_submit' => '搜索酒店',
    'search_checkin' => '入住时间',
    'search_checkout' => '退房时间',
    'search_room_count' => '房间数',
    'search_adult_count' => '成人/房',
    'search_child_count' => '儿童/房',
    'coupon_step1' => '1. 复制优惠码',
    'coupon_step2' => '2. 在付款页面粘贴<br>酒店优惠码',
    'coupon_step3' => '3. 打折成功，付款完成<br>享受独家优惠。',
    'popular_destination' => '热门目的地',
    'more_hotels' => '更多酒店',
    'policy_title' => '条款和条件',
    'policy_content' => '此促销页面提供的折扣优惠仅适用于本页面酒店，并且仅对入住开始时间<span class="text-danger">:start</span>到<span class="text-danger">:end</span>有效，所有房价需视具体供应情况而定。显示的房价不包含税费和其它小费，走四方酒店网拥有最终解释权。',
    'book_now' => '预定',
];