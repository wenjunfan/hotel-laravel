<?php

return [
    'title' => '404错误',
    'error_desc' => '十分的抱歉！找不到您要找的页面.....',
    'error_so_title' => '您可以',
    'error_so_1' => '1.请检查输入的网址是否正确',
    'error_so_2' => '2.致电走四方旅游网热线：<a href="tel:25-6668-0241">25-6668-0241</a>。',
    'back_home' => '返回首页'
];