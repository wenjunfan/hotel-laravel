<?php

return [
    // home page
    'title_home' => "酒店预订，最便宜的北美酒店查询预订平台 - :domain走四方酒店",
    'description_home' => ":domain 北美酒店预订平台,为您提供美国、加拿大等北美酒店在线预订服务,您可以实时查询酒店房型价格、房态、品牌、设施、电话、地址、星级等信息,在线预订北美酒店,享受更多优惠价格。",
    'keywords_home' => "便宜酒店，北美酒店，美国酒店，特价酒店",
    
    // list page
    'title_list' => ":name酒店预订,:name特价酒店,折扣酒店在线预订- :domain走四方旅游网",
    'description_list' => ":domain走四方国际酒店预订平台,为您提供:name酒店在线预订且可享受超值优惠。查询符合您预算并提供最低价格保证的:name酒店。",
    'keywords_list' => ":name酒店预订,:name特价酒店预订,:name折扣酒店预订",

    // hotel page
    'title_hotel' => ":name 預訂,:name 特價,:name折扣",
    'description_hotel' => "走四方国际酒店预订平台,为您提供:name在线预订服务,您可以实时查询:name的房间价格、房态、设施、电话、地址、星级等信息,选择最优惠、最让您满意的房型。",
    'keywords_hotel' => ":name预订, :name特价,:name折扣",
];