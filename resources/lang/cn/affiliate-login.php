<?php

return [

    'password' => '密码',
    'email' => '电子邮箱',
    'login' => '登录',
    'remember_me' => '记住我',
    'forgot_password' => '忘记密码',
    'affiliate_user_login' => '分销用户登入',
    'no_account' => '没有账号',
    'register_now' => '马上注册',
];
