<?php

return [
    //cfg-old-order
    'title' => "订单详情",
    'cinfo' => "入住信息",
    'cname' => "‌联系人",
    'cphone' => "‌电话",
    'cemail' => "‌邮箱",
    'Room#' => "房间数",
    'guest' => "住客",
    'comment' => "预订须知",
    'invoice' => "发票",
    'invoiceId' => "发票号",
    'due' => "结算截止日期",
    'desc' => "详情",
    'bill' => "购货单位：",
    'amount' => "金额",
    'otherc' => "其他注意事项",
    'hcell' => "酒店电话",
    'specialrequest' => "特殊要求",

];