<?php

return [
    'cashing_request_true' => '我们已经收到您的请求，财务稍后会联系您兑现方式',
    'cashing_request_false' => '出错啦，请稍后再试。谢谢！',
    'cashing_request_false_amount' => '出错啦，您还没有达到$50的最低佣金兑现标准！',
    'cashing_not_accrued' => ' 所有订单退房后的佣金',
    'redirect_pc' => ' 很抱歉，请使用电脑去查阅您的订单详情，我们暂时不支持手机查看订单',
];