<?php

return [
//    group hotel request
    'group_title' => '创建您的团体酒店需求',
    'group_description' => '请通过填写以下表格来告诉我们您的需求，请务必在提交之前检查所有详细信息，我们的代表将很快与您联系。',
    'group_input_city' => '目的地城市',
    'group_input_checkin' => '入住日期',
    'group_input_checkout' => '退房日期',
    'group_add_another' => '添加另一备选目的地或者其他日期',
    'group_add_close' => '- 收起',
    'group_input_backup_city' => '备选目的地城市',
    'group_input_group_code' => '团体编号 (选填)',
    'group_input_rpn' => '每晚客房数量 (9+)',
    'group_input_star' => '理想酒店星级',
    'stars' => '星级',
    'group_input_budget' => '理想的每晚预算',
    'group_contact' => '我们应将报价发送至哪里?',
    'group_input_name' => '联系人姓名',
    'group_input_company' => '公司名称 (选填)',
    'group_input_email' => '电子邮件',
    'group_input_phone' => '联系电话',
    'group_input_additional' => '其他备注或请求 (选填)',
    'modal_btn_submit' => '提交',
    'btn_submit_check' => '请在提交前检查上述申请信息的准确与完整性',
    'btn_on_submit' => '提交成功，我们会尽快联系您并提供最优的酒店报价',
    'please_check_input' => '请仔细检查必填选项，谢谢',
];
